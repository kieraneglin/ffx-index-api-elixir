defmodule FfxIndex.MixesTest do
  use FfxIndex.DataCase

  alias FfxIndex.Mixes

  describe "mixes" do
    alias FfxIndex.Mixes.Mix

    @valid_attrs %{description: "some description", name: "some name", slug: "some slug"}
    @update_attrs %{description: "some updated description", name: "some updated name", slug: "some updated slug"}
    @invalid_attrs %{description: nil, name: nil, slug: nil}

    def mix_fixture(attrs \\ %{}) do
      {:ok, mix} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Mixes.create_mix()

      mix
    end

    test "list_mixes/0 returns all mixes" do
      mix = mix_fixture()
      assert Mixes.list_mixes() == [mix]
    end

    test "get_mix!/1 returns the mix with given id" do
      mix = mix_fixture()
      assert Mixes.get_mix!(mix.id) == mix
    end

    test "create_mix/1 with valid data creates a mix" do
      assert {:ok, %Mix{} = mix} = Mixes.create_mix(@valid_attrs)
      assert mix.description == "some description"
      assert mix.name == "some name"
      assert mix.slug == "some slug"
    end

    test "create_mix/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Mixes.create_mix(@invalid_attrs)
    end

    test "update_mix/2 with valid data updates the mix" do
      mix = mix_fixture()
      assert {:ok, %Mix{} = mix} = Mixes.update_mix(mix, @update_attrs)
      assert mix.description == "some updated description"
      assert mix.name == "some updated name"
      assert mix.slug == "some updated slug"
    end

    test "update_mix/2 with invalid data returns error changeset" do
      mix = mix_fixture()
      assert {:error, %Ecto.Changeset{}} = Mixes.update_mix(mix, @invalid_attrs)
      assert mix == Mixes.get_mix!(mix.id)
    end

    test "delete_mix/1 deletes the mix" do
      mix = mix_fixture()
      assert {:ok, %Mix{}} = Mixes.delete_mix(mix)
      assert_raise Ecto.NoResultsError, fn -> Mixes.get_mix!(mix.id) end
    end

    test "change_mix/1 returns a mix changeset" do
      mix = mix_fixture()
      assert %Ecto.Changeset{} = Mixes.change_mix(mix)
    end
  end
end
