defmodule FfxIndexWeb.MixControllerTest do
  use FfxIndexWeb.ConnCase

  alias FfxIndex.Mixes
  alias FfxIndex.Mixes.Mix

  @create_attrs %{
    description: "some description",
    name: "some name",
    slug: "some slug"
  }
  @update_attrs %{
    description: "some updated description",
    name: "some updated name",
    slug: "some updated slug"
  }
  @invalid_attrs %{description: nil, name: nil, slug: nil}

  def fixture(:mix) do
    {:ok, mix} = Mixes.create_mix(@create_attrs)
    mix
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all mixes", %{conn: conn} do
      conn = get(conn, Routes.mix_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create mix" do
    test "renders mix when data is valid", %{conn: conn} do
      conn = post(conn, Routes.mix_path(conn, :create), mix: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.mix_path(conn, :show, id))

      assert %{
               "id" => id,
               "description" => "some description",
               "name" => "some name",
               "slug" => "some slug"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.mix_path(conn, :create), mix: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update mix" do
    setup [:create_mix]

    test "renders mix when data is valid", %{conn: conn, mix: %Mix{id: id} = mix} do
      conn = put(conn, Routes.mix_path(conn, :update, mix), mix: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.mix_path(conn, :show, id))

      assert %{
               "id" => id,
               "description" => "some updated description",
               "name" => "some updated name",
               "slug" => "some updated slug"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, mix: mix} do
      conn = put(conn, Routes.mix_path(conn, :update, mix), mix: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete mix" do
    setup [:create_mix]

    test "deletes chosen mix", %{conn: conn, mix: mix} do
      conn = delete(conn, Routes.mix_path(conn, :delete, mix))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.mix_path(conn, :show, mix))
      end
    end
  end

  defp create_mix(_) do
    mix = fixture(:mix)
    {:ok, mix: mix}
  end
end
