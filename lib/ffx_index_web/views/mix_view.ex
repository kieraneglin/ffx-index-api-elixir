defmodule FfxIndexWeb.MixView do
  use FfxIndexWeb, :view
  alias FfxIndex.Mixes.Mix
  alias FfxIndex.Items.Item

  def render("index.json", %{mixes: mixes}) do
    Enum.map(mixes, fn mix -> Mix.to_map(mix) end)
  end

  def render("show.json", %{mix: mix}) do
    Mix.to_map(mix)
      |> Map.merge(%{
        combinations: Enum.map(mix.mix_items, fn mix_item ->
          %{
            item_one: to_combination_map(mix_item.item_one),
            item_two: to_combination_map(mix_item.item_two)
          }
        end)
      })
  end

  defp to_combination_map(item) do
    Item.to_map(item)
      |> Map.take([:id, :name, :slug, :path])
  end
end
