defmodule FfxIndexWeb.MonsterView do
  use FfxIndexWeb, :view
  alias FfxIndex.Monsters
  alias FfxIndex.Monsters.Monster
  alias FfxIndex.Elements.Element
  alias FfxIndex.Locations.Location

  def render("index.json", %{monsters: monsters}) do
    Enum.map(monsters, fn monster -> 
      Monster.to_map(monster) |> Map.take([:id, :name, :slug, :path])
    end)
  end

  def render("show.json", %{monster: monster}) do
    weapon_drops = Monsters.get_drops(monster, "weapon")
    armour_drops = Monsters.get_drops(monster, "armour")

    Monster.to_map(monster)
      |> Map.merge(%{
        locations: Enum.map(monster.locations, fn location -> Location.to_map(location) end),
        elements: Element.to_map(monster.element),
        bribe_drop: bribe_drop_map(monster),
        kill_drops: Enum.map(monster.kill_drops, fn kill_drop -> kill_drop_map(kill_drop) end),
        steal_drops: Enum.map(monster.steal_drops, fn steal_drop -> steal_drop_map(steal_drop) end),
        drop_abilities: %{
          weapons: Enum.map(weapon_drops, fn weapon_drop -> ability_drop_map(weapon_drop) end),
          armours: Enum.map(armour_drops, fn armour_drop -> ability_drop_map(armour_drop) end)
        }
      })
  end

  defp bribe_drop_map(monster) do
    case monster.bribe_drop do
      nil -> nil
      _ -> %{
        id: monster.bribe_drop.item.id,
        name: monster.bribe_drop.item.name,
        amount: monster.bribe_drop.amount,
        cost: monster.bribe_drop.cost,
        slug: monster.bribe_drop.item.name,
        path: "/items/#{monster.bribe_drop.item.slug}"
      }
    end
  end

  defp kill_drop_map(kill_drop) do
    %{
      id: kill_drop.item.id,
      name: kill_drop.item.name,
      amount: kill_drop.amount,
      rare: kill_drop.rare,
      slug: kill_drop.item.slug,
      path: "/items/#{kill_drop.item.slug}"
    }
  end

  defp steal_drop_map(steal_drop) do
    %{
      id: steal_drop.item.id,
      name: steal_drop.item.name,
      amount: steal_drop.amount,
      rare: steal_drop.rare,
      slug: steal_drop.item.slug,
      path: "/items/#{steal_drop.item.slug}"
    }
  end

  defp ability_drop_map(drop) do
    %{
      id: drop.ability.id,
      name: drop.ability.name,
      slug: drop.ability.slug,
      path: "/abilities/#{drop.ability.slug}"
    }
  end
end
