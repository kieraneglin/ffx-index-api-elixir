defmodule FfxIndexWeb.AbilityView do
  use FfxIndexWeb, :view
  alias FfxIndex.Abilities.Ability

  def render("index.json", %{abilities: abilities}) do
    Enum.map(abilities, fn ability -> 
      Ability.to_map(ability)
        |> Map.merge(%{
          item: item_map(ability.item)
        })
    end)
  end

  def render("show.json", %{ability: ability}) do
    Ability.to_map(ability)
      |> Map.merge(%{
        item: item_map(ability.item),
        monster_drops: Enum.map(ability.monster_drop_abilities, fn drop_ability -> 
          monster_drop_map(drop_ability)
        end)
      })
  end

  defp item_map(item) do
    case item do
      nil -> nil
      _ -> %{
        id: item.id,
        name: item.name,
        slug: item.slug,
        path: "/items/#{item.slug}"
      }
    end
  end

  defp monster_drop_map(monster_drop) do
    %{
      id: monster_drop.monster.id,
      name: monster_drop.monster.name,
      slug: monster_drop.monster.slug,
      path: "/monsters/#{monster_drop.monster.slug}"
    }
  end
end
