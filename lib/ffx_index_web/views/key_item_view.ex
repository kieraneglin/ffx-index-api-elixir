defmodule FfxIndexWeb.KeyItemView do
  use FfxIndexWeb, :view
  alias FfxIndex.Items.KeyItem
  alias FfxIndex.Locations.Location

  def render("index.json", %{key_items: key_items}) do
    Enum.map(key_items, fn key_item -> 
      KeyItem.to_map(key_item)
        |> Map.merge(%{
          location: location_map(key_item)
        })
    end)
  end

  def render("show.json", %{key_item: key_item}) do
    KeyItem.to_map(key_item)
      |> Map.merge(%{
        location: location_map(key_item)
      })
  end

  defp location_map(key_item) do
    case key_item.location do
      nil -> nil
      _ -> Location.to_map(key_item.location)
    end
  end
end
