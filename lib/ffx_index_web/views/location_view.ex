defmodule FfxIndexWeb.LocationView do
  use FfxIndexWeb, :view
  alias FfxIndex.Locations.Location

  def render("index.json", %{locations: locations}) do
    Enum.map(locations, fn location -> Location.to_map(location) end)
  end

  def render("show.json", %{location: location}) do
    Location.to_map(location)
      |> Map.merge(%{
        monsters: Enum.map(location.monsters, fn monster -> monster_map(monster) end),
        key_items: Enum.map(location.key_items, fn key_item -> key_item_map(key_item) end)
      })
  end

  defp monster_map(monster) do
    %{
      id: monster.id,
      name: monster.name,
      slug: monster.slug,
      path: "/monsters/#{monster.slug}"
    }
  end

  defp key_item_map(key_item) do
    %{
      id: key_item.id,
      name: key_item.name,
      description: key_item.description,
      slug: key_item.slug,
      path: "/key-items/#{key_item.slug}"
    }
  end
end
