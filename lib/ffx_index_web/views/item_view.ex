defmodule FfxIndexWeb.ItemView do
  use FfxIndexWeb, :view
  alias FfxIndex.Mixes.Mix
  alias FfxIndex.Items
  alias FfxIndex.Items.Item
  alias FfxIndex.Abilities.Ability

  def render("index.json", %{items: items}) do
    Enum.map(items, fn item -> Item.to_map(item) end)
  end

  def render("show.json", %{item: item}) do
    mixes = Items.get_item_mixes!(item)

    Item.to_map(item)
      |> Map.merge(%{
        mixes: Enum.map(mixes, fn mix -> Mix.to_map(mix) end),
        abilities: Enum.map(item.abilities, fn ability -> Ability.to_map(ability) end),
        bribe_drops: Enum.map(item.bribe_drops, fn bribe_drop -> bribe_drop_map(bribe_drop) end),
        kill_drops: Enum.map(item.kill_drops, fn kill_drop -> kill_drop_map(kill_drop) end),
        steal_drops: Enum.map(item.steal_drops, fn steal_drop -> steal_drop_map(steal_drop) end)
      })
  end

  # Doing this instead of a map on the model since it's not reusable and it's more complex
  defp bribe_drop_map(bribe_drop) do
    %{
      cost: bribe_drop.cost,
      amount: bribe_drop.amount,
      monster: %{
        id: bribe_drop.monster.id,
        name: bribe_drop.monster.name,
        slug: bribe_drop.monster.slug,
        path: "/monsters/#{bribe_drop.monster.slug}"
      }
    }
  end

  defp kill_drop_map(kill_drop) do
    %{
      rare: kill_drop.rare,
      amount: kill_drop.amount,
      monster: %{
        id: kill_drop.monster.id,
        name: kill_drop.monster.name,
        slug: kill_drop.monster.slug,
        path: "/monsters/#{kill_drop.monster.slug}"
      }
    }
  end

  defp steal_drop_map(steal_drop) do
    %{
      rare: steal_drop.rare,
      amount: steal_drop.amount,
      monster: %{
        id: steal_drop.monster.id,
        name: steal_drop.monster.name,
        slug: steal_drop.monster.slug,
        path: "/monsters/#{steal_drop.monster.slug}"
      }
    }
  end
end
