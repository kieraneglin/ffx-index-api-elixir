defmodule FfxIndexWeb.Router do
  use FfxIndexWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug CORSPlug, origin: ["http://localhost:8080"]
  end

  scope "/api", FfxIndexWeb do
    pipe_through :api

    resources "/mixes", MixController, only: [:show, :index]
    resources "/items", ItemController, only: [:show, :index]
    resources "/monsters", MonsterController, only: [:show, :index]
    resources "/key-items", KeyItemController, only: [:show, :index]
    resources "/abilities", AbilityController, only: [:show, :index]
    resources "/locations", LocationController, only: [:show, :index]
  end
end
