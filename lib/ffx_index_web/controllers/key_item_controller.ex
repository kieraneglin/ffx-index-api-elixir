defmodule FfxIndexWeb.KeyItemController do
  use FfxIndexWeb, :controller

  alias FfxIndex.Items

  action_fallback FfxIndexWeb.FallbackController

  def index(conn, _params) do
    conn
      |> put_resp_header("cache-control", "public, max-age=86400")
      |> render("index.json", key_items: Items.list_key_items())
  end

  def show(conn, %{"id" => id}) do
    conn
      |> put_resp_header("cache-control", "public, max-age=86400")
      |> render("show.json", key_item: Items.get_key_item!(id))
  end
end
