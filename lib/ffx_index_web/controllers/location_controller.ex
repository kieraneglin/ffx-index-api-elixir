defmodule FfxIndexWeb.LocationController do
  use FfxIndexWeb, :controller

  alias FfxIndex.Locations

  action_fallback FfxIndexWeb.FallbackController

  def index(conn, _params) do
    conn
      |> put_resp_header("cache-control", "public, max-age=86400")
      |> render("index.json", locations: Locations.list_locations())
  end

  def show(conn, %{"id" => id}) do
    conn
      |> put_resp_header("cache-control", "public, max-age=86400")
      |> render("show.json", location: Locations.get_location!(id))
  end
end
