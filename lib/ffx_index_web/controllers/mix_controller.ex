defmodule FfxIndexWeb.MixController do
  use FfxIndexWeb, :controller

  alias FfxIndex.Mixes

  action_fallback FfxIndexWeb.FallbackController

  def index(conn, _params) do
    conn
      |> put_resp_header("cache-control", "public, max-age=86400")
      |> render("index.json", mixes: Mixes.list_mixes())
  end

  def show(conn, %{"id" => id}) do
    conn
      |> put_resp_header("cache-control", "public, max-age=86400")
      |> render("show.json", mix: Mixes.get_mix!(id))
  end
end
