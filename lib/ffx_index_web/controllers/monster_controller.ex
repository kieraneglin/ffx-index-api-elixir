defmodule FfxIndexWeb.MonsterController do
  use FfxIndexWeb, :controller

  alias FfxIndex.Monsters

  action_fallback FfxIndexWeb.FallbackController

  def index(conn, _params) do
    conn
      |> put_resp_header("cache-control", "public, max-age=86400")
      |> render("index.json", monsters: Monsters.list_monsters())
  end

  def show(conn, %{"id" => id}) do
    conn
      |> put_resp_header("cache-control", "public, max-age=86400")
      |> render("show.json", monster: Monsters.get_monster!(id))
  end
end
