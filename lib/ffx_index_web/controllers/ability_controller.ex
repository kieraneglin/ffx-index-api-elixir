defmodule FfxIndexWeb.AbilityController do
  use FfxIndexWeb, :controller

  alias FfxIndex.Abilities

  action_fallback FfxIndexWeb.FallbackController

  def index(conn, _params) do
    conn
      |> put_resp_header("cache-control", "public, max-age=86400")
      |> render("index.json", abilities: Abilities.list_abilities())
  end

  def show(conn, %{"id" => id}) do
    conn
      |> put_resp_header("cache-control", "public, max-age=86400")
      |> render("show.json", ability: Abilities.get_ability!(id))
  end
end
