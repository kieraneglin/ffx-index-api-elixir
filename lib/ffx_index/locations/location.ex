defmodule FfxIndex.Locations.Location do
  use Ecto.Schema

  schema "locations" do
    field :name, :string
    field :slug, :string

    has_many :key_items, FfxIndex.Items.KeyItem
    many_to_many :monsters, FfxIndex.Monsters.Monster, join_through: "locations_monsters"

    timestamps(inserted_at: :created_at)
  end

  def to_map(location) do
    %{
      id: location.id,
      name: location.name,
      slug: location.slug,
      path: "/locations/#{location.slug}"
    }
  end
end
