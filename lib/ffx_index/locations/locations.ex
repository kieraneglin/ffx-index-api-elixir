defmodule FfxIndex.Locations do
  import Ecto.Query, warn: false
  alias FfxIndex.Repo
  alias FfxIndex.Items.KeyItem
  alias FfxIndex.Monsters.Monster
  alias FfxIndex.Locations.Location

  def list_locations do
    query = from location in Location, 
      order_by: location.name

    query |> Repo.all
  end

  def get_location!(id_or_slug) do 
    Location 
      |> Repo.find_by_id_or_slug!(id_or_slug)
      |> Repo.preload([
        monsters: from(m in Monster, order_by: m.name), 
        key_items: from(k in KeyItem, order_by: k.name)
      ])
  end
end
