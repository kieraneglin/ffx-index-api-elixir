defmodule FfxIndex.Repo do
  use Ecto.Repo,
    otp_app: :ffx_index,
    adapter: Ecto.Adapters.Postgres

  def find_by_id_or_slug!(module, id) do
    case Integer.parse(id) do
      {_, ""} -> get!(module, id)
      _ -> get_by!(module, slug: id)
    end
  end
end
