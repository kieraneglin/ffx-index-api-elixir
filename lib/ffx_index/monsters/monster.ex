defmodule FfxIndex.Monsters.Monster do
  use Ecto.Schema

  schema "monsters" do
    field :name, :string
    field :health, :integer
    field :overkill, :integer
    field :strength, :integer
    field :defense, :integer
    field :magic, :integer
    field :magic_defense, :integer
    field :mp, :integer
    field :agility, :integer
    field :luck, :integer
    field :ap, :integer
    field :evasion, :integer
    field :accuracy, :integer
    field :gil, :integer
    field :boss, :boolean
    field :notes, :string
    field :skills, :string
    field :slug, :string

    has_one :element, FfxIndex.Elements.Element
    has_one :bribe_drop, FfxIndex.Drops.BribeDrop
    has_many :kill_drops, FfxIndex.Drops.KillDrop
    has_many :steal_drops, FfxIndex.Drops.StealDrop
    has_many :monster_drop_abilities, FfxIndex.Abilities.MonsterDropAbility
    many_to_many :locations, FfxIndex.Locations.Location, join_through: "locations_monsters"

    timestamps(inserted_at: :created_at)
  end

  def to_map(monster) do
    %{
      id: monster.id,
      name: monster.name,
      health: monster.health,
      overkill: monster.overkill,
      strength: monster.strength,
      defense: monster.defense,
      magic: monster.magic,
      magic_defense: monster.magic_defense,
      mp: monster.mp,
      agility: monster.agility,
      luck: monster.luck,
      ap: monster.ap,
      evasion: monster.evasion,
      accuracy: monster.accuracy,
      gil: monster.gil,
      boss: monster.boss,
      notes: monster.notes,
      skills: monster.skills,
      slug: monster.slug,
      path: "/monsters/#{monster.slug}"
    }
  end
end
