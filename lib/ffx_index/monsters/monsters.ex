defmodule FfxIndex.Monsters do
  import Ecto.Query, warn: false
  alias FfxIndex.Repo
  alias FfxIndex.Items.Item
  alias FfxIndex.Monsters.Monster
  alias FfxIndex.Abilities.Ability
  alias FfxIndex.Locations.Location
  alias FfxIndex.Abilities.MonsterDropAbility

  def list_monsters do
    Repo.all(Monster)
  end

  def get_monster!(id_or_slug) do 
    Monster
      |> Repo.find_by_id_or_slug!(id_or_slug)
      |> Repo.preload(monster_preload())
  end

  def get_drops(monster, drop_type) do
    query = from drop in MonsterDropAbility,
      where: drop.ability_type == ^drop_type and drop.monster_id == ^monster.id,
      preload: :ability

    query |> Repo.all
  end

  defp monster_preload do
    [
      element: [],
      locations: from(l in Location, order_by: l.name),
      bribe_drop: [item: from(i in Item, order_by: i.name)],
      kill_drops: [item: from(i in Item, order_by: i.name)],
      steal_drops: [item: from(i in Item, order_by: i.name)],
    ]
  end
end
