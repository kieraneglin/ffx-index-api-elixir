defmodule FfxIndex.Abilities.Ability do
  use Ecto.Schema

  schema "abilities" do
    field :name, :string
    field :ability_type, :string
    field :effect, :string
    field :item_amount, :integer
    field :slug, :string

    belongs_to :item, FfxIndex.Items.Item
    has_many :monster_drop_abilities, FfxIndex.Abilities.MonsterDropAbility

    timestamps(inserted_at: :created_at)
  end

  def to_map(ability) do
    %{
      id: ability.id,
      name: ability.name,
      ability_type: ability.ability_type,
      effect: ability.effect,
      item_amount: ability.item_amount,
      slug: ability.slug,
      path: "/abilities/#{ability.slug}"
    }
  end
end
