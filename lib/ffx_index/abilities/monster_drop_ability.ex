defmodule FfxIndex.Abilities.MonsterDropAbility do
  use Ecto.Schema

  schema "monster_drop_abilities" do
    field :ability_type, :string

    belongs_to :monster, FfxIndex.Monsters.Monster
    belongs_to :ability, FfxIndex.Abilities.Ability

    timestamps(inserted_at: :created_at)
  end
end
