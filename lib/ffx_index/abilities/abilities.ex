defmodule FfxIndex.Abilities do
  import Ecto.Query, warn: false
  alias FfxIndex.Repo
  alias FfxIndex.Monsters.Monster
  alias FfxIndex.Abilities.Ability

  def list_abilities do
    query = from ability in Ability, 
      preload: [:item],
      order_by: ability.name

    query |> Repo.all
  end

  def get_ability!(id_or_slug) do 
    Ability
      |> Repo.find_by_id_or_slug!(id_or_slug)
      |> Repo.preload(ability_preload())
  end

  defp ability_preload do
    [
      :item,
      monster_drop_abilities: [
        monster: from(m in Monster, order_by: m.name)
      ]
    ]
  end
end
