defmodule FfxIndex.Drops.KillDrop do
  use Ecto.Schema

  schema "kill_drops" do
    field :rare, :boolean
    field :amount, :integer

    belongs_to :item, FfxIndex.Items.Item
    belongs_to :monster, FfxIndex.Monsters.Monster

    timestamps(inserted_at: :created_at)
  end

  def to_map(kill_drop) do
    %{
      id: kill_drop.id,
      rare: kill_drop.rare,
      amount: kill_drop.amount
    }
  end
end
