defmodule FfxIndex.Drops.StealDrop do
  use Ecto.Schema

  schema "steal_drops" do
    field :rare, :boolean
    field :amount, :integer

    belongs_to :item, FfxIndex.Items.Item
    belongs_to :monster, FfxIndex.Monsters.Monster

    timestamps(inserted_at: :created_at)
  end

  def to_map(steal_drop) do
    %{
      id: steal_drop.id,
      rare: steal_drop.rare,
      amount: steal_drop.amount
    }
  end
end
