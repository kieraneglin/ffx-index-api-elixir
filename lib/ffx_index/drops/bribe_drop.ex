defmodule FfxIndex.Drops.BribeDrop do
  use Ecto.Schema

  schema "bribe_drops" do
    field :cost, :integer
    field :amount, :integer

    belongs_to :item, FfxIndex.Items.Item
    belongs_to :monster, FfxIndex.Monsters.Monster

    timestamps(inserted_at: :created_at)
  end

  def to_map(bribe_drop) do
    %{
      id: bribe_drop.id,
      cost: bribe_drop.cost,
      amount: bribe_drop.amount
    }
  end
end
