defmodule FfxIndex.Items.Item do
  use Ecto.Schema

  schema "items" do
    field :name, :string
    field :effect, :string
    field :effect_type, :string
    field :slug, :string

    has_many :abilities, FfxIndex.Abilities.Ability
    has_many :bribe_drops, FfxIndex.Drops.BribeDrop
    has_many :kill_drops, FfxIndex.Drops.KillDrop
    has_many :steal_drops, FfxIndex.Drops.StealDrop

    timestamps(inserted_at: :created_at)
  end

  def to_map(item) do
    %{
      id: item.id,
      name: item.name,
      effect: item.effect,
      effect_type: item.effect_type,
      slug: item.slug,
      path: "/items/#{item.slug}"
    }
  end
end
