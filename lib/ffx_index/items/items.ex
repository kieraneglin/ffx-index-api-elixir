defmodule FfxIndex.Items do
  import Ecto.Query, warn: false
  alias FfxIndex.Repo
  alias FfxIndex.Mixes.Mix
  alias FfxIndex.Items.Item
  alias FfxIndex.Items.KeyItem
  alias FfxIndex.Monsters.Monster

  def list_items do
    query = from item in Item, 
      order_by: item.name

    query |> Repo.all
  end

  def get_item!(id_or_slug) do 
    Item 
      |> Repo.find_by_id_or_slug!(id_or_slug)
      |> Repo.preload(item_preloads())
  end

  def list_key_items do
    query = from key_item in KeyItem, 
      preload: [:location],
      order_by: key_item.name

    query |> Repo.all
  end

  def get_key_item!(id_or_slug) do 
    KeyItem 
      |> Repo.find_by_id_or_slug!(id_or_slug)
      |> Repo.preload([:location])
  end

  def get_item_mixes!(item) do
    query = from mix in Mix,
      join: mi in assoc(mix, :mix_items), 
      where: mi.item_one_id == ^item.id or mi.item_two_id == ^item.id,
      distinct: true,
      order_by: mix.name

    query |> Repo.all
  end

  defp item_preloads do
    [
      :abilities,
      bribe_drops: [
        monster: from(m in Monster, order_by: m.name)
      ],
      kill_drops: [
        monster: from(m in Monster, order_by: m.name)
      ],
      steal_drops: [
        monster: from(m in Monster, order_by: m.name)
      ]
    ]
  end
end
