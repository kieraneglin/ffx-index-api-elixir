defmodule FfxIndex.Items.KeyItem do
  use Ecto.Schema

  schema "key_items" do
    field :name, :string
    field :details, :string
    field :description, :string
    field :slug, :string

    belongs_to :location, FfxIndex.Locations.Location

    timestamps(inserted_at: :created_at)
  end

  def to_map(key_item) do
    %{
      id: key_item.id,
      name: key_item.name,
      details: key_item.details,
      description: key_item.description,
      slug: key_item.slug,
      path: "/key-items/#{key_item.slug}"
    }
  end
end
