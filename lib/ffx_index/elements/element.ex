defmodule FfxIndex.Elements.Element do
  use Ecto.Schema

  schema "elements" do
    field :ice, :float
    field :fire, :float
    field :water, :float
    field :thunder, :float

    belongs_to :monster, FfxIndex.Monsters.Monster

    timestamps(inserted_at: :created_at)
  end

  def to_map(element) do
    %{
      ice: element.ice,
      fire: element.fire,
      water: element.water,
      thunder: element.thunder,
    }
  end
end
