defmodule FfxIndex.Mixes.MixItem do
  use Ecto.Schema

  schema "mix_items" do
    belongs_to :item_one, FfxIndex.Items.Item
    belongs_to :item_two, FfxIndex.Items.Item
    belongs_to :mix, FfxIndex.Mixes.Mix

    timestamps(inserted_at: :created_at)
  end
end
