defmodule FfxIndex.Mixes.Mix do
  use Ecto.Schema

  schema "mixes" do
    field :name, :string
    field :description, :string
    field :slug, :string

    has_many :mix_items, FfxIndex.Mixes.MixItem

    timestamps(inserted_at: :created_at)
  end

  def to_map(mix) do
    %{
      id: mix.id,
      name: mix.name,
      description: mix.description,
      slug: mix.slug,
      path: "/mixes/#{mix.slug}"
    }
  end
end
