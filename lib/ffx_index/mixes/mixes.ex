defmodule FfxIndex.Mixes do
  import Ecto.Query, warn: false
  alias FfxIndex.Repo
  alias FfxIndex.Mixes.Mix
  alias FfxIndex.Items.Item

  def list_mixes do
    query = from mix in Mix, 
      order_by: mix.name

    query |> Repo.all
  end

  def get_mix!(id_or_slug) do 
    Mix
      |> Repo.find_by_id_or_slug!(id_or_slug)
      |> Repo.preload(mix_items: [
        item_one: from(i in Item, order_by: i.name), 
        item_two: from(i in Item, order_by: i.name)
      ])
  end
end
