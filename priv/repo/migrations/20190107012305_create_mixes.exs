defmodule FfxIndex.Repo.Migrations.CreateMixes do
  use Ecto.Migration

  def change do
    create table(:mixes) do
      add :name, :string
      add :description, :string
      add :slug, :string

      timestamps()
    end

  end
end
