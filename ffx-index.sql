--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: abilities; Type: TABLE; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE TABLE abilities (
    id bigint NOT NULL,
    name character varying,
    ability_type character varying,
    item_id bigint,
    item_amount integer,
    effect character varying,
    slug character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE abilities OWNER TO "Kieran";

--
-- Name: abilities_id_seq; Type: SEQUENCE; Schema: public; Owner: Kieran
--

CREATE SEQUENCE abilities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE abilities_id_seq OWNER TO "Kieran";

--
-- Name: abilities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Kieran
--

ALTER SEQUENCE abilities_id_seq OWNED BY abilities.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE ar_internal_metadata OWNER TO "Kieran";

--
-- Name: bribe_drops; Type: TABLE; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE TABLE bribe_drops (
    id bigint NOT NULL,
    monster_id bigint,
    item_id bigint,
    amount integer,
    cost integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE bribe_drops OWNER TO "Kieran";

--
-- Name: bribe_drops_id_seq; Type: SEQUENCE; Schema: public; Owner: Kieran
--

CREATE SEQUENCE bribe_drops_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bribe_drops_id_seq OWNER TO "Kieran";

--
-- Name: bribe_drops_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Kieran
--

ALTER SEQUENCE bribe_drops_id_seq OWNED BY bribe_drops.id;


--
-- Name: elements; Type: TABLE; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE TABLE elements (
    id bigint NOT NULL,
    monster_id bigint,
    fire double precision,
    thunder double precision,
    ice double precision,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    water double precision
);


ALTER TABLE elements OWNER TO "Kieran";

--
-- Name: elements_id_seq; Type: SEQUENCE; Schema: public; Owner: Kieran
--

CREATE SEQUENCE elements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE elements_id_seq OWNER TO "Kieran";

--
-- Name: elements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Kieran
--

ALTER SEQUENCE elements_id_seq OWNED BY elements.id;


--
-- Name: items; Type: TABLE; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE TABLE items (
    id bigint NOT NULL,
    name character varying,
    effect text,
    effect_type character varying,
    slug character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE items OWNER TO "Kieran";

--
-- Name: items_id_seq; Type: SEQUENCE; Schema: public; Owner: Kieran
--

CREATE SEQUENCE items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE items_id_seq OWNER TO "Kieran";

--
-- Name: items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Kieran
--

ALTER SEQUENCE items_id_seq OWNED BY items.id;


--
-- Name: key_items; Type: TABLE; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE TABLE key_items (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    location_id bigint,
    details text,
    slug character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE key_items OWNER TO "Kieran";

--
-- Name: key_items_id_seq; Type: SEQUENCE; Schema: public; Owner: Kieran
--

CREATE SEQUENCE key_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE key_items_id_seq OWNER TO "Kieran";

--
-- Name: key_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Kieran
--

ALTER SEQUENCE key_items_id_seq OWNED BY key_items.id;


--
-- Name: kill_drops; Type: TABLE; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE TABLE kill_drops (
    id bigint NOT NULL,
    monster_id bigint,
    item_id bigint,
    amount integer,
    rare boolean DEFAULT false,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE kill_drops OWNER TO "Kieran";

--
-- Name: kill_drops_id_seq; Type: SEQUENCE; Schema: public; Owner: Kieran
--

CREATE SEQUENCE kill_drops_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE kill_drops_id_seq OWNER TO "Kieran";

--
-- Name: kill_drops_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Kieran
--

ALTER SEQUENCE kill_drops_id_seq OWNED BY kill_drops.id;


--
-- Name: locations; Type: TABLE; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE TABLE locations (
    id bigint NOT NULL,
    name character varying,
    slug character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE locations OWNER TO "Kieran";

--
-- Name: locations_id_seq; Type: SEQUENCE; Schema: public; Owner: Kieran
--

CREATE SEQUENCE locations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE locations_id_seq OWNER TO "Kieran";

--
-- Name: locations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Kieran
--

ALTER SEQUENCE locations_id_seq OWNED BY locations.id;


--
-- Name: locations_monsters; Type: TABLE; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE TABLE locations_monsters (
    id bigint NOT NULL,
    location_id bigint,
    monster_id bigint
);


ALTER TABLE locations_monsters OWNER TO "Kieran";

--
-- Name: locations_monsters_id_seq; Type: SEQUENCE; Schema: public; Owner: Kieran
--

CREATE SEQUENCE locations_monsters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE locations_monsters_id_seq OWNER TO "Kieran";

--
-- Name: locations_monsters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Kieran
--

ALTER SEQUENCE locations_monsters_id_seq OWNED BY locations_monsters.id;


--
-- Name: mix_items; Type: TABLE; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE TABLE mix_items (
    id bigint NOT NULL,
    mix_id bigint,
    item_one_id integer,
    item_two_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE mix_items OWNER TO "Kieran";

--
-- Name: mix_items_id_seq; Type: SEQUENCE; Schema: public; Owner: Kieran
--

CREATE SEQUENCE mix_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mix_items_id_seq OWNER TO "Kieran";

--
-- Name: mix_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Kieran
--

ALTER SEQUENCE mix_items_id_seq OWNED BY mix_items.id;


--
-- Name: mixes; Type: TABLE; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE TABLE mixes (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    slug character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE mixes OWNER TO "Kieran";

--
-- Name: mixes_id_seq; Type: SEQUENCE; Schema: public; Owner: Kieran
--

CREATE SEQUENCE mixes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mixes_id_seq OWNER TO "Kieran";

--
-- Name: mixes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Kieran
--

ALTER SEQUENCE mixes_id_seq OWNED BY mixes.id;


--
-- Name: monster_drop_abilities; Type: TABLE; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE TABLE monster_drop_abilities (
    id bigint NOT NULL,
    monster_id bigint,
    ability_id bigint,
    ability_type character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE monster_drop_abilities OWNER TO "Kieran";

--
-- Name: monster_drop_abilities_id_seq; Type: SEQUENCE; Schema: public; Owner: Kieran
--

CREATE SEQUENCE monster_drop_abilities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE monster_drop_abilities_id_seq OWNER TO "Kieran";

--
-- Name: monster_drop_abilities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Kieran
--

ALTER SEQUENCE monster_drop_abilities_id_seq OWNED BY monster_drop_abilities.id;


--
-- Name: monsters; Type: TABLE; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE TABLE monsters (
    id bigint NOT NULL,
    name character varying,
    slug character varying,
    health integer,
    overkill integer,
    strength integer,
    defense integer,
    magic integer,
    magic_defense integer,
    mp integer,
    agility integer,
    luck integer,
    ap integer,
    evasion integer,
    accuracy integer,
    gil integer,
    boss boolean DEFAULT false,
    notes character varying,
    skills character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE monsters OWNER TO "Kieran";

--
-- Name: monsters_id_seq; Type: SEQUENCE; Schema: public; Owner: Kieran
--

CREATE SEQUENCE monsters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE monsters_id_seq OWNER TO "Kieran";

--
-- Name: monsters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Kieran
--

ALTER SEQUENCE monsters_id_seq OWNED BY monsters.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE schema_migrations OWNER TO "Kieran";

--
-- Name: steal_drops; Type: TABLE; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE TABLE steal_drops (
    id bigint NOT NULL,
    monster_id bigint,
    item_id bigint,
    amount integer,
    rare boolean DEFAULT false,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE steal_drops OWNER TO "Kieran";

--
-- Name: steal_drops_id_seq; Type: SEQUENCE; Schema: public; Owner: Kieran
--

CREATE SEQUENCE steal_drops_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE steal_drops_id_seq OWNER TO "Kieran";

--
-- Name: steal_drops_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Kieran
--

ALTER SEQUENCE steal_drops_id_seq OWNED BY steal_drops.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY abilities ALTER COLUMN id SET DEFAULT nextval('abilities_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY bribe_drops ALTER COLUMN id SET DEFAULT nextval('bribe_drops_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY elements ALTER COLUMN id SET DEFAULT nextval('elements_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY items ALTER COLUMN id SET DEFAULT nextval('items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY key_items ALTER COLUMN id SET DEFAULT nextval('key_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY kill_drops ALTER COLUMN id SET DEFAULT nextval('kill_drops_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY locations ALTER COLUMN id SET DEFAULT nextval('locations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY locations_monsters ALTER COLUMN id SET DEFAULT nextval('locations_monsters_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY mix_items ALTER COLUMN id SET DEFAULT nextval('mix_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY mixes ALTER COLUMN id SET DEFAULT nextval('mixes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY monster_drop_abilities ALTER COLUMN id SET DEFAULT nextval('monster_drop_abilities_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY monsters ALTER COLUMN id SET DEFAULT nextval('monsters_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY steal_drops ALTER COLUMN id SET DEFAULT nextval('steal_drops_id_seq'::regclass);


--
-- Data for Name: abilities; Type: TABLE DATA; Schema: public; Owner: Kieran
--

COPY abilities (id, name, ability_type, item_id, item_amount, effect, slug, created_at, updated_at) FROM stdin;
2	Auto-Haste	Armor	17	80	Automatically casts Haste at beginning of battle	auto-haste	2018-04-14 20:55:49.156571	2018-04-14 21:11:59.977906
3	Auto-Med	Armor	79	20	Automatically use status recovery items when afflicted	auto-med	2018-04-14 20:55:49.158998	2018-04-14 21:11:59.982163
4	Auto-Phoenix	Armor	66	20	Automatically use a Phoenix Down when killed	auto-phoenix	2018-04-14 20:55:49.161291	2018-04-14 21:11:59.985183
5	Auto-Potion	Armor	96	4	Automatically use HP recovery items when hit	auto-potion	2018-04-14 20:55:49.163337	2018-04-14 21:11:59.987965
6	Auto-Protect	Armor	48	70	Automatically casts Protect at beginning of battle	auto-protect	2018-04-14 20:55:49.165528	2018-04-14 21:11:59.990617
7	Auto-Reflect	Armor	98	40	Automatically casts Reflect at beginning of battle	auto-reflect	2018-04-14 20:55:49.16759	2018-04-14 21:11:59.993449
8	Auto-Regen	Armor	41	80	Automatically casts Regen at beginning of battle	auto-regen	2018-04-14 20:55:49.169908	2018-04-14 21:11:59.996746
9	Auto-Shell	Armor	52	80	Automatically casts Shell at beginning of battle	auto-shell	2018-04-14 20:55:49.172195	2018-04-14 21:11:59.999346
10	Berserk Ward	Armor	46	8	Sometimes protects against Berserk	berserk-ward	2018-04-14 20:55:49.174387	2018-04-14 21:12:00.003409
11	Berserkproof	Armor	46	32	Protects against Berserk	berserkproof	2018-04-14 20:55:49.176302	2018-04-14 21:12:00.006404
13	Break HP Limit	Armor	110	30	HP can exceed 9999	break-hp-limit	2018-04-14 20:55:49.18028	2018-04-14 21:12:00.012134
14	Break MP Limit	Armor	103	30	MP can exceed 999	break-mp-limit	2018-04-14 20:55:49.182519	2018-04-14 21:12:00.015253
15	Confuse Ward	Armor	70	16	Sometimes protects against Confusion	confuse-ward	2018-04-14 20:55:49.184865	2018-04-14 21:12:00.017785
16	Confuseproof	Armor	70	48	Protects against Confusion	confuseproof	2018-04-14 20:55:49.187067	2018-04-14 21:12:00.020462
17	Counterattack	Weapon	37	1	Automatic attack when characters is hit by a physical attack	counterattack	2018-04-14 20:55:49.189274	2018-04-14 21:12:00.022967
18	Curseproof	Armor	102	12	Protects against Curse	curseproof	2018-04-14 20:55:49.191385	2018-04-14 21:12:00.025743
19	Dark Ward	Armor	30	40	Sometimes protects against Darkness	dark-ward	2018-04-14 20:55:49.19336	2018-04-14 21:12:00.028416
20	Darkproof	Armor	89	10	Protects against Darkness	darkproof	2018-04-14 20:55:49.195352	2018-04-14 21:12:00.031005
21	Darkstrike	Weapon	89	20	Physical attacks cause Darkness	darkstrike	2018-04-14 20:55:49.197498	2018-04-14 21:12:00.033549
22	Darktouch	Weapon	30	60	Physical attacks sometimes cause Darkness	darktouch	2018-04-14 20:55:49.199726	2018-04-14 21:12:00.036091
23	Death Ward	Armor	31	15	Sometimes protects against instant Death	death-ward	2018-04-14 20:55:49.201784	2018-04-14 21:12:00.038618
24	Deathproof	Armor	32	60	Protects against instant Death	deathproof	2018-04-14 20:55:49.204553	2018-04-14 21:12:00.041114
25	Deathstrike	Weapon	32	60	Physical attacks cause Death	deathstrike	2018-04-14 20:55:49.206751	2018-04-14 21:12:00.043789
26	Deathtouch	Weapon	31	30	Physical attacks sometimes cause Death	deathtouch	2018-04-14 20:55:49.209029	2018-04-14 21:12:00.046239
27	Defense +10%	Armor	92	1	Increase Defense by 10%	defense-10	2018-04-14 20:55:49.211277	2018-04-14 21:12:00.048654
28	Defense +20%	Armor	11	4	Increase Defense by 20%	defense-20	2018-04-14 20:55:49.213382	2018-04-14 21:12:00.051237
29	Defense +3%	Armor	77	3	Increase Defense by 3%	defense-3	2018-04-14 20:55:49.215567	2018-04-14 21:12:00.053943
30	Defense +5%	Armor	95	2	Increase Defense by 5%	defense-5	2018-04-14 20:55:49.217834	2018-04-14 21:12:00.056633
32	Double Overdrive	Weapon	106	30	Overdrive Gauge charges twice as fast	double-overdrive	2018-04-14 20:55:49.221945	2018-04-14 21:12:00.062804
33	Evade & Counter	Weapon	101	1	Automatic attack when character dodges a physical attack	evade-counter	2018-04-14 20:55:49.223944	2018-04-14 21:12:00.065799
34	Fire Eater	Armor	33	20	Fire attacks restore HP instead of doing damage	fire-eater	2018-04-14 20:55:49.225881	2018-04-14 21:12:00.068418
35	Fire Ward	Armor	14	4	Take half damage from Fire attacks	fire-ward	2018-04-14 20:55:49.228137	2018-04-14 21:12:00.070938
36	Fireproof	Armor	13	8	Take no damage from Fire attacks	fireproof	2018-04-14 20:55:49.2303	2018-04-14 21:12:00.073506
37	Firestrike	Weapon	14	4	Physical attacks are Fire elemental	firestrike	2018-04-14 20:55:49.232477	2018-04-14 21:12:00.076095
38	First Strike	Weapon	81	1	Your party gets the first turn during battle	first-strike	2018-04-14 20:55:49.234455	2018-04-14 21:12:00.078555
39	Gillionaire	Weapon	21	30	Doubles amount of gil gained after battle	gillionaire	2018-04-14 20:55:49.236829	2018-04-14 21:12:00.08117
40	Half MP Cost	Weapon	105	20	Reduce Ability MP costs by half	half-mp-cost	2018-04-14 20:55:49.238881	2018-04-14 21:12:00.083887
41	HP +10%	Armor	91	3	Increase max HP by 10%	hp-10	2018-04-14 20:55:49.241047	2018-04-14 21:12:00.086273
42	HP +20%	Armor	27	5	Increase max HP by 20%	hp-20	2018-04-14 20:55:49.243172	2018-04-14 21:12:00.088766
43	HP +30%	Armor	97	1	Increase max HP by 30%	hp-30	2018-04-14 20:55:49.245311	2018-04-14 21:12:00.091198
44	HP +5%	Armor	112	1	Increase max HP by 5%	hp-5	2018-04-14 20:55:49.247368	2018-04-14 21:12:00.09364
45	HP Stroll	Armor	96	2	Recover HP while walking	hp-stroll	2018-04-14 20:55:49.249363	2018-04-14 21:12:00.096112
46	Ice Eater	Armor	47	20	Ice attacks restore HP instead of doing damage	ice-eater	2018-04-14 20:55:49.251577	2018-04-14 21:12:00.098596
47	Ice Ward	Armor	7	4	Take half damage from Ice attacks	ice-ward	2018-04-14 20:55:49.253685	2018-04-14 21:12:00.100982
48	Iceproof	Armor	9	8	Take no damage from Ice attacks	iceproof	2018-04-14 20:55:49.25601	2018-04-14 21:12:00.103576
49	Icestrike	Weapon	7	4	Physical attacks are Ice elemental	icestrike	2018-04-14 20:55:49.258939	2018-04-14 21:12:00.106539
50	Initiative	Weapon	16	6	Increase change of getting a Preemptive strike	initiative	2018-04-14 20:55:49.262744	2018-04-14 21:12:00.111152
51	Lightning Eater	Armor	49	20	Thunder attacks restore HP instead of doing damage	lightning-eater	2018-04-14 20:55:49.264996	2018-04-14 21:12:00.113711
52	Lightning Ward	Armor	26	4	Take half damage from Thunder attacks	lightning-ward	2018-04-14 20:55:49.267586	2018-04-14 21:12:00.116089
53	Lightningproof	Armor	50	8	Take no damage from Thunder attacks	lightningproof	2018-04-14 20:55:49.269771	2018-04-14 21:12:00.118719
54	Lightningstrike	Weapon	26	4	Physical attacks are Thunder elemental	lightningstrike	2018-04-14 20:55:49.275252	2018-04-14 21:12:00.121546
55	Magic +10%	Weapon	12	1	Increase Magic by 10%	magic-10	2018-04-14 20:55:49.277346	2018-04-14 21:12:00.124187
56	Magic +20%	Weapon	100	4	Increase Magic by 20%	magic-20	2018-04-14 20:55:49.279273	2018-04-14 21:12:00.127629
57	Magic +3%	Weapon	60	3	Increase Magic by 3%	magic-3	2018-04-14 20:55:49.281328	2018-04-14 21:12:00.131178
58	Magic +5%	Weapon	61	2	Increase Magic by 5%	magic-5	2018-04-14 20:55:49.283606	2018-04-14 21:12:00.133891
60	Magic Counter	Weapon	83	16	Automatic attack when character is hit by a magical attack	magic-counter	2018-04-14 20:55:49.28798	2018-04-14 21:12:00.139006
122	Aim	Special	\N	\N	Raise Accuracy of all allies	aim	2018-04-14 20:55:49.430082	2018-04-14 20:55:49.430082
62	Magic Def +20%	Armor	11	4	Increase Magic Def by 20%	magic-def-20	2018-04-14 20:55:49.292495	2018-04-14 21:12:00.144233
63	Magic Def +3%	Armor	60	3	Increase Magic Def by 3%	magic-def-3	2018-04-14 20:55:49.294624	2018-04-14 21:12:00.146858
64	Magic Def +5%	Armor	61	2	Increase Magic Def by 5%	magic-def-5	2018-04-14 20:55:49.296607	2018-04-14 21:12:00.149444
65	Master Thief	Armor	71	30	Always steal rare items	master-thief	2018-04-14 20:55:49.298639	2018-04-14 21:12:00.151873
66	MP +10%	Armor	91	3	Increase max MP by 10%	mp-10	2018-04-14 20:55:49.300831	2018-04-14 21:12:00.154378
67	MP +20%	Armor	27	5	Increase max MP by 20%	mp-20	2018-04-14 20:55:49.302893	2018-04-14 21:12:00.15694
68	MP +30%	Armor	63	1	Increase max MP by 30%	mp-30	2018-04-14 20:55:49.305021	2018-04-14 21:12:00.159574
69	MP +5%	Armor	28	1	Increase max MP by 5%	mp-5	2018-04-14 20:55:49.307047	2018-04-14 21:12:00.161931
70	MP Stroll	Armor	62	2	Recover MP while walking	mp-stroll	2018-04-14 20:55:49.30919	2018-04-14 21:12:00.164456
71	No Encounters	Armor	78	30	Party will not encounter random battles	no-encounters	2018-04-14 20:55:49.311279	2018-04-14 21:12:00.167043
72	One MP Cost	Weapon	103	20	Reduce Ability MP costs to one	one-mp-cost	2018-04-14 20:55:49.313485	2018-04-14 21:12:00.169456
74	Pickpocket	Armor	6	30	Increase chance to steal rare items	pickpocket	2018-04-14 20:55:49.318857	2018-04-14 21:12:00.174627
75	Piercing	Weapon	54	1	Physical attacks bypass enemy armor doing normal damage	piercing	2018-04-14 20:55:49.321575	2018-04-14 21:12:00.177103
76	Poison Ward	Armor	8	40	Sometimes protects against Poison	poison-ward	2018-04-14 20:55:49.324911	2018-04-14 21:12:00.179613
77	Poisonproof	Armor	74	12	Protects against Poison	poisonproof	2018-04-14 20:55:49.328496	2018-04-14 21:12:00.182065
78	Poisonstrike	Weapon	74	24	Physical attacks cause Poison	poisonstrike	2018-04-14 20:55:49.331809	2018-04-14 21:12:00.184465
79	Poisontouch	Weapon	8	99	Physical attacks sometimes cause Poison	poisontouch	2018-04-14 20:55:49.334636	2018-04-14 21:12:00.187209
80	Sensor	Weapon	2	2	View the data of enemies during battle	sensor	2018-04-14 20:55:49.336962	2018-04-14 21:12:00.190119
81	Silence Ward	Armor	25	30	Sometimes protects against Silence	silence-ward	2018-04-14 20:55:49.33966	2018-04-14 21:12:00.19271
82	Silenceproof	Armor	85	10	Protects against Silence	silenceproof	2018-04-14 20:55:49.341788	2018-04-14 21:12:00.196262
83	Silencestrike	Weapon	85	20	Physical attacks cause Silence	silencestrike	2018-04-14 20:55:49.343922	2018-04-14 21:12:00.1988
84	Silencetouch	Weapon	25	60	Physical attacks sometimes cause Silence	silencetouch	2018-04-14 20:55:49.345884	2018-04-14 21:12:00.201301
85	Sleep Ward	Armor	88	6	Sometimes protects against Sleep	sleep-ward	2018-04-14 20:55:49.348146	2018-04-14 21:12:00.204027
86	Sleepproof	Armor	24	8	Protects against Sleep	sleepproof	2018-04-14 20:55:49.350499	2018-04-14 21:12:00.207041
87	Sleepstrike	Weapon	24	16	Physical attacks cause Sleep	sleepstrike	2018-04-14 20:55:49.352585	2018-04-14 21:12:00.210516
88	Sleeptouch	Weapon	88	10	Physical attacks sometimes cause Sleep	sleeptouch	2018-04-14 20:55:49.354682	2018-04-14 21:12:00.213662
89	Slow Ward	Armor	86	10	Sometimes protects against Slow	slow-ward	2018-04-14 20:55:49.356901	2018-04-14 21:12:00.216665
90	Slowproof	Armor	39	20	Protects against Slow	slowproof	2018-04-14 20:55:49.359108	2018-04-14 21:12:00.2194
91	Slowstrike	Weapon	39	30	Physical attacks cause Slow	slowstrike	2018-04-14 20:55:49.361145	2018-04-14 21:12:00.222169
92	Slowtouch	Weapon	86	16	Physical attacks sometimes cause Slow	slowtouch	2018-04-14 20:55:49.363266	2018-04-14 21:12:00.225082
93	SOS Haste	Armor	16	20	Casts Haste when HP is low	sos-haste	2018-04-14 20:55:49.365616	2018-04-14 21:12:00.227638
94	SOS NulBlaze	Armor	13	1	Casts NulBlaze when HP is low	sos-nulblaze	2018-04-14 20:55:49.367874	2018-04-14 21:12:00.230133
96	SOS NulShock	Armor	50	1	Casts NulShock when HP is low	sos-nulshock	2018-04-14 20:55:49.37241	2018-04-14 21:12:00.235141
97	SOS NulTide	Armor	23	1	Casts NulTide when HP is low	sos-nultide	2018-04-14 20:55:49.37469	2018-04-14 21:12:00.238512
98	SOS Overdrive	Weapon	38	20	Overdrive Gauge charges twice as fast when HP is low	sos-overdrive	2018-04-14 20:55:49.376756	2018-04-14 21:12:00.241453
99	SOS Protect	Armor	48	8	Casts Protect when HP is low	sos-protect	2018-04-14 20:55:49.378844	2018-04-14 21:12:00.24416
100	SOS Reflect	Armor	98	8	Casts Reflect when HP is low	sos-reflect	2018-04-14 20:55:49.380986	2018-04-14 21:12:00.246668
101	SOS Regen	Armor	41	12	Casts Regen when HP is low	sos-regen	2018-04-14 20:55:49.382976	2018-04-14 21:12:00.249303
102	SOS Shell	Armor	52	8	Casts Shell when HP is low	sos-shell	2018-04-14 20:55:49.385181	2018-04-14 21:12:00.251791
103	Stone Ward	Armor	90	30	Sometimes protects against Stone	stone-ward	2018-04-14 20:55:49.387237	2018-04-14 21:12:00.254492
104	Stoneproof	Armor	72	20	Protects against Stone	stoneproof	2018-04-14 20:55:49.389299	2018-04-14 21:12:00.257214
105	Stonestrike	Weapon	72	60	Physical attacks cause Stone	stonestrike	2018-04-14 20:55:49.391338	2018-04-14 21:12:00.259779
106	Stonetouch	Weapon	72	10	Physical attacks sometimes cause Stone	stonetouch	2018-04-14 20:55:49.393465	2018-04-14 21:12:00.263213
107	Strength +10%	Weapon	87	1	Increase Strength by 10%	strength-10	2018-04-14 20:55:49.395487	2018-04-14 21:12:00.266034
108	Strength +20%	Weapon	100	4	Increase Strength by 20%	strength-20	2018-04-14 20:55:49.397534	2018-04-14 21:12:00.269076
109	Strength +3%	Weapon	77	3	Increase Strength by 3%	strength-3	2018-04-14 20:55:49.399511	2018-04-14 21:12:00.273284
110	Strength +5%	Weapon	95	2	Increase Strength by 5%	strength-5	2018-04-14 20:55:49.401566	2018-04-14 21:12:00.275862
112	Triple Overdrive	Weapon	111	30	Overdrive Gauge charges three times as fast	triple-overdrive	2018-04-14 20:55:49.406705	2018-04-14 21:12:00.281595
113	Water Eater	Armor	108	20	Water attacks restore HP instead of doing damage	water-eater	2018-04-14 20:55:49.408999	2018-04-14 21:12:00.284321
114	Water Ward	Armor	34	4	Take half damage from Water attacks	water-ward	2018-04-14 20:55:49.411031	2018-04-14 21:12:00.287204
115	Waterproof	Armor	23	8	Take no damage from Water attacks	waterproof	2018-04-14 20:55:49.413046	2018-04-14 21:12:00.289864
116	Waterstrike	Weapon	34	4	Physical attacks are Water elemental	waterstrike	2018-04-14 20:55:49.415011	2018-04-14 21:12:00.292373
117	Zombie Ward	Armor	44	30	Sometimes protects against Zombie	zombie-ward	2018-04-14 20:55:49.417141	2018-04-14 21:12:00.294874
118	Zombieproof	Armor	15	10	Protects against Zombie	zombieproof	2018-04-14 20:55:49.419282	2018-04-14 21:12:00.29739
119	Zombiestrike	Weapon	15	30	Physical attacks cause Zombie	zombiestrike	2018-04-14 20:55:49.421985	2018-04-14 21:12:00.299805
120	Zombietouch	Weapon	44	70	Physical attacks sometimes cause Zombie	zombietouch	2018-04-14 20:55:49.425649	2018-04-14 21:12:00.302393
121	Ribbon	Armor	19	99	Protects against most status ailments	ribbon	2018-04-14 20:55:49.427902	2018-04-14 21:12:00.304938
123	Armor Break	Skill	\N	\N	Lower Defense while hitting an enemy	armor-break	2018-04-14 20:55:49.43216	2018-04-14 20:55:49.43216
124	Auto-Life	White Magic	\N	\N	Target automatically revives after being killed	auto-life	2018-04-14 20:55:49.434343	2018-04-14 20:55:49.434343
125	Bio	Black Magic	\N	\N	Inflicts Poison on the target	bio	2018-04-14 20:55:49.436227	2018-04-14 20:55:49.436227
126	Blizzaga	Black Magic	\N	\N	Deals large amount of Ice damage to target	blizzaga	2018-04-14 20:55:49.438429	2018-04-14 20:55:49.438429
127	Blizzara	Black Magic	\N	\N	Deals medium amount of Ice damage to target	blizzara	2018-04-14 20:55:49.440563	2018-04-14 20:55:49.440563
128	Blizzard	Black Magic	\N	\N	Deals small amount of Ice damage to target	blizzard	2018-04-14 20:55:49.442685	2018-04-14 20:55:49.442685
129	Bribe	Special	\N	\N	Pay enemies gil for safe passage and to receive items	bribe	2018-04-14 20:55:49.444672	2018-04-14 20:55:49.444672
130	Cheer	Special	\N	\N	Raise Strength and Defense of all allies	cheer	2018-04-14 20:55:49.446686	2018-04-14 20:55:49.446686
131	CopyCat	Special	\N	\N	Mimic the last action taken by an ally except overdrives and summons	copycat	2018-04-14 20:55:49.448817	2018-04-14 20:55:49.448817
132	Cura	White Magic	\N	\N	Restore medium amount of HP	cura	2018-04-14 20:55:49.450801	2018-04-14 20:55:49.450801
133	Curaga	White Magic	\N	\N	Restore large amount of HP	curaga	2018-04-14 20:55:49.452889	2018-04-14 20:55:49.452889
134	Cure	White Magic	\N	\N	Restore small amount of HP	cure	2018-04-14 20:55:49.455003	2018-04-14 20:55:49.455003
135	Dark Attack	Skill	\N	\N	Sometimes inflict Darkness for 3 turns while hitting an enemy	dark-attack	2018-04-14 20:55:49.457154	2018-04-14 20:55:49.457154
136	Dark Buster	Skill	\N	\N	Always inflict Darkness for 1 turn (unless immune) while hitting an enemy	dark-buster	2018-04-14 20:55:49.459347	2018-04-14 20:55:49.459347
137	Death	Black Magic	\N	\N	Instantly kills target	death	2018-04-14 20:55:49.461455	2018-04-14 20:55:49.461455
138	Delay Attack	Skill	\N	\N	Hit an enemy and delay its turn	delay-attack	2018-04-14 20:55:49.463399	2018-04-14 20:55:49.463399
139	Delay Buster	Skill	\N	\N	Hit an enemy and greatly delay its turn	delay-buster	2018-04-14 20:55:49.465404	2018-04-14 20:55:49.465404
140	Demi	Black Magic	\N	\N	Reduces target's HP by 1/4	demi	2018-04-14 20:55:49.467351	2018-04-14 20:55:49.467351
141	Dispel	White Magic	\N	\N	Remove any spells cast on the target	dispel	2018-04-14 20:55:49.46932	2018-04-14 20:55:49.46932
142	DoubleCast	Special	\N	\N	Cast 2 Black Magic spells in a single turn (must pay casting cost for both)	doublecast	2018-04-14 20:55:49.471314	2018-04-14 20:55:49.471314
143	Drain	Black Magic	\N	\N	Takes target's HP and gives it to the caster	drain	2018-04-14 20:55:49.473441	2018-04-14 20:55:49.473441
144	Entrust	Special	\N	\N	Transfer overdrive gauge to another ally (adding onto it)	entrust	2018-04-14 20:55:49.475499	2018-04-14 20:55:49.475499
145	Esuna	White Magic	\N	\N	Cure Darkness, Poison, Silence, Sleep, Confusion, Berserk, Slow, and Stone	esuna	2018-04-14 20:55:49.478558	2018-04-14 20:55:49.478558
146	Fira	Black Magic	\N	\N	Deals medium amount of Fire damage to target	fira	2018-04-14 20:55:49.482305	2018-04-14 20:55:49.482305
147	Firaga	Black Magic	\N	\N	Deals large amount of Fire damage to target	firaga	2018-04-14 20:55:49.485184	2018-04-14 20:55:49.485184
148	Fire	Black Magic	\N	\N	Deals small amount of Fire damage to target	fire	2018-04-14 20:55:49.487118	2018-04-14 20:55:49.487118
149	Flare	Black Magic	\N	\N	Deals huge amount of damage to target	flare	2018-04-14 20:55:49.489397	2018-04-14 20:55:49.489397
150	Flee	Special	\N	\N	Escape from battle	flee	2018-04-14 20:55:49.497489	2018-04-14 20:55:49.497489
151	Focus	Special	\N	\N	Raise Magic and Magic Defense of all allies	focus	2018-04-14 20:55:49.500437	2018-04-14 20:55:49.500437
152	Full-Life	White Magic	\N	\N	Revive a dead ally with full HP	full-life	2018-04-14 20:55:49.502289	2018-04-14 20:55:49.502289
153	Guard	Special	\N	\N	Take physical attack instead of other ally	guard	2018-04-14 20:55:49.504223	2018-04-14 20:55:49.504223
154	Haste	White Magic	\N	\N	Increase speed of one ally	haste	2018-04-14 20:55:49.507027	2018-04-14 20:55:49.507027
155	Hastega	White Magic	\N	\N	Increase speed of all allies	hastega	2018-04-14 20:55:49.510226	2018-04-14 20:55:49.510226
156	Holy	White Magic	\N	\N	Deals Holy damage to an enemy	holy	2018-04-14 20:55:49.512378	2018-04-14 20:55:49.512378
157	Jinx	Special	\N	\N	Lower Luck of all enemies	jinx	2018-04-14 20:55:49.514359	2018-04-14 20:55:49.514359
158	Lancet	Special	\N	\N	Drains HP/MP from an enemy (when used by Kimahri, may learn enemy skill)	lancet	2018-04-14 20:55:49.516347	2018-04-14 20:55:49.516347
159	Life	White Magic	\N	\N	Revive a dead ally	life	2018-04-14 20:55:49.518296	2018-04-14 20:55:49.518296
160	Luck	Special	\N	\N	Raise Luck of all allies	luck	2018-04-14 20:55:49.520356	2018-04-14 20:55:49.520356
161	Magic Break	Skill	\N	\N	Lower Magic while hitting an enemy	magic-break	2018-04-14 20:55:49.52261	2018-04-14 20:55:49.52261
162	Mental Break	Skill	\N	\N	Lower Magic Defense while hitting an enemy	mental-break	2018-04-14 20:55:49.524708	2018-04-14 20:55:49.524708
163	Mug	Skill	\N	\N	Steal items while hitting an enemy	mug	2018-04-14 20:55:49.527551	2018-04-14 20:55:49.527551
164	NulBlaze	White Magic	\N	\N	Grant immunity from one Fire attack to all allies	nulblaze	2018-04-14 20:55:49.530834	2018-04-14 20:55:49.530834
165	NulFrost	White Magic	\N	\N	Grant immunity from one Ice attack to all alllies	nulfrost	2018-04-14 20:55:49.532974	2018-04-14 20:55:49.532974
166	NulShock	White Magic	\N	\N	Grant immunity from one Thunder attack to all allies	nulshock	2018-04-14 20:55:49.535122	2018-04-14 20:55:49.535122
167	NulTide	White Magic	\N	\N	Grant immunity from one Water attack to all allies	nultide	2018-04-14 20:55:49.537287	2018-04-14 20:55:49.537287
168	Osmose	Black Magic	\N	\N	Takes target's MP and gives it to the caster	osmose	2018-04-14 20:55:49.539278	2018-04-14 20:55:49.539278
169	Power Break	Skill	\N	\N	Lower Strength while hitting an enemy	power-break	2018-04-14 20:55:49.541452	2018-04-14 20:55:49.541452
170	Pray	Special	\N	\N	Restores small amount of HP to allies	pray	2018-04-14 20:55:49.543579	2018-04-14 20:55:49.543579
171	Protect	White Magic	\N	\N	Increase an ally's Defense	protect	2018-04-14 20:55:49.545566	2018-04-14 20:55:49.545566
172	Provoke	Special	\N	\N	Draw an enemy's attack towards yourself	provoke	2018-04-14 20:55:49.547756	2018-04-14 20:55:49.547756
173	Quick Hit	Skill	\N	\N	Attack an enemy with reduced recovery time	quick-hit	2018-04-14 20:55:49.549818	2018-04-14 20:55:49.549818
174	Reflect	White Magic	\N	\N	Spells cast on the target are reflected back at the caster	reflect	2018-04-14 20:55:49.552125	2018-04-14 20:55:49.552125
175	Reflex	Special	\N	\N	Raise Evasion of all allies	reflex	2018-04-14 20:55:49.554229	2018-04-14 20:55:49.554229
176	Regen	White Magic	\N	\N	Target's HP increases by a small amount after each turn	regen	2018-04-14 20:55:49.556392	2018-04-14 20:55:49.556392
177	Scan	White Magic	\N	\N	View an enemy's HP, strengths and weaknesses	scan	2018-04-14 20:55:49.558592	2018-04-14 20:55:49.558592
178	Sentinel	Special	\N	\N	Take physical attack instead of other ally while in a defensive posture	sentinel	2018-04-14 20:55:49.560756	2018-04-14 20:55:49.560756
179	Shell	White Magic	\N	\N	Increase an ally's Magic Defense	shell	2018-04-14 20:55:49.562687	2018-04-14 20:55:49.562687
180	Silence Attack	Skill	\N	\N	Sometimes inflict Silence for 3 turns while hitting an enemy	silence-attack	2018-04-14 20:55:49.564849	2018-04-14 20:55:49.564849
181	Silence Buster	Skill	\N	\N	Always inflict Silence for 1 turn (unless immune) while hitting an enemy	silence-buster	2018-04-14 20:55:49.566906	2018-04-14 20:55:49.566906
182	Sleep Attack	Skill	\N	\N	Sometimes inflict Sleep for 3 turns while hitting an enemy	sleep-attack	2018-04-14 20:55:49.568874	2018-04-14 20:55:49.568874
183	Sleep Buster	Skill	\N	\N	Always inflict Sleep for 1 turn (unless immune) while hitting an enemy	sleep-buster	2018-04-14 20:55:49.57079	2018-04-14 20:55:49.57079
184	Slow	White Magic	\N	\N	Reduce speed of an enemy	slow	2018-04-14 20:55:49.572933	2018-04-14 20:55:49.572933
185	Slowga	White Magic	\N	\N	Reduce speed of all enemies	slowga	2018-04-14 20:55:49.575134	2018-04-14 20:55:49.575134
186	Spare Change	Special	\N	\N	Throw gil at enemy to do damage (damage is 1/10th amount of gil thrown)	spare-change	2018-04-14 20:55:49.577239	2018-04-14 20:55:49.577239
187	Steal	Special	\N	\N	Steal items from an enemy	steal	2018-04-14 20:55:49.579192	2018-04-14 20:55:49.579192
188	Threaten	Special	\N	\N	Immobilize an enemy with fear	threaten	2018-04-14 20:55:49.581285	2018-04-14 20:55:49.581285
189	Thundaga	Black Magic	\N	\N	Deals large amount of Thunder damage to target	thundaga	2018-04-14 20:55:49.583387	2018-04-14 20:55:49.583387
190	Thundara	Black Magic	\N	\N	Deals medium amount of Thunder damage to target	thundara	2018-04-14 20:55:49.585371	2018-04-14 20:55:49.585371
191	Thunder	Black Magic	\N	\N	Deals small amount of Thunder damage to target	thunder	2018-04-14 20:55:49.587331	2018-04-14 20:55:49.587331
192	Triple Foul	Skill	\N	\N	Sometimes inflict Darkness, Silence and Sleep for 3 turns while hitting an enemy	triple-foul	2018-04-14 20:55:49.589326	2018-04-14 20:55:49.589326
193	Ultima	Black Magic	\N	\N	Deals huge amount of damage to all enemies	ultima	2018-04-14 20:55:49.591533	2018-04-14 20:55:49.591533
194	Use	Special	\N	\N	Use any item in stock during battle	use	2018-04-14 20:55:49.59378	2018-04-14 20:55:49.59378
195	Water	Black Magic	\N	\N	Deals small amount of Water damage to target	water	2018-04-14 20:55:49.59579	2018-04-14 20:55:49.59579
196	Watera	Black Magic	\N	\N	Deals medium amount of Water damage to target	watera	2018-04-14 20:55:49.598002	2018-04-14 20:55:49.598002
197	Waterga	Black Magic	\N	\N	Deals large amount of Water damage to target	waterga	2018-04-14 20:55:49.5999	2018-04-14 20:55:49.5999
198	Zombie Attack	Skill	\N	\N	Sometimes inflict Zombie while hitting an enemy	zombie-attack	2018-04-14 20:55:49.601997	2018-04-14 20:55:49.601997
1	Alchemy	Weapon	42	4	Double effect of recovery items used by the character	alchemy	2018-04-14 20:55:49.152613	2018-04-14 21:11:59.954257
12	Break Damage Limit	Weapon	19	60	Allows attacks to do more than 9999 damage	break-damage-limit	2018-04-14 20:55:49.178332	2018-04-14 21:12:00.009305
31	Double AP	Weapon	68	20	AP is doubled after battle (only if character is in active party at end of battle)	double-ap	2018-04-14 20:55:49.219915	2018-04-14 21:12:00.059502
59	Magic Booster	Weapon	104	30	Increase Magic power for the price of increased MP cost	magic-booster	2018-04-14 20:55:49.285749	2018-04-14 21:12:00.136467
61	Magic Def +10%	Armor	109	1	Increase Magic Def by 10%	magic-def-10	2018-04-14 20:55:49.290126	2018-04-14 21:12:00.141562
73	Overdrive -> AP	Weapon	22	10	Earn more AP but Overdrive Gauge doesn't charge	overdrive-ap	2018-04-14 20:55:49.315562	2018-04-14 21:12:00.172089
95	SOS NulFrost	Armor	9	1	Casts NulFrost when HP is low	sos-nulfrost	2018-04-14 20:55:49.370063	2018-04-14 21:12:00.232683
111	Triple AP	Weapon	110	50	AP is tripled after battle (only if character is in active party at end of battle)	triple-ap	2018-04-14 20:55:49.403759	2018-04-14 21:12:00.278385
\.


--
-- Name: abilities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Kieran
--

SELECT pg_catalog.setval('abilities_id_seq', 198, true);


--
-- Data for Name: ar_internal_metadata; Type: TABLE DATA; Schema: public; Owner: Kieran
--

COPY ar_internal_metadata (key, value, created_at, updated_at) FROM stdin;
environment	development	2018-04-14 20:55:47.731143	2018-04-14 20:55:47.731143
\.


--
-- Data for Name: bribe_drops; Type: TABLE DATA; Schema: public; Owner: Kieran
--

COPY bribe_drops (id, monster_id, item_id, amount, cost, created_at, updated_at) FROM stdin;
1	3	41	16	102000	2018-04-14 20:55:49.630351	2018-04-14 20:55:49.630351
2	4	92	6	1088000	2018-04-14 20:55:49.641231	2018-04-14 20:55:49.641231
3	5	50	4	4000	2018-04-14 20:55:49.651849	2018-04-14 20:55:49.651849
4	6	32	6	56000	2018-04-14 20:55:49.657338	2018-04-14 20:55:49.657338
5	7	66	2	8600	2018-04-14 20:55:49.663035	2018-04-14 20:55:49.663035
6	8	42	16	116000	2018-04-14 20:55:49.667069	2018-04-14 20:55:49.667069
7	10	108	15	40500	2018-04-14 20:55:49.671051	2018-04-14 20:55:49.671051
8	11	24	20	36000	2018-04-14 20:55:49.6751	2018-04-14 20:55:49.6751
9	12	101	20	1900000	2018-04-14 20:55:49.679216	2018-04-14 20:55:49.679216
10	13	96	80	340000	2018-04-14 20:55:49.683229	2018-04-14 20:55:49.683229
11	14	72	24	40500	2018-04-14 20:55:49.687133	2018-04-14 20:55:49.687133
12	15	85	12	7600	2018-04-14 20:55:49.692071	2018-04-14 20:55:49.692071
13	16	54	30	460000	2018-04-14 20:55:49.697573	2018-04-14 20:55:49.697573
14	17	103	14	1350000	2018-04-14 20:55:49.701439	2018-04-14 20:55:49.701439
15	19	74	2	4000	2018-04-14 20:55:49.709249	2018-04-14 20:55:49.709249
16	20	12	2	152000	2018-04-14 20:55:49.713583	2018-04-14 20:55:49.713583
17	21	108	9	30000	2018-04-14 20:55:49.717475	2018-04-14 20:55:49.717475
18	22	13	16	17000	2018-04-14 20:55:49.72133	2018-04-14 20:55:49.72133
19	25	70	2	4600	2018-04-14 20:55:49.727863	2018-04-14 20:55:49.727863
20	26	46	16	8000	2018-04-14 20:55:49.732839	2018-04-14 20:55:49.732839
21	31	72	6	11000	2018-04-14 20:55:49.736739	2018-04-14 20:55:49.736739
22	32	62	10	105000	2018-04-14 20:55:49.741297	2018-04-14 20:55:49.741297
23	33	56	2	196000	2018-04-14 20:55:49.749173	2018-04-14 20:55:49.749173
24	36	37	2	120000	2018-04-14 20:55:49.753178	2018-04-14 20:55:49.753178
25	38	89	3	1900	2018-04-14 20:55:49.757219	2018-04-14 20:55:49.757219
26	40	81	3	36000	2018-04-14 20:55:49.761181	2018-04-14 20:55:49.761181
27	41	109	2	256000	2018-04-14 20:55:49.765051	2018-04-14 20:55:49.765051
28	42	96	20	24000	2018-04-14 20:55:49.768974	2018-04-14 20:55:49.768974
29	44	21	5	846000	2018-04-14 20:55:49.773041	2018-04-14 20:55:49.773041
30	45	55	40	900000	2018-04-14 20:55:49.777153	2018-04-14 20:55:49.777153
31	46	88	4	2500	2018-04-14 20:55:49.781012	2018-04-14 20:55:49.781012
32	47	72	2	2800	2018-04-14 20:55:49.785017	2018-04-14 20:55:49.785017
33	49	43	60	37500	2018-04-14 20:55:49.788952	2018-04-14 20:55:49.788952
34	51	32	25	174000	2018-04-14 20:55:49.792633	2018-04-14 20:55:49.792633
35	53	70	3	6200	2018-04-14 20:55:49.796398	2018-04-14 20:55:49.796398
36	56	104	1	148000	2018-04-14 20:55:49.80026	2018-04-14 20:55:49.80026
37	62	33	10	30000	2018-04-14 20:55:49.803987	2018-04-14 20:55:49.803987
38	63	38	10	134000	2018-04-14 20:55:49.807945	2018-04-14 20:55:49.807945
39	64	70	1	2800	2018-04-14 20:55:49.811781	2018-04-14 20:55:49.811781
40	65	104	2	10800	2018-04-14 20:55:49.815718	2018-04-14 20:55:49.815718
41	66	50	3	2960	2018-04-14 20:55:49.820361	2018-04-14 20:55:49.820361
42	67	88	7	4800	2018-04-14 20:55:49.824375	2018-04-14 20:55:49.824375
43	70	97	10	720000	2018-04-14 20:55:49.828353	2018-04-14 20:55:49.828353
44	71	97	10	720000	2018-04-14 20:55:49.832119	2018-04-14 20:55:49.832119
45	74	66	38	199980	2018-04-14 20:55:49.835887	2018-04-14 20:55:49.835887
46	75	50	20	24000	2018-04-14 20:55:49.839993	2018-04-14 20:55:49.839993
47	76	79	40	80000	2018-04-14 20:55:49.844018	2018-04-14 20:55:49.844018
48	77	110	8	1280000	2018-04-14 20:55:49.848344	2018-04-14 20:55:49.848344
49	79	83	12	150000	2018-04-14 20:55:49.852311	2018-04-14 20:55:49.852311
50	80	67	60	190000	2018-04-14 20:55:49.855921	2018-04-14 20:55:49.855921
51	83	28	10	40000	2018-04-14 20:55:49.860002	2018-04-14 20:55:49.860002
52	84	112	30	52000	2018-04-14 20:55:49.864147	2018-04-14 20:55:49.864147
53	85	100	20	260000	2018-04-14 20:55:49.867897	2018-04-14 20:55:49.867897
54	88	47	9	27000	2018-04-14 20:55:49.872114	2018-04-14 20:55:49.872114
55	89	72	5	7400	2018-04-14 20:55:49.875831	2018-04-14 20:55:49.875831
56	90	53	4	17600	2018-04-14 20:55:49.87971	2018-04-14 20:55:49.87971
57	91	72	3	3600	2018-04-14 20:55:49.883739	2018-04-14 20:55:49.883739
58	92	97	1	72000	2018-04-14 20:55:49.887888	2018-04-14 20:55:49.887888
59	97	74	1	2200	2018-04-14 20:55:49.895141	2018-04-14 20:55:49.895141
60	101	86	20	8900	2018-04-14 20:55:49.8993	2018-04-14 20:55:49.8993
61	102	86	10	5500	2018-04-14 20:55:49.903077	2018-04-14 20:55:49.903077
62	103	19	2	1600000	2018-04-14 20:55:49.906962	2018-04-14 20:55:49.906962
63	104	84	10	29960	2018-04-14 20:55:49.910919	2018-04-14 20:55:49.910919
64	106	79	80	92980	2018-04-14 20:55:49.914839	2018-04-14 20:55:49.914839
65	107	17	60	360000	2018-04-14 20:55:49.918806	2018-04-14 20:55:49.918806
66	108	10	1	200000	2018-04-14 20:55:49.922736	2018-04-14 20:55:49.922736
67	109	46	28	14200	2018-04-14 20:55:49.926684	2018-04-14 20:55:49.926684
68	111	110	4	540000	2018-04-14 20:55:49.930635	2018-04-14 20:55:49.930635
69	113	81	24	620000	2018-04-14 20:55:49.934309	2018-04-14 20:55:49.934309
70	114	107	1	260000	2018-04-14 20:55:49.938429	2018-04-14 20:55:49.938429
71	115	71	3	960000	2018-04-14 20:55:49.942443	2018-04-14 20:55:49.942443
72	116	108	14	44440	2018-04-14 20:55:49.946414	2018-04-14 20:55:49.946414
73	117	5	99	174000	2018-04-14 20:55:49.950331	2018-04-14 20:55:49.950331
74	118	40	50	25600	2018-04-14 20:55:49.954197	2018-04-14 20:55:49.954197
75	119	5	40	56000	2018-04-14 20:55:49.958047	2018-04-14 20:55:49.958047
76	120	5	60	110000	2018-04-14 20:55:49.962944	2018-04-14 20:55:49.962944
77	121	22	2	74000	2018-04-14 20:55:49.973765	2018-04-14 20:55:49.973765
78	122	22	1	55000	2018-04-14 20:55:49.984599	2018-04-14 20:55:49.984599
79	123	72	4	5300	2018-04-14 20:55:49.995186	2018-04-14 20:55:49.995186
80	124	88	5	3200	2018-04-14 20:55:50.000206	2018-04-14 20:55:50.000206
81	131	46	24	11600	2018-04-14 20:55:50.00421	2018-04-14 20:55:50.00421
82	132	90	1	13600	2018-04-14 20:55:50.00795	2018-04-14 20:55:50.00795
83	133	74	6	14000	2018-04-14 20:55:50.011881	2018-04-14 20:55:50.011881
84	138	39	12	40000	2018-04-14 20:55:50.015688	2018-04-14 20:55:50.015688
85	140	79	70	144000	2018-04-14 20:55:50.019653	2018-04-14 20:55:50.019653
86	141	95	20	90000	2018-04-14 20:55:50.023844	2018-04-14 20:55:50.023844
87	142	95	50	188000	2018-04-14 20:55:50.02807	2018-04-14 20:55:50.02807
88	146	41	6	33600	2018-04-14 20:55:50.033066	2018-04-14 20:55:50.033066
89	147	108	1	1000	2018-04-14 20:55:50.036938	2018-04-14 20:55:50.036938
90	150	83	36	400000	2018-04-14 20:55:50.040857	2018-04-14 20:55:50.040857
91	152	79	8	15600	2018-04-14 20:55:50.044805	2018-04-14 20:55:50.044805
92	153	46	10	4800	2018-04-14 20:55:50.048824	2018-04-14 20:55:50.048824
93	154	72	3	4000	2018-04-14 20:55:50.052872	2018-04-14 20:55:50.052872
94	155	13	8	9000	2018-04-14 20:55:50.056856	2018-04-14 20:55:50.056856
95	156	108	20	60000	2018-04-14 20:55:50.06087	2018-04-14 20:55:50.06087
96	159	108	1	27600	2018-04-14 20:55:50.064863	2018-04-14 20:55:50.064863
97	160	108	1	7600	2018-04-14 20:55:50.068755	2018-04-14 20:55:50.068755
98	163	88	12	9000	2018-04-14 20:55:50.072787	2018-04-14 20:55:50.072787
99	164	79	99	255000	2018-04-14 20:55:50.078464	2018-04-14 20:55:50.078464
100	165	111	15	900000	2018-04-14 20:55:50.087552	2018-04-14 20:55:50.087552
101	171	46	50	39000	2018-04-14 20:55:50.092119	2018-04-14 20:55:50.092119
102	172	89	5	4000	2018-04-14 20:55:50.096338	2018-04-14 20:55:50.096338
103	185	24	12	20000	2018-04-14 20:55:50.10061	2018-04-14 20:55:50.10061
104	187	7	10	12000	2018-04-14 20:55:50.104636	2018-04-14 20:55:50.104636
105	188	88	11	8000	2018-04-14 20:55:50.108996	2018-04-14 20:55:50.108996
106	192	105	10	200000	2018-04-14 20:55:50.113196	2018-04-14 20:55:50.113196
107	193	23	12	4000	2018-04-14 20:55:50.117392	2018-04-14 20:55:50.117392
108	195	46	28	17000	2018-04-14 20:55:50.121868	2018-04-14 20:55:50.121868
109	198	104	16	81600	2018-04-14 20:55:50.126549	2018-04-14 20:55:50.126549
110	199	50	8	9000	2018-04-14 20:55:50.130891	2018-04-14 20:55:50.130891
111	200	6	2	270000	2018-04-14 20:55:50.135045	2018-04-14 20:55:50.135045
112	203	71	99	1400000	2018-04-14 20:55:50.139513	2018-04-14 20:55:50.139513
113	204	112	60	174000	2018-04-14 20:55:50.144312	2018-04-14 20:55:50.144312
114	205	68	20	1120000	2018-04-14 20:55:50.149042	2018-04-14 20:55:50.149042
115	211	74	3	7200	2018-04-14 20:55:50.153656	2018-04-14 20:55:50.153656
116	212	108	2	6300	2018-04-14 20:55:50.157873	2018-04-14 20:55:50.157873
117	214	7	7	7800	2018-04-14 20:55:50.16204	2018-04-14 20:55:50.16204
118	216	32	60	444440	2018-04-14 20:55:50.166609	2018-04-14 20:55:50.166609
119	217	68	1	54000	2018-04-14 20:55:50.17231	2018-04-14 20:55:50.17231
120	218	28	16	74000	2018-04-14 20:55:50.17915	2018-04-14 20:55:50.17915
121	219	28	10	54000	2018-04-14 20:55:50.185825	2018-04-14 20:55:50.185825
122	220	50	6	6000	2018-04-14 20:55:50.192782	2018-04-14 20:55:50.192782
123	222	27	12	124000	2018-04-14 20:55:50.198903	2018-04-14 20:55:50.198903
124	223	27	8	84000	2018-04-14 20:55:50.204649	2018-04-14 20:55:50.204649
125	225	72	12	18000	2018-04-14 20:55:50.210171	2018-04-14 20:55:50.210171
126	230	80	10	157000	2018-04-14 20:55:50.216713	2018-04-14 20:55:50.216713
\.


--
-- Name: bribe_drops_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Kieran
--

SELECT pg_catalog.setval('bribe_drops_id_seq', 126, true);


--
-- Data for Name: elements; Type: TABLE DATA; Schema: public; Owner: Kieran
--

COPY elements (id, monster_id, fire, thunder, ice, created_at, updated_at, water) FROM stdin;
1	1	1	1	1	2018-04-14 20:55:59.336395	2018-04-14 20:55:59.336395	1
2	2	1	1	1	2018-04-14 20:55:59.340234	2018-04-14 20:55:59.340234	1
3	3	1	2	1	2018-04-14 20:55:59.343382	2018-04-14 20:55:59.343382	0.5
4	4	0	1	1	2018-04-14 20:55:59.346449	2018-04-14 20:55:59.346449	1
5	5	1	1	2	2018-04-14 20:55:59.350958	2018-04-14 20:55:59.350958	1
6	6	1	1	1	2018-04-14 20:55:59.354212	2018-04-14 20:55:59.354212	1
7	7	2	1	1	2018-04-14 20:55:59.357303	2018-04-14 20:55:59.357303	1
8	8	1	0	1	2018-04-14 20:55:59.360279	2018-04-14 20:55:59.360279	1
9	9	1	1	1	2018-04-14 20:55:59.3632	2018-04-14 20:55:59.3632	1
10	10	1	2	0	2018-04-14 20:55:59.366366	2018-04-14 20:55:59.366366	1
11	11	2	1	1	2018-04-14 20:55:59.36963	2018-04-14 20:55:59.36963	1
12	12	1	1	1	2018-04-14 20:55:59.372596	2018-04-14 20:55:59.372596	1
13	13	2	1	1	2018-04-14 20:55:59.375568	2018-04-14 20:55:59.375568	1
14	14	1	0.5	1	2018-04-14 20:55:59.378805	2018-04-14 20:55:59.378805	1
15	15	2	1	1	2018-04-14 20:55:59.382062	2018-04-14 20:55:59.382062	1
16	16	1	1	1	2018-04-14 20:55:59.385286	2018-04-14 20:55:59.385286	1
17	17	1	1	1	2018-04-14 20:55:59.388296	2018-04-14 20:55:59.388296	1
18	18	1	1	1	2018-04-14 20:55:59.391293	2018-04-14 20:55:59.391293	1
19	19	1	1	2	2018-04-14 20:55:59.394293	2018-04-14 20:55:59.394293	1
20	20	1	1	1	2018-04-14 20:55:59.397385	2018-04-14 20:55:59.397385	1
21	21	0.5	2	0	2018-04-14 20:55:59.400627	2018-04-14 20:55:59.400627	0.5
22	22	0	1	1	2018-04-14 20:55:59.403582	2018-04-14 20:55:59.403582	2
23	23	0	1	1	2018-04-14 20:55:59.406565	2018-04-14 20:55:59.406565	1
24	24	1	1	1	2018-04-14 20:55:59.409464	2018-04-14 20:55:59.409464	1
25	25	1	1	2	2018-04-14 20:55:59.412294	2018-04-14 20:55:59.412294	1
26	26	1	1	2	2018-04-14 20:55:59.416545	2018-04-14 20:55:59.416545	2
27	27	1	1	1	2018-04-14 20:55:59.420787	2018-04-14 20:55:59.420787	1
28	28	1	1	1	2018-04-14 20:55:59.423695	2018-04-14 20:55:59.423695	1
29	29	1	1	1	2018-04-14 20:55:59.42651	2018-04-14 20:55:59.42651	1
30	30	1	1	1	2018-04-14 20:55:59.429498	2018-04-14 20:55:59.429498	1
31	31	0.5	1	2	2018-04-14 20:55:59.434068	2018-04-14 20:55:59.434068	0.5
32	32	0.5	1	0.5	2018-04-14 20:55:59.439688	2018-04-14 20:55:59.439688	1
33	33	0.5	1	0.5	2018-04-14 20:55:59.444382	2018-04-14 20:55:59.444382	1
34	34	1	1	1	2018-04-14 20:55:59.448769	2018-04-14 20:55:59.448769	1
35	35	2	1	1	2018-04-14 20:55:59.453289	2018-04-14 20:55:59.453289	1
36	36	1	0.5	1	2018-04-14 20:55:59.456676	2018-04-14 20:55:59.456676	0.5
37	37	1	1	1	2018-04-14 20:55:59.460282	2018-04-14 20:55:59.460282	1
38	38	2	1	1	2018-04-14 20:55:59.463194	2018-04-14 20:55:59.463194	1
39	39	0.5	2	0.5	2018-04-14 20:55:59.466094	2018-04-14 20:55:59.466094	0.5
40	40	1	1	1	2018-04-14 20:55:59.469195	2018-04-14 20:55:59.469195	1
41	41	0.5	0.5	0.5	2018-04-14 20:55:59.472321	2018-04-14 20:55:59.472321	0.5
42	42	1	1	1	2018-04-14 20:55:59.475243	2018-04-14 20:55:59.475243	1
43	43	1	1	1	2018-04-14 20:55:59.47826	2018-04-14 20:55:59.47826	1
44	44	0.5	0.5	0.5	2018-04-14 20:55:59.481412	2018-04-14 20:55:59.481412	0.5
45	45	0.5	0.5	0.5	2018-04-14 20:55:59.484826	2018-04-14 20:55:59.484826	0.5
46	46	2	1	1	2018-04-14 20:55:59.487841	2018-04-14 20:55:59.487841	1
47	47	0.5	1	1	2018-04-14 20:55:59.490752	2018-04-14 20:55:59.490752	2
48	48	1	1	1	2018-04-14 20:55:59.493654	2018-04-14 20:55:59.493654	1
49	49	1	1	1	2018-04-14 20:55:59.496593	2018-04-14 20:55:59.496593	1
50	50	1	1	1	2018-04-14 20:55:59.501319	2018-04-14 20:55:59.501319	1
51	51	1	1	1	2018-04-14 20:55:59.505145	2018-04-14 20:55:59.505145	1
52	52	1	1	1	2018-04-14 20:55:59.508333	2018-04-14 20:55:59.508333	1
53	53	2	1	1	2018-04-14 20:55:59.511348	2018-04-14 20:55:59.511348	1
54	54	0.5	0.5	0.5	2018-04-14 20:55:59.514182	2018-04-14 20:55:59.514182	0.5
55	55	1	1	1	2018-04-14 20:55:59.517237	2018-04-14 20:55:59.517237	1
56	56	2	0	0	2018-04-14 20:55:59.520402	2018-04-14 20:55:59.520402	0
57	57	0.5	2	0.5	2018-04-14 20:55:59.524312	2018-04-14 20:55:59.524312	0.5
58	58	0	0	1	2018-04-14 20:55:59.527512	2018-04-14 20:55:59.527512	0
59	59	1	1	1	2018-04-14 20:55:59.530503	2018-04-14 20:55:59.530503	1
60	60	1	1	1	2018-04-14 20:55:59.53377	2018-04-14 20:55:59.53377	1
61	61	0.5	0.5	0.5	2018-04-14 20:55:59.536802	2018-04-14 20:55:59.536802	0.5
62	62	0	0.5	0.5	2018-04-14 20:55:59.539704	2018-04-14 20:55:59.539704	2
63	63	1	1	1	2018-04-14 20:55:59.542552	2018-04-14 20:55:59.542552	1
64	64	1	2	1	2018-04-14 20:55:59.545306	2018-04-14 20:55:59.545306	1
65	65	2	0.5	0.5	2018-04-14 20:55:59.548511	2018-04-14 20:55:59.548511	0.5
66	66	1	1	2	2018-04-14 20:55:59.551661	2018-04-14 20:55:59.551661	1
67	67	2	2	1	2018-04-14 20:55:59.554676	2018-04-14 20:55:59.554676	1
68	68	1	1	1	2018-04-14 20:55:59.557777	2018-04-14 20:55:59.557777	1
69	69	1	1	1	2018-04-14 20:55:59.56086	2018-04-14 20:55:59.56086	1
70	70	0.5	0.5	1	2018-04-14 20:55:59.564076	2018-04-14 20:55:59.564076	1
71	71	0.5	0.5	1	2018-04-14 20:55:59.567253	2018-04-14 20:55:59.567253	1
72	72	1	0.5	0	2018-04-14 20:55:59.570449	2018-04-14 20:55:59.570449	0.5
73	73	2	2	2	2018-04-14 20:55:59.573532	2018-04-14 20:55:59.573532	2
74	74	1	1	1	2018-04-14 20:55:59.576533	2018-04-14 20:55:59.576533	1
75	75	0.5	0	2	2018-04-14 20:55:59.579445	2018-04-14 20:55:59.579445	0.5
76	76	2	1	1	2018-04-14 20:55:59.582612	2018-04-14 20:55:59.582612	1
77	77	2	1	1	2018-04-14 20:55:59.587783	2018-04-14 20:55:59.587783	1
78	78	1	1	1	2018-04-14 20:55:59.591859	2018-04-14 20:55:59.591859	1
79	79	0	1	1	2018-04-14 20:55:59.594812	2018-04-14 20:55:59.594812	0
80	80	0.5	1	1	2018-04-14 20:55:59.597764	2018-04-14 20:55:59.597764	1
81	81	0	1	1	2018-04-14 20:55:59.60324	2018-04-14 20:55:59.60324	1
82	82	1	1	1	2018-04-14 20:55:59.608856	2018-04-14 20:55:59.608856	1
83	83	1	1	1	2018-04-14 20:55:59.613623	2018-04-14 20:55:59.613623	1
84	84	1	1	1	2018-04-14 20:55:59.617973	2018-04-14 20:55:59.617973	1
85	85	2	0.5	0.5	2018-04-14 20:55:59.621237	2018-04-14 20:55:59.621237	0.5
86	86	1	1	1	2018-04-14 20:55:59.624257	2018-04-14 20:55:59.624257	1
87	87	1	1	1	2018-04-14 20:55:59.627233	2018-04-14 20:55:59.627233	1
88	88	2	0.5	0.5	2018-04-14 20:55:59.630411	2018-04-14 20:55:59.630411	0
89	89	0.5	1	2	2018-04-14 20:55:59.635136	2018-04-14 20:55:59.635136	0.5
90	90	1	1	2	2018-04-14 20:55:59.640823	2018-04-14 20:55:59.640823	1
91	91	0.5	1	1	2018-04-14 20:55:59.645556	2018-04-14 20:55:59.645556	2
92	92	1	0.5	1	2018-04-14 20:55:59.650556	2018-04-14 20:55:59.650556	1
93	93	1	1	1	2018-04-14 20:55:59.654572	2018-04-14 20:55:59.654572	1
94	94	1	1	1	2018-04-14 20:55:59.657803	2018-04-14 20:55:59.657803	1
95	95	0	1	1	2018-04-14 20:55:59.661054	2018-04-14 20:55:59.661054	1
96	96	0	0	0	2018-04-14 20:55:59.664188	2018-04-14 20:55:59.664188	0
97	97	1	1	1	2018-04-14 20:55:59.667278	2018-04-14 20:55:59.667278	2
98	98	1	1	1	2018-04-14 20:55:59.670329	2018-04-14 20:55:59.670329	1
99	99	1	1	1	2018-04-14 20:55:59.673187	2018-04-14 20:55:59.673187	1
100	100	1	1	1	2018-04-14 20:55:59.676135	2018-04-14 20:55:59.676135	1
101	101	1	1	2	2018-04-14 20:55:59.679102	2018-04-14 20:55:59.679102	1
102	102	2	1	1	2018-04-14 20:55:59.682218	2018-04-14 20:55:59.682218	1
103	103	1	1	1	2018-04-14 20:55:59.685715	2018-04-14 20:55:59.685715	1
104	104	1	0	2	2018-04-14 20:55:59.688868	2018-04-14 20:55:59.688868	1
105	105	1	1	1	2018-04-14 20:55:59.691786	2018-04-14 20:55:59.691786	1
106	106	2	1	1	2018-04-14 20:55:59.694791	2018-04-14 20:55:59.694791	1
107	107	1	1	1	2018-04-14 20:55:59.697888	2018-04-14 20:55:59.697888	1
108	108	1	2	1	2018-04-14 20:55:59.70123	2018-04-14 20:55:59.70123	1
109	109	1	2	1	2018-04-14 20:55:59.704467	2018-04-14 20:55:59.704467	2
110	110	1	1	1	2018-04-14 20:55:59.70782	2018-04-14 20:55:59.70782	1
111	111	2	1	1	2018-04-14 20:55:59.710894	2018-04-14 20:55:59.710894	1
112	112	2	1	0	2018-04-14 20:55:59.713859	2018-04-14 20:55:59.713859	1
113	113	2	1	0	2018-04-14 20:55:59.71699	2018-04-14 20:55:59.71699	1
114	114	1	1	1	2018-04-14 20:55:59.720236	2018-04-14 20:55:59.720236	1
115	115	0.5	0.5	0.5	2018-04-14 20:55:59.723187	2018-04-14 20:55:59.723187	0.5
116	116	1	2	0	2018-04-14 20:55:59.726206	2018-04-14 20:55:59.726206	1
117	117	1	2	1	2018-04-14 20:55:59.72931	2018-04-14 20:55:59.72931	1
118	118	1	2	1	2018-04-14 20:55:59.732324	2018-04-14 20:55:59.732324	1
119	119	1	2	1	2018-04-14 20:55:59.735536	2018-04-14 20:55:59.735536	1
120	120	1	2	1	2018-04-14 20:55:59.738534	2018-04-14 20:55:59.738534	1
121	121	1	2	1	2018-04-14 20:55:59.741517	2018-04-14 20:55:59.741517	1
122	122	1	2	1	2018-04-14 20:55:59.744645	2018-04-14 20:55:59.744645	1
123	123	0.5	0.5	1	2018-04-14 20:55:59.747662	2018-04-14 20:55:59.747662	2
124	124	2	1	2	2018-04-14 20:55:59.756042	2018-04-14 20:55:59.756042	1
125	125	1	1	1	2018-04-14 20:55:59.759221	2018-04-14 20:55:59.759221	1
126	126	0.5	0.5	0.5	2018-04-14 20:55:59.762334	2018-04-14 20:55:59.762334	0.5
127	127	1	1	1	2018-04-14 20:55:59.765328	2018-04-14 20:55:59.765328	1
128	128	1	1	1	2018-04-14 20:55:59.768689	2018-04-14 20:55:59.768689	1
129	129	1	1	1	2018-04-14 20:55:59.771959	2018-04-14 20:55:59.771959	1
130	130	1	1	1	2018-04-14 20:55:59.774866	2018-04-14 20:55:59.774866	1
131	131	2	2	1	2018-04-14 20:55:59.777856	2018-04-14 20:55:59.777856	1
132	132	2	2	1	2018-04-14 20:55:59.780755	2018-04-14 20:55:59.780755	1
133	133	1	1	1	2018-04-14 20:55:59.783711	2018-04-14 20:55:59.783711	2
134	134	0	0	0	2018-04-14 20:55:59.786955	2018-04-14 20:55:59.786955	0
135	135	0.5	0.5	0.5	2018-04-14 20:55:59.790103	2018-04-14 20:55:59.790103	0.5
136	136	0	0	0	2018-04-14 20:55:59.792856	2018-04-14 20:55:59.792856	0
137	137	1	1	1	2018-04-14 20:55:59.795733	2018-04-14 20:55:59.795733	1
138	138	0.5	1	1	2018-04-14 20:55:59.798677	2018-04-14 20:55:59.798677	2
139	139	0.5	2	0.5	2018-04-14 20:55:59.803484	2018-04-14 20:55:59.803484	0.5
140	140	2	1	0	2018-04-14 20:55:59.808234	2018-04-14 20:55:59.808234	1
141	141	1	2	0	2018-04-14 20:55:59.811706	2018-04-14 20:55:59.811706	1
142	142	2	1	1	2018-04-14 20:55:59.816084	2018-04-14 20:55:59.816084	1
143	143	0.5	0.5	0.5	2018-04-14 20:55:59.819941	2018-04-14 20:55:59.819941	0.5
144	144	1	1	1	2018-04-14 20:55:59.823052	2018-04-14 20:55:59.823052	1
145	145	0	1	1	2018-04-14 20:55:59.826122	2018-04-14 20:55:59.826122	1
146	146	0.5	2	1	2018-04-14 20:55:59.829228	2018-04-14 20:55:59.829228	0.5
147	147	0.5	2	1	2018-04-14 20:55:59.832351	2018-04-14 20:55:59.832351	1
148	148	1	1	1	2018-04-14 20:55:59.835441	2018-04-14 20:55:59.835441	1
149	149	1	1	1	2018-04-14 20:55:59.838451	2018-04-14 20:55:59.838451	1
150	150	1	1	1	2018-04-14 20:55:59.841528	2018-04-14 20:55:59.841528	1
151	151	1	1	1	2018-04-14 20:55:59.844434	2018-04-14 20:55:59.844434	1
152	152	2	1	0	2018-04-14 20:55:59.847485	2018-04-14 20:55:59.847485	1
153	153	1	1	1	2018-04-14 20:55:59.850717	2018-04-14 20:55:59.850717	2
154	154	0.5	0.5	1	2018-04-14 20:55:59.853993	2018-04-14 20:55:59.853993	2
155	155	0	0.5	0.5	2018-04-14 20:55:59.856931	2018-04-14 20:55:59.856931	1
156	156	1	2	0.5	2018-04-14 20:55:59.859805	2018-04-14 20:55:59.859805	1
157	157	1	1	1	2018-04-14 20:55:59.862788	2018-04-14 20:55:59.862788	1
158	158	1	2	1	2018-04-14 20:55:59.865767	2018-04-14 20:55:59.865767	1
159	159	1	2	1	2018-04-14 20:55:59.868863	2018-04-14 20:55:59.868863	1
160	160	0.5	2	1	2018-04-14 20:55:59.872092	2018-04-14 20:55:59.872092	1
161	161	1	2	1	2018-04-14 20:55:59.875112	2018-04-14 20:55:59.875112	1
162	162	1	1	1	2018-04-14 20:55:59.878186	2018-04-14 20:55:59.878186	1
163	163	2	1	1	2018-04-14 20:55:59.881328	2018-04-14 20:55:59.881328	2
164	164	2	1	0	2018-04-14 20:55:59.884887	2018-04-14 20:55:59.884887	1
165	165	0.5	1	2	2018-04-14 20:55:59.888196	2018-04-14 20:55:59.888196	2
166	166	1	1	1	2018-04-14 20:55:59.891031	2018-04-14 20:55:59.891031	1
167	167	1	1	1	2018-04-14 20:55:59.894099	2018-04-14 20:55:59.894099	1
168	168	1	1	1	2018-04-14 20:55:59.897016	2018-04-14 20:55:59.897016	1
169	169	1	1	1	2018-04-14 20:55:59.899897	2018-04-14 20:55:59.899897	1
170	170	1	1	1	2018-04-14 20:55:59.903083	2018-04-14 20:55:59.903083	1
171	171	2	0.5	0.5	2018-04-14 20:55:59.906213	2018-04-14 20:55:59.906213	0.5
172	172	2	1	1	2018-04-14 20:55:59.909456	2018-04-14 20:55:59.909456	1
173	173	1	1	1	2018-04-14 20:55:59.912434	2018-04-14 20:55:59.912434	1
174	174	1	1	1	2018-04-14 20:55:59.91538	2018-04-14 20:55:59.91538	1
175	175	1	1	1	2018-04-14 20:55:59.91833	2018-04-14 20:55:59.91833	1
176	176	1	1	1	2018-04-14 20:55:59.921621	2018-04-14 20:55:59.921621	1
177	177	1	1	1	2018-04-14 20:55:59.924601	2018-04-14 20:55:59.924601	1
178	178	1	1	1	2018-04-14 20:55:59.92758	2018-04-14 20:55:59.92758	1
179	179	1	1	1	2018-04-14 20:55:59.930673	2018-04-14 20:55:59.930673	1
180	180	1	1	1	2018-04-14 20:55:59.933748	2018-04-14 20:55:59.933748	1
181	181	2	1	0	2018-04-14 20:55:59.936809	2018-04-14 20:55:59.936809	1
182	182	2	1	0	2018-04-14 20:55:59.939788	2018-04-14 20:55:59.939788	1
183	183	1	1	1	2018-04-14 20:55:59.942683	2018-04-14 20:55:59.942683	1
184	184	1	1	1	2018-04-14 20:55:59.945571	2018-04-14 20:55:59.945571	1
185	185	2	2	1	2018-04-14 20:55:59.948726	2018-04-14 20:55:59.948726	1
186	186	2	1	1	2018-04-14 20:55:59.951823	2018-04-14 20:55:59.951823	1
187	187	2	0.5	0.5	2018-04-14 20:55:59.954908	2018-04-14 20:55:59.954908	0
188	188	2	1	1	2018-04-14 20:55:59.957685	2018-04-14 20:55:59.957685	1
189	189	1	1	1	2018-04-14 20:55:59.960627	2018-04-14 20:55:59.960627	1
190	190	1	1	1	2018-04-14 20:55:59.963551	2018-04-14 20:55:59.963551	1
191	191	1	1	1	2018-04-14 20:55:59.967566	2018-04-14 20:55:59.967566	1
192	192	0.5	0	0.5	2018-04-14 20:55:59.970768	2018-04-14 20:55:59.970768	0.5
193	193	0.5	2	1	2018-04-14 20:55:59.97382	2018-04-14 20:55:59.97382	1
194	194	1	1	1	2018-04-14 20:55:59.976862	2018-04-14 20:55:59.976862	1
195	195	1	2	1	2018-04-14 20:55:59.979781	2018-04-14 20:55:59.979781	2
196	196	1	1	1	2018-04-14 20:55:59.982906	2018-04-14 20:55:59.982906	1
197	197	1	1	1	2018-04-14 20:55:59.985995	2018-04-14 20:55:59.985995	1
198	198	2	0.5	0.5	2018-04-14 20:55:59.989099	2018-04-14 20:55:59.989099	0.5
199	199	0.5	0	2	2018-04-14 20:55:59.99198	2018-04-14 20:55:59.99198	0.5
200	200	1	1	1	2018-04-14 20:55:59.995008	2018-04-14 20:55:59.995008	1
201	201	1	1	1	2018-04-14 20:55:59.997835	2018-04-14 20:55:59.997835	1
202	202	1	1	1	2018-04-14 20:56:00.000987	2018-04-14 20:56:00.000987	1
203	203	1	1	1	2018-04-14 20:56:00.004484	2018-04-14 20:56:00.004484	1
204	204	0.5	1	1	2018-04-14 20:56:00.007846	2018-04-14 20:56:00.007846	1
205	205	0.5	0.5	0.5	2018-04-14 20:56:00.01122	2018-04-14 20:56:00.01122	0.5
206	206	1	1	1	2018-04-14 20:56:00.014543	2018-04-14 20:56:00.014543	1
207	207	1	1	1	2018-04-14 20:56:00.017932	2018-04-14 20:56:00.017932	1
208	208	1	1	1	2018-04-14 20:56:00.021115	2018-04-14 20:56:00.021115	1
209	209	1	1	1	2018-04-14 20:56:00.024046	2018-04-14 20:56:00.024046	1
210	210	1	1	1	2018-04-14 20:56:00.02718	2018-04-14 20:56:00.02718	1
211	211	1	1	1	2018-04-14 20:56:00.030402	2018-04-14 20:56:00.030402	2
212	212	0.5	2	0	2018-04-14 20:56:00.033709	2018-04-14 20:56:00.033709	0.5
213	213	1	1	1	2018-04-14 20:56:00.036753	2018-04-14 20:56:00.036753	1
214	214	2	0.5	0.5	2018-04-14 20:56:00.039949	2018-04-14 20:56:00.039949	0
215	215	1	2	1	2018-04-14 20:56:00.042872	2018-04-14 20:56:00.042872	1
216	216	1	1	1	2018-04-14 20:56:00.045814	2018-04-14 20:56:00.045814	2
217	217	1	1	1	2018-04-14 20:56:00.04893	2018-04-14 20:56:00.04893	1
218	218	2	1	2	2018-04-14 20:56:00.053629	2018-04-14 20:56:00.053629	1
219	219	2	1	2	2018-04-14 20:56:00.057711	2018-04-14 20:56:00.057711	1
220	220	2	1	2	2018-04-14 20:56:00.06075	2018-04-14 20:56:00.06075	1
221	221	2	1	2	2018-04-14 20:56:00.063743	2018-04-14 20:56:00.063743	1
222	222	0.5	0	2	2018-04-14 20:56:00.066728	2018-04-14 20:56:00.066728	0.5
223	223	1	1	1	2018-04-14 20:56:00.069633	2018-04-14 20:56:00.069633	1
224	224	1	1	1	2018-04-14 20:56:00.072648	2018-04-14 20:56:00.072648	1
225	225	0.5	0.5	0.5	2018-04-14 20:56:00.07563	2018-04-14 20:56:00.07563	2
226	226	1	1	1	2018-04-14 20:56:00.078763	2018-04-14 20:56:00.078763	1
227	227	1	1	1	2018-04-14 20:56:00.081712	2018-04-14 20:56:00.081712	1
228	228	1	1	1	2018-04-14 20:56:00.085071	2018-04-14 20:56:00.085071	1
229	229	1	1	1	2018-04-14 20:56:00.088418	2018-04-14 20:56:00.088418	1
230	230	1	1	1	2018-04-14 20:56:00.091517	2018-04-14 20:56:00.091517	1
231	231	1	1	1	2018-04-14 20:56:00.094594	2018-04-14 20:56:00.094594	1
\.


--
-- Name: elements_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Kieran
--

SELECT pg_catalog.setval('elements_id_seq', 231, true);


--
-- Data for Name: items; Type: TABLE DATA; Schema: public; Owner: Kieran
--

COPY items (id, name, effect, effect_type, slug, created_at, updated_at) FROM stdin;
2	Ability Sphere	Activates ability nodes on Sphere Grid	Sphere	ability-sphere	2018-04-14 20:55:48.87654	2018-04-14 21:18:13.658607
4	Agility Sphere	Turns empty node to Agility+4 node on Sphere Grid	Sphere	agility-sphere	2018-04-14 20:55:48.884125	2018-04-14 21:18:13.663806
5	Al Bhed Potion	Cures Poison, Stone, Silence and restores 1000 HP of all allies	Battle	al-bhed-potion	2018-04-14 20:55:48.887505	2018-04-14 21:18:13.666204
6	Amulet	There seems to be some use for this...	Useless	amulet	2018-04-14 20:55:48.890494	2018-04-14 21:18:13.668941
7	Antarctic Wind	Deals Ice damage to an enemy	Battle	antarctic-wind	2018-04-14 20:55:48.893281	2018-04-14 21:18:13.672081
8	Antidote	Cures Poison	Recovery	antidote	2018-04-14 20:55:48.895685	2018-04-14 21:18:13.675224
9	Arctic Wind	Deals Ice damage to an enemy	Battle	arctic-wind	2018-04-14 20:55:48.897852	2018-04-14 21:18:13.678666
11	Blessed Gem	Deals damage to an enemy	Battle	blessed-gem	2018-04-14 20:55:48.901731	2018-04-14 21:18:13.683899
12	Blk Magic Sphere	Activates black magic node activated by ally on the Sphere Grid	Sphere	blk-magic-sphere	2018-04-14 20:55:48.903806	2018-04-14 21:18:13.686627
13	Bomb Core	Deals Fire damage to an enemy	Battle	bomb-core	2018-04-14 20:55:48.905685	2018-04-14 21:18:13.689082
14	Bomb Fragment	Deals Fire damage to an enemy	Battle	bomb-fragment	2018-04-14 20:55:48.907583	2018-04-14 21:18:13.691611
15	Candle of Life	Casts Doom on an enemy	Battle	candle-of-life	2018-04-14 20:55:48.909655	2018-04-14 21:18:13.694191
16	Chocobo Feather	Casts Haste on one ally	Battle	chocobo-feather	2018-04-14 20:55:48.911701	2018-04-14 21:18:13.697567
17	Chocobo Wing	Casts Haste on all allies	Battle	chocobo-wing	2018-04-14 20:55:48.913994	2018-04-14 21:18:13.7002
18	Clear Sphere	Turns any node to empty node on Sphere Grid	Sphere	clear-sphere	2018-04-14 20:55:48.91619	2018-04-14 21:18:13.702828
19	Dark Matter	Does major damage to all enemies	Battle	dark-matter	2018-04-14 20:55:48.918866	2018-04-14 21:18:13.70534
21	Designer Wallet	There seems to be some use for this...	Useless	designer-wallet	2018-04-14 20:55:48.92306	2018-04-14 21:18:13.711208
22	Door to Tomorrow	There seems to be some use for this...	Useless	door-to-tomorrow	2018-04-14 20:55:48.925041	2018-04-14 21:18:13.715442
23	Dragon Scale	Deals Water damage to an enemy	Battle	dragon-scale	2018-04-14 20:55:48.92701	2018-04-14 21:18:13.717899
24	Dream Powder	Deals damage and causes Sleep on all enemies	Battle	dream-powder	2018-04-14 20:55:48.92899	2018-04-14 21:18:13.720611
25	Echo Screen	Cures Silence	Recovery	echo-screen	2018-04-14 20:55:48.930998	2018-04-14 21:18:13.723201
26	Electro Marble	Deals Thunder damage to an enemy	Battle	electro-marble	2018-04-14 20:55:48.93287	2018-04-14 21:18:13.725667
28	Ether	Restores 100 MP of one ally	Recovery	ether	2018-04-14 20:55:48.936602	2018-04-14 21:18:13.731015
29	Evasion Sphere	Turns empty node to Evasion+4 node on Sphere Grid	Sphere	evasion-sphere	2018-04-14 20:55:48.938608	2018-04-14 21:18:13.733574
30	Eye Drops	Cures Darkness	Recovery	eye-drops	2018-04-14 20:55:48.940834	2018-04-14 21:18:13.736262
31	Farplane Shadow	Casts Death on an enemy	Battle	farplane-shadow	2018-04-14 20:55:48.94303	2018-04-14 21:18:13.739213
32	Farplane Wind	Casts Death on all enemies	Battle	farplane-wind	2018-04-14 20:55:48.945018	2018-04-14 21:18:13.742543
33	Fire Gem	Deals Fire damage to all enemies	Battle	fire-gem	2018-04-14 20:55:48.946849	2018-04-14 21:18:13.745436
34	Fish Scale	Deals Water damage to an enemy	Battle	fish-scale	2018-04-14 20:55:48.948743	2018-04-14 21:18:13.748074
36	Frag Grenade	Deals damage and uses Armor Break on all enemies	Battle	frag-grenade	2018-04-14 20:55:48.952842	2018-04-14 21:18:13.753532
37	Friend Sphere	Move to any ally's current location on Sphere Grid.	Sphere	friend-sphere	2018-04-14 20:55:48.955018	2018-04-14 21:18:13.756404
38	Gambler's Spirit	There seems to be some use for this...	Useless	gambler-s-spirit	2018-04-14 20:55:48.957159	2018-04-14 21:18:13.75965
39	Gold Hourglass	Damages all enemies and delays their next turn	Battle	gold-hourglass	2018-04-14 20:55:48.95919	2018-04-14 21:18:13.762542
40	Grenade	Deals damage to all enemies	Battle	grenade	2018-04-14 20:55:48.961062	2018-04-14 21:18:13.765207
41	Healing Spring	Casts Regen on one ally	Battle	healing-spring	2018-04-14 20:55:48.962937	2018-04-14 21:18:13.767779
43	Hi-Potion	Restores 1000 HP of one character	Recovery	hi-potion	2018-04-14 20:55:48.967077	2018-04-14 21:18:13.772807
44	Holy Water	Cures Zombie and Curse	Recovery	holy-water	2018-04-14 20:55:48.969428	2018-04-14 21:18:13.776557
45	HP Sphere	Turns empty node to HP+4 node on Sphere Grid	Sphere	hp-sphere	2018-04-14 20:55:48.971574	2018-04-14 21:18:13.781189
46	Hypello Potion	There seems to be some use for this...	Useless	hypello-potion	2018-04-14 20:55:48.973713	2018-04-14 21:18:13.784711
47	Ice Gem	Deals Ice damage to all enemies	Battle	ice-gem	2018-04-14 20:55:48.975757	2018-04-14 21:18:13.787366
48	Light Curtain	Casts Protect on one ally	Battle	light-curtain	2018-04-14 20:55:48.977857	2018-04-14 21:18:13.789873
49	Lightning Gem	Deals Thunder damage to all enemies	Battle	lightning-gem	2018-04-14 20:55:48.979854	2018-04-14 21:18:13.792587
51	Luck Sphere	Turns empty node to Luck+4 node on Sphere Grid	Sphere	luck-sphere	2018-04-14 20:55:48.984127	2018-04-14 21:18:13.797838
52	Lunar Curtain	Casts Shell on one ally	Battle	lunar-curtain	2018-04-14 20:55:48.986857	2018-04-14 21:18:13.800479
53	Lv. 1 Key Sphere	Opens Lv. 1 locks on Sphere Grid	Sphere	lv-1-key-sphere	2018-04-14 20:55:48.989965	2018-04-14 21:18:13.803082
54	Lv. 2 Key Sphere	Opens Lv. 2 locks on Sphere Grid	Sphere	lv-2-key-sphere	2018-04-14 20:55:48.993905	2018-04-14 21:18:13.806015
55	Lv. 3 Key Sphere	Opens Lv. 3 locks on Sphere Grid	Sphere	lv-3-key-sphere	2018-04-14 20:55:48.997546	2018-04-14 21:18:13.809114
56	Lv. 4 Key Sphere	Opens Lv. 4 locks on Sphere Grid	Sphere	lv-4-key-sphere	2018-04-14 20:55:49.001104	2018-04-14 21:18:13.812067
58	Magic Sphere	Turns empty node into Magic+4 node	Sphere	magic-sphere	2018-04-14 20:55:49.007257	2018-04-14 21:18:13.817094
59	Mana Distiller	Makes an enemy drop Mana Spheres	Battle	mana-distiller	2018-04-14 20:55:49.009626	2018-04-14 21:18:13.819853
60	Mana Sphere	Activates Magic, Magic Defense, or MP node on Sphere Grid	Sphere	mana-sphere	2018-04-14 20:55:49.011984	2018-04-14 21:18:13.822366
61	Mana Spring	Absorbs MP from an enemy	Battle	mana-spring	2018-04-14 20:55:49.014749	2018-04-14 21:18:13.824876
62	Mana Tablet	Doubles max MP of one ally in battle	Battle	mana-tablet	2018-04-14 20:55:49.017122	2018-04-14 21:18:13.828208
63	Mana Tonic	Doubles max MP of all allies in battle	Battle	mana-tonic	2018-04-14 20:55:49.019095	2018-04-14 21:18:13.830914
64	Map	Displays world map	Misc	map	2018-04-14 20:55:49.02139	2018-04-14 21:18:13.833561
66	Mega Phoenix	Brings all dead allies back to life	Recovery	mega-phoenix	2018-04-14 20:55:49.025312	2018-04-14 21:18:13.838656
67	Mega-Potion	Restores 2000 HP of all allies	Recovery	mega-potion	2018-04-14 20:55:49.027387	2018-04-14 21:18:13.841182
68	Megalixir	Fully restores HP and MP of all allies	Recovery	megalixir	2018-04-14 20:55:49.029737	2018-04-14 21:18:13.843713
69	MP Sphere	Turns empty node to MP+30 node on Sphere Grid	Sphere	mp-sphere	2018-04-14 20:55:49.032135	2018-04-14 21:18:13.846181
70	Musk	There seems to be some use for this...	Useless	musk	2018-04-14 20:55:49.034529	2018-04-14 21:18:13.848591
71	Pendulum	There seems to be some use for this...	Useless	pendulum	2018-04-14 20:55:49.036524	2018-04-14 21:18:13.851192
72	Petrify Grenade	Turns all enemies to Stone	Battle	petrify-grenade	2018-04-14 20:55:49.038864	2018-04-14 21:18:13.853753
73	Phoenix Down	Brings one ally back to life	Recovery	phoenix-down	2018-04-14 20:55:49.040976	2018-04-14 21:18:13.856462
1	Ability Distiller	Makes an enemy drop Ability Spheres	Battle	ability-distiller	2018-04-14 20:55:48.866338	2018-04-14 21:18:13.65357
3	Accuracy Sphere	Turns empty node to Accuracy+4 node on Sphere Grid	Sphere	accuracy-sphere	2018-04-14 20:55:48.880651	2018-04-14 21:18:13.661189
10	Attribute Sphere	Activates attribute node activated by ally on Sphere Grid	Sphere	attribute-sphere	2018-04-14 20:55:48.899769	2018-04-14 21:18:13.681285
20	Defense Sphere	Turns empty node to Defense+4 node	Sphere	defense-sphere	2018-04-14 20:55:48.921006	2018-04-14 21:18:13.708
27	Elixir	Fully restores HP and MP of one character	Recovery	elixir	2018-04-14 20:55:48.93478	2018-04-14 21:18:13.728412
35	Fortune Sphere	Activates Luck node on Sphere Grid	Sphere	fortune-sphere	2018-04-14 20:55:48.950714	2018-04-14 21:18:13.750851
42	Healing Water	Fully restores HP of all allies	Battle	healing-water	2018-04-14 20:55:48.964831	2018-04-14 21:18:13.770292
50	Lightning Marble	Deals Thunder damage to an enemy	Battle	lightning-marble	2018-04-14 20:55:48.981985	2018-04-14 21:18:13.795134
57	Magic Def Sphere	Turns empty node into Magic Def+4 node	Sphere	magic-def-sphere	2018-04-14 20:55:49.004241	2018-04-14 21:18:13.814589
65	Master Sphere	Activates any ability or attribute node on Sphere Grid	Sphere	master-sphere	2018-04-14 20:55:49.023458	2018-04-14 21:18:13.836188
74	Poison Fang	Deals damage and causes Poison on an enemy	Battle	poison-fang	2018-04-14 20:55:49.046111	2018-04-14 21:18:13.858919
75	Potion	Restores 200 HP of one character	Recovery	potion	2018-04-14 20:55:49.048646	2018-04-14 21:18:13.86133
76	Power Distiller	Makes an enemy drop Power Spheres	Battle	power-distiller	2018-04-14 20:55:49.050716	2018-04-14 21:18:13.864019
77	Power Sphere	Activates Strength, Defense, or HP node on Sphere Grid	Sphere	power-sphere	2018-04-14 20:55:49.052757	2018-04-14 21:18:13.866895
78	Purifying Salt	Damages and casts Dispel on an enemy	Battle	purifying-salt	2018-04-14 20:55:49.055885	2018-04-14 21:18:13.869759
79	Remedy	Cures Berserk, Confusion, Darkness, Poison, Silence, Sleep, Slow, and Stone	Recovery	remedy	2018-04-14 20:55:49.058115	2018-04-14 21:18:13.87243
80	Rename Card	Renames an aeon	Misc	rename-card	2018-04-14 20:55:49.060332	2018-04-14 21:18:13.875781
81	Return Sphere	Return to any previously activated node on Sphere Grid	Sphere	return-sphere	2018-04-14 20:55:49.062267	2018-04-14 21:18:13.879175
82	Shadow Gem	Casts Demi on all enemies	Battle	shadow-gem	2018-04-14 20:55:49.064243	2018-04-14 21:18:13.881904
83	Shining Gem	Deals damage to an enemy	Battle	shining-gem	2018-04-14 20:55:49.066221	2018-04-14 21:18:13.88435
84	Shining Thorn	There seems to be some use for this...	Useless	shining-thorn	2018-04-14 20:55:49.068334	2018-04-14 21:18:13.887069
85	Silence Grenade	Deals damage and causes Silence on all enemies	Battle	silence-grenade	2018-04-14 20:55:49.070574	2018-04-14 21:18:13.889554
86	Silver Hourglass	Delays next turn of all enemies	Battle	silver-hourglass	2018-04-14 20:55:49.072575	2018-04-14 21:18:13.892113
87	Skill Sphere	Activates skill node activated by ally on Sphere Grid	Sphere	skill-sphere	2018-04-14 20:55:49.07479	2018-04-14 21:18:13.89482
88	Sleeping Powder	Deals damage and causes Sleep on all enemies	Battle	sleeping-powder	2018-04-14 20:55:49.076959	2018-04-14 21:18:13.897313
89	Smoke Bomb	Deals damage and causes Darkness on all enemies	Battle	smoke-bomb	2018-04-14 20:55:49.078931	2018-04-14 21:18:13.899848
90	Soft	Cures Stone	Recovery	soft	2018-04-14 20:55:49.081245	2018-04-14 21:18:13.902505
91	Soul Spring	Absorbs HP and MP from an enemy	Battle	soul-spring	2018-04-14 20:55:49.083468	2018-04-14 21:18:13.905062
92	Special Sphere	Activates special node activated by ally on Sphere Grid	Sphere	special-sphere	2018-04-14 20:55:49.085587	2018-04-14 21:18:13.907612
93	Speed Distiller	Makes an enemy drop Speed Spheres	Battle	speed-distiller	2018-04-14 20:55:49.088111	2018-04-14 21:18:13.910199
94	Speed Sphere	Activates Accuracy, Agility, or Evasion node on Sphere Grid	Sphere	speed-sphere	2018-04-14 20:55:49.090219	2018-04-14 21:18:13.912559
95	Stamina Spring	Absorbs HP from an enemy	Battle	stamina-spring	2018-04-14 20:55:49.092287	2018-04-14 21:18:13.915206
96	Stamina Tablet	Doubles max HP of one ally in battle	Battle	stamina-tablet	2018-04-14 20:55:49.094183	2018-04-14 21:18:13.917869
97	Stamina Tonic	Doubles max HP of all allies in battle	Battle	stamina-tonic	2018-04-14 20:55:49.096064	2018-04-14 21:18:13.92047
98	Star Curtain	Casts Reflect on one ally	Battle	star-curtain	2018-04-14 20:55:49.097998	2018-04-14 21:18:13.922945
99	Strength Sphere	Turns empty node to Strength+4 node on Sphere Grid	Sphere	strength-sphere	2018-04-14 20:55:49.099744	2018-04-14 21:18:13.941178
100	Supreme Gem	Deals damage to all enemies	Battle	supreme-gem	2018-04-14 20:55:49.101777	2018-04-14 21:18:13.945759
101	Teleport Sphere	Move to any node activated by ally on Sphere Grid	Sphere	teleport-sphere	2018-04-14 20:55:49.103801	2018-04-14 21:18:13.948626
102	Tetra Elemental	Fully restores HP and casts NulBlaze, NulShock, NulTide and NulFrost on all allies	Battle	tetra-elemental	2018-04-14 20:55:49.105897	2018-04-14 21:18:13.951187
103	Three Stars	Reduces MP cost of all abilities to 0 for all allies	Battle	three-stars	2018-04-14 20:55:49.107878	2018-04-14 21:18:13.953768
104	Turbo Ether	Restores 500 MP of one ally	Recovery	turbo-ether	2018-04-14 20:55:49.10979	2018-04-14 21:18:13.956305
105	Twin Stars	Reduces MP cost of all abilities to 0 for an ally	Battle	twin-stars	2018-04-14 20:55:49.111911	2018-04-14 21:18:13.958935
106	Underdog's Secret	There seems to be some use for this...	Useless	underdog-s-secret	2018-04-14 20:55:49.113959	2018-04-14 21:18:13.962401
107	Warp Sphere	Move to any node on the Sphere Grid	Sphere	warp-sphere	2018-04-14 20:55:49.116036	2018-04-14 21:18:13.965269
108	Water Gem	Deals Water damage to all enemies	Battle	water-gem	2018-04-14 20:55:49.118145	2018-04-14 21:18:13.968003
109	Wht Magic Sphere	Activates white magic node activated by ally on Sphere Grid	Sphere	wht-magic-sphere	2018-04-14 20:55:49.120085	2018-04-14 21:18:13.9706
110	Wings to Discovery	There seems to be some use for this...	Useless	wings-to-discovery	2018-04-14 20:55:49.122255	2018-04-14 21:18:13.973191
111	Winning Formula	There seems to be some use for this...	Useless	winning-formula	2018-04-14 20:55:49.124341	2018-04-14 21:18:13.975811
112	X-Potion	Fully restores HP of one ally	Recovery	x-potion	2018-04-14 20:55:49.126835	2018-04-14 21:18:13.978382
\.


--
-- Name: items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Kieran
--

SELECT pg_catalog.setval('items_id_seq', 112, true);


--
-- Data for Name: key_items; Type: TABLE DATA; Schema: public; Owner: Kieran
--

COPY key_items (id, name, description, location_id, details, slug, created_at, updated_at) FROM stdin;
1	Al Bhed Primer II	Translates P to B	19	Found in the Crusader's Lodge near attendant's desk.	al-bhed-primer-ii	2018-04-14 21:29:16.244801	2018-04-14 21:29:24.564134
2	Al Bhed Primer III	Translates L to C	28	Found in the power room.  Re-obtainable in Bikanel Desert where Wakka was found.	al-bhed-primer-iii	2018-04-14 21:29:16.262235	2018-04-14 21:29:24.571417
3	Al Bhed Primer IV	Translates T to D	21	Found in the tavern on the counter.  Only available after Yuna's sending	al-bhed-primer-iv	2018-04-14 21:29:16.270925	2018-04-14 21:29:24.576763
4	Al Bhed Primer VI	Translates V to F	23	Found in the stadium - basement B	al-bhed-primer-vi	2018-04-14 21:29:16.279075	2018-04-14 21:29:24.580909
5	Al Bhed Primer VII	Translates K to G	23	Found in the lower-left corner of the theatre	al-bhed-primer-vii	2018-04-14 21:29:16.288468	2018-04-14 21:29:24.584455
6	Al Bhed Primer VIII	Translates R to H	17	Automatically given to you by Rin	al-bhed-primer-viii	2018-04-14 21:29:16.299207	2018-04-14 21:29:24.589087
7	Al Bhed Primer IX	Translates E to I	17	Found on the Newroad, North	al-bhed-primer-ix	2018-04-14 21:29:16.304248	2018-04-14 21:29:24.592883
8	Al Bhed Primer X	Translates G to K	22	Found on the Precipice, right before the elevator	al-bhed-primer-x	2018-04-14 21:29:16.308007	2018-04-14 21:29:24.597278
9	Al Bhed Primer XI	Translates G to K	13	Found behind a stone pillar on the way to the temple.	al-bhed-primer-xi	2018-04-14 21:29:16.310655	2018-04-14 21:29:24.600629
10	Al Bhed Primer XII	Translates M to L	14	Found on the North Warf	al-bhed-primer-xii	2018-04-14 21:29:16.313341	2018-04-14 21:29:24.604553
11	Al Bhed Primer XIV	Translates H to N	5	Found in the Agency if you tell Rin your studies are going well.  Re-obtainable in Bikanel Desert	al-bhed-primer-xiv	2018-04-14 21:29:16.3179	2018-04-14 21:29:24.608679
12	Al Bhed Primer XV	Translates U to O	15	Found across from O'aka in an alcove	al-bhed-primer-xv	2018-04-14 21:29:16.320563	2018-04-14 21:29:24.612101
13	Al Bhed Primer XVI	Translates B to P	9	Found in front of agency	al-bhed-primer-xvi	2018-04-14 21:29:16.323365	2018-04-14 21:29:24.615429
14	Al Bhed Primer XVII	Translates N to R	7	Found in northern Central under ruins	al-bhed-primer-xvii	2018-04-14 21:29:16.325869	2018-04-14 21:29:24.618977
15	Al Bhed Primer XVIII	Translates N to R	7	Found in Central	al-bhed-primer-xviii	2018-04-14 21:29:16.328366	2018-04-14 21:29:24.62246
16	Al Bhed Primer XX	Translates D to T	16	Found in the Living Quarters on a bed. This is a missable Primer - you will not be able to return to Home	al-bhed-primer-xx	2018-04-14 21:29:16.331316	2018-04-14 21:29:24.625974
17	Al Bhed Primer XXI	Translates I to U	16	Found at the end of Main Corridor. This is a missable Primer - you will not be able to return to Home	al-bhed-primer-xxi	2018-04-14 21:29:16.33405	2018-04-14 21:29:24.629523
18	Al Bhed Primer XXII	Translates J to V	10	Found in the Priest's Passage.  This is a missable Primer - you will not be able to return to Bevelle Temple	al-bhed-primer-xxii	2018-04-14 21:29:16.336822	2018-04-14 21:29:24.633008
19	Al Bhed Primer XXIII	Translates F to W	8	Found in western Central	al-bhed-primer-xxiii	2018-04-14 21:29:16.340463	2018-04-14 21:29:24.636282
20	Al Bhed Primer XXV	Translates O to Y	20	Found in a western alcove	al-bhed-primer-xxv	2018-04-14 21:29:16.346719	2018-04-14 21:29:24.639795
21	Al Bhed Primer XXVI	Translates W to Z	4	Found at the back of the cave near four chests	al-bhed-primer-xxvi	2018-04-14 21:29:16.349313	2018-04-14 21:29:24.643188
22	Al Bhed Primer I	Translates Y to A	27	Found in rear-right corner of Salvage Ship.  Re-obtainable in Bikanel Desert, northeastern area of the Oasis	al-bhed-primer-i	2018-04-14 21:29:16.352069	2018-04-14 21:29:24.647024
23	Al Bhed Primer XIX	Translates C to S	16	Found in first room after cutscene on the left.  This is a missable Primer - you will not be able to return to Home	al-bhed-primer-xix	2018-04-14 21:29:16.354948	2018-04-14 21:29:24.650566
24	Blossom Crown	Seems to have some connection with a hidden aeon...	1	Obtainable after capturing one of each fiend on Mt. Gagazet.  Unlocks one of the seals on the Chamber of the Fayth in Remiem Temple.	blossom-crown	2018-04-14 21:29:16.357714	2018-04-14 21:29:24.654221
25	Celestial Mirror	The surface is shiny and bright	15	Converted from the Cloudy Mirror, used to obtain the legendary weapons	celestial-mirror	2018-04-14 21:29:16.36224	2018-04-14 21:29:24.658054
26	Jecht's Sphere	A sphere recorded by Jecht ten years ago	15	The first of 10 spheres left behind by Jecht, Braska and Auron on their pilgrimage	jecht-s-sphere	2018-04-14 21:29:16.366918	2018-04-14 21:29:24.662763
27	Sun Crest	A Celestial Token, fashioned in the shape of the Sun	29	Partially powers up Tidus's Caladbolg.  Found in Zanarkand Dome after defeating Yunalesca.  If missed, you'll have to defeat an incredibly powerful boss to re-obtain.	sun-crest	2018-04-14 21:29:16.369871	2018-04-14 21:29:24.666518
28	Jupiter Crest	A Celestial Token, fashioned in the shape of Jupiter	23	Partially powers up Wakka's World Champion.  Found in Besaid Aurochs' locker room	jupiter-crest	2018-04-14 21:29:16.372606	2018-04-14 21:29:24.670347
29	Sun Sigil	A Celestial Token, fashioned in the shape of the Sun	8	Fully powers up Tidus's Caladbolg. To obtain, beat the Chocobo trainer in the Calm Lands with a time 0:00. 	sun-sigil	2018-04-14 21:29:16.375231	2018-04-14 21:29:24.674561
30	Mars Crest	A Celestial Token, fashioned in the shape of Mars	17	Partially powers up Auron's Masamune.  Obtained near the entrance to Mushroom Rock Road, along the final area of the Mi'ihen Highroad	mars-crest	2018-04-14 21:29:16.380543	2018-04-14 21:29:24.678267
31	Mars Sigil	A Celestial Token, fashioned in the shape of Mars	1	Fully powers up Auron's Masamune.  To obtain, unlock a total of ten Area Conquest or Species Conquest monsters	mars-sigil	2018-04-14 21:29:16.383139	2018-04-14 21:29:24.681906
32	Mercury Crest	A Celestial Token, fashioned in the shape of Mecury	7	Partially powers up Rikku's Godhand.  Obtained in a treasure chest in a shifting sand pit in the final area of Bikanel Desert	mercury-crest	2018-04-14 21:29:16.385756	2018-04-14 21:29:24.685255
33	Saturn Crest	A Celestial Token, fashioned in the shape of Saturn	12	Partially powers up Kimahri's Spirit Lance.  Obtained near the top of Mt. Gagazet	saturn-crest	2018-04-14 21:29:16.390342	2018-04-14 21:29:24.688727
34	Moon Crest	A Celestial Token, fashioned in the shape of the Moon	19	Partially powers up Yuna's Nirvana.  Found on the beach in Besaid	moon-crest	2018-04-14 21:29:16.395262	2018-04-14 21:29:24.692269
35	Saturn Sigil	A Celestial Token, fashioned in the shape of Saturn	15	Fully powers up Kimahri's Spirit Lance.  Obtained after winning the butterfly game	saturn-sigil	2018-04-14 21:29:16.397994	2018-04-14 21:29:24.695785
36	Venus Sigil	A Celestial Token, fashioned in the shape of Venus	5	Fully powers up Lulu's Onion Knight.  Obtained by dodging 200 consecutive bolts	venus-sigil	2018-04-14 21:29:16.402313	2018-04-14 21:29:24.699638
37	Mark of Conquest	Given to those who have surpassed mortal bounds	1	No use.  Obtained after mastering Monster Arena	mark-of-conquest	2018-04-14 21:29:16.405176	2018-04-14 21:29:24.703162
\.


--
-- Name: key_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Kieran
--

SELECT pg_catalog.setval('key_items_id_seq', 37, true);


--
-- Data for Name: kill_drops; Type: TABLE DATA; Schema: public; Owner: Kieran
--

COPY kill_drops (id, monster_id, item_id, amount, rare, created_at, updated_at) FROM stdin;
1	1	62	1	f	2018-04-14 20:55:50.251168	2018-04-14 20:55:50.251168
2	1	19	1	t	2018-04-14 20:55:50.258323	2018-04-14 20:55:50.258323
3	2	97	1	f	2018-04-14 20:55:50.265238	2018-04-14 20:55:50.265238
4	2	19	1	t	2018-04-14 20:55:50.270995	2018-04-14 20:55:50.270995
5	3	77	1	f	2018-04-14 20:55:50.277386	2018-04-14 20:55:50.277386
6	4	77	2	f	2018-04-14 20:55:50.284828	2018-04-14 20:55:50.284828
7	5	60	1	f	2018-04-14 20:55:50.290773	2018-04-14 20:55:50.290773
8	6	2	1	f	2018-04-14 20:55:50.29493	2018-04-14 20:55:50.29493
9	6	94	1	t	2018-04-14 20:55:50.298922	2018-04-14 20:55:50.298922
10	7	94	1	f	2018-04-14 20:55:50.302893	2018-04-14 20:55:50.302893
11	8	77	1	f	2018-04-14 20:55:50.307016	2018-04-14 20:55:50.307016
12	9	2	1	f	2018-04-14 20:55:50.310993	2018-04-14 20:55:50.310993
13	10	60	1	f	2018-04-14 20:55:50.314887	2018-04-14 20:55:50.314887
14	11	77	1	f	2018-04-14 20:55:50.320142	2018-04-14 20:55:50.320142
15	12	77	1	f	2018-04-14 20:55:50.324344	2018-04-14 20:55:50.324344
16	12	81	1	t	2018-04-14 20:55:50.328538	2018-04-14 20:55:50.328538
17	13	77	1	f	2018-04-14 20:55:50.332691	2018-04-14 20:55:50.332691
18	14	2	1	f	2018-04-14 20:55:50.336928	2018-04-14 20:55:50.336928
19	15	94	1	f	2018-04-14 20:55:50.341864	2018-04-14 20:55:50.341864
20	16	77	1	f	2018-04-14 20:55:50.347257	2018-04-14 20:55:50.347257
21	17	77	2	f	2018-04-14 20:55:50.351344	2018-04-14 20:55:50.351344
22	18	81	1	f	2018-04-14 20:55:50.355284	2018-04-14 20:55:50.355284
23	18	37	1	t	2018-04-14 20:55:50.359182	2018-04-14 20:55:50.359182
24	19	94	1	f	2018-04-14 20:55:50.363095	2018-04-14 20:55:50.363095
25	20	60	2	f	2018-04-14 20:55:50.36761	2018-04-14 20:55:50.36761
26	21	60	1	f	2018-04-14 20:55:50.374119	2018-04-14 20:55:50.374119
27	22	77	1	f	2018-04-14 20:55:50.379484	2018-04-14 20:55:50.379484
28	23	22	1	f	2018-04-14 20:55:50.383348	2018-04-14 20:55:50.383348
29	23	19	1	t	2018-04-14 20:55:50.387085	2018-04-14 20:55:50.387085
30	25	94	1	f	2018-04-14 20:55:50.390921	2018-04-14 20:55:50.390921
31	26	77	1	f	2018-04-14 20:55:50.396643	2018-04-14 20:55:50.396643
32	27	94	2	f	2018-04-14 20:55:50.401815	2018-04-14 20:55:50.401815
33	27	94	3	t	2018-04-14 20:55:50.405921	2018-04-14 20:55:50.405921
34	28	11	3	f	2018-04-14 20:55:50.409828	2018-04-14 20:55:50.409828
35	28	19	1	t	2018-04-14 20:55:50.413892	2018-04-14 20:55:50.413892
36	29	21	1	f	2018-04-14 20:55:50.418342	2018-04-14 20:55:50.418342
37	29	19	1	t	2018-04-14 20:55:50.424768	2018-04-14 20:55:50.424768
38	30	103	1	f	2018-04-14 20:55:50.431688	2018-04-14 20:55:50.431688
39	30	19	1	t	2018-04-14 20:55:50.437713	2018-04-14 20:55:50.437713
40	31	94	1	f	2018-04-14 20:55:50.442713	2018-04-14 20:55:50.442713
41	31	77	1	t	2018-04-14 20:55:50.447566	2018-04-14 20:55:50.447566
42	32	2	1	f	2018-04-14 20:55:50.451661	2018-04-14 20:55:50.451661
43	32	2	2	t	2018-04-14 20:55:50.455538	2018-04-14 20:55:50.455538
44	33	2	1	f	2018-04-14 20:55:50.459502	2018-04-14 20:55:50.459502
45	33	49	2	t	2018-04-14 20:55:50.463348	2018-04-14 20:55:50.463348
46	34	81	1	f	2018-04-14 20:55:50.468168	2018-04-14 20:55:50.468168
47	34	19	1	t	2018-04-14 20:55:50.472581	2018-04-14 20:55:50.472581
48	35	53	2	f	2018-04-14 20:55:50.477539	2018-04-14 20:55:50.477539
49	36	60	1	f	2018-04-14 20:55:50.482284	2018-04-14 20:55:50.482284
50	36	60	2	t	2018-04-14 20:55:50.486684	2018-04-14 20:55:50.486684
51	37	83	3	f	2018-04-14 20:55:50.490932	2018-04-14 20:55:50.490932
52	37	19	1	t	2018-04-14 20:55:50.495223	2018-04-14 20:55:50.495223
53	38	94	1	f	2018-04-14 20:55:50.499915	2018-04-14 20:55:50.499915
54	40	60	1	f	2018-04-14 20:55:50.504985	2018-04-14 20:55:50.504985
55	41	60	1	f	2018-04-14 20:55:50.510202	2018-04-14 20:55:50.510202
56	41	60	2	t	2018-04-14 20:55:50.515525	2018-04-14 20:55:50.515525
57	42	77	2	f	2018-04-14 20:55:50.520115	2018-04-14 20:55:50.520115
58	43	54	1	f	2018-04-14 20:55:50.525099	2018-04-14 20:55:50.525099
59	44	54	1	f	2018-04-14 20:55:50.530569	2018-04-14 20:55:50.530569
60	44	55	1	t	2018-04-14 20:55:50.535967	2018-04-14 20:55:50.535967
61	45	60	2	f	2018-04-14 20:55:50.543753	2018-04-14 20:55:50.543753
62	46	77	1	f	2018-04-14 20:55:50.55028	2018-04-14 20:55:50.55028
63	47	94	1	f	2018-04-14 20:55:50.556055	2018-04-14 20:55:50.556055
64	47	77	1	t	2018-04-14 20:55:50.561218	2018-04-14 20:55:50.561218
65	48	32	3	f	2018-04-14 20:55:50.565698	2018-04-14 20:55:50.565698
66	48	19	1	t	2018-04-14 20:55:50.569938	2018-04-14 20:55:50.569938
67	49	2	1	f	2018-04-14 20:55:50.574092	2018-04-14 20:55:50.574092
68	50	35	1	f	2018-04-14 20:55:50.577885	2018-04-14 20:55:50.577885
69	50	19	1	t	2018-04-14 20:55:50.582088	2018-04-14 20:55:50.582088
70	51	2	1	f	2018-04-14 20:55:50.585926	2018-04-14 20:55:50.585926
71	52	80	1	f	2018-04-14 20:55:50.589654	2018-04-14 20:55:50.589654
72	52	19	1	t	2018-04-14 20:55:50.593392	2018-04-14 20:55:50.593392
73	53	94	1	f	2018-04-14 20:55:50.597267	2018-04-14 20:55:50.597267
74	54	12	1	f	2018-04-14 20:55:50.601322	2018-04-14 20:55:50.601322
75	55	12	1	f	2018-04-14 20:55:50.605479	2018-04-14 20:55:50.605479
76	56	60	1	f	2018-04-14 20:55:50.609446	2018-04-14 20:55:50.609446
77	57	66	1	f	2018-04-14 20:55:50.613565	2018-04-14 20:55:50.613565
78	58	48	20	f	2018-04-14 20:55:50.617767	2018-04-14 20:55:50.617767
79	58	19	1	t	2018-04-14 20:55:50.621637	2018-04-14 20:55:50.621637
80	59	2	1	f	2018-04-14 20:55:50.625368	2018-04-14 20:55:50.625368
81	60	2	1	f	2018-04-14 20:55:50.62948	2018-04-14 20:55:50.62948
82	61	4	1	f	2018-04-14 20:55:50.633706	2018-04-14 20:55:50.633706
83	61	19	1	t	2018-04-14 20:55:50.637961	2018-04-14 20:55:50.637961
84	62	60	1	f	2018-04-14 20:55:50.642074	2018-04-14 20:55:50.642074
85	63	60	2	f	2018-04-14 20:55:50.646143	2018-04-14 20:55:50.646143
86	64	94	1	f	2018-04-14 20:55:50.65028	2018-04-14 20:55:50.65028
87	65	60	1	f	2018-04-14 20:55:50.654063	2018-04-14 20:55:50.654063
88	66	60	1	f	2018-04-14 20:55:50.657901	2018-04-14 20:55:50.657901
89	67	77	1	f	2018-04-14 20:55:50.66233	2018-04-14 20:55:50.66233
90	68	77	1	f	2018-04-14 20:55:50.66727	2018-04-14 20:55:50.66727
91	69	77	1	f	2018-04-14 20:55:50.671335	2018-04-14 20:55:50.671335
92	70	77	1	f	2018-04-14 20:55:50.675284	2018-04-14 20:55:50.675284
93	70	77	2	t	2018-04-14 20:55:50.679345	2018-04-14 20:55:50.679345
94	71	77	1	f	2018-04-14 20:55:50.68335	2018-04-14 20:55:50.68335
95	71	77	2	t	2018-04-14 20:55:50.687426	2018-04-14 20:55:50.687426
96	72	2	1	f	2018-04-14 20:55:50.691319	2018-04-14 20:55:50.691319
97	74	60	1	f	2018-04-14 20:55:50.69519	2018-04-14 20:55:50.69519
98	75	60	1	f	2018-04-14 20:55:50.699109	2018-04-14 20:55:50.699109
99	76	60	1	f	2018-04-14 20:55:50.703203	2018-04-14 20:55:50.703203
100	77	60	2	f	2018-04-14 20:55:50.707122	2018-04-14 20:55:50.707122
101	78	51	1	f	2018-04-14 20:55:50.710896	2018-04-14 20:55:50.710896
102	78	19	1	t	2018-04-14 20:55:50.715062	2018-04-14 20:55:50.715062
103	79	77	1	f	2018-04-14 20:55:50.719023	2018-04-14 20:55:50.719023
104	80	2	1	f	2018-04-14 20:55:50.722956	2018-04-14 20:55:50.722956
105	80	2	2	t	2018-04-14 20:55:50.726955	2018-04-14 20:55:50.726955
106	82	2	1	f	2018-04-14 20:55:50.731193	2018-04-14 20:55:50.731193
107	83	2	1	f	2018-04-14 20:55:50.735164	2018-04-14 20:55:50.735164
108	84	2	1	f	2018-04-14 20:55:50.739095	2018-04-14 20:55:50.739095
109	85	77	1	f	2018-04-14 20:55:50.743207	2018-04-14 20:55:50.743207
110	85	77	2	t	2018-04-14 20:55:50.749109	2018-04-14 20:55:50.749109
111	87	3	1	f	2018-04-14 20:55:50.754045	2018-04-14 20:55:50.754045
112	87	19	1	t	2018-04-14 20:55:50.760363	2018-04-14 20:55:50.760363
113	88	60	1	f	2018-04-14 20:55:50.768873	2018-04-14 20:55:50.768873
114	89	94	1	f	2018-04-14 20:55:50.773024	2018-04-14 20:55:50.773024
115	89	77	1	t	2018-04-14 20:55:50.777286	2018-04-14 20:55:50.777286
116	90	60	1	f	2018-04-14 20:55:50.781507	2018-04-14 20:55:50.781507
117	91	94	1	f	2018-04-14 20:55:50.785635	2018-04-14 20:55:50.785635
118	91	77	1	t	2018-04-14 20:55:50.789982	2018-04-14 20:55:50.789982
119	92	77	2	f	2018-04-14 20:55:50.794174	2018-04-14 20:55:50.794174
120	92	77	3	t	2018-04-14 20:55:50.798222	2018-04-14 20:55:50.798222
121	93	45	1	f	2018-04-14 20:55:50.802382	2018-04-14 20:55:50.802382
122	93	19	1	t	2018-04-14 20:55:50.806183	2018-04-14 20:55:50.806183
123	94	100	2	f	2018-04-14 20:55:50.811151	2018-04-14 20:55:50.811151
124	94	19	1	t	2018-04-14 20:55:50.815777	2018-04-14 20:55:50.815777
125	95	99	1	f	2018-04-14 20:55:50.820696	2018-04-14 20:55:50.820696
126	95	19	1	t	2018-04-14 20:55:50.825612	2018-04-14 20:55:50.825612
127	96	58	1	f	2018-04-14 20:55:50.829717	2018-04-14 20:55:50.829717
128	96	19	1	t	2018-04-14 20:55:50.833828	2018-04-14 20:55:50.833828
129	97	94	1	f	2018-04-14 20:55:50.837949	2018-04-14 20:55:50.837949
130	98	2	2	f	2018-04-14 20:55:50.841988	2018-04-14 20:55:50.841988
131	99	2	2	f	2018-04-14 20:55:50.845981	2018-04-14 20:55:50.845981
132	100	41	20	f	2018-04-14 20:55:50.85036	2018-04-14 20:55:50.85036
133	100	19	1	t	2018-04-14 20:55:50.854366	2018-04-14 20:55:50.854366
134	101	77	1	f	2018-04-14 20:55:50.858541	2018-04-14 20:55:50.858541
135	102	77	1	f	2018-04-14 20:55:50.862942	2018-04-14 20:55:50.862942
136	103	77	1	f	2018-04-14 20:55:50.867321	2018-04-14 20:55:50.867321
137	103	56	1	t	2018-04-14 20:55:50.871437	2018-04-14 20:55:50.871437
138	104	2	1	f	2018-04-14 20:55:50.875633	2018-04-14 20:55:50.875633
139	105	45	1	f	2018-04-14 20:55:50.879944	2018-04-14 20:55:50.879944
140	106	69	1	f	2018-04-14 20:55:50.88421	2018-04-14 20:55:50.88421
141	107	60	1	f	2018-04-14 20:55:50.88801	2018-04-14 20:55:50.88801
142	107	55	1	t	2018-04-14 20:55:50.891852	2018-04-14 20:55:50.891852
143	108	60	1	f	2018-04-14 20:55:50.895709	2018-04-14 20:55:50.895709
144	109	77	1	f	2018-04-14 20:55:50.90006	2018-04-14 20:55:50.90006
145	111	60	2	f	2018-04-14 20:55:50.903738	2018-04-14 20:55:50.903738
146	112	63	2	f	2018-04-14 20:55:50.907778	2018-04-14 20:55:50.907778
147	112	19	1	t	2018-04-14 20:55:50.911731	2018-04-14 20:55:50.911731
148	113	60	1	f	2018-04-14 20:55:50.916293	2018-04-14 20:55:50.916293
149	114	53	1	f	2018-04-14 20:55:50.920274	2018-04-14 20:55:50.920274
150	114	37	1	t	2018-04-14 20:55:50.924012	2018-04-14 20:55:50.924012
151	115	77	1	f	2018-04-14 20:55:50.927919	2018-04-14 20:55:50.927919
152	115	101	1	t	2018-04-14 20:55:50.934244	2018-04-14 20:55:50.934244
153	116	2	1	f	2018-04-14 20:55:50.939299	2018-04-14 20:55:50.939299
154	117	73	1	f	2018-04-14 20:55:50.943077	2018-04-14 20:55:50.943077
155	117	66	1	t	2018-04-14 20:55:50.947004	2018-04-14 20:55:50.947004
156	118	43	1	f	2018-04-14 20:55:50.952091	2018-04-14 20:55:50.952091
157	119	43	2	f	2018-04-14 20:55:50.956919	2018-04-14 20:55:50.956919
158	120	73	1	f	2018-04-14 20:55:50.961126	2018-04-14 20:55:50.961126
159	120	66	1	t	2018-04-14 20:55:50.965275	2018-04-14 20:55:50.965275
160	121	43	1	f	2018-04-14 20:55:50.971975	2018-04-14 20:55:50.971975
161	121	67	1	t	2018-04-14 20:55:50.975845	2018-04-14 20:55:50.975845
162	122	43	1	f	2018-04-14 20:55:50.979906	2018-04-14 20:55:50.979906
163	122	67	1	t	2018-04-14 20:55:50.983982	2018-04-14 20:55:50.983982
164	123	94	1	f	2018-04-14 20:55:50.987852	2018-04-14 20:55:50.987852
165	123	77	1	t	2018-04-14 20:55:50.99164	2018-04-14 20:55:50.99164
166	124	77	1	f	2018-04-14 20:55:50.995429	2018-04-14 20:55:50.995429
167	131	77	1	f	2018-04-14 20:55:50.999864	2018-04-14 20:55:50.999864
168	132	77	1	f	2018-04-14 20:55:51.003689	2018-04-14 20:55:51.003689
169	133	60	1	f	2018-04-14 20:55:51.007397	2018-04-14 20:55:51.007397
170	134	105	2	f	2018-04-14 20:55:51.014046	2018-04-14 20:55:51.014046
171	134	19	1	t	2018-04-14 20:55:51.018878	2018-04-14 20:55:51.018878
172	135	75	1	f	2018-04-14 20:55:51.022961	2018-04-14 20:55:51.022961
173	135	43	1	t	2018-04-14 20:55:51.026898	2018-04-14 20:55:51.026898
174	136	107	1	f	2018-04-14 20:55:51.030586	2018-04-14 20:55:51.030586
175	136	19	1	t	2018-04-14 20:55:51.03482	2018-04-14 20:55:51.03482
176	137	71	1	f	2018-04-14 20:55:51.038785	2018-04-14 20:55:51.038785
177	137	19	1	t	2018-04-14 20:55:51.04276	2018-04-14 20:55:51.04276
178	138	77	1	f	2018-04-14 20:55:51.046676	2018-04-14 20:55:51.046676
179	140	77	1	f	2018-04-14 20:55:51.05101	2018-04-14 20:55:51.05101
180	140	60	2	t	2018-04-14 20:55:51.055017	2018-04-14 20:55:51.055017
181	141	60	1	f	2018-04-14 20:55:51.058707	2018-04-14 20:55:51.058707
182	142	77	1	f	2018-04-14 20:55:51.062355	2018-04-14 20:55:51.062355
183	143	56	3	f	2018-04-14 20:55:51.066407	2018-04-14 20:55:51.066407
184	144	57	1	f	2018-04-14 20:55:51.070457	2018-04-14 20:55:51.070457
185	144	19	1	t	2018-04-14 20:55:51.074163	2018-04-14 20:55:51.074163
186	145	38	2	f	2018-04-14 20:55:51.077804	2018-04-14 20:55:51.077804
187	145	19	1	t	2018-04-14 20:55:51.081707	2018-04-14 20:55:51.081707
188	146	77	1	f	2018-04-14 20:55:51.085909	2018-04-14 20:55:51.085909
189	147	77	1	f	2018-04-14 20:55:51.089738	2018-04-14 20:55:51.089738
190	147	2	1	t	2018-04-14 20:55:51.093503	2018-04-14 20:55:51.093503
191	149	29	1	f	2018-04-14 20:55:51.097393	2018-04-14 20:55:51.097393
192	149	19	1	t	2018-04-14 20:55:51.101611	2018-04-14 20:55:51.101611
193	150	60	1	f	2018-04-14 20:55:51.105641	2018-04-14 20:55:51.105641
194	150	53	1	t	2018-04-14 20:55:51.109853	2018-04-14 20:55:51.109853
195	151	94	1	f	2018-04-14 20:55:51.113877	2018-04-14 20:55:51.113877
196	151	94	2	t	2018-04-14 20:55:51.118038	2018-04-14 20:55:51.118038
197	152	60	1	f	2018-04-14 20:55:51.122133	2018-04-14 20:55:51.122133
198	153	77	1	f	2018-04-14 20:55:51.126091	2018-04-14 20:55:51.126091
199	154	94	1	f	2018-04-14 20:55:51.129768	2018-04-14 20:55:51.129768
200	154	77	1	t	2018-04-14 20:55:51.136437	2018-04-14 20:55:51.136437
201	155	60	1	f	2018-04-14 20:55:51.141224	2018-04-14 20:55:51.141224
202	156	60	1	f	2018-04-14 20:55:51.145155	2018-04-14 20:55:51.145155
203	157	55	1	f	2018-04-14 20:55:51.154584	2018-04-14 20:55:51.154584
204	159	77	1	f	2018-04-14 20:55:51.158973	2018-04-14 20:55:51.158973
205	160	77	1	f	2018-04-14 20:55:51.162666	2018-04-14 20:55:51.162666
206	161	77	1	f	2018-04-14 20:55:51.166912	2018-04-14 20:55:51.166912
207	162	81	1	f	2018-04-14 20:55:51.170945	2018-04-14 20:55:51.170945
208	163	77	1	f	2018-04-14 20:55:51.174836	2018-04-14 20:55:51.174836
209	163	5	1	t	2018-04-14 20:55:51.178545	2018-04-14 20:55:51.178545
210	164	60	1	f	2018-04-14 20:55:51.182839	2018-04-14 20:55:51.182839
211	165	2	2	f	2018-04-14 20:55:51.186863	2018-04-14 20:55:51.186863
212	165	53	1	t	2018-04-14 20:55:51.193518	2018-04-14 20:55:51.193518
213	166	12	1	f	2018-04-14 20:55:51.200867	2018-04-14 20:55:51.200867
214	166	92	1	t	2018-04-14 20:55:51.208233	2018-04-14 20:55:51.208233
215	167	56	1	f	2018-04-14 20:55:51.214139	2018-04-14 20:55:51.214139
216	168	54	2	f	2018-04-14 20:55:51.219312	2018-04-14 20:55:51.219312
217	169	55	1	f	2018-04-14 20:55:51.223237	2018-04-14 20:55:51.223237
218	169	55	2	t	2018-04-14 20:55:51.22714	2018-04-14 20:55:51.22714
219	170	110	1	f	2018-04-14 20:55:51.231582	2018-04-14 20:55:51.231582
220	170	19	1	t	2018-04-14 20:55:51.235574	2018-04-14 20:55:51.235574
221	171	94	1	f	2018-04-14 20:55:51.239621	2018-04-14 20:55:51.239621
222	172	94	1	f	2018-04-14 20:55:51.243419	2018-04-14 20:55:51.243419
223	173	55	1	f	2018-04-14 20:55:51.247281	2018-04-14 20:55:51.247281
224	174	69	1	f	2018-04-14 20:55:51.251045	2018-04-14 20:55:51.251045
225	175	60	1	f	2018-04-14 20:55:51.254711	2018-04-14 20:55:51.254711
226	177	60	1	f	2018-04-14 20:55:51.258797	2018-04-14 20:55:51.258797
227	178	60	1	f	2018-04-14 20:55:51.262683	2018-04-14 20:55:51.262683
228	180	2	2	f	2018-04-14 20:55:51.266574	2018-04-14 20:55:51.266574
229	181	81	1	f	2018-04-14 20:55:51.270549	2018-04-14 20:55:51.270549
230	182	77	2	f	2018-04-14 20:55:51.274437	2018-04-14 20:55:51.274437
231	182	77	3	t	2018-04-14 20:55:51.278127	2018-04-14 20:55:51.278127
232	185	77	1	f	2018-04-14 20:55:51.283386	2018-04-14 20:55:51.283386
233	186	101	1	f	2018-04-14 20:55:51.288937	2018-04-14 20:55:51.288937
234	186	19	1	t	2018-04-14 20:55:51.292889	2018-04-14 20:55:51.292889
235	187	60	1	f	2018-04-14 20:55:51.296824	2018-04-14 20:55:51.296824
236	188	77	1	f	2018-04-14 20:55:51.30074	2018-04-14 20:55:51.30074
237	190	56	1	f	2018-04-14 20:55:51.304581	2018-04-14 20:55:51.304581
238	191	54	1	f	2018-04-14 20:55:51.309619	2018-04-14 20:55:51.309619
239	192	60	1	f	2018-04-14 20:55:51.31343	2018-04-14 20:55:51.31343
240	192	37	1	t	2018-04-14 20:55:51.317648	2018-04-14 20:55:51.317648
241	193	77	1	f	2018-04-14 20:55:51.321675	2018-04-14 20:55:51.321675
242	194	6	2	f	2018-04-14 20:55:51.32572	2018-04-14 20:55:51.32572
243	194	19	1	t	2018-04-14 20:55:51.332324	2018-04-14 20:55:51.332324
244	196	20	1	f	2018-04-14 20:55:51.336868	2018-04-14 20:55:51.336868
245	196	19	1	t	2018-04-14 20:55:51.341071	2018-04-14 20:55:51.341071
246	197	106	1	f	2018-04-14 20:55:51.345008	2018-04-14 20:55:51.345008
247	197	19	1	t	2018-04-14 20:55:51.349128	2018-04-14 20:55:51.349128
248	198	60	1	f	2018-04-14 20:55:51.353784	2018-04-14 20:55:51.353784
249	199	60	1	f	2018-04-14 20:55:51.358028	2018-04-14 20:55:51.358028
250	200	77	1	f	2018-04-14 20:55:51.362217	2018-04-14 20:55:51.362217
251	200	54	1	t	2018-04-14 20:55:51.366525	2018-04-14 20:55:51.366525
252	201	77	2	f	2018-04-14 20:55:51.370778	2018-04-14 20:55:51.370778
253	202	111	1	f	2018-04-14 20:55:51.374818	2018-04-14 20:55:51.374818
254	202	19	1	t	2018-04-14 20:55:51.378845	2018-04-14 20:55:51.378845
255	203	55	3	f	2018-04-14 20:55:51.382958	2018-04-14 20:55:51.382958
256	204	2	1	f	2018-04-14 20:55:51.387424	2018-04-14 20:55:51.387424
257	205	58	1	f	2018-04-14 20:55:51.391453	2018-04-14 20:55:51.391453
258	205	54	1	t	2018-04-14 20:55:51.395773	2018-04-14 20:55:51.395773
259	206	69	1	f	2018-04-14 20:55:51.400996	2018-04-14 20:55:51.400996
260	206	19	1	t	2018-04-14 20:55:51.406743	2018-04-14 20:55:51.406743
261	207	37	1	f	2018-04-14 20:55:51.411808	2018-04-14 20:55:51.411808
262	207	19	1	t	2018-04-14 20:55:51.418463	2018-04-14 20:55:51.418463
263	209	2	1	f	2018-04-14 20:55:51.424948	2018-04-14 20:55:51.424948
264	210	2	1	f	2018-04-14 20:55:51.430953	2018-04-14 20:55:51.430953
265	211	94	1	f	2018-04-14 20:55:51.441181	2018-04-14 20:55:51.441181
266	212	60	1	f	2018-04-14 20:55:51.449382	2018-04-14 20:55:51.449382
267	213	77	1	f	2018-04-14 20:55:51.455991	2018-04-14 20:55:51.455991
268	213	77	2	t	2018-04-14 20:55:51.461502	2018-04-14 20:55:51.461502
269	214	60	1	f	2018-04-14 20:55:51.467614	2018-04-14 20:55:51.467614
270	215	43	1	f	2018-04-14 20:55:51.473335	2018-04-14 20:55:51.473335
271	215	112	1	t	2018-04-14 20:55:51.479283	2018-04-14 20:55:51.479283
272	216	58	1	f	2018-04-14 20:55:51.484555	2018-04-14 20:55:51.484555
273	216	58	2	t	2018-04-14 20:55:51.491222	2018-04-14 20:55:51.491222
274	217	2	1	f	2018-04-14 20:55:51.497111	2018-04-14 20:55:51.497111
275	217	2	2	t	2018-04-14 20:55:51.503652	2018-04-14 20:55:51.503652
276	218	66	1	t	2018-04-14 20:55:51.509137	2018-04-14 20:55:51.509137
277	219	73	1	f	2018-04-14 20:55:51.515243	2018-04-14 20:55:51.515243
278	219	66	1	t	2018-04-14 20:55:51.521248	2018-04-14 20:55:51.521248
279	220	60	1	f	2018-04-14 20:55:51.531754	2018-04-14 20:55:51.531754
280	221	81	1	f	2018-04-14 20:55:51.536604	2018-04-14 20:55:51.536604
281	221	37	1	t	2018-04-14 20:55:51.540514	2018-04-14 20:55:51.540514
282	222	43	2	f	2018-04-14 20:55:51.544365	2018-04-14 20:55:51.544365
283	222	67	1	t	2018-04-14 20:55:51.54796	2018-04-14 20:55:51.54796
284	223	43	1	f	2018-04-14 20:55:51.55194	2018-04-14 20:55:51.55194
285	223	67	1	t	2018-04-14 20:55:51.555879	2018-04-14 20:55:51.555879
286	225	94	1	f	2018-04-14 20:55:51.559782	2018-04-14 20:55:51.559782
287	225	77	1	t	2018-04-14 20:55:51.563493	2018-04-14 20:55:51.563493
288	229	55	1	f	2018-04-14 20:55:51.568355	2018-04-14 20:55:51.568355
289	230	94	2	f	2018-04-14 20:55:51.57331	2018-04-14 20:55:51.57331
290	231	77	1	f	2018-04-14 20:55:51.577225	2018-04-14 20:55:51.577225
291	231	77	2	t	2018-04-14 20:55:51.580847	2018-04-14 20:55:51.580847
\.


--
-- Name: kill_drops_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Kieran
--

SELECT pg_catalog.setval('kill_drops_id_seq', 291, true);


--
-- Data for Name: locations; Type: TABLE DATA; Schema: public; Owner: Kieran
--

COPY locations (id, name, slug, created_at, updated_at) FROM stdin;
1	Monster Arena	monster-arena	2018-04-14 20:55:48.022979	2018-04-14 20:55:48.022979
2	Gagazet - Mountain Cave	gagazet-mountain-cave	2018-04-14 20:55:48.031645	2018-04-14 20:55:48.031645
3	Inside Sin	inside-sin	2018-04-14 20:55:48.034035	2018-04-14 20:55:48.034035
4	Omega Ruins	omega-ruins	2018-04-14 20:55:48.036461	2018-04-14 20:55:48.036461
5	Thunder Plains	thunder-plains	2018-04-14 20:55:48.038583	2018-04-14 20:55:48.038583
6	Ruins of Zanarkand	ruins-of-zanarkand	2018-04-14 20:55:48.040462	2018-04-14 20:55:48.040462
7	Sanubia Desert	sanubia-desert	2018-04-14 20:55:48.042578	2018-04-14 20:55:48.042578
8	Calm Lands	calm-lands	2018-04-14 20:55:48.044323	2018-04-14 20:55:48.044323
9	Lake Macalania	lake-macalania	2018-04-14 20:55:48.046143	2018-04-14 20:55:48.046143
10	Bevelle	bevelle	2018-04-14 20:55:48.047977	2018-04-14 20:55:48.047977
11	Via Purifico	via-purifico	2018-04-14 20:55:48.049811	2018-04-14 20:55:48.049811
12	Gagazet - Mountain Path	gagazet-mountain-path	2018-04-14 20:55:48.051645	2018-04-14 20:55:48.051645
13	Djose Highroad and Temple	djose-highroad-and-temple	2018-04-14 20:55:48.053423	2018-04-14 20:55:48.053423
14	Moonflow	moonflow	2018-04-14 20:55:48.055232	2018-04-14 20:55:48.055232
15	Macalania Woods	macalania-woods	2018-04-14 20:55:48.057082	2018-04-14 20:55:48.057082
16	Home	home	2018-04-14 20:55:48.058844	2018-04-14 20:55:48.058844
17	Mi'ihen Highroad	mi-ihen-highroad	2018-04-14 20:55:48.0606	2018-04-14 20:55:48.0606
18	Village of the Cactuars	village-of-the-cactuars	2018-04-14 20:55:48.062472	2018-04-14 20:55:48.062472
19	Besaid	besaid	2018-04-14 20:55:48.064973	2018-04-14 20:55:48.064973
20	Cavern of the Stolen Fayth	cavern-of-the-stolen-fayth	2018-04-14 20:55:48.067333	2018-04-14 20:55:48.067333
21	Kilika	kilika	2018-04-14 20:55:48.069644	2018-04-14 20:55:48.069644
22	Mushroom Rock	mushroom-rock	2018-04-14 20:55:48.071894	2018-04-14 20:55:48.071894
23	Luca	luca	2018-04-14 20:55:48.074104	2018-04-14 20:55:48.074104
24	Ruins	ruins	2018-04-14 20:55:48.076215	2018-04-14 20:55:48.076215
25	Baaj Temple	baaj-temple	2018-04-14 20:55:48.078243	2018-04-14 20:55:48.078243
26	Airship - Battle with Sin	airship-battle-with-sin	2018-04-14 20:55:48.080557	2018-04-14 20:55:48.080557
27	Salvage Ship	salvage-ship	2018-04-14 20:55:48.083455	2018-04-14 20:55:48.083455
28	S.S. Liki	s-s-liki	2018-04-14 20:55:48.085291	2018-04-14 20:55:48.085291
29	Destruction of Zanarkand	destruction-of-zanarkand	2018-04-14 20:55:48.087031	2018-04-14 20:55:48.087031
\.


--
-- Name: locations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Kieran
--

SELECT pg_catalog.setval('locations_id_seq', 29, true);


--
-- Data for Name: locations_monsters; Type: TABLE DATA; Schema: public; Owner: Kieran
--

COPY locations_monsters (id, location_id, monster_id) FROM stdin;
1	1	1
2	1	2
3	2	3
4	3	4
5	4	4
6	5	5
7	2	6
8	3	6
9	6	6
10	7	7
11	8	8
12	9	9
13	10	10
14	11	10
15	2	11
16	12	11
17	3	12
18	2	13
19	12	13
20	6	13
21	13	14
22	10	15
23	11	15
24	2	16
25	6	16
26	3	17
27	12	18
28	13	19
29	14	19
30	4	20
31	15	21
32	10	22
33	16	22
34	17	22
35	1	23
36	3	24
37	5	25
38	13	26
39	14	26
40	7	27
41	18	27
42	1	28
43	1	29
44	1	30
45	10	31
46	11	31
47	16	32
48	15	32
49	8	33
50	1	34
51	17	35
52	8	36
53	1	37
54	19	38
55	9	39
56	20	40
57	2	41
58	6	41
59	20	42
60	8	43
61	6	44
62	4	44
63	3	45
64	4	45
65	19	46
66	21	47
67	1	48
68	10	49
69	16	49
70	17	49
71	1	50
72	8	51
73	20	51
74	1	52
75	10	53
76	16	53
77	9	53
78	10	54
79	11	55
80	3	56
81	14	57
82	1	58
83	6	59
84	1	61
85	8	62
86	4	63
87	17	64
88	22	64
89	13	65
90	14	65
91	22	65
92	22	66
93	13	67
94	14	67
95	19	68
96	22	68
97	23	69
98	3	70
99	4	70
100	3	71
101	4	71
102	24	73
103	25	73
104	20	74
105	5	75
106	12	76
107	3	77
108	1	78
109	12	79
110	2	80
111	6	80
112	11	81
113	16	82
114	9	82
115	4	85
116	1	87
117	9	88
118	15	89
119	20	90
120	12	90
121	22	91
122	5	92
123	1	93
124	1	94
125	1	95
126	1	96
127	21	97
128	19	98
129	24	99
130	1	100
131	5	101
132	13	102
133	22	102
134	3	103
135	5	104
136	26	105
137	21	106
138	4	107
139	2	108
140	20	110
141	8	111
142	1	112
143	2	113
144	6	113
145	4	114
146	4	115
147	11	116
148	2	117
149	12	117
150	7	118
151	7	119
152	20	120
153	2	121
154	12	121
155	8	122
156	5	123
157	17	124
158	4	125
159	4	126
160	4	127
161	4	128
162	11	129
163	12	130
164	15	131
165	7	132
166	8	133
167	1	134
168	9	135
169	1	136
170	1	137
171	2	138
172	12	138
173	23	139
174	14	140
175	11	141
176	8	142
177	4	143
178	1	144
179	1	145
180	11	146
181	19	147
182	27	147
183	11	148
184	1	149
185	4	150
186	5	151
187	21	152
188	17	153
189	13	154
190	22	154
191	22	155
192	11	156
193	26	157
194	10	158
195	24	158
196	11	158
197	23	161
198	2	162
199	7	163
200	7	164
201	7	165
202	9	166
203	12	167
204	11	168
205	1	170
206	8	171
207	13	172
208	26	173
209	26	174
210	28	175
211	29	176
212	29	177
213	28	178
214	29	179
215	28	180
216	26	181
217	21	182
218	22	183
219	8	185
220	1	186
221	13	187
222	14	187
223	9	188
224	6	190
225	15	191
226	4	192
227	2	193
228	1	194
229	1	196
230	1	197
231	20	198
232	22	199
233	20	200
234	27	201
235	1	202
236	4	203
237	20	204
238	4	205
239	1	206
240	1	207
241	23	208
242	22	208
243	10	209
244	11	209
245	15	211
246	19	212
247	9	213
248	17	214
249	23	215
250	3	216
251	4	216
252	15	217
253	6	218
254	10	219
255	11	219
256	21	222
257	12	223
258	6	220
259	10	221
260	11	221
261	20	224
262	20	225
263	3	226
264	6	227
265	6	228
266	6	229
267	4	230
268	7	231
\.


--
-- Name: locations_monsters_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Kieran
--

SELECT pg_catalog.setval('locations_monsters_id_seq', 268, true);


--
-- Data for Name: mix_items; Type: TABLE DATA; Schema: public; Owner: Kieran
--

COPY mix_items (id, mix_id, item_one_id, item_two_id, created_at, updated_at) FROM stdin;
1	1	13	16	2018-04-14 20:56:00.278186	2018-04-14 20:56:00.278186
2	1	13	17	2018-04-14 20:56:00.285338	2018-04-14 20:56:00.285338
3	1	13	24	2018-04-14 20:56:00.289923	2018-04-14 20:56:00.289923
4	1	13	31	2018-04-14 20:56:00.294503	2018-04-14 20:56:00.294503
5	1	13	32	2018-04-14 20:56:00.298924	2018-04-14 20:56:00.298924
6	1	14	16	2018-04-14 20:56:00.303511	2018-04-14 20:56:00.303511
7	1	14	17	2018-04-14 20:56:00.308098	2018-04-14 20:56:00.308098
8	1	33	15	2018-04-14 20:56:00.312505	2018-04-14 20:56:00.312505
9	1	33	16	2018-04-14 20:56:00.316701	2018-04-14 20:56:00.316701
10	1	33	17	2018-04-14 20:56:00.321183	2018-04-14 20:56:00.321183
11	1	33	24	2018-04-14 20:56:00.325426	2018-04-14 20:56:00.325426
12	1	33	31	2018-04-14 20:56:00.329817	2018-04-14 20:56:00.329817
13	1	33	32	2018-04-14 20:56:00.334195	2018-04-14 20:56:00.334195
14	1	33	39	2018-04-14 20:56:00.338874	2018-04-14 20:56:00.338874
15	1	33	41	2018-04-14 20:56:00.343318	2018-04-14 20:56:00.343318
16	1	33	46	2018-04-14 20:56:00.347692	2018-04-14 20:56:00.347692
17	1	33	61	2018-04-14 20:56:00.352094	2018-04-14 20:56:00.352094
18	1	33	70	2018-04-14 20:56:00.356916	2018-04-14 20:56:00.356916
19	1	33	72	2018-04-14 20:56:00.361209	2018-04-14 20:56:00.361209
20	1	33	74	2018-04-14 20:56:00.365632	2018-04-14 20:56:00.365632
21	1	33	78	2018-04-14 20:56:00.370171	2018-04-14 20:56:00.370171
22	1	33	84	2018-04-14 20:56:00.374756	2018-04-14 20:56:00.374756
23	1	33	85	2018-04-14 20:56:00.37909	2018-04-14 20:56:00.37909
24	1	33	86	2018-04-14 20:56:00.38351	2018-04-14 20:56:00.38351
25	1	33	88	2018-04-14 20:56:00.388677	2018-04-14 20:56:00.388677
26	1	33	89	2018-04-14 20:56:00.393133	2018-04-14 20:56:00.393133
27	1	33	91	2018-04-14 20:56:00.397398	2018-04-14 20:56:00.397398
28	1	33	95	2018-04-14 20:56:00.40167	2018-04-14 20:56:00.40167
29	2	23	15	2018-04-14 20:56:00.406427	2018-04-14 20:56:00.406427
30	2	23	16	2018-04-14 20:56:00.410809	2018-04-14 20:56:00.410809
31	2	23	17	2018-04-14 20:56:00.415147	2018-04-14 20:56:00.415147
32	2	23	24	2018-04-14 20:56:00.419839	2018-04-14 20:56:00.419839
33	2	23	39	2018-04-14 20:56:00.424549	2018-04-14 20:56:00.424549
34	2	23	41	2018-04-14 20:56:00.428909	2018-04-14 20:56:00.428909
35	2	23	46	2018-04-14 20:56:00.433387	2018-04-14 20:56:00.433387
36	2	23	61	2018-04-14 20:56:00.438749	2018-04-14 20:56:00.438749
37	2	23	70	2018-04-14 20:56:00.444344	2018-04-14 20:56:00.444344
38	2	23	72	2018-04-14 20:56:00.449005	2018-04-14 20:56:00.449005
39	2	23	74	2018-04-14 20:56:00.453317	2018-04-14 20:56:00.453317
40	2	23	78	2018-04-14 20:56:00.45821	2018-04-14 20:56:00.45821
41	2	23	84	2018-04-14 20:56:00.462806	2018-04-14 20:56:00.462806
42	2	23	85	2018-04-14 20:56:00.467418	2018-04-14 20:56:00.467418
43	2	23	86	2018-04-14 20:56:00.473973	2018-04-14 20:56:00.473973
44	2	23	88	2018-04-14 20:56:00.48022	2018-04-14 20:56:00.48022
45	2	23	89	2018-04-14 20:56:00.484764	2018-04-14 20:56:00.484764
46	2	23	91	2018-04-14 20:56:00.489357	2018-04-14 20:56:00.489357
47	2	23	95	2018-04-14 20:56:00.493788	2018-04-14 20:56:00.493788
48	2	34	15	2018-04-14 20:56:00.499905	2018-04-14 20:56:00.499905
49	2	34	16	2018-04-14 20:56:00.506211	2018-04-14 20:56:00.506211
50	2	34	17	2018-04-14 20:56:00.51197	2018-04-14 20:56:00.51197
51	2	34	24	2018-04-14 20:56:00.516532	2018-04-14 20:56:00.516532
52	2	34	31	2018-04-14 20:56:00.520803	2018-04-14 20:56:00.520803
53	2	34	32	2018-04-14 20:56:00.525614	2018-04-14 20:56:00.525614
54	2	34	39	2018-04-14 20:56:00.529923	2018-04-14 20:56:00.529923
55	2	34	41	2018-04-14 20:56:00.534697	2018-04-14 20:56:00.534697
56	2	34	46	2018-04-14 20:56:00.539393	2018-04-14 20:56:00.539393
57	2	34	61	2018-04-14 20:56:00.543895	2018-04-14 20:56:00.543895
58	2	34	70	2018-04-14 20:56:00.5482	2018-04-14 20:56:00.5482
59	2	34	72	2018-04-14 20:56:00.552874	2018-04-14 20:56:00.552874
60	2	34	74	2018-04-14 20:56:00.558403	2018-04-14 20:56:00.558403
61	2	34	78	2018-04-14 20:56:00.563037	2018-04-14 20:56:00.563037
62	2	34	84	2018-04-14 20:56:00.567685	2018-04-14 20:56:00.567685
63	2	34	85	2018-04-14 20:56:00.572377	2018-04-14 20:56:00.572377
64	2	34	86	2018-04-14 20:56:00.576967	2018-04-14 20:56:00.576967
65	2	34	88	2018-04-14 20:56:00.581296	2018-04-14 20:56:00.581296
66	2	34	89	2018-04-14 20:56:00.58588	2018-04-14 20:56:00.58588
67	2	34	91	2018-04-14 20:56:00.59055	2018-04-14 20:56:00.59055
68	2	34	95	2018-04-14 20:56:00.595095	2018-04-14 20:56:00.595095
69	3	82	6	2018-04-14 20:56:00.599727	2018-04-14 20:56:00.599727
70	3	82	22	2018-04-14 20:56:00.604255	2018-04-14 20:56:00.604255
71	3	82	38	2018-04-14 20:56:00.609259	2018-04-14 20:56:00.609259
72	3	82	65	2018-04-14 20:56:00.613821	2018-04-14 20:56:00.613821
73	3	82	71	2018-04-14 20:56:00.618461	2018-04-14 20:56:00.618461
74	3	82	106	2018-04-14 20:56:00.62327	2018-04-14 20:56:00.62327
75	3	82	107	2018-04-14 20:56:00.629541	2018-04-14 20:56:00.629541
76	3	82	110	2018-04-14 20:56:00.633851	2018-04-14 20:56:00.633851
77	3	82	111	2018-04-14 20:56:00.638383	2018-04-14 20:56:00.638383
78	4	1	36	2018-04-14 20:56:00.642962	2018-04-14 20:56:00.642962
79	4	1	40	2018-04-14 20:56:00.649845	2018-04-14 20:56:00.649845
80	4	2	46	2018-04-14 20:56:00.658033	2018-04-14 20:56:00.658033
81	4	2	70	2018-04-14 20:56:00.665295	2018-04-14 20:56:00.665295
82	4	2	84	2018-04-14 20:56:00.670506	2018-04-14 20:56:00.670506
83	4	4	46	2018-04-14 20:56:00.675373	2018-04-14 20:56:00.675373
84	4	4	70	2018-04-14 20:56:00.689895	2018-04-14 20:56:00.689895
85	4	4	84	2018-04-14 20:56:00.695155	2018-04-14 20:56:00.695155
86	4	5	36	2018-04-14 20:56:00.699859	2018-04-14 20:56:00.699859
87	4	5	40	2018-04-14 20:56:00.704347	2018-04-14 20:56:00.704347
88	4	8	36	2018-04-14 20:56:00.708997	2018-04-14 20:56:00.708997
89	4	8	40	2018-04-14 20:56:00.713479	2018-04-14 20:56:00.713479
90	4	15	2	2018-04-14 20:56:00.717948	2018-04-14 20:56:00.717948
91	4	15	4	2018-04-14 20:56:00.72279	2018-04-14 20:56:00.72279
92	4	15	60	2018-04-14 20:56:00.727455	2018-04-14 20:56:00.727455
93	4	15	64	2018-04-14 20:56:00.731907	2018-04-14 20:56:00.731907
94	4	15	77	2018-04-14 20:56:00.736196	2018-04-14 20:56:00.736196
95	4	15	80	2018-04-14 20:56:00.74067	2018-04-14 20:56:00.74067
96	4	24	2	2018-04-14 20:56:00.745044	2018-04-14 20:56:00.745044
97	4	24	4	2018-04-14 20:56:00.749387	2018-04-14 20:56:00.749387
98	4	24	60	2018-04-14 20:56:00.753899	2018-04-14 20:56:00.753899
99	4	24	71	2018-04-14 20:56:00.761196	2018-04-14 20:56:00.761196
100	4	24	77	2018-04-14 20:56:00.765409	2018-04-14 20:56:00.765409
101	4	25	36	2018-04-14 20:56:00.769682	2018-04-14 20:56:00.769682
102	4	25	40	2018-04-14 20:56:00.774026	2018-04-14 20:56:00.774026
103	4	30	36	2018-04-14 20:56:00.778603	2018-04-14 20:56:00.778603
104	4	30	40	2018-04-14 20:56:00.783002	2018-04-14 20:56:00.783002
105	4	36	71	2018-04-14 20:56:00.787484	2018-04-14 20:56:00.787484
106	4	39	2	2018-04-14 20:56:00.793655	2018-04-14 20:56:00.793655
107	4	39	4	2018-04-14 20:56:00.801099	2018-04-14 20:56:00.801099
108	4	39	60	2018-04-14 20:56:00.806969	2018-04-14 20:56:00.806969
109	4	39	64	2018-04-14 20:56:00.811414	2018-04-14 20:56:00.811414
110	4	39	77	2018-04-14 20:56:00.816067	2018-04-14 20:56:00.816067
111	4	39	80	2018-04-14 20:56:00.820673	2018-04-14 20:56:00.820673
112	4	40	2	2018-04-14 20:56:00.825788	2018-04-14 20:56:00.825788
113	4	40	4	2018-04-14 20:56:00.830882	2018-04-14 20:56:00.830882
114	4	40	15	2018-04-14 20:56:00.835081	2018-04-14 20:56:00.835081
115	4	40	16	2018-04-14 20:56:00.839494	2018-04-14 20:56:00.839494
116	4	40	17	2018-04-14 20:56:00.844073	2018-04-14 20:56:00.844073
117	4	40	24	2018-04-14 20:56:00.850121	2018-04-14 20:56:00.850121
118	4	40	39	2018-04-14 20:56:00.855689	2018-04-14 20:56:00.855689
119	4	40	46	2018-04-14 20:56:00.860285	2018-04-14 20:56:00.860285
120	4	40	60	2018-04-14 20:56:00.864656	2018-04-14 20:56:00.864656
121	4	40	61	2018-04-14 20:56:00.868883	2018-04-14 20:56:00.868883
122	4	40	70	2018-04-14 20:56:00.873619	2018-04-14 20:56:00.873619
123	4	40	71	2018-04-14 20:56:00.880549	2018-04-14 20:56:00.880549
124	4	40	72	2018-04-14 20:56:00.886181	2018-04-14 20:56:00.886181
125	4	40	74	2018-04-14 20:56:00.890819	2018-04-14 20:56:00.890819
126	4	40	77	2018-04-14 20:56:00.895166	2018-04-14 20:56:00.895166
127	4	40	78	2018-04-14 20:56:00.89957	2018-04-14 20:56:00.89957
128	4	40	84	2018-04-14 20:56:00.903829	2018-04-14 20:56:00.903829
129	4	40	85	2018-04-14 20:56:00.908527	2018-04-14 20:56:00.908527
130	4	40	86	2018-04-14 20:56:00.913052	2018-04-14 20:56:00.913052
131	4	40	88	2018-04-14 20:56:00.917607	2018-04-14 20:56:00.917607
132	4	40	89	2018-04-14 20:56:00.921959	2018-04-14 20:56:00.921959
133	4	40	91	2018-04-14 20:56:00.927555	2018-04-14 20:56:00.927555
134	4	40	95	2018-04-14 20:56:00.931666	2018-04-14 20:56:00.931666
135	4	44	36	2018-04-14 20:56:00.936196	2018-04-14 20:56:00.936196
136	4	44	40	2018-04-14 20:56:00.940657	2018-04-14 20:56:00.940657
137	4	59	36	2018-04-14 20:56:00.945044	2018-04-14 20:56:00.945044
138	4	59	40	2018-04-14 20:56:00.949635	2018-04-14 20:56:00.949635
139	4	60	46	2018-04-14 20:56:00.954232	2018-04-14 20:56:00.954232
140	4	60	70	2018-04-14 20:56:00.958842	2018-04-14 20:56:00.958842
141	4	60	84	2018-04-14 20:56:00.963295	2018-04-14 20:56:00.963295
142	4	72	2	2018-04-14 20:56:00.969826	2018-04-14 20:56:00.969826
143	4	72	4	2018-04-14 20:56:00.974344	2018-04-14 20:56:00.974344
144	4	72	60	2018-04-14 20:56:00.97895	2018-04-14 20:56:00.97895
145	4	72	64	2018-04-14 20:56:00.983577	2018-04-14 20:56:00.983577
146	4	72	77	2018-04-14 20:56:00.98815	2018-04-14 20:56:00.98815
147	4	72	80	2018-04-14 20:56:00.993258	2018-04-14 20:56:00.993258
148	4	74	2	2018-04-14 20:56:00.997853	2018-04-14 20:56:00.997853
149	4	74	4	2018-04-14 20:56:01.002141	2018-04-14 20:56:01.002141
150	4	74	60	2018-04-14 20:56:01.006602	2018-04-14 20:56:01.006602
151	4	74	64	2018-04-14 20:56:01.011103	2018-04-14 20:56:01.011103
152	4	74	77	2018-04-14 20:56:01.015547	2018-04-14 20:56:01.015547
153	4	74	80	2018-04-14 20:56:01.020553	2018-04-14 20:56:01.020553
154	4	76	36	2018-04-14 20:56:01.025502	2018-04-14 20:56:01.025502
155	4	76	40	2018-04-14 20:56:01.029894	2018-04-14 20:56:01.029894
156	4	77	46	2018-04-14 20:56:01.034316	2018-04-14 20:56:01.034316
157	4	77	70	2018-04-14 20:56:01.039108	2018-04-14 20:56:01.039108
158	4	77	84	2018-04-14 20:56:01.04371	2018-04-14 20:56:01.04371
159	4	79	36	2018-04-14 20:56:01.04824	2018-04-14 20:56:01.04824
160	4	79	40	2018-04-14 20:56:01.052856	2018-04-14 20:56:01.052856
161	4	85	2	2018-04-14 20:56:01.058788	2018-04-14 20:56:01.058788
162	4	85	4	2018-04-14 20:56:01.063452	2018-04-14 20:56:01.063452
163	4	85	60	2018-04-14 20:56:01.068487	2018-04-14 20:56:01.068487
164	4	85	71	2018-04-14 20:56:01.073303	2018-04-14 20:56:01.073303
165	4	85	77	2018-04-14 20:56:01.079688	2018-04-14 20:56:01.079688
166	4	86	2	2018-04-14 20:56:01.084169	2018-04-14 20:56:01.084169
167	4	86	4	2018-04-14 20:56:01.088811	2018-04-14 20:56:01.088811
168	4	86	60	2018-04-14 20:56:01.103685	2018-04-14 20:56:01.103685
169	4	86	64	2018-04-14 20:56:01.109397	2018-04-14 20:56:01.109397
170	4	86	77	2018-04-14 20:56:01.114034	2018-04-14 20:56:01.114034
171	4	86	80	2018-04-14 20:56:01.120334	2018-04-14 20:56:01.120334
172	4	88	2	2018-04-14 20:56:01.126019	2018-04-14 20:56:01.126019
173	4	88	4	2018-04-14 20:56:01.13057	2018-04-14 20:56:01.13057
174	4	88	60	2018-04-14 20:56:01.137327	2018-04-14 20:56:01.137327
175	4	88	71	2018-04-14 20:56:01.142996	2018-04-14 20:56:01.142996
176	4	88	77	2018-04-14 20:56:01.148925	2018-04-14 20:56:01.148925
177	4	89	2	2018-04-14 20:56:01.153495	2018-04-14 20:56:01.153495
178	4	89	4	2018-04-14 20:56:01.157908	2018-04-14 20:56:01.157908
179	4	89	60	2018-04-14 20:56:01.162596	2018-04-14 20:56:01.162596
180	4	89	71	2018-04-14 20:56:01.168583	2018-04-14 20:56:01.168583
181	4	89	77	2018-04-14 20:56:01.173071	2018-04-14 20:56:01.173071
182	4	90	36	2018-04-14 20:56:01.178401	2018-04-14 20:56:01.178401
183	4	90	40	2018-04-14 20:56:01.186036	2018-04-14 20:56:01.186036
184	4	93	36	2018-04-14 20:56:01.192246	2018-04-14 20:56:01.192246
185	4	93	40	2018-04-14 20:56:01.196977	2018-04-14 20:56:01.196977
186	5	10	33	2018-04-14 20:56:01.201397	2018-04-14 20:56:01.201397
187	5	13	12	2018-04-14 20:56:01.205749	2018-04-14 20:56:01.205749
188	5	13	15	2018-04-14 20:56:01.210391	2018-04-14 20:56:01.210391
189	5	13	39	2018-04-14 20:56:01.214904	2018-04-14 20:56:01.214904
190	5	13	41	2018-04-14 20:56:01.219199	2018-04-14 20:56:01.219199
191	5	13	46	2018-04-14 20:56:01.223629	2018-04-14 20:56:01.223629
192	5	13	61	2018-04-14 20:56:01.231699	2018-04-14 20:56:01.231699
193	5	13	70	2018-04-14 20:56:01.236639	2018-04-14 20:56:01.236639
194	5	13	72	2018-04-14 20:56:01.241184	2018-04-14 20:56:01.241184
195	5	13	74	2018-04-14 20:56:01.245867	2018-04-14 20:56:01.245867
196	5	13	78	2018-04-14 20:56:01.250438	2018-04-14 20:56:01.250438
197	5	13	84	2018-04-14 20:56:01.254776	2018-04-14 20:56:01.254776
198	5	13	85	2018-04-14 20:56:01.260106	2018-04-14 20:56:01.260106
199	5	13	86	2018-04-14 20:56:01.267739	2018-04-14 20:56:01.267739
200	5	13	87	2018-04-14 20:56:01.27277	2018-04-14 20:56:01.27277
201	5	13	88	2018-04-14 20:56:01.279667	2018-04-14 20:56:01.279667
202	5	13	89	2018-04-14 20:56:01.285172	2018-04-14 20:56:01.285172
203	5	13	91	2018-04-14 20:56:01.289627	2018-04-14 20:56:01.289627
204	5	13	92	2018-04-14 20:56:01.294061	2018-04-14 20:56:01.294061
205	5	13	95	2018-04-14 20:56:01.298514	2018-04-14 20:56:01.298514
206	5	13	109	2018-04-14 20:56:01.302943	2018-04-14 20:56:01.302943
207	5	14	15	2018-04-14 20:56:01.307434	2018-04-14 20:56:01.307434
208	5	14	24	2018-04-14 20:56:01.312077	2018-04-14 20:56:01.312077
209	5	14	31	2018-04-14 20:56:01.316719	2018-04-14 20:56:01.316719
210	5	14	32	2018-04-14 20:56:01.321219	2018-04-14 20:56:01.321219
211	5	14	39	2018-04-14 20:56:01.326708	2018-04-14 20:56:01.326708
212	5	14	41	2018-04-14 20:56:01.333236	2018-04-14 20:56:01.333236
213	5	14	46	2018-04-14 20:56:01.3388	2018-04-14 20:56:01.3388
214	5	14	61	2018-04-14 20:56:01.343219	2018-04-14 20:56:01.343219
215	5	14	70	2018-04-14 20:56:01.350609	2018-04-14 20:56:01.350609
216	5	14	72	2018-04-14 20:56:01.355872	2018-04-14 20:56:01.355872
217	5	14	74	2018-04-14 20:56:01.360349	2018-04-14 20:56:01.360349
218	5	14	78	2018-04-14 20:56:01.364817	2018-04-14 20:56:01.364817
219	5	14	84	2018-04-14 20:56:01.370232	2018-04-14 20:56:01.370232
220	5	14	85	2018-04-14 20:56:01.374643	2018-04-14 20:56:01.374643
221	5	14	86	2018-04-14 20:56:01.379324	2018-04-14 20:56:01.379324
222	5	14	88	2018-04-14 20:56:01.384142	2018-04-14 20:56:01.384142
223	5	14	89	2018-04-14 20:56:01.388747	2018-04-14 20:56:01.388747
224	5	14	91	2018-04-14 20:56:01.393475	2018-04-14 20:56:01.393475
225	5	14	95	2018-04-14 20:56:01.398461	2018-04-14 20:56:01.398461
226	5	33	2	2018-04-14 20:56:01.402983	2018-04-14 20:56:01.402983
227	5	33	4	2018-04-14 20:56:01.407554	2018-04-14 20:56:01.407554
228	5	33	60	2018-04-14 20:56:01.411781	2018-04-14 20:56:01.411781
229	5	33	77	2018-04-14 20:56:01.416315	2018-04-14 20:56:01.416315
230	6	13	3	2018-04-14 20:56:01.421048	2018-04-14 20:56:01.421048
231	6	13	20	2018-04-14 20:56:01.425378	2018-04-14 20:56:01.425378
232	6	13	29	2018-04-14 20:56:01.429857	2018-04-14 20:56:01.429857
233	6	13	37	2018-04-14 20:56:01.437599	2018-04-14 20:56:01.437599
234	6	13	45	2018-04-14 20:56:01.444816	2018-04-14 20:56:01.444816
235	6	13	54	2018-04-14 20:56:01.453458	2018-04-14 20:56:01.453458
236	6	13	55	2018-04-14 20:56:01.46021	2018-04-14 20:56:01.46021
237	6	13	56	2018-04-14 20:56:01.465969	2018-04-14 20:56:01.465969
238	6	13	57	2018-04-14 20:56:01.470706	2018-04-14 20:56:01.470706
239	6	13	58	2018-04-14 20:56:01.475081	2018-04-14 20:56:01.475081
240	6	13	65	2018-04-14 20:56:01.47986	2018-04-14 20:56:01.47986
241	6	13	69	2018-04-14 20:56:01.484842	2018-04-14 20:56:01.484842
242	6	13	81	2018-04-14 20:56:01.489842	2018-04-14 20:56:01.489842
243	6	13	94	2018-04-14 20:56:01.498218	2018-04-14 20:56:01.498218
244	6	13	99	2018-04-14 20:56:01.505618	2018-04-14 20:56:01.505618
245	6	13	101	2018-04-14 20:56:01.523184	2018-04-14 20:56:01.523184
246	6	13	107	2018-04-14 20:56:01.528123	2018-04-14 20:56:01.528123
247	6	14	3	2018-04-14 20:56:01.53254	2018-04-14 20:56:01.53254
248	6	14	6	2018-04-14 20:56:01.537263	2018-04-14 20:56:01.537263
249	6	14	20	2018-04-14 20:56:01.545287	2018-04-14 20:56:01.545287
250	6	14	22	2018-04-14 20:56:01.55324	2018-04-14 20:56:01.55324
251	6	14	29	2018-04-14 20:56:01.560499	2018-04-14 20:56:01.560499
252	6	14	37	2018-04-14 20:56:01.569371	2018-04-14 20:56:01.569371
253	6	14	38	2018-04-14 20:56:01.575481	2018-04-14 20:56:01.575481
254	6	14	45	2018-04-14 20:56:01.582635	2018-04-14 20:56:01.582635
255	6	14	55	2018-04-14 20:56:01.589389	2018-04-14 20:56:01.589389
256	6	14	56	2018-04-14 20:56:01.594103	2018-04-14 20:56:01.594103
257	6	14	57	2018-04-14 20:56:01.598541	2018-04-14 20:56:01.598541
258	6	14	58	2018-04-14 20:56:01.602884	2018-04-14 20:56:01.602884
259	6	14	65	2018-04-14 20:56:01.6073	2018-04-14 20:56:01.6073
260	6	14	69	2018-04-14 20:56:01.613333	2018-04-14 20:56:01.613333
261	6	14	71	2018-04-14 20:56:01.621432	2018-04-14 20:56:01.621432
262	6	14	81	2018-04-14 20:56:01.628164	2018-04-14 20:56:01.628164
263	6	14	94	2018-04-14 20:56:01.633582	2018-04-14 20:56:01.633582
264	6	14	99	2018-04-14 20:56:01.637973	2018-04-14 20:56:01.637973
265	6	14	101	2018-04-14 20:56:01.642492	2018-04-14 20:56:01.642492
266	6	14	106	2018-04-14 20:56:01.648141	2018-04-14 20:56:01.648141
267	6	14	107	2018-04-14 20:56:01.652916	2018-04-14 20:56:01.652916
268	6	14	110	2018-04-14 20:56:01.660822	2018-04-14 20:56:01.660822
269	6	14	111	2018-04-14 20:56:01.667217	2018-04-14 20:56:01.667217
270	6	33	3	2018-04-14 20:56:01.671881	2018-04-14 20:56:01.671881
271	6	33	12	2018-04-14 20:56:01.676093	2018-04-14 20:56:01.676093
272	6	33	20	2018-04-14 20:56:01.68042	2018-04-14 20:56:01.68042
273	6	33	29	2018-04-14 20:56:01.685157	2018-04-14 20:56:01.685157
274	6	33	37	2018-04-14 20:56:01.689738	2018-04-14 20:56:01.689738
275	6	33	45	2018-04-14 20:56:01.694115	2018-04-14 20:56:01.694115
276	6	33	53	2018-04-14 20:56:01.698536	2018-04-14 20:56:01.698536
277	6	33	54	2018-04-14 20:56:01.703037	2018-04-14 20:56:01.703037
278	6	33	55	2018-04-14 20:56:01.707592	2018-04-14 20:56:01.707592
279	6	33	56	2018-04-14 20:56:01.712002	2018-04-14 20:56:01.712002
280	6	33	57	2018-04-14 20:56:01.716612	2018-04-14 20:56:01.716612
281	6	33	58	2018-04-14 20:56:01.721058	2018-04-14 20:56:01.721058
282	6	33	65	2018-04-14 20:56:01.725402	2018-04-14 20:56:01.725402
283	6	33	69	2018-04-14 20:56:01.733001	2018-04-14 20:56:01.733001
284	6	33	81	2018-04-14 20:56:01.738325	2018-04-14 20:56:01.738325
285	6	33	87	2018-04-14 20:56:01.742718	2018-04-14 20:56:01.742718
286	6	33	92	2018-04-14 20:56:01.74991	2018-04-14 20:56:01.74991
287	6	33	94	2018-04-14 20:56:01.754323	2018-04-14 20:56:01.754323
288	6	33	99	2018-04-14 20:56:01.758719	2018-04-14 20:56:01.758719
289	6	33	101	2018-04-14 20:56:01.763177	2018-04-14 20:56:01.763177
290	6	33	107	2018-04-14 20:56:01.767598	2018-04-14 20:56:01.767598
291	6	33	109	2018-04-14 20:56:01.771919	2018-04-14 20:56:01.771919
292	7	9	49	2018-04-14 20:56:01.776322	2018-04-14 20:56:01.776322
293	7	9	108	2018-04-14 20:56:01.780834	2018-04-14 20:56:01.780834
294	7	10	15	2018-04-14 20:56:01.785176	2018-04-14 20:56:01.785176
295	7	10	24	2018-04-14 20:56:01.789602	2018-04-14 20:56:01.789602
296	7	10	36	2018-04-14 20:56:01.794023	2018-04-14 20:56:01.794023
297	7	10	39	2018-04-14 20:56:01.799648	2018-04-14 20:56:01.799648
298	7	10	72	2018-04-14 20:56:01.804083	2018-04-14 20:56:01.804083
299	7	10	74	2018-04-14 20:56:01.808576	2018-04-14 20:56:01.808576
300	7	10	85	2018-04-14 20:56:01.814919	2018-04-14 20:56:01.814919
301	7	10	86	2018-04-14 20:56:01.82058	2018-04-14 20:56:01.82058
302	7	10	88	2018-04-14 20:56:01.825336	2018-04-14 20:56:01.825336
303	7	10	89	2018-04-14 20:56:01.829892	2018-04-14 20:56:01.829892
304	7	11	15	2018-04-14 20:56:01.834567	2018-04-14 20:56:01.834567
305	7	11	16	2018-04-14 20:56:01.840264	2018-04-14 20:56:01.840264
306	7	11	17	2018-04-14 20:56:01.849497	2018-04-14 20:56:01.849497
307	7	11	39	2018-04-14 20:56:01.857253	2018-04-14 20:56:01.857253
308	7	11	41	2018-04-14 20:56:01.864068	2018-04-14 20:56:01.864068
309	7	11	48	2018-04-14 20:56:01.868722	2018-04-14 20:56:01.868722
310	7	11	52	2018-04-14 20:56:01.873232	2018-04-14 20:56:01.873232
311	7	11	61	2018-04-14 20:56:01.877698	2018-04-14 20:56:01.877698
312	7	11	72	2018-04-14 20:56:01.882762	2018-04-14 20:56:01.882762
313	7	11	74	2018-04-14 20:56:01.887478	2018-04-14 20:56:01.887478
314	7	11	78	2018-04-14 20:56:01.891899	2018-04-14 20:56:01.891899
315	7	11	86	2018-04-14 20:56:01.896692	2018-04-14 20:56:01.896692
316	7	11	91	2018-04-14 20:56:01.901234	2018-04-14 20:56:01.901234
317	7	11	95	2018-04-14 20:56:01.910028	2018-04-14 20:56:01.910028
318	7	11	98	2018-04-14 20:56:01.914773	2018-04-14 20:56:01.914773
319	7	12	46	2018-04-14 20:56:01.919581	2018-04-14 20:56:01.919581
320	7	12	70	2018-04-14 20:56:01.923872	2018-04-14 20:56:01.923872
321	7	12	84	2018-04-14 20:56:01.928343	2018-04-14 20:56:01.928343
322	7	13	49	2018-04-14 20:56:01.935454	2018-04-14 20:56:01.935454
323	7	13	108	2018-04-14 20:56:01.943551	2018-04-14 20:56:01.943551
324	7	15	12	2018-04-14 20:56:01.949842	2018-04-14 20:56:01.949842
325	7	15	18	2018-04-14 20:56:01.955255	2018-04-14 20:56:01.955255
326	7	15	31	2018-04-14 20:56:01.959756	2018-04-14 20:56:01.959756
327	7	15	32	2018-04-14 20:56:01.964275	2018-04-14 20:56:01.964275
328	7	15	53	2018-04-14 20:56:01.968864	2018-04-14 20:56:01.968864
329	7	15	54	2018-04-14 20:56:01.97341	2018-04-14 20:56:01.97341
330	7	15	87	2018-04-14 20:56:01.977948	2018-04-14 20:56:01.977948
331	7	15	92	2018-04-14 20:56:01.982641	2018-04-14 20:56:01.982641
332	7	15	103	2018-04-14 20:56:01.987198	2018-04-14 20:56:01.987198
333	7	15	109	2018-04-14 20:56:01.991552	2018-04-14 20:56:01.991552
334	7	18	46	2018-04-14 20:56:01.99605	2018-04-14 20:56:01.99605
335	7	18	70	2018-04-14 20:56:02.001443	2018-04-14 20:56:02.001443
336	7	18	84	2018-04-14 20:56:02.009172	2018-04-14 20:56:02.009172
337	7	24	12	2018-04-14 20:56:02.014377	2018-04-14 20:56:02.014377
338	7	24	18	2018-04-14 20:56:02.020148	2018-04-14 20:56:02.020148
339	7	24	31	2018-04-14 20:56:02.02518	2018-04-14 20:56:02.02518
340	7	24	32	2018-04-14 20:56:02.02967	2018-04-14 20:56:02.02967
341	7	24	53	2018-04-14 20:56:02.03447	2018-04-14 20:56:02.03447
342	7	24	54	2018-04-14 20:56:02.038939	2018-04-14 20:56:02.038939
343	7	24	87	2018-04-14 20:56:02.043281	2018-04-14 20:56:02.043281
344	7	24	92	2018-04-14 20:56:02.047867	2018-04-14 20:56:02.047867
345	7	24	103	2018-04-14 20:56:02.052424	2018-04-14 20:56:02.052424
346	7	24	109	2018-04-14 20:56:02.056763	2018-04-14 20:56:02.056763
347	7	31	16	2018-04-14 20:56:02.061155	2018-04-14 20:56:02.061155
348	7	31	17	2018-04-14 20:56:02.066894	2018-04-14 20:56:02.066894
349	7	31	31	2018-04-14 20:56:02.07148	2018-04-14 20:56:02.07148
350	7	31	32	2018-04-14 20:56:02.079411	2018-04-14 20:56:02.079411
351	7	31	41	2018-04-14 20:56:02.085909	2018-04-14 20:56:02.085909
352	7	31	46	2018-04-14 20:56:02.090539	2018-04-14 20:56:02.090539
353	7	31	48	2018-04-14 20:56:02.094913	2018-04-14 20:56:02.094913
354	7	31	52	2018-04-14 20:56:02.099759	2018-04-14 20:56:02.099759
355	7	31	61	2018-04-14 20:56:02.104273	2018-04-14 20:56:02.104273
356	7	31	70	2018-04-14 20:56:02.109166	2018-04-14 20:56:02.109166
357	7	31	78	2018-04-14 20:56:02.113594	2018-04-14 20:56:02.113594
358	7	31	84	2018-04-14 20:56:02.119166	2018-04-14 20:56:02.119166
359	7	31	91	2018-04-14 20:56:02.123511	2018-04-14 20:56:02.123511
360	7	31	95	2018-04-14 20:56:02.127798	2018-04-14 20:56:02.127798
361	7	31	98	2018-04-14 20:56:02.132321	2018-04-14 20:56:02.132321
362	7	31	103	2018-04-14 20:56:02.13688	2018-04-14 20:56:02.13688
363	7	32	16	2018-04-14 20:56:02.1413	2018-04-14 20:56:02.1413
364	7	32	17	2018-04-14 20:56:02.145783	2018-04-14 20:56:02.145783
365	7	32	41	2018-04-14 20:56:02.151141	2018-04-14 20:56:02.151141
366	7	32	46	2018-04-14 20:56:02.157984	2018-04-14 20:56:02.157984
367	7	32	48	2018-04-14 20:56:02.162385	2018-04-14 20:56:02.162385
368	7	32	52	2018-04-14 20:56:02.167332	2018-04-14 20:56:02.167332
369	7	32	61	2018-04-14 20:56:02.171623	2018-04-14 20:56:02.171623
370	7	32	70	2018-04-14 20:56:02.176127	2018-04-14 20:56:02.176127
371	7	32	78	2018-04-14 20:56:02.18084	2018-04-14 20:56:02.18084
372	7	32	84	2018-04-14 20:56:02.185376	2018-04-14 20:56:02.185376
373	7	32	91	2018-04-14 20:56:02.189955	2018-04-14 20:56:02.189955
374	7	32	95	2018-04-14 20:56:02.194367	2018-04-14 20:56:02.194367
375	7	32	98	2018-04-14 20:56:02.201398	2018-04-14 20:56:02.201398
376	7	32	103	2018-04-14 20:56:02.206988	2018-04-14 20:56:02.206988
377	7	36	12	2018-04-14 20:56:02.211431	2018-04-14 20:56:02.211431
378	7	36	18	2018-04-14 20:56:02.216294	2018-04-14 20:56:02.216294
379	7	36	53	2018-04-14 20:56:02.221107	2018-04-14 20:56:02.221107
380	7	36	54	2018-04-14 20:56:02.225798	2018-04-14 20:56:02.225798
381	7	36	87	2018-04-14 20:56:02.230446	2018-04-14 20:56:02.230446
382	7	36	92	2018-04-14 20:56:02.235063	2018-04-14 20:56:02.235063
383	7	36	109	2018-04-14 20:56:02.247102	2018-04-14 20:56:02.247102
384	7	39	12	2018-04-14 20:56:02.25268	2018-04-14 20:56:02.25268
385	7	39	18	2018-04-14 20:56:02.257663	2018-04-14 20:56:02.257663
386	7	39	31	2018-04-14 20:56:02.262314	2018-04-14 20:56:02.262314
387	7	39	32	2018-04-14 20:56:02.270627	2018-04-14 20:56:02.270627
388	7	39	53	2018-04-14 20:56:02.276191	2018-04-14 20:56:02.276191
389	7	39	54	2018-04-14 20:56:02.281204	2018-04-14 20:56:02.281204
390	7	39	87	2018-04-14 20:56:02.286537	2018-04-14 20:56:02.286537
391	7	39	92	2018-04-14 20:56:02.291185	2018-04-14 20:56:02.291185
392	7	39	103	2018-04-14 20:56:02.299477	2018-04-14 20:56:02.299477
393	7	39	109	2018-04-14 20:56:02.306871	2018-04-14 20:56:02.306871
394	7	40	55	2018-04-14 20:56:02.313077	2018-04-14 20:56:02.313077
395	7	40	56	2018-04-14 20:56:02.318573	2018-04-14 20:56:02.318573
396	7	40	65	2018-04-14 20:56:02.323452	2018-04-14 20:56:02.323452
397	7	47	23	2018-04-14 20:56:02.328125	2018-04-14 20:56:02.328125
398	7	47	49	2018-04-14 20:56:02.334895	2018-04-14 20:56:02.334895
399	7	47	50	2018-04-14 20:56:02.339766	2018-04-14 20:56:02.339766
400	7	47	108	2018-04-14 20:56:02.344455	2018-04-14 20:56:02.344455
401	7	53	46	2018-04-14 20:56:02.349333	2018-04-14 20:56:02.349333
402	7	53	70	2018-04-14 20:56:02.354099	2018-04-14 20:56:02.354099
403	7	53	84	2018-04-14 20:56:02.358752	2018-04-14 20:56:02.358752
404	7	54	46	2018-04-14 20:56:02.363698	2018-04-14 20:56:02.363698
405	7	54	70	2018-04-14 20:56:02.369849	2018-04-14 20:56:02.369849
406	7	54	84	2018-04-14 20:56:02.374737	2018-04-14 20:56:02.374737
407	7	72	12	2018-04-14 20:56:02.379522	2018-04-14 20:56:02.379522
408	7	72	18	2018-04-14 20:56:02.384434	2018-04-14 20:56:02.384434
409	7	72	31	2018-04-14 20:56:02.389851	2018-04-14 20:56:02.389851
410	7	72	32	2018-04-14 20:56:02.394793	2018-04-14 20:56:02.394793
411	7	72	53	2018-04-14 20:56:02.401265	2018-04-14 20:56:02.401265
412	7	72	54	2018-04-14 20:56:02.406545	2018-04-14 20:56:02.406545
413	7	72	72	2018-04-14 20:56:02.41241	2018-04-14 20:56:02.41241
414	7	72	87	2018-04-14 20:56:02.419369	2018-04-14 20:56:02.419369
415	7	72	92	2018-04-14 20:56:02.430792	2018-04-14 20:56:02.430792
416	7	72	103	2018-04-14 20:56:02.438403	2018-04-14 20:56:02.438403
417	7	72	109	2018-04-14 20:56:02.44557	2018-04-14 20:56:02.44557
418	7	74	12	2018-04-14 20:56:02.453163	2018-04-14 20:56:02.453163
419	7	74	18	2018-04-14 20:56:02.459712	2018-04-14 20:56:02.459712
420	7	74	31	2018-04-14 20:56:02.470002	2018-04-14 20:56:02.470002
421	7	74	32	2018-04-14 20:56:02.478511	2018-04-14 20:56:02.478511
422	7	74	53	2018-04-14 20:56:02.485476	2018-04-14 20:56:02.485476
423	7	74	54	2018-04-14 20:56:02.496543	2018-04-14 20:56:02.496543
424	7	74	87	2018-04-14 20:56:02.5043	2018-04-14 20:56:02.5043
425	7	74	92	2018-04-14 20:56:02.51098	2018-04-14 20:56:02.51098
426	7	74	103	2018-04-14 20:56:02.517603	2018-04-14 20:56:02.517603
427	7	74	109	2018-04-14 20:56:02.52431	2018-04-14 20:56:02.52431
428	7	82	103	2018-04-14 20:56:02.531124	2018-04-14 20:56:02.531124
429	7	83	15	2018-04-14 20:56:02.539491	2018-04-14 20:56:02.539491
430	7	83	16	2018-04-14 20:56:02.545777	2018-04-14 20:56:02.545777
431	7	83	17	2018-04-14 20:56:02.55308	2018-04-14 20:56:02.55308
432	7	83	39	2018-04-14 20:56:02.563777	2018-04-14 20:56:02.563777
433	7	83	41	2018-04-14 20:56:02.57189	2018-04-14 20:56:02.57189
434	7	83	48	2018-04-14 20:56:02.576376	2018-04-14 20:56:02.576376
435	7	83	52	2018-04-14 20:56:02.580594	2018-04-14 20:56:02.580594
436	7	83	61	2018-04-14 20:56:02.587415	2018-04-14 20:56:02.587415
437	7	83	72	2018-04-14 20:56:02.592599	2018-04-14 20:56:02.592599
438	7	83	74	2018-04-14 20:56:02.596925	2018-04-14 20:56:02.596925
439	7	83	78	2018-04-14 20:56:02.60121	2018-04-14 20:56:02.60121
440	7	83	86	2018-04-14 20:56:02.605547	2018-04-14 20:56:02.605547
441	7	83	91	2018-04-14 20:56:02.609484	2018-04-14 20:56:02.609484
442	7	83	95	2018-04-14 20:56:02.617337	2018-04-14 20:56:02.617337
443	7	83	98	2018-04-14 20:56:02.624726	2018-04-14 20:56:02.624726
444	7	85	12	2018-04-14 20:56:02.633894	2018-04-14 20:56:02.633894
445	7	85	18	2018-04-14 20:56:02.64012	2018-04-14 20:56:02.64012
446	7	85	31	2018-04-14 20:56:02.64446	2018-04-14 20:56:02.64446
447	7	85	32	2018-04-14 20:56:02.649069	2018-04-14 20:56:02.649069
448	7	85	53	2018-04-14 20:56:02.653668	2018-04-14 20:56:02.653668
449	7	85	54	2018-04-14 20:56:02.661702	2018-04-14 20:56:02.661702
450	7	85	87	2018-04-14 20:56:02.669164	2018-04-14 20:56:02.669164
451	7	85	92	2018-04-14 20:56:02.675689	2018-04-14 20:56:02.675689
452	7	85	103	2018-04-14 20:56:02.680083	2018-04-14 20:56:02.680083
453	7	85	109	2018-04-14 20:56:02.684515	2018-04-14 20:56:02.684515
454	7	86	12	2018-04-14 20:56:02.689233	2018-04-14 20:56:02.689233
455	7	86	18	2018-04-14 20:56:02.69382	2018-04-14 20:56:02.69382
456	7	86	31	2018-04-14 20:56:02.698209	2018-04-14 20:56:02.698209
457	7	86	32	2018-04-14 20:56:02.705802	2018-04-14 20:56:02.705802
458	7	86	53	2018-04-14 20:56:02.713543	2018-04-14 20:56:02.713543
459	7	86	54	2018-04-14 20:56:02.719907	2018-04-14 20:56:02.719907
460	7	86	87	2018-04-14 20:56:02.724402	2018-04-14 20:56:02.724402
461	7	86	92	2018-04-14 20:56:02.729038	2018-04-14 20:56:02.729038
462	7	86	103	2018-04-14 20:56:02.73361	2018-04-14 20:56:02.73361
463	7	86	109	2018-04-14 20:56:02.738507	2018-04-14 20:56:02.738507
464	7	87	46	2018-04-14 20:56:02.743309	2018-04-14 20:56:02.743309
465	7	87	70	2018-04-14 20:56:02.74838	2018-04-14 20:56:02.74838
466	7	87	84	2018-04-14 20:56:02.753723	2018-04-14 20:56:02.753723
467	7	88	12	2018-04-14 20:56:02.758887	2018-04-14 20:56:02.758887
468	7	88	18	2018-04-14 20:56:02.773444	2018-04-14 20:56:02.773444
469	7	88	31	2018-04-14 20:56:02.786668	2018-04-14 20:56:02.786668
470	7	88	32	2018-04-14 20:56:02.792342	2018-04-14 20:56:02.792342
471	7	88	53	2018-04-14 20:56:02.79712	2018-04-14 20:56:02.79712
472	7	88	54	2018-04-14 20:56:02.801625	2018-04-14 20:56:02.801625
473	7	88	87	2018-04-14 20:56:02.806034	2018-04-14 20:56:02.806034
474	7	88	92	2018-04-14 20:56:02.810457	2018-04-14 20:56:02.810457
475	7	88	103	2018-04-14 20:56:02.81484	2018-04-14 20:56:02.81484
476	7	88	109	2018-04-14 20:56:02.819717	2018-04-14 20:56:02.819717
477	7	89	12	2018-04-14 20:56:02.824278	2018-04-14 20:56:02.824278
478	7	89	18	2018-04-14 20:56:02.828953	2018-04-14 20:56:02.828953
479	7	89	31	2018-04-14 20:56:02.833691	2018-04-14 20:56:02.833691
480	7	89	32	2018-04-14 20:56:02.838342	2018-04-14 20:56:02.838342
481	7	89	53	2018-04-14 20:56:02.843047	2018-04-14 20:56:02.843047
482	7	89	54	2018-04-14 20:56:02.847548	2018-04-14 20:56:02.847548
483	7	89	87	2018-04-14 20:56:02.852259	2018-04-14 20:56:02.852259
484	7	89	92	2018-04-14 20:56:02.856738	2018-04-14 20:56:02.856738
485	7	89	103	2018-04-14 20:56:02.861103	2018-04-14 20:56:02.861103
486	7	89	109	2018-04-14 20:56:02.865434	2018-04-14 20:56:02.865434
487	7	92	46	2018-04-14 20:56:02.874924	2018-04-14 20:56:02.874924
488	7	92	70	2018-04-14 20:56:02.881625	2018-04-14 20:56:02.881625
489	7	92	84	2018-04-14 20:56:02.886576	2018-04-14 20:56:02.886576
490	7	100	2	2018-04-14 20:56:02.891191	2018-04-14 20:56:02.891191
491	7	100	4	2018-04-14 20:56:02.895611	2018-04-14 20:56:02.895611
492	7	100	60	2018-04-14 20:56:02.900379	2018-04-14 20:56:02.900379
493	7	100	77	2018-04-14 20:56:02.905141	2018-04-14 20:56:02.905141
494	7	109	46	2018-04-14 20:56:02.909667	2018-04-14 20:56:02.909667
495	7	109	70	2018-04-14 20:56:02.91404	2018-04-14 20:56:02.91404
496	7	109	84	2018-04-14 20:56:02.918564	2018-04-14 20:56:02.918564
497	8	3	46	2018-04-14 20:56:02.923105	2018-04-14 20:56:02.923105
498	8	3	70	2018-04-14 20:56:02.927541	2018-04-14 20:56:02.927541
499	8	3	84	2018-04-14 20:56:02.931933	2018-04-14 20:56:02.931933
500	8	10	31	2018-04-14 20:56:02.93656	2018-04-14 20:56:02.93656
501	8	10	32	2018-04-14 20:56:02.940939	2018-04-14 20:56:02.940939
502	8	11	31	2018-04-14 20:56:02.945323	2018-04-14 20:56:02.945323
503	8	11	32	2018-04-14 20:56:02.949653	2018-04-14 20:56:02.949653
504	8	15	3	2018-04-14 20:56:02.954049	2018-04-14 20:56:02.954049
505	8	15	6	2018-04-14 20:56:02.958716	2018-04-14 20:56:02.958716
506	8	15	20	2018-04-14 20:56:02.963029	2018-04-14 20:56:02.963029
507	8	15	22	2018-04-14 20:56:02.967239	2018-04-14 20:56:02.967239
508	8	15	29	2018-04-14 20:56:02.971965	2018-04-14 20:56:02.971965
509	8	15	37	2018-04-14 20:56:02.979606	2018-04-14 20:56:02.979606
510	8	15	38	2018-04-14 20:56:02.986239	2018-04-14 20:56:02.986239
511	8	15	45	2018-04-14 20:56:02.991115	2018-04-14 20:56:02.991115
512	8	15	55	2018-04-14 20:56:02.995503	2018-04-14 20:56:02.995503
513	8	15	56	2018-04-14 20:56:03.000088	2018-04-14 20:56:03.000088
514	8	15	57	2018-04-14 20:56:03.005501	2018-04-14 20:56:03.005501
515	8	15	58	2018-04-14 20:56:03.010538	2018-04-14 20:56:03.010538
516	8	15	65	2018-04-14 20:56:03.015268	2018-04-14 20:56:03.015268
517	8	15	69	2018-04-14 20:56:03.020159	2018-04-14 20:56:03.020159
518	8	15	71	2018-04-14 20:56:03.024983	2018-04-14 20:56:03.024983
519	8	15	81	2018-04-14 20:56:03.029908	2018-04-14 20:56:03.029908
520	8	15	94	2018-04-14 20:56:03.03508	2018-04-14 20:56:03.03508
521	8	15	99	2018-04-14 20:56:03.040709	2018-04-14 20:56:03.040709
522	8	15	101	2018-04-14 20:56:03.049805	2018-04-14 20:56:03.049805
523	8	15	106	2018-04-14 20:56:03.057506	2018-04-14 20:56:03.057506
524	8	15	107	2018-04-14 20:56:03.062627	2018-04-14 20:56:03.062627
525	8	15	110	2018-04-14 20:56:03.067632	2018-04-14 20:56:03.067632
526	8	15	111	2018-04-14 20:56:03.072456	2018-04-14 20:56:03.072456
527	8	20	46	2018-04-14 20:56:03.077262	2018-04-14 20:56:03.077262
528	8	20	70	2018-04-14 20:56:03.082202	2018-04-14 20:56:03.082202
529	8	20	84	2018-04-14 20:56:03.088742	2018-04-14 20:56:03.088742
530	8	24	3	2018-04-14 20:56:03.096369	2018-04-14 20:56:03.096369
531	8	24	20	2018-04-14 20:56:03.102567	2018-04-14 20:56:03.102567
532	8	24	29	2018-04-14 20:56:03.107533	2018-04-14 20:56:03.107533
533	8	24	37	2018-04-14 20:56:03.111916	2018-04-14 20:56:03.111916
534	8	24	45	2018-04-14 20:56:03.116303	2018-04-14 20:56:03.116303
535	8	24	55	2018-04-14 20:56:03.120819	2018-04-14 20:56:03.120819
536	8	24	56	2018-04-14 20:56:03.125246	2018-04-14 20:56:03.125246
537	8	24	57	2018-04-14 20:56:03.129819	2018-04-14 20:56:03.129819
538	8	24	58	2018-04-14 20:56:03.134157	2018-04-14 20:56:03.134157
539	8	24	65	2018-04-14 20:56:03.138569	2018-04-14 20:56:03.138569
540	8	24	69	2018-04-14 20:56:03.142941	2018-04-14 20:56:03.142941
541	8	24	81	2018-04-14 20:56:03.147572	2018-04-14 20:56:03.147572
542	8	24	94	2018-04-14 20:56:03.152288	2018-04-14 20:56:03.152288
543	8	24	99	2018-04-14 20:56:03.161088	2018-04-14 20:56:03.161088
544	8	24	101	2018-04-14 20:56:03.16804	2018-04-14 20:56:03.16804
545	8	24	107	2018-04-14 20:56:03.173682	2018-04-14 20:56:03.173682
546	8	29	46	2018-04-14 20:56:03.178169	2018-04-14 20:56:03.178169
547	8	29	70	2018-04-14 20:56:03.182438	2018-04-14 20:56:03.182438
548	8	29	84	2018-04-14 20:56:03.187068	2018-04-14 20:56:03.187068
549	8	31	3	2018-04-14 20:56:03.191873	2018-04-14 20:56:03.191873
550	8	31	6	2018-04-14 20:56:03.196332	2018-04-14 20:56:03.196332
551	8	31	12	2018-04-14 20:56:03.200863	2018-04-14 20:56:03.200863
552	8	31	18	2018-04-14 20:56:03.205352	2018-04-14 20:56:03.205352
553	8	31	20	2018-04-14 20:56:03.209912	2018-04-14 20:56:03.209912
554	8	31	22	2018-04-14 20:56:03.214351	2018-04-14 20:56:03.214351
555	8	31	29	2018-04-14 20:56:03.219238	2018-04-14 20:56:03.219238
556	8	31	37	2018-04-14 20:56:03.227137	2018-04-14 20:56:03.227137
557	8	31	38	2018-04-14 20:56:03.234384	2018-04-14 20:56:03.234384
558	8	31	45	2018-04-14 20:56:03.240406	2018-04-14 20:56:03.240406
559	8	31	53	2018-04-14 20:56:03.244973	2018-04-14 20:56:03.244973
560	8	31	54	2018-04-14 20:56:03.249529	2018-04-14 20:56:03.249529
561	8	31	55	2018-04-14 20:56:03.254023	2018-04-14 20:56:03.254023
562	8	31	56	2018-04-14 20:56:03.25911	2018-04-14 20:56:03.25911
563	8	31	57	2018-04-14 20:56:03.267021	2018-04-14 20:56:03.267021
564	8	31	58	2018-04-14 20:56:03.273716	2018-04-14 20:56:03.273716
565	8	31	65	2018-04-14 20:56:03.279458	2018-04-14 20:56:03.279458
566	8	31	69	2018-04-14 20:56:03.28425	2018-04-14 20:56:03.28425
567	8	31	71	2018-04-14 20:56:03.289417	2018-04-14 20:56:03.289417
568	8	31	81	2018-04-14 20:56:03.293903	2018-04-14 20:56:03.293903
569	8	31	87	2018-04-14 20:56:03.301722	2018-04-14 20:56:03.301722
570	8	31	92	2018-04-14 20:56:03.308449	2018-04-14 20:56:03.308449
571	8	31	94	2018-04-14 20:56:03.313318	2018-04-14 20:56:03.313318
572	8	31	99	2018-04-14 20:56:03.318116	2018-04-14 20:56:03.318116
573	8	31	101	2018-04-14 20:56:03.322766	2018-04-14 20:56:03.322766
574	8	31	106	2018-04-14 20:56:03.326934	2018-04-14 20:56:03.326934
575	8	31	107	2018-04-14 20:56:03.33152	2018-04-14 20:56:03.33152
576	8	31	109	2018-04-14 20:56:03.336166	2018-04-14 20:56:03.336166
577	8	31	110	2018-04-14 20:56:03.34106	2018-04-14 20:56:03.34106
578	8	31	111	2018-04-14 20:56:03.349445	2018-04-14 20:56:03.349445
579	8	32	3	2018-04-14 20:56:03.356825	2018-04-14 20:56:03.356825
580	8	32	6	2018-04-14 20:56:03.361625	2018-04-14 20:56:03.361625
581	8	32	12	2018-04-14 20:56:03.366212	2018-04-14 20:56:03.366212
582	8	32	18	2018-04-14 20:56:03.37138	2018-04-14 20:56:03.37138
583	8	32	20	2018-04-14 20:56:03.37745	2018-04-14 20:56:03.37745
584	8	32	22	2018-04-14 20:56:03.38189	2018-04-14 20:56:03.38189
585	8	32	29	2018-04-14 20:56:03.386437	2018-04-14 20:56:03.386437
586	8	32	32	2018-04-14 20:56:03.391098	2018-04-14 20:56:03.391098
587	8	32	37	2018-04-14 20:56:03.395755	2018-04-14 20:56:03.395755
588	8	32	38	2018-04-14 20:56:03.400232	2018-04-14 20:56:03.400232
589	8	32	45	2018-04-14 20:56:03.405064	2018-04-14 20:56:03.405064
590	8	32	53	2018-04-14 20:56:03.409824	2018-04-14 20:56:03.409824
591	8	32	54	2018-04-14 20:56:03.4143	2018-04-14 20:56:03.4143
592	8	32	55	2018-04-14 20:56:03.419059	2018-04-14 20:56:03.419059
593	8	32	56	2018-04-14 20:56:03.426717	2018-04-14 20:56:03.426717
594	8	32	57	2018-04-14 20:56:03.43411	2018-04-14 20:56:03.43411
595	8	32	58	2018-04-14 20:56:03.439655	2018-04-14 20:56:03.439655
596	8	32	65	2018-04-14 20:56:03.444206	2018-04-14 20:56:03.444206
597	8	32	69	2018-04-14 20:56:03.448883	2018-04-14 20:56:03.448883
598	8	32	71	2018-04-14 20:56:03.453392	2018-04-14 20:56:03.453392
599	8	32	81	2018-04-14 20:56:03.45891	2018-04-14 20:56:03.45891
600	8	32	87	2018-04-14 20:56:03.463511	2018-04-14 20:56:03.463511
601	8	32	92	2018-04-14 20:56:03.468255	2018-04-14 20:56:03.468255
602	8	32	94	2018-04-14 20:56:03.479026	2018-04-14 20:56:03.479026
603	8	32	99	2018-04-14 20:56:03.483805	2018-04-14 20:56:03.483805
604	8	32	101	2018-04-14 20:56:03.488204	2018-04-14 20:56:03.488204
605	8	32	106	2018-04-14 20:56:03.493093	2018-04-14 20:56:03.493093
606	8	32	107	2018-04-14 20:56:03.497701	2018-04-14 20:56:03.497701
607	8	32	109	2018-04-14 20:56:03.502303	2018-04-14 20:56:03.502303
608	8	32	110	2018-04-14 20:56:03.511064	2018-04-14 20:56:03.511064
609	8	32	111	2018-04-14 20:56:03.519461	2018-04-14 20:56:03.519461
610	8	36	3	2018-04-14 20:56:03.526397	2018-04-14 20:56:03.526397
611	8	36	20	2018-04-14 20:56:03.532744	2018-04-14 20:56:03.532744
612	8	36	29	2018-04-14 20:56:03.537499	2018-04-14 20:56:03.537499
613	8	36	37	2018-04-14 20:56:03.541974	2018-04-14 20:56:03.541974
614	8	36	45	2018-04-14 20:56:03.546539	2018-04-14 20:56:03.546539
615	8	36	55	2018-04-14 20:56:03.551028	2018-04-14 20:56:03.551028
616	8	36	56	2018-04-14 20:56:03.555532	2018-04-14 20:56:03.555532
617	8	36	57	2018-04-14 20:56:03.560255	2018-04-14 20:56:03.560255
618	8	36	58	2018-04-14 20:56:03.564624	2018-04-14 20:56:03.564624
619	8	36	65	2018-04-14 20:56:03.569049	2018-04-14 20:56:03.569049
620	8	36	69	2018-04-14 20:56:03.573676	2018-04-14 20:56:03.573676
621	8	36	81	2018-04-14 20:56:03.578359	2018-04-14 20:56:03.578359
622	8	36	94	2018-04-14 20:56:03.582701	2018-04-14 20:56:03.582701
623	8	36	99	2018-04-14 20:56:03.587119	2018-04-14 20:56:03.587119
624	8	36	101	2018-04-14 20:56:03.592125	2018-04-14 20:56:03.592125
625	8	36	107	2018-04-14 20:56:03.596656	2018-04-14 20:56:03.596656
626	8	37	46	2018-04-14 20:56:03.600906	2018-04-14 20:56:03.600906
627	8	37	70	2018-04-14 20:56:03.608801	2018-04-14 20:56:03.608801
628	8	37	84	2018-04-14 20:56:03.615605	2018-04-14 20:56:03.615605
629	8	39	3	2018-04-14 20:56:03.621053	2018-04-14 20:56:03.621053
630	8	39	6	2018-04-14 20:56:03.627676	2018-04-14 20:56:03.627676
631	8	39	20	2018-04-14 20:56:03.633695	2018-04-14 20:56:03.633695
632	8	39	22	2018-04-14 20:56:03.640504	2018-04-14 20:56:03.640504
633	8	39	29	2018-04-14 20:56:03.645761	2018-04-14 20:56:03.645761
634	8	39	37	2018-04-14 20:56:03.650163	2018-04-14 20:56:03.650163
635	8	39	38	2018-04-14 20:56:03.654559	2018-04-14 20:56:03.654559
636	8	39	45	2018-04-14 20:56:03.662243	2018-04-14 20:56:03.662243
637	8	39	55	2018-04-14 20:56:03.670791	2018-04-14 20:56:03.670791
638	8	39	56	2018-04-14 20:56:03.67754	2018-04-14 20:56:03.67754
639	8	39	57	2018-04-14 20:56:03.682545	2018-04-14 20:56:03.682545
640	8	39	58	2018-04-14 20:56:03.687136	2018-04-14 20:56:03.687136
641	8	39	65	2018-04-14 20:56:03.692029	2018-04-14 20:56:03.692029
642	8	39	69	2018-04-14 20:56:03.696442	2018-04-14 20:56:03.696442
643	8	39	71	2018-04-14 20:56:03.703917	2018-04-14 20:56:03.703917
644	8	39	81	2018-04-14 20:56:03.710526	2018-04-14 20:56:03.710526
645	8	39	94	2018-04-14 20:56:03.715184	2018-04-14 20:56:03.715184
646	8	39	99	2018-04-14 20:56:03.720083	2018-04-14 20:56:03.720083
647	8	39	101	2018-04-14 20:56:03.725075	2018-04-14 20:56:03.725075
648	8	39	106	2018-04-14 20:56:03.729851	2018-04-14 20:56:03.729851
649	8	39	107	2018-04-14 20:56:03.734313	2018-04-14 20:56:03.734313
650	8	39	110	2018-04-14 20:56:03.738883	2018-04-14 20:56:03.738883
651	8	39	111	2018-04-14 20:56:03.743226	2018-04-14 20:56:03.743226
652	8	40	3	2018-04-14 20:56:03.750194	2018-04-14 20:56:03.750194
653	8	40	20	2018-04-14 20:56:03.754958	2018-04-14 20:56:03.754958
654	8	40	29	2018-04-14 20:56:03.762995	2018-04-14 20:56:03.762995
655	8	40	37	2018-04-14 20:56:03.771265	2018-04-14 20:56:03.771265
656	8	40	45	2018-04-14 20:56:03.779632	2018-04-14 20:56:03.779632
657	8	40	57	2018-04-14 20:56:03.786917	2018-04-14 20:56:03.786917
658	8	40	58	2018-04-14 20:56:03.793381	2018-04-14 20:56:03.793381
659	8	40	69	2018-04-14 20:56:03.798396	2018-04-14 20:56:03.798396
660	8	40	81	2018-04-14 20:56:03.803442	2018-04-14 20:56:03.803442
661	8	40	94	2018-04-14 20:56:03.808603	2018-04-14 20:56:03.808603
662	8	40	99	2018-04-14 20:56:03.813862	2018-04-14 20:56:03.813862
663	8	40	101	2018-04-14 20:56:03.819028	2018-04-14 20:56:03.819028
664	8	40	107	2018-04-14 20:56:03.823968	2018-04-14 20:56:03.823968
665	8	45	46	2018-04-14 20:56:03.828729	2018-04-14 20:56:03.828729
666	8	45	70	2018-04-14 20:56:03.833487	2018-04-14 20:56:03.833487
667	8	45	84	2018-04-14 20:56:03.838339	2018-04-14 20:56:03.838339
668	8	46	6	2018-04-14 20:56:03.843237	2018-04-14 20:56:03.843237
669	8	46	22	2018-04-14 20:56:03.848275	2018-04-14 20:56:03.848275
670	8	46	38	2018-04-14 20:56:03.853096	2018-04-14 20:56:03.853096
671	8	46	71	2018-04-14 20:56:03.857972	2018-04-14 20:56:03.857972
672	8	46	106	2018-04-14 20:56:03.862677	2018-04-14 20:56:03.862677
673	8	46	110	2018-04-14 20:56:03.867496	2018-04-14 20:56:03.867496
674	8	46	111	2018-04-14 20:56:03.872275	2018-04-14 20:56:03.872275
675	8	55	46	2018-04-14 20:56:03.877337	2018-04-14 20:56:03.877337
676	8	55	70	2018-04-14 20:56:03.882278	2018-04-14 20:56:03.882278
677	8	55	84	2018-04-14 20:56:03.887065	2018-04-14 20:56:03.887065
678	8	56	46	2018-04-14 20:56:03.891964	2018-04-14 20:56:03.891964
679	8	56	70	2018-04-14 20:56:03.896879	2018-04-14 20:56:03.896879
680	8	56	84	2018-04-14 20:56:03.901614	2018-04-14 20:56:03.901614
681	8	57	46	2018-04-14 20:56:03.906275	2018-04-14 20:56:03.906275
682	8	57	70	2018-04-14 20:56:03.910966	2018-04-14 20:56:03.910966
683	8	57	84	2018-04-14 20:56:03.915662	2018-04-14 20:56:03.915662
684	8	58	46	2018-04-14 20:56:03.920141	2018-04-14 20:56:03.920141
685	8	58	70	2018-04-14 20:56:03.924921	2018-04-14 20:56:03.924921
686	8	58	84	2018-04-14 20:56:03.929595	2018-04-14 20:56:03.929595
687	8	65	46	2018-04-14 20:56:03.934229	2018-04-14 20:56:03.934229
688	8	65	70	2018-04-14 20:56:03.938841	2018-04-14 20:56:03.938841
689	8	65	84	2018-04-14 20:56:03.94339	2018-04-14 20:56:03.94339
690	8	69	46	2018-04-14 20:56:03.948163	2018-04-14 20:56:03.948163
691	8	69	70	2018-04-14 20:56:03.953301	2018-04-14 20:56:03.953301
692	8	69	84	2018-04-14 20:56:03.960115	2018-04-14 20:56:03.960115
693	8	70	6	2018-04-14 20:56:03.967626	2018-04-14 20:56:03.967626
694	8	70	22	2018-04-14 20:56:03.972692	2018-04-14 20:56:03.972692
695	8	70	38	2018-04-14 20:56:03.977375	2018-04-14 20:56:03.977375
696	8	70	71	2018-04-14 20:56:03.982291	2018-04-14 20:56:03.982291
697	8	70	106	2018-04-14 20:56:03.987057	2018-04-14 20:56:03.987057
698	8	70	110	2018-04-14 20:56:03.991796	2018-04-14 20:56:03.991796
699	8	70	111	2018-04-14 20:56:03.996751	2018-04-14 20:56:03.996751
700	8	71	6	2018-04-14 20:56:04.001495	2018-04-14 20:56:04.001495
701	8	71	71	2018-04-14 20:56:04.006365	2018-04-14 20:56:04.006365
702	8	72	3	2018-04-14 20:56:04.011026	2018-04-14 20:56:04.011026
703	8	72	6	2018-04-14 20:56:04.015888	2018-04-14 20:56:04.015888
704	8	72	20	2018-04-14 20:56:04.028693	2018-04-14 20:56:04.028693
705	8	72	22	2018-04-14 20:56:04.039958	2018-04-14 20:56:04.039958
706	8	72	29	2018-04-14 20:56:04.044801	2018-04-14 20:56:04.044801
707	8	72	37	2018-04-14 20:56:04.052255	2018-04-14 20:56:04.052255
708	8	72	38	2018-04-14 20:56:04.057605	2018-04-14 20:56:04.057605
709	8	72	45	2018-04-14 20:56:04.062204	2018-04-14 20:56:04.062204
710	8	72	55	2018-04-14 20:56:04.066692	2018-04-14 20:56:04.066692
711	8	72	56	2018-04-14 20:56:04.072433	2018-04-14 20:56:04.072433
712	8	72	57	2018-04-14 20:56:04.076975	2018-04-14 20:56:04.076975
713	8	72	58	2018-04-14 20:56:04.081585	2018-04-14 20:56:04.081585
714	8	72	65	2018-04-14 20:56:04.0863	2018-04-14 20:56:04.0863
715	8	72	69	2018-04-14 20:56:04.090755	2018-04-14 20:56:04.090755
716	8	72	71	2018-04-14 20:56:04.095388	2018-04-14 20:56:04.095388
717	8	72	81	2018-04-14 20:56:04.100055	2018-04-14 20:56:04.100055
718	8	72	94	2018-04-14 20:56:04.107149	2018-04-14 20:56:04.107149
719	8	72	99	2018-04-14 20:56:04.112717	2018-04-14 20:56:04.112717
720	8	72	101	2018-04-14 20:56:04.117263	2018-04-14 20:56:04.117263
721	8	72	106	2018-04-14 20:56:04.121819	2018-04-14 20:56:04.121819
722	8	72	107	2018-04-14 20:56:04.126303	2018-04-14 20:56:04.126303
723	8	72	110	2018-04-14 20:56:04.130916	2018-04-14 20:56:04.130916
724	8	72	111	2018-04-14 20:56:04.135485	2018-04-14 20:56:04.135485
725	8	74	3	2018-04-14 20:56:04.140086	2018-04-14 20:56:04.140086
726	8	74	6	2018-04-14 20:56:04.1447	2018-04-14 20:56:04.1447
727	8	74	20	2018-04-14 20:56:04.150603	2018-04-14 20:56:04.150603
728	8	74	22	2018-04-14 20:56:04.159718	2018-04-14 20:56:04.159718
729	8	74	29	2018-04-14 20:56:04.167193	2018-04-14 20:56:04.167193
730	8	74	37	2018-04-14 20:56:04.172711	2018-04-14 20:56:04.172711
731	8	74	38	2018-04-14 20:56:04.177269	2018-04-14 20:56:04.177269
732	8	74	45	2018-04-14 20:56:04.181865	2018-04-14 20:56:04.181865
733	8	74	55	2018-04-14 20:56:04.186442	2018-04-14 20:56:04.186442
734	8	74	56	2018-04-14 20:56:04.190804	2018-04-14 20:56:04.190804
735	8	74	57	2018-04-14 20:56:04.195238	2018-04-14 20:56:04.195238
736	8	74	58	2018-04-14 20:56:04.199738	2018-04-14 20:56:04.199738
737	8	74	65	2018-04-14 20:56:04.204144	2018-04-14 20:56:04.204144
738	8	74	69	2018-04-14 20:56:04.208655	2018-04-14 20:56:04.208655
739	8	74	71	2018-04-14 20:56:04.21367	2018-04-14 20:56:04.21367
740	8	74	81	2018-04-14 20:56:04.218465	2018-04-14 20:56:04.218465
741	8	74	94	2018-04-14 20:56:04.223111	2018-04-14 20:56:04.223111
742	8	74	99	2018-04-14 20:56:04.227691	2018-04-14 20:56:04.227691
743	8	74	101	2018-04-14 20:56:04.232154	2018-04-14 20:56:04.232154
744	8	74	106	2018-04-14 20:56:04.237053	2018-04-14 20:56:04.237053
745	8	74	107	2018-04-14 20:56:04.241415	2018-04-14 20:56:04.241415
746	8	74	110	2018-04-14 20:56:04.246088	2018-04-14 20:56:04.246088
747	8	74	111	2018-04-14 20:56:04.257604	2018-04-14 20:56:04.257604
748	8	81	46	2018-04-14 20:56:04.263829	2018-04-14 20:56:04.263829
749	8	81	70	2018-04-14 20:56:04.268451	2018-04-14 20:56:04.268451
750	8	81	84	2018-04-14 20:56:04.272667	2018-04-14 20:56:04.272667
751	8	83	31	2018-04-14 20:56:04.279099	2018-04-14 20:56:04.279099
752	8	83	32	2018-04-14 20:56:04.28808	2018-04-14 20:56:04.28808
753	8	84	6	2018-04-14 20:56:04.295078	2018-04-14 20:56:04.295078
754	8	84	22	2018-04-14 20:56:04.299931	2018-04-14 20:56:04.299931
755	8	84	38	2018-04-14 20:56:04.304302	2018-04-14 20:56:04.304302
756	8	84	71	2018-04-14 20:56:04.308646	2018-04-14 20:56:04.308646
757	8	84	106	2018-04-14 20:56:04.316343	2018-04-14 20:56:04.316343
758	8	84	110	2018-04-14 20:56:04.324122	2018-04-14 20:56:04.324122
759	8	84	111	2018-04-14 20:56:04.332773	2018-04-14 20:56:04.332773
760	8	85	3	2018-04-14 20:56:04.339386	2018-04-14 20:56:04.339386
761	8	85	20	2018-04-14 20:56:04.344864	2018-04-14 20:56:04.344864
762	8	85	29	2018-04-14 20:56:04.349709	2018-04-14 20:56:04.349709
763	8	85	37	2018-04-14 20:56:04.354161	2018-04-14 20:56:04.354161
764	8	85	45	2018-04-14 20:56:04.358569	2018-04-14 20:56:04.358569
765	8	85	55	2018-04-14 20:56:04.363107	2018-04-14 20:56:04.363107
766	8	85	56	2018-04-14 20:56:04.367834	2018-04-14 20:56:04.367834
767	8	85	57	2018-04-14 20:56:04.37236	2018-04-14 20:56:04.37236
768	8	85	58	2018-04-14 20:56:04.376778	2018-04-14 20:56:04.376778
769	8	85	65	2018-04-14 20:56:04.381453	2018-04-14 20:56:04.381453
770	8	85	69	2018-04-14 20:56:04.385918	2018-04-14 20:56:04.385918
771	8	85	81	2018-04-14 20:56:04.390521	2018-04-14 20:56:04.390521
772	8	85	94	2018-04-14 20:56:04.395079	2018-04-14 20:56:04.395079
773	8	85	99	2018-04-14 20:56:04.399607	2018-04-14 20:56:04.399607
774	8	85	101	2018-04-14 20:56:04.404014	2018-04-14 20:56:04.404014
775	8	85	107	2018-04-14 20:56:04.412539	2018-04-14 20:56:04.412539
776	8	86	3	2018-04-14 20:56:04.420241	2018-04-14 20:56:04.420241
777	8	86	6	2018-04-14 20:56:04.426908	2018-04-14 20:56:04.426908
778	8	86	20	2018-04-14 20:56:04.431978	2018-04-14 20:56:04.431978
779	8	86	22	2018-04-14 20:56:04.436644	2018-04-14 20:56:04.436644
780	8	86	29	2018-04-14 20:56:04.44123	2018-04-14 20:56:04.44123
781	8	86	37	2018-04-14 20:56:04.445865	2018-04-14 20:56:04.445865
782	8	86	38	2018-04-14 20:56:04.450568	2018-04-14 20:56:04.450568
783	8	86	45	2018-04-14 20:56:04.454887	2018-04-14 20:56:04.454887
784	8	86	55	2018-04-14 20:56:04.459461	2018-04-14 20:56:04.459461
785	8	86	56	2018-04-14 20:56:04.464278	2018-04-14 20:56:04.464278
786	8	86	57	2018-04-14 20:56:04.471964	2018-04-14 20:56:04.471964
787	8	86	58	2018-04-14 20:56:04.477159	2018-04-14 20:56:04.477159
788	8	86	65	2018-04-14 20:56:04.482017	2018-04-14 20:56:04.482017
789	8	86	69	2018-04-14 20:56:04.486828	2018-04-14 20:56:04.486828
790	8	86	71	2018-04-14 20:56:04.491372	2018-04-14 20:56:04.491372
791	8	86	81	2018-04-14 20:56:04.497854	2018-04-14 20:56:04.497854
792	8	86	94	2018-04-14 20:56:04.502755	2018-04-14 20:56:04.502755
793	8	86	99	2018-04-14 20:56:04.507155	2018-04-14 20:56:04.507155
794	8	86	101	2018-04-14 20:56:04.511594	2018-04-14 20:56:04.511594
795	8	86	106	2018-04-14 20:56:04.515935	2018-04-14 20:56:04.515935
796	8	86	107	2018-04-14 20:56:04.52036	2018-04-14 20:56:04.52036
797	8	86	110	2018-04-14 20:56:04.524808	2018-04-14 20:56:04.524808
798	8	86	111	2018-04-14 20:56:04.529368	2018-04-14 20:56:04.529368
799	8	88	3	2018-04-14 20:56:04.534283	2018-04-14 20:56:04.534283
800	8	88	20	2018-04-14 20:56:04.539019	2018-04-14 20:56:04.539019
801	8	88	29	2018-04-14 20:56:04.543182	2018-04-14 20:56:04.543182
802	8	88	37	2018-04-14 20:56:04.547878	2018-04-14 20:56:04.547878
803	8	88	45	2018-04-14 20:56:04.552653	2018-04-14 20:56:04.552653
804	8	88	55	2018-04-14 20:56:04.557151	2018-04-14 20:56:04.557151
805	8	88	56	2018-04-14 20:56:04.562131	2018-04-14 20:56:04.562131
806	8	88	57	2018-04-14 20:56:04.56732	2018-04-14 20:56:04.56732
807	8	88	58	2018-04-14 20:56:04.571688	2018-04-14 20:56:04.571688
808	8	88	65	2018-04-14 20:56:04.580106	2018-04-14 20:56:04.580106
809	8	88	69	2018-04-14 20:56:04.587742	2018-04-14 20:56:04.587742
810	8	88	81	2018-04-14 20:56:04.592947	2018-04-14 20:56:04.592947
811	8	88	94	2018-04-14 20:56:04.601068	2018-04-14 20:56:04.601068
812	8	88	99	2018-04-14 20:56:04.609886	2018-04-14 20:56:04.609886
813	8	88	101	2018-04-14 20:56:04.616632	2018-04-14 20:56:04.616632
814	8	88	107	2018-04-14 20:56:04.622069	2018-04-14 20:56:04.622069
815	8	89	3	2018-04-14 20:56:04.626467	2018-04-14 20:56:04.626467
816	8	89	20	2018-04-14 20:56:04.630991	2018-04-14 20:56:04.630991
817	8	89	29	2018-04-14 20:56:04.635609	2018-04-14 20:56:04.635609
818	8	89	37	2018-04-14 20:56:04.640773	2018-04-14 20:56:04.640773
819	8	89	45	2018-04-14 20:56:04.648039	2018-04-14 20:56:04.648039
820	8	89	55	2018-04-14 20:56:04.653531	2018-04-14 20:56:04.653531
821	8	89	56	2018-04-14 20:56:04.66054	2018-04-14 20:56:04.66054
822	8	89	57	2018-04-14 20:56:04.669404	2018-04-14 20:56:04.669404
823	8	89	58	2018-04-14 20:56:04.676085	2018-04-14 20:56:04.676085
824	8	89	65	2018-04-14 20:56:04.682656	2018-04-14 20:56:04.682656
825	8	89	69	2018-04-14 20:56:04.687323	2018-04-14 20:56:04.687323
826	8	89	81	2018-04-14 20:56:04.691763	2018-04-14 20:56:04.691763
827	8	89	94	2018-04-14 20:56:04.696355	2018-04-14 20:56:04.696355
828	8	89	99	2018-04-14 20:56:04.70097	2018-04-14 20:56:04.70097
829	8	89	101	2018-04-14 20:56:04.708943	2018-04-14 20:56:04.708943
830	8	89	107	2018-04-14 20:56:04.714588	2018-04-14 20:56:04.714588
831	8	94	46	2018-04-14 20:56:04.7199	2018-04-14 20:56:04.7199
832	8	94	70	2018-04-14 20:56:04.724221	2018-04-14 20:56:04.724221
833	8	94	84	2018-04-14 20:56:04.728637	2018-04-14 20:56:04.728637
834	8	99	46	2018-04-14 20:56:04.734016	2018-04-14 20:56:04.734016
835	8	99	70	2018-04-14 20:56:04.738361	2018-04-14 20:56:04.738361
836	8	99	84	2018-04-14 20:56:04.74278	2018-04-14 20:56:04.74278
837	8	100	15	2018-04-14 20:56:04.74737	2018-04-14 20:56:04.74737
838	8	100	16	2018-04-14 20:56:04.751893	2018-04-14 20:56:04.751893
839	8	100	17	2018-04-14 20:56:04.756291	2018-04-14 20:56:04.756291
840	8	100	31	2018-04-14 20:56:04.760498	2018-04-14 20:56:04.760498
841	8	100	32	2018-04-14 20:56:04.764935	2018-04-14 20:56:04.764935
842	8	100	39	2018-04-14 20:56:04.769394	2018-04-14 20:56:04.769394
843	8	100	41	2018-04-14 20:56:04.773777	2018-04-14 20:56:04.773777
844	8	100	48	2018-04-14 20:56:04.781922	2018-04-14 20:56:04.781922
845	8	100	52	2018-04-14 20:56:04.789789	2018-04-14 20:56:04.789789
846	8	100	61	2018-04-14 20:56:04.796209	2018-04-14 20:56:04.796209
847	8	100	72	2018-04-14 20:56:04.800876	2018-04-14 20:56:04.800876
848	8	100	74	2018-04-14 20:56:04.807501	2018-04-14 20:56:04.807501
849	8	100	78	2018-04-14 20:56:04.814155	2018-04-14 20:56:04.814155
850	8	100	86	2018-04-14 20:56:04.820254	2018-04-14 20:56:04.820254
851	8	100	91	2018-04-14 20:56:04.824848	2018-04-14 20:56:04.824848
852	8	100	95	2018-04-14 20:56:04.833115	2018-04-14 20:56:04.833115
853	8	100	98	2018-04-14 20:56:04.840863	2018-04-14 20:56:04.840863
854	8	101	46	2018-04-14 20:56:04.847547	2018-04-14 20:56:04.847547
855	8	101	70	2018-04-14 20:56:04.852496	2018-04-14 20:56:04.852496
856	8	101	84	2018-04-14 20:56:04.858819	2018-04-14 20:56:04.858819
857	8	107	46	2018-04-14 20:56:04.863288	2018-04-14 20:56:04.863288
858	8	107	70	2018-04-14 20:56:04.867976	2018-04-14 20:56:04.867976
859	8	107	84	2018-04-14 20:56:04.872616	2018-04-14 20:56:04.872616
860	9	2	3	2018-04-14 20:56:04.877153	2018-04-14 20:56:04.877153
861	9	2	6	2018-04-14 20:56:04.883215	2018-04-14 20:56:04.883215
862	9	2	20	2018-04-14 20:56:04.888061	2018-04-14 20:56:04.888061
863	9	2	22	2018-04-14 20:56:04.892449	2018-04-14 20:56:04.892449
864	9	2	29	2018-04-14 20:56:04.896835	2018-04-14 20:56:04.896835
865	9	2	37	2018-04-14 20:56:04.901528	2018-04-14 20:56:04.901528
866	9	2	38	2018-04-14 20:56:04.908433	2018-04-14 20:56:04.908433
867	9	2	45	2018-04-14 20:56:04.913913	2018-04-14 20:56:04.913913
868	9	2	55	2018-04-14 20:56:04.918565	2018-04-14 20:56:04.918565
869	9	2	56	2018-04-14 20:56:04.923047	2018-04-14 20:56:04.923047
870	9	2	57	2018-04-14 20:56:04.927383	2018-04-14 20:56:04.927383
871	9	2	58	2018-04-14 20:56:04.93436	2018-04-14 20:56:04.93436
872	9	2	65	2018-04-14 20:56:04.939716	2018-04-14 20:56:04.939716
873	9	2	69	2018-04-14 20:56:04.944048	2018-04-14 20:56:04.944048
874	9	2	71	2018-04-14 20:56:04.948555	2018-04-14 20:56:04.948555
875	9	2	81	2018-04-14 20:56:04.953226	2018-04-14 20:56:04.953226
876	9	2	94	2018-04-14 20:56:04.961015	2018-04-14 20:56:04.961015
877	9	2	99	2018-04-14 20:56:04.969007	2018-04-14 20:56:04.969007
878	9	2	101	2018-04-14 20:56:04.975348	2018-04-14 20:56:04.975348
879	9	2	106	2018-04-14 20:56:04.979981	2018-04-14 20:56:04.979981
880	9	2	107	2018-04-14 20:56:04.984729	2018-04-14 20:56:04.984729
881	9	2	110	2018-04-14 20:56:04.99522	2018-04-14 20:56:04.99522
882	9	2	111	2018-04-14 20:56:05.000099	2018-04-14 20:56:05.000099
883	9	3	3	2018-04-14 20:56:05.004613	2018-04-14 20:56:05.004613
884	9	3	37	2018-04-14 20:56:05.009062	2018-04-14 20:56:05.009062
885	9	3	81	2018-04-14 20:56:05.013675	2018-04-14 20:56:05.013675
886	9	3	101	2018-04-14 20:56:05.018626	2018-04-14 20:56:05.018626
887	9	3	107	2018-04-14 20:56:05.023184	2018-04-14 20:56:05.023184
888	9	4	3	2018-04-14 20:56:05.027735	2018-04-14 20:56:05.027735
889	9	4	6	2018-04-14 20:56:05.032287	2018-04-14 20:56:05.032287
890	9	4	20	2018-04-14 20:56:05.037032	2018-04-14 20:56:05.037032
891	9	4	22	2018-04-14 20:56:05.041478	2018-04-14 20:56:05.041478
892	9	4	29	2018-04-14 20:56:05.045846	2018-04-14 20:56:05.045846
893	9	4	37	2018-04-14 20:56:05.050752	2018-04-14 20:56:05.050752
894	9	4	38	2018-04-14 20:56:05.055733	2018-04-14 20:56:05.055733
895	9	4	45	2018-04-14 20:56:05.060457	2018-04-14 20:56:05.060457
896	9	4	55	2018-04-14 20:56:05.065003	2018-04-14 20:56:05.065003
897	9	4	56	2018-04-14 20:56:05.069821	2018-04-14 20:56:05.069821
898	9	4	57	2018-04-14 20:56:05.074279	2018-04-14 20:56:05.074279
899	9	4	58	2018-04-14 20:56:05.078997	2018-04-14 20:56:05.078997
900	9	4	65	2018-04-14 20:56:05.086552	2018-04-14 20:56:05.086552
901	9	4	69	2018-04-14 20:56:05.0953	2018-04-14 20:56:05.0953
902	9	4	71	2018-04-14 20:56:05.102285	2018-04-14 20:56:05.102285
903	9	4	81	2018-04-14 20:56:05.107216	2018-04-14 20:56:05.107216
904	9	4	94	2018-04-14 20:56:05.111766	2018-04-14 20:56:05.111766
905	9	4	99	2018-04-14 20:56:05.116325	2018-04-14 20:56:05.116325
906	9	4	101	2018-04-14 20:56:05.120929	2018-04-14 20:56:05.120929
907	9	4	106	2018-04-14 20:56:05.125224	2018-04-14 20:56:05.125224
908	9	4	107	2018-04-14 20:56:05.129687	2018-04-14 20:56:05.129687
909	9	4	110	2018-04-14 20:56:05.134263	2018-04-14 20:56:05.134263
910	9	4	111	2018-04-14 20:56:05.140303	2018-04-14 20:56:05.140303
911	9	7	11	2018-04-14 20:56:05.144618	2018-04-14 20:56:05.144618
912	9	7	83	2018-04-14 20:56:05.149503	2018-04-14 20:56:05.149503
913	9	9	11	2018-04-14 20:56:05.154015	2018-04-14 20:56:05.154015
914	9	9	83	2018-04-14 20:56:05.158434	2018-04-14 20:56:05.158434
915	9	11	64	2018-04-14 20:56:05.162912	2018-04-14 20:56:05.162912
916	9	11	80	2018-04-14 20:56:05.169721	2018-04-14 20:56:05.169721
917	9	12	3	2018-04-14 20:56:05.174234	2018-04-14 20:56:05.174234
918	9	12	20	2018-04-14 20:56:05.181354	2018-04-14 20:56:05.181354
919	9	12	29	2018-04-14 20:56:05.186199	2018-04-14 20:56:05.186199
920	9	12	37	2018-04-14 20:56:05.190659	2018-04-14 20:56:05.190659
921	9	12	45	2018-04-14 20:56:05.195055	2018-04-14 20:56:05.195055
922	9	12	54	2018-04-14 20:56:05.201306	2018-04-14 20:56:05.201306
923	9	12	55	2018-04-14 20:56:05.208239	2018-04-14 20:56:05.208239
924	9	12	57	2018-04-14 20:56:05.213195	2018-04-14 20:56:05.213195
925	9	12	58	2018-04-14 20:56:05.217756	2018-04-14 20:56:05.217756
926	9	12	69	2018-04-14 20:56:05.222154	2018-04-14 20:56:05.222154
927	9	12	81	2018-04-14 20:56:05.226563	2018-04-14 20:56:05.226563
928	9	12	94	2018-04-14 20:56:05.230948	2018-04-14 20:56:05.230948
929	9	12	99	2018-04-14 20:56:05.236431	2018-04-14 20:56:05.236431
930	9	12	101	2018-04-14 20:56:05.240965	2018-04-14 20:56:05.240965
931	9	13	11	2018-04-14 20:56:05.245116	2018-04-14 20:56:05.245116
932	9	13	83	2018-04-14 20:56:05.249769	2018-04-14 20:56:05.249769
933	9	14	11	2018-04-14 20:56:05.254366	2018-04-14 20:56:05.254366
934	9	14	83	2018-04-14 20:56:05.258612	2018-04-14 20:56:05.258612
935	9	18	37	2018-04-14 20:56:05.262995	2018-04-14 20:56:05.262995
936	9	18	81	2018-04-14 20:56:05.267582	2018-04-14 20:56:05.267582
937	9	18	101	2018-04-14 20:56:05.27214	2018-04-14 20:56:05.27214
938	9	18	107	2018-04-14 20:56:05.277743	2018-04-14 20:56:05.277743
939	9	20	3	2018-04-14 20:56:05.283519	2018-04-14 20:56:05.283519
940	9	20	20	2018-04-14 20:56:05.288078	2018-04-14 20:56:05.288078
941	9	20	29	2018-04-14 20:56:05.292386	2018-04-14 20:56:05.292386
942	9	20	37	2018-04-14 20:56:05.296746	2018-04-14 20:56:05.296746
943	9	20	57	2018-04-14 20:56:05.301037	2018-04-14 20:56:05.301037
944	9	20	58	2018-04-14 20:56:05.305612	2018-04-14 20:56:05.305612
945	9	20	81	2018-04-14 20:56:05.310044	2018-04-14 20:56:05.310044
946	9	20	94	2018-04-14 20:56:05.31439	2018-04-14 20:56:05.31439
947	9	20	101	2018-04-14 20:56:05.318879	2018-04-14 20:56:05.318879
948	9	20	107	2018-04-14 20:56:05.323222	2018-04-14 20:56:05.323222
949	9	23	11	2018-04-14 20:56:05.327714	2018-04-14 20:56:05.327714
950	9	23	83	2018-04-14 20:56:05.331992	2018-04-14 20:56:05.331992
951	9	24	11	2018-04-14 20:56:05.336877	2018-04-14 20:56:05.336877
952	9	24	83	2018-04-14 20:56:05.341155	2018-04-14 20:56:05.341155
953	9	26	11	2018-04-14 20:56:05.348339	2018-04-14 20:56:05.348339
954	9	26	83	2018-04-14 20:56:05.352941	2018-04-14 20:56:05.352941
955	9	29	3	2018-04-14 20:56:05.357493	2018-04-14 20:56:05.357493
956	9	29	29	2018-04-14 20:56:05.361905	2018-04-14 20:56:05.361905
957	9	29	37	2018-04-14 20:56:05.366336	2018-04-14 20:56:05.366336
958	9	29	81	2018-04-14 20:56:05.372324	2018-04-14 20:56:05.372324
959	9	29	101	2018-04-14 20:56:05.376883	2018-04-14 20:56:05.376883
960	9	29	107	2018-04-14 20:56:05.381218	2018-04-14 20:56:05.381218
961	9	33	11	2018-04-14 20:56:05.385863	2018-04-14 20:56:05.385863
962	9	33	83	2018-04-14 20:56:05.390399	2018-04-14 20:56:05.390399
963	9	34	11	2018-04-14 20:56:05.394605	2018-04-14 20:56:05.394605
964	9	34	83	2018-04-14 20:56:05.39907	2018-04-14 20:56:05.39907
965	9	36	11	2018-04-14 20:56:05.403651	2018-04-14 20:56:05.403651
966	9	36	83	2018-04-14 20:56:05.408167	2018-04-14 20:56:05.408167
967	9	37	37	2018-04-14 20:56:05.412918	2018-04-14 20:56:05.412918
968	9	37	101	2018-04-14 20:56:05.419114	2018-04-14 20:56:05.419114
969	9	37	107	2018-04-14 20:56:05.425276	2018-04-14 20:56:05.425276
970	9	40	11	2018-04-14 20:56:05.432994	2018-04-14 20:56:05.432994
971	9	40	83	2018-04-14 20:56:05.441839	2018-04-14 20:56:05.441839
972	9	45	3	2018-04-14 20:56:05.449023	2018-04-14 20:56:05.449023
973	9	45	20	2018-04-14 20:56:05.455292	2018-04-14 20:56:05.455292
974	9	45	29	2018-04-14 20:56:05.459798	2018-04-14 20:56:05.459798
975	9	45	37	2018-04-14 20:56:05.46408	2018-04-14 20:56:05.46408
976	9	45	45	2018-04-14 20:56:05.468855	2018-04-14 20:56:05.468855
977	9	45	57	2018-04-14 20:56:05.473513	2018-04-14 20:56:05.473513
978	9	45	58	2018-04-14 20:56:05.477983	2018-04-14 20:56:05.477983
979	9	45	69	2018-04-14 20:56:05.482535	2018-04-14 20:56:05.482535
980	9	45	81	2018-04-14 20:56:05.487398	2018-04-14 20:56:05.487398
981	9	45	94	2018-04-14 20:56:05.491716	2018-04-14 20:56:05.491716
982	9	45	99	2018-04-14 20:56:05.496188	2018-04-14 20:56:05.496188
983	9	45	101	2018-04-14 20:56:05.502501	2018-04-14 20:56:05.502501
984	9	45	107	2018-04-14 20:56:05.507185	2018-04-14 20:56:05.507185
985	9	47	11	2018-04-14 20:56:05.511526	2018-04-14 20:56:05.511526
986	9	47	83	2018-04-14 20:56:05.51599	2018-04-14 20:56:05.51599
987	9	49	11	2018-04-14 20:56:05.520686	2018-04-14 20:56:05.520686
988	9	49	83	2018-04-14 20:56:05.525101	2018-04-14 20:56:05.525101
989	9	50	11	2018-04-14 20:56:05.52949	2018-04-14 20:56:05.52949
990	9	50	83	2018-04-14 20:56:05.533754	2018-04-14 20:56:05.533754
991	9	53	3	2018-04-14 20:56:05.542625	2018-04-14 20:56:05.542625
992	9	53	20	2018-04-14 20:56:05.549339	2018-04-14 20:56:05.549339
993	9	53	29	2018-04-14 20:56:05.554646	2018-04-14 20:56:05.554646
994	9	53	37	2018-04-14 20:56:05.560144	2018-04-14 20:56:05.560144
995	9	53	45	2018-04-14 20:56:05.565031	2018-04-14 20:56:05.565031
996	9	53	54	2018-04-14 20:56:05.570151	2018-04-14 20:56:05.570151
997	9	53	55	2018-04-14 20:56:05.574949	2018-04-14 20:56:05.574949
998	9	53	57	2018-04-14 20:56:05.579603	2018-04-14 20:56:05.579603
999	9	53	58	2018-04-14 20:56:05.584809	2018-04-14 20:56:05.584809
1000	9	53	69	2018-04-14 20:56:05.590669	2018-04-14 20:56:05.590669
1001	9	53	81	2018-04-14 20:56:05.595045	2018-04-14 20:56:05.595045
1002	9	53	94	2018-04-14 20:56:05.599609	2018-04-14 20:56:05.599609
1003	9	53	99	2018-04-14 20:56:05.604053	2018-04-14 20:56:05.604053
1004	9	53	101	2018-04-14 20:56:05.608545	2018-04-14 20:56:05.608545
1005	9	54	3	2018-04-14 20:56:05.620599	2018-04-14 20:56:05.620599
1006	9	54	20	2018-04-14 20:56:05.627335	2018-04-14 20:56:05.627335
1007	9	54	29	2018-04-14 20:56:05.631655	2018-04-14 20:56:05.631655
1008	9	54	37	2018-04-14 20:56:05.636189	2018-04-14 20:56:05.636189
1009	9	54	45	2018-04-14 20:56:05.640826	2018-04-14 20:56:05.640826
1010	9	54	54	2018-04-14 20:56:05.645148	2018-04-14 20:56:05.645148
1011	9	54	55	2018-04-14 20:56:05.649441	2018-04-14 20:56:05.649441
1012	9	54	57	2018-04-14 20:56:05.653778	2018-04-14 20:56:05.653778
1013	9	54	58	2018-04-14 20:56:05.658354	2018-04-14 20:56:05.658354
1014	9	54	69	2018-04-14 20:56:05.663385	2018-04-14 20:56:05.663385
1015	9	54	81	2018-04-14 20:56:05.668736	2018-04-14 20:56:05.668736
1016	9	54	94	2018-04-14 20:56:05.67339	2018-04-14 20:56:05.67339
1017	9	54	99	2018-04-14 20:56:05.677622	2018-04-14 20:56:05.677622
1018	9	54	101	2018-04-14 20:56:05.682082	2018-04-14 20:56:05.682082
1019	9	55	3	2018-04-14 20:56:05.686646	2018-04-14 20:56:05.686646
1020	9	55	20	2018-04-14 20:56:05.690989	2018-04-14 20:56:05.690989
1021	9	55	29	2018-04-14 20:56:05.695341	2018-04-14 20:56:05.695341
1022	9	55	37	2018-04-14 20:56:05.700069	2018-04-14 20:56:05.700069
1023	9	55	45	2018-04-14 20:56:05.704887	2018-04-14 20:56:05.704887
1024	9	55	55	2018-04-14 20:56:05.709687	2018-04-14 20:56:05.709687
1025	9	55	57	2018-04-14 20:56:05.714088	2018-04-14 20:56:05.714088
1026	9	55	58	2018-04-14 20:56:05.718739	2018-04-14 20:56:05.718739
1027	9	55	69	2018-04-14 20:56:05.723487	2018-04-14 20:56:05.723487
1028	9	55	81	2018-04-14 20:56:05.727816	2018-04-14 20:56:05.727816
1029	9	55	94	2018-04-14 20:56:05.732266	2018-04-14 20:56:05.732266
1030	9	55	99	2018-04-14 20:56:05.736757	2018-04-14 20:56:05.736757
1031	9	55	101	2018-04-14 20:56:05.7411	2018-04-14 20:56:05.7411
1032	9	56	64	2018-04-14 20:56:05.745492	2018-04-14 20:56:05.745492
1033	9	56	80	2018-04-14 20:56:05.750149	2018-04-14 20:56:05.750149
1034	9	57	3	2018-04-14 20:56:05.754708	2018-04-14 20:56:05.754708
1035	9	57	29	2018-04-14 20:56:05.762575	2018-04-14 20:56:05.762575
1036	9	57	37	2018-04-14 20:56:05.769715	2018-04-14 20:56:05.769715
1037	9	57	57	2018-04-14 20:56:05.775522	2018-04-14 20:56:05.775522
1038	9	57	81	2018-04-14 20:56:05.779892	2018-04-14 20:56:05.779892
1039	9	57	94	2018-04-14 20:56:05.787541	2018-04-14 20:56:05.787541
1040	9	57	101	2018-04-14 20:56:05.795372	2018-04-14 20:56:05.795372
1041	9	57	107	2018-04-14 20:56:05.801438	2018-04-14 20:56:05.801438
1042	9	58	3	2018-04-14 20:56:05.805945	2018-04-14 20:56:05.805945
1043	9	58	29	2018-04-14 20:56:05.811014	2018-04-14 20:56:05.811014
1044	9	58	37	2018-04-14 20:56:05.815638	2018-04-14 20:56:05.815638
1045	9	58	57	2018-04-14 20:56:05.820245	2018-04-14 20:56:05.820245
1046	9	58	58	2018-04-14 20:56:05.824405	2018-04-14 20:56:05.824405
1047	9	58	81	2018-04-14 20:56:05.828889	2018-04-14 20:56:05.828889
1048	9	58	94	2018-04-14 20:56:05.833316	2018-04-14 20:56:05.833316
1049	9	58	101	2018-04-14 20:56:05.840271	2018-04-14 20:56:05.840271
1050	9	58	107	2018-04-14 20:56:05.845782	2018-04-14 20:56:05.845782
1051	9	60	3	2018-04-14 20:56:05.850451	2018-04-14 20:56:05.850451
1052	9	60	6	2018-04-14 20:56:05.855064	2018-04-14 20:56:05.855064
1053	9	60	20	2018-04-14 20:56:05.860534	2018-04-14 20:56:05.860534
1054	9	60	22	2018-04-14 20:56:05.864904	2018-04-14 20:56:05.864904
1055	9	60	29	2018-04-14 20:56:05.869361	2018-04-14 20:56:05.869361
1056	9	60	37	2018-04-14 20:56:05.875416	2018-04-14 20:56:05.875416
1057	9	60	38	2018-04-14 20:56:05.880108	2018-04-14 20:56:05.880108
1058	9	60	45	2018-04-14 20:56:05.884767	2018-04-14 20:56:05.884767
1059	9	60	55	2018-04-14 20:56:05.889898	2018-04-14 20:56:05.889898
1060	9	60	56	2018-04-14 20:56:05.894286	2018-04-14 20:56:05.894286
1061	9	60	57	2018-04-14 20:56:05.898546	2018-04-14 20:56:05.898546
1062	9	60	58	2018-04-14 20:56:05.902903	2018-04-14 20:56:05.902903
1063	9	60	65	2018-04-14 20:56:05.907727	2018-04-14 20:56:05.907727
1064	9	60	69	2018-04-14 20:56:05.912086	2018-04-14 20:56:05.912086
1065	9	60	71	2018-04-14 20:56:05.916407	2018-04-14 20:56:05.916407
1066	9	60	81	2018-04-14 20:56:05.922721	2018-04-14 20:56:05.922721
1067	9	60	94	2018-04-14 20:56:05.930343	2018-04-14 20:56:05.930343
1068	9	60	99	2018-04-14 20:56:05.937074	2018-04-14 20:56:05.937074
1069	9	60	101	2018-04-14 20:56:05.94269	2018-04-14 20:56:05.94269
1070	9	60	106	2018-04-14 20:56:05.94727	2018-04-14 20:56:05.94727
1071	9	60	107	2018-04-14 20:56:05.951691	2018-04-14 20:56:05.951691
1072	9	60	110	2018-04-14 20:56:05.956609	2018-04-14 20:56:05.956609
1073	9	60	111	2018-04-14 20:56:05.961071	2018-04-14 20:56:05.961071
1074	9	64	6	2018-04-14 20:56:05.968666	2018-04-14 20:56:05.968666
1075	9	64	22	2018-04-14 20:56:05.974114	2018-04-14 20:56:05.974114
1076	9	64	38	2018-04-14 20:56:05.978587	2018-04-14 20:56:05.978587
1077	9	64	71	2018-04-14 20:56:05.983002	2018-04-14 20:56:05.983002
1078	9	64	106	2018-04-14 20:56:05.987614	2018-04-14 20:56:05.987614
1079	9	64	110	2018-04-14 20:56:05.992019	2018-04-14 20:56:05.992019
1080	9	64	111	2018-04-14 20:56:05.996584	2018-04-14 20:56:05.996584
1081	9	65	3	2018-04-14 20:56:06.001042	2018-04-14 20:56:06.001042
1082	9	65	20	2018-04-14 20:56:06.007807	2018-04-14 20:56:06.007807
1083	9	65	29	2018-04-14 20:56:06.013289	2018-04-14 20:56:06.013289
1084	9	65	37	2018-04-14 20:56:06.017608	2018-04-14 20:56:06.017608
1085	9	65	45	2018-04-14 20:56:06.022121	2018-04-14 20:56:06.022121
1086	9	65	54	2018-04-14 20:56:06.026461	2018-04-14 20:56:06.026461
1087	9	65	55	2018-04-14 20:56:06.030797	2018-04-14 20:56:06.030797
1088	9	65	57	2018-04-14 20:56:06.035558	2018-04-14 20:56:06.035558
1089	9	65	58	2018-04-14 20:56:06.043312	2018-04-14 20:56:06.043312
1090	9	65	69	2018-04-14 20:56:06.052348	2018-04-14 20:56:06.052348
1091	9	65	81	2018-04-14 20:56:06.059284	2018-04-14 20:56:06.059284
1092	9	65	94	2018-04-14 20:56:06.064539	2018-04-14 20:56:06.064539
1093	9	65	99	2018-04-14 20:56:06.069628	2018-04-14 20:56:06.069628
1094	9	65	101	2018-04-14 20:56:06.074316	2018-04-14 20:56:06.074316
1095	9	69	3	2018-04-14 20:56:06.078972	2018-04-14 20:56:06.078972
1096	9	69	20	2018-04-14 20:56:06.083472	2018-04-14 20:56:06.083472
1097	9	69	29	2018-04-14 20:56:06.088177	2018-04-14 20:56:06.088177
1098	9	69	37	2018-04-14 20:56:06.092701	2018-04-14 20:56:06.092701
1099	9	69	57	2018-04-14 20:56:06.097161	2018-04-14 20:56:06.097161
1100	9	69	58	2018-04-14 20:56:06.101808	2018-04-14 20:56:06.101808
1101	9	69	69	2018-04-14 20:56:06.107519	2018-04-14 20:56:06.107519
1102	9	69	81	2018-04-14 20:56:06.111924	2018-04-14 20:56:06.111924
1103	9	69	94	2018-04-14 20:56:06.11648	2018-04-14 20:56:06.11648
1104	9	69	99	2018-04-14 20:56:06.121597	2018-04-14 20:56:06.121597
1105	9	69	101	2018-04-14 20:56:06.127382	2018-04-14 20:56:06.127382
1106	9	69	107	2018-04-14 20:56:06.131819	2018-04-14 20:56:06.131819
1107	9	77	3	2018-04-14 20:56:06.136198	2018-04-14 20:56:06.136198
1108	9	77	6	2018-04-14 20:56:06.140799	2018-04-14 20:56:06.140799
1109	9	77	20	2018-04-14 20:56:06.145414	2018-04-14 20:56:06.145414
1110	9	77	22	2018-04-14 20:56:06.150036	2018-04-14 20:56:06.150036
1111	9	77	29	2018-04-14 20:56:06.154526	2018-04-14 20:56:06.154526
1112	9	77	37	2018-04-14 20:56:06.160344	2018-04-14 20:56:06.160344
1113	9	77	38	2018-04-14 20:56:06.166665	2018-04-14 20:56:06.166665
1114	9	77	45	2018-04-14 20:56:06.171064	2018-04-14 20:56:06.171064
1115	9	77	55	2018-04-14 20:56:06.175888	2018-04-14 20:56:06.175888
1116	9	77	56	2018-04-14 20:56:06.180381	2018-04-14 20:56:06.180381
1117	9	77	57	2018-04-14 20:56:06.184916	2018-04-14 20:56:06.184916
1118	9	77	58	2018-04-14 20:56:06.191161	2018-04-14 20:56:06.191161
1119	9	77	65	2018-04-14 20:56:06.199613	2018-04-14 20:56:06.199613
1120	9	77	69	2018-04-14 20:56:06.21166	2018-04-14 20:56:06.21166
1121	9	77	71	2018-04-14 20:56:06.218311	2018-04-14 20:56:06.218311
1122	9	77	81	2018-04-14 20:56:06.226278	2018-04-14 20:56:06.226278
1123	9	77	94	2018-04-14 20:56:06.232988	2018-04-14 20:56:06.232988
1124	9	77	99	2018-04-14 20:56:06.237797	2018-04-14 20:56:06.237797
1125	9	77	101	2018-04-14 20:56:06.242923	2018-04-14 20:56:06.242923
1126	9	77	106	2018-04-14 20:56:06.247636	2018-04-14 20:56:06.247636
1127	9	77	107	2018-04-14 20:56:06.252136	2018-04-14 20:56:06.252136
1128	9	77	110	2018-04-14 20:56:06.258355	2018-04-14 20:56:06.258355
1129	9	77	111	2018-04-14 20:56:06.266342	2018-04-14 20:56:06.266342
1130	9	80	6	2018-04-14 20:56:06.273152	2018-04-14 20:56:06.273152
1131	9	80	22	2018-04-14 20:56:06.27831	2018-04-14 20:56:06.27831
1132	9	80	38	2018-04-14 20:56:06.282961	2018-04-14 20:56:06.282961
1133	9	80	71	2018-04-14 20:56:06.287575	2018-04-14 20:56:06.287575
1134	9	80	106	2018-04-14 20:56:06.292359	2018-04-14 20:56:06.292359
1135	9	80	110	2018-04-14 20:56:06.296787	2018-04-14 20:56:06.296787
1136	9	80	111	2018-04-14 20:56:06.304064	2018-04-14 20:56:06.304064
1137	9	81	37	2018-04-14 20:56:06.309635	2018-04-14 20:56:06.309635
1138	9	81	81	2018-04-14 20:56:06.31405	2018-04-14 20:56:06.31405
1139	9	81	101	2018-04-14 20:56:06.31869	2018-04-14 20:56:06.31869
1140	9	81	107	2018-04-14 20:56:06.324986	2018-04-14 20:56:06.324986
1141	9	83	64	2018-04-14 20:56:06.33102	2018-04-14 20:56:06.33102
1142	9	83	80	2018-04-14 20:56:06.335642	2018-04-14 20:56:06.335642
1143	9	85	11	2018-04-14 20:56:06.340401	2018-04-14 20:56:06.340401
1144	9	85	83	2018-04-14 20:56:06.344738	2018-04-14 20:56:06.344738
1145	9	87	3	2018-04-14 20:56:06.349519	2018-04-14 20:56:06.349519
1146	9	87	20	2018-04-14 20:56:06.354097	2018-04-14 20:56:06.354097
1147	9	87	29	2018-04-14 20:56:06.362146	2018-04-14 20:56:06.362146
1148	9	87	37	2018-04-14 20:56:06.368922	2018-04-14 20:56:06.368922
1149	9	87	45	2018-04-14 20:56:06.374258	2018-04-14 20:56:06.374258
1150	9	87	54	2018-04-14 20:56:06.380333	2018-04-14 20:56:06.380333
1151	9	87	55	2018-04-14 20:56:06.385854	2018-04-14 20:56:06.385854
1152	9	87	57	2018-04-14 20:56:06.393385	2018-04-14 20:56:06.393385
1153	9	87	58	2018-04-14 20:56:06.39888	2018-04-14 20:56:06.39888
1154	9	87	69	2018-04-14 20:56:06.403779	2018-04-14 20:56:06.403779
1155	9	87	81	2018-04-14 20:56:06.408633	2018-04-14 20:56:06.408633
1156	9	87	94	2018-04-14 20:56:06.41424	2018-04-14 20:56:06.41424
1157	9	87	99	2018-04-14 20:56:06.418972	2018-04-14 20:56:06.418972
1158	9	87	101	2018-04-14 20:56:06.423701	2018-04-14 20:56:06.423701
1159	9	88	11	2018-04-14 20:56:06.428274	2018-04-14 20:56:06.428274
1160	9	88	83	2018-04-14 20:56:06.433004	2018-04-14 20:56:06.433004
1161	9	89	11	2018-04-14 20:56:06.437556	2018-04-14 20:56:06.437556
1162	9	89	83	2018-04-14 20:56:06.441998	2018-04-14 20:56:06.441998
1163	9	92	3	2018-04-14 20:56:06.446446	2018-04-14 20:56:06.446446
1164	9	92	20	2018-04-14 20:56:06.450804	2018-04-14 20:56:06.450804
1165	9	92	29	2018-04-14 20:56:06.455445	2018-04-14 20:56:06.455445
1166	9	92	37	2018-04-14 20:56:06.460195	2018-04-14 20:56:06.460195
1167	9	92	45	2018-04-14 20:56:06.465487	2018-04-14 20:56:06.465487
1168	9	92	54	2018-04-14 20:56:06.470885	2018-04-14 20:56:06.470885
1169	9	92	55	2018-04-14 20:56:06.478664	2018-04-14 20:56:06.478664
1170	9	92	57	2018-04-14 20:56:06.485712	2018-04-14 20:56:06.485712
1171	9	92	58	2018-04-14 20:56:06.490803	2018-04-14 20:56:06.490803
1172	9	92	69	2018-04-14 20:56:06.495495	2018-04-14 20:56:06.495495
1173	9	92	81	2018-04-14 20:56:06.499984	2018-04-14 20:56:06.499984
1174	9	92	94	2018-04-14 20:56:06.504354	2018-04-14 20:56:06.504354
1175	9	92	99	2018-04-14 20:56:06.508902	2018-04-14 20:56:06.508902
1176	9	92	101	2018-04-14 20:56:06.513326	2018-04-14 20:56:06.513326
1177	9	94	3	2018-04-14 20:56:06.517717	2018-04-14 20:56:06.517717
1178	9	94	29	2018-04-14 20:56:06.521956	2018-04-14 20:56:06.521956
1179	9	94	37	2018-04-14 20:56:06.526814	2018-04-14 20:56:06.526814
1180	9	94	81	2018-04-14 20:56:06.5312	2018-04-14 20:56:06.5312
1181	9	94	94	2018-04-14 20:56:06.535621	2018-04-14 20:56:06.535621
1182	9	94	101	2018-04-14 20:56:06.540255	2018-04-14 20:56:06.540255
1183	9	94	107	2018-04-14 20:56:06.544791	2018-04-14 20:56:06.544791
1184	9	99	3	2018-04-14 20:56:06.54932	2018-04-14 20:56:06.54932
1185	9	99	20	2018-04-14 20:56:06.553783	2018-04-14 20:56:06.553783
1186	9	99	29	2018-04-14 20:56:06.559325	2018-04-14 20:56:06.559325
1187	9	99	37	2018-04-14 20:56:06.564003	2018-04-14 20:56:06.564003
1188	9	99	57	2018-04-14 20:56:06.570132	2018-04-14 20:56:06.570132
1189	9	99	58	2018-04-14 20:56:06.57584	2018-04-14 20:56:06.57584
1190	9	99	81	2018-04-14 20:56:06.581492	2018-04-14 20:56:06.581492
1191	9	99	94	2018-04-14 20:56:06.586133	2018-04-14 20:56:06.586133
1192	9	99	99	2018-04-14 20:56:06.591001	2018-04-14 20:56:06.591001
1193	9	99	101	2018-04-14 20:56:06.595414	2018-04-14 20:56:06.595414
1194	9	99	107	2018-04-14 20:56:06.599771	2018-04-14 20:56:06.599771
1195	9	100	64	2018-04-14 20:56:06.60427	2018-04-14 20:56:06.60427
1196	9	100	80	2018-04-14 20:56:06.60883	2018-04-14 20:56:06.60883
1197	9	101	101	2018-04-14 20:56:06.613222	2018-04-14 20:56:06.613222
1198	9	101	107	2018-04-14 20:56:06.617759	2018-04-14 20:56:06.617759
1199	9	107	107	2018-04-14 20:56:06.622307	2018-04-14 20:56:06.622307
1200	9	108	11	2018-04-14 20:56:06.626771	2018-04-14 20:56:06.626771
1201	9	108	83	2018-04-14 20:56:06.631365	2018-04-14 20:56:06.631365
1202	9	109	3	2018-04-14 20:56:06.63599	2018-04-14 20:56:06.63599
1203	9	109	20	2018-04-14 20:56:06.640317	2018-04-14 20:56:06.640317
1204	9	109	29	2018-04-14 20:56:06.645128	2018-04-14 20:56:06.645128
1205	9	109	37	2018-04-14 20:56:06.649684	2018-04-14 20:56:06.649684
1206	9	109	45	2018-04-14 20:56:06.654084	2018-04-14 20:56:06.654084
1207	9	109	54	2018-04-14 20:56:06.65878	2018-04-14 20:56:06.65878
1208	9	109	55	2018-04-14 20:56:06.663432	2018-04-14 20:56:06.663432
1209	9	109	57	2018-04-14 20:56:06.668309	2018-04-14 20:56:06.668309
1210	9	109	58	2018-04-14 20:56:06.676196	2018-04-14 20:56:06.676196
1211	9	109	69	2018-04-14 20:56:06.683088	2018-04-14 20:56:06.683088
1212	9	109	81	2018-04-14 20:56:06.688781	2018-04-14 20:56:06.688781
1213	9	109	94	2018-04-14 20:56:06.693292	2018-04-14 20:56:06.693292
1214	9	109	99	2018-04-14 20:56:06.69785	2018-04-14 20:56:06.69785
1215	9	109	101	2018-04-14 20:56:06.702425	2018-04-14 20:56:06.702425
1216	10	23	31	2018-04-14 20:56:06.706993	2018-04-14 20:56:06.706993
1217	10	23	32	2018-04-14 20:56:06.711382	2018-04-14 20:56:06.711382
1218	10	108	15	2018-04-14 20:56:06.715953	2018-04-14 20:56:06.715953
1219	10	108	16	2018-04-14 20:56:06.720296	2018-04-14 20:56:06.720296
1220	10	108	17	2018-04-14 20:56:06.726511	2018-04-14 20:56:06.726511
1221	10	108	24	2018-04-14 20:56:06.73201	2018-04-14 20:56:06.73201
1222	10	108	31	2018-04-14 20:56:06.736572	2018-04-14 20:56:06.736572
1223	10	108	32	2018-04-14 20:56:06.7411	2018-04-14 20:56:06.7411
1224	10	108	39	2018-04-14 20:56:06.746638	2018-04-14 20:56:06.746638
1225	10	108	41	2018-04-14 20:56:06.757062	2018-04-14 20:56:06.757062
1226	10	108	46	2018-04-14 20:56:06.762252	2018-04-14 20:56:06.762252
1227	10	108	61	2018-04-14 20:56:06.766774	2018-04-14 20:56:06.766774
1228	10	108	70	2018-04-14 20:56:06.774599	2018-04-14 20:56:06.774599
1229	10	108	72	2018-04-14 20:56:06.781306	2018-04-14 20:56:06.781306
1230	10	108	74	2018-04-14 20:56:06.786158	2018-04-14 20:56:06.786158
1231	10	108	78	2018-04-14 20:56:06.790485	2018-04-14 20:56:06.790485
1232	10	108	84	2018-04-14 20:56:06.795113	2018-04-14 20:56:06.795113
1233	10	108	85	2018-04-14 20:56:06.799579	2018-04-14 20:56:06.799579
1234	10	108	86	2018-04-14 20:56:06.803997	2018-04-14 20:56:06.803997
1235	10	108	88	2018-04-14 20:56:06.808759	2018-04-14 20:56:06.808759
1236	10	108	89	2018-04-14 20:56:06.81368	2018-04-14 20:56:06.81368
1237	10	108	91	2018-04-14 20:56:06.818399	2018-04-14 20:56:06.818399
1238	10	108	95	2018-04-14 20:56:06.822934	2018-04-14 20:56:06.822934
1239	11	6	6	2018-04-14 20:56:06.827588	2018-04-14 20:56:06.827588
1240	11	6	22	2018-04-14 20:56:06.832093	2018-04-14 20:56:06.832093
1241	11	6	38	2018-04-14 20:56:06.836483	2018-04-14 20:56:06.836483
1242	11	6	106	2018-04-14 20:56:06.840918	2018-04-14 20:56:06.840918
1243	11	6	110	2018-04-14 20:56:06.845493	2018-04-14 20:56:06.845493
1244	11	6	111	2018-04-14 20:56:06.852343	2018-04-14 20:56:06.852343
1245	11	10	68	2018-04-14 20:56:06.857828	2018-04-14 20:56:06.857828
1246	11	27	6	2018-04-14 20:56:06.862423	2018-04-14 20:56:06.862423
1247	11	27	19	2018-04-14 20:56:06.86686	2018-04-14 20:56:06.86686
1248	11	27	22	2018-04-14 20:56:06.871138	2018-04-14 20:56:06.871138
1249	11	27	38	2018-04-14 20:56:06.875868	2018-04-14 20:56:06.875868
1250	11	27	106	2018-04-14 20:56:06.88087	2018-04-14 20:56:06.88087
1251	11	27	110	2018-04-14 20:56:06.88565	2018-04-14 20:56:06.88565
1252	11	27	111	2018-04-14 20:56:06.890111	2018-04-14 20:56:06.890111
1253	11	62	6	2018-04-14 20:56:06.894978	2018-04-14 20:56:06.894978
1254	11	62	22	2018-04-14 20:56:06.899746	2018-04-14 20:56:06.899746
1255	11	62	38	2018-04-14 20:56:06.90441	2018-04-14 20:56:06.90441
1256	11	62	106	2018-04-14 20:56:06.911811	2018-04-14 20:56:06.911811
1257	11	62	110	2018-04-14 20:56:06.918585	2018-04-14 20:56:06.918585
1258	11	62	111	2018-04-14 20:56:06.926581	2018-04-14 20:56:06.926581
1259	11	63	6	2018-04-14 20:56:06.933561	2018-04-14 20:56:06.933561
1260	11	63	22	2018-04-14 20:56:06.940031	2018-04-14 20:56:06.940031
1261	11	63	38	2018-04-14 20:56:06.947632	2018-04-14 20:56:06.947632
1262	11	63	106	2018-04-14 20:56:06.955511	2018-04-14 20:56:06.955511
1263	11	63	110	2018-04-14 20:56:06.962042	2018-04-14 20:56:06.962042
1264	11	63	111	2018-04-14 20:56:06.966621	2018-04-14 20:56:06.966621
1265	11	68	3	2018-04-14 20:56:06.97103	2018-04-14 20:56:06.97103
1266	11	68	6	2018-04-14 20:56:06.975395	2018-04-14 20:56:06.975395
1267	11	68	12	2018-04-14 20:56:06.981256	2018-04-14 20:56:06.981256
1268	11	68	18	2018-04-14 20:56:06.986066	2018-04-14 20:56:06.986066
1269	11	68	19	2018-04-14 20:56:06.990559	2018-04-14 20:56:06.990559
1270	11	68	20	2018-04-14 20:56:06.995145	2018-04-14 20:56:06.995145
1271	11	68	22	2018-04-14 20:56:06.999558	2018-04-14 20:56:06.999558
1272	11	68	29	2018-04-14 20:56:07.003932	2018-04-14 20:56:07.003932
1273	11	68	37	2018-04-14 20:56:07.008264	2018-04-14 20:56:07.008264
1274	11	68	38	2018-04-14 20:56:07.013325	2018-04-14 20:56:07.013325
1275	11	68	45	2018-04-14 20:56:07.017603	2018-04-14 20:56:07.017603
1276	11	68	53	2018-04-14 20:56:07.021985	2018-04-14 20:56:07.021985
1277	11	68	54	2018-04-14 20:56:07.027281	2018-04-14 20:56:07.027281
1278	11	68	55	2018-04-14 20:56:07.032974	2018-04-14 20:56:07.032974
1279	11	68	56	2018-04-14 20:56:07.037402	2018-04-14 20:56:07.037402
1280	11	68	57	2018-04-14 20:56:07.045022	2018-04-14 20:56:07.045022
1281	11	68	58	2018-04-14 20:56:07.052009	2018-04-14 20:56:07.052009
1282	11	68	62	2018-04-14 20:56:07.057137	2018-04-14 20:56:07.057137
1283	11	68	63	2018-04-14 20:56:07.06157	2018-04-14 20:56:07.06157
1284	11	68	65	2018-04-14 20:56:07.066818	2018-04-14 20:56:07.066818
1285	11	68	69	2018-04-14 20:56:07.071086	2018-04-14 20:56:07.071086
1286	11	68	71	2018-04-14 20:56:07.076497	2018-04-14 20:56:07.076497
1287	11	68	81	2018-04-14 20:56:07.080962	2018-04-14 20:56:07.080962
1288	11	68	87	2018-04-14 20:56:07.085545	2018-04-14 20:56:07.085545
1289	11	68	92	2018-04-14 20:56:07.090403	2018-04-14 20:56:07.090403
1290	11	68	94	2018-04-14 20:56:07.098368	2018-04-14 20:56:07.098368
1291	11	68	96	2018-04-14 20:56:07.104664	2018-04-14 20:56:07.104664
1292	11	68	97	2018-04-14 20:56:07.109568	2018-04-14 20:56:07.109568
1293	11	68	99	2018-04-14 20:56:07.114148	2018-04-14 20:56:07.114148
1294	11	68	101	2018-04-14 20:56:07.118744	2018-04-14 20:56:07.118744
1295	11	68	103	2018-04-14 20:56:07.123119	2018-04-14 20:56:07.123119
1296	11	68	105	2018-04-14 20:56:07.127617	2018-04-14 20:56:07.127617
1297	11	68	106	2018-04-14 20:56:07.13202	2018-04-14 20:56:07.13202
1298	11	68	107	2018-04-14 20:56:07.136375	2018-04-14 20:56:07.136375
1299	11	68	109	2018-04-14 20:56:07.140766	2018-04-14 20:56:07.140766
1300	11	68	110	2018-04-14 20:56:07.145128	2018-04-14 20:56:07.145128
1301	11	68	111	2018-04-14 20:56:07.149764	2018-04-14 20:56:07.149764
1302	11	71	22	2018-04-14 20:56:07.154433	2018-04-14 20:56:07.154433
1303	11	71	38	2018-04-14 20:56:07.158775	2018-04-14 20:56:07.158775
1304	11	71	106	2018-04-14 20:56:07.163576	2018-04-14 20:56:07.163576
1305	11	71	110	2018-04-14 20:56:07.171393	2018-04-14 20:56:07.171393
1306	11	71	111	2018-04-14 20:56:07.176514	2018-04-14 20:56:07.176514
1307	11	97	6	2018-04-14 20:56:07.181259	2018-04-14 20:56:07.181259
1308	11	97	22	2018-04-14 20:56:07.185914	2018-04-14 20:56:07.185914
1309	11	97	38	2018-04-14 20:56:07.190323	2018-04-14 20:56:07.190323
1310	11	97	106	2018-04-14 20:56:07.194923	2018-04-14 20:56:07.194923
1311	11	97	110	2018-04-14 20:56:07.19962	2018-04-14 20:56:07.19962
1312	11	97	111	2018-04-14 20:56:07.204363	2018-04-14 20:56:07.204363
1313	11	103	6	2018-04-14 20:56:07.208949	2018-04-14 20:56:07.208949
1314	11	103	22	2018-04-14 20:56:07.213363	2018-04-14 20:56:07.213363
1315	11	103	38	2018-04-14 20:56:07.217806	2018-04-14 20:56:07.217806
1316	11	103	106	2018-04-14 20:56:07.222385	2018-04-14 20:56:07.222385
1317	11	103	110	2018-04-14 20:56:07.22678	2018-04-14 20:56:07.22678
1318	11	103	111	2018-04-14 20:56:07.232686	2018-04-14 20:56:07.232686
1319	11	105	6	2018-04-14 20:56:07.237238	2018-04-14 20:56:07.237238
1320	11	105	22	2018-04-14 20:56:07.241846	2018-04-14 20:56:07.241846
1321	11	105	38	2018-04-14 20:56:07.249493	2018-04-14 20:56:07.249493
1322	11	105	106	2018-04-14 20:56:07.256859	2018-04-14 20:56:07.256859
1323	11	105	110	2018-04-14 20:56:07.267614	2018-04-14 20:56:07.267614
1324	11	105	111	2018-04-14 20:56:07.272073	2018-04-14 20:56:07.272073
1325	12	26	15	2018-04-14 20:56:07.277126	2018-04-14 20:56:07.277126
1326	12	26	16	2018-04-14 20:56:07.282045	2018-04-14 20:56:07.282045
1327	12	26	17	2018-04-14 20:56:07.286644	2018-04-14 20:56:07.286644
1328	12	26	24	2018-04-14 20:56:07.290979	2018-04-14 20:56:07.290979
1329	12	26	31	2018-04-14 20:56:07.295451	2018-04-14 20:56:07.295451
1330	12	26	32	2018-04-14 20:56:07.300029	2018-04-14 20:56:07.300029
1331	12	26	39	2018-04-14 20:56:07.304348	2018-04-14 20:56:07.304348
1332	12	26	41	2018-04-14 20:56:07.308916	2018-04-14 20:56:07.308916
1333	12	26	46	2018-04-14 20:56:07.31327	2018-04-14 20:56:07.31327
1334	12	26	61	2018-04-14 20:56:07.31773	2018-04-14 20:56:07.31773
1335	12	26	70	2018-04-14 20:56:07.32211	2018-04-14 20:56:07.32211
1336	12	26	72	2018-04-14 20:56:07.326472	2018-04-14 20:56:07.326472
1337	12	26	74	2018-04-14 20:56:07.330901	2018-04-14 20:56:07.330901
1338	12	26	78	2018-04-14 20:56:07.335452	2018-04-14 20:56:07.335452
1339	12	26	84	2018-04-14 20:56:07.340275	2018-04-14 20:56:07.340275
1340	12	26	85	2018-04-14 20:56:07.344797	2018-04-14 20:56:07.344797
1341	12	26	86	2018-04-14 20:56:07.34973	2018-04-14 20:56:07.34973
1342	12	26	88	2018-04-14 20:56:07.354159	2018-04-14 20:56:07.354159
1343	12	26	89	2018-04-14 20:56:07.358616	2018-04-14 20:56:07.358616
1344	12	26	91	2018-04-14 20:56:07.363503	2018-04-14 20:56:07.363503
1345	12	26	95	2018-04-14 20:56:07.370256	2018-04-14 20:56:07.370256
1346	12	50	15	2018-04-14 20:56:07.378124	2018-04-14 20:56:07.378124
1347	12	50	16	2018-04-14 20:56:07.384791	2018-04-14 20:56:07.384791
1348	12	50	17	2018-04-14 20:56:07.390058	2018-04-14 20:56:07.390058
1349	12	50	39	2018-04-14 20:56:07.394599	2018-04-14 20:56:07.394599
1350	12	50	41	2018-04-14 20:56:07.399453	2018-04-14 20:56:07.399453
1351	12	50	46	2018-04-14 20:56:07.404061	2018-04-14 20:56:07.404061
1352	12	50	61	2018-04-14 20:56:07.40911	2018-04-14 20:56:07.40911
1353	12	50	70	2018-04-14 20:56:07.413908	2018-04-14 20:56:07.413908
1354	12	50	72	2018-04-14 20:56:07.418607	2018-04-14 20:56:07.418607
1355	12	50	74	2018-04-14 20:56:07.423142	2018-04-14 20:56:07.423142
1356	12	50	78	2018-04-14 20:56:07.430434	2018-04-14 20:56:07.430434
1357	12	50	84	2018-04-14 20:56:07.437076	2018-04-14 20:56:07.437076
1358	12	50	85	2018-04-14 20:56:07.442219	2018-04-14 20:56:07.442219
1359	12	50	86	2018-04-14 20:56:07.448422	2018-04-14 20:56:07.448422
1360	12	50	88	2018-04-14 20:56:07.453839	2018-04-14 20:56:07.453839
1361	12	50	89	2018-04-14 20:56:07.458237	2018-04-14 20:56:07.458237
1362	12	50	91	2018-04-14 20:56:07.463649	2018-04-14 20:56:07.463649
1363	12	50	95	2018-04-14 20:56:07.469202	2018-04-14 20:56:07.469202
1364	13	8	2	2018-04-14 20:56:07.473517	2018-04-14 20:56:07.473517
1365	13	8	4	2018-04-14 20:56:07.478069	2018-04-14 20:56:07.478069
1366	13	8	60	2018-04-14 20:56:07.482911	2018-04-14 20:56:07.482911
1367	13	8	77	2018-04-14 20:56:07.48759	2018-04-14 20:56:07.48759
1368	13	25	2	2018-04-14 20:56:07.491944	2018-04-14 20:56:07.491944
1369	13	25	4	2018-04-14 20:56:07.496534	2018-04-14 20:56:07.496534
1370	13	25	60	2018-04-14 20:56:07.50127	2018-04-14 20:56:07.50127
1371	13	25	77	2018-04-14 20:56:07.505776	2018-04-14 20:56:07.505776
1372	13	30	2	2018-04-14 20:56:07.510452	2018-04-14 20:56:07.510452
1373	13	30	4	2018-04-14 20:56:07.514937	2018-04-14 20:56:07.514937
1374	13	30	60	2018-04-14 20:56:07.519569	2018-04-14 20:56:07.519569
1375	13	30	77	2018-04-14 20:56:07.523925	2018-04-14 20:56:07.523925
1376	13	90	2	2018-04-14 20:56:07.528241	2018-04-14 20:56:07.528241
1377	13	90	4	2018-04-14 20:56:07.533113	2018-04-14 20:56:07.533113
1378	13	90	60	2018-04-14 20:56:07.537498	2018-04-14 20:56:07.537498
1379	13	90	77	2018-04-14 20:56:07.541766	2018-04-14 20:56:07.541766
1380	14	1	19	2018-04-14 20:56:07.546176	2018-04-14 20:56:07.546176
1381	14	1	55	2018-04-14 20:56:07.550756	2018-04-14 20:56:07.550756
1382	14	1	56	2018-04-14 20:56:07.557433	2018-04-14 20:56:07.557433
1383	14	1	65	2018-04-14 20:56:07.561814	2018-04-14 20:56:07.561814
1384	14	1	103	2018-04-14 20:56:07.566559	2018-04-14 20:56:07.566559
1385	14	5	19	2018-04-14 20:56:07.571077	2018-04-14 20:56:07.571077
1386	14	5	55	2018-04-14 20:56:07.575635	2018-04-14 20:56:07.575635
1387	14	5	56	2018-04-14 20:56:07.581086	2018-04-14 20:56:07.581086
1388	14	5	65	2018-04-14 20:56:07.588776	2018-04-14 20:56:07.588776
1389	14	5	103	2018-04-14 20:56:07.597881	2018-04-14 20:56:07.597881
1390	14	8	19	2018-04-14 20:56:07.604823	2018-04-14 20:56:07.604823
1391	14	8	65	2018-04-14 20:56:07.60978	2018-04-14 20:56:07.60978
1392	14	8	103	2018-04-14 20:56:07.615725	2018-04-14 20:56:07.615725
1393	14	10	43	2018-04-14 20:56:07.620384	2018-04-14 20:56:07.620384
1394	14	10	66	2018-04-14 20:56:07.624743	2018-04-14 20:56:07.624743
1395	14	10	67	2018-04-14 20:56:07.629087	2018-04-14 20:56:07.629087
1396	14	10	112	2018-04-14 20:56:07.633801	2018-04-14 20:56:07.633801
1397	14	25	19	2018-04-14 20:56:07.638453	2018-04-14 20:56:07.638453
1398	14	25	65	2018-04-14 20:56:07.642728	2018-04-14 20:56:07.642728
1399	14	25	103	2018-04-14 20:56:07.65054	2018-04-14 20:56:07.65054
1400	14	27	11	2018-04-14 20:56:07.657417	2018-04-14 20:56:07.657417
1401	14	27	32	2018-04-14 20:56:07.662355	2018-04-14 20:56:07.662355
1402	14	27	83	2018-04-14 20:56:07.667623	2018-04-14 20:56:07.667623
1403	14	27	100	2018-04-14 20:56:07.672239	2018-04-14 20:56:07.672239
1404	14	28	19	2018-04-14 20:56:07.677128	2018-04-14 20:56:07.677128
1405	14	30	19	2018-04-14 20:56:07.683273	2018-04-14 20:56:07.683273
1406	14	30	65	2018-04-14 20:56:07.69288	2018-04-14 20:56:07.69288
1407	14	30	103	2018-04-14 20:56:07.701449	2018-04-14 20:56:07.701449
1408	14	42	6	2018-04-14 20:56:07.707802	2018-04-14 20:56:07.707802
1409	14	42	19	2018-04-14 20:56:07.713006	2018-04-14 20:56:07.713006
1410	14	42	22	2018-04-14 20:56:07.717785	2018-04-14 20:56:07.717785
1411	14	42	32	2018-04-14 20:56:07.722313	2018-04-14 20:56:07.722313
1412	14	42	38	2018-04-14 20:56:07.730536	2018-04-14 20:56:07.730536
1413	14	42	55	2018-04-14 20:56:07.740178	2018-04-14 20:56:07.740178
1414	14	42	56	2018-04-14 20:56:07.745146	2018-04-14 20:56:07.745146
1415	14	42	65	2018-04-14 20:56:07.751518	2018-04-14 20:56:07.751518
1416	14	42	71	2018-04-14 20:56:07.756996	2018-04-14 20:56:07.756996
1417	14	42	101	2018-04-14 20:56:07.761528	2018-04-14 20:56:07.761528
1418	14	42	103	2018-04-14 20:56:07.768394	2018-04-14 20:56:07.768394
1419	14	42	106	2018-04-14 20:56:07.773929	2018-04-14 20:56:07.773929
1420	14	42	107	2018-04-14 20:56:07.782437	2018-04-14 20:56:07.782437
1421	14	42	110	2018-04-14 20:56:07.790705	2018-04-14 20:56:07.790705
1422	14	42	111	2018-04-14 20:56:07.79741	2018-04-14 20:56:07.79741
1423	14	43	3	2018-04-14 20:56:07.802401	2018-04-14 20:56:07.802401
1424	14	43	6	2018-04-14 20:56:07.80654	2018-04-14 20:56:07.80654
1425	14	43	12	2018-04-14 20:56:07.810972	2018-04-14 20:56:07.810972
1426	14	43	18	2018-04-14 20:56:07.815791	2018-04-14 20:56:07.815791
1427	14	43	19	2018-04-14 20:56:07.820342	2018-04-14 20:56:07.820342
1428	14	43	20	2018-04-14 20:56:07.8247	2018-04-14 20:56:07.8247
1429	14	43	22	2018-04-14 20:56:07.829223	2018-04-14 20:56:07.829223
1430	14	43	29	2018-04-14 20:56:07.834281	2018-04-14 20:56:07.834281
1431	14	43	37	2018-04-14 20:56:07.838887	2018-04-14 20:56:07.838887
1432	14	43	38	2018-04-14 20:56:07.84326	2018-04-14 20:56:07.84326
1433	14	43	45	2018-04-14 20:56:07.847854	2018-04-14 20:56:07.847854
1434	14	43	53	2018-04-14 20:56:07.852397	2018-04-14 20:56:07.852397
1435	14	43	54	2018-04-14 20:56:07.856976	2018-04-14 20:56:07.856976
1436	14	43	55	2018-04-14 20:56:07.861315	2018-04-14 20:56:07.861315
1437	14	43	56	2018-04-14 20:56:07.86576	2018-04-14 20:56:07.86576
1438	14	43	57	2018-04-14 20:56:07.870189	2018-04-14 20:56:07.870189
1439	14	43	58	2018-04-14 20:56:07.874547	2018-04-14 20:56:07.874547
1440	14	43	65	2018-04-14 20:56:07.879196	2018-04-14 20:56:07.879196
1441	14	43	69	2018-04-14 20:56:07.883879	2018-04-14 20:56:07.883879
1442	14	43	71	2018-04-14 20:56:07.888534	2018-04-14 20:56:07.888534
1443	14	43	81	2018-04-14 20:56:07.892895	2018-04-14 20:56:07.892895
1444	14	43	87	2018-04-14 20:56:07.897367	2018-04-14 20:56:07.897367
1445	14	43	92	2018-04-14 20:56:07.90205	2018-04-14 20:56:07.90205
1446	14	43	94	2018-04-14 20:56:07.906629	2018-04-14 20:56:07.906629
1447	14	43	99	2018-04-14 20:56:07.911158	2018-04-14 20:56:07.911158
1448	14	43	101	2018-04-14 20:56:07.915785	2018-04-14 20:56:07.915785
1449	14	43	106	2018-04-14 20:56:07.920429	2018-04-14 20:56:07.920429
1450	14	43	107	2018-04-14 20:56:07.924706	2018-04-14 20:56:07.924706
1451	14	43	109	2018-04-14 20:56:07.929191	2018-04-14 20:56:07.929191
1452	14	43	110	2018-04-14 20:56:07.937066	2018-04-14 20:56:07.937066
1453	14	43	111	2018-04-14 20:56:07.943711	2018-04-14 20:56:07.943711
1454	14	44	19	2018-04-14 20:56:07.948378	2018-04-14 20:56:07.948378
1455	14	44	65	2018-04-14 20:56:07.952957	2018-04-14 20:56:07.952957
1456	14	44	103	2018-04-14 20:56:07.958301	2018-04-14 20:56:07.958301
1457	14	59	19	2018-04-14 20:56:07.965526	2018-04-14 20:56:07.965526
1458	14	59	55	2018-04-14 20:56:07.971976	2018-04-14 20:56:07.971976
1459	14	59	56	2018-04-14 20:56:07.976598	2018-04-14 20:56:07.976598
1460	14	59	65	2018-04-14 20:56:07.982633	2018-04-14 20:56:07.982633
1461	14	59	103	2018-04-14 20:56:07.987177	2018-04-14 20:56:07.987177
1462	14	61	6	2018-04-14 20:56:07.991466	2018-04-14 20:56:07.991466
1463	14	61	22	2018-04-14 20:56:07.996009	2018-04-14 20:56:07.996009
1464	14	61	38	2018-04-14 20:56:08.000698	2018-04-14 20:56:08.000698
1465	14	61	71	2018-04-14 20:56:08.005369	2018-04-14 20:56:08.005369
1466	14	61	106	2018-04-14 20:56:08.009996	2018-04-14 20:56:08.009996
1467	14	61	110	2018-04-14 20:56:08.014616	2018-04-14 20:56:08.014616
1468	14	61	111	2018-04-14 20:56:08.019342	2018-04-14 20:56:08.019342
1469	14	66	3	2018-04-14 20:56:08.023773	2018-04-14 20:56:08.023773
1470	14	66	6	2018-04-14 20:56:08.027957	2018-04-14 20:56:08.027957
1471	14	66	12	2018-04-14 20:56:08.033391	2018-04-14 20:56:08.033391
1472	14	66	18	2018-04-14 20:56:08.039284	2018-04-14 20:56:08.039284
1473	14	66	19	2018-04-14 20:56:08.043672	2018-04-14 20:56:08.043672
1474	14	66	20	2018-04-14 20:56:08.050933	2018-04-14 20:56:08.050933
1475	14	66	22	2018-04-14 20:56:08.05982	2018-04-14 20:56:08.05982
1476	14	66	29	2018-04-14 20:56:08.066617	2018-04-14 20:56:08.066617
1477	14	66	37	2018-04-14 20:56:08.071552	2018-04-14 20:56:08.071552
1478	14	66	38	2018-04-14 20:56:08.075904	2018-04-14 20:56:08.075904
1479	14	66	45	2018-04-14 20:56:08.080413	2018-04-14 20:56:08.080413
1480	14	66	53	2018-04-14 20:56:08.085024	2018-04-14 20:56:08.085024
1481	14	66	54	2018-04-14 20:56:08.089629	2018-04-14 20:56:08.089629
1482	14	66	55	2018-04-14 20:56:08.093772	2018-04-14 20:56:08.093772
1483	14	66	56	2018-04-14 20:56:08.098198	2018-04-14 20:56:08.098198
1484	14	66	57	2018-04-14 20:56:08.102865	2018-04-14 20:56:08.102865
1485	14	66	58	2018-04-14 20:56:08.107307	2018-04-14 20:56:08.107307
1486	14	66	65	2018-04-14 20:56:08.111635	2018-04-14 20:56:08.111635
1487	14	66	69	2018-04-14 20:56:08.116038	2018-04-14 20:56:08.116038
1488	14	66	71	2018-04-14 20:56:08.120673	2018-04-14 20:56:08.120673
1489	14	66	81	2018-04-14 20:56:08.126028	2018-04-14 20:56:08.126028
1490	14	66	87	2018-04-14 20:56:08.130501	2018-04-14 20:56:08.130501
1491	14	66	92	2018-04-14 20:56:08.135081	2018-04-14 20:56:08.135081
1492	14	66	94	2018-04-14 20:56:08.13981	2018-04-14 20:56:08.13981
1493	14	66	99	2018-04-14 20:56:08.144333	2018-04-14 20:56:08.144333
1494	14	66	100	2018-04-14 20:56:08.150194	2018-04-14 20:56:08.150194
1495	14	66	101	2018-04-14 20:56:08.154819	2018-04-14 20:56:08.154819
1496	14	66	106	2018-04-14 20:56:08.163896	2018-04-14 20:56:08.163896
1497	14	66	107	2018-04-14 20:56:08.172352	2018-04-14 20:56:08.172352
1498	14	66	109	2018-04-14 20:56:08.179721	2018-04-14 20:56:08.179721
1499	14	66	110	2018-04-14 20:56:08.18457	2018-04-14 20:56:08.18457
1500	14	66	111	2018-04-14 20:56:08.189135	2018-04-14 20:56:08.189135
1501	14	67	3	2018-04-14 20:56:08.19361	2018-04-14 20:56:08.19361
1502	14	67	6	2018-04-14 20:56:08.198313	2018-04-14 20:56:08.198313
1503	14	67	12	2018-04-14 20:56:08.202995	2018-04-14 20:56:08.202995
1504	14	67	18	2018-04-14 20:56:08.210394	2018-04-14 20:56:08.210394
1505	14	67	19	2018-04-14 20:56:08.217389	2018-04-14 20:56:08.217389
1506	14	67	20	2018-04-14 20:56:08.223496	2018-04-14 20:56:08.223496
1507	14	67	22	2018-04-14 20:56:08.227878	2018-04-14 20:56:08.227878
1508	14	67	29	2018-04-14 20:56:08.23236	2018-04-14 20:56:08.23236
1509	14	67	37	2018-04-14 20:56:08.237032	2018-04-14 20:56:08.237032
1510	14	67	38	2018-04-14 20:56:08.241499	2018-04-14 20:56:08.241499
1511	14	67	45	2018-04-14 20:56:08.245988	2018-04-14 20:56:08.245988
1512	14	67	51	2018-04-14 20:56:08.250632	2018-04-14 20:56:08.250632
1513	14	67	53	2018-04-14 20:56:08.255123	2018-04-14 20:56:08.255123
1514	14	67	54	2018-04-14 20:56:08.259659	2018-04-14 20:56:08.259659
1515	14	67	55	2018-04-14 20:56:08.263963	2018-04-14 20:56:08.263963
1516	14	67	56	2018-04-14 20:56:08.27037	2018-04-14 20:56:08.27037
1517	14	67	57	2018-04-14 20:56:08.275863	2018-04-14 20:56:08.275863
1518	14	67	58	2018-04-14 20:56:08.281147	2018-04-14 20:56:08.281147
1519	14	67	65	2018-04-14 20:56:08.285803	2018-04-14 20:56:08.285803
1520	14	67	69	2018-04-14 20:56:08.290372	2018-04-14 20:56:08.290372
1521	14	67	71	2018-04-14 20:56:08.294753	2018-04-14 20:56:08.294753
1522	14	67	81	2018-04-14 20:56:08.299333	2018-04-14 20:56:08.299333
1523	14	67	87	2018-04-14 20:56:08.304132	2018-04-14 20:56:08.304132
1524	14	67	92	2018-04-14 20:56:08.308636	2018-04-14 20:56:08.308636
1525	14	67	94	2018-04-14 20:56:08.313119	2018-04-14 20:56:08.313119
1526	14	67	99	2018-04-14 20:56:08.317864	2018-04-14 20:56:08.317864
1527	14	67	100	2018-04-14 20:56:08.322304	2018-04-14 20:56:08.322304
1528	14	67	101	2018-04-14 20:56:08.326714	2018-04-14 20:56:08.326714
1529	14	67	106	2018-04-14 20:56:08.330915	2018-04-14 20:56:08.330915
1530	14	67	107	2018-04-14 20:56:08.336263	2018-04-14 20:56:08.336263
1531	14	67	109	2018-04-14 20:56:08.340962	2018-04-14 20:56:08.340962
1532	14	67	110	2018-04-14 20:56:08.3454	2018-04-14 20:56:08.3454
1533	14	67	111	2018-04-14 20:56:08.349919	2018-04-14 20:56:08.349919
1534	14	68	7	2018-04-14 20:56:08.354648	2018-04-14 20:56:08.354648
1535	14	68	9	2018-04-14 20:56:08.359236	2018-04-14 20:56:08.359236
1536	14	68	11	2018-04-14 20:56:08.363685	2018-04-14 20:56:08.363685
1537	14	68	13	2018-04-14 20:56:08.368382	2018-04-14 20:56:08.368382
1538	14	68	14	2018-04-14 20:56:08.373271	2018-04-14 20:56:08.373271
1539	14	68	15	2018-04-14 20:56:08.377912	2018-04-14 20:56:08.377912
1540	14	68	16	2018-04-14 20:56:08.382831	2018-04-14 20:56:08.382831
1541	14	68	17	2018-04-14 20:56:08.387665	2018-04-14 20:56:08.387665
1542	14	68	23	2018-04-14 20:56:08.392572	2018-04-14 20:56:08.392572
1543	14	68	24	2018-04-14 20:56:08.397237	2018-04-14 20:56:08.397237
1544	14	68	26	2018-04-14 20:56:08.402159	2018-04-14 20:56:08.402159
1545	14	68	31	2018-04-14 20:56:08.407152	2018-04-14 20:56:08.407152
1546	14	68	32	2018-04-14 20:56:08.412058	2018-04-14 20:56:08.412058
1547	14	68	33	2018-04-14 20:56:08.417104	2018-04-14 20:56:08.417104
1548	14	68	34	2018-04-14 20:56:08.422666	2018-04-14 20:56:08.422666
1549	14	68	39	2018-04-14 20:56:08.428098	2018-04-14 20:56:08.428098
1550	14	68	41	2018-04-14 20:56:08.434389	2018-04-14 20:56:08.434389
1551	14	68	42	2018-04-14 20:56:08.439442	2018-04-14 20:56:08.439442
1552	14	68	46	2018-04-14 20:56:08.444326	2018-04-14 20:56:08.444326
1553	14	68	47	2018-04-14 20:56:08.451391	2018-04-14 20:56:08.451391
1554	14	68	48	2018-04-14 20:56:08.457446	2018-04-14 20:56:08.457446
1555	14	68	49	2018-04-14 20:56:08.464374	2018-04-14 20:56:08.464374
1556	14	68	50	2018-04-14 20:56:08.471114	2018-04-14 20:56:08.471114
1557	14	68	52	2018-04-14 20:56:08.476408	2018-04-14 20:56:08.476408
1558	14	68	61	2018-04-14 20:56:08.483176	2018-04-14 20:56:08.483176
1559	14	68	70	2018-04-14 20:56:08.488343	2018-04-14 20:56:08.488343
1560	14	68	72	2018-04-14 20:56:08.493275	2018-04-14 20:56:08.493275
1561	14	68	74	2018-04-14 20:56:08.500586	2018-04-14 20:56:08.500586
1562	14	68	78	2018-04-14 20:56:08.506292	2018-04-14 20:56:08.506292
1563	14	68	82	2018-04-14 20:56:08.51094	2018-04-14 20:56:08.51094
1564	14	68	83	2018-04-14 20:56:08.518434	2018-04-14 20:56:08.518434
1565	14	68	84	2018-04-14 20:56:08.524363	2018-04-14 20:56:08.524363
1566	14	68	85	2018-04-14 20:56:08.529921	2018-04-14 20:56:08.529921
1567	14	68	86	2018-04-14 20:56:08.535598	2018-04-14 20:56:08.535598
1568	14	68	88	2018-04-14 20:56:08.540322	2018-04-14 20:56:08.540322
1569	14	68	89	2018-04-14 20:56:08.545092	2018-04-14 20:56:08.545092
1570	14	68	91	2018-04-14 20:56:08.551059	2018-04-14 20:56:08.551059
1571	14	68	95	2018-04-14 20:56:08.555847	2018-04-14 20:56:08.555847
1572	14	68	98	2018-04-14 20:56:08.560589	2018-04-14 20:56:08.560589
1573	14	68	100	2018-04-14 20:56:08.571246	2018-04-14 20:56:08.571246
1574	14	68	102	2018-04-14 20:56:08.576052	2018-04-14 20:56:08.576052
1575	14	68	108	2018-04-14 20:56:08.581925	2018-04-14 20:56:08.581925
1576	14	73	19	2018-04-14 20:56:08.587149	2018-04-14 20:56:08.587149
1577	14	73	100	2018-04-14 20:56:08.591595	2018-04-14 20:56:08.591595
1578	14	75	19	2018-04-14 20:56:08.596881	2018-04-14 20:56:08.596881
1579	14	76	19	2018-04-14 20:56:08.602964	2018-04-14 20:56:08.602964
1580	14	76	55	2018-04-14 20:56:08.607716	2018-04-14 20:56:08.607716
1581	14	76	56	2018-04-14 20:56:08.612871	2018-04-14 20:56:08.612871
1582	14	76	65	2018-04-14 20:56:08.618334	2018-04-14 20:56:08.618334
1583	14	76	103	2018-04-14 20:56:08.622918	2018-04-14 20:56:08.622918
1584	14	78	6	2018-04-14 20:56:08.627564	2018-04-14 20:56:08.627564
1585	14	78	22	2018-04-14 20:56:08.633191	2018-04-14 20:56:08.633191
1586	14	78	38	2018-04-14 20:56:08.640836	2018-04-14 20:56:08.640836
1587	14	78	71	2018-04-14 20:56:08.648244	2018-04-14 20:56:08.648244
1588	14	78	106	2018-04-14 20:56:08.655843	2018-04-14 20:56:08.655843
1589	14	78	110	2018-04-14 20:56:08.662783	2018-04-14 20:56:08.662783
1590	14	78	111	2018-04-14 20:56:08.668577	2018-04-14 20:56:08.668577
1591	14	79	19	2018-04-14 20:56:08.67323	2018-04-14 20:56:08.67323
1592	14	79	65	2018-04-14 20:56:08.677886	2018-04-14 20:56:08.677886
1593	14	79	103	2018-04-14 20:56:08.6836	2018-04-14 20:56:08.6836
1594	14	90	19	2018-04-14 20:56:08.688913	2018-04-14 20:56:08.688913
1595	14	90	65	2018-04-14 20:56:08.693464	2018-04-14 20:56:08.693464
1596	14	90	103	2018-04-14 20:56:08.698998	2018-04-14 20:56:08.698998
1597	14	91	6	2018-04-14 20:56:08.704053	2018-04-14 20:56:08.704053
1598	14	91	22	2018-04-14 20:56:08.708689	2018-04-14 20:56:08.708689
1599	14	91	38	2018-04-14 20:56:08.713645	2018-04-14 20:56:08.713645
1600	14	91	71	2018-04-14 20:56:08.71886	2018-04-14 20:56:08.71886
1601	14	91	106	2018-04-14 20:56:08.72385	2018-04-14 20:56:08.72385
1602	14	91	110	2018-04-14 20:56:08.728512	2018-04-14 20:56:08.728512
1603	14	91	111	2018-04-14 20:56:08.733951	2018-04-14 20:56:08.733951
1604	14	93	19	2018-04-14 20:56:08.740711	2018-04-14 20:56:08.740711
1605	14	93	55	2018-04-14 20:56:08.746296	2018-04-14 20:56:08.746296
1606	14	93	56	2018-04-14 20:56:08.751527	2018-04-14 20:56:08.751527
1607	14	93	65	2018-04-14 20:56:08.759661	2018-04-14 20:56:08.759661
1608	14	93	103	2018-04-14 20:56:08.767318	2018-04-14 20:56:08.767318
1609	14	95	6	2018-04-14 20:56:08.772442	2018-04-14 20:56:08.772442
1610	14	95	22	2018-04-14 20:56:08.776921	2018-04-14 20:56:08.776921
1611	14	95	38	2018-04-14 20:56:08.782564	2018-04-14 20:56:08.782564
1612	14	95	71	2018-04-14 20:56:08.789302	2018-04-14 20:56:08.789302
1613	14	95	106	2018-04-14 20:56:08.794938	2018-04-14 20:56:08.794938
1614	14	95	110	2018-04-14 20:56:08.802962	2018-04-14 20:56:08.802962
1615	14	95	111	2018-04-14 20:56:08.809043	2018-04-14 20:56:08.809043
1616	14	96	6	2018-04-14 20:56:08.815396	2018-04-14 20:56:08.815396
1617	14	96	22	2018-04-14 20:56:08.820734	2018-04-14 20:56:08.820734
1618	14	96	38	2018-04-14 20:56:08.827118	2018-04-14 20:56:08.827118
1619	14	96	71	2018-04-14 20:56:08.83347	2018-04-14 20:56:08.83347
1620	14	96	106	2018-04-14 20:56:08.840144	2018-04-14 20:56:08.840144
1621	14	96	110	2018-04-14 20:56:08.849809	2018-04-14 20:56:08.849809
1622	14	96	111	2018-04-14 20:56:08.86226	2018-04-14 20:56:08.86226
1623	14	102	6	2018-04-14 20:56:08.882941	2018-04-14 20:56:08.882941
1624	14	102	19	2018-04-14 20:56:08.8919	2018-04-14 20:56:08.8919
1625	14	102	22	2018-04-14 20:56:08.899776	2018-04-14 20:56:08.899776
1626	14	102	32	2018-04-14 20:56:08.905285	2018-04-14 20:56:08.905285
1627	14	102	38	2018-04-14 20:56:08.910874	2018-04-14 20:56:08.910874
1628	14	102	55	2018-04-14 20:56:08.918558	2018-04-14 20:56:08.918558
1629	14	102	56	2018-04-14 20:56:08.923942	2018-04-14 20:56:08.923942
1630	14	102	65	2018-04-14 20:56:08.929817	2018-04-14 20:56:08.929817
1631	14	102	71	2018-04-14 20:56:08.936078	2018-04-14 20:56:08.936078
1632	14	102	101	2018-04-14 20:56:08.94073	2018-04-14 20:56:08.94073
1633	14	102	103	2018-04-14 20:56:08.948283	2018-04-14 20:56:08.948283
1634	14	102	106	2018-04-14 20:56:08.954003	2018-04-14 20:56:08.954003
1635	14	102	107	2018-04-14 20:56:08.959238	2018-04-14 20:56:08.959238
1636	14	102	110	2018-04-14 20:56:08.964033	2018-04-14 20:56:08.964033
1637	14	102	111	2018-04-14 20:56:08.969983	2018-04-14 20:56:08.969983
1638	14	104	19	2018-04-14 20:56:08.975233	2018-04-14 20:56:08.975233
1639	14	112	3	2018-04-14 20:56:08.980322	2018-04-14 20:56:08.980322
1640	14	112	6	2018-04-14 20:56:08.989149	2018-04-14 20:56:08.989149
1641	14	112	12	2018-04-14 20:56:09.006676	2018-04-14 20:56:09.006676
1642	14	112	18	2018-04-14 20:56:09.022707	2018-04-14 20:56:09.022707
1643	14	112	19	2018-04-14 20:56:09.037234	2018-04-14 20:56:09.037234
1644	14	112	20	2018-04-14 20:56:09.055059	2018-04-14 20:56:09.055059
1645	14	112	22	2018-04-14 20:56:09.066423	2018-04-14 20:56:09.066423
1646	14	112	29	2018-04-14 20:56:09.077153	2018-04-14 20:56:09.077153
1647	14	112	37	2018-04-14 20:56:09.08275	2018-04-14 20:56:09.08275
1648	14	112	38	2018-04-14 20:56:09.088279	2018-04-14 20:56:09.088279
1649	14	112	45	2018-04-14 20:56:09.101353	2018-04-14 20:56:09.101353
1650	14	112	53	2018-04-14 20:56:09.112943	2018-04-14 20:56:09.112943
1651	14	112	54	2018-04-14 20:56:09.12543	2018-04-14 20:56:09.12543
1652	14	112	55	2018-04-14 20:56:09.140176	2018-04-14 20:56:09.140176
1653	14	112	56	2018-04-14 20:56:09.145931	2018-04-14 20:56:09.145931
1654	14	112	57	2018-04-14 20:56:09.155367	2018-04-14 20:56:09.155367
1655	14	112	58	2018-04-14 20:56:09.161435	2018-04-14 20:56:09.161435
1656	14	112	65	2018-04-14 20:56:09.171972	2018-04-14 20:56:09.171972
1657	14	112	69	2018-04-14 20:56:09.178624	2018-04-14 20:56:09.178624
1658	14	112	71	2018-04-14 20:56:09.188267	2018-04-14 20:56:09.188267
1659	14	112	81	2018-04-14 20:56:09.195365	2018-04-14 20:56:09.195365
1660	14	112	87	2018-04-14 20:56:09.204117	2018-04-14 20:56:09.204117
1661	14	112	92	2018-04-14 20:56:09.210282	2018-04-14 20:56:09.210282
1662	14	112	94	2018-04-14 20:56:09.218776	2018-04-14 20:56:09.218776
1663	14	112	99	2018-04-14 20:56:09.22551	2018-04-14 20:56:09.22551
1664	14	112	100	2018-04-14 20:56:09.23913	2018-04-14 20:56:09.23913
1665	14	112	101	2018-04-14 20:56:09.255371	2018-04-14 20:56:09.255371
1666	14	112	106	2018-04-14 20:56:09.263127	2018-04-14 20:56:09.263127
1667	14	112	107	2018-04-14 20:56:09.270689	2018-04-14 20:56:09.270689
1668	14	112	109	2018-04-14 20:56:09.275961	2018-04-14 20:56:09.275961
1669	14	112	110	2018-04-14 20:56:09.280719	2018-04-14 20:56:09.280719
1670	14	112	111	2018-04-14 20:56:09.287508	2018-04-14 20:56:09.287508
1671	15	10	73	2018-04-14 20:56:09.293094	2018-04-14 20:56:09.293094
1672	15	43	66	2018-04-14 20:56:09.298184	2018-04-14 20:56:09.298184
1673	15	66	1	2018-04-14 20:56:09.305209	2018-04-14 20:56:09.305209
1674	15	66	2	2018-04-14 20:56:09.310645	2018-04-14 20:56:09.310645
1675	15	66	4	2018-04-14 20:56:09.316733	2018-04-14 20:56:09.316733
1676	15	66	5	2018-04-14 20:56:09.328	2018-04-14 20:56:09.328
1677	15	66	8	2018-04-14 20:56:09.336301	2018-04-14 20:56:09.336301
1678	15	66	11	2018-04-14 20:56:09.343079	2018-04-14 20:56:09.343079
1679	15	66	15	2018-04-14 20:56:09.349151	2018-04-14 20:56:09.349151
1680	15	66	16	2018-04-14 20:56:09.35665	2018-04-14 20:56:09.35665
1681	15	66	17	2018-04-14 20:56:09.361912	2018-04-14 20:56:09.361912
1682	15	66	24	2018-04-14 20:56:09.367656	2018-04-14 20:56:09.367656
1683	15	66	25	2018-04-14 20:56:09.374491	2018-04-14 20:56:09.374491
1684	15	66	27	2018-04-14 20:56:09.37952	2018-04-14 20:56:09.37952
1685	15	66	30	2018-04-14 20:56:09.386322	2018-04-14 20:56:09.386322
1686	15	66	31	2018-04-14 20:56:09.392443	2018-04-14 20:56:09.392443
1687	15	66	32	2018-04-14 20:56:09.397044	2018-04-14 20:56:09.397044
1688	15	66	36	2018-04-14 20:56:09.403728	2018-04-14 20:56:09.403728
1689	15	66	39	2018-04-14 20:56:09.409945	2018-04-14 20:56:09.409945
1690	15	66	40	2018-04-14 20:56:09.415077	2018-04-14 20:56:09.415077
1691	15	66	41	2018-04-14 20:56:09.422405	2018-04-14 20:56:09.422405
1692	15	66	42	2018-04-14 20:56:09.428004	2018-04-14 20:56:09.428004
1693	15	66	44	2018-04-14 20:56:09.433663	2018-04-14 20:56:09.433663
1694	15	66	46	2018-04-14 20:56:09.440918	2018-04-14 20:56:09.440918
1695	15	66	48	2018-04-14 20:56:09.446224	2018-04-14 20:56:09.446224
1696	15	66	52	2018-04-14 20:56:09.452359	2018-04-14 20:56:09.452359
1697	15	66	59	2018-04-14 20:56:09.458909	2018-04-14 20:56:09.458909
1698	15	66	60	2018-04-14 20:56:09.463879	2018-04-14 20:56:09.463879
1699	15	66	61	2018-04-14 20:56:09.470421	2018-04-14 20:56:09.470421
1700	15	66	66	2018-04-14 20:56:09.475718	2018-04-14 20:56:09.475718
1701	15	66	70	2018-04-14 20:56:09.480623	2018-04-14 20:56:09.480623
1702	15	66	72	2018-04-14 20:56:09.486437	2018-04-14 20:56:09.486437
1703	15	66	74	2018-04-14 20:56:09.491513	2018-04-14 20:56:09.491513
1704	15	66	76	2018-04-14 20:56:09.496722	2018-04-14 20:56:09.496722
1705	15	66	77	2018-04-14 20:56:09.503512	2018-04-14 20:56:09.503512
1706	15	66	78	2018-04-14 20:56:09.508849	2018-04-14 20:56:09.508849
1707	15	66	79	2018-04-14 20:56:09.514267	2018-04-14 20:56:09.514267
1708	15	66	82	2018-04-14 20:56:09.520115	2018-04-14 20:56:09.520115
1709	15	66	83	2018-04-14 20:56:09.525452	2018-04-14 20:56:09.525452
1710	15	66	84	2018-04-14 20:56:09.536463	2018-04-14 20:56:09.536463
1711	15	66	85	2018-04-14 20:56:09.542645	2018-04-14 20:56:09.542645
1712	15	66	86	2018-04-14 20:56:09.548226	2018-04-14 20:56:09.548226
1713	15	66	88	2018-04-14 20:56:09.554895	2018-04-14 20:56:09.554895
1714	15	66	89	2018-04-14 20:56:09.560344	2018-04-14 20:56:09.560344
1715	15	66	90	2018-04-14 20:56:09.566025	2018-04-14 20:56:09.566025
1716	15	66	91	2018-04-14 20:56:09.572359	2018-04-14 20:56:09.572359
1717	15	66	93	2018-04-14 20:56:09.577863	2018-04-14 20:56:09.577863
1718	15	66	95	2018-04-14 20:56:09.583388	2018-04-14 20:56:09.583388
1719	15	66	98	2018-04-14 20:56:09.589844	2018-04-14 20:56:09.589844
1720	15	66	102	2018-04-14 20:56:09.595608	2018-04-14 20:56:09.595608
1721	15	67	66	2018-04-14 20:56:09.601292	2018-04-14 20:56:09.601292
1722	15	73	3	2018-04-14 20:56:09.607757	2018-04-14 20:56:09.607757
1723	15	73	6	2018-04-14 20:56:09.613214	2018-04-14 20:56:09.613214
1724	15	73	11	2018-04-14 20:56:09.619035	2018-04-14 20:56:09.619035
1725	15	73	12	2018-04-14 20:56:09.625093	2018-04-14 20:56:09.625093
1726	15	73	18	2018-04-14 20:56:09.630636	2018-04-14 20:56:09.630636
1727	15	73	20	2018-04-14 20:56:09.637212	2018-04-14 20:56:09.637212
1728	15	73	22	2018-04-14 20:56:09.643198	2018-04-14 20:56:09.643198
1729	15	73	29	2018-04-14 20:56:09.648915	2018-04-14 20:56:09.648915
1730	15	73	31	2018-04-14 20:56:09.655591	2018-04-14 20:56:09.655591
1731	15	73	32	2018-04-14 20:56:09.664686	2018-04-14 20:56:09.664686
1732	15	73	37	2018-04-14 20:56:09.679026	2018-04-14 20:56:09.679026
1733	15	73	38	2018-04-14 20:56:09.690745	2018-04-14 20:56:09.690745
1734	15	73	45	2018-04-14 20:56:09.701495	2018-04-14 20:56:09.701495
1735	15	73	46	2018-04-14 20:56:09.708009	2018-04-14 20:56:09.708009
1736	15	73	53	2018-04-14 20:56:09.714084	2018-04-14 20:56:09.714084
1737	15	73	54	2018-04-14 20:56:09.720975	2018-04-14 20:56:09.720975
1738	15	73	55	2018-04-14 20:56:09.728542	2018-04-14 20:56:09.728542
1739	15	73	56	2018-04-14 20:56:09.734619	2018-04-14 20:56:09.734619
1740	15	73	57	2018-04-14 20:56:09.743294	2018-04-14 20:56:09.743294
1741	15	73	58	2018-04-14 20:56:09.752776	2018-04-14 20:56:09.752776
1742	15	73	65	2018-04-14 20:56:09.761761	2018-04-14 20:56:09.761761
1743	15	73	69	2018-04-14 20:56:09.770152	2018-04-14 20:56:09.770152
1744	15	73	70	2018-04-14 20:56:09.77731	2018-04-14 20:56:09.77731
1745	15	73	71	2018-04-14 20:56:09.784126	2018-04-14 20:56:09.784126
1746	15	73	81	2018-04-14 20:56:09.791237	2018-04-14 20:56:09.791237
1747	15	73	83	2018-04-14 20:56:09.798355	2018-04-14 20:56:09.798355
1748	15	73	84	2018-04-14 20:56:09.805099	2018-04-14 20:56:09.805099
1749	15	73	87	2018-04-14 20:56:09.815814	2018-04-14 20:56:09.815814
1750	15	73	92	2018-04-14 20:56:09.823712	2018-04-14 20:56:09.823712
1751	15	73	94	2018-04-14 20:56:09.831787	2018-04-14 20:56:09.831787
1752	15	73	99	2018-04-14 20:56:09.839184	2018-04-14 20:56:09.839184
1753	15	73	101	2018-04-14 20:56:09.847176	2018-04-14 20:56:09.847176
1754	15	73	106	2018-04-14 20:56:09.855374	2018-04-14 20:56:09.855374
1755	15	73	107	2018-04-14 20:56:09.862895	2018-04-14 20:56:09.862895
1756	15	73	109	2018-04-14 20:56:09.87096	2018-04-14 20:56:09.87096
1757	15	73	110	2018-04-14 20:56:09.881725	2018-04-14 20:56:09.881725
1758	15	73	111	2018-04-14 20:56:09.889854	2018-04-14 20:56:09.889854
1759	15	75	66	2018-04-14 20:56:09.897669	2018-04-14 20:56:09.897669
1760	15	112	66	2018-04-14 20:56:09.906739	2018-04-14 20:56:09.906739
1761	16	1	33	2018-04-14 20:56:09.915401	2018-04-14 20:56:09.915401
1762	16	5	13	2018-04-14 20:56:09.92465	2018-04-14 20:56:09.92465
1763	16	5	33	2018-04-14 20:56:09.931915	2018-04-14 20:56:09.931915
1764	16	8	33	2018-04-14 20:56:09.941213	2018-04-14 20:56:09.941213
1765	16	13	13	2018-04-14 20:56:09.948564	2018-04-14 20:56:09.948564
1766	16	13	18	2018-04-14 20:56:09.95828	2018-04-14 20:56:09.95828
1767	16	13	33	2018-04-14 20:56:09.966174	2018-04-14 20:56:09.966174
1768	16	13	36	2018-04-14 20:56:09.975056	2018-04-14 20:56:09.975056
1769	16	13	53	2018-04-14 20:56:09.982438	2018-04-14 20:56:09.982438
1770	16	13	103	2018-04-14 20:56:10.003272	2018-04-14 20:56:10.003272
1771	16	14	13	2018-04-14 20:56:10.00819	2018-04-14 20:56:10.00819
1772	16	14	18	2018-04-14 20:56:10.01272	2018-04-14 20:56:10.01272
1773	16	14	33	2018-04-14 20:56:10.017017	2018-04-14 20:56:10.017017
1774	16	14	54	2018-04-14 20:56:10.021613	2018-04-14 20:56:10.021613
1775	16	25	33	2018-04-14 20:56:10.025965	2018-04-14 20:56:10.025965
1776	16	30	33	2018-04-14 20:56:10.030494	2018-04-14 20:56:10.030494
1777	16	33	18	2018-04-14 20:56:10.036947	2018-04-14 20:56:10.036947
1778	16	33	33	2018-04-14 20:56:10.041648	2018-04-14 20:56:10.041648
1779	16	33	36	2018-04-14 20:56:10.04597	2018-04-14 20:56:10.04597
1780	16	33	40	2018-04-14 20:56:10.050477	2018-04-14 20:56:10.050477
1781	16	42	13	2018-04-14 20:56:10.055203	2018-04-14 20:56:10.055203
1782	16	42	14	2018-04-14 20:56:10.059955	2018-04-14 20:56:10.059955
1783	16	42	33	2018-04-14 20:56:10.064518	2018-04-14 20:56:10.064518
1784	16	44	33	2018-04-14 20:56:10.069479	2018-04-14 20:56:10.069479
1785	16	59	33	2018-04-14 20:56:10.074711	2018-04-14 20:56:10.074711
1786	16	76	33	2018-04-14 20:56:10.079593	2018-04-14 20:56:10.079593
1787	16	79	33	2018-04-14 20:56:10.084623	2018-04-14 20:56:10.084623
1788	16	90	33	2018-04-14 20:56:10.089734	2018-04-14 20:56:10.089734
1789	16	93	33	2018-04-14 20:56:10.09444	2018-04-14 20:56:10.09444
1790	16	102	13	2018-04-14 20:56:10.099099	2018-04-14 20:56:10.099099
1791	16	102	14	2018-04-14 20:56:10.104109	2018-04-14 20:56:10.104109
1792	16	102	33	2018-04-14 20:56:10.108841	2018-04-14 20:56:10.108841
1793	17	1	108	2018-04-14 20:56:10.114011	2018-04-14 20:56:10.114011
1794	17	5	23	2018-04-14 20:56:10.119435	2018-04-14 20:56:10.119435
1795	17	5	108	2018-04-14 20:56:10.124203	2018-04-14 20:56:10.124203
1796	17	8	108	2018-04-14 20:56:10.12897	2018-04-14 20:56:10.12897
1797	17	10	108	2018-04-14 20:56:10.134805	2018-04-14 20:56:10.134805
1798	17	23	12	2018-04-14 20:56:10.141028	2018-04-14 20:56:10.141028
1799	17	23	18	2018-04-14 20:56:10.146065	2018-04-14 20:56:10.146065
1800	17	23	23	2018-04-14 20:56:10.150679	2018-04-14 20:56:10.150679
1801	17	23	36	2018-04-14 20:56:10.155754	2018-04-14 20:56:10.155754
1802	17	23	53	2018-04-14 20:56:10.160422	2018-04-14 20:56:10.160422
1803	17	23	87	2018-04-14 20:56:10.164992	2018-04-14 20:56:10.164992
1804	17	23	92	2018-04-14 20:56:10.170018	2018-04-14 20:56:10.170018
1805	17	23	103	2018-04-14 20:56:10.174856	2018-04-14 20:56:10.174856
1806	17	23	108	2018-04-14 20:56:10.179764	2018-04-14 20:56:10.179764
1807	17	23	109	2018-04-14 20:56:10.184311	2018-04-14 20:56:10.184311
1808	17	25	108	2018-04-14 20:56:10.188866	2018-04-14 20:56:10.188866
1809	17	30	108	2018-04-14 20:56:10.193821	2018-04-14 20:56:10.193821
1810	17	34	18	2018-04-14 20:56:10.198291	2018-04-14 20:56:10.198291
1811	17	34	23	2018-04-14 20:56:10.202979	2018-04-14 20:56:10.202979
1812	17	34	34	2018-04-14 20:56:10.207767	2018-04-14 20:56:10.207767
1813	17	34	54	2018-04-14 20:56:10.212322	2018-04-14 20:56:10.212322
1814	17	34	108	2018-04-14 20:56:10.217058	2018-04-14 20:56:10.217058
1815	17	36	36	2018-04-14 20:56:10.221961	2018-04-14 20:56:10.221961
1816	17	40	36	2018-04-14 20:56:10.22681	2018-04-14 20:56:10.22681
1817	17	40	40	2018-04-14 20:56:10.231412	2018-04-14 20:56:10.231412
1818	17	42	23	2018-04-14 20:56:10.236251	2018-04-14 20:56:10.236251
1819	17	42	34	2018-04-14 20:56:10.241121	2018-04-14 20:56:10.241121
1820	17	42	108	2018-04-14 20:56:10.246018	2018-04-14 20:56:10.246018
1821	17	44	108	2018-04-14 20:56:10.25071	2018-04-14 20:56:10.25071
1822	17	59	108	2018-04-14 20:56:10.255261	2018-04-14 20:56:10.255261
1823	17	76	108	2018-04-14 20:56:10.259927	2018-04-14 20:56:10.259927
1824	17	79	108	2018-04-14 20:56:10.264541	2018-04-14 20:56:10.264541
1825	17	90	108	2018-04-14 20:56:10.269237	2018-04-14 20:56:10.269237
1826	17	93	108	2018-04-14 20:56:10.273941	2018-04-14 20:56:10.273941
1827	17	102	23	2018-04-14 20:56:10.280818	2018-04-14 20:56:10.280818
1828	17	102	34	2018-04-14 20:56:10.286237	2018-04-14 20:56:10.286237
1829	17	102	108	2018-04-14 20:56:10.291202	2018-04-14 20:56:10.291202
1830	17	108	2	2018-04-14 20:56:10.2964	2018-04-14 20:56:10.2964
1831	17	108	4	2018-04-14 20:56:10.303029	2018-04-14 20:56:10.303029
1832	17	108	18	2018-04-14 20:56:10.308993	2018-04-14 20:56:10.308993
1833	17	108	36	2018-04-14 20:56:10.314716	2018-04-14 20:56:10.314716
1834	17	108	40	2018-04-14 20:56:10.320669	2018-04-14 20:56:10.320669
1835	17	108	60	2018-04-14 20:56:10.328127	2018-04-14 20:56:10.328127
1836	17	108	77	2018-04-14 20:56:10.333746	2018-04-14 20:56:10.333746
1837	17	108	108	2018-04-14 20:56:10.340272	2018-04-14 20:56:10.340272
1838	18	2	2	2018-04-14 20:56:10.347206	2018-04-14 20:56:10.347206
1839	18	4	2	2018-04-14 20:56:10.35243	2018-04-14 20:56:10.35243
1840	18	4	4	2018-04-14 20:56:10.359447	2018-04-14 20:56:10.359447
1841	18	60	2	2018-04-14 20:56:10.365705	2018-04-14 20:56:10.365705
1842	18	60	4	2018-04-14 20:56:10.375249	2018-04-14 20:56:10.375249
1843	18	60	60	2018-04-14 20:56:10.380739	2018-04-14 20:56:10.380739
1844	18	77	2	2018-04-14 20:56:10.385572	2018-04-14 20:56:10.385572
1845	18	77	4	2018-04-14 20:56:10.390571	2018-04-14 20:56:10.390571
1846	18	77	60	2018-04-14 20:56:10.395122	2018-04-14 20:56:10.395122
1847	18	77	77	2018-04-14 20:56:10.400775	2018-04-14 20:56:10.400775
1848	19	28	32	2018-04-14 20:56:10.405169	2018-04-14 20:56:10.405169
1849	19	28	66	2018-04-14 20:56:10.409611	2018-04-14 20:56:10.409611
1850	19	28	79	2018-04-14 20:56:10.414347	2018-04-14 20:56:10.414347
1851	20	1	105	2018-04-14 20:56:10.418784	2018-04-14 20:56:10.418784
1852	20	5	105	2018-04-14 20:56:10.423598	2018-04-14 20:56:10.423598
1853	20	7	105	2018-04-14 20:56:10.430982	2018-04-14 20:56:10.430982
1854	20	8	105	2018-04-14 20:56:10.435994	2018-04-14 20:56:10.435994
1855	20	9	105	2018-04-14 20:56:10.440348	2018-04-14 20:56:10.440348
1856	20	10	103	2018-04-14 20:56:10.444684	2018-04-14 20:56:10.444684
1857	20	10	105	2018-04-14 20:56:10.44952	2018-04-14 20:56:10.44952
1858	20	13	105	2018-04-14 20:56:10.453925	2018-04-14 20:56:10.453925
1859	20	14	105	2018-04-14 20:56:10.458221	2018-04-14 20:56:10.458221
1860	20	15	105	2018-04-14 20:56:10.462897	2018-04-14 20:56:10.462897
1861	20	16	105	2018-04-14 20:56:10.46762	2018-04-14 20:56:10.46762
1862	20	17	105	2018-04-14 20:56:10.472151	2018-04-14 20:56:10.472151
1863	20	21	105	2018-04-14 20:56:10.476413	2018-04-14 20:56:10.476413
1864	20	23	105	2018-04-14 20:56:10.481194	2018-04-14 20:56:10.481194
1865	20	24	105	2018-04-14 20:56:10.485873	2018-04-14 20:56:10.485873
1866	20	25	105	2018-04-14 20:56:10.490479	2018-04-14 20:56:10.490479
1867	20	26	105	2018-04-14 20:56:10.495757	2018-04-14 20:56:10.495757
1868	20	27	105	2018-04-14 20:56:10.500534	2018-04-14 20:56:10.500534
1869	20	28	6	2018-04-14 20:56:10.507404	2018-04-14 20:56:10.507404
1870	20	28	11	2018-04-14 20:56:10.514315	2018-04-14 20:56:10.514315
1871	20	28	22	2018-04-14 20:56:10.519276	2018-04-14 20:56:10.519276
1872	20	28	38	2018-04-14 20:56:10.524024	2018-04-14 20:56:10.524024
1873	20	28	70	2018-04-14 20:56:10.52923	2018-04-14 20:56:10.52923
1874	20	28	71	2018-04-14 20:56:10.534043	2018-04-14 20:56:10.534043
1875	20	28	83	2018-04-14 20:56:10.539237	2018-04-14 20:56:10.539237
1876	20	28	96	2018-04-14 20:56:10.544292	2018-04-14 20:56:10.544292
1877	20	28	100	2018-04-14 20:56:10.549344	2018-04-14 20:56:10.549344
1878	20	28	105	2018-04-14 20:56:10.554076	2018-04-14 20:56:10.554076
1879	20	28	106	2018-04-14 20:56:10.558611	2018-04-14 20:56:10.558611
1880	20	28	110	2018-04-14 20:56:10.56436	2018-04-14 20:56:10.56436
1881	20	28	111	2018-04-14 20:56:10.569456	2018-04-14 20:56:10.569456
1882	20	30	105	2018-04-14 20:56:10.575018	2018-04-14 20:56:10.575018
1883	20	31	105	2018-04-14 20:56:10.579968	2018-04-14 20:56:10.579968
1884	20	32	105	2018-04-14 20:56:10.585418	2018-04-14 20:56:10.585418
1885	20	33	105	2018-04-14 20:56:10.590338	2018-04-14 20:56:10.590338
1886	20	34	105	2018-04-14 20:56:10.595494	2018-04-14 20:56:10.595494
1887	20	36	105	2018-04-14 20:56:10.60045	2018-04-14 20:56:10.60045
1888	20	39	105	2018-04-14 20:56:10.604883	2018-04-14 20:56:10.604883
1889	20	40	105	2018-04-14 20:56:10.609498	2018-04-14 20:56:10.609498
1890	20	41	105	2018-04-14 20:56:10.614079	2018-04-14 20:56:10.614079
1891	20	42	105	2018-04-14 20:56:10.619071	2018-04-14 20:56:10.619071
1892	20	43	105	2018-04-14 20:56:10.630574	2018-04-14 20:56:10.630574
1893	20	44	105	2018-04-14 20:56:10.642041	2018-04-14 20:56:10.642041
1894	20	47	105	2018-04-14 20:56:10.646949	2018-04-14 20:56:10.646949
1895	20	48	105	2018-04-14 20:56:10.651645	2018-04-14 20:56:10.651645
1896	20	49	105	2018-04-14 20:56:10.656778	2018-04-14 20:56:10.656778
1897	20	50	105	2018-04-14 20:56:10.661779	2018-04-14 20:56:10.661779
1898	20	52	105	2018-04-14 20:56:10.666693	2018-04-14 20:56:10.666693
1899	20	59	105	2018-04-14 20:56:10.67172	2018-04-14 20:56:10.67172
1900	20	61	105	2018-04-14 20:56:10.676603	2018-04-14 20:56:10.676603
1901	20	67	105	2018-04-14 20:56:10.681537	2018-04-14 20:56:10.681537
1902	20	72	105	2018-04-14 20:56:10.686007	2018-04-14 20:56:10.686007
1903	20	73	105	2018-04-14 20:56:10.690444	2018-04-14 20:56:10.690444
1904	20	74	105	2018-04-14 20:56:10.69507	2018-04-14 20:56:10.69507
1905	20	75	105	2018-04-14 20:56:10.699694	2018-04-14 20:56:10.699694
1906	20	76	105	2018-04-14 20:56:10.704168	2018-04-14 20:56:10.704168
1907	20	78	105	2018-04-14 20:56:10.708672	2018-04-14 20:56:10.708672
1908	20	79	105	2018-04-14 20:56:10.712925	2018-04-14 20:56:10.712925
1909	20	82	105	2018-04-14 20:56:10.717814	2018-04-14 20:56:10.717814
1910	20	85	105	2018-04-14 20:56:10.722622	2018-04-14 20:56:10.722622
1911	20	86	105	2018-04-14 20:56:10.727519	2018-04-14 20:56:10.727519
1912	20	88	105	2018-04-14 20:56:10.732904	2018-04-14 20:56:10.732904
1913	20	89	105	2018-04-14 20:56:10.73864	2018-04-14 20:56:10.73864
1914	20	90	105	2018-04-14 20:56:10.743967	2018-04-14 20:56:10.743967
1915	20	91	105	2018-04-14 20:56:10.751383	2018-04-14 20:56:10.751383
1916	20	93	105	2018-04-14 20:56:10.756557	2018-04-14 20:56:10.756557
1917	20	95	105	2018-04-14 20:56:10.762573	2018-04-14 20:56:10.762573
1918	20	98	105	2018-04-14 20:56:10.76835	2018-04-14 20:56:10.76835
1919	20	102	105	2018-04-14 20:56:10.774202	2018-04-14 20:56:10.774202
1920	20	103	2	2018-04-14 20:56:10.781003	2018-04-14 20:56:10.781003
1921	20	103	3	2018-04-14 20:56:10.786417	2018-04-14 20:56:10.786417
1922	20	103	4	2018-04-14 20:56:10.792878	2018-04-14 20:56:10.792878
1923	20	103	12	2018-04-14 20:56:10.799282	2018-04-14 20:56:10.799282
1924	20	103	18	2018-04-14 20:56:10.804798	2018-04-14 20:56:10.804798
1925	20	103	20	2018-04-14 20:56:10.811611	2018-04-14 20:56:10.811611
1926	20	103	29	2018-04-14 20:56:10.817405	2018-04-14 20:56:10.817405
1927	20	103	37	2018-04-14 20:56:10.822831	2018-04-14 20:56:10.822831
1928	20	103	45	2018-04-14 20:56:10.830354	2018-04-14 20:56:10.830354
1929	20	103	46	2018-04-14 20:56:10.8358	2018-04-14 20:56:10.8358
1930	20	103	53	2018-04-14 20:56:10.841826	2018-04-14 20:56:10.841826
1931	20	103	54	2018-04-14 20:56:10.848118	2018-04-14 20:56:10.848118
1932	20	103	55	2018-04-14 20:56:10.854166	2018-04-14 20:56:10.854166
1933	20	103	56	2018-04-14 20:56:10.861218	2018-04-14 20:56:10.861218
1934	20	103	57	2018-04-14 20:56:10.867475	2018-04-14 20:56:10.867475
1935	20	103	58	2018-04-14 20:56:10.872905	2018-04-14 20:56:10.872905
1936	20	103	60	2018-04-14 20:56:10.880229	2018-04-14 20:56:10.880229
1937	20	103	64	2018-04-14 20:56:10.886242	2018-04-14 20:56:10.886242
1938	20	103	65	2018-04-14 20:56:10.900691	2018-04-14 20:56:10.900691
1939	20	103	69	2018-04-14 20:56:10.906167	2018-04-14 20:56:10.906167
1940	20	103	70	2018-04-14 20:56:10.912826	2018-04-14 20:56:10.912826
1941	20	103	71	2018-04-14 20:56:10.919283	2018-04-14 20:56:10.919283
1942	20	103	77	2018-04-14 20:56:10.924712	2018-04-14 20:56:10.924712
1943	20	103	80	2018-04-14 20:56:10.930848	2018-04-14 20:56:10.930848
1944	20	103	81	2018-04-14 20:56:10.936692	2018-04-14 20:56:10.936692
1945	20	103	84	2018-04-14 20:56:10.942054	2018-04-14 20:56:10.942054
1946	20	103	87	2018-04-14 20:56:10.946706	2018-04-14 20:56:10.946706
1947	20	103	92	2018-04-14 20:56:10.952828	2018-04-14 20:56:10.952828
1948	20	103	94	2018-04-14 20:56:10.958525	2018-04-14 20:56:10.958525
1949	20	103	99	2018-04-14 20:56:10.964623	2018-04-14 20:56:10.964623
1950	20	103	101	2018-04-14 20:56:10.969857	2018-04-14 20:56:10.969857
1951	20	103	107	2018-04-14 20:56:10.975242	2018-04-14 20:56:10.975242
1952	20	103	109	2018-04-14 20:56:10.982011	2018-04-14 20:56:10.982011
1953	20	104	6	2018-04-14 20:56:10.987091	2018-04-14 20:56:10.987091
1954	20	104	11	2018-04-14 20:56:10.993572	2018-04-14 20:56:10.993572
1955	20	104	22	2018-04-14 20:56:10.999783	2018-04-14 20:56:10.999783
1956	20	104	32	2018-04-14 20:56:11.005027	2018-04-14 20:56:11.005027
1957	20	104	38	2018-04-14 20:56:11.011248	2018-04-14 20:56:11.011248
1958	20	104	66	2018-04-14 20:56:11.017342	2018-04-14 20:56:11.017342
1959	20	104	70	2018-04-14 20:56:11.022521	2018-04-14 20:56:11.022521
1960	20	104	71	2018-04-14 20:56:11.029915	2018-04-14 20:56:11.029915
1961	20	104	79	2018-04-14 20:56:11.03544	2018-04-14 20:56:11.03544
1962	20	104	83	2018-04-14 20:56:11.04034	2018-04-14 20:56:11.04034
1963	20	104	96	2018-04-14 20:56:11.046441	2018-04-14 20:56:11.046441
1964	20	104	100	2018-04-14 20:56:11.051837	2018-04-14 20:56:11.051837
1965	20	104	105	2018-04-14 20:56:11.056968	2018-04-14 20:56:11.056968
1966	20	104	106	2018-04-14 20:56:11.063921	2018-04-14 20:56:11.063921
1967	20	104	110	2018-04-14 20:56:11.069042	2018-04-14 20:56:11.069042
1968	20	104	111	2018-04-14 20:56:11.073823	2018-04-14 20:56:11.073823
1969	20	105	2	2018-04-14 20:56:11.079177	2018-04-14 20:56:11.079177
1970	20	105	3	2018-04-14 20:56:11.084684	2018-04-14 20:56:11.084684
1971	20	105	4	2018-04-14 20:56:11.090775	2018-04-14 20:56:11.090775
1972	20	105	12	2018-04-14 20:56:11.097006	2018-04-14 20:56:11.097006
1973	20	105	18	2018-04-14 20:56:11.10262	2018-04-14 20:56:11.10262
1974	20	105	20	2018-04-14 20:56:11.107656	2018-04-14 20:56:11.107656
1975	20	105	29	2018-04-14 20:56:11.114593	2018-04-14 20:56:11.114593
1976	20	105	37	2018-04-14 20:56:11.120482	2018-04-14 20:56:11.120482
1977	20	105	45	2018-04-14 20:56:11.125618	2018-04-14 20:56:11.125618
1978	20	105	46	2018-04-14 20:56:11.131912	2018-04-14 20:56:11.131912
1979	20	105	53	2018-04-14 20:56:11.137059	2018-04-14 20:56:11.137059
1980	20	105	54	2018-04-14 20:56:11.142021	2018-04-14 20:56:11.142021
1981	20	105	55	2018-04-14 20:56:11.148681	2018-04-14 20:56:11.148681
1982	20	105	56	2018-04-14 20:56:11.15353	2018-04-14 20:56:11.15353
1983	20	105	57	2018-04-14 20:56:11.158594	2018-04-14 20:56:11.158594
1984	20	105	58	2018-04-14 20:56:11.166481	2018-04-14 20:56:11.166481
1985	20	105	60	2018-04-14 20:56:11.174166	2018-04-14 20:56:11.174166
1986	20	105	64	2018-04-14 20:56:11.179857	2018-04-14 20:56:11.179857
1987	20	105	65	2018-04-14 20:56:11.184742	2018-04-14 20:56:11.184742
1988	20	105	69	2018-04-14 20:56:11.189546	2018-04-14 20:56:11.189546
1989	20	105	70	2018-04-14 20:56:11.194463	2018-04-14 20:56:11.194463
1990	20	105	71	2018-04-14 20:56:11.199089	2018-04-14 20:56:11.199089
1991	20	105	77	2018-04-14 20:56:11.204475	2018-04-14 20:56:11.204475
1992	20	105	80	2018-04-14 20:56:11.209617	2018-04-14 20:56:11.209617
1993	20	105	81	2018-04-14 20:56:11.214864	2018-04-14 20:56:11.214864
1994	20	105	84	2018-04-14 20:56:11.22008	2018-04-14 20:56:11.22008
1995	20	105	87	2018-04-14 20:56:11.224659	2018-04-14 20:56:11.224659
1996	20	105	92	2018-04-14 20:56:11.23056	2018-04-14 20:56:11.23056
1997	20	105	94	2018-04-14 20:56:11.235833	2018-04-14 20:56:11.235833
1998	20	105	99	2018-04-14 20:56:11.24077	2018-04-14 20:56:11.24077
1999	20	105	101	2018-04-14 20:56:11.246398	2018-04-14 20:56:11.246398
2000	20	105	103	2018-04-14 20:56:11.251588	2018-04-14 20:56:11.251588
2001	20	105	105	2018-04-14 20:56:11.256536	2018-04-14 20:56:11.256536
2002	20	105	107	2018-04-14 20:56:11.262745	2018-04-14 20:56:11.262745
2003	20	105	109	2018-04-14 20:56:11.268186	2018-04-14 20:56:11.268186
2004	20	108	105	2018-04-14 20:56:11.27334	2018-04-14 20:56:11.27334
2005	20	112	105	2018-04-14 20:56:11.280346	2018-04-14 20:56:11.280346
2006	21	7	14	2018-04-14 20:56:11.285656	2018-04-14 20:56:11.285656
2007	21	9	13	2018-04-14 20:56:11.290611	2018-04-14 20:56:11.290611
2008	21	26	34	2018-04-14 20:56:11.297974	2018-04-14 20:56:11.297974
2009	21	47	33	2018-04-14 20:56:11.304303	2018-04-14 20:56:11.304303
2010	21	49	108	2018-04-14 20:56:11.309032	2018-04-14 20:56:11.309032
2011	21	50	23	2018-04-14 20:56:11.314921	2018-04-14 20:56:11.314921
2012	22	7	15	2018-04-14 20:56:11.320126	2018-04-14 20:56:11.320126
2013	22	7	16	2018-04-14 20:56:11.324886	2018-04-14 20:56:11.324886
2014	22	7	17	2018-04-14 20:56:11.330677	2018-04-14 20:56:11.330677
2015	22	7	24	2018-04-14 20:56:11.336025	2018-04-14 20:56:11.336025
2016	22	7	31	2018-04-14 20:56:11.340693	2018-04-14 20:56:11.340693
2017	22	7	32	2018-04-14 20:56:11.346326	2018-04-14 20:56:11.346326
2018	22	7	39	2018-04-14 20:56:11.351569	2018-04-14 20:56:11.351569
2019	22	7	41	2018-04-14 20:56:11.356586	2018-04-14 20:56:11.356586
2020	22	7	46	2018-04-14 20:56:11.362377	2018-04-14 20:56:11.362377
2021	22	7	61	2018-04-14 20:56:11.367923	2018-04-14 20:56:11.367923
2022	22	7	70	2018-04-14 20:56:11.373331	2018-04-14 20:56:11.373331
2023	22	7	72	2018-04-14 20:56:11.379276	2018-04-14 20:56:11.379276
2024	22	7	74	2018-04-14 20:56:11.386034	2018-04-14 20:56:11.386034
2025	22	7	78	2018-04-14 20:56:11.39125	2018-04-14 20:56:11.39125
2026	22	7	84	2018-04-14 20:56:11.397508	2018-04-14 20:56:11.397508
2027	22	7	85	2018-04-14 20:56:11.402967	2018-04-14 20:56:11.402967
2028	22	7	86	2018-04-14 20:56:11.40815	2018-04-14 20:56:11.40815
2029	22	7	88	2018-04-14 20:56:11.412865	2018-04-14 20:56:11.412865
2030	22	7	89	2018-04-14 20:56:11.417527	2018-04-14 20:56:11.417527
2031	22	7	91	2018-04-14 20:56:11.422854	2018-04-14 20:56:11.422854
2032	22	7	95	2018-04-14 20:56:11.428162	2018-04-14 20:56:11.428162
2033	22	9	15	2018-04-14 20:56:11.432818	2018-04-14 20:56:11.432818
2034	22	9	16	2018-04-14 20:56:11.438011	2018-04-14 20:56:11.438011
2035	22	9	17	2018-04-14 20:56:11.443369	2018-04-14 20:56:11.443369
2036	22	9	39	2018-04-14 20:56:11.448499	2018-04-14 20:56:11.448499
2037	22	9	41	2018-04-14 20:56:11.454404	2018-04-14 20:56:11.454404
2038	22	9	46	2018-04-14 20:56:11.460081	2018-04-14 20:56:11.460081
2039	22	9	61	2018-04-14 20:56:11.465947	2018-04-14 20:56:11.465947
2040	22	9	70	2018-04-14 20:56:11.47121	2018-04-14 20:56:11.47121
2041	22	9	72	2018-04-14 20:56:11.476899	2018-04-14 20:56:11.476899
2042	22	9	74	2018-04-14 20:56:11.483845	2018-04-14 20:56:11.483845
2043	22	9	78	2018-04-14 20:56:11.489036	2018-04-14 20:56:11.489036
2044	22	9	84	2018-04-14 20:56:11.494432	2018-04-14 20:56:11.494432
2045	22	9	86	2018-04-14 20:56:11.499394	2018-04-14 20:56:11.499394
2046	22	9	88	2018-04-14 20:56:11.504422	2018-04-14 20:56:11.504422
2047	22	9	89	2018-04-14 20:56:11.510024	2018-04-14 20:56:11.510024
2048	22	9	91	2018-04-14 20:56:11.515477	2018-04-14 20:56:11.515477
2049	22	9	95	2018-04-14 20:56:11.520285	2018-04-14 20:56:11.520285
2050	22	47	88	2018-04-14 20:56:11.524806	2018-04-14 20:56:11.524806
2051	23	10	40	2018-04-14 20:56:11.52969	2018-04-14 20:56:11.52969
2052	23	11	2	2018-04-14 20:56:11.53414	2018-04-14 20:56:11.53414
2053	23	11	4	2018-04-14 20:56:11.538671	2018-04-14 20:56:11.538671
2054	23	11	60	2018-04-14 20:56:11.542984	2018-04-14 20:56:11.542984
2055	23	11	77	2018-04-14 20:56:11.547435	2018-04-14 20:56:11.547435
2056	23	15	15	2018-04-14 20:56:11.551979	2018-04-14 20:56:11.551979
2057	23	15	16	2018-04-14 20:56:11.556709	2018-04-14 20:56:11.556709
2058	23	15	17	2018-04-14 20:56:11.562301	2018-04-14 20:56:11.562301
2059	23	15	41	2018-04-14 20:56:11.567004	2018-04-14 20:56:11.567004
2060	23	15	46	2018-04-14 20:56:11.571895	2018-04-14 20:56:11.571895
2061	23	15	48	2018-04-14 20:56:11.57687	2018-04-14 20:56:11.57687
2062	23	15	52	2018-04-14 20:56:11.581682	2018-04-14 20:56:11.581682
2063	23	15	61	2018-04-14 20:56:11.586056	2018-04-14 20:56:11.586056
2064	23	15	70	2018-04-14 20:56:11.591846	2018-04-14 20:56:11.591846
2065	23	15	72	2018-04-14 20:56:11.598462	2018-04-14 20:56:11.598462
2066	23	15	78	2018-04-14 20:56:11.606765	2018-04-14 20:56:11.606765
2067	23	15	84	2018-04-14 20:56:11.613744	2018-04-14 20:56:11.613744
2068	23	15	91	2018-04-14 20:56:11.618766	2018-04-14 20:56:11.618766
2069	23	15	95	2018-04-14 20:56:11.624167	2018-04-14 20:56:11.624167
2070	23	15	98	2018-04-14 20:56:11.629069	2018-04-14 20:56:11.629069
2071	23	24	15	2018-04-14 20:56:11.634812	2018-04-14 20:56:11.634812
2072	23	24	16	2018-04-14 20:56:11.639933	2018-04-14 20:56:11.639933
2073	23	24	17	2018-04-14 20:56:11.644997	2018-04-14 20:56:11.644997
2074	23	24	24	2018-04-14 20:56:11.651483	2018-04-14 20:56:11.651483
2075	23	24	39	2018-04-14 20:56:11.656477	2018-04-14 20:56:11.656477
2076	23	24	41	2018-04-14 20:56:11.661239	2018-04-14 20:56:11.661239
2077	23	24	46	2018-04-14 20:56:11.669488	2018-04-14 20:56:11.669488
2078	23	24	61	2018-04-14 20:56:11.675138	2018-04-14 20:56:11.675138
2079	23	24	64	2018-04-14 20:56:11.683058	2018-04-14 20:56:11.683058
2080	23	24	70	2018-04-14 20:56:11.688934	2018-04-14 20:56:11.688934
2081	23	24	72	2018-04-14 20:56:11.694309	2018-04-14 20:56:11.694309
2082	23	24	74	2018-04-14 20:56:11.702034	2018-04-14 20:56:11.702034
2083	23	24	78	2018-04-14 20:56:11.707896	2018-04-14 20:56:11.707896
2084	23	24	80	2018-04-14 20:56:11.714343	2018-04-14 20:56:11.714343
2085	23	24	84	2018-04-14 20:56:11.720995	2018-04-14 20:56:11.720995
2086	23	24	85	2018-04-14 20:56:11.726827	2018-04-14 20:56:11.726827
2087	23	24	86	2018-04-14 20:56:11.734151	2018-04-14 20:56:11.734151
2088	23	24	89	2018-04-14 20:56:11.740358	2018-04-14 20:56:11.740358
2089	23	24	91	2018-04-14 20:56:11.745693	2018-04-14 20:56:11.745693
2090	23	24	95	2018-04-14 20:56:11.752867	2018-04-14 20:56:11.752867
2091	23	31	2	2018-04-14 20:56:11.758884	2018-04-14 20:56:11.758884
2092	23	31	4	2018-04-14 20:56:11.765894	2018-04-14 20:56:11.765894
2093	23	31	60	2018-04-14 20:56:11.779243	2018-04-14 20:56:11.779243
2094	23	31	64	2018-04-14 20:56:11.786663	2018-04-14 20:56:11.786663
2095	23	31	77	2018-04-14 20:56:11.791799	2018-04-14 20:56:11.791799
2096	23	31	80	2018-04-14 20:56:11.797497	2018-04-14 20:56:11.797497
2097	23	32	2	2018-04-14 20:56:11.804054	2018-04-14 20:56:11.804054
2098	23	32	4	2018-04-14 20:56:11.809415	2018-04-14 20:56:11.809415
2099	23	32	60	2018-04-14 20:56:11.816209	2018-04-14 20:56:11.816209
2100	23	32	64	2018-04-14 20:56:11.822538	2018-04-14 20:56:11.822538
2101	23	32	77	2018-04-14 20:56:11.828057	2018-04-14 20:56:11.828057
2102	23	32	80	2018-04-14 20:56:11.83647	2018-04-14 20:56:11.83647
2103	23	36	2	2018-04-14 20:56:11.842057	2018-04-14 20:56:11.842057
2104	23	36	4	2018-04-14 20:56:11.848125	2018-04-14 20:56:11.848125
2105	23	36	15	2018-04-14 20:56:11.855329	2018-04-14 20:56:11.855329
2106	23	36	16	2018-04-14 20:56:11.860627	2018-04-14 20:56:11.860627
2107	23	36	17	2018-04-14 20:56:11.867797	2018-04-14 20:56:11.867797
2108	23	36	24	2018-04-14 20:56:11.873134	2018-04-14 20:56:11.873134
2109	23	36	31	2018-04-14 20:56:11.878492	2018-04-14 20:56:11.878492
2110	23	36	32	2018-04-14 20:56:11.884075	2018-04-14 20:56:11.884075
2111	23	36	39	2018-04-14 20:56:11.888912	2018-04-14 20:56:11.888912
2112	23	36	46	2018-04-14 20:56:11.893898	2018-04-14 20:56:11.893898
2113	23	36	60	2018-04-14 20:56:11.899701	2018-04-14 20:56:11.899701
2114	23	36	61	2018-04-14 20:56:11.904232	2018-04-14 20:56:11.904232
2115	23	36	64	2018-04-14 20:56:11.90984	2018-04-14 20:56:11.90984
2116	23	36	70	2018-04-14 20:56:11.914901	2018-04-14 20:56:11.914901
2117	23	36	72	2018-04-14 20:56:11.920877	2018-04-14 20:56:11.920877
2118	23	36	74	2018-04-14 20:56:11.925634	2018-04-14 20:56:11.925634
2119	23	36	77	2018-04-14 20:56:11.930698	2018-04-14 20:56:11.930698
2120	23	36	78	2018-04-14 20:56:11.93518	2018-04-14 20:56:11.93518
2121	23	36	80	2018-04-14 20:56:11.93998	2018-04-14 20:56:11.93998
2122	23	36	84	2018-04-14 20:56:11.944569	2018-04-14 20:56:11.944569
2123	23	36	85	2018-04-14 20:56:11.949188	2018-04-14 20:56:11.949188
2124	23	36	86	2018-04-14 20:56:11.953678	2018-04-14 20:56:11.953678
2125	23	36	88	2018-04-14 20:56:11.958143	2018-04-14 20:56:11.958143
2126	23	36	89	2018-04-14 20:56:11.962633	2018-04-14 20:56:11.962633
2127	23	36	91	2018-04-14 20:56:11.967348	2018-04-14 20:56:11.967348
2128	23	36	95	2018-04-14 20:56:11.97209	2018-04-14 20:56:11.97209
2129	23	39	15	2018-04-14 20:56:11.976713	2018-04-14 20:56:11.976713
2130	23	39	16	2018-04-14 20:56:11.981595	2018-04-14 20:56:11.981595
2131	23	39	17	2018-04-14 20:56:11.986248	2018-04-14 20:56:11.986248
2132	23	39	39	2018-04-14 20:56:11.991054	2018-04-14 20:56:11.991054
2133	23	39	41	2018-04-14 20:56:11.996423	2018-04-14 20:56:11.996423
2134	23	39	46	2018-04-14 20:56:12.009308	2018-04-14 20:56:12.009308
2135	23	39	48	2018-04-14 20:56:12.020949	2018-04-14 20:56:12.020949
2136	23	39	52	2018-04-14 20:56:12.030638	2018-04-14 20:56:12.030638
2137	23	39	61	2018-04-14 20:56:12.037524	2018-04-14 20:56:12.037524
2138	23	39	70	2018-04-14 20:56:12.042608	2018-04-14 20:56:12.042608
2139	23	39	72	2018-04-14 20:56:12.049416	2018-04-14 20:56:12.049416
2140	23	39	78	2018-04-14 20:56:12.055831	2018-04-14 20:56:12.055831
2141	23	39	84	2018-04-14 20:56:12.062735	2018-04-14 20:56:12.062735
2142	23	39	91	2018-04-14 20:56:12.069954	2018-04-14 20:56:12.069954
2143	23	39	95	2018-04-14 20:56:12.075733	2018-04-14 20:56:12.075733
2144	23	39	98	2018-04-14 20:56:12.081532	2018-04-14 20:56:12.081532
2145	23	40	12	2018-04-14 20:56:12.087875	2018-04-14 20:56:12.087875
2146	23	40	18	2018-04-14 20:56:12.093084	2018-04-14 20:56:12.093084
2147	23	40	31	2018-04-14 20:56:12.098254	2018-04-14 20:56:12.098254
2148	23	40	32	2018-04-14 20:56:12.105364	2018-04-14 20:56:12.105364
2149	23	40	53	2018-04-14 20:56:12.110883	2018-04-14 20:56:12.110883
2150	23	40	54	2018-04-14 20:56:12.116256	2018-04-14 20:56:12.116256
2151	23	40	64	2018-04-14 20:56:12.12181	2018-04-14 20:56:12.12181
2152	23	40	80	2018-04-14 20:56:12.126619	2018-04-14 20:56:12.126619
2153	23	40	87	2018-04-14 20:56:12.13166	2018-04-14 20:56:12.13166
2154	23	40	92	2018-04-14 20:56:12.138184	2018-04-14 20:56:12.138184
2155	23	40	109	2018-04-14 20:56:12.143907	2018-04-14 20:56:12.143907
2156	23	42	36	2018-04-14 20:56:12.149943	2018-04-14 20:56:12.149943
2157	23	42	40	2018-04-14 20:56:12.155706	2018-04-14 20:56:12.155706
2158	23	64	46	2018-04-14 20:56:12.161073	2018-04-14 20:56:12.161073
2159	23	64	70	2018-04-14 20:56:12.168303	2018-04-14 20:56:12.168303
2160	23	64	84	2018-04-14 20:56:12.174675	2018-04-14 20:56:12.174675
2161	23	72	16	2018-04-14 20:56:12.181118	2018-04-14 20:56:12.181118
2162	23	72	17	2018-04-14 20:56:12.194657	2018-04-14 20:56:12.194657
2163	23	72	41	2018-04-14 20:56:12.206798	2018-04-14 20:56:12.206798
2164	23	72	46	2018-04-14 20:56:12.220853	2018-04-14 20:56:12.220853
2165	23	72	48	2018-04-14 20:56:12.226588	2018-04-14 20:56:12.226588
2166	23	72	52	2018-04-14 20:56:12.231592	2018-04-14 20:56:12.231592
2167	23	72	61	2018-04-14 20:56:12.238421	2018-04-14 20:56:12.238421
2168	23	72	70	2018-04-14 20:56:12.246286	2018-04-14 20:56:12.246286
2169	23	72	78	2018-04-14 20:56:12.253537	2018-04-14 20:56:12.253537
2170	23	72	84	2018-04-14 20:56:12.258921	2018-04-14 20:56:12.258921
2171	23	72	91	2018-04-14 20:56:12.263756	2018-04-14 20:56:12.263756
2172	23	72	95	2018-04-14 20:56:12.268671	2018-04-14 20:56:12.268671
2173	23	72	98	2018-04-14 20:56:12.273588	2018-04-14 20:56:12.273588
2174	23	74	15	2018-04-14 20:56:12.278664	2018-04-14 20:56:12.278664
2175	23	74	16	2018-04-14 20:56:12.284045	2018-04-14 20:56:12.284045
2176	23	74	17	2018-04-14 20:56:12.289173	2018-04-14 20:56:12.289173
2177	23	74	39	2018-04-14 20:56:12.294652	2018-04-14 20:56:12.294652
2178	23	74	41	2018-04-14 20:56:12.299891	2018-04-14 20:56:12.299891
2179	23	74	46	2018-04-14 20:56:12.30463	2018-04-14 20:56:12.30463
2180	23	74	48	2018-04-14 20:56:12.309891	2018-04-14 20:56:12.309891
2181	23	74	52	2018-04-14 20:56:12.315151	2018-04-14 20:56:12.315151
2182	23	74	61	2018-04-14 20:56:12.320152	2018-04-14 20:56:12.320152
2183	23	74	70	2018-04-14 20:56:12.32534	2018-04-14 20:56:12.32534
2184	23	74	72	2018-04-14 20:56:12.331132	2018-04-14 20:56:12.331132
2185	23	74	74	2018-04-14 20:56:12.336986	2018-04-14 20:56:12.336986
2186	23	74	78	2018-04-14 20:56:12.342273	2018-04-14 20:56:12.342273
2187	23	74	84	2018-04-14 20:56:12.346959	2018-04-14 20:56:12.346959
2188	23	74	86	2018-04-14 20:56:12.352157	2018-04-14 20:56:12.352157
2189	23	74	91	2018-04-14 20:56:12.357002	2018-04-14 20:56:12.357002
2190	23	74	95	2018-04-14 20:56:12.36254	2018-04-14 20:56:12.36254
2191	23	74	98	2018-04-14 20:56:12.368048	2018-04-14 20:56:12.368048
2192	23	80	46	2018-04-14 20:56:12.372431	2018-04-14 20:56:12.372431
2193	23	80	70	2018-04-14 20:56:12.379825	2018-04-14 20:56:12.379825
2194	23	80	84	2018-04-14 20:56:12.391098	2018-04-14 20:56:12.391098
2195	23	83	2	2018-04-14 20:56:12.402328	2018-04-14 20:56:12.402328
2196	23	83	4	2018-04-14 20:56:12.407076	2018-04-14 20:56:12.407076
2197	23	83	60	2018-04-14 20:56:12.413704	2018-04-14 20:56:12.413704
2198	23	83	77	2018-04-14 20:56:12.420095	2018-04-14 20:56:12.420095
2199	23	85	15	2018-04-14 20:56:12.425184	2018-04-14 20:56:12.425184
2200	23	85	16	2018-04-14 20:56:12.430406	2018-04-14 20:56:12.430406
2201	23	85	17	2018-04-14 20:56:12.43807	2018-04-14 20:56:12.43807
2202	23	85	39	2018-04-14 20:56:12.44332	2018-04-14 20:56:12.44332
2203	23	85	41	2018-04-14 20:56:12.44846	2018-04-14 20:56:12.44846
2204	23	85	46	2018-04-14 20:56:12.454309	2018-04-14 20:56:12.454309
2205	23	85	61	2018-04-14 20:56:12.459469	2018-04-14 20:56:12.459469
2206	23	85	64	2018-04-14 20:56:12.464401	2018-04-14 20:56:12.464401
2207	23	85	70	2018-04-14 20:56:12.470107	2018-04-14 20:56:12.470107
2208	23	85	72	2018-04-14 20:56:12.475293	2018-04-14 20:56:12.475293
2209	23	85	74	2018-04-14 20:56:12.48029	2018-04-14 20:56:12.48029
2210	23	85	78	2018-04-14 20:56:12.486061	2018-04-14 20:56:12.486061
2211	23	85	80	2018-04-14 20:56:12.491798	2018-04-14 20:56:12.491798
2212	23	85	84	2018-04-14 20:56:12.497302	2018-04-14 20:56:12.497302
2213	23	85	85	2018-04-14 20:56:12.503401	2018-04-14 20:56:12.503401
2214	23	85	86	2018-04-14 20:56:12.508968	2018-04-14 20:56:12.508968
2215	23	85	89	2018-04-14 20:56:12.513944	2018-04-14 20:56:12.513944
2216	23	85	91	2018-04-14 20:56:12.519935	2018-04-14 20:56:12.519935
2217	23	85	95	2018-04-14 20:56:12.525123	2018-04-14 20:56:12.525123
2218	23	86	15	2018-04-14 20:56:12.529999	2018-04-14 20:56:12.529999
2219	23	86	16	2018-04-14 20:56:12.534881	2018-04-14 20:56:12.534881
2220	23	86	17	2018-04-14 20:56:12.539488	2018-04-14 20:56:12.539488
2221	23	86	39	2018-04-14 20:56:12.544648	2018-04-14 20:56:12.544648
2222	23	86	41	2018-04-14 20:56:12.549508	2018-04-14 20:56:12.549508
2223	23	86	46	2018-04-14 20:56:12.554086	2018-04-14 20:56:12.554086
2224	23	86	48	2018-04-14 20:56:12.558348	2018-04-14 20:56:12.558348
2225	23	86	52	2018-04-14 20:56:12.563353	2018-04-14 20:56:12.563353
2226	23	86	61	2018-04-14 20:56:12.567976	2018-04-14 20:56:12.567976
2227	23	86	70	2018-04-14 20:56:12.572467	2018-04-14 20:56:12.572467
2228	23	86	72	2018-04-14 20:56:12.577118	2018-04-14 20:56:12.577118
2229	23	86	78	2018-04-14 20:56:12.581829	2018-04-14 20:56:12.581829
2230	23	86	84	2018-04-14 20:56:12.587111	2018-04-14 20:56:12.587111
2231	23	86	86	2018-04-14 20:56:12.593959	2018-04-14 20:56:12.593959
2232	23	86	91	2018-04-14 20:56:12.604947	2018-04-14 20:56:12.604947
2233	23	86	95	2018-04-14 20:56:12.618357	2018-04-14 20:56:12.618357
2234	23	86	98	2018-04-14 20:56:12.623941	2018-04-14 20:56:12.623941
2235	23	88	15	2018-04-14 20:56:12.62956	2018-04-14 20:56:12.62956
2236	23	88	16	2018-04-14 20:56:12.648353	2018-04-14 20:56:12.648353
2237	23	88	17	2018-04-14 20:56:12.66259	2018-04-14 20:56:12.66259
2238	23	88	24	2018-04-14 20:56:12.668457	2018-04-14 20:56:12.668457
2239	23	88	39	2018-04-14 20:56:12.674387	2018-04-14 20:56:12.674387
2240	23	88	41	2018-04-14 20:56:12.681943	2018-04-14 20:56:12.681943
2241	23	88	46	2018-04-14 20:56:12.687798	2018-04-14 20:56:12.687798
2242	23	88	61	2018-04-14 20:56:12.694212	2018-04-14 20:56:12.694212
2243	23	88	64	2018-04-14 20:56:12.700243	2018-04-14 20:56:12.700243
2244	23	88	70	2018-04-14 20:56:12.706148	2018-04-14 20:56:12.706148
2245	23	88	72	2018-04-14 20:56:12.713795	2018-04-14 20:56:12.713795
2246	23	88	74	2018-04-14 20:56:12.727091	2018-04-14 20:56:12.727091
2247	23	88	78	2018-04-14 20:56:12.739009	2018-04-14 20:56:12.739009
2248	23	88	80	2018-04-14 20:56:12.750363	2018-04-14 20:56:12.750363
2249	23	88	84	2018-04-14 20:56:12.755932	2018-04-14 20:56:12.755932
2250	23	88	85	2018-04-14 20:56:12.761449	2018-04-14 20:56:12.761449
2251	23	88	86	2018-04-14 20:56:12.767906	2018-04-14 20:56:12.767906
2252	23	88	88	2018-04-14 20:56:12.774957	2018-04-14 20:56:12.774957
2253	23	88	89	2018-04-14 20:56:12.782938	2018-04-14 20:56:12.782938
2254	23	88	91	2018-04-14 20:56:12.798358	2018-04-14 20:56:12.798358
2255	23	88	95	2018-04-14 20:56:12.810334	2018-04-14 20:56:12.810334
2256	23	89	15	2018-04-14 20:56:12.82176	2018-04-14 20:56:12.82176
2257	23	89	16	2018-04-14 20:56:12.838733	2018-04-14 20:56:12.838733
2258	23	89	17	2018-04-14 20:56:12.844021	2018-04-14 20:56:12.844021
2259	23	89	39	2018-04-14 20:56:12.849861	2018-04-14 20:56:12.849861
2260	23	89	41	2018-04-14 20:56:12.855163	2018-04-14 20:56:12.855163
2261	23	89	46	2018-04-14 20:56:12.862447	2018-04-14 20:56:12.862447
2262	23	89	61	2018-04-14 20:56:12.867589	2018-04-14 20:56:12.867589
2263	23	89	64	2018-04-14 20:56:12.87196	2018-04-14 20:56:12.87196
2264	23	89	70	2018-04-14 20:56:12.876258	2018-04-14 20:56:12.876258
2265	23	89	72	2018-04-14 20:56:12.880957	2018-04-14 20:56:12.880957
2266	23	89	74	2018-04-14 20:56:12.88572	2018-04-14 20:56:12.88572
2267	23	89	78	2018-04-14 20:56:12.89079	2018-04-14 20:56:12.89079
2268	23	89	80	2018-04-14 20:56:12.895591	2018-04-14 20:56:12.895591
2269	23	89	84	2018-04-14 20:56:12.900349	2018-04-14 20:56:12.900349
2270	23	89	86	2018-04-14 20:56:12.905107	2018-04-14 20:56:12.905107
2271	23	89	89	2018-04-14 20:56:12.909683	2018-04-14 20:56:12.909683
2272	23	89	91	2018-04-14 20:56:12.914776	2018-04-14 20:56:12.914776
2273	23	89	95	2018-04-14 20:56:12.919851	2018-04-14 20:56:12.919851
2274	23	102	36	2018-04-14 20:56:12.924568	2018-04-14 20:56:12.924568
2275	23	102	40	2018-04-14 20:56:12.9294	2018-04-14 20:56:12.9294
2276	24	1	13	2018-04-14 20:56:12.936312	2018-04-14 20:56:12.936312
2277	24	1	14	2018-04-14 20:56:12.942468	2018-04-14 20:56:12.942468
2278	24	5	14	2018-04-14 20:56:12.947818	2018-04-14 20:56:12.947818
2279	24	7	13	2018-04-14 20:56:12.952307	2018-04-14 20:56:12.952307
2280	24	7	33	2018-04-14 20:56:12.957144	2018-04-14 20:56:12.957144
2281	24	8	13	2018-04-14 20:56:12.961904	2018-04-14 20:56:12.961904
2282	24	8	14	2018-04-14 20:56:12.967122	2018-04-14 20:56:12.967122
2283	24	9	33	2018-04-14 20:56:12.971892	2018-04-14 20:56:12.971892
2284	24	10	13	2018-04-14 20:56:12.977819	2018-04-14 20:56:12.977819
2285	24	10	14	2018-04-14 20:56:12.982662	2018-04-14 20:56:12.982662
2286	24	13	2	2018-04-14 20:56:12.987422	2018-04-14 20:56:12.987422
2287	24	13	4	2018-04-14 20:56:12.992129	2018-04-14 20:56:12.992129
2288	24	13	40	2018-04-14 20:56:12.996859	2018-04-14 20:56:12.996859
2289	24	13	60	2018-04-14 20:56:13.001543	2018-04-14 20:56:13.001543
2290	24	13	64	2018-04-14 20:56:13.006063	2018-04-14 20:56:13.006063
2291	24	13	77	2018-04-14 20:56:13.010557	2018-04-14 20:56:13.010557
2292	24	13	80	2018-04-14 20:56:13.014791	2018-04-14 20:56:13.014791
2293	24	14	2	2018-04-14 20:56:13.019533	2018-04-14 20:56:13.019533
2294	24	14	4	2018-04-14 20:56:13.024172	2018-04-14 20:56:13.024172
2295	24	14	12	2018-04-14 20:56:13.028639	2018-04-14 20:56:13.028639
2296	24	14	36	2018-04-14 20:56:13.033419	2018-04-14 20:56:13.033419
2297	24	14	40	2018-04-14 20:56:13.039447	2018-04-14 20:56:13.039447
2298	24	14	53	2018-04-14 20:56:13.044091	2018-04-14 20:56:13.044091
2299	24	14	60	2018-04-14 20:56:13.048612	2018-04-14 20:56:13.048612
2300	24	14	64	2018-04-14 20:56:13.053027	2018-04-14 20:56:13.053027
2301	24	14	77	2018-04-14 20:56:13.060318	2018-04-14 20:56:13.060318
2302	24	14	80	2018-04-14 20:56:13.065851	2018-04-14 20:56:13.065851
2303	24	14	87	2018-04-14 20:56:13.070626	2018-04-14 20:56:13.070626
2304	24	14	92	2018-04-14 20:56:13.075104	2018-04-14 20:56:13.075104
2305	24	14	103	2018-04-14 20:56:13.079928	2018-04-14 20:56:13.079928
2306	24	14	109	2018-04-14 20:56:13.086421	2018-04-14 20:56:13.086421
2307	24	25	13	2018-04-14 20:56:13.091652	2018-04-14 20:56:13.091652
2308	24	25	14	2018-04-14 20:56:13.096203	2018-04-14 20:56:13.096203
2309	24	30	13	2018-04-14 20:56:13.101165	2018-04-14 20:56:13.101165
2310	24	30	14	2018-04-14 20:56:13.106628	2018-04-14 20:56:13.106628
2311	24	33	64	2018-04-14 20:56:13.11128	2018-04-14 20:56:13.11128
2312	24	33	80	2018-04-14 20:56:13.116706	2018-04-14 20:56:13.116706
2313	24	44	13	2018-04-14 20:56:13.121546	2018-04-14 20:56:13.121546
2314	24	44	14	2018-04-14 20:56:13.126107	2018-04-14 20:56:13.126107
2315	24	59	13	2018-04-14 20:56:13.131233	2018-04-14 20:56:13.131233
2316	24	59	14	2018-04-14 20:56:13.136302	2018-04-14 20:56:13.136302
2317	24	76	13	2018-04-14 20:56:13.140813	2018-04-14 20:56:13.140813
2318	24	76	14	2018-04-14 20:56:13.145651	2018-04-14 20:56:13.145651
2319	24	79	13	2018-04-14 20:56:13.150729	2018-04-14 20:56:13.150729
2320	24	79	14	2018-04-14 20:56:13.155618	2018-04-14 20:56:13.155618
2321	24	90	13	2018-04-14 20:56:13.160224	2018-04-14 20:56:13.160224
2322	24	90	14	2018-04-14 20:56:13.165223	2018-04-14 20:56:13.165223
2323	24	93	13	2018-04-14 20:56:13.170164	2018-04-14 20:56:13.170164
2324	24	93	14	2018-04-14 20:56:13.174606	2018-04-14 20:56:13.174606
2325	25	1	21	2018-04-14 20:56:13.179369	2018-04-14 20:56:13.179369
2326	25	1	35	2018-04-14 20:56:13.184644	2018-04-14 20:56:13.184644
2327	25	1	51	2018-04-14 20:56:13.189238	2018-04-14 20:56:13.189238
2328	25	2	35	2018-04-14 20:56:13.193907	2018-04-14 20:56:13.193907
2329	25	2	51	2018-04-14 20:56:13.199814	2018-04-14 20:56:13.199814
2330	25	4	35	2018-04-14 20:56:13.204842	2018-04-14 20:56:13.204842
2331	25	4	51	2018-04-14 20:56:13.209541	2018-04-14 20:56:13.209541
2332	25	5	21	2018-04-14 20:56:13.214229	2018-04-14 20:56:13.214229
2333	25	5	35	2018-04-14 20:56:13.219155	2018-04-14 20:56:13.219155
2334	25	5	51	2018-04-14 20:56:13.223659	2018-04-14 20:56:13.223659
2335	25	7	35	2018-04-14 20:56:13.227996	2018-04-14 20:56:13.227996
2336	25	8	21	2018-04-14 20:56:13.232703	2018-04-14 20:56:13.232703
2337	25	8	35	2018-04-14 20:56:13.237443	2018-04-14 20:56:13.237443
2338	25	8	51	2018-04-14 20:56:13.241894	2018-04-14 20:56:13.241894
2339	25	14	35	2018-04-14 20:56:13.246322	2018-04-14 20:56:13.246322
2340	25	16	35	2018-04-14 20:56:13.250788	2018-04-14 20:56:13.250788
2341	25	17	35	2018-04-14 20:56:13.255114	2018-04-14 20:56:13.255114
2342	25	24	35	2018-04-14 20:56:13.259628	2018-04-14 20:56:13.259628
2343	25	25	21	2018-04-14 20:56:13.264102	2018-04-14 20:56:13.264102
2344	25	25	35	2018-04-14 20:56:13.26858	2018-04-14 20:56:13.26858
2345	25	25	51	2018-04-14 20:56:13.273239	2018-04-14 20:56:13.273239
2346	25	26	35	2018-04-14 20:56:13.277853	2018-04-14 20:56:13.277853
2347	25	27	35	2018-04-14 20:56:13.28565	2018-04-14 20:56:13.28565
2348	25	30	21	2018-04-14 20:56:13.292378	2018-04-14 20:56:13.292378
2349	25	30	35	2018-04-14 20:56:13.297292	2018-04-14 20:56:13.297292
2350	25	30	51	2018-04-14 20:56:13.301769	2018-04-14 20:56:13.301769
2351	25	34	35	2018-04-14 20:56:13.306431	2018-04-14 20:56:13.306431
2352	25	35	46	2018-04-14 20:56:13.310877	2018-04-14 20:56:13.310877
2353	25	35	64	2018-04-14 20:56:13.315554	2018-04-14 20:56:13.315554
2354	25	35	70	2018-04-14 20:56:13.320344	2018-04-14 20:56:13.320344
2355	25	35	80	2018-04-14 20:56:13.324683	2018-04-14 20:56:13.324683
2356	25	35	84	2018-04-14 20:56:13.331488	2018-04-14 20:56:13.331488
2357	25	36	35	2018-04-14 20:56:13.336794	2018-04-14 20:56:13.336794
2358	25	39	35	2018-04-14 20:56:13.342078	2018-04-14 20:56:13.342078
2359	25	40	21	2018-04-14 20:56:13.347158	2018-04-14 20:56:13.347158
2360	25	40	35	2018-04-14 20:56:13.352095	2018-04-14 20:56:13.352095
2361	25	42	21	2018-04-14 20:56:13.35675	2018-04-14 20:56:13.35675
2362	25	42	35	2018-04-14 20:56:13.361339	2018-04-14 20:56:13.361339
2363	25	42	51	2018-04-14 20:56:13.366308	2018-04-14 20:56:13.366308
2364	25	43	21	2018-04-14 20:56:13.372049	2018-04-14 20:56:13.372049
2365	25	43	35	2018-04-14 20:56:13.377865	2018-04-14 20:56:13.377865
2366	25	43	51	2018-04-14 20:56:13.384225	2018-04-14 20:56:13.384225
2367	25	44	21	2018-04-14 20:56:13.395664	2018-04-14 20:56:13.395664
2368	25	44	35	2018-04-14 20:56:13.4116	2018-04-14 20:56:13.4116
2369	25	44	51	2018-04-14 20:56:13.416705	2018-04-14 20:56:13.416705
2370	25	48	35	2018-04-14 20:56:13.42217	2018-04-14 20:56:13.42217
2371	25	52	35	2018-04-14 20:56:13.429228	2018-04-14 20:56:13.429228
2372	25	59	21	2018-04-14 20:56:13.43596	2018-04-14 20:56:13.43596
2373	25	59	35	2018-04-14 20:56:13.444513	2018-04-14 20:56:13.444513
2374	25	59	51	2018-04-14 20:56:13.453943	2018-04-14 20:56:13.453943
2375	25	60	35	2018-04-14 20:56:13.463835	2018-04-14 20:56:13.463835
2376	25	60	51	2018-04-14 20:56:13.478224	2018-04-14 20:56:13.478224
2377	25	61	35	2018-04-14 20:56:13.489747	2018-04-14 20:56:13.489747
2378	25	67	21	2018-04-14 20:56:13.497588	2018-04-14 20:56:13.497588
2379	25	67	35	2018-04-14 20:56:13.503384	2018-04-14 20:56:13.503384
2380	25	72	35	2018-04-14 20:56:13.509926	2018-04-14 20:56:13.509926
2381	25	73	21	2018-04-14 20:56:13.515865	2018-04-14 20:56:13.515865
2382	25	73	35	2018-04-14 20:56:13.521323	2018-04-14 20:56:13.521323
2383	25	73	51	2018-04-14 20:56:13.527773	2018-04-14 20:56:13.527773
2384	25	74	35	2018-04-14 20:56:13.533275	2018-04-14 20:56:13.533275
2385	25	75	21	2018-04-14 20:56:13.538435	2018-04-14 20:56:13.538435
2386	25	75	35	2018-04-14 20:56:13.544884	2018-04-14 20:56:13.544884
2387	25	75	51	2018-04-14 20:56:13.550503	2018-04-14 20:56:13.550503
2388	25	76	21	2018-04-14 20:56:13.555745	2018-04-14 20:56:13.555745
2389	25	76	35	2018-04-14 20:56:13.564377	2018-04-14 20:56:13.564377
2390	25	76	51	2018-04-14 20:56:13.56984	2018-04-14 20:56:13.56984
2391	25	77	35	2018-04-14 20:56:13.575972	2018-04-14 20:56:13.575972
2392	25	77	51	2018-04-14 20:56:13.581818	2018-04-14 20:56:13.581818
2393	25	78	35	2018-04-14 20:56:13.58755	2018-04-14 20:56:13.58755
2394	25	79	21	2018-04-14 20:56:13.59411	2018-04-14 20:56:13.59411
2395	25	79	35	2018-04-14 20:56:13.599251	2018-04-14 20:56:13.599251
2396	25	79	51	2018-04-14 20:56:13.604838	2018-04-14 20:56:13.604838
2397	25	82	35	2018-04-14 20:56:13.61147	2018-04-14 20:56:13.61147
2398	25	85	35	2018-04-14 20:56:13.616982	2018-04-14 20:56:13.616982
2399	25	88	35	2018-04-14 20:56:13.622248	2018-04-14 20:56:13.622248
2400	25	89	35	2018-04-14 20:56:13.628442	2018-04-14 20:56:13.628442
2401	25	90	21	2018-04-14 20:56:13.633885	2018-04-14 20:56:13.633885
2402	25	90	35	2018-04-14 20:56:13.639274	2018-04-14 20:56:13.639274
2403	25	90	51	2018-04-14 20:56:13.645827	2018-04-14 20:56:13.645827
2404	25	91	35	2018-04-14 20:56:13.651118	2018-04-14 20:56:13.651118
2405	25	93	21	2018-04-14 20:56:13.656271	2018-04-14 20:56:13.656271
2406	25	93	35	2018-04-14 20:56:13.662367	2018-04-14 20:56:13.662367
2407	25	93	51	2018-04-14 20:56:13.667967	2018-04-14 20:56:13.667967
2408	25	95	35	2018-04-14 20:56:13.673492	2018-04-14 20:56:13.673492
2409	25	98	35	2018-04-14 20:56:13.680102	2018-04-14 20:56:13.680102
2410	25	102	21	2018-04-14 20:56:13.686029	2018-04-14 20:56:13.686029
2411	25	102	35	2018-04-14 20:56:13.691453	2018-04-14 20:56:13.691453
2412	25	102	51	2018-04-14 20:56:13.697469	2018-04-14 20:56:13.697469
2413	25	112	21	2018-04-14 20:56:13.702745	2018-04-14 20:56:13.702745
2414	25	112	35	2018-04-14 20:56:13.708184	2018-04-14 20:56:13.708184
2415	25	112	51	2018-04-14 20:56:13.714172	2018-04-14 20:56:13.714172
2416	26	10	27	2018-04-14 20:56:13.719691	2018-04-14 20:56:13.719691
2417	26	11	56	2018-04-14 20:56:13.72506	2018-04-14 20:56:13.72506
2418	26	11	97	2018-04-14 20:56:13.731273	2018-04-14 20:56:13.731273
2419	26	11	105	2018-04-14 20:56:13.73635	2018-04-14 20:56:13.73635
2420	26	27	3	2018-04-14 20:56:13.74149	2018-04-14 20:56:13.74149
2421	26	27	12	2018-04-14 20:56:13.747731	2018-04-14 20:56:13.747731
2422	26	27	18	2018-04-14 20:56:13.753041	2018-04-14 20:56:13.753041
2423	26	27	20	2018-04-14 20:56:13.758768	2018-04-14 20:56:13.758768
2424	26	27	29	2018-04-14 20:56:13.765226	2018-04-14 20:56:13.765226
2425	26	27	37	2018-04-14 20:56:13.772357	2018-04-14 20:56:13.772357
2426	26	27	45	2018-04-14 20:56:13.78377	2018-04-14 20:56:13.78377
2427	26	27	53	2018-04-14 20:56:13.797197	2018-04-14 20:56:13.797197
2428	26	27	54	2018-04-14 20:56:13.802718	2018-04-14 20:56:13.802718
2429	26	27	55	2018-04-14 20:56:13.808033	2018-04-14 20:56:13.808033
2430	26	27	56	2018-04-14 20:56:13.814655	2018-04-14 20:56:13.814655
2431	26	27	57	2018-04-14 20:56:13.819939	2018-04-14 20:56:13.819939
2432	26	27	58	2018-04-14 20:56:13.825531	2018-04-14 20:56:13.825531
2433	26	27	65	2018-04-14 20:56:13.8339	2018-04-14 20:56:13.8339
2434	26	27	69	2018-04-14 20:56:13.8394	2018-04-14 20:56:13.8394
2435	26	27	71	2018-04-14 20:56:13.846458	2018-04-14 20:56:13.846458
2436	26	27	81	2018-04-14 20:56:13.852273	2018-04-14 20:56:13.852273
2437	26	27	87	2018-04-14 20:56:13.857486	2018-04-14 20:56:13.857486
2438	26	27	92	2018-04-14 20:56:13.864141	2018-04-14 20:56:13.864141
2439	26	27	94	2018-04-14 20:56:13.869538	2018-04-14 20:56:13.869538
2440	26	27	99	2018-04-14 20:56:13.874614	2018-04-14 20:56:13.874614
2441	26	27	101	2018-04-14 20:56:13.881503	2018-04-14 20:56:13.881503
2442	26	27	107	2018-04-14 20:56:13.886439	2018-04-14 20:56:13.886439
2443	26	27	109	2018-04-14 20:56:13.891417	2018-04-14 20:56:13.891417
2444	26	33	103	2018-04-14 20:56:13.897594	2018-04-14 20:56:13.897594
2445	26	47	103	2018-04-14 20:56:13.902354	2018-04-14 20:56:13.902354
2446	26	49	103	2018-04-14 20:56:13.906915	2018-04-14 20:56:13.906915
2447	26	66	62	2018-04-14 20:56:13.911422	2018-04-14 20:56:13.911422
2448	26	66	63	2018-04-14 20:56:13.915953	2018-04-14 20:56:13.915953
2449	26	66	96	2018-04-14 20:56:13.920534	2018-04-14 20:56:13.920534
2450	26	66	97	2018-04-14 20:56:13.92522	2018-04-14 20:56:13.92522
2451	26	66	103	2018-04-14 20:56:13.92979	2018-04-14 20:56:13.92979
2452	26	66	105	2018-04-14 20:56:13.9344	2018-04-14 20:56:13.9344
2453	26	68	68	2018-04-14 20:56:13.939215	2018-04-14 20:56:13.939215
2454	26	83	62	2018-04-14 20:56:13.944054	2018-04-14 20:56:13.944054
2455	26	83	63	2018-04-14 20:56:13.948996	2018-04-14 20:56:13.948996
2456	26	83	97	2018-04-14 20:56:13.954298	2018-04-14 20:56:13.954298
2457	26	83	105	2018-04-14 20:56:13.95947	2018-04-14 20:56:13.95947
2458	26	100	37	2018-04-14 20:56:13.964238	2018-04-14 20:56:13.964238
2459	26	100	55	2018-04-14 20:56:13.969253	2018-04-14 20:56:13.969253
2460	26	100	97	2018-04-14 20:56:13.974394	2018-04-14 20:56:13.974394
2461	26	100	101	2018-04-14 20:56:13.97931	2018-04-14 20:56:13.97931
2462	26	100	105	2018-04-14 20:56:13.984182	2018-04-14 20:56:13.984182
2463	26	108	103	2018-04-14 20:56:13.990753	2018-04-14 20:56:13.990753
2464	27	1	62	2018-04-14 20:56:13.996499	2018-04-14 20:56:13.996499
2465	27	1	63	2018-04-14 20:56:14.001783	2018-04-14 20:56:14.001783
2466	27	5	62	2018-04-14 20:56:14.00708	2018-04-14 20:56:14.00708
2467	27	5	63	2018-04-14 20:56:14.01329	2018-04-14 20:56:14.01329
2468	27	7	62	2018-04-14 20:56:14.019123	2018-04-14 20:56:14.019123
2469	27	7	63	2018-04-14 20:56:14.024723	2018-04-14 20:56:14.024723
2470	27	8	62	2018-04-14 20:56:14.031519	2018-04-14 20:56:14.031519
2471	27	8	63	2018-04-14 20:56:14.036821	2018-04-14 20:56:14.036821
2472	27	9	62	2018-04-14 20:56:14.042577	2018-04-14 20:56:14.042577
2473	27	9	63	2018-04-14 20:56:14.056163	2018-04-14 20:56:14.056163
2474	27	10	62	2018-04-14 20:56:14.067734	2018-04-14 20:56:14.067734
2475	27	10	63	2018-04-14 20:56:14.082218	2018-04-14 20:56:14.082218
2476	27	13	62	2018-04-14 20:56:14.088656	2018-04-14 20:56:14.088656
2477	27	13	63	2018-04-14 20:56:14.100834	2018-04-14 20:56:14.100834
2478	27	14	62	2018-04-14 20:56:14.114922	2018-04-14 20:56:14.114922
2479	27	14	63	2018-04-14 20:56:14.125287	2018-04-14 20:56:14.125287
2480	27	15	62	2018-04-14 20:56:14.133006	2018-04-14 20:56:14.133006
2481	27	15	63	2018-04-14 20:56:14.139742	2018-04-14 20:56:14.139742
2482	27	16	62	2018-04-14 20:56:14.148264	2018-04-14 20:56:14.148264
2483	27	16	63	2018-04-14 20:56:14.156789	2018-04-14 20:56:14.156789
2484	27	17	62	2018-04-14 20:56:14.164483	2018-04-14 20:56:14.164483
2485	27	17	63	2018-04-14 20:56:14.171083	2018-04-14 20:56:14.171083
2486	27	21	62	2018-04-14 20:56:14.177171	2018-04-14 20:56:14.177171
2487	27	21	63	2018-04-14 20:56:14.18433	2018-04-14 20:56:14.18433
2488	27	23	62	2018-04-14 20:56:14.191822	2018-04-14 20:56:14.191822
2489	27	23	63	2018-04-14 20:56:14.207641	2018-04-14 20:56:14.207641
2490	27	24	62	2018-04-14 20:56:14.215419	2018-04-14 20:56:14.215419
2491	27	24	63	2018-04-14 20:56:14.221866	2018-04-14 20:56:14.221866
2492	27	25	62	2018-04-14 20:56:14.234021	2018-04-14 20:56:14.234021
2493	27	25	63	2018-04-14 20:56:14.246075	2018-04-14 20:56:14.246075
2494	27	26	62	2018-04-14 20:56:14.257423	2018-04-14 20:56:14.257423
2495	27	26	63	2018-04-14 20:56:14.264585	2018-04-14 20:56:14.264585
2496	27	27	62	2018-04-14 20:56:14.271481	2018-04-14 20:56:14.271481
2497	27	27	63	2018-04-14 20:56:14.280768	2018-04-14 20:56:14.280768
2498	27	28	62	2018-04-14 20:56:14.287357	2018-04-14 20:56:14.287357
2499	27	28	63	2018-04-14 20:56:14.293369	2018-04-14 20:56:14.293369
2500	27	28	103	2018-04-14 20:56:14.300815	2018-04-14 20:56:14.300815
2501	27	30	62	2018-04-14 20:56:14.307385	2018-04-14 20:56:14.307385
2502	27	30	63	2018-04-14 20:56:14.314036	2018-04-14 20:56:14.314036
2503	27	31	62	2018-04-14 20:56:14.319862	2018-04-14 20:56:14.319862
2504	27	31	63	2018-04-14 20:56:14.32535	2018-04-14 20:56:14.32535
2505	27	32	62	2018-04-14 20:56:14.332744	2018-04-14 20:56:14.332744
2506	27	32	63	2018-04-14 20:56:14.338436	2018-04-14 20:56:14.338436
2507	27	33	62	2018-04-14 20:56:14.343839	2018-04-14 20:56:14.343839
2508	27	33	63	2018-04-14 20:56:14.350524	2018-04-14 20:56:14.350524
2509	27	34	62	2018-04-14 20:56:14.356379	2018-04-14 20:56:14.356379
2510	27	34	63	2018-04-14 20:56:14.361784	2018-04-14 20:56:14.361784
2511	27	36	62	2018-04-14 20:56:14.368342	2018-04-14 20:56:14.368342
2512	27	36	63	2018-04-14 20:56:14.374696	2018-04-14 20:56:14.374696
2513	27	39	62	2018-04-14 20:56:14.388117	2018-04-14 20:56:14.388117
2514	27	39	63	2018-04-14 20:56:14.402308	2018-04-14 20:56:14.402308
2515	27	40	62	2018-04-14 20:56:14.414227	2018-04-14 20:56:14.414227
2516	27	40	63	2018-04-14 20:56:14.420162	2018-04-14 20:56:14.420162
2517	27	41	62	2018-04-14 20:56:14.429381	2018-04-14 20:56:14.429381
2518	27	41	63	2018-04-14 20:56:14.437724	2018-04-14 20:56:14.437724
2519	27	42	62	2018-04-14 20:56:14.443741	2018-04-14 20:56:14.443741
2520	27	42	63	2018-04-14 20:56:14.449801	2018-04-14 20:56:14.449801
2521	27	43	62	2018-04-14 20:56:14.462351	2018-04-14 20:56:14.462351
2522	27	43	63	2018-04-14 20:56:14.47783	2018-04-14 20:56:14.47783
2523	27	44	62	2018-04-14 20:56:14.48976	2018-04-14 20:56:14.48976
2524	27	44	63	2018-04-14 20:56:14.502635	2018-04-14 20:56:14.502635
2525	27	47	62	2018-04-14 20:56:14.517027	2018-04-14 20:56:14.517027
2526	27	47	63	2018-04-14 20:56:14.528368	2018-04-14 20:56:14.528368
2527	27	48	62	2018-04-14 20:56:14.540053	2018-04-14 20:56:14.540053
2528	27	48	63	2018-04-14 20:56:14.545946	2018-04-14 20:56:14.545946
2529	27	49	62	2018-04-14 20:56:14.553341	2018-04-14 20:56:14.553341
2530	27	49	63	2018-04-14 20:56:14.559105	2018-04-14 20:56:14.559105
2531	27	50	62	2018-04-14 20:56:14.565758	2018-04-14 20:56:14.565758
2532	27	50	63	2018-04-14 20:56:14.571349	2018-04-14 20:56:14.571349
2533	27	52	62	2018-04-14 20:56:14.577257	2018-04-14 20:56:14.577257
2534	27	52	63	2018-04-14 20:56:14.58396	2018-04-14 20:56:14.58396
2535	27	59	62	2018-04-14 20:56:14.588842	2018-04-14 20:56:14.588842
2536	27	59	63	2018-04-14 20:56:14.594311	2018-04-14 20:56:14.594311
2537	27	61	62	2018-04-14 20:56:14.600862	2018-04-14 20:56:14.600862
2538	27	61	63	2018-04-14 20:56:14.606563	2018-04-14 20:56:14.606563
2539	27	62	2	2018-04-14 20:56:14.612172	2018-04-14 20:56:14.612172
2540	27	62	3	2018-04-14 20:56:14.620378	2018-04-14 20:56:14.620378
2541	27	62	4	2018-04-14 20:56:14.634183	2018-04-14 20:56:14.634183
2542	27	62	12	2018-04-14 20:56:14.644998	2018-04-14 20:56:14.644998
2543	27	62	18	2018-04-14 20:56:14.652167	2018-04-14 20:56:14.652167
2544	27	62	20	2018-04-14 20:56:14.658114	2018-04-14 20:56:14.658114
2545	27	62	29	2018-04-14 20:56:14.665631	2018-04-14 20:56:14.665631
2546	27	62	37	2018-04-14 20:56:14.672935	2018-04-14 20:56:14.672935
2547	27	62	45	2018-04-14 20:56:14.678242	2018-04-14 20:56:14.678242
2548	27	62	46	2018-04-14 20:56:14.684669	2018-04-14 20:56:14.684669
2549	27	62	53	2018-04-14 20:56:14.689911	2018-04-14 20:56:14.689911
2550	27	62	54	2018-04-14 20:56:14.69526	2018-04-14 20:56:14.69526
2551	27	62	55	2018-04-14 20:56:14.701537	2018-04-14 20:56:14.701537
2552	27	62	56	2018-04-14 20:56:14.706841	2018-04-14 20:56:14.706841
2553	27	62	57	2018-04-14 20:56:14.712079	2018-04-14 20:56:14.712079
2554	27	62	58	2018-04-14 20:56:14.718384	2018-04-14 20:56:14.718384
2555	27	62	60	2018-04-14 20:56:14.723979	2018-04-14 20:56:14.723979
2556	27	62	62	2018-04-14 20:56:14.72926	2018-04-14 20:56:14.72926
2557	27	62	63	2018-04-14 20:56:14.735451	2018-04-14 20:56:14.735451
2558	27	62	64	2018-04-14 20:56:14.740564	2018-04-14 20:56:14.740564
2559	27	62	65	2018-04-14 20:56:14.745788	2018-04-14 20:56:14.745788
2560	27	62	69	2018-04-14 20:56:14.751958	2018-04-14 20:56:14.751958
2561	27	62	70	2018-04-14 20:56:14.758734	2018-04-14 20:56:14.758734
2562	27	62	71	2018-04-14 20:56:14.763981	2018-04-14 20:56:14.763981
2563	27	62	77	2018-04-14 20:56:14.770476	2018-04-14 20:56:14.770476
2564	27	62	80	2018-04-14 20:56:14.775759	2018-04-14 20:56:14.775759
2565	27	62	81	2018-04-14 20:56:14.781547	2018-04-14 20:56:14.781547
2566	27	62	84	2018-04-14 20:56:14.787488	2018-04-14 20:56:14.787488
2567	27	62	87	2018-04-14 20:56:14.792829	2018-04-14 20:56:14.792829
2568	27	62	92	2018-04-14 20:56:14.798434	2018-04-14 20:56:14.798434
2569	27	62	94	2018-04-14 20:56:14.804181	2018-04-14 20:56:14.804181
2570	27	62	97	2018-04-14 20:56:14.809716	2018-04-14 20:56:14.809716
2571	27	62	99	2018-04-14 20:56:14.81544	2018-04-14 20:56:14.81544
2572	27	62	101	2018-04-14 20:56:14.82114	2018-04-14 20:56:14.82114
2573	27	62	103	2018-04-14 20:56:14.826566	2018-04-14 20:56:14.826566
2574	27	62	105	2018-04-14 20:56:14.832365	2018-04-14 20:56:14.832365
2575	27	62	107	2018-04-14 20:56:14.838009	2018-04-14 20:56:14.838009
2576	27	62	109	2018-04-14 20:56:14.844003	2018-04-14 20:56:14.844003
2577	27	63	2	2018-04-14 20:56:14.850758	2018-04-14 20:56:14.850758
2578	27	63	3	2018-04-14 20:56:14.856835	2018-04-14 20:56:14.856835
2579	27	63	4	2018-04-14 20:56:14.86214	2018-04-14 20:56:14.86214
2580	27	63	12	2018-04-14 20:56:14.868756	2018-04-14 20:56:14.868756
2581	27	63	18	2018-04-14 20:56:14.873758	2018-04-14 20:56:14.873758
2582	27	63	20	2018-04-14 20:56:14.879091	2018-04-14 20:56:14.879091
2583	27	63	29	2018-04-14 20:56:14.885699	2018-04-14 20:56:14.885699
2584	27	63	37	2018-04-14 20:56:14.891419	2018-04-14 20:56:14.891419
2585	27	63	45	2018-04-14 20:56:14.896634	2018-04-14 20:56:14.896634
2586	27	63	46	2018-04-14 20:56:14.903094	2018-04-14 20:56:14.903094
2587	27	63	53	2018-04-14 20:56:14.908479	2018-04-14 20:56:14.908479
2588	27	63	54	2018-04-14 20:56:14.913693	2018-04-14 20:56:14.913693
2589	27	63	55	2018-04-14 20:56:14.920368	2018-04-14 20:56:14.920368
2590	27	63	56	2018-04-14 20:56:14.925789	2018-04-14 20:56:14.925789
2591	27	63	57	2018-04-14 20:56:14.930886	2018-04-14 20:56:14.930886
2592	27	63	58	2018-04-14 20:56:14.937355	2018-04-14 20:56:14.937355
2593	27	63	60	2018-04-14 20:56:14.942772	2018-04-14 20:56:14.942772
2594	27	63	63	2018-04-14 20:56:14.948065	2018-04-14 20:56:14.948065
2595	27	63	64	2018-04-14 20:56:14.954229	2018-04-14 20:56:14.954229
2596	27	63	65	2018-04-14 20:56:14.96039	2018-04-14 20:56:14.96039
2597	27	63	69	2018-04-14 20:56:14.966205	2018-04-14 20:56:14.966205
2598	27	63	70	2018-04-14 20:56:14.971944	2018-04-14 20:56:14.971944
2599	27	63	71	2018-04-14 20:56:14.977415	2018-04-14 20:56:14.977415
2600	27	63	77	2018-04-14 20:56:14.983406	2018-04-14 20:56:14.983406
2601	27	63	80	2018-04-14 20:56:14.989354	2018-04-14 20:56:14.989354
2602	27	63	81	2018-04-14 20:56:15.003257	2018-04-14 20:56:15.003257
2603	27	63	84	2018-04-14 20:56:15.008368	2018-04-14 20:56:15.008368
2604	27	63	87	2018-04-14 20:56:15.013428	2018-04-14 20:56:15.013428
2605	27	63	92	2018-04-14 20:56:15.020675	2018-04-14 20:56:15.020675
2606	27	63	94	2018-04-14 20:56:15.02603	2018-04-14 20:56:15.02603
2607	27	63	99	2018-04-14 20:56:15.031081	2018-04-14 20:56:15.031081
2608	27	63	101	2018-04-14 20:56:15.037329	2018-04-14 20:56:15.037329
2609	27	63	103	2018-04-14 20:56:15.042727	2018-04-14 20:56:15.042727
2610	27	63	107	2018-04-14 20:56:15.047885	2018-04-14 20:56:15.047885
2611	27	63	109	2018-04-14 20:56:15.053865	2018-04-14 20:56:15.053865
2612	27	67	62	2018-04-14 20:56:15.059736	2018-04-14 20:56:15.059736
2613	27	67	63	2018-04-14 20:56:15.064838	2018-04-14 20:56:15.064838
2614	27	72	62	2018-04-14 20:56:15.071156	2018-04-14 20:56:15.071156
2615	27	72	63	2018-04-14 20:56:15.078077	2018-04-14 20:56:15.078077
2616	27	73	62	2018-04-14 20:56:15.085475	2018-04-14 20:56:15.085475
2617	27	74	62	2018-04-14 20:56:15.091216	2018-04-14 20:56:15.091216
2618	27	74	63	2018-04-14 20:56:15.096741	2018-04-14 20:56:15.096741
2619	27	75	62	2018-04-14 20:56:15.102747	2018-04-14 20:56:15.102747
2620	27	75	63	2018-04-14 20:56:15.108388	2018-04-14 20:56:15.108388
2621	27	76	62	2018-04-14 20:56:15.114065	2018-04-14 20:56:15.114065
2622	27	76	63	2018-04-14 20:56:15.120746	2018-04-14 20:56:15.120746
2623	27	78	62	2018-04-14 20:56:15.126575	2018-04-14 20:56:15.126575
2624	27	78	63	2018-04-14 20:56:15.131641	2018-04-14 20:56:15.131641
2625	27	79	62	2018-04-14 20:56:15.13783	2018-04-14 20:56:15.13783
2626	27	79	63	2018-04-14 20:56:15.143399	2018-04-14 20:56:15.143399
2627	27	82	62	2018-04-14 20:56:15.148394	2018-04-14 20:56:15.148394
2628	27	82	63	2018-04-14 20:56:15.15461	2018-04-14 20:56:15.15461
2629	27	85	62	2018-04-14 20:56:15.159861	2018-04-14 20:56:15.159861
2630	27	85	63	2018-04-14 20:56:15.164707	2018-04-14 20:56:15.164707
2631	27	86	62	2018-04-14 20:56:15.1711	2018-04-14 20:56:15.1711
2632	27	86	63	2018-04-14 20:56:15.17653	2018-04-14 20:56:15.17653
2633	27	88	62	2018-04-14 20:56:15.181494	2018-04-14 20:56:15.181494
2634	27	88	63	2018-04-14 20:56:15.187835	2018-04-14 20:56:15.187835
2635	27	89	62	2018-04-14 20:56:15.192779	2018-04-14 20:56:15.192779
2636	27	89	63	2018-04-14 20:56:15.197919	2018-04-14 20:56:15.197919
2637	27	90	62	2018-04-14 20:56:15.204197	2018-04-14 20:56:15.204197
2638	27	90	63	2018-04-14 20:56:15.209577	2018-04-14 20:56:15.209577
2639	27	91	62	2018-04-14 20:56:15.214756	2018-04-14 20:56:15.214756
2640	27	91	63	2018-04-14 20:56:15.221332	2018-04-14 20:56:15.221332
2641	27	93	62	2018-04-14 20:56:15.226776	2018-04-14 20:56:15.226776
2642	27	93	63	2018-04-14 20:56:15.231736	2018-04-14 20:56:15.231736
2643	27	95	62	2018-04-14 20:56:15.239057	2018-04-14 20:56:15.239057
2644	27	95	63	2018-04-14 20:56:15.24488	2018-04-14 20:56:15.24488
2645	27	96	62	2018-04-14 20:56:15.249908	2018-04-14 20:56:15.249908
2646	27	96	63	2018-04-14 20:56:15.256429	2018-04-14 20:56:15.256429
2647	27	96	105	2018-04-14 20:56:15.261795	2018-04-14 20:56:15.261795
2648	27	98	62	2018-04-14 20:56:15.26729	2018-04-14 20:56:15.26729
2649	27	98	63	2018-04-14 20:56:15.273738	2018-04-14 20:56:15.273738
2650	27	102	62	2018-04-14 20:56:15.28004	2018-04-14 20:56:15.28004
2651	27	102	63	2018-04-14 20:56:15.286166	2018-04-14 20:56:15.286166
2652	27	104	62	2018-04-14 20:56:15.291911	2018-04-14 20:56:15.291911
2653	27	104	63	2018-04-14 20:56:15.297112	2018-04-14 20:56:15.297112
2654	27	104	103	2018-04-14 20:56:15.303327	2018-04-14 20:56:15.303327
2655	27	105	63	2018-04-14 20:56:15.309973	2018-04-14 20:56:15.309973
2656	27	105	97	2018-04-14 20:56:15.315633	2018-04-14 20:56:15.315633
2657	27	108	62	2018-04-14 20:56:15.322199	2018-04-14 20:56:15.322199
2658	27	108	63	2018-04-14 20:56:15.327698	2018-04-14 20:56:15.327698
2659	27	112	62	2018-04-14 20:56:15.332598	2018-04-14 20:56:15.332598
2660	27	112	63	2018-04-14 20:56:15.339099	2018-04-14 20:56:15.339099
2661	28	17	6	2018-04-14 20:56:15.345905	2018-04-14 20:56:15.345905
2662	28	17	22	2018-04-14 20:56:15.352757	2018-04-14 20:56:15.352757
2663	28	17	38	2018-04-14 20:56:15.35848	2018-04-14 20:56:15.35848
2664	28	17	71	2018-04-14 20:56:15.364259	2018-04-14 20:56:15.364259
2665	28	17	106	2018-04-14 20:56:15.369623	2018-04-14 20:56:15.369623
2666	28	17	110	2018-04-14 20:56:15.375233	2018-04-14 20:56:15.375233
2667	28	17	111	2018-04-14 20:56:15.381913	2018-04-14 20:56:15.381913
2668	28	41	6	2018-04-14 20:56:15.390959	2018-04-14 20:56:15.390959
2669	28	41	22	2018-04-14 20:56:15.396735	2018-04-14 20:56:15.396735
2670	28	41	38	2018-04-14 20:56:15.40179	2018-04-14 20:56:15.40179
2671	28	41	71	2018-04-14 20:56:15.406887	2018-04-14 20:56:15.406887
2672	28	41	106	2018-04-14 20:56:15.412263	2018-04-14 20:56:15.412263
2673	28	41	110	2018-04-14 20:56:15.41779	2018-04-14 20:56:15.41779
2674	28	41	111	2018-04-14 20:56:15.423558	2018-04-14 20:56:15.423558
2675	28	48	6	2018-04-14 20:56:15.430158	2018-04-14 20:56:15.430158
2676	28	48	22	2018-04-14 20:56:15.435398	2018-04-14 20:56:15.435398
2677	28	48	38	2018-04-14 20:56:15.443631	2018-04-14 20:56:15.443631
2678	28	48	71	2018-04-14 20:56:15.452312	2018-04-14 20:56:15.452312
2679	28	48	106	2018-04-14 20:56:15.458786	2018-04-14 20:56:15.458786
2680	28	48	110	2018-04-14 20:56:15.465134	2018-04-14 20:56:15.465134
2681	28	48	111	2018-04-14 20:56:15.472873	2018-04-14 20:56:15.472873
2682	28	52	6	2018-04-14 20:56:15.478858	2018-04-14 20:56:15.478858
2683	28	52	22	2018-04-14 20:56:15.484171	2018-04-14 20:56:15.484171
2684	28	52	38	2018-04-14 20:56:15.491968	2018-04-14 20:56:15.491968
2685	28	52	71	2018-04-14 20:56:15.497451	2018-04-14 20:56:15.497451
2686	28	52	106	2018-04-14 20:56:15.503889	2018-04-14 20:56:15.503889
2687	28	52	110	2018-04-14 20:56:15.50966	2018-04-14 20:56:15.50966
2688	28	52	111	2018-04-14 20:56:15.515785	2018-04-14 20:56:15.515785
2689	28	98	6	2018-04-14 20:56:15.522345	2018-04-14 20:56:15.522345
2690	28	98	22	2018-04-14 20:56:15.52866	2018-04-14 20:56:15.52866
2691	28	98	38	2018-04-14 20:56:15.533776	2018-04-14 20:56:15.533776
2692	28	98	71	2018-04-14 20:56:15.541239	2018-04-14 20:56:15.541239
2693	28	98	106	2018-04-14 20:56:15.548263	2018-04-14 20:56:15.548263
2694	28	98	110	2018-04-14 20:56:15.554554	2018-04-14 20:56:15.554554
2695	28	98	111	2018-04-14 20:56:15.561178	2018-04-14 20:56:15.561178
2696	29	46	46	2018-04-14 20:56:15.566611	2018-04-14 20:56:15.566611
2697	29	46	84	2018-04-14 20:56:15.573543	2018-04-14 20:56:15.573543
2698	29	48	61	2018-04-14 20:56:15.579437	2018-04-14 20:56:15.579437
2699	29	48	78	2018-04-14 20:56:15.584751	2018-04-14 20:56:15.584751
2700	29	48	91	2018-04-14 20:56:15.591719	2018-04-14 20:56:15.591719
2701	29	48	95	2018-04-14 20:56:15.597257	2018-04-14 20:56:15.597257
2702	29	52	61	2018-04-14 20:56:15.602603	2018-04-14 20:56:15.602603
2703	29	52	78	2018-04-14 20:56:15.609171	2018-04-14 20:56:15.609171
2704	29	52	91	2018-04-14 20:56:15.614848	2018-04-14 20:56:15.614848
2705	29	52	95	2018-04-14 20:56:15.621409	2018-04-14 20:56:15.621409
2706	29	61	46	2018-04-14 20:56:15.632858	2018-04-14 20:56:15.632858
2707	29	61	61	2018-04-14 20:56:15.639438	2018-04-14 20:56:15.639438
2708	29	61	70	2018-04-14 20:56:15.645115	2018-04-14 20:56:15.645115
2709	29	61	78	2018-04-14 20:56:15.650688	2018-04-14 20:56:15.650688
2710	29	61	84	2018-04-14 20:56:15.65764	2018-04-14 20:56:15.65764
2711	29	61	91	2018-04-14 20:56:15.663243	2018-04-14 20:56:15.663243
2712	29	61	95	2018-04-14 20:56:15.66823	2018-04-14 20:56:15.66823
2713	29	70	46	2018-04-14 20:56:15.674999	2018-04-14 20:56:15.674999
2714	29	70	70	2018-04-14 20:56:15.680489	2018-04-14 20:56:15.680489
2715	29	70	84	2018-04-14 20:56:15.685824	2018-04-14 20:56:15.685824
2716	29	78	46	2018-04-14 20:56:15.692684	2018-04-14 20:56:15.692684
2717	29	78	70	2018-04-14 20:56:15.698171	2018-04-14 20:56:15.698171
2718	29	78	78	2018-04-14 20:56:15.703451	2018-04-14 20:56:15.703451
2719	29	78	84	2018-04-14 20:56:15.709586	2018-04-14 20:56:15.709586
2720	29	84	84	2018-04-14 20:56:15.715357	2018-04-14 20:56:15.715357
2721	29	91	46	2018-04-14 20:56:15.722667	2018-04-14 20:56:15.722667
2722	29	91	70	2018-04-14 20:56:15.728638	2018-04-14 20:56:15.728638
2723	29	91	78	2018-04-14 20:56:15.733946	2018-04-14 20:56:15.733946
2724	29	91	84	2018-04-14 20:56:15.7405	2018-04-14 20:56:15.7405
2725	29	91	91	2018-04-14 20:56:15.74613	2018-04-14 20:56:15.74613
2726	29	95	46	2018-04-14 20:56:15.752823	2018-04-14 20:56:15.752823
2727	29	95	70	2018-04-14 20:56:15.759586	2018-04-14 20:56:15.759586
2728	29	95	78	2018-04-14 20:56:15.764973	2018-04-14 20:56:15.764973
2729	29	95	84	2018-04-14 20:56:15.770544	2018-04-14 20:56:15.770544
2730	29	95	91	2018-04-14 20:56:15.776826	2018-04-14 20:56:15.776826
2731	29	95	95	2018-04-14 20:56:15.782604	2018-04-14 20:56:15.782604
2732	29	98	61	2018-04-14 20:56:15.789075	2018-04-14 20:56:15.789075
2733	29	98	78	2018-04-14 20:56:15.794882	2018-04-14 20:56:15.794882
2734	29	98	91	2018-04-14 20:56:15.800521	2018-04-14 20:56:15.800521
2735	29	98	95	2018-04-14 20:56:15.806892	2018-04-14 20:56:15.806892
2736	29	98	98	2018-04-14 20:56:15.812593	2018-04-14 20:56:15.812593
2737	30	1	97	2018-04-14 20:56:15.817941	2018-04-14 20:56:15.817941
2738	30	5	97	2018-04-14 20:56:15.8247	2018-04-14 20:56:15.8247
2739	30	7	97	2018-04-14 20:56:15.830143	2018-04-14 20:56:15.830143
2740	30	8	97	2018-04-14 20:56:15.835241	2018-04-14 20:56:15.835241
2741	30	9	97	2018-04-14 20:56:15.840133	2018-04-14 20:56:15.840133
2742	30	10	97	2018-04-14 20:56:15.845253	2018-04-14 20:56:15.845253
2743	30	13	97	2018-04-14 20:56:15.850515	2018-04-14 20:56:15.850515
2744	30	14	97	2018-04-14 20:56:15.855346	2018-04-14 20:56:15.855346
2745	30	15	97	2018-04-14 20:56:15.860157	2018-04-14 20:56:15.860157
2746	30	16	97	2018-04-14 20:56:15.866624	2018-04-14 20:56:15.866624
2747	30	17	97	2018-04-14 20:56:15.871115	2018-04-14 20:56:15.871115
2748	30	21	97	2018-04-14 20:56:15.875753	2018-04-14 20:56:15.875753
2749	30	23	97	2018-04-14 20:56:15.880701	2018-04-14 20:56:15.880701
2750	30	24	97	2018-04-14 20:56:15.885467	2018-04-14 20:56:15.885467
2751	30	25	97	2018-04-14 20:56:15.890495	2018-04-14 20:56:15.890495
2752	30	26	97	2018-04-14 20:56:15.897682	2018-04-14 20:56:15.897682
2753	30	27	97	2018-04-14 20:56:15.904871	2018-04-14 20:56:15.904871
2754	30	28	97	2018-04-14 20:56:15.910468	2018-04-14 20:56:15.910468
2755	30	30	97	2018-04-14 20:56:15.915425	2018-04-14 20:56:15.915425
2756	30	31	97	2018-04-14 20:56:15.920738	2018-04-14 20:56:15.920738
2757	30	32	97	2018-04-14 20:56:15.925431	2018-04-14 20:56:15.925431
2758	30	33	97	2018-04-14 20:56:15.931868	2018-04-14 20:56:15.931868
2759	30	34	97	2018-04-14 20:56:15.937469	2018-04-14 20:56:15.937469
2760	30	36	97	2018-04-14 20:56:15.942908	2018-04-14 20:56:15.942908
2761	30	39	97	2018-04-14 20:56:15.948665	2018-04-14 20:56:15.948665
2762	30	40	97	2018-04-14 20:56:15.954055	2018-04-14 20:56:15.954055
2763	30	41	97	2018-04-14 20:56:15.96105	2018-04-14 20:56:15.96105
2764	30	42	97	2018-04-14 20:56:15.966362	2018-04-14 20:56:15.966362
2765	30	43	97	2018-04-14 20:56:15.971939	2018-04-14 20:56:15.971939
2766	30	44	97	2018-04-14 20:56:15.978002	2018-04-14 20:56:15.978002
2767	30	47	97	2018-04-14 20:56:15.983923	2018-04-14 20:56:15.983923
2768	30	48	97	2018-04-14 20:56:15.989785	2018-04-14 20:56:15.989785
2769	30	49	97	2018-04-14 20:56:15.995703	2018-04-14 20:56:15.995703
2770	30	50	97	2018-04-14 20:56:16.001723	2018-04-14 20:56:16.001723
2771	30	52	97	2018-04-14 20:56:16.008274	2018-04-14 20:56:16.008274
2772	30	59	97	2018-04-14 20:56:16.014141	2018-04-14 20:56:16.014141
2773	30	61	97	2018-04-14 20:56:16.019998	2018-04-14 20:56:16.019998
2774	30	67	97	2018-04-14 20:56:16.026963	2018-04-14 20:56:16.026963
2775	30	72	97	2018-04-14 20:56:16.0327	2018-04-14 20:56:16.0327
2776	30	73	96	2018-04-14 20:56:16.039587	2018-04-14 20:56:16.039587
2777	30	74	97	2018-04-14 20:56:16.046009	2018-04-14 20:56:16.046009
2778	30	75	97	2018-04-14 20:56:16.052339	2018-04-14 20:56:16.052339
2779	30	76	97	2018-04-14 20:56:16.060311	2018-04-14 20:56:16.060311
2780	30	78	97	2018-04-14 20:56:16.066787	2018-04-14 20:56:16.066787
2781	30	79	97	2018-04-14 20:56:16.07408	2018-04-14 20:56:16.07408
2782	30	82	97	2018-04-14 20:56:16.080178	2018-04-14 20:56:16.080178
2783	30	85	97	2018-04-14 20:56:16.086073	2018-04-14 20:56:16.086073
2784	30	86	97	2018-04-14 20:56:16.092881	2018-04-14 20:56:16.092881
2785	30	88	97	2018-04-14 20:56:16.098328	2018-04-14 20:56:16.098328
2786	30	89	97	2018-04-14 20:56:16.103457	2018-04-14 20:56:16.103457
2787	30	90	97	2018-04-14 20:56:16.108825	2018-04-14 20:56:16.108825
2788	30	91	97	2018-04-14 20:56:16.114141	2018-04-14 20:56:16.114141
2789	30	93	97	2018-04-14 20:56:16.120133	2018-04-14 20:56:16.120133
2790	30	95	97	2018-04-14 20:56:16.125262	2018-04-14 20:56:16.125262
2791	30	96	97	2018-04-14 20:56:16.132073	2018-04-14 20:56:16.132073
2792	30	97	2	2018-04-14 20:56:16.137732	2018-04-14 20:56:16.137732
2793	30	97	3	2018-04-14 20:56:16.142467	2018-04-14 20:56:16.142467
2794	30	97	4	2018-04-14 20:56:16.148055	2018-04-14 20:56:16.148055
2795	30	97	12	2018-04-14 20:56:16.154637	2018-04-14 20:56:16.154637
2796	30	97	18	2018-04-14 20:56:16.16092	2018-04-14 20:56:16.16092
2797	30	97	20	2018-04-14 20:56:16.166582	2018-04-14 20:56:16.166582
2798	30	97	29	2018-04-14 20:56:16.172481	2018-04-14 20:56:16.172481
2799	30	97	37	2018-04-14 20:56:16.177715	2018-04-14 20:56:16.177715
2800	30	97	45	2018-04-14 20:56:16.1839	2018-04-14 20:56:16.1839
2801	30	97	46	2018-04-14 20:56:16.189056	2018-04-14 20:56:16.189056
2802	30	97	53	2018-04-14 20:56:16.20037	2018-04-14 20:56:16.20037
2803	30	97	54	2018-04-14 20:56:16.205169	2018-04-14 20:56:16.205169
2804	30	97	55	2018-04-14 20:56:16.209908	2018-04-14 20:56:16.209908
2805	30	97	56	2018-04-14 20:56:16.214519	2018-04-14 20:56:16.214519
2806	30	97	57	2018-04-14 20:56:16.219401	2018-04-14 20:56:16.219401
2807	30	97	58	2018-04-14 20:56:16.224082	2018-04-14 20:56:16.224082
2808	30	97	60	2018-04-14 20:56:16.228618	2018-04-14 20:56:16.228618
2809	30	97	63	2018-04-14 20:56:16.233299	2018-04-14 20:56:16.233299
2810	30	97	64	2018-04-14 20:56:16.237707	2018-04-14 20:56:16.237707
2811	30	97	65	2018-04-14 20:56:16.242671	2018-04-14 20:56:16.242671
2812	30	97	69	2018-04-14 20:56:16.248082	2018-04-14 20:56:16.248082
2813	30	97	70	2018-04-14 20:56:16.252809	2018-04-14 20:56:16.252809
2814	30	97	71	2018-04-14 20:56:16.257464	2018-04-14 20:56:16.257464
2815	30	97	77	2018-04-14 20:56:16.261869	2018-04-14 20:56:16.261869
2816	30	97	80	2018-04-14 20:56:16.266758	2018-04-14 20:56:16.266758
2817	30	97	81	2018-04-14 20:56:16.271284	2018-04-14 20:56:16.271284
2818	30	97	84	2018-04-14 20:56:16.276185	2018-04-14 20:56:16.276185
2819	30	97	87	2018-04-14 20:56:16.280838	2018-04-14 20:56:16.280838
2820	30	97	92	2018-04-14 20:56:16.285612	2018-04-14 20:56:16.285612
2821	30	97	94	2018-04-14 20:56:16.290512	2018-04-14 20:56:16.290512
2822	30	97	97	2018-04-14 20:56:16.295337	2018-04-14 20:56:16.295337
2823	30	97	99	2018-04-14 20:56:16.300421	2018-04-14 20:56:16.300421
2824	30	97	101	2018-04-14 20:56:16.305482	2018-04-14 20:56:16.305482
2825	30	97	103	2018-04-14 20:56:16.310439	2018-04-14 20:56:16.310439
2826	30	97	107	2018-04-14 20:56:16.315904	2018-04-14 20:56:16.315904
2827	30	97	109	2018-04-14 20:56:16.321283	2018-04-14 20:56:16.321283
2828	30	98	97	2018-04-14 20:56:16.326321	2018-04-14 20:56:16.326321
2829	30	102	97	2018-04-14 20:56:16.334404	2018-04-14 20:56:16.334404
2830	30	104	97	2018-04-14 20:56:16.339834	2018-04-14 20:56:16.339834
2831	30	108	97	2018-04-14 20:56:16.346277	2018-04-14 20:56:16.346277
2832	30	112	97	2018-04-14 20:56:16.352012	2018-04-14 20:56:16.352012
2833	31	1	47	2018-04-14 20:56:16.358179	2018-04-14 20:56:16.358179
2834	31	5	9	2018-04-14 20:56:16.36355	2018-04-14 20:56:16.36355
2835	31	5	47	2018-04-14 20:56:16.370114	2018-04-14 20:56:16.370114
2836	31	7	7	2018-04-14 20:56:16.376017	2018-04-14 20:56:16.376017
2837	31	7	9	2018-04-14 20:56:16.382063	2018-04-14 20:56:16.382063
2838	31	7	18	2018-04-14 20:56:16.387292	2018-04-14 20:56:16.387292
2839	31	7	47	2018-04-14 20:56:16.3949	2018-04-14 20:56:16.3949
2840	31	7	54	2018-04-14 20:56:16.400329	2018-04-14 20:56:16.400329
2841	31	8	47	2018-04-14 20:56:16.404951	2018-04-14 20:56:16.404951
2842	31	9	9	2018-04-14 20:56:16.410305	2018-04-14 20:56:16.410305
2843	31	9	18	2018-04-14 20:56:16.415554	2018-04-14 20:56:16.415554
2844	31	9	36	2018-04-14 20:56:16.420379	2018-04-14 20:56:16.420379
2845	31	9	47	2018-04-14 20:56:16.424976	2018-04-14 20:56:16.424976
2846	31	9	53	2018-04-14 20:56:16.429988	2018-04-14 20:56:16.429988
2847	31	9	103	2018-04-14 20:56:16.43529	2018-04-14 20:56:16.43529
2848	31	10	47	2018-04-14 20:56:16.44048	2018-04-14 20:56:16.44048
2849	31	14	14	2018-04-14 20:56:16.446562	2018-04-14 20:56:16.446562
2850	31	25	47	2018-04-14 20:56:16.451769	2018-04-14 20:56:16.451769
2851	31	30	47	2018-04-14 20:56:16.456736	2018-04-14 20:56:16.456736
2852	31	42	7	2018-04-14 20:56:16.462223	2018-04-14 20:56:16.462223
2853	31	42	9	2018-04-14 20:56:16.467511	2018-04-14 20:56:16.467511
2854	31	42	47	2018-04-14 20:56:16.472128	2018-04-14 20:56:16.472128
2855	31	44	47	2018-04-14 20:56:16.476798	2018-04-14 20:56:16.476798
2856	31	47	2	2018-04-14 20:56:16.481944	2018-04-14 20:56:16.481944
2857	31	47	4	2018-04-14 20:56:16.487056	2018-04-14 20:56:16.487056
2858	31	47	18	2018-04-14 20:56:16.491579	2018-04-14 20:56:16.491579
2859	31	47	36	2018-04-14 20:56:16.496116	2018-04-14 20:56:16.496116
2860	31	47	40	2018-04-14 20:56:16.500969	2018-04-14 20:56:16.500969
2861	31	47	47	2018-04-14 20:56:16.505695	2018-04-14 20:56:16.505695
2862	31	47	60	2018-04-14 20:56:16.510443	2018-04-14 20:56:16.510443
2863	31	47	77	2018-04-14 20:56:16.516003	2018-04-14 20:56:16.516003
2864	31	59	47	2018-04-14 20:56:16.520746	2018-04-14 20:56:16.520746
2865	31	76	47	2018-04-14 20:56:16.525218	2018-04-14 20:56:16.525218
2866	31	79	47	2018-04-14 20:56:16.529782	2018-04-14 20:56:16.529782
2867	31	90	47	2018-04-14 20:56:16.534625	2018-04-14 20:56:16.534625
2868	31	93	47	2018-04-14 20:56:16.539143	2018-04-14 20:56:16.539143
2869	31	102	7	2018-04-14 20:56:16.543713	2018-04-14 20:56:16.543713
2870	31	102	9	2018-04-14 20:56:16.548176	2018-04-14 20:56:16.548176
2871	31	102	47	2018-04-14 20:56:16.552999	2018-04-14 20:56:16.552999
2872	32	9	24	2018-04-14 20:56:16.561051	2018-04-14 20:56:16.561051
2873	32	9	31	2018-04-14 20:56:16.568383	2018-04-14 20:56:16.568383
2874	32	9	32	2018-04-14 20:56:16.573702	2018-04-14 20:56:16.573702
2875	32	9	85	2018-04-14 20:56:16.578445	2018-04-14 20:56:16.578445
2876	32	47	15	2018-04-14 20:56:16.583164	2018-04-14 20:56:16.583164
2877	32	47	16	2018-04-14 20:56:16.589208	2018-04-14 20:56:16.589208
2878	32	47	17	2018-04-14 20:56:16.597571	2018-04-14 20:56:16.597571
2879	32	47	24	2018-04-14 20:56:16.603951	2018-04-14 20:56:16.603951
2880	32	47	31	2018-04-14 20:56:16.608688	2018-04-14 20:56:16.608688
2881	32	47	32	2018-04-14 20:56:16.613157	2018-04-14 20:56:16.613157
2882	32	47	39	2018-04-14 20:56:16.618981	2018-04-14 20:56:16.618981
2883	32	47	41	2018-04-14 20:56:16.624101	2018-04-14 20:56:16.624101
2884	32	47	46	2018-04-14 20:56:16.629744	2018-04-14 20:56:16.629744
2885	32	47	61	2018-04-14 20:56:16.635381	2018-04-14 20:56:16.635381
2886	32	47	70	2018-04-14 20:56:16.640373	2018-04-14 20:56:16.640373
2887	32	47	72	2018-04-14 20:56:16.646989	2018-04-14 20:56:16.646989
2888	32	47	74	2018-04-14 20:56:16.652357	2018-04-14 20:56:16.652357
2889	32	47	78	2018-04-14 20:56:16.65768	2018-04-14 20:56:16.65768
2890	32	47	84	2018-04-14 20:56:16.669771	2018-04-14 20:56:16.669771
2891	32	47	85	2018-04-14 20:56:16.675215	2018-04-14 20:56:16.675215
2892	32	47	86	2018-04-14 20:56:16.68192	2018-04-14 20:56:16.68192
2893	32	47	89	2018-04-14 20:56:16.687418	2018-04-14 20:56:16.687418
2894	32	47	91	2018-04-14 20:56:16.693109	2018-04-14 20:56:16.693109
2895	32	47	95	2018-04-14 20:56:16.699529	2018-04-14 20:56:16.699529
2896	33	26	3	2018-04-14 20:56:16.704935	2018-04-14 20:56:16.704935
2897	33	26	6	2018-04-14 20:56:16.711367	2018-04-14 20:56:16.711367
2898	33	26	20	2018-04-14 20:56:16.717678	2018-04-14 20:56:16.717678
2899	33	26	22	2018-04-14 20:56:16.723349	2018-04-14 20:56:16.723349
2900	33	26	29	2018-04-14 20:56:16.730909	2018-04-14 20:56:16.730909
2901	33	26	37	2018-04-14 20:56:16.73675	2018-04-14 20:56:16.73675
2902	33	26	38	2018-04-14 20:56:16.74178	2018-04-14 20:56:16.74178
2903	33	26	45	2018-04-14 20:56:16.748623	2018-04-14 20:56:16.748623
2904	33	26	55	2018-04-14 20:56:16.754446	2018-04-14 20:56:16.754446
2905	33	26	56	2018-04-14 20:56:16.760858	2018-04-14 20:56:16.760858
2906	33	26	57	2018-04-14 20:56:16.767346	2018-04-14 20:56:16.767346
2907	33	26	58	2018-04-14 20:56:16.773015	2018-04-14 20:56:16.773015
2908	33	26	65	2018-04-14 20:56:16.78003	2018-04-14 20:56:16.78003
2909	33	26	69	2018-04-14 20:56:16.786057	2018-04-14 20:56:16.786057
2910	33	26	71	2018-04-14 20:56:16.791805	2018-04-14 20:56:16.791805
2911	33	26	81	2018-04-14 20:56:16.799382	2018-04-14 20:56:16.799382
2912	33	26	94	2018-04-14 20:56:16.804988	2018-04-14 20:56:16.804988
2913	33	26	99	2018-04-14 20:56:16.812463	2018-04-14 20:56:16.812463
2914	33	26	101	2018-04-14 20:56:16.818726	2018-04-14 20:56:16.818726
2915	33	26	106	2018-04-14 20:56:16.824134	2018-04-14 20:56:16.824134
2916	33	26	107	2018-04-14 20:56:16.829937	2018-04-14 20:56:16.829937
2917	33	26	110	2018-04-14 20:56:16.835101	2018-04-14 20:56:16.835101
2918	33	26	111	2018-04-14 20:56:16.840617	2018-04-14 20:56:16.840617
2919	33	49	3	2018-04-14 20:56:16.846273	2018-04-14 20:56:16.846273
2920	33	49	12	2018-04-14 20:56:16.854168	2018-04-14 20:56:16.854168
2921	33	49	20	2018-04-14 20:56:16.859801	2018-04-14 20:56:16.859801
2922	33	49	29	2018-04-14 20:56:16.865915	2018-04-14 20:56:16.865915
2923	33	49	37	2018-04-14 20:56:16.871828	2018-04-14 20:56:16.871828
2924	33	49	45	2018-04-14 20:56:16.879186	2018-04-14 20:56:16.879186
2925	33	49	53	2018-04-14 20:56:16.887933	2018-04-14 20:56:16.887933
2926	33	49	54	2018-04-14 20:56:16.893767	2018-04-14 20:56:16.893767
2927	33	49	55	2018-04-14 20:56:16.900401	2018-04-14 20:56:16.900401
2928	33	49	56	2018-04-14 20:56:16.906394	2018-04-14 20:56:16.906394
2929	33	49	57	2018-04-14 20:56:16.913163	2018-04-14 20:56:16.913163
2930	33	49	58	2018-04-14 20:56:16.919978	2018-04-14 20:56:16.919978
2931	33	49	65	2018-04-14 20:56:16.927056	2018-04-14 20:56:16.927056
2932	33	49	69	2018-04-14 20:56:16.935093	2018-04-14 20:56:16.935093
2933	33	49	81	2018-04-14 20:56:16.945517	2018-04-14 20:56:16.945517
2934	33	49	87	2018-04-14 20:56:16.954828	2018-04-14 20:56:16.954828
2935	33	49	94	2018-04-14 20:56:16.962634	2018-04-14 20:56:16.962634
2936	33	49	99	2018-04-14 20:56:16.970383	2018-04-14 20:56:16.970383
2937	33	49	101	2018-04-14 20:56:16.977599	2018-04-14 20:56:16.977599
2938	33	49	107	2018-04-14 20:56:16.986218	2018-04-14 20:56:16.986218
2939	33	49	109	2018-04-14 20:56:16.993289	2018-04-14 20:56:16.993289
2940	33	50	3	2018-04-14 20:56:17.004024	2018-04-14 20:56:17.004024
2941	33	50	20	2018-04-14 20:56:17.011322	2018-04-14 20:56:17.011322
2942	33	50	29	2018-04-14 20:56:17.021482	2018-04-14 20:56:17.021482
2943	33	50	37	2018-04-14 20:56:17.028329	2018-04-14 20:56:17.028329
2944	33	50	45	2018-04-14 20:56:17.035904	2018-04-14 20:56:17.035904
2945	33	50	54	2018-04-14 20:56:17.043306	2018-04-14 20:56:17.043306
2946	33	50	55	2018-04-14 20:56:17.050638	2018-04-14 20:56:17.050638
2947	33	50	56	2018-04-14 20:56:17.061411	2018-04-14 20:56:17.061411
2948	33	50	57	2018-04-14 20:56:17.06868	2018-04-14 20:56:17.06868
2949	33	50	58	2018-04-14 20:56:17.076271	2018-04-14 20:56:17.076271
2950	33	50	65	2018-04-14 20:56:17.086238	2018-04-14 20:56:17.086238
2951	33	50	69	2018-04-14 20:56:17.093166	2018-04-14 20:56:17.093166
2952	33	50	81	2018-04-14 20:56:17.103452	2018-04-14 20:56:17.103452
2953	33	50	94	2018-04-14 20:56:17.111354	2018-04-14 20:56:17.111354
2954	33	50	99	2018-04-14 20:56:17.118321	2018-04-14 20:56:17.118321
2955	33	50	101	2018-04-14 20:56:17.125133	2018-04-14 20:56:17.125133
2956	33	50	107	2018-04-14 20:56:17.131361	2018-04-14 20:56:17.131361
2957	34	28	1	2018-04-14 20:56:17.138217	2018-04-14 20:56:17.138217
2958	34	28	2	2018-04-14 20:56:17.145189	2018-04-14 20:56:17.145189
2959	34	28	4	2018-04-14 20:56:17.155129	2018-04-14 20:56:17.155129
2960	34	28	5	2018-04-14 20:56:17.163908	2018-04-14 20:56:17.163908
2961	34	28	7	2018-04-14 20:56:17.171041	2018-04-14 20:56:17.171041
2962	34	28	8	2018-04-14 20:56:17.179694	2018-04-14 20:56:17.179694
2963	34	28	14	2018-04-14 20:56:17.188165	2018-04-14 20:56:17.188165
2964	34	28	25	2018-04-14 20:56:17.196315	2018-04-14 20:56:17.196315
2965	34	28	26	2018-04-14 20:56:17.204172	2018-04-14 20:56:17.204172
2966	34	28	30	2018-04-14 20:56:17.210937	2018-04-14 20:56:17.210937
2967	34	28	34	2018-04-14 20:56:17.217802	2018-04-14 20:56:17.217802
2968	34	28	36	2018-04-14 20:56:17.227748	2018-04-14 20:56:17.227748
2969	34	28	40	2018-04-14 20:56:17.235658	2018-04-14 20:56:17.235658
2970	34	28	44	2018-04-14 20:56:17.24177	2018-04-14 20:56:17.24177
2971	34	28	59	2018-04-14 20:56:17.249193	2018-04-14 20:56:17.249193
2972	34	28	60	2018-04-14 20:56:17.279217	2018-04-14 20:56:17.279217
2973	34	28	73	2018-04-14 20:56:17.286474	2018-04-14 20:56:17.286474
2974	34	28	76	2018-04-14 20:56:17.291858	2018-04-14 20:56:17.291858
2975	34	28	77	2018-04-14 20:56:17.296373	2018-04-14 20:56:17.296373
2976	34	28	90	2018-04-14 20:56:17.300707	2018-04-14 20:56:17.300707
2977	34	28	93	2018-04-14 20:56:17.304857	2018-04-14 20:56:17.304857
2978	34	43	28	2018-04-14 20:56:17.309293	2018-04-14 20:56:17.309293
2979	34	67	28	2018-04-14 20:56:17.313451	2018-04-14 20:56:17.313451
2980	34	75	28	2018-04-14 20:56:17.317889	2018-04-14 20:56:17.317889
2981	34	112	28	2018-04-14 20:56:17.322364	2018-04-14 20:56:17.322364
2982	35	10	28	2018-04-14 20:56:17.326871	2018-04-14 20:56:17.326871
2983	35	10	104	2018-04-14 20:56:17.331282	2018-04-14 20:56:17.331282
2984	35	28	3	2018-04-14 20:56:17.335791	2018-04-14 20:56:17.335791
2985	35	28	9	2018-04-14 20:56:17.340524	2018-04-14 20:56:17.340524
2986	35	28	12	2018-04-14 20:56:17.344859	2018-04-14 20:56:17.344859
2987	35	28	13	2018-04-14 20:56:17.349532	2018-04-14 20:56:17.349532
2988	35	28	15	2018-04-14 20:56:17.353895	2018-04-14 20:56:17.353895
2989	35	28	16	2018-04-14 20:56:17.358516	2018-04-14 20:56:17.358516
2990	35	28	17	2018-04-14 20:56:17.362812	2018-04-14 20:56:17.362812
2991	35	28	18	2018-04-14 20:56:17.367252	2018-04-14 20:56:17.367252
2992	35	28	20	2018-04-14 20:56:17.371682	2018-04-14 20:56:17.371682
2993	35	28	23	2018-04-14 20:56:17.376215	2018-04-14 20:56:17.376215
2994	35	28	24	2018-04-14 20:56:17.380758	2018-04-14 20:56:17.380758
2995	35	28	27	2018-04-14 20:56:17.385611	2018-04-14 20:56:17.385611
2996	35	28	28	2018-04-14 20:56:17.39051	2018-04-14 20:56:17.39051
2997	35	28	29	2018-04-14 20:56:17.398617	2018-04-14 20:56:17.398617
2998	35	28	31	2018-04-14 20:56:17.405228	2018-04-14 20:56:17.405228
2999	35	28	33	2018-04-14 20:56:17.409737	2018-04-14 20:56:17.409737
3000	35	28	37	2018-04-14 20:56:17.414799	2018-04-14 20:56:17.414799
3001	35	28	39	2018-04-14 20:56:17.420571	2018-04-14 20:56:17.420571
3002	35	28	41	2018-04-14 20:56:17.425254	2018-04-14 20:56:17.425254
3003	35	28	42	2018-04-14 20:56:17.429987	2018-04-14 20:56:17.429987
3004	35	28	45	2018-04-14 20:56:17.43458	2018-04-14 20:56:17.43458
3005	35	28	46	2018-04-14 20:56:17.439787	2018-04-14 20:56:17.439787
3006	35	28	47	2018-04-14 20:56:17.445903	2018-04-14 20:56:17.445903
3007	35	28	48	2018-04-14 20:56:17.451521	2018-04-14 20:56:17.451521
3008	35	28	49	2018-04-14 20:56:17.456266	2018-04-14 20:56:17.456266
3009	35	28	50	2018-04-14 20:56:17.461106	2018-04-14 20:56:17.461106
3010	35	28	52	2018-04-14 20:56:17.465666	2018-04-14 20:56:17.465666
3011	35	28	53	2018-04-14 20:56:17.470654	2018-04-14 20:56:17.470654
3012	35	28	54	2018-04-14 20:56:17.475238	2018-04-14 20:56:17.475238
3013	35	28	55	2018-04-14 20:56:17.480024	2018-04-14 20:56:17.480024
3014	35	28	56	2018-04-14 20:56:17.485063	2018-04-14 20:56:17.485063
3015	35	28	57	2018-04-14 20:56:17.489948	2018-04-14 20:56:17.489948
3016	35	28	58	2018-04-14 20:56:17.494614	2018-04-14 20:56:17.494614
3017	35	28	61	2018-04-14 20:56:17.498932	2018-04-14 20:56:17.498932
3018	35	28	64	2018-04-14 20:56:17.503923	2018-04-14 20:56:17.503923
3019	35	28	65	2018-04-14 20:56:17.508677	2018-04-14 20:56:17.508677
3020	35	28	69	2018-04-14 20:56:17.513181	2018-04-14 20:56:17.513181
3021	35	28	72	2018-04-14 20:56:17.518912	2018-04-14 20:56:17.518912
3022	35	28	74	2018-04-14 20:56:17.525483	2018-04-14 20:56:17.525483
3023	35	28	78	2018-04-14 20:56:17.530385	2018-04-14 20:56:17.530385
3024	35	28	80	2018-04-14 20:56:17.535363	2018-04-14 20:56:17.535363
3025	35	28	81	2018-04-14 20:56:17.540036	2018-04-14 20:56:17.540036
3026	35	28	82	2018-04-14 20:56:17.545197	2018-04-14 20:56:17.545197
3027	35	28	84	2018-04-14 20:56:17.550037	2018-04-14 20:56:17.550037
3028	35	28	85	2018-04-14 20:56:17.554383	2018-04-14 20:56:17.554383
3029	35	28	86	2018-04-14 20:56:17.559219	2018-04-14 20:56:17.559219
3030	35	28	87	2018-04-14 20:56:17.563816	2018-04-14 20:56:17.563816
3031	35	28	88	2018-04-14 20:56:17.568995	2018-04-14 20:56:17.568995
3032	35	28	89	2018-04-14 20:56:17.57412	2018-04-14 20:56:17.57412
3033	35	28	91	2018-04-14 20:56:17.580244	2018-04-14 20:56:17.580244
3034	35	28	92	2018-04-14 20:56:17.586238	2018-04-14 20:56:17.586238
3035	35	28	94	2018-04-14 20:56:17.591371	2018-04-14 20:56:17.591371
3036	35	28	95	2018-04-14 20:56:17.596191	2018-04-14 20:56:17.596191
3037	35	28	98	2018-04-14 20:56:17.601178	2018-04-14 20:56:17.601178
3038	35	28	99	2018-04-14 20:56:17.606483	2018-04-14 20:56:17.606483
3039	35	28	101	2018-04-14 20:56:17.611014	2018-04-14 20:56:17.611014
3040	35	28	102	2018-04-14 20:56:17.615875	2018-04-14 20:56:17.615875
3041	35	28	104	2018-04-14 20:56:17.620637	2018-04-14 20:56:17.620637
3042	35	28	107	2018-04-14 20:56:17.625236	2018-04-14 20:56:17.625236
3043	35	28	108	2018-04-14 20:56:17.629873	2018-04-14 20:56:17.629873
3044	35	28	109	2018-04-14 20:56:17.634734	2018-04-14 20:56:17.634734
3045	35	43	104	2018-04-14 20:56:17.640027	2018-04-14 20:56:17.640027
3046	35	67	104	2018-04-14 20:56:17.645216	2018-04-14 20:56:17.645216
3047	35	75	104	2018-04-14 20:56:17.649682	2018-04-14 20:56:17.649682
3048	35	104	1	2018-04-14 20:56:17.65409	2018-04-14 20:56:17.65409
3049	35	104	2	2018-04-14 20:56:17.658768	2018-04-14 20:56:17.658768
3050	35	104	3	2018-04-14 20:56:17.663687	2018-04-14 20:56:17.663687
3051	35	104	4	2018-04-14 20:56:17.668634	2018-04-14 20:56:17.668634
3052	35	104	5	2018-04-14 20:56:17.673471	2018-04-14 20:56:17.673471
3053	35	104	7	2018-04-14 20:56:17.67846	2018-04-14 20:56:17.67846
3054	35	104	8	2018-04-14 20:56:17.683008	2018-04-14 20:56:17.683008
3055	35	104	9	2018-04-14 20:56:17.687612	2018-04-14 20:56:17.687612
3056	35	104	12	2018-04-14 20:56:17.692458	2018-04-14 20:56:17.692458
3057	35	104	13	2018-04-14 20:56:17.697227	2018-04-14 20:56:17.697227
3058	35	104	14	2018-04-14 20:56:17.701865	2018-04-14 20:56:17.701865
3059	35	104	15	2018-04-14 20:56:17.706365	2018-04-14 20:56:17.706365
3060	35	104	16	2018-04-14 20:56:17.711158	2018-04-14 20:56:17.711158
3061	35	104	17	2018-04-14 20:56:17.716123	2018-04-14 20:56:17.716123
3062	35	104	18	2018-04-14 20:56:17.720535	2018-04-14 20:56:17.720535
3063	35	104	20	2018-04-14 20:56:17.725078	2018-04-14 20:56:17.725078
3064	35	104	23	2018-04-14 20:56:17.729574	2018-04-14 20:56:17.729574
3065	35	104	24	2018-04-14 20:56:17.734722	2018-04-14 20:56:17.734722
3066	35	104	25	2018-04-14 20:56:17.739592	2018-04-14 20:56:17.739592
3067	35	104	26	2018-04-14 20:56:17.74423	2018-04-14 20:56:17.74423
3068	35	104	27	2018-04-14 20:56:17.748815	2018-04-14 20:56:17.748815
3069	35	104	29	2018-04-14 20:56:17.753216	2018-04-14 20:56:17.753216
3070	35	104	30	2018-04-14 20:56:17.758272	2018-04-14 20:56:17.758272
3071	35	104	31	2018-04-14 20:56:17.7627	2018-04-14 20:56:17.7627
3072	35	104	33	2018-04-14 20:56:17.767073	2018-04-14 20:56:17.767073
3073	35	104	34	2018-04-14 20:56:17.771353	2018-04-14 20:56:17.771353
3074	35	104	36	2018-04-14 20:56:17.777957	2018-04-14 20:56:17.777957
3075	35	104	37	2018-04-14 20:56:17.782439	2018-04-14 20:56:17.782439
3076	35	104	39	2018-04-14 20:56:17.786797	2018-04-14 20:56:17.786797
3077	35	104	40	2018-04-14 20:56:17.790986	2018-04-14 20:56:17.790986
3078	35	104	41	2018-04-14 20:56:17.795226	2018-04-14 20:56:17.795226
3079	35	104	42	2018-04-14 20:56:17.79958	2018-04-14 20:56:17.79958
3080	35	104	44	2018-04-14 20:56:17.803851	2018-04-14 20:56:17.803851
3081	35	104	45	2018-04-14 20:56:17.808136	2018-04-14 20:56:17.808136
3082	35	104	46	2018-04-14 20:56:17.812442	2018-04-14 20:56:17.812442
3083	35	104	47	2018-04-14 20:56:17.816716	2018-04-14 20:56:17.816716
3084	35	104	48	2018-04-14 20:56:17.821509	2018-04-14 20:56:17.821509
3085	35	104	49	2018-04-14 20:56:17.8275	2018-04-14 20:56:17.8275
3086	35	104	50	2018-04-14 20:56:17.832913	2018-04-14 20:56:17.832913
3087	35	104	52	2018-04-14 20:56:17.83925	2018-04-14 20:56:17.83925
3088	35	104	53	2018-04-14 20:56:17.846023	2018-04-14 20:56:17.846023
3089	35	104	54	2018-04-14 20:56:17.852707	2018-04-14 20:56:17.852707
3090	35	104	55	2018-04-14 20:56:17.858911	2018-04-14 20:56:17.858911
3091	35	104	56	2018-04-14 20:56:17.86412	2018-04-14 20:56:17.86412
3092	35	104	57	2018-04-14 20:56:17.87018	2018-04-14 20:56:17.87018
3093	35	104	58	2018-04-14 20:56:17.875239	2018-04-14 20:56:17.875239
3094	35	104	59	2018-04-14 20:56:17.880695	2018-04-14 20:56:17.880695
3095	35	104	60	2018-04-14 20:56:17.885988	2018-04-14 20:56:17.885988
3096	35	104	61	2018-04-14 20:56:17.890772	2018-04-14 20:56:17.890772
3097	35	104	64	2018-04-14 20:56:17.895833	2018-04-14 20:56:17.895833
3098	35	104	65	2018-04-14 20:56:17.901292	2018-04-14 20:56:17.901292
3099	35	104	69	2018-04-14 20:56:17.907329	2018-04-14 20:56:17.907329
3100	35	104	72	2018-04-14 20:56:17.912337	2018-04-14 20:56:17.912337
3101	35	104	73	2018-04-14 20:56:17.919249	2018-04-14 20:56:17.919249
3102	35	104	74	2018-04-14 20:56:17.924739	2018-04-14 20:56:17.924739
3103	35	104	76	2018-04-14 20:56:17.929911	2018-04-14 20:56:17.929911
3104	35	104	77	2018-04-14 20:56:17.938047	2018-04-14 20:56:17.938047
3105	35	104	78	2018-04-14 20:56:17.944358	2018-04-14 20:56:17.944358
3106	35	104	80	2018-04-14 20:56:17.956952	2018-04-14 20:56:17.956952
3107	35	104	81	2018-04-14 20:56:17.97067	2018-04-14 20:56:17.97067
3108	35	104	82	2018-04-14 20:56:17.976227	2018-04-14 20:56:17.976227
3109	35	104	84	2018-04-14 20:56:17.981791	2018-04-14 20:56:17.981791
3110	35	104	85	2018-04-14 20:56:17.98895	2018-04-14 20:56:17.98895
3111	35	104	86	2018-04-14 20:56:17.996685	2018-04-14 20:56:17.996685
3112	35	104	87	2018-04-14 20:56:18.006738	2018-04-14 20:56:18.006738
3113	35	104	88	2018-04-14 20:56:18.015223	2018-04-14 20:56:18.015223
3114	35	104	89	2018-04-14 20:56:18.025909	2018-04-14 20:56:18.025909
3115	35	104	90	2018-04-14 20:56:18.033667	2018-04-14 20:56:18.033667
3116	35	104	91	2018-04-14 20:56:18.046131	2018-04-14 20:56:18.046131
3117	35	104	92	2018-04-14 20:56:18.057639	2018-04-14 20:56:18.057639
3118	35	104	93	2018-04-14 20:56:18.062828	2018-04-14 20:56:18.062828
3119	35	104	94	2018-04-14 20:56:18.069421	2018-04-14 20:56:18.069421
3120	35	104	95	2018-04-14 20:56:18.075019	2018-04-14 20:56:18.075019
3121	35	104	98	2018-04-14 20:56:18.08085	2018-04-14 20:56:18.08085
3122	35	104	99	2018-04-14 20:56:18.087431	2018-04-14 20:56:18.087431
3123	35	104	101	2018-04-14 20:56:18.092556	2018-04-14 20:56:18.092556
3124	35	104	102	2018-04-14 20:56:18.097963	2018-04-14 20:56:18.097963
3125	35	104	104	2018-04-14 20:56:18.104301	2018-04-14 20:56:18.104301
3126	35	104	107	2018-04-14 20:56:18.112887	2018-04-14 20:56:18.112887
3127	35	104	108	2018-04-14 20:56:18.119408	2018-04-14 20:56:18.119408
3128	35	104	109	2018-04-14 20:56:18.125036	2018-04-14 20:56:18.125036
3129	35	112	104	2018-04-14 20:56:18.130889	2018-04-14 20:56:18.130889
3130	36	24	48	2018-04-14 20:56:18.137257	2018-04-14 20:56:18.137257
3131	36	24	52	2018-04-14 20:56:18.142448	2018-04-14 20:56:18.142448
3132	36	24	98	2018-04-14 20:56:18.147985	2018-04-14 20:56:18.147985
3133	36	43	7	2018-04-14 20:56:18.154129	2018-04-14 20:56:18.154129
3134	36	43	9	2018-04-14 20:56:18.159217	2018-04-14 20:56:18.159217
3135	36	43	13	2018-04-14 20:56:18.164554	2018-04-14 20:56:18.164554
3136	36	43	14	2018-04-14 20:56:18.170542	2018-04-14 20:56:18.170542
3137	36	43	23	2018-04-14 20:56:18.176124	2018-04-14 20:56:18.176124
3138	36	43	26	2018-04-14 20:56:18.18122	2018-04-14 20:56:18.18122
3139	36	43	33	2018-04-14 20:56:18.187083	2018-04-14 20:56:18.187083
3140	36	43	34	2018-04-14 20:56:18.19226	2018-04-14 20:56:18.19226
3141	36	43	47	2018-04-14 20:56:18.197439	2018-04-14 20:56:18.197439
3142	36	43	49	2018-04-14 20:56:18.203527	2018-04-14 20:56:18.203527
3143	36	43	50	2018-04-14 20:56:18.208557	2018-04-14 20:56:18.208557
3144	36	43	108	2018-04-14 20:56:18.21392	2018-04-14 20:56:18.21392
3145	36	48	46	2018-04-14 20:56:18.22001	2018-04-14 20:56:18.22001
3146	36	48	64	2018-04-14 20:56:18.225444	2018-04-14 20:56:18.225444
3147	36	48	70	2018-04-14 20:56:18.231339	2018-04-14 20:56:18.231339
3148	36	48	80	2018-04-14 20:56:18.237259	2018-04-14 20:56:18.237259
3149	36	48	84	2018-04-14 20:56:18.242195	2018-04-14 20:56:18.242195
3150	36	52	46	2018-04-14 20:56:18.247186	2018-04-14 20:56:18.247186
3151	36	52	64	2018-04-14 20:56:18.25295	2018-04-14 20:56:18.25295
3152	36	52	70	2018-04-14 20:56:18.258424	2018-04-14 20:56:18.258424
3153	36	52	80	2018-04-14 20:56:18.263527	2018-04-14 20:56:18.263527
3154	36	52	84	2018-04-14 20:56:18.268974	2018-04-14 20:56:18.268974
3155	36	66	7	2018-04-14 20:56:18.274514	2018-04-14 20:56:18.274514
3156	36	66	9	2018-04-14 20:56:18.279491	2018-04-14 20:56:18.279491
3157	36	66	13	2018-04-14 20:56:18.284759	2018-04-14 20:56:18.284759
3158	36	66	14	2018-04-14 20:56:18.291061	2018-04-14 20:56:18.291061
3159	36	66	23	2018-04-14 20:56:18.296388	2018-04-14 20:56:18.296388
3160	36	66	26	2018-04-14 20:56:18.301628	2018-04-14 20:56:18.301628
3161	36	66	33	2018-04-14 20:56:18.307833	2018-04-14 20:56:18.307833
3162	36	66	34	2018-04-14 20:56:18.312881	2018-04-14 20:56:18.312881
3163	36	66	47	2018-04-14 20:56:18.317888	2018-04-14 20:56:18.317888
3164	36	66	49	2018-04-14 20:56:18.323984	2018-04-14 20:56:18.323984
3165	36	66	50	2018-04-14 20:56:18.32914	2018-04-14 20:56:18.32914
3166	36	66	108	2018-04-14 20:56:18.334417	2018-04-14 20:56:18.334417
3167	36	67	7	2018-04-14 20:56:18.340803	2018-04-14 20:56:18.340803
3168	36	67	9	2018-04-14 20:56:18.346094	2018-04-14 20:56:18.346094
3169	36	67	13	2018-04-14 20:56:18.351229	2018-04-14 20:56:18.351229
3170	36	67	14	2018-04-14 20:56:18.357615	2018-04-14 20:56:18.357615
3171	36	67	23	2018-04-14 20:56:18.362657	2018-04-14 20:56:18.362657
3172	36	67	26	2018-04-14 20:56:18.367743	2018-04-14 20:56:18.367743
3173	36	67	33	2018-04-14 20:56:18.374094	2018-04-14 20:56:18.374094
3174	36	67	34	2018-04-14 20:56:18.380204	2018-04-14 20:56:18.380204
3175	36	67	47	2018-04-14 20:56:18.38529	2018-04-14 20:56:18.38529
3176	36	67	49	2018-04-14 20:56:18.39219	2018-04-14 20:56:18.39219
3177	36	67	50	2018-04-14 20:56:18.397344	2018-04-14 20:56:18.397344
3178	36	67	108	2018-04-14 20:56:18.40266	2018-04-14 20:56:18.40266
3179	36	82	48	2018-04-14 20:56:18.408703	2018-04-14 20:56:18.408703
3180	36	82	52	2018-04-14 20:56:18.413866	2018-04-14 20:56:18.413866
3181	36	82	98	2018-04-14 20:56:18.41947	2018-04-14 20:56:18.41947
3182	36	85	48	2018-04-14 20:56:18.425456	2018-04-14 20:56:18.425456
3183	36	85	52	2018-04-14 20:56:18.430975	2018-04-14 20:56:18.430975
3184	36	85	98	2018-04-14 20:56:18.437318	2018-04-14 20:56:18.437318
3185	36	88	48	2018-04-14 20:56:18.44349	2018-04-14 20:56:18.44349
3186	36	88	52	2018-04-14 20:56:18.448702	2018-04-14 20:56:18.448702
3187	36	88	98	2018-04-14 20:56:18.454901	2018-04-14 20:56:18.454901
3188	36	89	48	2018-04-14 20:56:18.460858	2018-04-14 20:56:18.460858
3189	36	89	52	2018-04-14 20:56:18.466066	2018-04-14 20:56:18.466066
3190	36	89	98	2018-04-14 20:56:18.470938	2018-04-14 20:56:18.470938
3191	36	98	2	2018-04-14 20:56:18.475513	2018-04-14 20:56:18.475513
3192	36	98	4	2018-04-14 20:56:18.482281	2018-04-14 20:56:18.482281
3193	36	98	46	2018-04-14 20:56:18.488194	2018-04-14 20:56:18.488194
3194	36	98	60	2018-04-14 20:56:18.492823	2018-04-14 20:56:18.492823
3195	36	98	64	2018-04-14 20:56:18.499199	2018-04-14 20:56:18.499199
3196	36	98	70	2018-04-14 20:56:18.505185	2018-04-14 20:56:18.505185
3197	36	98	77	2018-04-14 20:56:18.511331	2018-04-14 20:56:18.511331
3198	36	98	80	2018-04-14 20:56:18.517689	2018-04-14 20:56:18.517689
3199	36	98	84	2018-04-14 20:56:18.52264	2018-04-14 20:56:18.52264
3200	36	112	7	2018-04-14 20:56:18.527528	2018-04-14 20:56:18.527528
3201	36	112	9	2018-04-14 20:56:18.542052	2018-04-14 20:56:18.542052
3202	36	112	13	2018-04-14 20:56:18.547705	2018-04-14 20:56:18.547705
3203	36	112	14	2018-04-14 20:56:18.552799	2018-04-14 20:56:18.552799
3204	36	112	23	2018-04-14 20:56:18.557337	2018-04-14 20:56:18.557337
3205	36	112	26	2018-04-14 20:56:18.561867	2018-04-14 20:56:18.561867
3206	36	112	33	2018-04-14 20:56:18.568489	2018-04-14 20:56:18.568489
3207	36	112	34	2018-04-14 20:56:18.57384	2018-04-14 20:56:18.57384
3208	36	112	47	2018-04-14 20:56:18.578508	2018-04-14 20:56:18.578508
3209	36	112	49	2018-04-14 20:56:18.58381	2018-04-14 20:56:18.58381
3210	36	112	50	2018-04-14 20:56:18.589987	2018-04-14 20:56:18.589987
3211	36	112	108	2018-04-14 20:56:18.595408	2018-04-14 20:56:18.595408
3212	37	43	73	2018-04-14 20:56:18.600942	2018-04-14 20:56:18.600942
3213	37	67	73	2018-04-14 20:56:18.607331	2018-04-14 20:56:18.607331
3214	37	73	1	2018-04-14 20:56:18.612233	2018-04-14 20:56:18.612233
3215	37	73	2	2018-04-14 20:56:18.617599	2018-04-14 20:56:18.617599
3216	37	73	4	2018-04-14 20:56:18.623406	2018-04-14 20:56:18.623406
3217	37	73	5	2018-04-14 20:56:18.628531	2018-04-14 20:56:18.628531
3218	37	73	15	2018-04-14 20:56:18.633966	2018-04-14 20:56:18.633966
3219	37	73	16	2018-04-14 20:56:18.639743	2018-04-14 20:56:18.639743
3220	37	73	17	2018-04-14 20:56:18.645041	2018-04-14 20:56:18.645041
3221	37	73	24	2018-04-14 20:56:18.650674	2018-04-14 20:56:18.650674
3222	37	73	36	2018-04-14 20:56:18.656961	2018-04-14 20:56:18.656961
3223	37	73	39	2018-04-14 20:56:18.6622	2018-04-14 20:56:18.6622
3224	37	73	40	2018-04-14 20:56:18.667576	2018-04-14 20:56:18.667576
3225	37	73	41	2018-04-14 20:56:18.67393	2018-04-14 20:56:18.67393
3226	37	73	42	2018-04-14 20:56:18.679061	2018-04-14 20:56:18.679061
3227	37	73	48	2018-04-14 20:56:18.684402	2018-04-14 20:56:18.684402
3228	37	73	52	2018-04-14 20:56:18.690896	2018-04-14 20:56:18.690896
3229	37	73	59	2018-04-14 20:56:18.696025	2018-04-14 20:56:18.696025
3230	37	73	60	2018-04-14 20:56:18.701505	2018-04-14 20:56:18.701505
3231	37	73	61	2018-04-14 20:56:18.70756	2018-04-14 20:56:18.70756
3232	37	73	63	2018-04-14 20:56:18.712791	2018-04-14 20:56:18.712791
3233	37	73	72	2018-04-14 20:56:18.718852	2018-04-14 20:56:18.718852
3234	37	73	73	2018-04-14 20:56:18.725103	2018-04-14 20:56:18.725103
3235	37	73	74	2018-04-14 20:56:18.730461	2018-04-14 20:56:18.730461
3236	37	73	76	2018-04-14 20:56:18.735687	2018-04-14 20:56:18.735687
3237	37	73	77	2018-04-14 20:56:18.741646	2018-04-14 20:56:18.741646
3238	37	73	78	2018-04-14 20:56:18.746681	2018-04-14 20:56:18.746681
3239	37	73	82	2018-04-14 20:56:18.752017	2018-04-14 20:56:18.752017
3240	37	73	85	2018-04-14 20:56:18.758257	2018-04-14 20:56:18.758257
3241	37	73	86	2018-04-14 20:56:18.763037	2018-04-14 20:56:18.763037
3242	37	73	88	2018-04-14 20:56:18.768605	2018-04-14 20:56:18.768605
3243	37	73	89	2018-04-14 20:56:18.774838	2018-04-14 20:56:18.774838
3244	37	73	91	2018-04-14 20:56:18.781116	2018-04-14 20:56:18.781116
3245	37	73	93	2018-04-14 20:56:18.786529	2018-04-14 20:56:18.786529
3246	37	73	95	2018-04-14 20:56:18.792814	2018-04-14 20:56:18.792814
3247	37	73	98	2018-04-14 20:56:18.798489	2018-04-14 20:56:18.798489
3248	37	73	102	2018-04-14 20:56:18.803623	2018-04-14 20:56:18.803623
3249	37	73	103	2018-04-14 20:56:18.809681	2018-04-14 20:56:18.809681
3250	37	75	73	2018-04-14 20:56:18.814953	2018-04-14 20:56:18.814953
3251	37	112	73	2018-04-14 20:56:18.819946	2018-04-14 20:56:18.819946
3252	38	1	96	2018-04-14 20:56:18.82633	2018-04-14 20:56:18.82633
3253	38	5	96	2018-04-14 20:56:18.83165	2018-04-14 20:56:18.83165
3254	38	7	96	2018-04-14 20:56:18.836685	2018-04-14 20:56:18.836685
3255	38	8	96	2018-04-14 20:56:18.842797	2018-04-14 20:56:18.842797
3256	38	9	96	2018-04-14 20:56:18.848005	2018-04-14 20:56:18.848005
3257	38	10	96	2018-04-14 20:56:18.853093	2018-04-14 20:56:18.853093
3258	38	11	96	2018-04-14 20:56:18.858943	2018-04-14 20:56:18.858943
3259	38	13	96	2018-04-14 20:56:18.867044	2018-04-14 20:56:18.867044
3260	38	14	96	2018-04-14 20:56:18.874791	2018-04-14 20:56:18.874791
3261	38	15	96	2018-04-14 20:56:18.881274	2018-04-14 20:56:18.881274
3262	38	16	96	2018-04-14 20:56:18.894552	2018-04-14 20:56:18.894552
3263	38	17	96	2018-04-14 20:56:18.907439	2018-04-14 20:56:18.907439
3264	38	21	96	2018-04-14 20:56:18.920251	2018-04-14 20:56:18.920251
3265	38	23	96	2018-04-14 20:56:18.932812	2018-04-14 20:56:18.932812
3266	38	24	96	2018-04-14 20:56:18.940312	2018-04-14 20:56:18.940312
3267	38	25	96	2018-04-14 20:56:18.952357	2018-04-14 20:56:18.952357
3268	38	26	96	2018-04-14 20:56:18.964019	2018-04-14 20:56:18.964019
3269	38	27	96	2018-04-14 20:56:18.976753	2018-04-14 20:56:18.976753
3270	38	27	103	2018-04-14 20:56:18.988618	2018-04-14 20:56:18.988618
3271	38	30	96	2018-04-14 20:56:18.994397	2018-04-14 20:56:18.994397
3272	38	31	96	2018-04-14 20:56:19.003524	2018-04-14 20:56:19.003524
3273	38	32	96	2018-04-14 20:56:19.011247	2018-04-14 20:56:19.011247
3274	38	33	96	2018-04-14 20:56:19.017321	2018-04-14 20:56:19.017321
3275	38	34	96	2018-04-14 20:56:19.029833	2018-04-14 20:56:19.029833
3276	38	36	96	2018-04-14 20:56:19.0436	2018-04-14 20:56:19.0436
3277	38	39	96	2018-04-14 20:56:19.04961	2018-04-14 20:56:19.04961
3278	38	40	96	2018-04-14 20:56:19.054889	2018-04-14 20:56:19.054889
3279	38	41	96	2018-04-14 20:56:19.062878	2018-04-14 20:56:19.062878
3280	38	42	96	2018-04-14 20:56:19.069134	2018-04-14 20:56:19.069134
3281	38	43	43	2018-04-14 20:56:19.074983	2018-04-14 20:56:19.074983
3282	38	43	96	2018-04-14 20:56:19.080524	2018-04-14 20:56:19.080524
3283	38	43	103	2018-04-14 20:56:19.085997	2018-04-14 20:56:19.085997
3284	38	43	112	2018-04-14 20:56:19.092037	2018-04-14 20:56:19.092037
3285	38	44	96	2018-04-14 20:56:19.097821	2018-04-14 20:56:19.097821
3286	38	47	96	2018-04-14 20:56:19.103297	2018-04-14 20:56:19.103297
3287	38	48	96	2018-04-14 20:56:19.109253	2018-04-14 20:56:19.109253
3288	38	49	96	2018-04-14 20:56:19.115084	2018-04-14 20:56:19.115084
3289	38	50	96	2018-04-14 20:56:19.120463	2018-04-14 20:56:19.120463
3290	38	52	96	2018-04-14 20:56:19.126711	2018-04-14 20:56:19.126711
3291	38	59	96	2018-04-14 20:56:19.131968	2018-04-14 20:56:19.131968
3292	38	61	96	2018-04-14 20:56:19.13731	2018-04-14 20:56:19.13731
3293	38	67	96	2018-04-14 20:56:19.143253	2018-04-14 20:56:19.143253
3294	38	67	103	2018-04-14 20:56:19.148938	2018-04-14 20:56:19.148938
3295	38	72	96	2018-04-14 20:56:19.154148	2018-04-14 20:56:19.154148
3296	38	73	97	2018-04-14 20:56:19.160386	2018-04-14 20:56:19.160386
3297	38	74	96	2018-04-14 20:56:19.165596	2018-04-14 20:56:19.165596
3298	38	75	96	2018-04-14 20:56:19.17082	2018-04-14 20:56:19.17082
3299	38	75	103	2018-04-14 20:56:19.176981	2018-04-14 20:56:19.176981
3300	38	76	96	2018-04-14 20:56:19.182744	2018-04-14 20:56:19.182744
3301	38	78	96	2018-04-14 20:56:19.187883	2018-04-14 20:56:19.187883
3302	38	79	96	2018-04-14 20:56:19.193969	2018-04-14 20:56:19.193969
3303	38	82	96	2018-04-14 20:56:19.199328	2018-04-14 20:56:19.199328
3304	38	83	96	2018-04-14 20:56:19.204527	2018-04-14 20:56:19.204527
3305	38	85	96	2018-04-14 20:56:19.211192	2018-04-14 20:56:19.211192
3306	38	86	96	2018-04-14 20:56:19.217261	2018-04-14 20:56:19.217261
3307	38	88	96	2018-04-14 20:56:19.22238	2018-04-14 20:56:19.22238
3308	38	89	96	2018-04-14 20:56:19.229391	2018-04-14 20:56:19.229391
3309	38	90	96	2018-04-14 20:56:19.235017	2018-04-14 20:56:19.235017
3310	38	91	96	2018-04-14 20:56:19.240664	2018-04-14 20:56:19.240664
3311	38	93	96	2018-04-14 20:56:19.246929	2018-04-14 20:56:19.246929
3312	38	95	96	2018-04-14 20:56:19.252103	2018-04-14 20:56:19.252103
3313	38	96	2	2018-04-14 20:56:19.257361	2018-04-14 20:56:19.257361
3314	38	96	3	2018-04-14 20:56:19.263507	2018-04-14 20:56:19.263507
3315	38	96	4	2018-04-14 20:56:19.268902	2018-04-14 20:56:19.268902
3316	38	96	12	2018-04-14 20:56:19.274235	2018-04-14 20:56:19.274235
3317	38	96	18	2018-04-14 20:56:19.280471	2018-04-14 20:56:19.280471
3318	38	96	20	2018-04-14 20:56:19.285774	2018-04-14 20:56:19.285774
3319	38	96	29	2018-04-14 20:56:19.29111	2018-04-14 20:56:19.29111
3320	38	96	37	2018-04-14 20:56:19.297301	2018-04-14 20:56:19.297301
3321	38	96	45	2018-04-14 20:56:19.30266	2018-04-14 20:56:19.30266
3322	38	96	46	2018-04-14 20:56:19.308236	2018-04-14 20:56:19.308236
3323	38	96	53	2018-04-14 20:56:19.314417	2018-04-14 20:56:19.314417
3324	38	96	54	2018-04-14 20:56:19.32	2018-04-14 20:56:19.32
3325	38	96	55	2018-04-14 20:56:19.325346	2018-04-14 20:56:19.325346
3326	38	96	56	2018-04-14 20:56:19.331392	2018-04-14 20:56:19.331392
3327	38	96	57	2018-04-14 20:56:19.336948	2018-04-14 20:56:19.336948
3328	38	96	58	2018-04-14 20:56:19.342246	2018-04-14 20:56:19.342246
3329	38	96	60	2018-04-14 20:56:19.34827	2018-04-14 20:56:19.34827
3330	38	96	64	2018-04-14 20:56:19.353435	2018-04-14 20:56:19.353435
3331	38	96	65	2018-04-14 20:56:19.359258	2018-04-14 20:56:19.359258
3332	38	96	69	2018-04-14 20:56:19.364801	2018-04-14 20:56:19.364801
3333	38	96	70	2018-04-14 20:56:19.370606	2018-04-14 20:56:19.370606
3334	38	96	77	2018-04-14 20:56:19.376691	2018-04-14 20:56:19.376691
3335	38	96	80	2018-04-14 20:56:19.382134	2018-04-14 20:56:19.382134
3336	38	96	81	2018-04-14 20:56:19.38744	2018-04-14 20:56:19.38744
3337	38	96	84	2018-04-14 20:56:19.39359	2018-04-14 20:56:19.39359
3338	38	96	87	2018-04-14 20:56:19.399459	2018-04-14 20:56:19.399459
3339	38	96	92	2018-04-14 20:56:19.404693	2018-04-14 20:56:19.404693
3340	38	96	94	2018-04-14 20:56:19.411213	2018-04-14 20:56:19.411213
3341	38	96	96	2018-04-14 20:56:19.416499	2018-04-14 20:56:19.416499
3342	38	96	99	2018-04-14 20:56:19.421913	2018-04-14 20:56:19.421913
3343	38	96	101	2018-04-14 20:56:19.428058	2018-04-14 20:56:19.428058
3344	38	96	103	2018-04-14 20:56:19.435648	2018-04-14 20:56:19.435648
3345	38	96	107	2018-04-14 20:56:19.440771	2018-04-14 20:56:19.440771
3346	38	96	109	2018-04-14 20:56:19.448016	2018-04-14 20:56:19.448016
3347	38	98	96	2018-04-14 20:56:19.453361	2018-04-14 20:56:19.453361
3348	38	100	96	2018-04-14 20:56:19.463053	2018-04-14 20:56:19.463053
3349	38	102	96	2018-04-14 20:56:19.46898	2018-04-14 20:56:19.46898
3350	38	108	96	2018-04-14 20:56:19.474173	2018-04-14 20:56:19.474173
3351	38	112	96	2018-04-14 20:56:19.481656	2018-04-14 20:56:19.481656
3352	38	112	103	2018-04-14 20:56:19.487896	2018-04-14 20:56:19.487896
3353	38	112	112	2018-04-14 20:56:19.494445	2018-04-14 20:56:19.494445
3354	39	1	11	2018-04-14 20:56:19.500625	2018-04-14 20:56:19.500625
3355	39	1	42	2018-04-14 20:56:19.506139	2018-04-14 20:56:19.506139
3356	39	1	83	2018-04-14 20:56:19.514723	2018-04-14 20:56:19.514723
3357	39	1	100	2018-04-14 20:56:19.52047	2018-04-14 20:56:19.52047
3358	39	1	102	2018-04-14 20:56:19.526698	2018-04-14 20:56:19.526698
3359	39	5	2	2018-04-14 20:56:19.533606	2018-04-14 20:56:19.533606
3360	39	5	4	2018-04-14 20:56:19.53943	2018-04-14 20:56:19.53943
3361	39	5	11	2018-04-14 20:56:19.548152	2018-04-14 20:56:19.548152
3362	39	5	41	2018-04-14 20:56:19.554054	2018-04-14 20:56:19.554054
3363	39	5	42	2018-04-14 20:56:19.560814	2018-04-14 20:56:19.560814
3364	39	5	60	2018-04-14 20:56:19.567466	2018-04-14 20:56:19.567466
3365	39	5	61	2018-04-14 20:56:19.572944	2018-04-14 20:56:19.572944
3366	39	5	64	2018-04-14 20:56:19.581017	2018-04-14 20:56:19.581017
3367	39	5	77	2018-04-14 20:56:19.586528	2018-04-14 20:56:19.586528
3368	39	5	78	2018-04-14 20:56:19.592056	2018-04-14 20:56:19.592056
3369	39	5	80	2018-04-14 20:56:19.600215	2018-04-14 20:56:19.600215
3370	39	5	83	2018-04-14 20:56:19.605675	2018-04-14 20:56:19.605675
3371	39	5	91	2018-04-14 20:56:19.614575	2018-04-14 20:56:19.614575
3372	39	5	95	2018-04-14 20:56:19.620818	2018-04-14 20:56:19.620818
3373	39	5	100	2018-04-14 20:56:19.62851	2018-04-14 20:56:19.62851
3374	39	5	102	2018-04-14 20:56:19.63516	2018-04-14 20:56:19.63516
3375	39	8	11	2018-04-14 20:56:19.640772	2018-04-14 20:56:19.640772
3376	39	8	15	2018-04-14 20:56:19.648797	2018-04-14 20:56:19.648797
3377	39	8	16	2018-04-14 20:56:19.654454	2018-04-14 20:56:19.654454
3378	39	8	17	2018-04-14 20:56:19.660903	2018-04-14 20:56:19.660903
3379	39	8	24	2018-04-14 20:56:19.66761	2018-04-14 20:56:19.66761
3380	39	8	31	2018-04-14 20:56:19.673064	2018-04-14 20:56:19.673064
3381	39	8	32	2018-04-14 20:56:19.681778	2018-04-14 20:56:19.681778
3382	39	8	39	2018-04-14 20:56:19.687744	2018-04-14 20:56:19.687744
3383	39	8	41	2018-04-14 20:56:19.694085	2018-04-14 20:56:19.694085
3384	39	8	42	2018-04-14 20:56:19.701185	2018-04-14 20:56:19.701185
3385	39	8	46	2018-04-14 20:56:19.706697	2018-04-14 20:56:19.706697
3386	39	8	61	2018-04-14 20:56:19.71486	2018-04-14 20:56:19.71486
3387	39	8	64	2018-04-14 20:56:19.720948	2018-04-14 20:56:19.720948
3388	39	8	70	2018-04-14 20:56:19.727072	2018-04-14 20:56:19.727072
3389	39	8	72	2018-04-14 20:56:19.734126	2018-04-14 20:56:19.734126
3390	39	8	74	2018-04-14 20:56:19.741087	2018-04-14 20:56:19.741087
3391	39	8	78	2018-04-14 20:56:19.749803	2018-04-14 20:56:19.749803
3392	39	8	80	2018-04-14 20:56:19.755588	2018-04-14 20:56:19.755588
3393	39	8	82	2018-04-14 20:56:19.763331	2018-04-14 20:56:19.763331
3394	39	8	83	2018-04-14 20:56:19.772108	2018-04-14 20:56:19.772108
3395	39	8	84	2018-04-14 20:56:19.777372	2018-04-14 20:56:19.777372
3396	39	8	85	2018-04-14 20:56:19.784284	2018-04-14 20:56:19.784284
3397	39	8	86	2018-04-14 20:56:19.790093	2018-04-14 20:56:19.790093
3398	39	8	88	2018-04-14 20:56:19.800894	2018-04-14 20:56:19.800894
3399	39	8	89	2018-04-14 20:56:19.807513	2018-04-14 20:56:19.807513
3400	39	8	91	2018-04-14 20:56:19.815149	2018-04-14 20:56:19.815149
3401	39	8	95	2018-04-14 20:56:19.820702	2018-04-14 20:56:19.820702
3402	39	8	100	2018-04-14 20:56:19.825716	2018-04-14 20:56:19.825716
3403	39	8	102	2018-04-14 20:56:19.832185	2018-04-14 20:56:19.832185
3404	39	25	11	2018-04-14 20:56:19.837478	2018-04-14 20:56:19.837478
3405	39	25	15	2018-04-14 20:56:19.842257	2018-04-14 20:56:19.842257
3406	39	25	16	2018-04-14 20:56:19.847115	2018-04-14 20:56:19.847115
3407	39	25	17	2018-04-14 20:56:19.852033	2018-04-14 20:56:19.852033
3408	39	25	24	2018-04-14 20:56:19.856653	2018-04-14 20:56:19.856653
3409	39	25	31	2018-04-14 20:56:19.861741	2018-04-14 20:56:19.861741
3410	39	25	32	2018-04-14 20:56:19.86682	2018-04-14 20:56:19.86682
3411	39	25	39	2018-04-14 20:56:19.872096	2018-04-14 20:56:19.872096
3412	39	25	41	2018-04-14 20:56:19.878613	2018-04-14 20:56:19.878613
3413	39	25	42	2018-04-14 20:56:19.893935	2018-04-14 20:56:19.893935
3414	39	25	46	2018-04-14 20:56:19.899796	2018-04-14 20:56:19.899796
3415	39	25	61	2018-04-14 20:56:19.905529	2018-04-14 20:56:19.905529
3416	39	25	64	2018-04-14 20:56:19.910901	2018-04-14 20:56:19.910901
3417	39	25	70	2018-04-14 20:56:19.918045	2018-04-14 20:56:19.918045
3418	39	25	72	2018-04-14 20:56:19.923358	2018-04-14 20:56:19.923358
3419	39	25	74	2018-04-14 20:56:19.928846	2018-04-14 20:56:19.928846
3420	39	25	78	2018-04-14 20:56:19.935362	2018-04-14 20:56:19.935362
3421	39	25	80	2018-04-14 20:56:19.94095	2018-04-14 20:56:19.94095
3422	39	25	82	2018-04-14 20:56:19.946732	2018-04-14 20:56:19.946732
3423	39	25	83	2018-04-14 20:56:19.952798	2018-04-14 20:56:19.952798
3424	39	25	84	2018-04-14 20:56:19.958555	2018-04-14 20:56:19.958555
3425	39	25	85	2018-04-14 20:56:19.965214	2018-04-14 20:56:19.965214
3426	39	25	86	2018-04-14 20:56:19.971648	2018-04-14 20:56:19.971648
3427	39	25	88	2018-04-14 20:56:19.976755	2018-04-14 20:56:19.976755
3428	39	25	89	2018-04-14 20:56:19.983878	2018-04-14 20:56:19.983878
3429	39	25	91	2018-04-14 20:56:19.989401	2018-04-14 20:56:19.989401
3430	39	25	95	2018-04-14 20:56:19.994171	2018-04-14 20:56:19.994171
3431	39	25	100	2018-04-14 20:56:20.001123	2018-04-14 20:56:20.001123
3432	39	25	102	2018-04-14 20:56:20.006742	2018-04-14 20:56:20.006742
3433	39	27	1	2018-04-14 20:56:20.011681	2018-04-14 20:56:20.011681
3434	39	27	2	2018-04-14 20:56:20.018759	2018-04-14 20:56:20.018759
3435	39	27	4	2018-04-14 20:56:20.024315	2018-04-14 20:56:20.024315
3436	39	27	5	2018-04-14 20:56:20.030332	2018-04-14 20:56:20.030332
3437	39	27	8	2018-04-14 20:56:20.037181	2018-04-14 20:56:20.037181
3438	39	27	25	2018-04-14 20:56:20.042155	2018-04-14 20:56:20.042155
3439	39	27	27	2018-04-14 20:56:20.048608	2018-04-14 20:56:20.048608
3440	39	27	30	2018-04-14 20:56:20.054604	2018-04-14 20:56:20.054604
3441	39	27	36	2018-04-14 20:56:20.059668	2018-04-14 20:56:20.059668
3442	39	27	40	2018-04-14 20:56:20.066394	2018-04-14 20:56:20.066394
3443	39	27	42	2018-04-14 20:56:20.071807	2018-04-14 20:56:20.071807
3444	39	27	44	2018-04-14 20:56:20.077277	2018-04-14 20:56:20.077277
3445	39	27	59	2018-04-14 20:56:20.083919	2018-04-14 20:56:20.083919
3446	39	27	60	2018-04-14 20:56:20.090061	2018-04-14 20:56:20.090061
3447	39	27	64	2018-04-14 20:56:20.095071	2018-04-14 20:56:20.095071
3448	39	27	76	2018-04-14 20:56:20.102274	2018-04-14 20:56:20.102274
3449	39	27	77	2018-04-14 20:56:20.107748	2018-04-14 20:56:20.107748
3450	39	27	80	2018-04-14 20:56:20.113308	2018-04-14 20:56:20.113308
3451	39	27	90	2018-04-14 20:56:20.120033	2018-04-14 20:56:20.120033
3452	39	27	93	2018-04-14 20:56:20.125644	2018-04-14 20:56:20.125644
3453	39	30	11	2018-04-14 20:56:20.131874	2018-04-14 20:56:20.131874
3454	39	30	15	2018-04-14 20:56:20.138335	2018-04-14 20:56:20.138335
3455	39	30	16	2018-04-14 20:56:20.143555	2018-04-14 20:56:20.143555
3456	39	30	17	2018-04-14 20:56:20.153007	2018-04-14 20:56:20.153007
3457	39	30	24	2018-04-14 20:56:20.159479	2018-04-14 20:56:20.159479
3458	39	30	31	2018-04-14 20:56:20.16762	2018-04-14 20:56:20.16762
3459	39	30	32	2018-04-14 20:56:20.173208	2018-04-14 20:56:20.173208
3460	39	30	39	2018-04-14 20:56:20.178161	2018-04-14 20:56:20.178161
3461	39	30	41	2018-04-14 20:56:20.184909	2018-04-14 20:56:20.184909
3462	39	30	42	2018-04-14 20:56:20.190812	2018-04-14 20:56:20.190812
3463	39	30	46	2018-04-14 20:56:20.195692	2018-04-14 20:56:20.195692
3464	39	30	61	2018-04-14 20:56:20.202451	2018-04-14 20:56:20.202451
3465	39	30	64	2018-04-14 20:56:20.20799	2018-04-14 20:56:20.20799
3466	39	30	70	2018-04-14 20:56:20.213203	2018-04-14 20:56:20.213203
3467	39	30	72	2018-04-14 20:56:20.219959	2018-04-14 20:56:20.219959
3468	39	30	74	2018-04-14 20:56:20.225313	2018-04-14 20:56:20.225313
3469	39	30	78	2018-04-14 20:56:20.230785	2018-04-14 20:56:20.230785
3470	39	30	80	2018-04-14 20:56:20.236937	2018-04-14 20:56:20.236937
3471	39	30	82	2018-04-14 20:56:20.241941	2018-04-14 20:56:20.241941
3472	39	30	83	2018-04-14 20:56:20.250316	2018-04-14 20:56:20.250316
3473	39	30	84	2018-04-14 20:56:20.256954	2018-04-14 20:56:20.256954
3474	39	30	85	2018-04-14 20:56:20.261987	2018-04-14 20:56:20.261987
3475	39	30	86	2018-04-14 20:56:20.266619	2018-04-14 20:56:20.266619
3476	39	30	88	2018-04-14 20:56:20.271253	2018-04-14 20:56:20.271253
3477	39	30	89	2018-04-14 20:56:20.276945	2018-04-14 20:56:20.276945
3478	39	30	91	2018-04-14 20:56:20.282133	2018-04-14 20:56:20.282133
3479	39	30	95	2018-04-14 20:56:20.287864	2018-04-14 20:56:20.287864
3480	39	30	100	2018-04-14 20:56:20.292772	2018-04-14 20:56:20.292772
3481	39	30	102	2018-04-14 20:56:20.297534	2018-04-14 20:56:20.297534
3482	39	41	2	2018-04-14 20:56:20.302408	2018-04-14 20:56:20.302408
3483	39	41	4	2018-04-14 20:56:20.310352	2018-04-14 20:56:20.310352
3484	39	41	60	2018-04-14 20:56:20.317932	2018-04-14 20:56:20.317932
3485	39	41	77	2018-04-14 20:56:20.323683	2018-04-14 20:56:20.323683
3486	39	42	2	2018-04-14 20:56:20.328089	2018-04-14 20:56:20.328089
3487	39	42	4	2018-04-14 20:56:20.332909	2018-04-14 20:56:20.332909
3488	39	42	60	2018-04-14 20:56:20.337702	2018-04-14 20:56:20.337702
3489	39	42	64	2018-04-14 20:56:20.345463	2018-04-14 20:56:20.345463
3490	39	42	77	2018-04-14 20:56:20.350633	2018-04-14 20:56:20.350633
3491	39	42	80	2018-04-14 20:56:20.358379	2018-04-14 20:56:20.358379
3492	39	43	11	2018-04-14 20:56:20.365372	2018-04-14 20:56:20.365372
3493	39	43	15	2018-04-14 20:56:20.370211	2018-04-14 20:56:20.370211
3494	39	43	16	2018-04-14 20:56:20.374757	2018-04-14 20:56:20.374757
3495	39	43	17	2018-04-14 20:56:20.379412	2018-04-14 20:56:20.379412
3496	39	43	24	2018-04-14 20:56:20.383882	2018-04-14 20:56:20.383882
3497	39	43	27	2018-04-14 20:56:20.388227	2018-04-14 20:56:20.388227
3498	39	43	31	2018-04-14 20:56:20.392709	2018-04-14 20:56:20.392709
3499	39	43	32	2018-04-14 20:56:20.397295	2018-04-14 20:56:20.397295
3500	39	43	39	2018-04-14 20:56:20.401747	2018-04-14 20:56:20.401747
3501	39	43	41	2018-04-14 20:56:20.406324	2018-04-14 20:56:20.406324
3502	39	43	42	2018-04-14 20:56:20.410864	2018-04-14 20:56:20.410864
3503	39	43	46	2018-04-14 20:56:20.41576	2018-04-14 20:56:20.41576
3504	39	43	48	2018-04-14 20:56:20.420278	2018-04-14 20:56:20.420278
3505	39	43	52	2018-04-14 20:56:20.424878	2018-04-14 20:56:20.424878
3506	39	43	61	2018-04-14 20:56:20.429991	2018-04-14 20:56:20.429991
3507	39	43	70	2018-04-14 20:56:20.43456	2018-04-14 20:56:20.43456
3508	39	43	72	2018-04-14 20:56:20.439119	2018-04-14 20:56:20.439119
3509	39	43	74	2018-04-14 20:56:20.443687	2018-04-14 20:56:20.443687
3510	39	43	78	2018-04-14 20:56:20.448275	2018-04-14 20:56:20.448275
3511	39	43	82	2018-04-14 20:56:20.453016	2018-04-14 20:56:20.453016
3512	39	43	83	2018-04-14 20:56:20.457497	2018-04-14 20:56:20.457497
3513	39	43	84	2018-04-14 20:56:20.461949	2018-04-14 20:56:20.461949
3514	39	43	85	2018-04-14 20:56:20.467939	2018-04-14 20:56:20.467939
3515	39	43	86	2018-04-14 20:56:20.472607	2018-04-14 20:56:20.472607
3516	39	43	88	2018-04-14 20:56:20.477599	2018-04-14 20:56:20.477599
3517	39	43	89	2018-04-14 20:56:20.482422	2018-04-14 20:56:20.482422
3518	39	43	91	2018-04-14 20:56:20.486927	2018-04-14 20:56:20.486927
3519	39	43	95	2018-04-14 20:56:20.4913	2018-04-14 20:56:20.4913
3520	39	43	98	2018-04-14 20:56:20.49567	2018-04-14 20:56:20.49567
3521	39	43	102	2018-04-14 20:56:20.500358	2018-04-14 20:56:20.500358
3522	39	44	2	2018-04-14 20:56:20.504973	2018-04-14 20:56:20.504973
3523	39	44	4	2018-04-14 20:56:20.510341	2018-04-14 20:56:20.510341
3524	39	44	11	2018-04-14 20:56:20.514648	2018-04-14 20:56:20.514648
3525	39	44	15	2018-04-14 20:56:20.519307	2018-04-14 20:56:20.519307
3526	39	44	16	2018-04-14 20:56:20.523594	2018-04-14 20:56:20.523594
3527	39	44	17	2018-04-14 20:56:20.528011	2018-04-14 20:56:20.528011
3528	39	44	24	2018-04-14 20:56:20.532554	2018-04-14 20:56:20.532554
3529	39	44	31	2018-04-14 20:56:20.536914	2018-04-14 20:56:20.536914
3530	39	44	32	2018-04-14 20:56:20.541522	2018-04-14 20:56:20.541522
3531	39	44	39	2018-04-14 20:56:20.545969	2018-04-14 20:56:20.545969
3532	39	44	41	2018-04-14 20:56:20.550309	2018-04-14 20:56:20.550309
3533	39	44	42	2018-04-14 20:56:20.554796	2018-04-14 20:56:20.554796
3534	39	44	46	2018-04-14 20:56:20.55962	2018-04-14 20:56:20.55962
3535	39	44	60	2018-04-14 20:56:20.564275	2018-04-14 20:56:20.564275
3536	39	44	61	2018-04-14 20:56:20.56891	2018-04-14 20:56:20.56891
3537	39	44	64	2018-04-14 20:56:20.573337	2018-04-14 20:56:20.573337
3538	39	44	70	2018-04-14 20:56:20.577534	2018-04-14 20:56:20.577534
3539	39	44	72	2018-04-14 20:56:20.582442	2018-04-14 20:56:20.582442
3540	39	44	74	2018-04-14 20:56:20.586988	2018-04-14 20:56:20.586988
3541	39	44	77	2018-04-14 20:56:20.591537	2018-04-14 20:56:20.591537
3542	39	44	78	2018-04-14 20:56:20.597423	2018-04-14 20:56:20.597423
3543	39	44	80	2018-04-14 20:56:20.602751	2018-04-14 20:56:20.602751
3544	39	44	82	2018-04-14 20:56:20.607153	2018-04-14 20:56:20.607153
3545	39	44	83	2018-04-14 20:56:20.611622	2018-04-14 20:56:20.611622
3546	39	44	84	2018-04-14 20:56:20.616122	2018-04-14 20:56:20.616122
3547	39	44	85	2018-04-14 20:56:20.620733	2018-04-14 20:56:20.620733
3548	39	44	86	2018-04-14 20:56:20.62524	2018-04-14 20:56:20.62524
3549	39	44	88	2018-04-14 20:56:20.629775	2018-04-14 20:56:20.629775
3550	39	44	89	2018-04-14 20:56:20.634183	2018-04-14 20:56:20.634183
3551	39	44	91	2018-04-14 20:56:20.638819	2018-04-14 20:56:20.638819
3552	39	44	95	2018-04-14 20:56:20.643446	2018-04-14 20:56:20.643446
3553	39	44	100	2018-04-14 20:56:20.648149	2018-04-14 20:56:20.648149
3554	39	44	102	2018-04-14 20:56:20.652919	2018-04-14 20:56:20.652919
3555	39	59	11	2018-04-14 20:56:20.657436	2018-04-14 20:56:20.657436
3556	39	59	42	2018-04-14 20:56:20.6624	2018-04-14 20:56:20.6624
3557	39	59	83	2018-04-14 20:56:20.667588	2018-04-14 20:56:20.667588
3558	39	59	100	2018-04-14 20:56:20.672284	2018-04-14 20:56:20.672284
3559	39	59	102	2018-04-14 20:56:20.67751	2018-04-14 20:56:20.67751
3560	39	61	2	2018-04-14 20:56:20.683487	2018-04-14 20:56:20.683487
3561	39	61	4	2018-04-14 20:56:20.689082	2018-04-14 20:56:20.689082
3562	39	61	60	2018-04-14 20:56:20.694158	2018-04-14 20:56:20.694158
3563	39	61	64	2018-04-14 20:56:20.698903	2018-04-14 20:56:20.698903
3564	39	61	77	2018-04-14 20:56:20.703774	2018-04-14 20:56:20.703774
3565	39	61	80	2018-04-14 20:56:20.708501	2018-04-14 20:56:20.708501
3566	39	66	64	2018-04-14 20:56:20.712997	2018-04-14 20:56:20.712997
3567	39	66	80	2018-04-14 20:56:20.718011	2018-04-14 20:56:20.718011
3568	39	67	11	2018-04-14 20:56:20.722671	2018-04-14 20:56:20.722671
3569	39	67	15	2018-04-14 20:56:20.727244	2018-04-14 20:56:20.727244
3570	39	67	16	2018-04-14 20:56:20.732084	2018-04-14 20:56:20.732084
3571	39	67	17	2018-04-14 20:56:20.736642	2018-04-14 20:56:20.736642
3572	39	67	24	2018-04-14 20:56:20.741513	2018-04-14 20:56:20.741513
3573	39	67	27	2018-04-14 20:56:20.745999	2018-04-14 20:56:20.745999
3574	39	67	36	2018-04-14 20:56:20.750516	2018-04-14 20:56:20.750516
3575	39	67	39	2018-04-14 20:56:20.758608	2018-04-14 20:56:20.758608
3576	39	67	40	2018-04-14 20:56:20.763881	2018-04-14 20:56:20.763881
3577	39	67	41	2018-04-14 20:56:20.77146	2018-04-14 20:56:20.77146
3578	39	67	42	2018-04-14 20:56:20.776991	2018-04-14 20:56:20.776991
3579	39	67	46	2018-04-14 20:56:20.78175	2018-04-14 20:56:20.78175
3580	39	67	48	2018-04-14 20:56:20.788086	2018-04-14 20:56:20.788086
3581	39	67	52	2018-04-14 20:56:20.793704	2018-04-14 20:56:20.793704
3582	39	67	61	2018-04-14 20:56:20.798577	2018-04-14 20:56:20.798577
3583	39	67	67	2018-04-14 20:56:20.803348	2018-04-14 20:56:20.803348
3584	39	67	70	2018-04-14 20:56:20.807741	2018-04-14 20:56:20.807741
3585	39	67	72	2018-04-14 20:56:20.812226	2018-04-14 20:56:20.812226
3586	39	67	74	2018-04-14 20:56:20.81658	2018-04-14 20:56:20.81658
3587	39	67	78	2018-04-14 20:56:20.821317	2018-04-14 20:56:20.821317
3588	39	67	82	2018-04-14 20:56:20.82616	2018-04-14 20:56:20.82616
3589	39	67	83	2018-04-14 20:56:20.830712	2018-04-14 20:56:20.830712
3590	39	67	84	2018-04-14 20:56:20.835177	2018-04-14 20:56:20.835177
3591	39	67	85	2018-04-14 20:56:20.839713	2018-04-14 20:56:20.839713
3592	39	67	86	2018-04-14 20:56:20.844223	2018-04-14 20:56:20.844223
3593	39	67	88	2018-04-14 20:56:20.848901	2018-04-14 20:56:20.848901
3594	39	67	89	2018-04-14 20:56:20.853572	2018-04-14 20:56:20.853572
3595	39	67	91	2018-04-14 20:56:20.858124	2018-04-14 20:56:20.858124
3596	39	67	95	2018-04-14 20:56:20.863011	2018-04-14 20:56:20.863011
3597	39	67	98	2018-04-14 20:56:20.867644	2018-04-14 20:56:20.867644
3598	39	67	102	2018-04-14 20:56:20.87205	2018-04-14 20:56:20.87205
3599	39	73	27	2018-04-14 20:56:20.877318	2018-04-14 20:56:20.877318
3600	39	73	66	2018-04-14 20:56:20.882924	2018-04-14 20:56:20.882924
3601	39	75	11	2018-04-14 20:56:20.888175	2018-04-14 20:56:20.888175
3602	39	75	27	2018-04-14 20:56:20.893034	2018-04-14 20:56:20.893034
3603	39	75	31	2018-04-14 20:56:20.897839	2018-04-14 20:56:20.897839
3604	39	75	32	2018-04-14 20:56:20.902575	2018-04-14 20:56:20.902575
3605	39	75	78	2018-04-14 20:56:20.9074	2018-04-14 20:56:20.9074
3606	39	75	83	2018-04-14 20:56:20.912407	2018-04-14 20:56:20.912407
3607	39	75	91	2018-04-14 20:56:20.917443	2018-04-14 20:56:20.917443
3608	39	76	11	2018-04-14 20:56:20.934384	2018-04-14 20:56:20.934384
3609	39	76	42	2018-04-14 20:56:20.941863	2018-04-14 20:56:20.941863
3610	39	76	83	2018-04-14 20:56:20.947391	2018-04-14 20:56:20.947391
3611	39	76	100	2018-04-14 20:56:20.952018	2018-04-14 20:56:20.952018
3612	39	76	102	2018-04-14 20:56:20.956674	2018-04-14 20:56:20.956674
3613	39	78	2	2018-04-14 20:56:20.961358	2018-04-14 20:56:20.961358
3614	39	78	4	2018-04-14 20:56:20.966379	2018-04-14 20:56:20.966379
3615	39	78	60	2018-04-14 20:56:20.970902	2018-04-14 20:56:20.970902
3616	39	78	64	2018-04-14 20:56:20.977165	2018-04-14 20:56:20.977165
3617	39	78	77	2018-04-14 20:56:20.982335	2018-04-14 20:56:20.982335
3618	39	78	80	2018-04-14 20:56:20.986712	2018-04-14 20:56:20.986712
3619	39	79	2	2018-04-14 20:56:20.991366	2018-04-14 20:56:20.991366
3620	39	79	4	2018-04-14 20:56:20.995989	2018-04-14 20:56:20.995989
3621	39	79	46	2018-04-14 20:56:21.000292	2018-04-14 20:56:21.000292
3622	39	79	60	2018-04-14 20:56:21.004737	2018-04-14 20:56:21.004737
3623	39	79	64	2018-04-14 20:56:21.009219	2018-04-14 20:56:21.009219
3624	39	79	70	2018-04-14 20:56:21.015494	2018-04-14 20:56:21.015494
3625	39	79	77	2018-04-14 20:56:21.021669	2018-04-14 20:56:21.021669
3626	39	79	80	2018-04-14 20:56:21.02802	2018-04-14 20:56:21.02802
3627	39	79	84	2018-04-14 20:56:21.032734	2018-04-14 20:56:21.032734
3628	39	90	11	2018-04-14 20:56:21.037277	2018-04-14 20:56:21.037277
3629	39	90	15	2018-04-14 20:56:21.041561	2018-04-14 20:56:21.041561
3630	39	90	16	2018-04-14 20:56:21.046074	2018-04-14 20:56:21.046074
3631	39	90	17	2018-04-14 20:56:21.050506	2018-04-14 20:56:21.050506
3632	39	90	24	2018-04-14 20:56:21.055957	2018-04-14 20:56:21.055957
3633	39	90	31	2018-04-14 20:56:21.0607	2018-04-14 20:56:21.0607
3634	39	90	32	2018-04-14 20:56:21.065313	2018-04-14 20:56:21.065313
3635	39	90	39	2018-04-14 20:56:21.072736	2018-04-14 20:56:21.072736
3636	39	90	41	2018-04-14 20:56:21.081239	2018-04-14 20:56:21.081239
3637	39	90	42	2018-04-14 20:56:21.088525	2018-04-14 20:56:21.088525
3638	39	90	46	2018-04-14 20:56:21.094402	2018-04-14 20:56:21.094402
3639	39	90	61	2018-04-14 20:56:21.099324	2018-04-14 20:56:21.099324
3640	39	90	64	2018-04-14 20:56:21.103735	2018-04-14 20:56:21.103735
3641	39	90	70	2018-04-14 20:56:21.109579	2018-04-14 20:56:21.109579
3642	39	90	72	2018-04-14 20:56:21.115203	2018-04-14 20:56:21.115203
3643	39	90	74	2018-04-14 20:56:21.119903	2018-04-14 20:56:21.119903
3644	39	90	78	2018-04-14 20:56:21.124488	2018-04-14 20:56:21.124488
3645	39	90	80	2018-04-14 20:56:21.129011	2018-04-14 20:56:21.129011
3646	39	90	82	2018-04-14 20:56:21.133714	2018-04-14 20:56:21.133714
3647	39	90	83	2018-04-14 20:56:21.138116	2018-04-14 20:56:21.138116
3648	39	90	84	2018-04-14 20:56:21.142799	2018-04-14 20:56:21.142799
3649	39	90	85	2018-04-14 20:56:21.14907	2018-04-14 20:56:21.14907
3650	39	90	86	2018-04-14 20:56:21.158606	2018-04-14 20:56:21.158606
3651	39	90	88	2018-04-14 20:56:21.167331	2018-04-14 20:56:21.167331
3652	39	90	89	2018-04-14 20:56:21.174086	2018-04-14 20:56:21.174086
3653	39	90	91	2018-04-14 20:56:21.178561	2018-04-14 20:56:21.178561
3654	39	90	95	2018-04-14 20:56:21.182867	2018-04-14 20:56:21.182867
3655	39	90	100	2018-04-14 20:56:21.188765	2018-04-14 20:56:21.188765
3656	39	90	102	2018-04-14 20:56:21.19325	2018-04-14 20:56:21.19325
3657	39	91	2	2018-04-14 20:56:21.19803	2018-04-14 20:56:21.19803
3658	39	91	4	2018-04-14 20:56:21.202622	2018-04-14 20:56:21.202622
3659	39	91	60	2018-04-14 20:56:21.207331	2018-04-14 20:56:21.207331
3660	39	91	64	2018-04-14 20:56:21.211821	2018-04-14 20:56:21.211821
3661	39	91	77	2018-04-14 20:56:21.216394	2018-04-14 20:56:21.216394
3662	39	91	80	2018-04-14 20:56:21.220768	2018-04-14 20:56:21.220768
3663	39	93	11	2018-04-14 20:56:21.225215	2018-04-14 20:56:21.225215
3664	39	93	42	2018-04-14 20:56:21.229675	2018-04-14 20:56:21.229675
3665	39	93	83	2018-04-14 20:56:21.234055	2018-04-14 20:56:21.234055
3666	39	93	100	2018-04-14 20:56:21.238221	2018-04-14 20:56:21.238221
3667	39	93	102	2018-04-14 20:56:21.24261	2018-04-14 20:56:21.24261
3668	39	95	2	2018-04-14 20:56:21.247207	2018-04-14 20:56:21.247207
3669	39	95	4	2018-04-14 20:56:21.25161	2018-04-14 20:56:21.25161
3670	39	95	60	2018-04-14 20:56:21.255981	2018-04-14 20:56:21.255981
3671	39	95	64	2018-04-14 20:56:21.260587	2018-04-14 20:56:21.260587
3672	39	95	77	2018-04-14 20:56:21.265282	2018-04-14 20:56:21.265282
3673	39	95	80	2018-04-14 20:56:21.269931	2018-04-14 20:56:21.269931
3674	39	102	2	2018-04-14 20:56:21.274352	2018-04-14 20:56:21.274352
3675	39	102	4	2018-04-14 20:56:21.27897	2018-04-14 20:56:21.27897
3676	39	102	60	2018-04-14 20:56:21.283474	2018-04-14 20:56:21.283474
3677	39	102	64	2018-04-14 20:56:21.287764	2018-04-14 20:56:21.287764
3678	39	102	77	2018-04-14 20:56:21.292044	2018-04-14 20:56:21.292044
3679	39	102	80	2018-04-14 20:56:21.296441	2018-04-14 20:56:21.296441
3680	39	112	11	2018-04-14 20:56:21.301084	2018-04-14 20:56:21.301084
3681	39	112	15	2018-04-14 20:56:21.305427	2018-04-14 20:56:21.305427
3682	39	112	16	2018-04-14 20:56:21.30971	2018-04-14 20:56:21.30971
3683	39	112	17	2018-04-14 20:56:21.314224	2018-04-14 20:56:21.314224
3684	39	112	24	2018-04-14 20:56:21.318951	2018-04-14 20:56:21.318951
3685	39	112	27	2018-04-14 20:56:21.323738	2018-04-14 20:56:21.323738
3686	39	112	36	2018-04-14 20:56:21.329607	2018-04-14 20:56:21.329607
3687	39	112	39	2018-04-14 20:56:21.334597	2018-04-14 20:56:21.334597
3688	39	112	40	2018-04-14 20:56:21.339474	2018-04-14 20:56:21.339474
3689	39	112	41	2018-04-14 20:56:21.344168	2018-04-14 20:56:21.344168
3690	39	112	42	2018-04-14 20:56:21.348644	2018-04-14 20:56:21.348644
3691	39	112	46	2018-04-14 20:56:21.353172	2018-04-14 20:56:21.353172
3692	39	112	48	2018-04-14 20:56:21.357498	2018-04-14 20:56:21.357498
3693	39	112	52	2018-04-14 20:56:21.361934	2018-04-14 20:56:21.361934
3694	39	112	61	2018-04-14 20:56:21.366439	2018-04-14 20:56:21.366439
3695	39	112	67	2018-04-14 20:56:21.371676	2018-04-14 20:56:21.371676
3696	39	112	70	2018-04-14 20:56:21.376217	2018-04-14 20:56:21.376217
3697	39	112	72	2018-04-14 20:56:21.380939	2018-04-14 20:56:21.380939
3698	39	112	74	2018-04-14 20:56:21.385609	2018-04-14 20:56:21.385609
3699	39	112	78	2018-04-14 20:56:21.390468	2018-04-14 20:56:21.390468
3700	39	112	82	2018-04-14 20:56:21.39486	2018-04-14 20:56:21.39486
3701	39	112	83	2018-04-14 20:56:21.399607	2018-04-14 20:56:21.399607
3702	39	112	84	2018-04-14 20:56:21.405748	2018-04-14 20:56:21.405748
3703	39	112	85	2018-04-14 20:56:21.410632	2018-04-14 20:56:21.410632
3704	39	112	86	2018-04-14 20:56:21.415544	2018-04-14 20:56:21.415544
3705	39	112	88	2018-04-14 20:56:21.42028	2018-04-14 20:56:21.42028
3706	39	112	89	2018-04-14 20:56:21.424848	2018-04-14 20:56:21.424848
3707	39	112	91	2018-04-14 20:56:21.42944	2018-04-14 20:56:21.42944
3708	39	112	95	2018-04-14 20:56:21.43364	2018-04-14 20:56:21.43364
3709	39	112	98	2018-04-14 20:56:21.438439	2018-04-14 20:56:21.438439
3710	39	112	102	2018-04-14 20:56:21.442899	2018-04-14 20:56:21.442899
3711	40	1	1	2018-04-14 20:56:21.447598	2018-04-14 20:56:21.447598
3712	40	5	46	2018-04-14 20:56:21.452433	2018-04-14 20:56:21.452433
3713	40	5	48	2018-04-14 20:56:21.457041	2018-04-14 20:56:21.457041
3714	40	5	52	2018-04-14 20:56:21.46148	2018-04-14 20:56:21.46148
3715	40	5	70	2018-04-14 20:56:21.466088	2018-04-14 20:56:21.466088
3716	40	5	84	2018-04-14 20:56:21.470842	2018-04-14 20:56:21.470842
3717	40	5	98	2018-04-14 20:56:21.475814	2018-04-14 20:56:21.475814
3718	40	7	48	2018-04-14 20:56:21.481119	2018-04-14 20:56:21.481119
3719	40	7	52	2018-04-14 20:56:21.485885	2018-04-14 20:56:21.485885
3720	40	7	98	2018-04-14 20:56:21.490103	2018-04-14 20:56:21.490103
3721	40	14	48	2018-04-14 20:56:21.494685	2018-04-14 20:56:21.494685
3722	40	14	52	2018-04-14 20:56:21.499373	2018-04-14 20:56:21.499373
3723	40	14	98	2018-04-14 20:56:21.504006	2018-04-14 20:56:21.504006
3724	40	26	48	2018-04-14 20:56:21.509657	2018-04-14 20:56:21.509657
3725	40	26	52	2018-04-14 20:56:21.513909	2018-04-14 20:56:21.513909
3726	40	26	98	2018-04-14 20:56:21.518553	2018-04-14 20:56:21.518553
3727	40	34	48	2018-04-14 20:56:21.522925	2018-04-14 20:56:21.522925
3728	40	34	52	2018-04-14 20:56:21.527733	2018-04-14 20:56:21.527733
3729	40	34	98	2018-04-14 20:56:21.532384	2018-04-14 20:56:21.532384
3730	40	42	46	2018-04-14 20:56:21.537272	2018-04-14 20:56:21.537272
3731	40	42	48	2018-04-14 20:56:21.54195	2018-04-14 20:56:21.54195
3732	40	42	52	2018-04-14 20:56:21.549357	2018-04-14 20:56:21.549357
3733	40	42	70	2018-04-14 20:56:21.55748	2018-04-14 20:56:21.55748
3734	40	42	84	2018-04-14 20:56:21.56457	2018-04-14 20:56:21.56457
3735	40	42	98	2018-04-14 20:56:21.569781	2018-04-14 20:56:21.569781
3736	40	59	1	2018-04-14 20:56:21.5747	2018-04-14 20:56:21.5747
3737	40	59	59	2018-04-14 20:56:21.580436	2018-04-14 20:56:21.580436
3738	40	59	93	2018-04-14 20:56:21.585339	2018-04-14 20:56:21.585339
3739	40	76	1	2018-04-14 20:56:21.589889	2018-04-14 20:56:21.589889
3740	40	76	59	2018-04-14 20:56:21.594404	2018-04-14 20:56:21.594404
3741	40	76	76	2018-04-14 20:56:21.599095	2018-04-14 20:56:21.599095
3742	40	76	93	2018-04-14 20:56:21.604539	2018-04-14 20:56:21.604539
3743	40	79	48	2018-04-14 20:56:21.610161	2018-04-14 20:56:21.610161
3744	40	79	52	2018-04-14 20:56:21.614895	2018-04-14 20:56:21.614895
3745	40	79	98	2018-04-14 20:56:21.619524	2018-04-14 20:56:21.619524
3746	40	93	1	2018-04-14 20:56:21.624042	2018-04-14 20:56:21.624042
3747	40	93	93	2018-04-14 20:56:21.628629	2018-04-14 20:56:21.628629
3748	40	102	46	2018-04-14 20:56:21.633333	2018-04-14 20:56:21.633333
3749	40	102	48	2018-04-14 20:56:21.63812	2018-04-14 20:56:21.63812
3750	40	102	52	2018-04-14 20:56:21.642595	2018-04-14 20:56:21.642595
3751	40	102	70	2018-04-14 20:56:21.648022	2018-04-14 20:56:21.648022
3752	40	102	84	2018-04-14 20:56:21.652971	2018-04-14 20:56:21.652971
3753	40	102	98	2018-04-14 20:56:21.657819	2018-04-14 20:56:21.657819
3754	41	1	2	2018-04-14 20:56:21.664168	2018-04-14 20:56:21.664168
3755	41	1	4	2018-04-14 20:56:21.671602	2018-04-14 20:56:21.671602
3756	41	1	48	2018-04-14 20:56:21.683744	2018-04-14 20:56:21.683744
3757	41	1	52	2018-04-14 20:56:21.693118	2018-04-14 20:56:21.693118
3758	41	1	60	2018-04-14 20:56:21.699848	2018-04-14 20:56:21.699848
3759	41	1	77	2018-04-14 20:56:21.706014	2018-04-14 20:56:21.706014
3760	41	1	98	2018-04-14 20:56:21.714576	2018-04-14 20:56:21.714576
3761	41	8	48	2018-04-14 20:56:21.720782	2018-04-14 20:56:21.720782
3762	41	8	52	2018-04-14 20:56:21.729368	2018-04-14 20:56:21.729368
3763	41	8	98	2018-04-14 20:56:21.736867	2018-04-14 20:56:21.736867
3764	41	16	2	2018-04-14 20:56:21.745281	2018-04-14 20:56:21.745281
3765	41	16	4	2018-04-14 20:56:21.750626	2018-04-14 20:56:21.750626
3766	41	16	60	2018-04-14 20:56:21.755722	2018-04-14 20:56:21.755722
3767	41	16	77	2018-04-14 20:56:21.763647	2018-04-14 20:56:21.763647
3768	41	17	2	2018-04-14 20:56:21.768963	2018-04-14 20:56:21.768963
3769	41	17	4	2018-04-14 20:56:21.775435	2018-04-14 20:56:21.775435
3770	41	17	60	2018-04-14 20:56:21.781802	2018-04-14 20:56:21.781802
3771	41	17	77	2018-04-14 20:56:21.7872	2018-04-14 20:56:21.7872
3772	41	25	48	2018-04-14 20:56:21.794531	2018-04-14 20:56:21.794531
3773	41	25	52	2018-04-14 20:56:21.800043	2018-04-14 20:56:21.800043
3774	41	25	98	2018-04-14 20:56:21.805071	2018-04-14 20:56:21.805071
3775	41	30	48	2018-04-14 20:56:21.812191	2018-04-14 20:56:21.812191
3776	41	30	52	2018-04-14 20:56:21.817724	2018-04-14 20:56:21.817724
3777	41	30	98	2018-04-14 20:56:21.82295	2018-04-14 20:56:21.82295
3778	41	40	48	2018-04-14 20:56:21.829582	2018-04-14 20:56:21.829582
3779	41	40	52	2018-04-14 20:56:21.834743	2018-04-14 20:56:21.834743
3780	41	40	98	2018-04-14 20:56:21.839939	2018-04-14 20:56:21.839939
3781	41	44	48	2018-04-14 20:56:21.846505	2018-04-14 20:56:21.846505
3782	41	44	52	2018-04-14 20:56:21.851469	2018-04-14 20:56:21.851469
3783	41	44	98	2018-04-14 20:56:21.859852	2018-04-14 20:56:21.859852
3784	41	48	2	2018-04-14 20:56:21.86684	2018-04-14 20:56:21.86684
3785	41	48	4	2018-04-14 20:56:21.872623	2018-04-14 20:56:21.872623
3786	41	48	60	2018-04-14 20:56:21.879525	2018-04-14 20:56:21.879525
3787	41	48	77	2018-04-14 20:56:21.88484	2018-04-14 20:56:21.88484
3788	41	52	2	2018-04-14 20:56:21.897933	2018-04-14 20:56:21.897933
3789	41	52	4	2018-04-14 20:56:21.904042	2018-04-14 20:56:21.904042
3790	41	52	60	2018-04-14 20:56:21.909702	2018-04-14 20:56:21.909702
3791	41	52	77	2018-04-14 20:56:21.915606	2018-04-14 20:56:21.915606
3792	41	59	2	2018-04-14 20:56:21.92053	2018-04-14 20:56:21.92053
3793	41	59	4	2018-04-14 20:56:21.926894	2018-04-14 20:56:21.926894
3794	41	59	48	2018-04-14 20:56:21.932195	2018-04-14 20:56:21.932195
3795	41	59	52	2018-04-14 20:56:21.937283	2018-04-14 20:56:21.937283
3796	41	59	60	2018-04-14 20:56:21.942932	2018-04-14 20:56:21.942932
3797	41	59	77	2018-04-14 20:56:21.948548	2018-04-14 20:56:21.948548
3798	41	59	98	2018-04-14 20:56:21.956271	2018-04-14 20:56:21.956271
3799	41	76	2	2018-04-14 20:56:21.96086	2018-04-14 20:56:21.96086
3800	41	76	4	2018-04-14 20:56:21.965584	2018-04-14 20:56:21.965584
3801	41	76	48	2018-04-14 20:56:21.970112	2018-04-14 20:56:21.970112
3802	41	76	52	2018-04-14 20:56:21.974407	2018-04-14 20:56:21.974407
3803	41	76	60	2018-04-14 20:56:21.979079	2018-04-14 20:56:21.979079
3804	41	76	77	2018-04-14 20:56:21.983671	2018-04-14 20:56:21.983671
3805	41	76	98	2018-04-14 20:56:21.988283	2018-04-14 20:56:21.988283
3806	41	90	48	2018-04-14 20:56:21.992622	2018-04-14 20:56:21.992622
3807	41	90	52	2018-04-14 20:56:21.997196	2018-04-14 20:56:21.997196
3808	41	90	98	2018-04-14 20:56:22.005167	2018-04-14 20:56:22.005167
3809	41	93	2	2018-04-14 20:56:22.011551	2018-04-14 20:56:22.011551
3810	41	93	4	2018-04-14 20:56:22.016249	2018-04-14 20:56:22.016249
3811	41	93	48	2018-04-14 20:56:22.020848	2018-04-14 20:56:22.020848
3812	41	93	52	2018-04-14 20:56:22.025234	2018-04-14 20:56:22.025234
3813	41	93	60	2018-04-14 20:56:22.02973	2018-04-14 20:56:22.02973
3814	41	93	77	2018-04-14 20:56:22.034547	2018-04-14 20:56:22.034547
3815	41	93	98	2018-04-14 20:56:22.039111	2018-04-14 20:56:22.039111
3816	42	3	51	2018-04-14 20:56:22.043947	2018-04-14 20:56:22.043947
3817	42	7	21	2018-04-14 20:56:22.048466	2018-04-14 20:56:22.048466
3818	42	7	51	2018-04-14 20:56:22.053151	2018-04-14 20:56:22.053151
3819	42	9	21	2018-04-14 20:56:22.058617	2018-04-14 20:56:22.058617
3820	42	9	35	2018-04-14 20:56:22.064513	2018-04-14 20:56:22.064513
3821	42	9	51	2018-04-14 20:56:22.069349	2018-04-14 20:56:22.069349
3822	42	10	21	2018-04-14 20:56:22.073986	2018-04-14 20:56:22.073986
3823	42	10	35	2018-04-14 20:56:22.078322	2018-04-14 20:56:22.078322
3824	42	11	21	2018-04-14 20:56:22.085119	2018-04-14 20:56:22.085119
3825	42	11	35	2018-04-14 20:56:22.089995	2018-04-14 20:56:22.089995
3826	42	11	51	2018-04-14 20:56:22.094332	2018-04-14 20:56:22.094332
3827	42	12	51	2018-04-14 20:56:22.099072	2018-04-14 20:56:22.099072
3828	42	13	21	2018-04-14 20:56:22.103682	2018-04-14 20:56:22.103682
3829	42	13	35	2018-04-14 20:56:22.108422	2018-04-14 20:56:22.108422
3830	42	13	51	2018-04-14 20:56:22.112905	2018-04-14 20:56:22.112905
3831	42	14	21	2018-04-14 20:56:22.117536	2018-04-14 20:56:22.117536
3832	42	14	51	2018-04-14 20:56:22.122285	2018-04-14 20:56:22.122285
3833	42	15	21	2018-04-14 20:56:22.128162	2018-04-14 20:56:22.128162
3834	42	15	35	2018-04-14 20:56:22.133053	2018-04-14 20:56:22.133053
3835	42	15	51	2018-04-14 20:56:22.138198	2018-04-14 20:56:22.138198
3836	42	16	51	2018-04-14 20:56:22.143133	2018-04-14 20:56:22.143133
3837	42	17	51	2018-04-14 20:56:22.148307	2018-04-14 20:56:22.148307
3838	42	20	51	2018-04-14 20:56:22.154334	2018-04-14 20:56:22.154334
3839	42	21	2	2018-04-14 20:56:22.159959	2018-04-14 20:56:22.159959
3840	42	21	3	2018-04-14 20:56:22.165389	2018-04-14 20:56:22.165389
3841	42	21	4	2018-04-14 20:56:22.170563	2018-04-14 20:56:22.170563
3842	42	21	6	2018-04-14 20:56:22.175823	2018-04-14 20:56:22.175823
3843	42	21	12	2018-04-14 20:56:22.180833	2018-04-14 20:56:22.180833
3844	42	21	16	2018-04-14 20:56:22.187311	2018-04-14 20:56:22.187311
3845	42	21	17	2018-04-14 20:56:22.192407	2018-04-14 20:56:22.192407
3846	42	21	18	2018-04-14 20:56:22.198164	2018-04-14 20:56:22.198164
3847	42	21	20	2018-04-14 20:56:22.203688	2018-04-14 20:56:22.203688
3848	42	21	21	2018-04-14 20:56:22.208531	2018-04-14 20:56:22.208531
3849	42	21	22	2018-04-14 20:56:22.215134	2018-04-14 20:56:22.215134
3850	42	21	29	2018-04-14 20:56:22.220537	2018-04-14 20:56:22.220537
3851	42	21	35	2018-04-14 20:56:22.225753	2018-04-14 20:56:22.225753
3852	42	21	37	2018-04-14 20:56:22.231877	2018-04-14 20:56:22.231877
3853	42	21	38	2018-04-14 20:56:22.236988	2018-04-14 20:56:22.236988
3854	42	21	41	2018-04-14 20:56:22.242157	2018-04-14 20:56:22.242157
3855	42	21	45	2018-04-14 20:56:22.248975	2018-04-14 20:56:22.248975
3856	42	21	46	2018-04-14 20:56:22.254156	2018-04-14 20:56:22.254156
3857	42	21	48	2018-04-14 20:56:22.259473	2018-04-14 20:56:22.259473
3858	42	21	51	2018-04-14 20:56:22.265858	2018-04-14 20:56:22.265858
3859	42	21	52	2018-04-14 20:56:22.27136	2018-04-14 20:56:22.27136
3860	42	21	53	2018-04-14 20:56:22.277542	2018-04-14 20:56:22.277542
3861	42	21	54	2018-04-14 20:56:22.283815	2018-04-14 20:56:22.283815
3862	42	21	55	2018-04-14 20:56:22.288995	2018-04-14 20:56:22.288995
3863	42	21	56	2018-04-14 20:56:22.294678	2018-04-14 20:56:22.294678
3864	42	21	57	2018-04-14 20:56:22.300279	2018-04-14 20:56:22.300279
3865	42	21	58	2018-04-14 20:56:22.305694	2018-04-14 20:56:22.305694
3866	42	21	60	2018-04-14 20:56:22.311844	2018-04-14 20:56:22.311844
3867	42	21	61	2018-04-14 20:56:22.317986	2018-04-14 20:56:22.317986
3868	42	21	64	2018-04-14 20:56:22.322834	2018-04-14 20:56:22.322834
3869	42	21	65	2018-04-14 20:56:22.328554	2018-04-14 20:56:22.328554
3870	42	21	69	2018-04-14 20:56:22.335203	2018-04-14 20:56:22.335203
3871	42	21	70	2018-04-14 20:56:22.340031	2018-04-14 20:56:22.340031
3872	42	21	71	2018-04-14 20:56:22.347222	2018-04-14 20:56:22.347222
3873	42	21	77	2018-04-14 20:56:22.352655	2018-04-14 20:56:22.352655
3874	42	21	78	2018-04-14 20:56:22.357481	2018-04-14 20:56:22.357481
3875	42	21	80	2018-04-14 20:56:22.363497	2018-04-14 20:56:22.363497
3876	42	21	81	2018-04-14 20:56:22.369459	2018-04-14 20:56:22.369459
3877	42	21	84	2018-04-14 20:56:22.374354	2018-04-14 20:56:22.374354
3878	42	21	87	2018-04-14 20:56:22.381299	2018-04-14 20:56:22.381299
3879	42	21	91	2018-04-14 20:56:22.387076	2018-04-14 20:56:22.387076
3880	42	21	92	2018-04-14 20:56:22.392542	2018-04-14 20:56:22.392542
3881	42	21	94	2018-04-14 20:56:22.399402	2018-04-14 20:56:22.399402
3882	42	21	95	2018-04-14 20:56:22.404507	2018-04-14 20:56:22.404507
3883	42	21	98	2018-04-14 20:56:22.409374	2018-04-14 20:56:22.409374
3884	42	21	99	2018-04-14 20:56:22.41594	2018-04-14 20:56:22.41594
3885	42	21	101	2018-04-14 20:56:22.421144	2018-04-14 20:56:22.421144
3886	42	21	103	2018-04-14 20:56:22.427355	2018-04-14 20:56:22.427355
3887	42	21	106	2018-04-14 20:56:22.434284	2018-04-14 20:56:22.434284
3888	42	21	107	2018-04-14 20:56:22.439526	2018-04-14 20:56:22.439526
3889	42	21	109	2018-04-14 20:56:22.444525	2018-04-14 20:56:22.444525
3890	42	21	110	2018-04-14 20:56:22.44942	2018-04-14 20:56:22.44942
3891	42	21	111	2018-04-14 20:56:22.454081	2018-04-14 20:56:22.454081
3892	42	23	21	2018-04-14 20:56:22.458631	2018-04-14 20:56:22.458631
3893	42	23	35	2018-04-14 20:56:22.463255	2018-04-14 20:56:22.463255
3894	42	23	51	2018-04-14 20:56:22.467921	2018-04-14 20:56:22.467921
3895	42	24	21	2018-04-14 20:56:22.472809	2018-04-14 20:56:22.472809
3896	42	24	51	2018-04-14 20:56:22.477715	2018-04-14 20:56:22.477715
3897	42	26	21	2018-04-14 20:56:22.482809	2018-04-14 20:56:22.482809
3898	42	26	51	2018-04-14 20:56:22.487848	2018-04-14 20:56:22.487848
3899	42	27	21	2018-04-14 20:56:22.4924	2018-04-14 20:56:22.4924
3900	42	27	51	2018-04-14 20:56:22.496883	2018-04-14 20:56:22.496883
3901	42	28	21	2018-04-14 20:56:22.501687	2018-04-14 20:56:22.501687
3902	42	28	35	2018-04-14 20:56:22.506636	2018-04-14 20:56:22.506636
3903	42	28	51	2018-04-14 20:56:22.511253	2018-04-14 20:56:22.511253
3904	42	29	51	2018-04-14 20:56:22.515999	2018-04-14 20:56:22.515999
3905	42	31	21	2018-04-14 20:56:22.520768	2018-04-14 20:56:22.520768
3906	42	31	35	2018-04-14 20:56:22.525567	2018-04-14 20:56:22.525567
3907	42	31	51	2018-04-14 20:56:22.53025	2018-04-14 20:56:22.53025
3908	42	32	21	2018-04-14 20:56:22.534614	2018-04-14 20:56:22.534614
3909	42	32	35	2018-04-14 20:56:22.539507	2018-04-14 20:56:22.539507
3910	42	32	51	2018-04-14 20:56:22.544101	2018-04-14 20:56:22.544101
3911	42	33	21	2018-04-14 20:56:22.548635	2018-04-14 20:56:22.548635
3912	42	33	35	2018-04-14 20:56:22.553199	2018-04-14 20:56:22.553199
3913	42	33	51	2018-04-14 20:56:22.558411	2018-04-14 20:56:22.558411
3914	42	34	21	2018-04-14 20:56:22.563925	2018-04-14 20:56:22.563925
3915	42	34	51	2018-04-14 20:56:22.569012	2018-04-14 20:56:22.569012
3916	42	35	3	2018-04-14 20:56:22.573927	2018-04-14 20:56:22.573927
3917	42	35	6	2018-04-14 20:56:22.578886	2018-04-14 20:56:22.578886
3918	42	35	12	2018-04-14 20:56:22.58392	2018-04-14 20:56:22.58392
3919	42	35	18	2018-04-14 20:56:22.589129	2018-04-14 20:56:22.589129
3920	42	35	20	2018-04-14 20:56:22.59409	2018-04-14 20:56:22.59409
3921	42	35	22	2018-04-14 20:56:22.59884	2018-04-14 20:56:22.59884
3922	42	35	29	2018-04-14 20:56:22.605202	2018-04-14 20:56:22.605202
3923	42	35	35	2018-04-14 20:56:22.610587	2018-04-14 20:56:22.610587
3924	42	35	37	2018-04-14 20:56:22.616409	2018-04-14 20:56:22.616409
3925	42	35	38	2018-04-14 20:56:22.621903	2018-04-14 20:56:22.621903
3926	42	35	45	2018-04-14 20:56:22.627621	2018-04-14 20:56:22.627621
3927	42	35	51	2018-04-14 20:56:22.634109	2018-04-14 20:56:22.634109
3928	42	35	53	2018-04-14 20:56:22.639358	2018-04-14 20:56:22.639358
3929	42	35	54	2018-04-14 20:56:22.645077	2018-04-14 20:56:22.645077
3930	42	35	55	2018-04-14 20:56:22.651638	2018-04-14 20:56:22.651638
3931	42	35	56	2018-04-14 20:56:22.656828	2018-04-14 20:56:22.656828
3932	42	35	57	2018-04-14 20:56:22.663157	2018-04-14 20:56:22.663157
3933	42	35	58	2018-04-14 20:56:22.669247	2018-04-14 20:56:22.669247
3934	42	35	65	2018-04-14 20:56:22.675056	2018-04-14 20:56:22.675056
3935	42	35	69	2018-04-14 20:56:22.681605	2018-04-14 20:56:22.681605
3936	42	35	71	2018-04-14 20:56:22.687755	2018-04-14 20:56:22.687755
3937	42	35	81	2018-04-14 20:56:22.692998	2018-04-14 20:56:22.692998
3938	42	35	87	2018-04-14 20:56:22.700543	2018-04-14 20:56:22.700543
3939	42	35	92	2018-04-14 20:56:22.706121	2018-04-14 20:56:22.706121
3940	42	35	94	2018-04-14 20:56:22.71183	2018-04-14 20:56:22.71183
3941	42	35	99	2018-04-14 20:56:22.718512	2018-04-14 20:56:22.718512
3942	42	35	101	2018-04-14 20:56:22.72405	2018-04-14 20:56:22.72405
3943	42	35	106	2018-04-14 20:56:22.730041	2018-04-14 20:56:22.730041
3944	42	35	107	2018-04-14 20:56:22.736383	2018-04-14 20:56:22.736383
3945	42	35	109	2018-04-14 20:56:22.741523	2018-04-14 20:56:22.741523
3946	42	35	110	2018-04-14 20:56:22.747775	2018-04-14 20:56:22.747775
3947	42	35	111	2018-04-14 20:56:22.754343	2018-04-14 20:56:22.754343
3948	42	36	21	2018-04-14 20:56:22.759488	2018-04-14 20:56:22.759488
3949	42	36	51	2018-04-14 20:56:22.766755	2018-04-14 20:56:22.766755
3950	42	39	21	2018-04-14 20:56:22.772242	2018-04-14 20:56:22.772242
3951	42	39	51	2018-04-14 20:56:22.777393	2018-04-14 20:56:22.777393
3952	42	40	51	2018-04-14 20:56:22.784155	2018-04-14 20:56:22.784155
3953	42	41	35	2018-04-14 20:56:22.790259	2018-04-14 20:56:22.790259
3954	42	41	51	2018-04-14 20:56:22.80564	2018-04-14 20:56:22.80564
3955	42	45	51	2018-04-14 20:56:22.811971	2018-04-14 20:56:22.811971
3956	42	47	21	2018-04-14 20:56:22.817376	2018-04-14 20:56:22.817376
3957	42	47	35	2018-04-14 20:56:22.824272	2018-04-14 20:56:22.824272
3958	42	47	51	2018-04-14 20:56:22.830115	2018-04-14 20:56:22.830115
3959	42	48	51	2018-04-14 20:56:22.842426	2018-04-14 20:56:22.842426
3960	42	49	21	2018-04-14 20:56:22.853816	2018-04-14 20:56:22.853816
3961	42	49	35	2018-04-14 20:56:22.864978	2018-04-14 20:56:22.864978
3962	42	49	51	2018-04-14 20:56:22.871519	2018-04-14 20:56:22.871519
3963	42	50	21	2018-04-14 20:56:22.876953	2018-04-14 20:56:22.876953
3964	42	50	35	2018-04-14 20:56:22.884061	2018-04-14 20:56:22.884061
3965	42	50	51	2018-04-14 20:56:22.88977	2018-04-14 20:56:22.88977
3966	42	51	6	2018-04-14 20:56:22.894888	2018-04-14 20:56:22.894888
3967	42	51	18	2018-04-14 20:56:22.902066	2018-04-14 20:56:22.902066
3968	42	51	22	2018-04-14 20:56:22.907589	2018-04-14 20:56:22.907589
3969	42	51	37	2018-04-14 20:56:22.912593	2018-04-14 20:56:22.912593
3970	42	51	38	2018-04-14 20:56:22.919918	2018-04-14 20:56:22.919918
3971	42	51	46	2018-04-14 20:56:22.925621	2018-04-14 20:56:22.925621
3972	42	51	51	2018-04-14 20:56:22.931701	2018-04-14 20:56:22.931701
3973	42	51	64	2018-04-14 20:56:22.938617	2018-04-14 20:56:22.938617
3974	42	51	70	2018-04-14 20:56:22.944013	2018-04-14 20:56:22.944013
3975	42	51	71	2018-04-14 20:56:22.951049	2018-04-14 20:56:22.951049
3976	42	51	80	2018-04-14 20:56:22.956842	2018-04-14 20:56:22.956842
3977	42	51	81	2018-04-14 20:56:22.961789	2018-04-14 20:56:22.961789
3978	42	51	84	2018-04-14 20:56:22.968792	2018-04-14 20:56:22.968792
3979	42	51	101	2018-04-14 20:56:22.974326	2018-04-14 20:56:22.974326
3980	42	51	106	2018-04-14 20:56:22.980259	2018-04-14 20:56:22.980259
3981	42	51	107	2018-04-14 20:56:22.986653	2018-04-14 20:56:22.986653
3982	42	51	110	2018-04-14 20:56:22.991917	2018-04-14 20:56:22.991917
3983	42	51	111	2018-04-14 20:56:22.998366	2018-04-14 20:56:22.998366
3984	42	52	51	2018-04-14 20:56:23.004654	2018-04-14 20:56:23.004654
3985	42	53	51	2018-04-14 20:56:23.010125	2018-04-14 20:56:23.010125
3986	42	54	51	2018-04-14 20:56:23.016586	2018-04-14 20:56:23.016586
3987	42	55	51	2018-04-14 20:56:23.022531	2018-04-14 20:56:23.022531
3988	42	56	51	2018-04-14 20:56:23.028018	2018-04-14 20:56:23.028018
3989	42	57	51	2018-04-14 20:56:23.035234	2018-04-14 20:56:23.035234
3990	42	58	51	2018-04-14 20:56:23.040631	2018-04-14 20:56:23.040631
3991	42	61	51	2018-04-14 20:56:23.045812	2018-04-14 20:56:23.045812
3992	42	62	35	2018-04-14 20:56:23.05309	2018-04-14 20:56:23.05309
3993	42	62	51	2018-04-14 20:56:23.058702	2018-04-14 20:56:23.058702
3994	42	63	35	2018-04-14 20:56:23.064494	2018-04-14 20:56:23.064494
3995	42	63	51	2018-04-14 20:56:23.071126	2018-04-14 20:56:23.071126
3996	42	65	51	2018-04-14 20:56:23.076717	2018-04-14 20:56:23.076717
3997	42	66	21	2018-04-14 20:56:23.083231	2018-04-14 20:56:23.083231
3998	42	66	35	2018-04-14 20:56:23.089554	2018-04-14 20:56:23.089554
3999	42	66	51	2018-04-14 20:56:23.094884	2018-04-14 20:56:23.094884
4000	42	68	21	2018-04-14 20:56:23.102	2018-04-14 20:56:23.102
4001	42	68	35	2018-04-14 20:56:23.108109	2018-04-14 20:56:23.108109
4002	42	68	51	2018-04-14 20:56:23.112927	2018-04-14 20:56:23.112927
4003	42	69	51	2018-04-14 20:56:23.119857	2018-04-14 20:56:23.119857
4004	42	72	21	2018-04-14 20:56:23.12493	2018-04-14 20:56:23.12493
4005	42	72	51	2018-04-14 20:56:23.129833	2018-04-14 20:56:23.129833
4006	42	74	21	2018-04-14 20:56:23.136373	2018-04-14 20:56:23.136373
4007	42	74	51	2018-04-14 20:56:23.141396	2018-04-14 20:56:23.141396
4008	42	78	51	2018-04-14 20:56:23.146601	2018-04-14 20:56:23.146601
4009	42	82	21	2018-04-14 20:56:23.1531	2018-04-14 20:56:23.1531
4010	42	82	51	2018-04-14 20:56:23.157926	2018-04-14 20:56:23.157926
4011	42	83	21	2018-04-14 20:56:23.162654	2018-04-14 20:56:23.162654
4012	42	83	35	2018-04-14 20:56:23.167541	2018-04-14 20:56:23.167541
4013	42	83	51	2018-04-14 20:56:23.172199	2018-04-14 20:56:23.172199
4014	42	85	21	2018-04-14 20:56:23.176903	2018-04-14 20:56:23.176903
4015	42	85	51	2018-04-14 20:56:23.18157	2018-04-14 20:56:23.18157
4016	42	86	21	2018-04-14 20:56:23.18595	2018-04-14 20:56:23.18595
4017	42	86	35	2018-04-14 20:56:23.190496	2018-04-14 20:56:23.190496
4018	42	86	51	2018-04-14 20:56:23.195123	2018-04-14 20:56:23.195123
4019	42	87	51	2018-04-14 20:56:23.199441	2018-04-14 20:56:23.199441
4020	42	88	21	2018-04-14 20:56:23.204626	2018-04-14 20:56:23.204626
4021	42	88	51	2018-04-14 20:56:23.210185	2018-04-14 20:56:23.210185
4022	42	89	21	2018-04-14 20:56:23.217981	2018-04-14 20:56:23.217981
4023	42	89	51	2018-04-14 20:56:23.22617	2018-04-14 20:56:23.22617
4024	42	91	51	2018-04-14 20:56:23.231549	2018-04-14 20:56:23.231549
4025	42	92	51	2018-04-14 20:56:23.236067	2018-04-14 20:56:23.236067
4026	42	94	51	2018-04-14 20:56:23.240514	2018-04-14 20:56:23.240514
4027	42	95	51	2018-04-14 20:56:23.245059	2018-04-14 20:56:23.245059
4028	42	96	35	2018-04-14 20:56:23.249564	2018-04-14 20:56:23.249564
4029	42	96	51	2018-04-14 20:56:23.25396	2018-04-14 20:56:23.25396
4030	42	97	35	2018-04-14 20:56:23.258682	2018-04-14 20:56:23.258682
4031	42	97	51	2018-04-14 20:56:23.263114	2018-04-14 20:56:23.263114
4032	42	98	51	2018-04-14 20:56:23.267673	2018-04-14 20:56:23.267673
4033	42	99	51	2018-04-14 20:56:23.272032	2018-04-14 20:56:23.272032
4034	42	100	21	2018-04-14 20:56:23.276651	2018-04-14 20:56:23.276651
4035	42	100	35	2018-04-14 20:56:23.281144	2018-04-14 20:56:23.281144
4036	42	100	51	2018-04-14 20:56:23.2858	2018-04-14 20:56:23.2858
4037	42	103	35	2018-04-14 20:56:23.290172	2018-04-14 20:56:23.290172
4038	42	103	51	2018-04-14 20:56:23.294609	2018-04-14 20:56:23.294609
4039	42	104	21	2018-04-14 20:56:23.299268	2018-04-14 20:56:23.299268
4040	42	104	35	2018-04-14 20:56:23.303663	2018-04-14 20:56:23.303663
4041	42	104	51	2018-04-14 20:56:23.308203	2018-04-14 20:56:23.308203
4042	42	105	35	2018-04-14 20:56:23.312515	2018-04-14 20:56:23.312515
4043	42	105	51	2018-04-14 20:56:23.317253	2018-04-14 20:56:23.317253
4044	42	108	21	2018-04-14 20:56:23.321866	2018-04-14 20:56:23.321866
4045	42	108	35	2018-04-14 20:56:23.326089	2018-04-14 20:56:23.326089
4046	42	108	51	2018-04-14 20:56:23.330463	2018-04-14 20:56:23.330463
4047	42	109	51	2018-04-14 20:56:23.334957	2018-04-14 20:56:23.334957
4048	43	1	82	2018-04-14 20:56:23.339475	2018-04-14 20:56:23.339475
4049	43	5	82	2018-04-14 20:56:23.34403	2018-04-14 20:56:23.34403
4050	43	7	82	2018-04-14 20:56:23.348452	2018-04-14 20:56:23.348452
4051	43	9	82	2018-04-14 20:56:23.352704	2018-04-14 20:56:23.352704
4052	43	10	82	2018-04-14 20:56:23.357155	2018-04-14 20:56:23.357155
4053	43	13	82	2018-04-14 20:56:23.361461	2018-04-14 20:56:23.361461
4054	43	14	82	2018-04-14 20:56:23.365902	2018-04-14 20:56:23.365902
4055	43	23	82	2018-04-14 20:56:23.370255	2018-04-14 20:56:23.370255
4056	43	24	82	2018-04-14 20:56:23.374682	2018-04-14 20:56:23.374682
4057	43	26	82	2018-04-14 20:56:23.378979	2018-04-14 20:56:23.378979
4058	43	33	82	2018-04-14 20:56:23.383298	2018-04-14 20:56:23.383298
4059	43	34	82	2018-04-14 20:56:23.387723	2018-04-14 20:56:23.387723
4060	43	36	82	2018-04-14 20:56:23.392074	2018-04-14 20:56:23.392074
4061	43	40	82	2018-04-14 20:56:23.396605	2018-04-14 20:56:23.396605
4062	43	47	82	2018-04-14 20:56:23.40128	2018-04-14 20:56:23.40128
4063	43	49	82	2018-04-14 20:56:23.409282	2018-04-14 20:56:23.409282
4064	43	50	82	2018-04-14 20:56:23.41559	2018-04-14 20:56:23.41559
4065	43	59	82	2018-04-14 20:56:23.420196	2018-04-14 20:56:23.420196
4066	43	76	82	2018-04-14 20:56:23.424687	2018-04-14 20:56:23.424687
4067	43	82	2	2018-04-14 20:56:23.429242	2018-04-14 20:56:23.429242
4068	43	82	3	2018-04-14 20:56:23.433795	2018-04-14 20:56:23.433795
4069	43	82	4	2018-04-14 20:56:23.438271	2018-04-14 20:56:23.438271
4070	43	82	12	2018-04-14 20:56:23.442696	2018-04-14 20:56:23.442696
4071	43	82	15	2018-04-14 20:56:23.447322	2018-04-14 20:56:23.447322
4072	43	82	16	2018-04-14 20:56:23.451904	2018-04-14 20:56:23.451904
4073	43	82	17	2018-04-14 20:56:23.456606	2018-04-14 20:56:23.456606
4074	43	82	18	2018-04-14 20:56:23.460991	2018-04-14 20:56:23.460991
4075	43	82	20	2018-04-14 20:56:23.465636	2018-04-14 20:56:23.465636
4076	43	82	29	2018-04-14 20:56:23.470201	2018-04-14 20:56:23.470201
4077	43	82	31	2018-04-14 20:56:23.474501	2018-04-14 20:56:23.474501
4078	43	82	32	2018-04-14 20:56:23.480163	2018-04-14 20:56:23.480163
4079	43	82	37	2018-04-14 20:56:23.484716	2018-04-14 20:56:23.484716
4080	43	82	39	2018-04-14 20:56:23.4893	2018-04-14 20:56:23.4893
4081	43	82	41	2018-04-14 20:56:23.493867	2018-04-14 20:56:23.493867
4082	43	82	45	2018-04-14 20:56:23.499763	2018-04-14 20:56:23.499763
4083	43	82	46	2018-04-14 20:56:23.504149	2018-04-14 20:56:23.504149
4084	43	82	53	2018-04-14 20:56:23.508723	2018-04-14 20:56:23.508723
4085	43	82	54	2018-04-14 20:56:23.51312	2018-04-14 20:56:23.51312
4086	43	82	55	2018-04-14 20:56:23.518023	2018-04-14 20:56:23.518023
4087	43	82	56	2018-04-14 20:56:23.5225	2018-04-14 20:56:23.5225
4088	43	82	57	2018-04-14 20:56:23.526885	2018-04-14 20:56:23.526885
4089	43	82	58	2018-04-14 20:56:23.531477	2018-04-14 20:56:23.531477
4090	43	82	60	2018-04-14 20:56:23.536234	2018-04-14 20:56:23.536234
4091	43	82	61	2018-04-14 20:56:23.540649	2018-04-14 20:56:23.540649
4092	43	82	64	2018-04-14 20:56:23.545145	2018-04-14 20:56:23.545145
4093	43	82	69	2018-04-14 20:56:23.550118	2018-04-14 20:56:23.550118
4094	43	82	70	2018-04-14 20:56:23.554694	2018-04-14 20:56:23.554694
4095	43	82	72	2018-04-14 20:56:23.559436	2018-04-14 20:56:23.559436
4096	43	82	74	2018-04-14 20:56:23.564113	2018-04-14 20:56:23.564113
4097	43	82	77	2018-04-14 20:56:23.568981	2018-04-14 20:56:23.568981
4098	43	82	78	2018-04-14 20:56:23.573907	2018-04-14 20:56:23.573907
4099	43	82	80	2018-04-14 20:56:23.578426	2018-04-14 20:56:23.578426
4100	43	82	81	2018-04-14 20:56:23.585968	2018-04-14 20:56:23.585968
4101	43	82	82	2018-04-14 20:56:23.592983	2018-04-14 20:56:23.592983
4102	43	82	84	2018-04-14 20:56:23.599536	2018-04-14 20:56:23.599536
4103	43	82	86	2018-04-14 20:56:23.605064	2018-04-14 20:56:23.605064
4104	43	82	87	2018-04-14 20:56:23.610239	2018-04-14 20:56:23.610239
4105	43	82	91	2018-04-14 20:56:23.615588	2018-04-14 20:56:23.615588
4106	43	82	92	2018-04-14 20:56:23.620419	2018-04-14 20:56:23.620419
4107	43	82	94	2018-04-14 20:56:23.625268	2018-04-14 20:56:23.625268
4108	43	82	95	2018-04-14 20:56:23.639635	2018-04-14 20:56:23.639635
4109	43	82	99	2018-04-14 20:56:23.644993	2018-04-14 20:56:23.644993
4110	43	82	101	2018-04-14 20:56:23.65005	2018-04-14 20:56:23.65005
4111	43	82	109	2018-04-14 20:56:23.657672	2018-04-14 20:56:23.657672
4112	43	85	82	2018-04-14 20:56:23.663094	2018-04-14 20:56:23.663094
4113	43	88	82	2018-04-14 20:56:23.668921	2018-04-14 20:56:23.668921
4114	43	89	82	2018-04-14 20:56:23.674481	2018-04-14 20:56:23.674481
4115	43	93	82	2018-04-14 20:56:23.679753	2018-04-14 20:56:23.679753
4116	43	108	82	2018-04-14 20:56:23.685815	2018-04-14 20:56:23.685815
4117	44	73	7	2018-04-14 20:56:23.69151	2018-04-14 20:56:23.69151
4118	44	73	9	2018-04-14 20:56:23.696864	2018-04-14 20:56:23.696864
4119	44	73	13	2018-04-14 20:56:23.702696	2018-04-14 20:56:23.702696
4120	44	73	14	2018-04-14 20:56:23.708082	2018-04-14 20:56:23.708082
4121	44	73	23	2018-04-14 20:56:23.713408	2018-04-14 20:56:23.713408
4122	44	73	26	2018-04-14 20:56:23.719298	2018-04-14 20:56:23.719298
4123	44	73	33	2018-04-14 20:56:23.724622	2018-04-14 20:56:23.724622
4124	44	73	34	2018-04-14 20:56:23.730252	2018-04-14 20:56:23.730252
4125	44	73	47	2018-04-14 20:56:23.737394	2018-04-14 20:56:23.737394
4126	44	73	49	2018-04-14 20:56:23.742709	2018-04-14 20:56:23.742709
4127	44	73	50	2018-04-14 20:56:23.748015	2018-04-14 20:56:23.748015
4128	44	73	108	2018-04-14 20:56:23.754053	2018-04-14 20:56:23.754053
4129	44	75	7	2018-04-14 20:56:23.759309	2018-04-14 20:56:23.759309
4130	44	75	9	2018-04-14 20:56:23.764568	2018-04-14 20:56:23.764568
4131	44	75	13	2018-04-14 20:56:23.770356	2018-04-14 20:56:23.770356
4132	44	75	14	2018-04-14 20:56:23.776025	2018-04-14 20:56:23.776025
4133	44	75	23	2018-04-14 20:56:23.781424	2018-04-14 20:56:23.781424
4134	44	75	26	2018-04-14 20:56:23.787349	2018-04-14 20:56:23.787349
4135	44	75	33	2018-04-14 20:56:23.793192	2018-04-14 20:56:23.793192
4136	44	75	34	2018-04-14 20:56:23.798279	2018-04-14 20:56:23.798279
4137	44	75	47	2018-04-14 20:56:23.803652	2018-04-14 20:56:23.803652
4138	44	75	49	2018-04-14 20:56:23.808781	2018-04-14 20:56:23.808781
4139	44	75	50	2018-04-14 20:56:23.813243	2018-04-14 20:56:23.813243
4140	44	75	108	2018-04-14 20:56:23.81803	2018-04-14 20:56:23.81803
4141	45	8	1	2018-04-14 20:56:23.822473	2018-04-14 20:56:23.822473
4142	45	8	5	2018-04-14 20:56:23.826965	2018-04-14 20:56:23.826965
4143	45	8	8	2018-04-14 20:56:23.831165	2018-04-14 20:56:23.831165
4144	45	8	25	2018-04-14 20:56:23.835501	2018-04-14 20:56:23.835501
4145	45	8	30	2018-04-14 20:56:23.840013	2018-04-14 20:56:23.840013
4146	45	8	44	2018-04-14 20:56:23.844377	2018-04-14 20:56:23.844377
4147	45	8	59	2018-04-14 20:56:23.848882	2018-04-14 20:56:23.848882
4148	45	8	76	2018-04-14 20:56:23.85322	2018-04-14 20:56:23.85322
4149	45	8	79	2018-04-14 20:56:23.857686	2018-04-14 20:56:23.857686
4150	45	8	90	2018-04-14 20:56:23.861995	2018-04-14 20:56:23.861995
4151	45	8	93	2018-04-14 20:56:23.866411	2018-04-14 20:56:23.866411
4152	45	25	1	2018-04-14 20:56:23.870573	2018-04-14 20:56:23.870573
4153	45	25	5	2018-04-14 20:56:23.874904	2018-04-14 20:56:23.874904
4154	45	25	25	2018-04-14 20:56:23.879432	2018-04-14 20:56:23.879432
4155	45	25	44	2018-04-14 20:56:23.883714	2018-04-14 20:56:23.883714
4156	45	25	59	2018-04-14 20:56:23.888298	2018-04-14 20:56:23.888298
4157	45	25	76	2018-04-14 20:56:23.892771	2018-04-14 20:56:23.892771
4158	45	25	79	2018-04-14 20:56:23.897787	2018-04-14 20:56:23.897787
4159	45	25	93	2018-04-14 20:56:23.902971	2018-04-14 20:56:23.902971
4160	45	30	1	2018-04-14 20:56:23.907438	2018-04-14 20:56:23.907438
4161	45	30	5	2018-04-14 20:56:23.912015	2018-04-14 20:56:23.912015
4162	45	30	25	2018-04-14 20:56:23.916624	2018-04-14 20:56:23.916624
4163	45	30	30	2018-04-14 20:56:23.921185	2018-04-14 20:56:23.921185
4164	45	30	44	2018-04-14 20:56:23.925775	2018-04-14 20:56:23.925775
4165	45	30	59	2018-04-14 20:56:23.930227	2018-04-14 20:56:23.930227
4166	45	30	76	2018-04-14 20:56:23.934743	2018-04-14 20:56:23.934743
4167	45	30	79	2018-04-14 20:56:23.93929	2018-04-14 20:56:23.93929
4168	45	30	93	2018-04-14 20:56:23.94375	2018-04-14 20:56:23.94375
4169	45	43	8	2018-04-14 20:56:23.948371	2018-04-14 20:56:23.948371
4170	45	43	25	2018-04-14 20:56:23.952851	2018-04-14 20:56:23.952851
4171	45	43	30	2018-04-14 20:56:23.957246	2018-04-14 20:56:23.957246
4172	45	43	44	2018-04-14 20:56:23.964162	2018-04-14 20:56:23.964162
4173	45	43	79	2018-04-14 20:56:23.972375	2018-04-14 20:56:23.972375
4174	45	43	90	2018-04-14 20:56:23.978459	2018-04-14 20:56:23.978459
4175	45	44	1	2018-04-14 20:56:23.983271	2018-04-14 20:56:23.983271
4176	45	44	5	2018-04-14 20:56:23.987733	2018-04-14 20:56:23.987733
4177	45	44	44	2018-04-14 20:56:23.992045	2018-04-14 20:56:23.992045
4178	45	44	59	2018-04-14 20:56:23.996545	2018-04-14 20:56:23.996545
4179	45	44	76	2018-04-14 20:56:24.001079	2018-04-14 20:56:24.001079
4180	45	44	79	2018-04-14 20:56:24.005476	2018-04-14 20:56:24.005476
4181	45	44	93	2018-04-14 20:56:24.009811	2018-04-14 20:56:24.009811
4182	45	67	8	2018-04-14 20:56:24.014199	2018-04-14 20:56:24.014199
4183	45	67	25	2018-04-14 20:56:24.018667	2018-04-14 20:56:24.018667
4184	45	67	30	2018-04-14 20:56:24.026348	2018-04-14 20:56:24.026348
4185	45	67	44	2018-04-14 20:56:24.033378	2018-04-14 20:56:24.033378
4186	45	67	79	2018-04-14 20:56:24.03836	2018-04-14 20:56:24.03836
4187	45	67	90	2018-04-14 20:56:24.043282	2018-04-14 20:56:24.043282
4188	45	73	8	2018-04-14 20:56:24.048003	2018-04-14 20:56:24.048003
4189	45	73	25	2018-04-14 20:56:24.052385	2018-04-14 20:56:24.052385
4190	45	73	30	2018-04-14 20:56:24.057312	2018-04-14 20:56:24.057312
4191	45	73	44	2018-04-14 20:56:24.061864	2018-04-14 20:56:24.061864
4192	45	73	79	2018-04-14 20:56:24.066427	2018-04-14 20:56:24.066427
4193	45	73	90	2018-04-14 20:56:24.070956	2018-04-14 20:56:24.070956
4194	45	75	8	2018-04-14 20:56:24.075679	2018-04-14 20:56:24.075679
4195	45	75	15	2018-04-14 20:56:24.080308	2018-04-14 20:56:24.080308
4196	45	75	16	2018-04-14 20:56:24.084583	2018-04-14 20:56:24.084583
4197	45	75	17	2018-04-14 20:56:24.089221	2018-04-14 20:56:24.089221
4198	45	75	24	2018-04-14 20:56:24.093911	2018-04-14 20:56:24.093911
4199	45	75	25	2018-04-14 20:56:24.098443	2018-04-14 20:56:24.098443
4200	45	75	30	2018-04-14 20:56:24.103069	2018-04-14 20:56:24.103069
4201	45	75	39	2018-04-14 20:56:24.107823	2018-04-14 20:56:24.107823
4202	45	75	41	2018-04-14 20:56:24.112529	2018-04-14 20:56:24.112529
4203	45	75	42	2018-04-14 20:56:24.117154	2018-04-14 20:56:24.117154
4204	45	75	44	2018-04-14 20:56:24.121845	2018-04-14 20:56:24.121845
4205	45	75	46	2018-04-14 20:56:24.126655	2018-04-14 20:56:24.126655
4206	45	75	48	2018-04-14 20:56:24.131307	2018-04-14 20:56:24.131307
4207	45	75	52	2018-04-14 20:56:24.135818	2018-04-14 20:56:24.135818
4208	45	75	61	2018-04-14 20:56:24.140477	2018-04-14 20:56:24.140477
4209	45	75	70	2018-04-14 20:56:24.144987	2018-04-14 20:56:24.144987
4210	45	75	72	2018-04-14 20:56:24.151414	2018-04-14 20:56:24.151414
4211	45	75	74	2018-04-14 20:56:24.156879	2018-04-14 20:56:24.156879
4212	45	75	79	2018-04-14 20:56:24.16132	2018-04-14 20:56:24.16132
4213	45	75	82	2018-04-14 20:56:24.166181	2018-04-14 20:56:24.166181
4214	45	75	84	2018-04-14 20:56:24.17092	2018-04-14 20:56:24.17092
4215	45	75	85	2018-04-14 20:56:24.175509	2018-04-14 20:56:24.175509
4216	45	75	86	2018-04-14 20:56:24.180118	2018-04-14 20:56:24.180118
4217	45	75	88	2018-04-14 20:56:24.184893	2018-04-14 20:56:24.184893
4218	45	75	89	2018-04-14 20:56:24.189334	2018-04-14 20:56:24.189334
4219	45	75	90	2018-04-14 20:56:24.193752	2018-04-14 20:56:24.193752
4220	45	75	95	2018-04-14 20:56:24.198495	2018-04-14 20:56:24.198495
4221	45	75	98	2018-04-14 20:56:24.203872	2018-04-14 20:56:24.203872
4222	45	75	102	2018-04-14 20:56:24.208316	2018-04-14 20:56:24.208316
4223	45	79	1	2018-04-14 20:56:24.212723	2018-04-14 20:56:24.212723
4224	45	79	5	2018-04-14 20:56:24.217648	2018-04-14 20:56:24.217648
4225	45	79	59	2018-04-14 20:56:24.222252	2018-04-14 20:56:24.222252
4226	45	79	76	2018-04-14 20:56:24.2269	2018-04-14 20:56:24.2269
4227	45	79	79	2018-04-14 20:56:24.231554	2018-04-14 20:56:24.231554
4228	45	79	93	2018-04-14 20:56:24.236981	2018-04-14 20:56:24.236981
4229	45	90	1	2018-04-14 20:56:24.242702	2018-04-14 20:56:24.242702
4230	45	90	5	2018-04-14 20:56:24.250332	2018-04-14 20:56:24.250332
4231	45	90	25	2018-04-14 20:56:24.257365	2018-04-14 20:56:24.257365
4232	45	90	30	2018-04-14 20:56:24.264942	2018-04-14 20:56:24.264942
4233	45	90	44	2018-04-14 20:56:24.270546	2018-04-14 20:56:24.270546
4234	45	90	59	2018-04-14 20:56:24.275925	2018-04-14 20:56:24.275925
4235	45	90	76	2018-04-14 20:56:24.286997	2018-04-14 20:56:24.286997
4236	45	90	79	2018-04-14 20:56:24.298055	2018-04-14 20:56:24.298055
4237	45	90	90	2018-04-14 20:56:24.303231	2018-04-14 20:56:24.303231
4238	45	90	93	2018-04-14 20:56:24.30847	2018-04-14 20:56:24.30847
4239	45	112	8	2018-04-14 20:56:24.313547	2018-04-14 20:56:24.313547
4240	45	112	25	2018-04-14 20:56:24.318581	2018-04-14 20:56:24.318581
4241	45	112	30	2018-04-14 20:56:24.323505	2018-04-14 20:56:24.323505
4242	45	112	44	2018-04-14 20:56:24.328515	2018-04-14 20:56:24.328515
4243	45	112	79	2018-04-14 20:56:24.333729	2018-04-14 20:56:24.333729
4244	45	112	90	2018-04-14 20:56:24.338743	2018-04-14 20:56:24.338743
4245	46	1	64	2018-04-14 20:56:24.343675	2018-04-14 20:56:24.343675
4246	46	1	80	2018-04-14 20:56:24.348987	2018-04-14 20:56:24.348987
4247	46	2	12	2018-04-14 20:56:24.354184	2018-04-14 20:56:24.354184
4248	46	2	53	2018-04-14 20:56:24.359322	2018-04-14 20:56:24.359322
4249	46	2	64	2018-04-14 20:56:24.364571	2018-04-14 20:56:24.364571
4250	46	2	80	2018-04-14 20:56:24.376935	2018-04-14 20:56:24.376935
4251	46	2	87	2018-04-14 20:56:24.38224	2018-04-14 20:56:24.38224
4252	46	2	92	2018-04-14 20:56:24.387101	2018-04-14 20:56:24.387101
4253	46	2	109	2018-04-14 20:56:24.391853	2018-04-14 20:56:24.391853
4254	46	4	12	2018-04-14 20:56:24.396545	2018-04-14 20:56:24.396545
4255	46	4	53	2018-04-14 20:56:24.401227	2018-04-14 20:56:24.401227
4256	46	4	64	2018-04-14 20:56:24.406016	2018-04-14 20:56:24.406016
4257	46	4	80	2018-04-14 20:56:24.410745	2018-04-14 20:56:24.410745
4258	46	4	87	2018-04-14 20:56:24.415742	2018-04-14 20:56:24.415742
4259	46	4	92	2018-04-14 20:56:24.420609	2018-04-14 20:56:24.420609
4260	46	4	109	2018-04-14 20:56:24.425316	2018-04-14 20:56:24.425316
4261	46	7	23	2018-04-14 20:56:24.430087	2018-04-14 20:56:24.430087
4262	46	7	26	2018-04-14 20:56:24.436165	2018-04-14 20:56:24.436165
4263	46	7	34	2018-04-14 20:56:24.441699	2018-04-14 20:56:24.441699
4264	46	7	49	2018-04-14 20:56:24.446538	2018-04-14 20:56:24.446538
4265	46	7	50	2018-04-14 20:56:24.451212	2018-04-14 20:56:24.451212
4266	46	7	108	2018-04-14 20:56:24.456031	2018-04-14 20:56:24.456031
4267	46	9	23	2018-04-14 20:56:24.460802	2018-04-14 20:56:24.460802
4268	46	9	26	2018-04-14 20:56:24.465765	2018-04-14 20:56:24.465765
4269	46	9	34	2018-04-14 20:56:24.470544	2018-04-14 20:56:24.470544
4270	46	9	50	2018-04-14 20:56:24.47534	2018-04-14 20:56:24.47534
4271	46	10	2	2018-04-14 20:56:24.480334	2018-04-14 20:56:24.480334
4272	46	10	4	2018-04-14 20:56:24.485299	2018-04-14 20:56:24.485299
4273	46	10	60	2018-04-14 20:56:24.490194	2018-04-14 20:56:24.490194
4274	46	10	77	2018-04-14 20:56:24.495298	2018-04-14 20:56:24.495298
4275	46	13	23	2018-04-14 20:56:24.506754	2018-04-14 20:56:24.506754
4276	46	13	26	2018-04-14 20:56:24.521706	2018-04-14 20:56:24.521706
4277	46	13	34	2018-04-14 20:56:24.530541	2018-04-14 20:56:24.530541
4278	46	13	50	2018-04-14 20:56:24.537335	2018-04-14 20:56:24.537335
4279	46	14	23	2018-04-14 20:56:24.542196	2018-04-14 20:56:24.542196
4280	46	14	26	2018-04-14 20:56:24.546922	2018-04-14 20:56:24.546922
4281	46	14	34	2018-04-14 20:56:24.55143	2018-04-14 20:56:24.55143
4282	46	14	49	2018-04-14 20:56:24.559336	2018-04-14 20:56:24.559336
4283	46	14	50	2018-04-14 20:56:24.565501	2018-04-14 20:56:24.565501
4284	46	14	108	2018-04-14 20:56:24.570407	2018-04-14 20:56:24.570407
4285	46	33	26	2018-04-14 20:56:24.574818	2018-04-14 20:56:24.574818
4286	46	33	34	2018-04-14 20:56:24.582572	2018-04-14 20:56:24.582572
4287	46	47	26	2018-04-14 20:56:24.58782	2018-04-14 20:56:24.58782
4288	46	47	34	2018-04-14 20:56:24.592462	2018-04-14 20:56:24.592462
4289	46	59	64	2018-04-14 20:56:24.597128	2018-04-14 20:56:24.597128
4290	46	59	80	2018-04-14 20:56:24.601534	2018-04-14 20:56:24.601534
4291	46	60	12	2018-04-14 20:56:24.605847	2018-04-14 20:56:24.605847
4292	46	60	53	2018-04-14 20:56:24.610086	2018-04-14 20:56:24.610086
4293	46	60	64	2018-04-14 20:56:24.614534	2018-04-14 20:56:24.614534
4294	46	60	80	2018-04-14 20:56:24.619536	2018-04-14 20:56:24.619536
4295	46	60	87	2018-04-14 20:56:24.623994	2018-04-14 20:56:24.623994
4296	46	60	92	2018-04-14 20:56:24.628431	2018-04-14 20:56:24.628431
4297	46	60	109	2018-04-14 20:56:24.632662	2018-04-14 20:56:24.632662
4298	46	76	64	2018-04-14 20:56:24.640126	2018-04-14 20:56:24.640126
4299	46	76	80	2018-04-14 20:56:24.646096	2018-04-14 20:56:24.646096
4300	46	77	12	2018-04-14 20:56:24.65165	2018-04-14 20:56:24.65165
4301	46	77	53	2018-04-14 20:56:24.655834	2018-04-14 20:56:24.655834
4302	46	77	64	2018-04-14 20:56:24.660274	2018-04-14 20:56:24.660274
4303	46	77	80	2018-04-14 20:56:24.664772	2018-04-14 20:56:24.664772
4304	46	77	87	2018-04-14 20:56:24.669228	2018-04-14 20:56:24.669228
4305	46	77	92	2018-04-14 20:56:24.673777	2018-04-14 20:56:24.673777
4306	46	77	109	2018-04-14 20:56:24.67808	2018-04-14 20:56:24.67808
4307	46	93	64	2018-04-14 20:56:24.682526	2018-04-14 20:56:24.682526
4308	46	93	80	2018-04-14 20:56:24.687063	2018-04-14 20:56:24.687063
4309	47	2	18	2018-04-14 20:56:24.691402	2018-04-14 20:56:24.691402
4310	47	2	54	2018-04-14 20:56:24.695937	2018-04-14 20:56:24.695937
4311	47	3	18	2018-04-14 20:56:24.700297	2018-04-14 20:56:24.700297
4312	47	3	64	2018-04-14 20:56:24.704627	2018-04-14 20:56:24.704627
4313	47	3	80	2018-04-14 20:56:24.709261	2018-04-14 20:56:24.709261
4314	47	4	18	2018-04-14 20:56:24.713811	2018-04-14 20:56:24.713811
4315	47	4	54	2018-04-14 20:56:24.718359	2018-04-14 20:56:24.718359
4316	47	10	10	2018-04-14 20:56:24.722664	2018-04-14 20:56:24.722664
4317	47	12	12	2018-04-14 20:56:24.727031	2018-04-14 20:56:24.727031
4318	47	12	18	2018-04-14 20:56:24.731387	2018-04-14 20:56:24.731387
4319	47	12	53	2018-04-14 20:56:24.735755	2018-04-14 20:56:24.735755
4320	47	12	64	2018-04-14 20:56:24.740232	2018-04-14 20:56:24.740232
4321	47	12	80	2018-04-14 20:56:24.744853	2018-04-14 20:56:24.744853
4322	47	18	18	2018-04-14 20:56:24.749498	2018-04-14 20:56:24.749498
4323	47	18	64	2018-04-14 20:56:24.75392	2018-04-14 20:56:24.75392
4324	47	18	80	2018-04-14 20:56:24.758168	2018-04-14 20:56:24.758168
4325	47	20	18	2018-04-14 20:56:24.762431	2018-04-14 20:56:24.762431
4326	47	20	64	2018-04-14 20:56:24.767074	2018-04-14 20:56:24.767074
4327	47	20	80	2018-04-14 20:56:24.771437	2018-04-14 20:56:24.771437
4328	47	29	18	2018-04-14 20:56:24.775939	2018-04-14 20:56:24.775939
4329	47	29	64	2018-04-14 20:56:24.780306	2018-04-14 20:56:24.780306
4330	47	29	80	2018-04-14 20:56:24.784658	2018-04-14 20:56:24.784658
4331	47	33	23	2018-04-14 20:56:24.789009	2018-04-14 20:56:24.789009
4332	47	33	49	2018-04-14 20:56:24.793519	2018-04-14 20:56:24.793519
4333	47	33	50	2018-04-14 20:56:24.79885	2018-04-14 20:56:24.79885
4334	47	33	108	2018-04-14 20:56:24.804558	2018-04-14 20:56:24.804558
4335	47	37	64	2018-04-14 20:56:24.809009	2018-04-14 20:56:24.809009
4336	47	37	80	2018-04-14 20:56:24.813574	2018-04-14 20:56:24.813574
4337	47	45	18	2018-04-14 20:56:24.818029	2018-04-14 20:56:24.818029
4338	47	45	64	2018-04-14 20:56:24.82239	2018-04-14 20:56:24.82239
4339	47	45	80	2018-04-14 20:56:24.826931	2018-04-14 20:56:24.826931
4340	47	53	18	2018-04-14 20:56:24.83146	2018-04-14 20:56:24.83146
4341	47	53	53	2018-04-14 20:56:24.835927	2018-04-14 20:56:24.835927
4342	47	53	64	2018-04-14 20:56:24.840332	2018-04-14 20:56:24.840332
4343	47	53	80	2018-04-14 20:56:24.844848	2018-04-14 20:56:24.844848
4344	47	54	18	2018-04-14 20:56:24.849336	2018-04-14 20:56:24.849336
4345	47	54	64	2018-04-14 20:56:24.853799	2018-04-14 20:56:24.853799
4346	47	54	80	2018-04-14 20:56:24.85837	2018-04-14 20:56:24.85837
4347	47	55	18	2018-04-14 20:56:24.862779	2018-04-14 20:56:24.862779
4348	47	55	64	2018-04-14 20:56:24.867136	2018-04-14 20:56:24.867136
4349	47	55	80	2018-04-14 20:56:24.871581	2018-04-14 20:56:24.871581
4350	47	57	18	2018-04-14 20:56:24.876075	2018-04-14 20:56:24.876075
4351	47	57	64	2018-04-14 20:56:24.880506	2018-04-14 20:56:24.880506
4352	47	57	80	2018-04-14 20:56:24.88498	2018-04-14 20:56:24.88498
4353	47	58	18	2018-04-14 20:56:24.890008	2018-04-14 20:56:24.890008
4354	47	58	64	2018-04-14 20:56:24.89449	2018-04-14 20:56:24.89449
4355	47	58	80	2018-04-14 20:56:24.899098	2018-04-14 20:56:24.899098
4356	47	60	18	2018-04-14 20:56:24.903454	2018-04-14 20:56:24.903454
4357	47	60	54	2018-04-14 20:56:24.908053	2018-04-14 20:56:24.908053
4358	47	64	64	2018-04-14 20:56:24.912511	2018-04-14 20:56:24.912511
4359	47	64	80	2018-04-14 20:56:24.917862	2018-04-14 20:56:24.917862
4360	47	65	18	2018-04-14 20:56:24.922316	2018-04-14 20:56:24.922316
4361	47	65	53	2018-04-14 20:56:24.926904	2018-04-14 20:56:24.926904
4362	47	65	64	2018-04-14 20:56:24.931547	2018-04-14 20:56:24.931547
4363	47	65	80	2018-04-14 20:56:24.936037	2018-04-14 20:56:24.936037
4364	47	69	18	2018-04-14 20:56:24.940298	2018-04-14 20:56:24.940298
4365	47	69	64	2018-04-14 20:56:24.945237	2018-04-14 20:56:24.945237
4366	47	69	80	2018-04-14 20:56:24.949958	2018-04-14 20:56:24.949958
4367	47	77	18	2018-04-14 20:56:24.956592	2018-04-14 20:56:24.956592
4368	47	77	54	2018-04-14 20:56:24.963224	2018-04-14 20:56:24.963224
4369	47	80	80	2018-04-14 20:56:24.968247	2018-04-14 20:56:24.968247
4370	47	81	64	2018-04-14 20:56:24.97399	2018-04-14 20:56:24.97399
4371	47	81	80	2018-04-14 20:56:24.97872	2018-04-14 20:56:24.97872
4372	47	87	12	2018-04-14 20:56:24.983317	2018-04-14 20:56:24.983317
4373	47	87	18	2018-04-14 20:56:24.987969	2018-04-14 20:56:24.987969
4374	47	87	53	2018-04-14 20:56:24.992659	2018-04-14 20:56:24.992659
4375	47	87	64	2018-04-14 20:56:24.997664	2018-04-14 20:56:24.997664
4376	47	87	80	2018-04-14 20:56:25.002585	2018-04-14 20:56:25.002585
4377	47	87	87	2018-04-14 20:56:25.007408	2018-04-14 20:56:25.007408
4378	47	87	109	2018-04-14 20:56:25.012119	2018-04-14 20:56:25.012119
4379	47	92	12	2018-04-14 20:56:25.017262	2018-04-14 20:56:25.017262
4380	47	92	18	2018-04-14 20:56:25.022655	2018-04-14 20:56:25.022655
4381	47	92	53	2018-04-14 20:56:25.037234	2018-04-14 20:56:25.037234
4382	47	92	64	2018-04-14 20:56:25.043723	2018-04-14 20:56:25.043723
4383	47	92	80	2018-04-14 20:56:25.049937	2018-04-14 20:56:25.049937
4384	47	92	87	2018-04-14 20:56:25.055052	2018-04-14 20:56:25.055052
4385	47	92	92	2018-04-14 20:56:25.060129	2018-04-14 20:56:25.060129
4386	47	92	109	2018-04-14 20:56:25.06528	2018-04-14 20:56:25.06528
4387	47	94	18	2018-04-14 20:56:25.07033	2018-04-14 20:56:25.07033
4388	47	94	64	2018-04-14 20:56:25.076697	2018-04-14 20:56:25.076697
4389	47	94	80	2018-04-14 20:56:25.082529	2018-04-14 20:56:25.082529
4390	47	99	18	2018-04-14 20:56:25.087876	2018-04-14 20:56:25.087876
4391	47	99	64	2018-04-14 20:56:25.09276	2018-04-14 20:56:25.09276
4392	47	99	80	2018-04-14 20:56:25.098724	2018-04-14 20:56:25.098724
4393	47	101	64	2018-04-14 20:56:25.103866	2018-04-14 20:56:25.103866
4394	47	101	80	2018-04-14 20:56:25.108833	2018-04-14 20:56:25.108833
4395	47	107	64	2018-04-14 20:56:25.11361	2018-04-14 20:56:25.11361
4396	47	107	80	2018-04-14 20:56:25.11893	2018-04-14 20:56:25.11893
4397	47	109	12	2018-04-14 20:56:25.12401	2018-04-14 20:56:25.12401
4398	47	109	18	2018-04-14 20:56:25.128769	2018-04-14 20:56:25.128769
4399	47	109	53	2018-04-14 20:56:25.133752	2018-04-14 20:56:25.133752
4400	47	109	64	2018-04-14 20:56:25.138819	2018-04-14 20:56:25.138819
4401	47	109	80	2018-04-14 20:56:25.143558	2018-04-14 20:56:25.143558
4402	47	109	109	2018-04-14 20:56:25.14834	2018-04-14 20:56:25.14834
4403	48	3	6	2018-04-14 20:56:25.153208	2018-04-14 20:56:25.153208
4404	48	3	22	2018-04-14 20:56:25.159322	2018-04-14 20:56:25.159322
4405	48	3	38	2018-04-14 20:56:25.165711	2018-04-14 20:56:25.165711
4406	48	3	71	2018-04-14 20:56:25.172132	2018-04-14 20:56:25.172132
4407	48	3	106	2018-04-14 20:56:25.178001	2018-04-14 20:56:25.178001
4408	48	3	110	2018-04-14 20:56:25.183368	2018-04-14 20:56:25.183368
4409	48	3	111	2018-04-14 20:56:25.190481	2018-04-14 20:56:25.190481
4410	48	7	19	2018-04-14 20:56:25.19692	2018-04-14 20:56:25.19692
4411	48	9	6	2018-04-14 20:56:25.205062	2018-04-14 20:56:25.205062
4412	48	9	19	2018-04-14 20:56:25.217014	2018-04-14 20:56:25.217014
4413	48	9	22	2018-04-14 20:56:25.228026	2018-04-14 20:56:25.228026
4414	48	9	38	2018-04-14 20:56:25.237914	2018-04-14 20:56:25.237914
4415	48	9	71	2018-04-14 20:56:25.244275	2018-04-14 20:56:25.244275
4416	48	9	106	2018-04-14 20:56:25.24985	2018-04-14 20:56:25.24985
4417	48	9	110	2018-04-14 20:56:25.2553	2018-04-14 20:56:25.2553
4418	48	9	111	2018-04-14 20:56:25.263275	2018-04-14 20:56:25.263275
4419	48	13	6	2018-04-14 20:56:25.27024	2018-04-14 20:56:25.27024
4420	48	13	22	2018-04-14 20:56:25.277583	2018-04-14 20:56:25.277583
4421	48	13	38	2018-04-14 20:56:25.283748	2018-04-14 20:56:25.283748
4422	48	13	71	2018-04-14 20:56:25.289696	2018-04-14 20:56:25.289696
4423	48	13	106	2018-04-14 20:56:25.297719	2018-04-14 20:56:25.297719
4424	48	13	110	2018-04-14 20:56:25.302923	2018-04-14 20:56:25.302923
4425	48	13	111	2018-04-14 20:56:25.307895	2018-04-14 20:56:25.307895
4426	48	14	19	2018-04-14 20:56:25.314223	2018-04-14 20:56:25.314223
4427	48	15	19	2018-04-14 20:56:25.320052	2018-04-14 20:56:25.320052
4428	48	19	2	2018-04-14 20:56:25.325673	2018-04-14 20:56:25.325673
4429	48	19	4	2018-04-14 20:56:25.331553	2018-04-14 20:56:25.331553
4430	48	19	16	2018-04-14 20:56:25.336899	2018-04-14 20:56:25.336899
4431	48	19	17	2018-04-14 20:56:25.342175	2018-04-14 20:56:25.342175
4432	48	19	41	2018-04-14 20:56:25.349441	2018-04-14 20:56:25.349441
4433	48	19	46	2018-04-14 20:56:25.355226	2018-04-14 20:56:25.355226
4434	48	19	48	2018-04-14 20:56:25.360777	2018-04-14 20:56:25.360777
4435	48	19	52	2018-04-14 20:56:25.367529	2018-04-14 20:56:25.367529
4436	48	19	60	2018-04-14 20:56:25.372901	2018-04-14 20:56:25.372901
4437	48	19	61	2018-04-14 20:56:25.379022	2018-04-14 20:56:25.379022
4438	48	19	64	2018-04-14 20:56:25.385303	2018-04-14 20:56:25.385303
4439	48	19	70	2018-04-14 20:56:25.391026	2018-04-14 20:56:25.391026
4440	48	19	77	2018-04-14 20:56:25.397449	2018-04-14 20:56:25.397449
4441	48	19	78	2018-04-14 20:56:25.403369	2018-04-14 20:56:25.403369
4442	48	19	80	2018-04-14 20:56:25.409467	2018-04-14 20:56:25.409467
4443	48	19	84	2018-04-14 20:56:25.41744	2018-04-14 20:56:25.41744
4444	48	19	91	2018-04-14 20:56:25.423323	2018-04-14 20:56:25.423323
4445	48	19	95	2018-04-14 20:56:25.429292	2018-04-14 20:56:25.429292
4446	48	19	98	2018-04-14 20:56:25.435523	2018-04-14 20:56:25.435523
4447	48	19	103	2018-04-14 20:56:25.440998	2018-04-14 20:56:25.440998
4448	48	20	6	2018-04-14 20:56:25.447606	2018-04-14 20:56:25.447606
4449	48	20	22	2018-04-14 20:56:25.453775	2018-04-14 20:56:25.453775
4450	48	20	38	2018-04-14 20:56:25.459292	2018-04-14 20:56:25.459292
4451	48	20	71	2018-04-14 20:56:25.466386	2018-04-14 20:56:25.466386
4452	48	20	106	2018-04-14 20:56:25.472273	2018-04-14 20:56:25.472273
4453	48	20	110	2018-04-14 20:56:25.477902	2018-04-14 20:56:25.477902
4454	48	20	111	2018-04-14 20:56:25.484601	2018-04-14 20:56:25.484601
4455	48	23	6	2018-04-14 20:56:25.490285	2018-04-14 20:56:25.490285
4456	48	23	22	2018-04-14 20:56:25.495959	2018-04-14 20:56:25.495959
4457	48	23	38	2018-04-14 20:56:25.5023	2018-04-14 20:56:25.5023
4458	48	23	71	2018-04-14 20:56:25.50776	2018-04-14 20:56:25.50776
4459	48	23	106	2018-04-14 20:56:25.513849	2018-04-14 20:56:25.513849
4460	48	23	110	2018-04-14 20:56:25.520638	2018-04-14 20:56:25.520638
4461	48	23	111	2018-04-14 20:56:25.52573	2018-04-14 20:56:25.52573
4462	48	24	19	2018-04-14 20:56:25.53261	2018-04-14 20:56:25.53261
4463	48	26	19	2018-04-14 20:56:25.538375	2018-04-14 20:56:25.538375
4464	48	29	6	2018-04-14 20:56:25.54352	2018-04-14 20:56:25.54352
4465	48	29	22	2018-04-14 20:56:25.550304	2018-04-14 20:56:25.550304
4466	48	29	38	2018-04-14 20:56:25.556165	2018-04-14 20:56:25.556165
4467	48	29	71	2018-04-14 20:56:25.561526	2018-04-14 20:56:25.561526
4468	48	29	106	2018-04-14 20:56:25.568451	2018-04-14 20:56:25.568451
4469	48	29	110	2018-04-14 20:56:25.574259	2018-04-14 20:56:25.574259
4470	48	29	111	2018-04-14 20:56:25.580353	2018-04-14 20:56:25.580353
4471	48	33	6	2018-04-14 20:56:25.586394	2018-04-14 20:56:25.586394
4472	48	33	22	2018-04-14 20:56:25.592183	2018-04-14 20:56:25.592183
4473	48	33	38	2018-04-14 20:56:25.599014	2018-04-14 20:56:25.599014
4474	48	33	71	2018-04-14 20:56:25.604954	2018-04-14 20:56:25.604954
4475	48	33	106	2018-04-14 20:56:25.609989	2018-04-14 20:56:25.609989
4476	48	33	110	2018-04-14 20:56:25.616674	2018-04-14 20:56:25.616674
4477	48	33	111	2018-04-14 20:56:25.622368	2018-04-14 20:56:25.622368
4478	48	34	19	2018-04-14 20:56:25.627559	2018-04-14 20:56:25.627559
4479	48	36	19	2018-04-14 20:56:25.634673	2018-04-14 20:56:25.634673
4480	48	39	19	2018-04-14 20:56:25.640473	2018-04-14 20:56:25.640473
4481	48	40	19	2018-04-14 20:56:25.645946	2018-04-14 20:56:25.645946
4482	48	45	6	2018-04-14 20:56:25.652157	2018-04-14 20:56:25.652157
4483	48	45	22	2018-04-14 20:56:25.657927	2018-04-14 20:56:25.657927
4484	48	45	38	2018-04-14 20:56:25.66333	2018-04-14 20:56:25.66333
4485	48	45	71	2018-04-14 20:56:25.668045	2018-04-14 20:56:25.668045
4486	48	45	106	2018-04-14 20:56:25.673203	2018-04-14 20:56:25.673203
4487	48	45	110	2018-04-14 20:56:25.678181	2018-04-14 20:56:25.678181
4488	48	45	111	2018-04-14 20:56:25.682695	2018-04-14 20:56:25.682695
4489	48	47	6	2018-04-14 20:56:25.688976	2018-04-14 20:56:25.688976
4490	48	47	22	2018-04-14 20:56:25.693969	2018-04-14 20:56:25.693969
4491	48	47	38	2018-04-14 20:56:25.699983	2018-04-14 20:56:25.699983
4492	48	47	71	2018-04-14 20:56:25.708052	2018-04-14 20:56:25.708052
4493	48	47	106	2018-04-14 20:56:25.713548	2018-04-14 20:56:25.713548
4494	48	47	110	2018-04-14 20:56:25.718277	2018-04-14 20:56:25.718277
4495	48	47	111	2018-04-14 20:56:25.723625	2018-04-14 20:56:25.723625
4496	48	49	6	2018-04-14 20:56:25.728984	2018-04-14 20:56:25.728984
4497	48	49	22	2018-04-14 20:56:25.733654	2018-04-14 20:56:25.733654
4498	48	49	38	2018-04-14 20:56:25.739518	2018-04-14 20:56:25.739518
4499	48	49	71	2018-04-14 20:56:25.745179	2018-04-14 20:56:25.745179
4500	48	49	106	2018-04-14 20:56:25.750999	2018-04-14 20:56:25.750999
4501	48	49	110	2018-04-14 20:56:25.757294	2018-04-14 20:56:25.757294
4502	48	49	111	2018-04-14 20:56:25.77256	2018-04-14 20:56:25.77256
4503	48	50	6	2018-04-14 20:56:25.778753	2018-04-14 20:56:25.778753
4504	48	50	22	2018-04-14 20:56:25.786102	2018-04-14 20:56:25.786102
4505	48	50	38	2018-04-14 20:56:25.792546	2018-04-14 20:56:25.792546
4506	48	50	71	2018-04-14 20:56:25.799387	2018-04-14 20:56:25.799387
4507	48	50	106	2018-04-14 20:56:25.805968	2018-04-14 20:56:25.805968
4508	48	50	110	2018-04-14 20:56:25.811864	2018-04-14 20:56:25.811864
4509	48	50	111	2018-04-14 20:56:25.819226	2018-04-14 20:56:25.819226
4510	48	53	6	2018-04-14 20:56:25.825054	2018-04-14 20:56:25.825054
4511	48	53	22	2018-04-14 20:56:25.832343	2018-04-14 20:56:25.832343
4512	48	53	38	2018-04-14 20:56:25.838582	2018-04-14 20:56:25.838582
4513	48	53	71	2018-04-14 20:56:25.844561	2018-04-14 20:56:25.844561
4514	48	53	106	2018-04-14 20:56:25.851824	2018-04-14 20:56:25.851824
4515	48	53	110	2018-04-14 20:56:25.859247	2018-04-14 20:56:25.859247
4516	48	53	111	2018-04-14 20:56:25.866388	2018-04-14 20:56:25.866388
4517	48	54	6	2018-04-14 20:56:25.872808	2018-04-14 20:56:25.872808
4518	48	54	22	2018-04-14 20:56:25.879183	2018-04-14 20:56:25.879183
4519	48	54	38	2018-04-14 20:56:25.887142	2018-04-14 20:56:25.887142
4520	48	54	71	2018-04-14 20:56:25.893658	2018-04-14 20:56:25.893658
4521	48	54	106	2018-04-14 20:56:25.900827	2018-04-14 20:56:25.900827
4522	48	54	110	2018-04-14 20:56:25.907076	2018-04-14 20:56:25.907076
4523	48	54	111	2018-04-14 20:56:25.912354	2018-04-14 20:56:25.912354
4524	48	57	6	2018-04-14 20:56:25.919289	2018-04-14 20:56:25.919289
4525	48	57	22	2018-04-14 20:56:25.924986	2018-04-14 20:56:25.924986
4526	48	57	38	2018-04-14 20:56:25.930745	2018-04-14 20:56:25.930745
4527	48	57	71	2018-04-14 20:56:25.937339	2018-04-14 20:56:25.937339
4528	48	57	106	2018-04-14 20:56:25.942994	2018-04-14 20:56:25.942994
4529	48	57	110	2018-04-14 20:56:25.947918	2018-04-14 20:56:25.947918
4530	48	57	111	2018-04-14 20:56:25.952742	2018-04-14 20:56:25.952742
4531	48	58	6	2018-04-14 20:56:25.958026	2018-04-14 20:56:25.958026
4532	48	58	22	2018-04-14 20:56:25.963022	2018-04-14 20:56:25.963022
4533	48	58	38	2018-04-14 20:56:25.968077	2018-04-14 20:56:25.968077
4534	48	58	71	2018-04-14 20:56:25.973666	2018-04-14 20:56:25.973666
4535	48	58	106	2018-04-14 20:56:25.979085	2018-04-14 20:56:25.979085
4536	48	58	110	2018-04-14 20:56:25.983952	2018-04-14 20:56:25.983952
4537	48	58	111	2018-04-14 20:56:25.989131	2018-04-14 20:56:25.989131
4538	48	69	6	2018-04-14 20:56:25.995854	2018-04-14 20:56:25.995854
4539	48	69	22	2018-04-14 20:56:26.000841	2018-04-14 20:56:26.000841
4540	48	69	38	2018-04-14 20:56:26.006171	2018-04-14 20:56:26.006171
4541	48	69	71	2018-04-14 20:56:26.011849	2018-04-14 20:56:26.011849
4542	48	69	106	2018-04-14 20:56:26.016992	2018-04-14 20:56:26.016992
4543	48	69	110	2018-04-14 20:56:26.024149	2018-04-14 20:56:26.024149
4544	48	69	111	2018-04-14 20:56:26.03207	2018-04-14 20:56:26.03207
4545	48	72	19	2018-04-14 20:56:26.038338	2018-04-14 20:56:26.038338
4546	48	74	19	2018-04-14 20:56:26.044067	2018-04-14 20:56:26.044067
4547	48	82	19	2018-04-14 20:56:26.049419	2018-04-14 20:56:26.049419
4548	48	85	19	2018-04-14 20:56:26.054539	2018-04-14 20:56:26.054539
4549	48	86	19	2018-04-14 20:56:26.06021	2018-04-14 20:56:26.06021
4550	48	88	19	2018-04-14 20:56:26.065715	2018-04-14 20:56:26.065715
4551	48	89	19	2018-04-14 20:56:26.070908	2018-04-14 20:56:26.070908
4552	48	94	6	2018-04-14 20:56:26.07618	2018-04-14 20:56:26.07618
4553	48	94	22	2018-04-14 20:56:26.081468	2018-04-14 20:56:26.081468
4554	48	94	38	2018-04-14 20:56:26.08671	2018-04-14 20:56:26.08671
4555	48	94	71	2018-04-14 20:56:26.092146	2018-04-14 20:56:26.092146
4556	48	94	106	2018-04-14 20:56:26.098101	2018-04-14 20:56:26.098101
4557	48	94	110	2018-04-14 20:56:26.103852	2018-04-14 20:56:26.103852
4558	48	94	111	2018-04-14 20:56:26.111153	2018-04-14 20:56:26.111153
4559	48	99	6	2018-04-14 20:56:26.118833	2018-04-14 20:56:26.118833
4560	48	99	22	2018-04-14 20:56:26.126062	2018-04-14 20:56:26.126062
4561	48	99	38	2018-04-14 20:56:26.134226	2018-04-14 20:56:26.134226
4562	48	99	71	2018-04-14 20:56:26.141875	2018-04-14 20:56:26.141875
4563	48	99	106	2018-04-14 20:56:26.152055	2018-04-14 20:56:26.152055
4564	48	99	110	2018-04-14 20:56:26.16113	2018-04-14 20:56:26.16113
4565	48	99	111	2018-04-14 20:56:26.16797	2018-04-14 20:56:26.16797
4566	48	100	56	2018-04-14 20:56:26.174623	2018-04-14 20:56:26.174623
4567	48	108	6	2018-04-14 20:56:26.183519	2018-04-14 20:56:26.183519
4568	48	108	22	2018-04-14 20:56:26.192262	2018-04-14 20:56:26.192262
4569	48	108	38	2018-04-14 20:56:26.201538	2018-04-14 20:56:26.201538
4570	48	108	71	2018-04-14 20:56:26.207776	2018-04-14 20:56:26.207776
4571	48	108	106	2018-04-14 20:56:26.215101	2018-04-14 20:56:26.215101
4572	48	108	110	2018-04-14 20:56:26.221947	2018-04-14 20:56:26.221947
4573	48	108	111	2018-04-14 20:56:26.23028	2018-04-14 20:56:26.23028
4574	49	1	49	2018-04-14 20:56:26.237516	2018-04-14 20:56:26.237516
4575	49	5	49	2018-04-14 20:56:26.244209	2018-04-14 20:56:26.244209
4576	49	5	50	2018-04-14 20:56:26.251458	2018-04-14 20:56:26.251458
4577	49	8	49	2018-04-14 20:56:26.25838	2018-04-14 20:56:26.25838
4578	49	10	49	2018-04-14 20:56:26.265259	2018-04-14 20:56:26.265259
4579	49	25	49	2018-04-14 20:56:26.272132	2018-04-14 20:56:26.272132
4580	49	26	18	2018-04-14 20:56:26.279232	2018-04-14 20:56:26.279232
4581	49	26	26	2018-04-14 20:56:26.285702	2018-04-14 20:56:26.285702
4582	49	26	49	2018-04-14 20:56:26.292762	2018-04-14 20:56:26.292762
4583	49	26	50	2018-04-14 20:56:26.300469	2018-04-14 20:56:26.300469
4584	49	26	54	2018-04-14 20:56:26.306882	2018-04-14 20:56:26.306882
4585	49	30	49	2018-04-14 20:56:26.312998	2018-04-14 20:56:26.312998
4586	49	42	26	2018-04-14 20:56:26.319196	2018-04-14 20:56:26.319196
4587	49	42	49	2018-04-14 20:56:26.329279	2018-04-14 20:56:26.329279
4588	49	42	50	2018-04-14 20:56:26.339494	2018-04-14 20:56:26.339494
4589	49	44	49	2018-04-14 20:56:26.346062	2018-04-14 20:56:26.346062
4590	49	49	2	2018-04-14 20:56:26.353131	2018-04-14 20:56:26.353131
4591	49	49	4	2018-04-14 20:56:26.359738	2018-04-14 20:56:26.359738
4592	49	49	18	2018-04-14 20:56:26.368113	2018-04-14 20:56:26.368113
4593	49	49	36	2018-04-14 20:56:26.377482	2018-04-14 20:56:26.377482
4594	49	49	40	2018-04-14 20:56:26.384646	2018-04-14 20:56:26.384646
4595	49	49	49	2018-04-14 20:56:26.391802	2018-04-14 20:56:26.391802
4596	49	49	60	2018-04-14 20:56:26.398637	2018-04-14 20:56:26.398637
4597	49	49	77	2018-04-14 20:56:26.405159	2018-04-14 20:56:26.405159
4598	49	49	92	2018-04-14 20:56:26.414748	2018-04-14 20:56:26.414748
4599	49	50	12	2018-04-14 20:56:26.422476	2018-04-14 20:56:26.422476
4600	49	50	18	2018-04-14 20:56:26.429835	2018-04-14 20:56:26.429835
4601	49	50	36	2018-04-14 20:56:26.437489	2018-04-14 20:56:26.437489
4602	49	50	49	2018-04-14 20:56:26.445867	2018-04-14 20:56:26.445867
4603	49	50	50	2018-04-14 20:56:26.453933	2018-04-14 20:56:26.453933
4604	49	50	53	2018-04-14 20:56:26.46379	2018-04-14 20:56:26.46379
4605	49	50	87	2018-04-14 20:56:26.474224	2018-04-14 20:56:26.474224
4606	49	50	103	2018-04-14 20:56:26.481222	2018-04-14 20:56:26.481222
4607	49	50	109	2018-04-14 20:56:26.487489	2018-04-14 20:56:26.487489
4608	49	59	49	2018-04-14 20:56:26.494192	2018-04-14 20:56:26.494192
4609	49	76	49	2018-04-14 20:56:26.503616	2018-04-14 20:56:26.503616
4610	49	79	49	2018-04-14 20:56:26.512842	2018-04-14 20:56:26.512842
4611	49	90	49	2018-04-14 20:56:26.519277	2018-04-14 20:56:26.519277
4612	49	93	49	2018-04-14 20:56:26.525866	2018-04-14 20:56:26.525866
4613	49	102	26	2018-04-14 20:56:26.532574	2018-04-14 20:56:26.532574
4614	49	102	49	2018-04-14 20:56:26.565561	2018-04-14 20:56:26.565561
4615	49	102	50	2018-04-14 20:56:26.57339	2018-04-14 20:56:26.57339
4616	50	1	7	2018-04-14 20:56:26.579116	2018-04-14 20:56:26.579116
4617	50	1	9	2018-04-14 20:56:26.584611	2018-04-14 20:56:26.584611
4618	50	5	7	2018-04-14 20:56:26.589132	2018-04-14 20:56:26.589132
4619	50	7	2	2018-04-14 20:56:26.594307	2018-04-14 20:56:26.594307
4620	50	7	4	2018-04-14 20:56:26.60016	2018-04-14 20:56:26.60016
4621	50	7	12	2018-04-14 20:56:26.608086	2018-04-14 20:56:26.608086
4622	50	7	36	2018-04-14 20:56:26.613209	2018-04-14 20:56:26.613209
4623	50	7	40	2018-04-14 20:56:26.618297	2018-04-14 20:56:26.618297
4624	50	7	53	2018-04-14 20:56:26.624849	2018-04-14 20:56:26.624849
4625	50	7	60	2018-04-14 20:56:26.629767	2018-04-14 20:56:26.629767
4626	50	7	64	2018-04-14 20:56:26.635357	2018-04-14 20:56:26.635357
4627	50	7	77	2018-04-14 20:56:26.641794	2018-04-14 20:56:26.641794
4628	50	7	80	2018-04-14 20:56:26.647107	2018-04-14 20:56:26.647107
4629	50	7	87	2018-04-14 20:56:26.652962	2018-04-14 20:56:26.652962
4630	50	7	92	2018-04-14 20:56:26.659333	2018-04-14 20:56:26.659333
4631	50	7	103	2018-04-14 20:56:26.664843	2018-04-14 20:56:26.664843
4632	50	7	109	2018-04-14 20:56:26.672536	2018-04-14 20:56:26.672536
4633	50	8	7	2018-04-14 20:56:26.679225	2018-04-14 20:56:26.679225
4634	50	8	9	2018-04-14 20:56:26.684655	2018-04-14 20:56:26.684655
4635	50	9	2	2018-04-14 20:56:26.69211	2018-04-14 20:56:26.69211
4636	50	9	4	2018-04-14 20:56:26.697186	2018-04-14 20:56:26.697186
4637	50	9	12	2018-04-14 20:56:26.703733	2018-04-14 20:56:26.703733
4638	50	9	14	2018-04-14 20:56:26.711188	2018-04-14 20:56:26.711188
4639	50	9	40	2018-04-14 20:56:26.716461	2018-04-14 20:56:26.716461
4640	50	9	60	2018-04-14 20:56:26.72392	2018-04-14 20:56:26.72392
4641	50	9	64	2018-04-14 20:56:26.729395	2018-04-14 20:56:26.729395
4642	50	9	77	2018-04-14 20:56:26.734948	2018-04-14 20:56:26.734948
4643	50	9	80	2018-04-14 20:56:26.742422	2018-04-14 20:56:26.742422
4644	50	9	87	2018-04-14 20:56:26.747573	2018-04-14 20:56:26.747573
4645	50	9	92	2018-04-14 20:56:26.754541	2018-04-14 20:56:26.754541
4646	50	9	109	2018-04-14 20:56:26.760858	2018-04-14 20:56:26.760858
4647	50	10	7	2018-04-14 20:56:26.767356	2018-04-14 20:56:26.767356
4648	50	10	9	2018-04-14 20:56:26.775579	2018-04-14 20:56:26.775579
4649	50	25	7	2018-04-14 20:56:26.780986	2018-04-14 20:56:26.780986
4650	50	25	9	2018-04-14 20:56:26.785741	2018-04-14 20:56:26.785741
4651	50	30	7	2018-04-14 20:56:26.79015	2018-04-14 20:56:26.79015
4652	50	30	9	2018-04-14 20:56:26.79443	2018-04-14 20:56:26.79443
4653	50	44	7	2018-04-14 20:56:26.79883	2018-04-14 20:56:26.79883
4654	50	44	9	2018-04-14 20:56:26.803168	2018-04-14 20:56:26.803168
4655	50	47	13	2018-04-14 20:56:26.807421	2018-04-14 20:56:26.807421
4656	50	47	14	2018-04-14 20:56:26.811699	2018-04-14 20:56:26.811699
4657	50	47	64	2018-04-14 20:56:26.816232	2018-04-14 20:56:26.816232
4658	50	47	80	2018-04-14 20:56:26.820466	2018-04-14 20:56:26.820466
4659	50	59	7	2018-04-14 20:56:26.828097	2018-04-14 20:56:26.828097
4660	50	59	9	2018-04-14 20:56:26.834291	2018-04-14 20:56:26.834291
4661	50	76	7	2018-04-14 20:56:26.839666	2018-04-14 20:56:26.839666
4662	50	76	9	2018-04-14 20:56:26.843915	2018-04-14 20:56:26.843915
4663	50	79	7	2018-04-14 20:56:26.848538	2018-04-14 20:56:26.848538
4664	50	79	9	2018-04-14 20:56:26.853635	2018-04-14 20:56:26.853635
4665	50	90	7	2018-04-14 20:56:26.859084	2018-04-14 20:56:26.859084
4666	50	90	9	2018-04-14 20:56:26.864459	2018-04-14 20:56:26.864459
4667	50	93	7	2018-04-14 20:56:26.869872	2018-04-14 20:56:26.869872
4668	50	93	9	2018-04-14 20:56:26.874981	2018-04-14 20:56:26.874981
4669	51	1	3	2018-04-14 20:56:26.880237	2018-04-14 20:56:26.880237
4670	51	1	6	2018-04-14 20:56:26.885449	2018-04-14 20:56:26.885449
4671	51	1	12	2018-04-14 20:56:26.890642	2018-04-14 20:56:26.890642
4672	51	1	18	2018-04-14 20:56:26.89609	2018-04-14 20:56:26.89609
4673	51	1	20	2018-04-14 20:56:26.901435	2018-04-14 20:56:26.901435
4674	51	1	22	2018-04-14 20:56:26.90684	2018-04-14 20:56:26.90684
4675	51	1	29	2018-04-14 20:56:26.912217	2018-04-14 20:56:26.912217
4676	51	1	32	2018-04-14 20:56:26.917645	2018-04-14 20:56:26.917645
4677	51	1	37	2018-04-14 20:56:26.924374	2018-04-14 20:56:26.924374
4678	51	1	38	2018-04-14 20:56:26.93061	2018-04-14 20:56:26.93061
4679	51	1	45	2018-04-14 20:56:26.936776	2018-04-14 20:56:26.936776
4680	51	1	53	2018-04-14 20:56:26.942035	2018-04-14 20:56:26.942035
4681	51	1	54	2018-04-14 20:56:26.947677	2018-04-14 20:56:26.947677
4682	51	1	57	2018-04-14 20:56:26.953123	2018-04-14 20:56:26.953123
4683	51	1	58	2018-04-14 20:56:26.961069	2018-04-14 20:56:26.961069
4684	51	1	69	2018-04-14 20:56:26.967264	2018-04-14 20:56:26.967264
4685	51	1	71	2018-04-14 20:56:26.973001	2018-04-14 20:56:26.973001
4686	51	1	81	2018-04-14 20:56:26.980037	2018-04-14 20:56:26.980037
4687	51	1	87	2018-04-14 20:56:26.985159	2018-04-14 20:56:26.985159
4688	51	1	92	2018-04-14 20:56:26.989684	2018-04-14 20:56:26.989684
4689	51	1	94	2018-04-14 20:56:26.996606	2018-04-14 20:56:26.996606
4690	51	1	99	2018-04-14 20:56:27.003383	2018-04-14 20:56:27.003383
4691	51	1	101	2018-04-14 20:56:27.009098	2018-04-14 20:56:27.009098
4692	51	1	106	2018-04-14 20:56:27.01405	2018-04-14 20:56:27.01405
4693	51	1	107	2018-04-14 20:56:27.019188	2018-04-14 20:56:27.019188
4694	51	1	109	2018-04-14 20:56:27.025435	2018-04-14 20:56:27.025435
4695	51	1	110	2018-04-14 20:56:27.032462	2018-04-14 20:56:27.032462
4696	51	1	111	2018-04-14 20:56:27.038776	2018-04-14 20:56:27.038776
4697	51	5	3	2018-04-14 20:56:27.044295	2018-04-14 20:56:27.044295
4698	51	5	6	2018-04-14 20:56:27.050122	2018-04-14 20:56:27.050122
4699	51	5	12	2018-04-14 20:56:27.05484	2018-04-14 20:56:27.05484
4700	51	5	18	2018-04-14 20:56:27.059846	2018-04-14 20:56:27.059846
4701	51	5	20	2018-04-14 20:56:27.065315	2018-04-14 20:56:27.065315
4702	51	5	22	2018-04-14 20:56:27.070116	2018-04-14 20:56:27.070116
4703	51	5	29	2018-04-14 20:56:27.074988	2018-04-14 20:56:27.074988
4704	51	5	32	2018-04-14 20:56:27.080322	2018-04-14 20:56:27.080322
4705	51	5	37	2018-04-14 20:56:27.086544	2018-04-14 20:56:27.086544
4706	51	5	38	2018-04-14 20:56:27.092984	2018-04-14 20:56:27.092984
4707	51	5	45	2018-04-14 20:56:27.098567	2018-04-14 20:56:27.098567
4708	51	5	53	2018-04-14 20:56:27.103619	2018-04-14 20:56:27.103619
4709	51	5	54	2018-04-14 20:56:27.110592	2018-04-14 20:56:27.110592
4710	51	5	57	2018-04-14 20:56:27.115954	2018-04-14 20:56:27.115954
4711	51	5	58	2018-04-14 20:56:27.123147	2018-04-14 20:56:27.123147
4712	51	5	69	2018-04-14 20:56:27.129192	2018-04-14 20:56:27.129192
4713	51	5	71	2018-04-14 20:56:27.134228	2018-04-14 20:56:27.134228
4714	51	5	81	2018-04-14 20:56:27.140884	2018-04-14 20:56:27.140884
4715	51	5	87	2018-04-14 20:56:27.14669	2018-04-14 20:56:27.14669
4716	51	5	92	2018-04-14 20:56:27.151758	2018-04-14 20:56:27.151758
4717	51	5	94	2018-04-14 20:56:27.158389	2018-04-14 20:56:27.158389
4718	51	5	99	2018-04-14 20:56:27.164053	2018-04-14 20:56:27.164053
4719	51	5	101	2018-04-14 20:56:27.169525	2018-04-14 20:56:27.169525
4720	51	5	106	2018-04-14 20:56:27.176096	2018-04-14 20:56:27.176096
4721	51	5	107	2018-04-14 20:56:27.181579	2018-04-14 20:56:27.181579
4722	51	5	109	2018-04-14 20:56:27.186719	2018-04-14 20:56:27.186719
4723	51	5	110	2018-04-14 20:56:27.193404	2018-04-14 20:56:27.193404
4724	51	5	111	2018-04-14 20:56:27.198432	2018-04-14 20:56:27.198432
4725	51	8	3	2018-04-14 20:56:27.203457	2018-04-14 20:56:27.203457
4726	51	8	6	2018-04-14 20:56:27.210374	2018-04-14 20:56:27.210374
4727	51	8	12	2018-04-14 20:56:27.215645	2018-04-14 20:56:27.215645
4728	51	8	18	2018-04-14 20:56:27.220937	2018-04-14 20:56:27.220937
4729	51	8	20	2018-04-14 20:56:27.227921	2018-04-14 20:56:27.227921
4730	51	8	22	2018-04-14 20:56:27.233125	2018-04-14 20:56:27.233125
4731	51	8	29	2018-04-14 20:56:27.237522	2018-04-14 20:56:27.237522
4732	51	8	37	2018-04-14 20:56:27.244274	2018-04-14 20:56:27.244274
4733	51	8	38	2018-04-14 20:56:27.249623	2018-04-14 20:56:27.249623
4734	51	8	45	2018-04-14 20:56:27.254143	2018-04-14 20:56:27.254143
4735	51	8	53	2018-04-14 20:56:27.260258	2018-04-14 20:56:27.260258
4736	51	8	54	2018-04-14 20:56:27.265478	2018-04-14 20:56:27.265478
4737	51	8	55	2018-04-14 20:56:27.270232	2018-04-14 20:56:27.270232
4738	51	8	56	2018-04-14 20:56:27.276234	2018-04-14 20:56:27.276234
4739	51	8	57	2018-04-14 20:56:27.281314	2018-04-14 20:56:27.281314
4740	51	8	58	2018-04-14 20:56:27.285746	2018-04-14 20:56:27.285746
4741	51	8	69	2018-04-14 20:56:27.290271	2018-04-14 20:56:27.290271
4742	51	8	71	2018-04-14 20:56:27.295298	2018-04-14 20:56:27.295298
4743	51	8	81	2018-04-14 20:56:27.299784	2018-04-14 20:56:27.299784
4744	51	8	87	2018-04-14 20:56:27.30751	2018-04-14 20:56:27.30751
4745	51	8	92	2018-04-14 20:56:27.314337	2018-04-14 20:56:27.314337
4746	51	8	94	2018-04-14 20:56:27.318696	2018-04-14 20:56:27.318696
4747	51	8	99	2018-04-14 20:56:27.323588	2018-04-14 20:56:27.323588
4748	51	8	101	2018-04-14 20:56:27.327906	2018-04-14 20:56:27.327906
4749	51	8	106	2018-04-14 20:56:27.332285	2018-04-14 20:56:27.332285
4750	51	8	107	2018-04-14 20:56:27.336761	2018-04-14 20:56:27.336761
4751	51	8	109	2018-04-14 20:56:27.341129	2018-04-14 20:56:27.341129
4752	51	8	110	2018-04-14 20:56:27.345562	2018-04-14 20:56:27.345562
4753	51	8	111	2018-04-14 20:56:27.350344	2018-04-14 20:56:27.350344
4754	51	10	1	2018-04-14 20:56:27.354768	2018-04-14 20:56:27.354768
4755	51	10	5	2018-04-14 20:56:27.359836	2018-04-14 20:56:27.359836
4756	51	10	8	2018-04-14 20:56:27.364732	2018-04-14 20:56:27.364732
4757	51	10	25	2018-04-14 20:56:27.370097	2018-04-14 20:56:27.370097
4758	51	10	30	2018-04-14 20:56:27.375759	2018-04-14 20:56:27.375759
4759	51	10	41	2018-04-14 20:56:27.382928	2018-04-14 20:56:27.382928
4760	51	10	42	2018-04-14 20:56:27.388532	2018-04-14 20:56:27.388532
4761	51	10	44	2018-04-14 20:56:27.394255	2018-04-14 20:56:27.394255
4762	51	10	59	2018-04-14 20:56:27.399752	2018-04-14 20:56:27.399752
4763	51	10	61	2018-04-14 20:56:27.406699	2018-04-14 20:56:27.406699
4764	51	10	75	2018-04-14 20:56:27.412697	2018-04-14 20:56:27.412697
4765	51	10	76	2018-04-14 20:56:27.418079	2018-04-14 20:56:27.418079
4766	51	10	78	2018-04-14 20:56:27.422735	2018-04-14 20:56:27.422735
4767	51	10	79	2018-04-14 20:56:27.427296	2018-04-14 20:56:27.427296
4768	51	10	90	2018-04-14 20:56:27.43211	2018-04-14 20:56:27.43211
4769	51	10	91	2018-04-14 20:56:27.436608	2018-04-14 20:56:27.436608
4770	51	10	93	2018-04-14 20:56:27.441098	2018-04-14 20:56:27.441098
4771	51	10	95	2018-04-14 20:56:27.44536	2018-04-14 20:56:27.44536
4772	51	10	102	2018-04-14 20:56:27.450416	2018-04-14 20:56:27.450416
4773	51	25	3	2018-04-14 20:56:27.456802	2018-04-14 20:56:27.456802
4774	51	25	6	2018-04-14 20:56:27.46112	2018-04-14 20:56:27.46112
4775	51	25	12	2018-04-14 20:56:27.465735	2018-04-14 20:56:27.465735
4776	51	25	18	2018-04-14 20:56:27.470337	2018-04-14 20:56:27.470337
4777	51	25	20	2018-04-14 20:56:27.475131	2018-04-14 20:56:27.475131
4778	51	25	22	2018-04-14 20:56:27.47974	2018-04-14 20:56:27.47974
4779	51	25	29	2018-04-14 20:56:27.484529	2018-04-14 20:56:27.484529
4780	51	25	37	2018-04-14 20:56:27.489193	2018-04-14 20:56:27.489193
4781	51	25	38	2018-04-14 20:56:27.495238	2018-04-14 20:56:27.495238
4782	51	25	45	2018-04-14 20:56:27.500027	2018-04-14 20:56:27.500027
4783	51	25	53	2018-04-14 20:56:27.50458	2018-04-14 20:56:27.50458
4784	51	25	54	2018-04-14 20:56:27.509171	2018-04-14 20:56:27.509171
4785	51	25	55	2018-04-14 20:56:27.513561	2018-04-14 20:56:27.513561
4786	51	25	56	2018-04-14 20:56:27.51796	2018-04-14 20:56:27.51796
4787	51	25	57	2018-04-14 20:56:27.525808	2018-04-14 20:56:27.525808
4788	51	25	58	2018-04-14 20:56:27.532423	2018-04-14 20:56:27.532423
4789	51	25	69	2018-04-14 20:56:27.537861	2018-04-14 20:56:27.537861
4790	51	25	71	2018-04-14 20:56:27.542275	2018-04-14 20:56:27.542275
4791	51	25	81	2018-04-14 20:56:27.547506	2018-04-14 20:56:27.547506
4792	51	25	87	2018-04-14 20:56:27.552202	2018-04-14 20:56:27.552202
4793	51	25	92	2018-04-14 20:56:27.556709	2018-04-14 20:56:27.556709
4794	51	25	94	2018-04-14 20:56:27.561644	2018-04-14 20:56:27.561644
4795	51	25	99	2018-04-14 20:56:27.566064	2018-04-14 20:56:27.566064
4796	51	25	101	2018-04-14 20:56:27.57085	2018-04-14 20:56:27.57085
4797	51	25	106	2018-04-14 20:56:27.575355	2018-04-14 20:56:27.575355
4798	51	25	107	2018-04-14 20:56:27.579675	2018-04-14 20:56:27.579675
4799	51	25	109	2018-04-14 20:56:27.584085	2018-04-14 20:56:27.584085
4800	51	25	110	2018-04-14 20:56:27.588439	2018-04-14 20:56:27.588439
4801	51	25	111	2018-04-14 20:56:27.592981	2018-04-14 20:56:27.592981
4802	51	27	7	2018-04-14 20:56:27.597512	2018-04-14 20:56:27.597512
4803	51	27	9	2018-04-14 20:56:27.602316	2018-04-14 20:56:27.602316
4804	51	27	13	2018-04-14 20:56:27.610861	2018-04-14 20:56:27.610861
4805	51	27	14	2018-04-14 20:56:27.620291	2018-04-14 20:56:27.620291
4806	51	27	15	2018-04-14 20:56:27.628335	2018-04-14 20:56:27.628335
4807	51	27	16	2018-04-14 20:56:27.634063	2018-04-14 20:56:27.634063
4808	51	27	17	2018-04-14 20:56:27.638275	2018-04-14 20:56:27.638275
4809	51	27	23	2018-04-14 20:56:27.643113	2018-04-14 20:56:27.643113
4810	51	27	24	2018-04-14 20:56:27.647898	2018-04-14 20:56:27.647898
4811	51	27	26	2018-04-14 20:56:27.652351	2018-04-14 20:56:27.652351
4812	51	27	31	2018-04-14 20:56:27.656791	2018-04-14 20:56:27.656791
4813	51	27	33	2018-04-14 20:56:27.661206	2018-04-14 20:56:27.661206
4814	51	27	34	2018-04-14 20:56:27.66581	2018-04-14 20:56:27.66581
4815	51	27	39	2018-04-14 20:56:27.670257	2018-04-14 20:56:27.670257
4816	51	27	41	2018-04-14 20:56:27.674925	2018-04-14 20:56:27.674925
4817	51	27	46	2018-04-14 20:56:27.680419	2018-04-14 20:56:27.680419
4818	51	27	47	2018-04-14 20:56:27.685082	2018-04-14 20:56:27.685082
4819	51	27	48	2018-04-14 20:56:27.689851	2018-04-14 20:56:27.689851
4820	51	27	49	2018-04-14 20:56:27.694443	2018-04-14 20:56:27.694443
4821	51	27	50	2018-04-14 20:56:27.699461	2018-04-14 20:56:27.699461
4822	51	27	52	2018-04-14 20:56:27.70502	2018-04-14 20:56:27.70502
4823	51	27	61	2018-04-14 20:56:27.710645	2018-04-14 20:56:27.710645
4824	51	27	68	2018-04-14 20:56:27.715842	2018-04-14 20:56:27.715842
4825	51	27	70	2018-04-14 20:56:27.720647	2018-04-14 20:56:27.720647
4826	51	27	72	2018-04-14 20:56:27.725116	2018-04-14 20:56:27.725116
4827	51	27	74	2018-04-14 20:56:27.73008	2018-04-14 20:56:27.73008
4828	51	27	78	2018-04-14 20:56:27.736204	2018-04-14 20:56:27.736204
4829	51	27	79	2018-04-14 20:56:27.740927	2018-04-14 20:56:27.740927
4830	51	27	82	2018-04-14 20:56:27.746391	2018-04-14 20:56:27.746391
4831	51	27	84	2018-04-14 20:56:27.751758	2018-04-14 20:56:27.751758
4832	51	27	85	2018-04-14 20:56:27.756683	2018-04-14 20:56:27.756683
4833	51	27	86	2018-04-14 20:56:27.763365	2018-04-14 20:56:27.763365
4834	51	27	88	2018-04-14 20:56:27.768565	2018-04-14 20:56:27.768565
4835	51	27	89	2018-04-14 20:56:27.773109	2018-04-14 20:56:27.773109
4836	51	27	91	2018-04-14 20:56:27.777487	2018-04-14 20:56:27.777487
4837	51	27	95	2018-04-14 20:56:27.781701	2018-04-14 20:56:27.781701
4838	51	27	98	2018-04-14 20:56:27.787344	2018-04-14 20:56:27.787344
4839	51	27	102	2018-04-14 20:56:27.792458	2018-04-14 20:56:27.792458
4840	51	27	108	2018-04-14 20:56:27.797901	2018-04-14 20:56:27.797901
4841	51	28	68	2018-04-14 20:56:27.80252	2018-04-14 20:56:27.80252
4842	51	30	3	2018-04-14 20:56:27.807983	2018-04-14 20:56:27.807983
4843	51	30	6	2018-04-14 20:56:27.815833	2018-04-14 20:56:27.815833
4844	51	30	12	2018-04-14 20:56:27.821883	2018-04-14 20:56:27.821883
4845	51	30	18	2018-04-14 20:56:27.829138	2018-04-14 20:56:27.829138
4846	51	30	20	2018-04-14 20:56:27.834241	2018-04-14 20:56:27.834241
4847	51	30	22	2018-04-14 20:56:27.83862	2018-04-14 20:56:27.83862
4848	51	30	29	2018-04-14 20:56:27.8467	2018-04-14 20:56:27.8467
4849	51	30	37	2018-04-14 20:56:27.854272	2018-04-14 20:56:27.854272
4850	51	30	38	2018-04-14 20:56:27.859908	2018-04-14 20:56:27.859908
4851	51	30	45	2018-04-14 20:56:27.865799	2018-04-14 20:56:27.865799
4852	51	30	53	2018-04-14 20:56:27.884817	2018-04-14 20:56:27.884817
4853	51	30	54	2018-04-14 20:56:27.889376	2018-04-14 20:56:27.889376
4854	51	30	55	2018-04-14 20:56:27.893785	2018-04-14 20:56:27.893785
4855	51	30	56	2018-04-14 20:56:27.898797	2018-04-14 20:56:27.898797
4856	51	30	57	2018-04-14 20:56:27.903049	2018-04-14 20:56:27.903049
4857	51	30	58	2018-04-14 20:56:27.907314	2018-04-14 20:56:27.907314
4858	51	30	69	2018-04-14 20:56:27.911481	2018-04-14 20:56:27.911481
4859	51	30	71	2018-04-14 20:56:27.915967	2018-04-14 20:56:27.915967
4860	51	30	81	2018-04-14 20:56:27.921965	2018-04-14 20:56:27.921965
4861	51	30	87	2018-04-14 20:56:27.930606	2018-04-14 20:56:27.930606
4862	51	30	92	2018-04-14 20:56:27.937967	2018-04-14 20:56:27.937967
4863	51	30	94	2018-04-14 20:56:27.942511	2018-04-14 20:56:27.942511
4864	51	30	99	2018-04-14 20:56:27.946877	2018-04-14 20:56:27.946877
4865	51	30	101	2018-04-14 20:56:27.951279	2018-04-14 20:56:27.951279
4866	51	30	106	2018-04-14 20:56:27.955841	2018-04-14 20:56:27.955841
4867	51	30	107	2018-04-14 20:56:27.961571	2018-04-14 20:56:27.961571
4868	51	30	109	2018-04-14 20:56:27.968351	2018-04-14 20:56:27.968351
4869	51	30	110	2018-04-14 20:56:27.972666	2018-04-14 20:56:27.972666
4870	51	30	111	2018-04-14 20:56:27.977015	2018-04-14 20:56:27.977015
4871	51	41	3	2018-04-14 20:56:27.981297	2018-04-14 20:56:27.981297
4872	51	41	12	2018-04-14 20:56:27.985912	2018-04-14 20:56:27.985912
4873	51	41	18	2018-04-14 20:56:27.990395	2018-04-14 20:56:27.990395
4874	51	41	20	2018-04-14 20:56:27.995037	2018-04-14 20:56:27.995037
4875	51	41	29	2018-04-14 20:56:28.001534	2018-04-14 20:56:28.001534
4876	51	41	37	2018-04-14 20:56:28.006161	2018-04-14 20:56:28.006161
4877	51	41	45	2018-04-14 20:56:28.010291	2018-04-14 20:56:28.010291
4878	51	41	53	2018-04-14 20:56:28.014437	2018-04-14 20:56:28.014437
4879	51	41	54	2018-04-14 20:56:28.018947	2018-04-14 20:56:28.018947
4880	51	41	55	2018-04-14 20:56:28.023179	2018-04-14 20:56:28.023179
4881	51	41	56	2018-04-14 20:56:28.027395	2018-04-14 20:56:28.027395
4882	51	41	57	2018-04-14 20:56:28.032873	2018-04-14 20:56:28.032873
4883	51	41	58	2018-04-14 20:56:28.049075	2018-04-14 20:56:28.049075
4884	51	41	64	2018-04-14 20:56:28.05801	2018-04-14 20:56:28.05801
4885	51	41	65	2018-04-14 20:56:28.064804	2018-04-14 20:56:28.064804
4886	51	41	69	2018-04-14 20:56:28.069661	2018-04-14 20:56:28.069661
4887	51	41	80	2018-04-14 20:56:28.074145	2018-04-14 20:56:28.074145
4888	51	41	81	2018-04-14 20:56:28.078513	2018-04-14 20:56:28.078513
4889	51	41	87	2018-04-14 20:56:28.085684	2018-04-14 20:56:28.085684
4890	51	41	92	2018-04-14 20:56:28.090589	2018-04-14 20:56:28.090589
4891	51	41	94	2018-04-14 20:56:28.094977	2018-04-14 20:56:28.094977
4892	51	41	99	2018-04-14 20:56:28.099436	2018-04-14 20:56:28.099436
4893	51	41	101	2018-04-14 20:56:28.105026	2018-04-14 20:56:28.105026
4894	51	41	107	2018-04-14 20:56:28.110671	2018-04-14 20:56:28.110671
4895	51	41	109	2018-04-14 20:56:28.115044	2018-04-14 20:56:28.115044
4896	51	42	3	2018-04-14 20:56:28.119567	2018-04-14 20:56:28.119567
4897	51	42	11	2018-04-14 20:56:28.124027	2018-04-14 20:56:28.124027
4898	51	42	12	2018-04-14 20:56:28.128475	2018-04-14 20:56:28.128475
4899	51	42	15	2018-04-14 20:56:28.13253	2018-04-14 20:56:28.13253
4900	51	42	18	2018-04-14 20:56:28.137233	2018-04-14 20:56:28.137233
4901	51	42	20	2018-04-14 20:56:28.144754	2018-04-14 20:56:28.144754
4902	51	42	24	2018-04-14 20:56:28.151675	2018-04-14 20:56:28.151675
4903	51	42	29	2018-04-14 20:56:28.156428	2018-04-14 20:56:28.156428
4904	51	42	31	2018-04-14 20:56:28.161033	2018-04-14 20:56:28.161033
4905	51	42	37	2018-04-14 20:56:28.165483	2018-04-14 20:56:28.165483
4906	51	42	39	2018-04-14 20:56:28.170197	2018-04-14 20:56:28.170197
4907	51	42	41	2018-04-14 20:56:28.174444	2018-04-14 20:56:28.174444
4908	51	42	42	2018-04-14 20:56:28.178891	2018-04-14 20:56:28.178891
4909	51	42	45	2018-04-14 20:56:28.184826	2018-04-14 20:56:28.184826
4910	51	42	53	2018-04-14 20:56:28.190138	2018-04-14 20:56:28.190138
4911	51	42	54	2018-04-14 20:56:28.197722	2018-04-14 20:56:28.197722
4912	51	42	57	2018-04-14 20:56:28.202886	2018-04-14 20:56:28.202886
4913	51	42	58	2018-04-14 20:56:28.208098	2018-04-14 20:56:28.208098
4914	51	42	61	2018-04-14 20:56:28.21244	2018-04-14 20:56:28.21244
4915	51	42	69	2018-04-14 20:56:28.218924	2018-04-14 20:56:28.218924
4916	51	42	72	2018-04-14 20:56:28.224386	2018-04-14 20:56:28.224386
4917	51	42	74	2018-04-14 20:56:28.228671	2018-04-14 20:56:28.228671
4918	51	42	78	2018-04-14 20:56:28.233247	2018-04-14 20:56:28.233247
4919	51	42	81	2018-04-14 20:56:28.237627	2018-04-14 20:56:28.237627
4920	51	42	82	2018-04-14 20:56:28.242322	2018-04-14 20:56:28.242322
4921	51	42	83	2018-04-14 20:56:28.246825	2018-04-14 20:56:28.246825
4922	51	42	85	2018-04-14 20:56:28.251694	2018-04-14 20:56:28.251694
4923	51	42	86	2018-04-14 20:56:28.256371	2018-04-14 20:56:28.256371
4924	51	42	87	2018-04-14 20:56:28.260735	2018-04-14 20:56:28.260735
4925	51	42	88	2018-04-14 20:56:28.265198	2018-04-14 20:56:28.265198
4926	51	42	89	2018-04-14 20:56:28.26967	2018-04-14 20:56:28.26967
4927	51	42	91	2018-04-14 20:56:28.27715	2018-04-14 20:56:28.27715
4928	51	42	92	2018-04-14 20:56:28.285074	2018-04-14 20:56:28.285074
4929	51	42	94	2018-04-14 20:56:28.291782	2018-04-14 20:56:28.291782
4930	51	42	95	2018-04-14 20:56:28.300005	2018-04-14 20:56:28.300005
4931	51	42	99	2018-04-14 20:56:28.307769	2018-04-14 20:56:28.307769
4932	51	42	100	2018-04-14 20:56:28.314169	2018-04-14 20:56:28.314169
4933	51	42	102	2018-04-14 20:56:28.318482	2018-04-14 20:56:28.318482
4934	51	42	109	2018-04-14 20:56:28.322773	2018-04-14 20:56:28.322773
4935	51	43	68	2018-04-14 20:56:28.326988	2018-04-14 20:56:28.326988
4936	51	43	100	2018-04-14 20:56:28.331642	2018-04-14 20:56:28.331642
4937	51	44	3	2018-04-14 20:56:28.336293	2018-04-14 20:56:28.336293
4938	51	44	6	2018-04-14 20:56:28.341177	2018-04-14 20:56:28.341177
4939	51	44	12	2018-04-14 20:56:28.345588	2018-04-14 20:56:28.345588
4940	51	44	18	2018-04-14 20:56:28.350222	2018-04-14 20:56:28.350222
4941	51	44	20	2018-04-14 20:56:28.355098	2018-04-14 20:56:28.355098
4942	51	44	22	2018-04-14 20:56:28.359703	2018-04-14 20:56:28.359703
4943	51	44	29	2018-04-14 20:56:28.364059	2018-04-14 20:56:28.364059
4944	51	44	37	2018-04-14 20:56:28.368783	2018-04-14 20:56:28.368783
4945	51	44	38	2018-04-14 20:56:28.372919	2018-04-14 20:56:28.372919
4946	51	44	45	2018-04-14 20:56:28.377317	2018-04-14 20:56:28.377317
4947	51	44	53	2018-04-14 20:56:28.381698	2018-04-14 20:56:28.381698
4948	51	44	54	2018-04-14 20:56:28.386067	2018-04-14 20:56:28.386067
4949	51	44	55	2018-04-14 20:56:28.390122	2018-04-14 20:56:28.390122
4950	51	44	56	2018-04-14 20:56:28.394426	2018-04-14 20:56:28.394426
4951	51	44	57	2018-04-14 20:56:28.399098	2018-04-14 20:56:28.399098
4952	51	44	58	2018-04-14 20:56:28.403587	2018-04-14 20:56:28.403587
4953	51	44	69	2018-04-14 20:56:28.409124	2018-04-14 20:56:28.409124
4954	51	44	71	2018-04-14 20:56:28.414472	2018-04-14 20:56:28.414472
4955	51	44	81	2018-04-14 20:56:28.418901	2018-04-14 20:56:28.418901
4956	51	44	87	2018-04-14 20:56:28.423088	2018-04-14 20:56:28.423088
4957	51	44	92	2018-04-14 20:56:28.427323	2018-04-14 20:56:28.427323
4958	51	44	94	2018-04-14 20:56:28.432248	2018-04-14 20:56:28.432248
4959	51	44	99	2018-04-14 20:56:28.437435	2018-04-14 20:56:28.437435
4960	51	44	101	2018-04-14 20:56:28.442157	2018-04-14 20:56:28.442157
4961	51	44	106	2018-04-14 20:56:28.446551	2018-04-14 20:56:28.446551
4962	51	44	107	2018-04-14 20:56:28.451321	2018-04-14 20:56:28.451321
4963	51	44	109	2018-04-14 20:56:28.456029	2018-04-14 20:56:28.456029
4964	51	44	110	2018-04-14 20:56:28.46049	2018-04-14 20:56:28.46049
4965	51	44	111	2018-04-14 20:56:28.465124	2018-04-14 20:56:28.465124
4966	51	59	3	2018-04-14 20:56:28.469633	2018-04-14 20:56:28.469633
4967	51	59	6	2018-04-14 20:56:28.474107	2018-04-14 20:56:28.474107
4968	51	59	12	2018-04-14 20:56:28.478701	2018-04-14 20:56:28.478701
4969	51	59	18	2018-04-14 20:56:28.483354	2018-04-14 20:56:28.483354
4970	51	59	20	2018-04-14 20:56:28.489049	2018-04-14 20:56:28.489049
4971	51	59	22	2018-04-14 20:56:28.494461	2018-04-14 20:56:28.494461
4972	51	59	29	2018-04-14 20:56:28.500436	2018-04-14 20:56:28.500436
4973	51	59	32	2018-04-14 20:56:28.505219	2018-04-14 20:56:28.505219
4974	51	59	37	2018-04-14 20:56:28.509757	2018-04-14 20:56:28.509757
4975	51	59	38	2018-04-14 20:56:28.514488	2018-04-14 20:56:28.514488
4976	51	59	45	2018-04-14 20:56:28.519259	2018-04-14 20:56:28.519259
4977	51	59	53	2018-04-14 20:56:28.524011	2018-04-14 20:56:28.524011
4978	51	59	54	2018-04-14 20:56:28.531753	2018-04-14 20:56:28.531753
4979	51	59	57	2018-04-14 20:56:28.538255	2018-04-14 20:56:28.538255
4980	51	59	58	2018-04-14 20:56:28.542743	2018-04-14 20:56:28.542743
4981	51	59	69	2018-04-14 20:56:28.548639	2018-04-14 20:56:28.548639
4982	51	59	71	2018-04-14 20:56:28.553393	2018-04-14 20:56:28.553393
4983	51	59	81	2018-04-14 20:56:28.559085	2018-04-14 20:56:28.559085
4984	51	59	87	2018-04-14 20:56:28.565184	2018-04-14 20:56:28.565184
4985	51	59	92	2018-04-14 20:56:28.569484	2018-04-14 20:56:28.569484
4986	51	59	94	2018-04-14 20:56:28.573954	2018-04-14 20:56:28.573954
4987	51	59	99	2018-04-14 20:56:28.578536	2018-04-14 20:56:28.578536
4988	51	59	101	2018-04-14 20:56:28.583138	2018-04-14 20:56:28.583138
4989	51	59	106	2018-04-14 20:56:28.588956	2018-04-14 20:56:28.588956
4990	51	59	107	2018-04-14 20:56:28.596835	2018-04-14 20:56:28.596835
4991	51	59	109	2018-04-14 20:56:28.602815	2018-04-14 20:56:28.602815
4992	51	59	110	2018-04-14 20:56:28.607195	2018-04-14 20:56:28.607195
4993	51	59	111	2018-04-14 20:56:28.611569	2018-04-14 20:56:28.611569
4994	51	61	3	2018-04-14 20:56:28.616265	2018-04-14 20:56:28.616265
4995	51	61	12	2018-04-14 20:56:28.620749	2018-04-14 20:56:28.620749
4996	51	61	18	2018-04-14 20:56:28.625412	2018-04-14 20:56:28.625412
4997	51	61	20	2018-04-14 20:56:28.629838	2018-04-14 20:56:28.629838
4998	51	61	29	2018-04-14 20:56:28.637115	2018-04-14 20:56:28.637115
4999	51	61	37	2018-04-14 20:56:28.643565	2018-04-14 20:56:28.643565
5000	51	61	45	2018-04-14 20:56:28.648523	2018-04-14 20:56:28.648523
5001	51	61	53	2018-04-14 20:56:28.653012	2018-04-14 20:56:28.653012
5002	51	61	54	2018-04-14 20:56:28.657629	2018-04-14 20:56:28.657629
5003	51	61	55	2018-04-14 20:56:28.661988	2018-04-14 20:56:28.661988
5004	51	61	56	2018-04-14 20:56:28.666791	2018-04-14 20:56:28.666791
5005	51	61	57	2018-04-14 20:56:28.671106	2018-04-14 20:56:28.671106
5006	51	61	58	2018-04-14 20:56:28.677346	2018-04-14 20:56:28.677346
5007	51	61	65	2018-04-14 20:56:28.682939	2018-04-14 20:56:28.682939
5008	51	61	69	2018-04-14 20:56:28.687414	2018-04-14 20:56:28.687414
5009	51	61	81	2018-04-14 20:56:28.691871	2018-04-14 20:56:28.691871
5010	51	61	87	2018-04-14 20:56:28.699393	2018-04-14 20:56:28.699393
5011	51	61	92	2018-04-14 20:56:28.705236	2018-04-14 20:56:28.705236
5012	51	61	94	2018-04-14 20:56:28.711004	2018-04-14 20:56:28.711004
5013	51	61	99	2018-04-14 20:56:28.717118	2018-04-14 20:56:28.717118
5014	51	61	101	2018-04-14 20:56:28.7221	2018-04-14 20:56:28.7221
5015	51	61	107	2018-04-14 20:56:28.726644	2018-04-14 20:56:28.726644
5016	51	61	109	2018-04-14 20:56:28.731447	2018-04-14 20:56:28.731447
5017	51	66	68	2018-04-14 20:56:28.736168	2018-04-14 20:56:28.736168
5018	51	67	31	2018-04-14 20:56:28.740688	2018-04-14 20:56:28.740688
5019	51	67	32	2018-04-14 20:56:28.745197	2018-04-14 20:56:28.745197
5020	51	67	68	2018-04-14 20:56:28.749721	2018-04-14 20:56:28.749721
5021	51	68	1	2018-04-14 20:56:28.754054	2018-04-14 20:56:28.754054
5022	51	68	2	2018-04-14 20:56:28.758414	2018-04-14 20:56:28.758414
5023	51	68	4	2018-04-14 20:56:28.76335	2018-04-14 20:56:28.76335
5024	51	68	5	2018-04-14 20:56:28.767894	2018-04-14 20:56:28.767894
5025	51	68	8	2018-04-14 20:56:28.77252	2018-04-14 20:56:28.77252
5026	51	68	25	2018-04-14 20:56:28.777494	2018-04-14 20:56:28.777494
5027	51	68	30	2018-04-14 20:56:28.782421	2018-04-14 20:56:28.782421
5028	51	68	36	2018-04-14 20:56:28.78802	2018-04-14 20:56:28.78802
5029	51	68	40	2018-04-14 20:56:28.799767	2018-04-14 20:56:28.799767
5030	51	68	44	2018-04-14 20:56:28.811144	2018-04-14 20:56:28.811144
5031	51	68	59	2018-04-14 20:56:28.818945	2018-04-14 20:56:28.818945
5032	51	68	60	2018-04-14 20:56:28.824351	2018-04-14 20:56:28.824351
5033	51	68	64	2018-04-14 20:56:28.828918	2018-04-14 20:56:28.828918
5034	51	68	76	2018-04-14 20:56:28.833671	2018-04-14 20:56:28.833671
5035	51	68	77	2018-04-14 20:56:28.838165	2018-04-14 20:56:28.838165
5036	51	68	79	2018-04-14 20:56:28.84287	2018-04-14 20:56:28.84287
5037	51	68	80	2018-04-14 20:56:28.847454	2018-04-14 20:56:28.847454
5038	51	68	90	2018-04-14 20:56:28.852026	2018-04-14 20:56:28.852026
5039	51	68	93	2018-04-14 20:56:28.857742	2018-04-14 20:56:28.857742
5040	51	73	68	2018-04-14 20:56:28.865413	2018-04-14 20:56:28.865413
5041	51	75	3	2018-04-14 20:56:28.873858	2018-04-14 20:56:28.873858
5042	51	75	6	2018-04-14 20:56:28.880587	2018-04-14 20:56:28.880587
5043	51	75	12	2018-04-14 20:56:28.885506	2018-04-14 20:56:28.885506
5044	51	75	18	2018-04-14 20:56:28.890255	2018-04-14 20:56:28.890255
5045	51	75	20	2018-04-14 20:56:28.894769	2018-04-14 20:56:28.894769
5046	51	75	22	2018-04-14 20:56:28.899086	2018-04-14 20:56:28.899086
5047	51	75	29	2018-04-14 20:56:28.903576	2018-04-14 20:56:28.903576
5048	51	75	37	2018-04-14 20:56:28.908201	2018-04-14 20:56:28.908201
5049	51	75	38	2018-04-14 20:56:28.912838	2018-04-14 20:56:28.912838
5050	51	75	45	2018-04-14 20:56:28.917711	2018-04-14 20:56:28.917711
5051	51	75	53	2018-04-14 20:56:28.92194	2018-04-14 20:56:28.92194
5052	51	75	54	2018-04-14 20:56:28.927944	2018-04-14 20:56:28.927944
5053	51	75	55	2018-04-14 20:56:28.933455	2018-04-14 20:56:28.933455
5054	51	75	56	2018-04-14 20:56:28.93961	2018-04-14 20:56:28.93961
5055	51	75	57	2018-04-14 20:56:28.944733	2018-04-14 20:56:28.944733
5056	51	75	58	2018-04-14 20:56:28.949324	2018-04-14 20:56:28.949324
5057	51	75	65	2018-04-14 20:56:28.953869	2018-04-14 20:56:28.953869
5058	51	75	68	2018-04-14 20:56:28.958249	2018-04-14 20:56:28.958249
5059	51	75	69	2018-04-14 20:56:28.962908	2018-04-14 20:56:28.962908
5060	51	75	71	2018-04-14 20:56:28.967649	2018-04-14 20:56:28.967649
5061	51	75	81	2018-04-14 20:56:28.972315	2018-04-14 20:56:28.972315
5062	51	75	87	2018-04-14 20:56:28.976798	2018-04-14 20:56:28.976798
5063	51	75	92	2018-04-14 20:56:28.981502	2018-04-14 20:56:28.981502
5064	51	75	94	2018-04-14 20:56:28.986148	2018-04-14 20:56:28.986148
5065	51	75	99	2018-04-14 20:56:28.990576	2018-04-14 20:56:28.990576
5066	51	75	100	2018-04-14 20:56:28.995137	2018-04-14 20:56:28.995137
5067	51	75	101	2018-04-14 20:56:28.999833	2018-04-14 20:56:28.999833
5068	51	75	106	2018-04-14 20:56:29.004404	2018-04-14 20:56:29.004404
5069	51	75	107	2018-04-14 20:56:29.011351	2018-04-14 20:56:29.011351
5070	51	75	109	2018-04-14 20:56:29.017026	2018-04-14 20:56:29.017026
5071	51	75	110	2018-04-14 20:56:29.021522	2018-04-14 20:56:29.021522
5072	51	75	111	2018-04-14 20:56:29.025842	2018-04-14 20:56:29.025842
5073	51	76	3	2018-04-14 20:56:29.03029	2018-04-14 20:56:29.03029
5074	51	76	6	2018-04-14 20:56:29.035368	2018-04-14 20:56:29.035368
5075	51	76	12	2018-04-14 20:56:29.039936	2018-04-14 20:56:29.039936
5076	51	76	18	2018-04-14 20:56:29.044128	2018-04-14 20:56:29.044128
5077	51	76	20	2018-04-14 20:56:29.048602	2018-04-14 20:56:29.048602
5078	51	76	22	2018-04-14 20:56:29.053171	2018-04-14 20:56:29.053171
5079	51	76	29	2018-04-14 20:56:29.057865	2018-04-14 20:56:29.057865
5080	51	76	32	2018-04-14 20:56:29.062308	2018-04-14 20:56:29.062308
5081	51	76	37	2018-04-14 20:56:29.067558	2018-04-14 20:56:29.067558
5082	51	76	38	2018-04-14 20:56:29.072327	2018-04-14 20:56:29.072327
5083	51	76	45	2018-04-14 20:56:29.076935	2018-04-14 20:56:29.076935
5084	51	76	53	2018-04-14 20:56:29.081524	2018-04-14 20:56:29.081524
5085	51	76	54	2018-04-14 20:56:29.086349	2018-04-14 20:56:29.086349
5086	51	76	57	2018-04-14 20:56:29.091369	2018-04-14 20:56:29.091369
5087	51	76	58	2018-04-14 20:56:29.097031	2018-04-14 20:56:29.097031
5088	51	76	69	2018-04-14 20:56:29.102677	2018-04-14 20:56:29.102677
5089	51	76	71	2018-04-14 20:56:29.108594	2018-04-14 20:56:29.108594
5090	51	76	81	2018-04-14 20:56:29.113315	2018-04-14 20:56:29.113315
5091	51	76	87	2018-04-14 20:56:29.118053	2018-04-14 20:56:29.118053
5092	51	76	92	2018-04-14 20:56:29.123058	2018-04-14 20:56:29.123058
5093	51	76	94	2018-04-14 20:56:29.128903	2018-04-14 20:56:29.128903
5094	51	76	99	2018-04-14 20:56:29.133445	2018-04-14 20:56:29.133445
5095	51	76	101	2018-04-14 20:56:29.13788	2018-04-14 20:56:29.13788
5096	51	76	106	2018-04-14 20:56:29.142489	2018-04-14 20:56:29.142489
5097	51	76	107	2018-04-14 20:56:29.147067	2018-04-14 20:56:29.147067
5098	51	76	109	2018-04-14 20:56:29.151732	2018-04-14 20:56:29.151732
5099	51	76	110	2018-04-14 20:56:29.156235	2018-04-14 20:56:29.156235
5100	51	76	111	2018-04-14 20:56:29.160592	2018-04-14 20:56:29.160592
5101	51	78	3	2018-04-14 20:56:29.165244	2018-04-14 20:56:29.165244
5102	51	78	12	2018-04-14 20:56:29.170103	2018-04-14 20:56:29.170103
5103	51	78	18	2018-04-14 20:56:29.174578	2018-04-14 20:56:29.174578
5104	51	78	20	2018-04-14 20:56:29.179212	2018-04-14 20:56:29.179212
5105	51	78	29	2018-04-14 20:56:29.183826	2018-04-14 20:56:29.183826
5106	51	78	37	2018-04-14 20:56:29.188716	2018-04-14 20:56:29.188716
5107	51	78	45	2018-04-14 20:56:29.193458	2018-04-14 20:56:29.193458
5108	51	78	53	2018-04-14 20:56:29.198066	2018-04-14 20:56:29.198066
5109	51	78	54	2018-04-14 20:56:29.203097	2018-04-14 20:56:29.203097
5110	51	78	55	2018-04-14 20:56:29.208987	2018-04-14 20:56:29.208987
5111	51	78	56	2018-04-14 20:56:29.213402	2018-04-14 20:56:29.213402
5112	51	78	57	2018-04-14 20:56:29.218198	2018-04-14 20:56:29.218198
5113	51	78	58	2018-04-14 20:56:29.222719	2018-04-14 20:56:29.222719
5114	51	78	65	2018-04-14 20:56:29.227176	2018-04-14 20:56:29.227176
5115	51	78	69	2018-04-14 20:56:29.231832	2018-04-14 20:56:29.231832
5116	51	78	81	2018-04-14 20:56:29.23648	2018-04-14 20:56:29.23648
5117	51	78	87	2018-04-14 20:56:29.241231	2018-04-14 20:56:29.241231
5118	51	78	92	2018-04-14 20:56:29.245851	2018-04-14 20:56:29.245851
5119	51	78	94	2018-04-14 20:56:29.250132	2018-04-14 20:56:29.250132
5120	51	78	99	2018-04-14 20:56:29.254667	2018-04-14 20:56:29.254667
5121	51	78	101	2018-04-14 20:56:29.259443	2018-04-14 20:56:29.259443
5122	51	78	107	2018-04-14 20:56:29.264271	2018-04-14 20:56:29.264271
5123	51	78	109	2018-04-14 20:56:29.269052	2018-04-14 20:56:29.269052
5124	51	79	3	2018-04-14 20:56:29.273629	2018-04-14 20:56:29.273629
5125	51	79	6	2018-04-14 20:56:29.278184	2018-04-14 20:56:29.278184
5126	51	79	11	2018-04-14 20:56:29.283	2018-04-14 20:56:29.283
5127	51	79	12	2018-04-14 20:56:29.29087	2018-04-14 20:56:29.29087
5128	51	79	15	2018-04-14 20:56:29.298582	2018-04-14 20:56:29.298582
5129	51	79	16	2018-04-14 20:56:29.305894	2018-04-14 20:56:29.305894
5130	51	79	17	2018-04-14 20:56:29.310533	2018-04-14 20:56:29.310533
5131	51	79	18	2018-04-14 20:56:29.315756	2018-04-14 20:56:29.315756
5132	51	79	20	2018-04-14 20:56:29.320718	2018-04-14 20:56:29.320718
5133	51	79	22	2018-04-14 20:56:29.325329	2018-04-14 20:56:29.325329
5134	51	79	24	2018-04-14 20:56:29.329753	2018-04-14 20:56:29.329753
5135	51	79	29	2018-04-14 20:56:29.334707	2018-04-14 20:56:29.334707
5136	51	79	31	2018-04-14 20:56:29.339422	2018-04-14 20:56:29.339422
5137	51	79	32	2018-04-14 20:56:29.344048	2018-04-14 20:56:29.344048
5138	51	79	37	2018-04-14 20:56:29.34886	2018-04-14 20:56:29.34886
5139	51	79	38	2018-04-14 20:56:29.353293	2018-04-14 20:56:29.353293
5140	51	79	39	2018-04-14 20:56:29.3578	2018-04-14 20:56:29.3578
5141	51	79	41	2018-04-14 20:56:29.362085	2018-04-14 20:56:29.362085
5142	51	79	42	2018-04-14 20:56:29.366629	2018-04-14 20:56:29.366629
5143	51	79	45	2018-04-14 20:56:29.371385	2018-04-14 20:56:29.371385
5144	51	79	53	2018-04-14 20:56:29.375936	2018-04-14 20:56:29.375936
5145	51	79	54	2018-04-14 20:56:29.380398	2018-04-14 20:56:29.380398
5146	51	79	55	2018-04-14 20:56:29.384792	2018-04-14 20:56:29.384792
5147	51	79	56	2018-04-14 20:56:29.389456	2018-04-14 20:56:29.389456
5148	51	79	57	2018-04-14 20:56:29.393992	2018-04-14 20:56:29.393992
5149	51	79	58	2018-04-14 20:56:29.398383	2018-04-14 20:56:29.398383
5150	51	79	61	2018-04-14 20:56:29.403326	2018-04-14 20:56:29.403326
5151	51	79	69	2018-04-14 20:56:29.407895	2018-04-14 20:56:29.407895
5152	51	79	71	2018-04-14 20:56:29.412453	2018-04-14 20:56:29.412453
5153	51	79	72	2018-04-14 20:56:29.416867	2018-04-14 20:56:29.416867
5154	51	79	74	2018-04-14 20:56:29.421505	2018-04-14 20:56:29.421505
5155	51	79	78	2018-04-14 20:56:29.426226	2018-04-14 20:56:29.426226
5156	51	79	81	2018-04-14 20:56:29.430836	2018-04-14 20:56:29.430836
5157	51	79	82	2018-04-14 20:56:29.437249	2018-04-14 20:56:29.437249
5158	51	79	83	2018-04-14 20:56:29.441885	2018-04-14 20:56:29.441885
5159	51	79	85	2018-04-14 20:56:29.446835	2018-04-14 20:56:29.446835
5160	51	79	86	2018-04-14 20:56:29.451632	2018-04-14 20:56:29.451632
5161	51	79	87	2018-04-14 20:56:29.456211	2018-04-14 20:56:29.456211
5162	51	79	88	2018-04-14 20:56:29.46072	2018-04-14 20:56:29.46072
5163	51	79	89	2018-04-14 20:56:29.465714	2018-04-14 20:56:29.465714
5164	51	79	91	2018-04-14 20:56:29.47047	2018-04-14 20:56:29.47047
5165	51	79	92	2018-04-14 20:56:29.475292	2018-04-14 20:56:29.475292
5166	51	79	94	2018-04-14 20:56:29.480433	2018-04-14 20:56:29.480433
5167	51	79	95	2018-04-14 20:56:29.4856	2018-04-14 20:56:29.4856
5168	51	79	99	2018-04-14 20:56:29.490323	2018-04-14 20:56:29.490323
5169	51	79	100	2018-04-14 20:56:29.495206	2018-04-14 20:56:29.495206
5170	51	79	101	2018-04-14 20:56:29.500337	2018-04-14 20:56:29.500337
5171	51	79	102	2018-04-14 20:56:29.508161	2018-04-14 20:56:29.508161
5172	51	79	106	2018-04-14 20:56:29.513856	2018-04-14 20:56:29.513856
5173	51	79	107	2018-04-14 20:56:29.518374	2018-04-14 20:56:29.518374
5174	51	79	109	2018-04-14 20:56:29.523513	2018-04-14 20:56:29.523513
5175	51	79	110	2018-04-14 20:56:29.54383	2018-04-14 20:56:29.54383
5176	51	79	111	2018-04-14 20:56:29.551385	2018-04-14 20:56:29.551385
5177	51	90	3	2018-04-14 20:56:29.556152	2018-04-14 20:56:29.556152
5178	51	90	6	2018-04-14 20:56:29.560601	2018-04-14 20:56:29.560601
5179	51	90	12	2018-04-14 20:56:29.565129	2018-04-14 20:56:29.565129
5180	51	90	18	2018-04-14 20:56:29.569682	2018-04-14 20:56:29.569682
5181	51	90	20	2018-04-14 20:56:29.573981	2018-04-14 20:56:29.573981
5182	51	90	22	2018-04-14 20:56:29.578766	2018-04-14 20:56:29.578766
5183	51	90	29	2018-04-14 20:56:29.583539	2018-04-14 20:56:29.583539
5184	51	90	37	2018-04-14 20:56:29.588156	2018-04-14 20:56:29.588156
5185	51	90	38	2018-04-14 20:56:29.592889	2018-04-14 20:56:29.592889
5186	51	90	45	2018-04-14 20:56:29.597466	2018-04-14 20:56:29.597466
5187	51	90	53	2018-04-14 20:56:29.602089	2018-04-14 20:56:29.602089
5188	51	90	54	2018-04-14 20:56:29.606445	2018-04-14 20:56:29.606445
5189	51	90	55	2018-04-14 20:56:29.611251	2018-04-14 20:56:29.611251
5190	51	90	56	2018-04-14 20:56:29.61625	2018-04-14 20:56:29.61625
5191	51	90	57	2018-04-14 20:56:29.623768	2018-04-14 20:56:29.623768
5192	51	90	58	2018-04-14 20:56:29.628776	2018-04-14 20:56:29.628776
5193	51	90	69	2018-04-14 20:56:29.633328	2018-04-14 20:56:29.633328
5194	51	90	71	2018-04-14 20:56:29.637641	2018-04-14 20:56:29.637641
5195	51	90	81	2018-04-14 20:56:29.642107	2018-04-14 20:56:29.642107
5196	51	90	87	2018-04-14 20:56:29.646405	2018-04-14 20:56:29.646405
5197	51	90	92	2018-04-14 20:56:29.650759	2018-04-14 20:56:29.650759
5198	51	90	94	2018-04-14 20:56:29.655149	2018-04-14 20:56:29.655149
5199	51	90	99	2018-04-14 20:56:29.659712	2018-04-14 20:56:29.659712
5200	51	90	101	2018-04-14 20:56:29.664023	2018-04-14 20:56:29.664023
5201	51	90	106	2018-04-14 20:56:29.668835	2018-04-14 20:56:29.668835
5202	51	90	107	2018-04-14 20:56:29.673803	2018-04-14 20:56:29.673803
5203	51	90	109	2018-04-14 20:56:29.679578	2018-04-14 20:56:29.679578
5204	51	90	110	2018-04-14 20:56:29.684419	2018-04-14 20:56:29.684419
5205	51	90	111	2018-04-14 20:56:29.689179	2018-04-14 20:56:29.689179
5206	51	91	3	2018-04-14 20:56:29.693882	2018-04-14 20:56:29.693882
5207	51	91	12	2018-04-14 20:56:29.698867	2018-04-14 20:56:29.698867
5208	51	91	18	2018-04-14 20:56:29.703433	2018-04-14 20:56:29.703433
5209	51	91	20	2018-04-14 20:56:29.707931	2018-04-14 20:56:29.707931
5210	51	91	29	2018-04-14 20:56:29.712327	2018-04-14 20:56:29.712327
5211	51	91	37	2018-04-14 20:56:29.717181	2018-04-14 20:56:29.717181
5212	51	91	45	2018-04-14 20:56:29.721895	2018-04-14 20:56:29.721895
5213	51	91	53	2018-04-14 20:56:29.726305	2018-04-14 20:56:29.726305
5214	51	91	54	2018-04-14 20:56:29.730883	2018-04-14 20:56:29.730883
5215	51	91	55	2018-04-14 20:56:29.735329	2018-04-14 20:56:29.735329
5216	51	91	56	2018-04-14 20:56:29.740118	2018-04-14 20:56:29.740118
5217	51	91	57	2018-04-14 20:56:29.744639	2018-04-14 20:56:29.744639
5218	51	91	58	2018-04-14 20:56:29.749463	2018-04-14 20:56:29.749463
5219	51	91	65	2018-04-14 20:56:29.753819	2018-04-14 20:56:29.753819
5220	51	91	69	2018-04-14 20:56:29.758429	2018-04-14 20:56:29.758429
5221	51	91	81	2018-04-14 20:56:29.763516	2018-04-14 20:56:29.763516
5222	51	91	87	2018-04-14 20:56:29.76828	2018-04-14 20:56:29.76828
5223	51	91	92	2018-04-14 20:56:29.772692	2018-04-14 20:56:29.772692
5224	51	91	94	2018-04-14 20:56:29.777293	2018-04-14 20:56:29.777293
5225	51	91	99	2018-04-14 20:56:29.781641	2018-04-14 20:56:29.781641
5226	51	91	101	2018-04-14 20:56:29.786225	2018-04-14 20:56:29.786225
5227	51	91	107	2018-04-14 20:56:29.7909	2018-04-14 20:56:29.7909
5228	51	91	109	2018-04-14 20:56:29.795227	2018-04-14 20:56:29.795227
5229	51	93	3	2018-04-14 20:56:29.799727	2018-04-14 20:56:29.799727
5230	51	93	6	2018-04-14 20:56:29.804207	2018-04-14 20:56:29.804207
5231	51	93	12	2018-04-14 20:56:29.808615	2018-04-14 20:56:29.808615
5232	51	93	18	2018-04-14 20:56:29.81341	2018-04-14 20:56:29.81341
5233	51	93	20	2018-04-14 20:56:29.818109	2018-04-14 20:56:29.818109
5234	51	93	22	2018-04-14 20:56:29.822477	2018-04-14 20:56:29.822477
5235	51	93	29	2018-04-14 20:56:29.827083	2018-04-14 20:56:29.827083
5236	51	93	32	2018-04-14 20:56:29.831528	2018-04-14 20:56:29.831528
5237	51	93	37	2018-04-14 20:56:29.83921	2018-04-14 20:56:29.83921
5238	51	93	38	2018-04-14 20:56:29.845649	2018-04-14 20:56:29.845649
5239	51	93	45	2018-04-14 20:56:29.850828	2018-04-14 20:56:29.850828
5240	51	93	53	2018-04-14 20:56:29.855248	2018-04-14 20:56:29.855248
5241	51	93	54	2018-04-14 20:56:29.859771	2018-04-14 20:56:29.859771
5242	51	93	57	2018-04-14 20:56:29.864078	2018-04-14 20:56:29.864078
5243	51	93	58	2018-04-14 20:56:29.868549	2018-04-14 20:56:29.868549
5244	51	93	69	2018-04-14 20:56:29.873164	2018-04-14 20:56:29.873164
5245	51	93	71	2018-04-14 20:56:29.877846	2018-04-14 20:56:29.877846
5246	51	93	81	2018-04-14 20:56:29.88244	2018-04-14 20:56:29.88244
5247	51	93	87	2018-04-14 20:56:29.887059	2018-04-14 20:56:29.887059
5248	51	93	92	2018-04-14 20:56:29.892208	2018-04-14 20:56:29.892208
5249	51	93	94	2018-04-14 20:56:29.898043	2018-04-14 20:56:29.898043
5250	51	93	99	2018-04-14 20:56:29.905601	2018-04-14 20:56:29.905601
5251	51	93	101	2018-04-14 20:56:29.912301	2018-04-14 20:56:29.912301
5252	51	93	106	2018-04-14 20:56:29.917768	2018-04-14 20:56:29.917768
5253	51	93	107	2018-04-14 20:56:29.922281	2018-04-14 20:56:29.922281
5254	51	93	109	2018-04-14 20:56:29.926527	2018-04-14 20:56:29.926527
5255	51	93	110	2018-04-14 20:56:29.933226	2018-04-14 20:56:29.933226
5256	51	93	111	2018-04-14 20:56:29.938753	2018-04-14 20:56:29.938753
5257	51	95	3	2018-04-14 20:56:29.943882	2018-04-14 20:56:29.943882
5258	51	95	12	2018-04-14 20:56:29.94901	2018-04-14 20:56:29.94901
5259	51	95	18	2018-04-14 20:56:29.955189	2018-04-14 20:56:29.955189
5260	51	95	20	2018-04-14 20:56:29.959751	2018-04-14 20:56:29.959751
5261	51	95	29	2018-04-14 20:56:29.964563	2018-04-14 20:56:29.964563
5262	51	95	37	2018-04-14 20:56:29.969202	2018-04-14 20:56:29.969202
5263	51	95	45	2018-04-14 20:56:29.973531	2018-04-14 20:56:29.973531
5264	51	95	53	2018-04-14 20:56:29.97808	2018-04-14 20:56:29.97808
5265	51	95	54	2018-04-14 20:56:29.982922	2018-04-14 20:56:29.982922
5266	51	95	55	2018-04-14 20:56:29.990503	2018-04-14 20:56:29.990503
5267	51	95	56	2018-04-14 20:56:29.995139	2018-04-14 20:56:29.995139
5268	51	95	57	2018-04-14 20:56:30.000267	2018-04-14 20:56:30.000267
5269	51	95	58	2018-04-14 20:56:30.005221	2018-04-14 20:56:30.005221
5270	51	95	65	2018-04-14 20:56:30.010111	2018-04-14 20:56:30.010111
5271	51	95	69	2018-04-14 20:56:30.01478	2018-04-14 20:56:30.01478
5272	51	95	81	2018-04-14 20:56:30.019494	2018-04-14 20:56:30.019494
5273	51	95	87	2018-04-14 20:56:30.023945	2018-04-14 20:56:30.023945
5274	51	95	92	2018-04-14 20:56:30.028306	2018-04-14 20:56:30.028306
5275	51	95	94	2018-04-14 20:56:30.032552	2018-04-14 20:56:30.032552
5276	51	95	99	2018-04-14 20:56:30.037107	2018-04-14 20:56:30.037107
5277	51	95	101	2018-04-14 20:56:30.041468	2018-04-14 20:56:30.041468
5278	51	95	107	2018-04-14 20:56:30.045837	2018-04-14 20:56:30.045837
5279	51	95	109	2018-04-14 20:56:30.050272	2018-04-14 20:56:30.050272
5280	51	102	3	2018-04-14 20:56:30.054793	2018-04-14 20:56:30.054793
5281	51	102	11	2018-04-14 20:56:30.059642	2018-04-14 20:56:30.059642
5282	51	102	12	2018-04-14 20:56:30.064437	2018-04-14 20:56:30.064437
5283	51	102	15	2018-04-14 20:56:30.069164	2018-04-14 20:56:30.069164
5284	51	102	18	2018-04-14 20:56:30.073793	2018-04-14 20:56:30.073793
5285	51	102	20	2018-04-14 20:56:30.078482	2018-04-14 20:56:30.078482
5286	51	102	24	2018-04-14 20:56:30.083018	2018-04-14 20:56:30.083018
5287	51	102	29	2018-04-14 20:56:30.087836	2018-04-14 20:56:30.087836
5288	51	102	31	2018-04-14 20:56:30.09219	2018-04-14 20:56:30.09219
5289	51	102	37	2018-04-14 20:56:30.096614	2018-04-14 20:56:30.096614
5290	51	102	39	2018-04-14 20:56:30.101352	2018-04-14 20:56:30.101352
5291	51	102	41	2018-04-14 20:56:30.107271	2018-04-14 20:56:30.107271
5292	51	102	45	2018-04-14 20:56:30.112576	2018-04-14 20:56:30.112576
5293	51	102	53	2018-04-14 20:56:30.117289	2018-04-14 20:56:30.117289
5294	51	102	54	2018-04-14 20:56:30.121995	2018-04-14 20:56:30.121995
5295	51	102	57	2018-04-14 20:56:30.126549	2018-04-14 20:56:30.126549
5296	51	102	58	2018-04-14 20:56:30.132007	2018-04-14 20:56:30.132007
5297	51	102	61	2018-04-14 20:56:30.139367	2018-04-14 20:56:30.139367
5298	51	102	69	2018-04-14 20:56:30.144725	2018-04-14 20:56:30.144725
5299	51	102	72	2018-04-14 20:56:30.14913	2018-04-14 20:56:30.14913
5300	51	102	74	2018-04-14 20:56:30.153485	2018-04-14 20:56:30.153485
5301	51	102	78	2018-04-14 20:56:30.158222	2018-04-14 20:56:30.158222
5302	51	102	81	2018-04-14 20:56:30.162823	2018-04-14 20:56:30.162823
5303	51	102	82	2018-04-14 20:56:30.16721	2018-04-14 20:56:30.16721
5304	51	102	83	2018-04-14 20:56:30.171779	2018-04-14 20:56:30.171779
5305	51	102	85	2018-04-14 20:56:30.176238	2018-04-14 20:56:30.176238
5306	51	102	86	2018-04-14 20:56:30.18069	2018-04-14 20:56:30.18069
5307	51	102	87	2018-04-14 20:56:30.185075	2018-04-14 20:56:30.185075
5308	51	102	88	2018-04-14 20:56:30.189595	2018-04-14 20:56:30.189595
5309	51	102	89	2018-04-14 20:56:30.194203	2018-04-14 20:56:30.194203
5310	51	102	91	2018-04-14 20:56:30.198789	2018-04-14 20:56:30.198789
5311	51	102	92	2018-04-14 20:56:30.203337	2018-04-14 20:56:30.203337
5312	51	102	94	2018-04-14 20:56:30.20775	2018-04-14 20:56:30.20775
5313	51	102	95	2018-04-14 20:56:30.211972	2018-04-14 20:56:30.211972
5314	51	102	99	2018-04-14 20:56:30.216686	2018-04-14 20:56:30.216686
5315	51	102	100	2018-04-14 20:56:30.221499	2018-04-14 20:56:30.221499
5316	51	102	102	2018-04-14 20:56:30.227264	2018-04-14 20:56:30.227264
5317	51	102	109	2018-04-14 20:56:30.234553	2018-04-14 20:56:30.234553
5318	51	104	68	2018-04-14 20:56:30.241598	2018-04-14 20:56:30.241598
5319	51	112	31	2018-04-14 20:56:30.246516	2018-04-14 20:56:30.246516
5320	51	112	32	2018-04-14 20:56:30.251683	2018-04-14 20:56:30.251683
5321	51	112	68	2018-04-14 20:56:30.256365	2018-04-14 20:56:30.256365
5322	52	9	48	2018-04-14 20:56:30.26107	2018-04-14 20:56:30.26107
5323	52	9	52	2018-04-14 20:56:30.265602	2018-04-14 20:56:30.265602
5324	52	9	98	2018-04-14 20:56:30.270442	2018-04-14 20:56:30.270442
5325	52	10	16	2018-04-14 20:56:30.275185	2018-04-14 20:56:30.275185
5326	52	10	17	2018-04-14 20:56:30.279807	2018-04-14 20:56:30.279807
5327	52	10	48	2018-04-14 20:56:30.284485	2018-04-14 20:56:30.284485
5328	52	10	52	2018-04-14 20:56:30.289012	2018-04-14 20:56:30.289012
5329	52	10	98	2018-04-14 20:56:30.293396	2018-04-14 20:56:30.293396
5330	52	13	48	2018-04-14 20:56:30.299135	2018-04-14 20:56:30.299135
5331	52	13	52	2018-04-14 20:56:30.304628	2018-04-14 20:56:30.304628
5332	52	13	98	2018-04-14 20:56:30.309386	2018-04-14 20:56:30.309386
5333	52	16	3	2018-04-14 20:56:30.317233	2018-04-14 20:56:30.317233
5334	52	16	6	2018-04-14 20:56:30.322475	2018-04-14 20:56:30.322475
5335	52	16	12	2018-04-14 20:56:30.326813	2018-04-14 20:56:30.326813
5336	52	16	16	2018-04-14 20:56:30.331232	2018-04-14 20:56:30.331232
5337	52	16	17	2018-04-14 20:56:30.336031	2018-04-14 20:56:30.336031
5338	52	16	18	2018-04-14 20:56:30.340752	2018-04-14 20:56:30.340752
5339	52	16	20	2018-04-14 20:56:30.345212	2018-04-14 20:56:30.345212
5340	52	16	22	2018-04-14 20:56:30.350424	2018-04-14 20:56:30.350424
5341	52	16	29	2018-04-14 20:56:30.357221	2018-04-14 20:56:30.357221
5342	52	16	37	2018-04-14 20:56:30.362266	2018-04-14 20:56:30.362266
5343	52	16	38	2018-04-14 20:56:30.367126	2018-04-14 20:56:30.367126
5344	52	16	41	2018-04-14 20:56:30.371739	2018-04-14 20:56:30.371739
5345	52	16	45	2018-04-14 20:56:30.377847	2018-04-14 20:56:30.377847
5346	52	16	46	2018-04-14 20:56:30.382512	2018-04-14 20:56:30.382512
5347	52	16	48	2018-04-14 20:56:30.388539	2018-04-14 20:56:30.388539
5348	52	16	52	2018-04-14 20:56:30.393737	2018-04-14 20:56:30.393737
5349	52	16	53	2018-04-14 20:56:30.399309	2018-04-14 20:56:30.399309
5350	52	16	54	2018-04-14 20:56:30.404224	2018-04-14 20:56:30.404224
5351	52	16	55	2018-04-14 20:56:30.408744	2018-04-14 20:56:30.408744
5352	52	16	56	2018-04-14 20:56:30.415076	2018-04-14 20:56:30.415076
5353	52	16	57	2018-04-14 20:56:30.420051	2018-04-14 20:56:30.420051
5354	52	16	58	2018-04-14 20:56:30.425022	2018-04-14 20:56:30.425022
5355	52	16	61	2018-04-14 20:56:30.43002	2018-04-14 20:56:30.43002
5356	52	16	64	2018-04-14 20:56:30.434795	2018-04-14 20:56:30.434795
5357	52	16	65	2018-04-14 20:56:30.439672	2018-04-14 20:56:30.439672
5358	52	16	69	2018-04-14 20:56:30.4442	2018-04-14 20:56:30.4442
5359	52	16	70	2018-04-14 20:56:30.448783	2018-04-14 20:56:30.448783
5360	52	16	71	2018-04-14 20:56:30.453222	2018-04-14 20:56:30.453222
5361	52	16	78	2018-04-14 20:56:30.457878	2018-04-14 20:56:30.457878
5362	52	16	80	2018-04-14 20:56:30.462558	2018-04-14 20:56:30.462558
5363	52	16	81	2018-04-14 20:56:30.467347	2018-04-14 20:56:30.467347
5364	52	16	84	2018-04-14 20:56:30.471946	2018-04-14 20:56:30.471946
5365	52	16	87	2018-04-14 20:56:30.476614	2018-04-14 20:56:30.476614
5366	52	16	91	2018-04-14 20:56:30.481709	2018-04-14 20:56:30.481709
5367	52	16	92	2018-04-14 20:56:30.486562	2018-04-14 20:56:30.486562
5368	52	16	94	2018-04-14 20:56:30.491058	2018-04-14 20:56:30.491058
5369	52	16	95	2018-04-14 20:56:30.495833	2018-04-14 20:56:30.495833
5370	52	16	98	2018-04-14 20:56:30.500344	2018-04-14 20:56:30.500344
5371	52	16	99	2018-04-14 20:56:30.505158	2018-04-14 20:56:30.505158
5372	52	16	101	2018-04-14 20:56:30.510351	2018-04-14 20:56:30.510351
5373	52	16	103	2018-04-14 20:56:30.515598	2018-04-14 20:56:30.515598
5374	52	16	106	2018-04-14 20:56:30.520004	2018-04-14 20:56:30.520004
5375	52	16	107	2018-04-14 20:56:30.524815	2018-04-14 20:56:30.524815
5376	52	16	109	2018-04-14 20:56:30.529932	2018-04-14 20:56:30.529932
5377	52	16	110	2018-04-14 20:56:30.534532	2018-04-14 20:56:30.534532
5378	52	16	111	2018-04-14 20:56:30.539286	2018-04-14 20:56:30.539286
5379	52	17	3	2018-04-14 20:56:30.54374	2018-04-14 20:56:30.54374
5380	52	17	12	2018-04-14 20:56:30.548735	2018-04-14 20:56:30.548735
5381	52	17	17	2018-04-14 20:56:30.553763	2018-04-14 20:56:30.553763
5382	52	17	18	2018-04-14 20:56:30.558403	2018-04-14 20:56:30.558403
5383	52	17	20	2018-04-14 20:56:30.563249	2018-04-14 20:56:30.563249
5384	52	17	29	2018-04-14 20:56:30.568254	2018-04-14 20:56:30.568254
5385	52	17	37	2018-04-14 20:56:30.573856	2018-04-14 20:56:30.573856
5386	52	17	41	2018-04-14 20:56:30.578429	2018-04-14 20:56:30.578429
5387	52	17	45	2018-04-14 20:56:30.583256	2018-04-14 20:56:30.583256
5388	52	17	46	2018-04-14 20:56:30.588507	2018-04-14 20:56:30.588507
5389	52	17	48	2018-04-14 20:56:30.599692	2018-04-14 20:56:30.599692
5390	52	17	52	2018-04-14 20:56:30.610875	2018-04-14 20:56:30.610875
5391	52	17	53	2018-04-14 20:56:30.615669	2018-04-14 20:56:30.615669
5392	52	17	54	2018-04-14 20:56:30.620123	2018-04-14 20:56:30.620123
5393	52	17	55	2018-04-14 20:56:30.624687	2018-04-14 20:56:30.624687
5394	52	17	56	2018-04-14 20:56:30.629258	2018-04-14 20:56:30.629258
5395	52	17	57	2018-04-14 20:56:30.633934	2018-04-14 20:56:30.633934
5396	52	17	58	2018-04-14 20:56:30.638442	2018-04-14 20:56:30.638442
5397	52	17	61	2018-04-14 20:56:30.643335	2018-04-14 20:56:30.643335
5398	52	17	64	2018-04-14 20:56:30.64805	2018-04-14 20:56:30.64805
5399	52	17	65	2018-04-14 20:56:30.652465	2018-04-14 20:56:30.652465
5400	52	17	69	2018-04-14 20:56:30.656987	2018-04-14 20:56:30.656987
5401	52	17	70	2018-04-14 20:56:30.661629	2018-04-14 20:56:30.661629
5402	52	17	78	2018-04-14 20:56:30.667956	2018-04-14 20:56:30.667956
5403	52	17	80	2018-04-14 20:56:30.673916	2018-04-14 20:56:30.673916
5404	52	17	81	2018-04-14 20:56:30.681299	2018-04-14 20:56:30.681299
5405	52	17	84	2018-04-14 20:56:30.685935	2018-04-14 20:56:30.685935
5406	52	17	87	2018-04-14 20:56:30.690555	2018-04-14 20:56:30.690555
5407	52	17	91	2018-04-14 20:56:30.695222	2018-04-14 20:56:30.695222
5408	52	17	92	2018-04-14 20:56:30.700449	2018-04-14 20:56:30.700449
5409	52	17	94	2018-04-14 20:56:30.708728	2018-04-14 20:56:30.708728
5410	52	17	95	2018-04-14 20:56:30.716338	2018-04-14 20:56:30.716338
5411	52	17	98	2018-04-14 20:56:30.722023	2018-04-14 20:56:30.722023
5412	52	17	99	2018-04-14 20:56:30.72647	2018-04-14 20:56:30.72647
5413	52	17	101	2018-04-14 20:56:30.731119	2018-04-14 20:56:30.731119
5414	52	17	103	2018-04-14 20:56:30.735718	2018-04-14 20:56:30.735718
5415	52	17	107	2018-04-14 20:56:30.740022	2018-04-14 20:56:30.740022
5416	52	17	109	2018-04-14 20:56:30.744478	2018-04-14 20:56:30.744478
5417	52	23	48	2018-04-14 20:56:30.750175	2018-04-14 20:56:30.750175
5418	52	23	52	2018-04-14 20:56:30.755824	2018-04-14 20:56:30.755824
5419	52	23	98	2018-04-14 20:56:30.76132	2018-04-14 20:56:30.76132
5420	52	33	48	2018-04-14 20:56:30.768675	2018-04-14 20:56:30.768675
5421	52	33	52	2018-04-14 20:56:30.773945	2018-04-14 20:56:30.773945
5422	52	33	98	2018-04-14 20:56:30.781954	2018-04-14 20:56:30.781954
5423	52	36	41	2018-04-14 20:56:30.789413	2018-04-14 20:56:30.789413
5424	52	36	48	2018-04-14 20:56:30.795555	2018-04-14 20:56:30.795555
5425	52	36	52	2018-04-14 20:56:30.802352	2018-04-14 20:56:30.802352
5426	52	36	98	2018-04-14 20:56:30.807094	2018-04-14 20:56:30.807094
5427	52	40	41	2018-04-14 20:56:30.811909	2018-04-14 20:56:30.811909
5428	52	41	103	2018-04-14 20:56:30.816542	2018-04-14 20:56:30.816542
5429	52	47	48	2018-04-14 20:56:30.821353	2018-04-14 20:56:30.821353
5430	52	47	52	2018-04-14 20:56:30.827457	2018-04-14 20:56:30.827457
5431	52	47	98	2018-04-14 20:56:30.835379	2018-04-14 20:56:30.835379
5432	52	48	3	2018-04-14 20:56:30.840605	2018-04-14 20:56:30.840605
5433	52	48	12	2018-04-14 20:56:30.845212	2018-04-14 20:56:30.845212
5434	52	48	18	2018-04-14 20:56:30.850051	2018-04-14 20:56:30.850051
5435	52	48	20	2018-04-14 20:56:30.854842	2018-04-14 20:56:30.854842
5436	52	48	29	2018-04-14 20:56:30.859858	2018-04-14 20:56:30.859858
5437	52	48	37	2018-04-14 20:56:30.864619	2018-04-14 20:56:30.864619
5438	52	48	45	2018-04-14 20:56:30.869401	2018-04-14 20:56:30.869401
5439	52	48	48	2018-04-14 20:56:30.874105	2018-04-14 20:56:30.874105
5440	52	48	53	2018-04-14 20:56:30.8789	2018-04-14 20:56:30.8789
5441	52	48	54	2018-04-14 20:56:30.883787	2018-04-14 20:56:30.883787
5442	52	48	55	2018-04-14 20:56:30.890299	2018-04-14 20:56:30.890299
5443	52	48	56	2018-04-14 20:56:30.895215	2018-04-14 20:56:30.895215
5444	52	48	57	2018-04-14 20:56:30.903664	2018-04-14 20:56:30.903664
5445	52	48	58	2018-04-14 20:56:30.920516	2018-04-14 20:56:30.920516
5446	52	48	65	2018-04-14 20:56:30.925462	2018-04-14 20:56:30.925462
5447	52	48	69	2018-04-14 20:56:30.93153	2018-04-14 20:56:30.93153
5448	52	48	81	2018-04-14 20:56:30.93778	2018-04-14 20:56:30.93778
5449	52	48	87	2018-04-14 20:56:30.942155	2018-04-14 20:56:30.942155
5450	52	48	92	2018-04-14 20:56:30.946915	2018-04-14 20:56:30.946915
5451	52	48	94	2018-04-14 20:56:30.951574	2018-04-14 20:56:30.951574
5452	52	48	98	2018-04-14 20:56:30.956186	2018-04-14 20:56:30.956186
5453	52	48	99	2018-04-14 20:56:30.960454	2018-04-14 20:56:30.960454
5454	52	48	101	2018-04-14 20:56:30.965371	2018-04-14 20:56:30.965371
5455	52	48	103	2018-04-14 20:56:30.969939	2018-04-14 20:56:30.969939
5456	52	48	107	2018-04-14 20:56:30.974418	2018-04-14 20:56:30.974418
5457	52	48	109	2018-04-14 20:56:30.979173	2018-04-14 20:56:30.979173
5458	52	49	48	2018-04-14 20:56:30.983595	2018-04-14 20:56:30.983595
5459	52	49	52	2018-04-14 20:56:30.988423	2018-04-14 20:56:30.988423
5460	52	49	98	2018-04-14 20:56:30.992938	2018-04-14 20:56:30.992938
5461	52	50	48	2018-04-14 20:56:30.99757	2018-04-14 20:56:30.99757
5462	52	50	52	2018-04-14 20:56:31.002079	2018-04-14 20:56:31.002079
5463	52	50	98	2018-04-14 20:56:31.006427	2018-04-14 20:56:31.006427
5464	52	52	3	2018-04-14 20:56:31.013243	2018-04-14 20:56:31.013243
5465	52	52	12	2018-04-14 20:56:31.020236	2018-04-14 20:56:31.020236
5466	52	52	18	2018-04-14 20:56:31.027105	2018-04-14 20:56:31.027105
5467	52	52	20	2018-04-14 20:56:31.033563	2018-04-14 20:56:31.033563
5468	52	52	29	2018-04-14 20:56:31.038655	2018-04-14 20:56:31.038655
5469	52	52	37	2018-04-14 20:56:31.043644	2018-04-14 20:56:31.043644
5470	52	52	45	2018-04-14 20:56:31.050575	2018-04-14 20:56:31.050575
5471	52	52	48	2018-04-14 20:56:31.057543	2018-04-14 20:56:31.057543
5472	52	52	52	2018-04-14 20:56:31.064276	2018-04-14 20:56:31.064276
5473	52	52	53	2018-04-14 20:56:31.069005	2018-04-14 20:56:31.069005
5474	52	52	54	2018-04-14 20:56:31.073292	2018-04-14 20:56:31.073292
5475	52	52	55	2018-04-14 20:56:31.077861	2018-04-14 20:56:31.077861
5476	52	52	56	2018-04-14 20:56:31.08245	2018-04-14 20:56:31.08245
5477	52	52	57	2018-04-14 20:56:31.08708	2018-04-14 20:56:31.08708
5478	52	52	58	2018-04-14 20:56:31.091482	2018-04-14 20:56:31.091482
5479	52	52	65	2018-04-14 20:56:31.096054	2018-04-14 20:56:31.096054
5480	52	52	69	2018-04-14 20:56:31.100482	2018-04-14 20:56:31.100482
5481	52	52	81	2018-04-14 20:56:31.104575	2018-04-14 20:56:31.104575
5482	52	52	87	2018-04-14 20:56:31.10928	2018-04-14 20:56:31.10928
5483	52	52	92	2018-04-14 20:56:31.114054	2018-04-14 20:56:31.114054
5484	52	52	94	2018-04-14 20:56:31.118699	2018-04-14 20:56:31.118699
5485	52	52	98	2018-04-14 20:56:31.123091	2018-04-14 20:56:31.123091
5486	52	52	99	2018-04-14 20:56:31.127454	2018-04-14 20:56:31.127454
5487	52	52	101	2018-04-14 20:56:31.132189	2018-04-14 20:56:31.132189
5488	52	52	103	2018-04-14 20:56:31.136857	2018-04-14 20:56:31.136857
5489	52	52	107	2018-04-14 20:56:31.141259	2018-04-14 20:56:31.141259
5490	52	52	109	2018-04-14 20:56:31.145665	2018-04-14 20:56:31.145665
5491	52	61	103	2018-04-14 20:56:31.151	2018-04-14 20:56:31.151
5492	52	78	103	2018-04-14 20:56:31.157371	2018-04-14 20:56:31.157371
5493	52	91	103	2018-04-14 20:56:31.16506	2018-04-14 20:56:31.16506
5494	52	95	103	2018-04-14 20:56:31.171918	2018-04-14 20:56:31.171918
5495	52	98	3	2018-04-14 20:56:31.176798	2018-04-14 20:56:31.176798
5496	52	98	12	2018-04-14 20:56:31.181498	2018-04-14 20:56:31.181498
5497	52	98	18	2018-04-14 20:56:31.185915	2018-04-14 20:56:31.185915
5498	52	98	20	2018-04-14 20:56:31.190361	2018-04-14 20:56:31.190361
5499	52	98	29	2018-04-14 20:56:31.194733	2018-04-14 20:56:31.194733
5500	52	98	37	2018-04-14 20:56:31.200379	2018-04-14 20:56:31.200379
5501	52	98	45	2018-04-14 20:56:31.206054	2018-04-14 20:56:31.206054
5502	52	98	53	2018-04-14 20:56:31.21038	2018-04-14 20:56:31.21038
5503	52	98	54	2018-04-14 20:56:31.215112	2018-04-14 20:56:31.215112
5504	52	98	55	2018-04-14 20:56:31.219728	2018-04-14 20:56:31.219728
5505	52	98	56	2018-04-14 20:56:31.224369	2018-04-14 20:56:31.224369
5506	52	98	57	2018-04-14 20:56:31.229101	2018-04-14 20:56:31.229101
5507	52	98	58	2018-04-14 20:56:31.233586	2018-04-14 20:56:31.233586
5508	52	98	65	2018-04-14 20:56:31.238197	2018-04-14 20:56:31.238197
5509	52	98	69	2018-04-14 20:56:31.242539	2018-04-14 20:56:31.242539
5510	52	98	81	2018-04-14 20:56:31.247251	2018-04-14 20:56:31.247251
5511	52	98	87	2018-04-14 20:56:31.251586	2018-04-14 20:56:31.251586
5512	52	98	92	2018-04-14 20:56:31.255935	2018-04-14 20:56:31.255935
5513	52	98	94	2018-04-14 20:56:31.260663	2018-04-14 20:56:31.260663
5514	52	98	99	2018-04-14 20:56:31.265278	2018-04-14 20:56:31.265278
5515	52	98	101	2018-04-14 20:56:31.269629	2018-04-14 20:56:31.269629
5516	52	98	103	2018-04-14 20:56:31.273916	2018-04-14 20:56:31.273916
5517	52	98	107	2018-04-14 20:56:31.278619	2018-04-14 20:56:31.278619
5518	52	98	109	2018-04-14 20:56:31.283146	2018-04-14 20:56:31.283146
5519	52	108	48	2018-04-14 20:56:31.287593	2018-04-14 20:56:31.287593
5520	52	108	52	2018-04-14 20:56:31.292106	2018-04-14 20:56:31.292106
5521	52	108	98	2018-04-14 20:56:31.296709	2018-04-14 20:56:31.296709
5522	53	10	19	2018-04-14 20:56:31.303556	2018-04-14 20:56:31.303556
5523	53	11	19	2018-04-14 20:56:31.309525	2018-04-14 20:56:31.309525
5524	53	11	62	2018-04-14 20:56:31.313997	2018-04-14 20:56:31.313997
5525	53	11	63	2018-04-14 20:56:31.319074	2018-04-14 20:56:31.319074
5526	53	11	100	2018-04-14 20:56:31.324993	2018-04-14 20:56:31.324993
5527	53	11	103	2018-04-14 20:56:31.330642	2018-04-14 20:56:31.330642
5528	53	19	3	2018-04-14 20:56:31.335592	2018-04-14 20:56:31.335592
5529	53	19	6	2018-04-14 20:56:31.340868	2018-04-14 20:56:31.340868
5530	53	19	12	2018-04-14 20:56:31.34548	2018-04-14 20:56:31.34548
5531	53	19	18	2018-04-14 20:56:31.35026	2018-04-14 20:56:31.35026
5532	53	19	19	2018-04-14 20:56:31.355587	2018-04-14 20:56:31.355587
5533	53	19	20	2018-04-14 20:56:31.361666	2018-04-14 20:56:31.361666
5534	53	19	22	2018-04-14 20:56:31.366252	2018-04-14 20:56:31.366252
5535	53	19	29	2018-04-14 20:56:31.370682	2018-04-14 20:56:31.370682
5536	53	19	35	2018-04-14 20:56:31.375344	2018-04-14 20:56:31.375344
5537	53	19	37	2018-04-14 20:56:31.380078	2018-04-14 20:56:31.380078
5538	53	19	38	2018-04-14 20:56:31.384644	2018-04-14 20:56:31.384644
5539	53	19	45	2018-04-14 20:56:31.389171	2018-04-14 20:56:31.389171
5540	53	19	51	2018-04-14 20:56:31.393598	2018-04-14 20:56:31.393598
5541	53	19	53	2018-04-14 20:56:31.398165	2018-04-14 20:56:31.398165
5542	53	19	54	2018-04-14 20:56:31.404765	2018-04-14 20:56:31.404765
5543	53	19	55	2018-04-14 20:56:31.412859	2018-04-14 20:56:31.412859
5544	53	19	56	2018-04-14 20:56:31.419354	2018-04-14 20:56:31.419354
5545	53	19	57	2018-04-14 20:56:31.424097	2018-04-14 20:56:31.424097
5546	53	19	58	2018-04-14 20:56:31.428668	2018-04-14 20:56:31.428668
5547	53	19	65	2018-04-14 20:56:31.434783	2018-04-14 20:56:31.434783
5548	53	19	69	2018-04-14 20:56:31.439773	2018-04-14 20:56:31.439773
5549	53	19	81	2018-04-14 20:56:31.443973	2018-04-14 20:56:31.443973
5550	53	19	87	2018-04-14 20:56:31.448418	2018-04-14 20:56:31.448418
5551	53	19	92	2018-04-14 20:56:31.453514	2018-04-14 20:56:31.453514
5552	53	19	94	2018-04-14 20:56:31.458675	2018-04-14 20:56:31.458675
5553	53	19	99	2018-04-14 20:56:31.463376	2018-04-14 20:56:31.463376
5554	53	19	101	2018-04-14 20:56:31.468388	2018-04-14 20:56:31.468388
5555	53	19	106	2018-04-14 20:56:31.473207	2018-04-14 20:56:31.473207
5556	53	19	107	2018-04-14 20:56:31.478246	2018-04-14 20:56:31.478246
5557	53	19	109	2018-04-14 20:56:31.483348	2018-04-14 20:56:31.483348
5558	53	19	110	2018-04-14 20:56:31.488079	2018-04-14 20:56:31.488079
5559	53	19	111	2018-04-14 20:56:31.493337	2018-04-14 20:56:31.493337
5560	53	21	19	2018-04-14 20:56:31.498378	2018-04-14 20:56:31.498378
5561	53	33	19	2018-04-14 20:56:31.503029	2018-04-14 20:56:31.503029
5562	53	38	106	2018-04-14 20:56:31.508513	2018-04-14 20:56:31.508513
5563	53	38	111	2018-04-14 20:56:31.515369	2018-04-14 20:56:31.515369
5564	53	47	19	2018-04-14 20:56:31.521176	2018-04-14 20:56:31.521176
5565	53	49	19	2018-04-14 20:56:31.526739	2018-04-14 20:56:31.526739
5566	53	83	19	2018-04-14 20:56:31.531987	2018-04-14 20:56:31.531987
5567	53	83	100	2018-04-14 20:56:31.539913	2018-04-14 20:56:31.539913
5568	53	83	103	2018-04-14 20:56:31.545265	2018-04-14 20:56:31.545265
5569	53	100	6	2018-04-14 20:56:31.550137	2018-04-14 20:56:31.550137
5570	53	100	19	2018-04-14 20:56:31.55489	2018-04-14 20:56:31.55489
5571	53	100	22	2018-04-14 20:56:31.561234	2018-04-14 20:56:31.561234
5572	53	100	38	2018-04-14 20:56:31.566292	2018-04-14 20:56:31.566292
5573	53	100	62	2018-04-14 20:56:31.572798	2018-04-14 20:56:31.572798
5574	53	100	63	2018-04-14 20:56:31.579725	2018-04-14 20:56:31.579725
5575	53	100	100	2018-04-14 20:56:31.585643	2018-04-14 20:56:31.585643
5576	53	100	103	2018-04-14 20:56:31.590944	2018-04-14 20:56:31.590944
5577	53	100	106	2018-04-14 20:56:31.59665	2018-04-14 20:56:31.59665
5578	53	100	107	2018-04-14 20:56:31.602973	2018-04-14 20:56:31.602973
5579	53	100	110	2018-04-14 20:56:31.610208	2018-04-14 20:56:31.610208
5580	53	100	111	2018-04-14 20:56:31.615542	2018-04-14 20:56:31.615542
5581	53	103	103	2018-04-14 20:56:31.621081	2018-04-14 20:56:31.621081
5582	53	106	106	2018-04-14 20:56:31.625834	2018-04-14 20:56:31.625834
5583	53	106	111	2018-04-14 20:56:31.630625	2018-04-14 20:56:31.630625
5584	53	108	19	2018-04-14 20:56:31.635655	2018-04-14 20:56:31.635655
5585	53	110	38	2018-04-14 20:56:31.640426	2018-04-14 20:56:31.640426
5586	53	110	106	2018-04-14 20:56:31.645033	2018-04-14 20:56:31.645033
5587	53	110	111	2018-04-14 20:56:31.649767	2018-04-14 20:56:31.649767
5588	53	111	111	2018-04-14 20:56:31.65431	2018-04-14 20:56:31.65431
5589	54	7	100	2018-04-14 20:56:31.658873	2018-04-14 20:56:31.658873
5590	54	9	100	2018-04-14 20:56:31.663257	2018-04-14 20:56:31.663257
5591	54	10	11	2018-04-14 20:56:31.667632	2018-04-14 20:56:31.667632
5592	54	10	83	2018-04-14 20:56:31.672097	2018-04-14 20:56:31.672097
5593	54	10	100	2018-04-14 20:56:31.676463	2018-04-14 20:56:31.676463
5594	54	11	3	2018-04-14 20:56:31.681542	2018-04-14 20:56:31.681542
5595	54	11	11	2018-04-14 20:56:31.687451	2018-04-14 20:56:31.687451
5596	54	11	12	2018-04-14 20:56:31.691819	2018-04-14 20:56:31.691819
5597	54	11	18	2018-04-14 20:56:31.696372	2018-04-14 20:56:31.696372
5598	54	11	20	2018-04-14 20:56:31.700864	2018-04-14 20:56:31.700864
5599	54	11	29	2018-04-14 20:56:31.705021	2018-04-14 20:56:31.705021
5600	54	11	37	2018-04-14 20:56:31.709481	2018-04-14 20:56:31.709481
5601	54	11	45	2018-04-14 20:56:31.713907	2018-04-14 20:56:31.713907
5602	54	11	46	2018-04-14 20:56:31.718441	2018-04-14 20:56:31.718441
5603	54	11	53	2018-04-14 20:56:31.722657	2018-04-14 20:56:31.722657
5604	54	11	54	2018-04-14 20:56:31.726877	2018-04-14 20:56:31.726877
5605	54	11	55	2018-04-14 20:56:31.731347	2018-04-14 20:56:31.731347
5606	54	11	57	2018-04-14 20:56:31.735664	2018-04-14 20:56:31.735664
5607	54	11	58	2018-04-14 20:56:31.740193	2018-04-14 20:56:31.740193
5608	54	11	69	2018-04-14 20:56:31.744624	2018-04-14 20:56:31.744624
5609	54	11	70	2018-04-14 20:56:31.748874	2018-04-14 20:56:31.748874
5610	54	11	81	2018-04-14 20:56:31.753302	2018-04-14 20:56:31.753302
5611	54	11	84	2018-04-14 20:56:31.760212	2018-04-14 20:56:31.760212
5612	54	11	87	2018-04-14 20:56:31.765748	2018-04-14 20:56:31.765748
5613	54	11	92	2018-04-14 20:56:31.770229	2018-04-14 20:56:31.770229
5614	54	11	94	2018-04-14 20:56:31.774718	2018-04-14 20:56:31.774718
5615	54	11	99	2018-04-14 20:56:31.779367	2018-04-14 20:56:31.779367
5616	54	11	101	2018-04-14 20:56:31.78386	2018-04-14 20:56:31.78386
5617	54	11	109	2018-04-14 20:56:31.788396	2018-04-14 20:56:31.788396
5618	54	12	6	2018-04-14 20:56:31.792858	2018-04-14 20:56:31.792858
5619	54	12	22	2018-04-14 20:56:31.797901	2018-04-14 20:56:31.797901
5620	54	12	38	2018-04-14 20:56:31.803172	2018-04-14 20:56:31.803172
5621	54	12	56	2018-04-14 20:56:31.807998	2018-04-14 20:56:31.807998
5622	54	12	65	2018-04-14 20:56:31.812908	2018-04-14 20:56:31.812908
5623	54	12	71	2018-04-14 20:56:31.817477	2018-04-14 20:56:31.817477
5624	54	12	106	2018-04-14 20:56:31.821988	2018-04-14 20:56:31.821988
5625	54	12	107	2018-04-14 20:56:31.830101	2018-04-14 20:56:31.830101
5626	54	12	110	2018-04-14 20:56:31.836334	2018-04-14 20:56:31.836334
5627	54	12	111	2018-04-14 20:56:31.841182	2018-04-14 20:56:31.841182
5628	54	13	100	2018-04-14 20:56:31.847573	2018-04-14 20:56:31.847573
5629	54	14	100	2018-04-14 20:56:31.853501	2018-04-14 20:56:31.853501
5630	54	18	6	2018-04-14 20:56:31.859805	2018-04-14 20:56:31.859805
5631	54	18	22	2018-04-14 20:56:31.864584	2018-04-14 20:56:31.864584
5632	54	18	38	2018-04-14 20:56:31.869725	2018-04-14 20:56:31.869725
5633	54	18	71	2018-04-14 20:56:31.877034	2018-04-14 20:56:31.877034
5634	54	18	106	2018-04-14 20:56:31.882162	2018-04-14 20:56:31.882162
5635	54	18	110	2018-04-14 20:56:31.888066	2018-04-14 20:56:31.888066
5636	54	18	111	2018-04-14 20:56:31.893124	2018-04-14 20:56:31.893124
5637	54	23	100	2018-04-14 20:56:31.898171	2018-04-14 20:56:31.898171
5638	54	24	6	2018-04-14 20:56:31.904233	2018-04-14 20:56:31.904233
5639	54	24	22	2018-04-14 20:56:31.909564	2018-04-14 20:56:31.909564
5640	54	24	38	2018-04-14 20:56:31.914223	2018-04-14 20:56:31.914223
5641	54	24	100	2018-04-14 20:56:31.921232	2018-04-14 20:56:31.921232
5642	54	24	106	2018-04-14 20:56:31.926997	2018-04-14 20:56:31.926997
5643	54	24	110	2018-04-14 20:56:31.932901	2018-04-14 20:56:31.932901
5644	54	24	111	2018-04-14 20:56:31.94009	2018-04-14 20:56:31.94009
5645	54	26	100	2018-04-14 20:56:31.94572	2018-04-14 20:56:31.94572
5646	54	33	100	2018-04-14 20:56:31.952228	2018-04-14 20:56:31.952228
5647	54	34	100	2018-04-14 20:56:31.958589	2018-04-14 20:56:31.958589
5648	54	36	6	2018-04-14 20:56:31.963826	2018-04-14 20:56:31.963826
5649	54	36	22	2018-04-14 20:56:31.971166	2018-04-14 20:56:31.971166
5650	54	36	38	2018-04-14 20:56:31.976955	2018-04-14 20:56:31.976955
5651	54	36	100	2018-04-14 20:56:31.982726	2018-04-14 20:56:31.982726
5652	54	36	103	2018-04-14 20:56:31.989978	2018-04-14 20:56:31.989978
5653	54	36	106	2018-04-14 20:56:31.995436	2018-04-14 20:56:31.995436
5654	54	36	110	2018-04-14 20:56:32.001125	2018-04-14 20:56:32.001125
5655	54	36	111	2018-04-14 20:56:32.007754	2018-04-14 20:56:32.007754
5656	54	37	6	2018-04-14 20:56:32.013735	2018-04-14 20:56:32.013735
5657	54	37	22	2018-04-14 20:56:32.020792	2018-04-14 20:56:32.020792
5658	54	37	38	2018-04-14 20:56:32.026491	2018-04-14 20:56:32.026491
5659	54	37	71	2018-04-14 20:56:32.032001	2018-04-14 20:56:32.032001
5660	54	37	106	2018-04-14 20:56:32.039335	2018-04-14 20:56:32.039335
5661	54	37	110	2018-04-14 20:56:32.045306	2018-04-14 20:56:32.045306
5662	54	37	111	2018-04-14 20:56:32.050708	2018-04-14 20:56:32.050708
5663	54	40	6	2018-04-14 20:56:32.057561	2018-04-14 20:56:32.057561
5664	54	40	22	2018-04-14 20:56:32.063628	2018-04-14 20:56:32.063628
5665	54	40	38	2018-04-14 20:56:32.070916	2018-04-14 20:56:32.070916
5666	54	40	100	2018-04-14 20:56:32.077037	2018-04-14 20:56:32.077037
5667	54	40	103	2018-04-14 20:56:32.082051	2018-04-14 20:56:32.082051
5668	54	40	106	2018-04-14 20:56:32.08932	2018-04-14 20:56:32.08932
5669	54	40	110	2018-04-14 20:56:32.095019	2018-04-14 20:56:32.095019
5670	54	40	111	2018-04-14 20:56:32.101107	2018-04-14 20:56:32.101107
5671	54	47	100	2018-04-14 20:56:32.1084	2018-04-14 20:56:32.1084
5672	54	49	100	2018-04-14 20:56:32.114299	2018-04-14 20:56:32.114299
5673	54	50	100	2018-04-14 20:56:32.121074	2018-04-14 20:56:32.121074
5674	54	53	56	2018-04-14 20:56:32.126832	2018-04-14 20:56:32.126832
5675	54	53	107	2018-04-14 20:56:32.132455	2018-04-14 20:56:32.132455
5676	54	54	56	2018-04-14 20:56:32.138204	2018-04-14 20:56:32.138204
5677	54	54	107	2018-04-14 20:56:32.143846	2018-04-14 20:56:32.143846
5678	54	55	56	2018-04-14 20:56:32.14917	2018-04-14 20:56:32.14917
5679	54	55	107	2018-04-14 20:56:32.155805	2018-04-14 20:56:32.155805
5680	54	56	3	2018-04-14 20:56:32.160939	2018-04-14 20:56:32.160939
5681	54	56	18	2018-04-14 20:56:32.16558	2018-04-14 20:56:32.16558
5682	54	56	20	2018-04-14 20:56:32.17048	2018-04-14 20:56:32.17048
5683	54	56	29	2018-04-14 20:56:32.175892	2018-04-14 20:56:32.175892
5684	54	56	37	2018-04-14 20:56:32.180892	2018-04-14 20:56:32.180892
5685	54	56	45	2018-04-14 20:56:32.185527	2018-04-14 20:56:32.185527
5686	54	56	56	2018-04-14 20:56:32.190243	2018-04-14 20:56:32.190243
5687	54	56	57	2018-04-14 20:56:32.196795	2018-04-14 20:56:32.196795
5688	54	56	58	2018-04-14 20:56:32.202343	2018-04-14 20:56:32.202343
5689	54	56	69	2018-04-14 20:56:32.208017	2018-04-14 20:56:32.208017
5690	54	56	81	2018-04-14 20:56:32.21341	2018-04-14 20:56:32.21341
5691	54	56	94	2018-04-14 20:56:32.218671	2018-04-14 20:56:32.218671
5692	54	56	99	2018-04-14 20:56:32.223898	2018-04-14 20:56:32.223898
5693	54	56	101	2018-04-14 20:56:32.230215	2018-04-14 20:56:32.230215
5694	54	56	107	2018-04-14 20:56:32.246631	2018-04-14 20:56:32.246631
5695	54	65	6	2018-04-14 20:56:32.251674	2018-04-14 20:56:32.251674
5696	54	65	22	2018-04-14 20:56:32.256468	2018-04-14 20:56:32.256468
5697	54	65	38	2018-04-14 20:56:32.262318	2018-04-14 20:56:32.262318
5698	54	65	56	2018-04-14 20:56:32.267509	2018-04-14 20:56:32.267509
5699	54	65	65	2018-04-14 20:56:32.27297	2018-04-14 20:56:32.27297
5700	54	65	71	2018-04-14 20:56:32.278597	2018-04-14 20:56:32.278597
5701	54	65	106	2018-04-14 20:56:32.283548	2018-04-14 20:56:32.283548
5702	54	65	107	2018-04-14 20:56:32.289676	2018-04-14 20:56:32.289676
5703	54	65	110	2018-04-14 20:56:32.295259	2018-04-14 20:56:32.295259
5704	54	65	111	2018-04-14 20:56:32.300358	2018-04-14 20:56:32.300358
5705	54	81	6	2018-04-14 20:56:32.306719	2018-04-14 20:56:32.306719
5706	54	81	22	2018-04-14 20:56:32.312205	2018-04-14 20:56:32.312205
5707	54	81	38	2018-04-14 20:56:32.317595	2018-04-14 20:56:32.317595
5708	54	81	71	2018-04-14 20:56:32.323828	2018-04-14 20:56:32.323828
5709	54	81	106	2018-04-14 20:56:32.328998	2018-04-14 20:56:32.328998
5710	54	81	110	2018-04-14 20:56:32.334192	2018-04-14 20:56:32.334192
5711	54	81	111	2018-04-14 20:56:32.340555	2018-04-14 20:56:32.340555
5712	54	82	11	2018-04-14 20:56:32.34581	2018-04-14 20:56:32.34581
5713	54	82	83	2018-04-14 20:56:32.351104	2018-04-14 20:56:32.351104
5714	54	82	100	2018-04-14 20:56:32.357596	2018-04-14 20:56:32.357596
5715	54	83	3	2018-04-14 20:56:32.362863	2018-04-14 20:56:32.362863
5716	54	83	11	2018-04-14 20:56:32.368167	2018-04-14 20:56:32.368167
5717	54	83	12	2018-04-14 20:56:32.374419	2018-04-14 20:56:32.374419
5718	54	83	18	2018-04-14 20:56:32.379381	2018-04-14 20:56:32.379381
5719	54	83	20	2018-04-14 20:56:32.384317	2018-04-14 20:56:32.384317
5720	54	83	29	2018-04-14 20:56:32.389298	2018-04-14 20:56:32.389298
5721	54	83	37	2018-04-14 20:56:32.393862	2018-04-14 20:56:32.393862
5722	54	83	45	2018-04-14 20:56:32.398593	2018-04-14 20:56:32.398593
5723	54	83	46	2018-04-14 20:56:32.403	2018-04-14 20:56:32.403
5724	54	83	53	2018-04-14 20:56:32.407546	2018-04-14 20:56:32.407546
5725	54	83	54	2018-04-14 20:56:32.413469	2018-04-14 20:56:32.413469
5726	54	83	55	2018-04-14 20:56:32.419015	2018-04-14 20:56:32.419015
5727	54	83	56	2018-04-14 20:56:32.423611	2018-04-14 20:56:32.423611
5728	54	83	57	2018-04-14 20:56:32.428154	2018-04-14 20:56:32.428154
5729	54	83	58	2018-04-14 20:56:32.432493	2018-04-14 20:56:32.432493
5730	54	83	69	2018-04-14 20:56:32.439668	2018-04-14 20:56:32.439668
5731	54	83	70	2018-04-14 20:56:32.44702	2018-04-14 20:56:32.44702
5732	54	83	81	2018-04-14 20:56:32.452598	2018-04-14 20:56:32.452598
5733	54	83	83	2018-04-14 20:56:32.457551	2018-04-14 20:56:32.457551
5734	54	83	84	2018-04-14 20:56:32.462563	2018-04-14 20:56:32.462563
5735	54	83	87	2018-04-14 20:56:32.467353	2018-04-14 20:56:32.467353
5736	54	83	92	2018-04-14 20:56:32.471869	2018-04-14 20:56:32.471869
5737	54	83	94	2018-04-14 20:56:32.476361	2018-04-14 20:56:32.476361
5738	54	83	99	2018-04-14 20:56:32.482703	2018-04-14 20:56:32.482703
5739	54	83	101	2018-04-14 20:56:32.489032	2018-04-14 20:56:32.489032
5740	54	83	109	2018-04-14 20:56:32.493783	2018-04-14 20:56:32.493783
5741	54	85	6	2018-04-14 20:56:32.498527	2018-04-14 20:56:32.498527
5742	54	85	22	2018-04-14 20:56:32.50328	2018-04-14 20:56:32.50328
5743	54	85	38	2018-04-14 20:56:32.507837	2018-04-14 20:56:32.507837
5744	54	85	100	2018-04-14 20:56:32.512303	2018-04-14 20:56:32.512303
5745	54	85	106	2018-04-14 20:56:32.517178	2018-04-14 20:56:32.517178
5746	54	85	110	2018-04-14 20:56:32.521658	2018-04-14 20:56:32.521658
5747	54	85	111	2018-04-14 20:56:32.531624	2018-04-14 20:56:32.531624
5748	54	87	6	2018-04-14 20:56:32.538005	2018-04-14 20:56:32.538005
5749	54	87	22	2018-04-14 20:56:32.542621	2018-04-14 20:56:32.542621
5750	54	87	38	2018-04-14 20:56:32.547335	2018-04-14 20:56:32.547335
5751	54	87	56	2018-04-14 20:56:32.552325	2018-04-14 20:56:32.552325
5752	54	87	65	2018-04-14 20:56:32.557399	2018-04-14 20:56:32.557399
5753	54	87	71	2018-04-14 20:56:32.564079	2018-04-14 20:56:32.564079
5754	54	87	106	2018-04-14 20:56:32.568899	2018-04-14 20:56:32.568899
5755	54	87	107	2018-04-14 20:56:32.573645	2018-04-14 20:56:32.573645
5756	54	87	110	2018-04-14 20:56:32.578065	2018-04-14 20:56:32.578065
5757	54	87	111	2018-04-14 20:56:32.582604	2018-04-14 20:56:32.582604
5758	54	88	6	2018-04-14 20:56:32.587374	2018-04-14 20:56:32.587374
5759	54	88	22	2018-04-14 20:56:32.591969	2018-04-14 20:56:32.591969
5760	54	88	38	2018-04-14 20:56:32.596716	2018-04-14 20:56:32.596716
5761	54	88	100	2018-04-14 20:56:32.601113	2018-04-14 20:56:32.601113
5762	54	88	106	2018-04-14 20:56:32.605734	2018-04-14 20:56:32.605734
5763	54	88	110	2018-04-14 20:56:32.610319	2018-04-14 20:56:32.610319
5764	54	88	111	2018-04-14 20:56:32.614819	2018-04-14 20:56:32.614819
5765	54	89	6	2018-04-14 20:56:32.619432	2018-04-14 20:56:32.619432
5766	54	89	22	2018-04-14 20:56:32.623971	2018-04-14 20:56:32.623971
5767	54	89	38	2018-04-14 20:56:32.628518	2018-04-14 20:56:32.628518
5768	54	89	100	2018-04-14 20:56:32.633438	2018-04-14 20:56:32.633438
5769	54	89	106	2018-04-14 20:56:32.637818	2018-04-14 20:56:32.637818
5770	54	89	110	2018-04-14 20:56:32.642213	2018-04-14 20:56:32.642213
5771	54	89	111	2018-04-14 20:56:32.647479	2018-04-14 20:56:32.647479
5772	54	92	6	2018-04-14 20:56:32.653127	2018-04-14 20:56:32.653127
5773	54	92	22	2018-04-14 20:56:32.657918	2018-04-14 20:56:32.657918
5774	54	92	38	2018-04-14 20:56:32.662432	2018-04-14 20:56:32.662432
5775	54	92	56	2018-04-14 20:56:32.666915	2018-04-14 20:56:32.666915
5776	54	92	65	2018-04-14 20:56:32.671706	2018-04-14 20:56:32.671706
5777	54	92	71	2018-04-14 20:56:32.676155	2018-04-14 20:56:32.676155
5778	54	92	106	2018-04-14 20:56:32.681172	2018-04-14 20:56:32.681172
5779	54	92	107	2018-04-14 20:56:32.686397	2018-04-14 20:56:32.686397
5780	54	92	110	2018-04-14 20:56:32.691117	2018-04-14 20:56:32.691117
5781	54	92	111	2018-04-14 20:56:32.696055	2018-04-14 20:56:32.696055
5782	54	100	3	2018-04-14 20:56:32.700934	2018-04-14 20:56:32.700934
5783	54	100	12	2018-04-14 20:56:32.70559	2018-04-14 20:56:32.70559
5784	54	100	18	2018-04-14 20:56:32.710276	2018-04-14 20:56:32.710276
5785	54	100	20	2018-04-14 20:56:32.714961	2018-04-14 20:56:32.714961
5786	54	100	29	2018-04-14 20:56:32.719417	2018-04-14 20:56:32.719417
5787	54	100	45	2018-04-14 20:56:32.723918	2018-04-14 20:56:32.723918
5788	54	100	46	2018-04-14 20:56:32.729068	2018-04-14 20:56:32.729068
5789	54	100	53	2018-04-14 20:56:32.733741	2018-04-14 20:56:32.733741
5790	54	100	54	2018-04-14 20:56:32.73834	2018-04-14 20:56:32.73834
5791	54	100	57	2018-04-14 20:56:32.743119	2018-04-14 20:56:32.743119
5792	54	100	58	2018-04-14 20:56:32.747741	2018-04-14 20:56:32.747741
5793	54	100	69	2018-04-14 20:56:32.752048	2018-04-14 20:56:32.752048
5794	54	100	70	2018-04-14 20:56:32.756552	2018-04-14 20:56:32.756552
5795	54	100	81	2018-04-14 20:56:32.760902	2018-04-14 20:56:32.760902
5796	54	100	84	2018-04-14 20:56:32.765555	2018-04-14 20:56:32.765555
5797	54	100	87	2018-04-14 20:56:32.770133	2018-04-14 20:56:32.770133
5798	54	100	92	2018-04-14 20:56:32.774759	2018-04-14 20:56:32.774759
5799	54	100	94	2018-04-14 20:56:32.779693	2018-04-14 20:56:32.779693
5800	54	100	99	2018-04-14 20:56:32.784129	2018-04-14 20:56:32.784129
5801	54	100	109	2018-04-14 20:56:32.788814	2018-04-14 20:56:32.788814
5802	54	101	6	2018-04-14 20:56:32.793438	2018-04-14 20:56:32.793438
5803	54	101	22	2018-04-14 20:56:32.79816	2018-04-14 20:56:32.79816
5804	54	101	38	2018-04-14 20:56:32.802792	2018-04-14 20:56:32.802792
5805	54	101	71	2018-04-14 20:56:32.807439	2018-04-14 20:56:32.807439
5806	54	101	106	2018-04-14 20:56:32.812071	2018-04-14 20:56:32.812071
5807	54	101	110	2018-04-14 20:56:32.816906	2018-04-14 20:56:32.816906
5808	54	101	111	2018-04-14 20:56:32.821567	2018-04-14 20:56:32.821567
5809	54	107	6	2018-04-14 20:56:32.827525	2018-04-14 20:56:32.827525
5810	54	107	22	2018-04-14 20:56:32.832041	2018-04-14 20:56:32.832041
5811	54	107	38	2018-04-14 20:56:32.838088	2018-04-14 20:56:32.838088
5812	54	107	71	2018-04-14 20:56:32.843271	2018-04-14 20:56:32.843271
5813	54	107	106	2018-04-14 20:56:32.856282	2018-04-14 20:56:32.856282
5814	54	107	110	2018-04-14 20:56:32.874836	2018-04-14 20:56:32.874836
5815	54	107	111	2018-04-14 20:56:32.880396	2018-04-14 20:56:32.880396
5816	54	108	100	2018-04-14 20:56:32.885603	2018-04-14 20:56:32.885603
5817	54	109	6	2018-04-14 20:56:32.890682	2018-04-14 20:56:32.890682
5818	54	109	22	2018-04-14 20:56:32.895377	2018-04-14 20:56:32.895377
5819	54	109	38	2018-04-14 20:56:32.901119	2018-04-14 20:56:32.901119
5820	54	109	56	2018-04-14 20:56:32.912489	2018-04-14 20:56:32.912489
5821	54	109	65	2018-04-14 20:56:32.933053	2018-04-14 20:56:32.933053
5822	54	109	71	2018-04-14 20:56:32.939805	2018-04-14 20:56:32.939805
5823	54	109	106	2018-04-14 20:56:32.944389	2018-04-14 20:56:32.944389
5824	54	109	107	2018-04-14 20:56:32.949289	2018-04-14 20:56:32.949289
5825	54	109	110	2018-04-14 20:56:32.953972	2018-04-14 20:56:32.953972
5826	54	109	111	2018-04-14 20:56:32.958663	2018-04-14 20:56:32.958663
5827	55	49	15	2018-04-14 20:56:32.963977	2018-04-14 20:56:32.963977
5828	55	49	16	2018-04-14 20:56:32.969102	2018-04-14 20:56:32.969102
5829	55	49	17	2018-04-14 20:56:32.973926	2018-04-14 20:56:32.973926
5830	55	49	24	2018-04-14 20:56:32.978365	2018-04-14 20:56:32.978365
5831	55	49	31	2018-04-14 20:56:32.983666	2018-04-14 20:56:32.983666
5832	55	49	32	2018-04-14 20:56:32.988418	2018-04-14 20:56:32.988418
5833	55	49	39	2018-04-14 20:56:32.993261	2018-04-14 20:56:32.993261
5834	55	49	41	2018-04-14 20:56:32.998803	2018-04-14 20:56:32.998803
5835	55	49	46	2018-04-14 20:56:33.003438	2018-04-14 20:56:33.003438
5836	55	49	61	2018-04-14 20:56:33.008004	2018-04-14 20:56:33.008004
5837	55	49	70	2018-04-14 20:56:33.01262	2018-04-14 20:56:33.01262
5838	55	49	72	2018-04-14 20:56:33.017615	2018-04-14 20:56:33.017615
5839	55	49	74	2018-04-14 20:56:33.022249	2018-04-14 20:56:33.022249
5840	55	49	78	2018-04-14 20:56:33.02678	2018-04-14 20:56:33.02678
5841	55	49	84	2018-04-14 20:56:33.031468	2018-04-14 20:56:33.031468
5842	55	49	85	2018-04-14 20:56:33.036038	2018-04-14 20:56:33.036038
5843	55	49	86	2018-04-14 20:56:33.041088	2018-04-14 20:56:33.041088
5844	55	49	88	2018-04-14 20:56:33.046231	2018-04-14 20:56:33.046231
5845	55	49	89	2018-04-14 20:56:33.051667	2018-04-14 20:56:33.051667
5846	55	49	91	2018-04-14 20:56:33.056145	2018-04-14 20:56:33.056145
5847	55	49	95	2018-04-14 20:56:33.060656	2018-04-14 20:56:33.060656
5848	55	50	24	2018-04-14 20:56:33.065397	2018-04-14 20:56:33.065397
5849	55	50	31	2018-04-14 20:56:33.070086	2018-04-14 20:56:33.070086
5850	55	50	32	2018-04-14 20:56:33.074774	2018-04-14 20:56:33.074774
5851	56	1	26	2018-04-14 20:56:33.079709	2018-04-14 20:56:33.079709
5852	56	1	50	2018-04-14 20:56:33.084213	2018-04-14 20:56:33.084213
5853	56	5	26	2018-04-14 20:56:33.089134	2018-04-14 20:56:33.089134
5854	56	8	26	2018-04-14 20:56:33.094706	2018-04-14 20:56:33.094706
5855	56	8	50	2018-04-14 20:56:33.099566	2018-04-14 20:56:33.099566
5856	56	10	26	2018-04-14 20:56:33.104011	2018-04-14 20:56:33.104011
5857	56	10	50	2018-04-14 20:56:33.108673	2018-04-14 20:56:33.108673
5858	56	25	26	2018-04-14 20:56:33.113497	2018-04-14 20:56:33.113497
5859	56	25	50	2018-04-14 20:56:33.118066	2018-04-14 20:56:33.118066
5860	56	26	2	2018-04-14 20:56:33.122658	2018-04-14 20:56:33.122658
5861	56	26	4	2018-04-14 20:56:33.127212	2018-04-14 20:56:33.127212
5862	56	26	12	2018-04-14 20:56:33.13161	2018-04-14 20:56:33.13161
5863	56	26	36	2018-04-14 20:56:33.136038	2018-04-14 20:56:33.136038
5864	56	26	40	2018-04-14 20:56:33.14051	2018-04-14 20:56:33.14051
5865	56	26	53	2018-04-14 20:56:33.145243	2018-04-14 20:56:33.145243
5866	56	26	60	2018-04-14 20:56:33.150633	2018-04-14 20:56:33.150633
5867	56	26	64	2018-04-14 20:56:33.155241	2018-04-14 20:56:33.155241
5868	56	26	77	2018-04-14 20:56:33.161265	2018-04-14 20:56:33.161265
5869	56	26	80	2018-04-14 20:56:33.165833	2018-04-14 20:56:33.165833
5870	56	26	87	2018-04-14 20:56:33.170377	2018-04-14 20:56:33.170377
5871	56	26	92	2018-04-14 20:56:33.175094	2018-04-14 20:56:33.175094
5872	56	26	103	2018-04-14 20:56:33.17987	2018-04-14 20:56:33.17987
5873	56	26	109	2018-04-14 20:56:33.184638	2018-04-14 20:56:33.184638
5874	56	30	26	2018-04-14 20:56:33.189674	2018-04-14 20:56:33.189674
5875	56	30	50	2018-04-14 20:56:33.194334	2018-04-14 20:56:33.194334
5876	56	44	26	2018-04-14 20:56:33.199236	2018-04-14 20:56:33.199236
5877	56	44	50	2018-04-14 20:56:33.204182	2018-04-14 20:56:33.204182
5878	56	49	23	2018-04-14 20:56:33.209174	2018-04-14 20:56:33.209174
5879	56	49	34	2018-04-14 20:56:33.21393	2018-04-14 20:56:33.21393
5880	56	49	64	2018-04-14 20:56:33.218881	2018-04-14 20:56:33.218881
5881	56	49	80	2018-04-14 20:56:33.223536	2018-04-14 20:56:33.223536
5882	56	50	2	2018-04-14 20:56:33.228136	2018-04-14 20:56:33.228136
5883	56	50	4	2018-04-14 20:56:33.232869	2018-04-14 20:56:33.232869
5884	56	50	34	2018-04-14 20:56:33.237842	2018-04-14 20:56:33.237842
5885	56	50	40	2018-04-14 20:56:33.242595	2018-04-14 20:56:33.242595
5886	56	50	60	2018-04-14 20:56:33.247584	2018-04-14 20:56:33.247584
5887	56	50	64	2018-04-14 20:56:33.252134	2018-04-14 20:56:33.252134
5888	56	50	77	2018-04-14 20:56:33.256819	2018-04-14 20:56:33.256819
5889	56	50	80	2018-04-14 20:56:33.261445	2018-04-14 20:56:33.261445
5890	56	50	92	2018-04-14 20:56:33.266348	2018-04-14 20:56:33.266348
5891	56	59	26	2018-04-14 20:56:33.271373	2018-04-14 20:56:33.271373
5892	56	59	50	2018-04-14 20:56:33.278101	2018-04-14 20:56:33.278101
5893	56	76	26	2018-04-14 20:56:33.282755	2018-04-14 20:56:33.282755
5894	56	76	50	2018-04-14 20:56:33.287478	2018-04-14 20:56:33.287478
5895	56	79	26	2018-04-14 20:56:33.292176	2018-04-14 20:56:33.292176
5896	56	79	50	2018-04-14 20:56:33.296918	2018-04-14 20:56:33.296918
5897	56	90	26	2018-04-14 20:56:33.301686	2018-04-14 20:56:33.301686
5898	56	90	50	2018-04-14 20:56:33.306163	2018-04-14 20:56:33.306163
5899	56	93	26	2018-04-14 20:56:33.311178	2018-04-14 20:56:33.311178
5900	56	93	50	2018-04-14 20:56:33.315838	2018-04-14 20:56:33.315838
5901	57	23	3	2018-04-14 20:56:33.320697	2018-04-14 20:56:33.320697
5902	57	23	20	2018-04-14 20:56:33.326093	2018-04-14 20:56:33.326093
5903	57	23	29	2018-04-14 20:56:33.330818	2018-04-14 20:56:33.330818
5904	57	23	37	2018-04-14 20:56:33.335638	2018-04-14 20:56:33.335638
5905	57	23	45	2018-04-14 20:56:33.340381	2018-04-14 20:56:33.340381
5906	57	23	54	2018-04-14 20:56:33.345027	2018-04-14 20:56:33.345027
5907	57	23	55	2018-04-14 20:56:33.350103	2018-04-14 20:56:33.350103
5908	57	23	56	2018-04-14 20:56:33.354807	2018-04-14 20:56:33.354807
5909	57	23	57	2018-04-14 20:56:33.359527	2018-04-14 20:56:33.359527
5910	57	23	58	2018-04-14 20:56:33.364142	2018-04-14 20:56:33.364142
5911	57	23	65	2018-04-14 20:56:33.368947	2018-04-14 20:56:33.368947
5912	57	23	69	2018-04-14 20:56:33.373743	2018-04-14 20:56:33.373743
5913	57	23	81	2018-04-14 20:56:33.379798	2018-04-14 20:56:33.379798
5914	57	23	94	2018-04-14 20:56:33.385077	2018-04-14 20:56:33.385077
5915	57	23	99	2018-04-14 20:56:33.389908	2018-04-14 20:56:33.389908
5916	57	23	101	2018-04-14 20:56:33.394459	2018-04-14 20:56:33.394459
5917	57	23	107	2018-04-14 20:56:33.399524	2018-04-14 20:56:33.399524
5918	57	34	3	2018-04-14 20:56:33.404418	2018-04-14 20:56:33.404418
5919	57	34	6	2018-04-14 20:56:33.409326	2018-04-14 20:56:33.409326
5920	57	34	20	2018-04-14 20:56:33.414008	2018-04-14 20:56:33.414008
5921	57	34	22	2018-04-14 20:56:33.418539	2018-04-14 20:56:33.418539
5922	57	34	29	2018-04-14 20:56:33.423665	2018-04-14 20:56:33.423665
5923	57	34	37	2018-04-14 20:56:33.428632	2018-04-14 20:56:33.428632
5924	57	34	38	2018-04-14 20:56:33.450136	2018-04-14 20:56:33.450136
5925	57	34	45	2018-04-14 20:56:33.457683	2018-04-14 20:56:33.457683
5926	57	34	55	2018-04-14 20:56:33.463611	2018-04-14 20:56:33.463611
5927	57	34	56	2018-04-14 20:56:33.468346	2018-04-14 20:56:33.468346
5928	57	34	57	2018-04-14 20:56:33.473389	2018-04-14 20:56:33.473389
5929	57	34	58	2018-04-14 20:56:33.47845	2018-04-14 20:56:33.47845
5930	57	34	65	2018-04-14 20:56:33.483103	2018-04-14 20:56:33.483103
5931	57	34	69	2018-04-14 20:56:33.487695	2018-04-14 20:56:33.487695
5932	57	34	71	2018-04-14 20:56:33.493168	2018-04-14 20:56:33.493168
5933	57	34	81	2018-04-14 20:56:33.498021	2018-04-14 20:56:33.498021
5934	57	34	94	2018-04-14 20:56:33.502769	2018-04-14 20:56:33.502769
5935	57	34	99	2018-04-14 20:56:33.507416	2018-04-14 20:56:33.507416
5936	57	34	101	2018-04-14 20:56:33.512076	2018-04-14 20:56:33.512076
5937	57	34	106	2018-04-14 20:56:33.516922	2018-04-14 20:56:33.516922
5938	57	34	107	2018-04-14 20:56:33.521781	2018-04-14 20:56:33.521781
5939	57	34	110	2018-04-14 20:56:33.526641	2018-04-14 20:56:33.526641
5940	57	34	111	2018-04-14 20:56:33.531392	2018-04-14 20:56:33.531392
5941	57	108	3	2018-04-14 20:56:33.535947	2018-04-14 20:56:33.535947
5942	57	108	12	2018-04-14 20:56:33.540432	2018-04-14 20:56:33.540432
5943	57	108	20	2018-04-14 20:56:33.545113	2018-04-14 20:56:33.545113
5944	57	108	29	2018-04-14 20:56:33.549993	2018-04-14 20:56:33.549993
5945	57	108	37	2018-04-14 20:56:33.55471	2018-04-14 20:56:33.55471
5946	57	108	45	2018-04-14 20:56:33.56148	2018-04-14 20:56:33.56148
5947	57	108	53	2018-04-14 20:56:33.567514	2018-04-14 20:56:33.567514
5948	57	108	54	2018-04-14 20:56:33.575061	2018-04-14 20:56:33.575061
5949	57	108	55	2018-04-14 20:56:33.582158	2018-04-14 20:56:33.582158
5950	57	108	56	2018-04-14 20:56:33.587813	2018-04-14 20:56:33.587813
5951	57	108	57	2018-04-14 20:56:33.592691	2018-04-14 20:56:33.592691
5952	57	108	58	2018-04-14 20:56:33.599021	2018-04-14 20:56:33.599021
5953	57	108	65	2018-04-14 20:56:33.60356	2018-04-14 20:56:33.60356
5954	57	108	69	2018-04-14 20:56:33.610663	2018-04-14 20:56:33.610663
5955	57	108	81	2018-04-14 20:56:33.617539	2018-04-14 20:56:33.617539
5956	57	108	87	2018-04-14 20:56:33.62263	2018-04-14 20:56:33.62263
5957	57	108	92	2018-04-14 20:56:33.629546	2018-04-14 20:56:33.629546
5958	57	108	94	2018-04-14 20:56:33.634191	2018-04-14 20:56:33.634191
5959	57	108	99	2018-04-14 20:56:33.639711	2018-04-14 20:56:33.639711
5960	57	108	101	2018-04-14 20:56:33.644241	2018-04-14 20:56:33.644241
5961	57	108	107	2018-04-14 20:56:33.649108	2018-04-14 20:56:33.649108
5962	57	108	109	2018-04-14 20:56:33.654989	2018-04-14 20:56:33.654989
5963	58	11	6	2018-04-14 20:56:33.660565	2018-04-14 20:56:33.660565
5964	58	11	22	2018-04-14 20:56:33.665334	2018-04-14 20:56:33.665334
5965	58	11	38	2018-04-14 20:56:33.670057	2018-04-14 20:56:33.670057
5966	58	11	65	2018-04-14 20:56:33.674511	2018-04-14 20:56:33.674511
5967	58	11	71	2018-04-14 20:56:33.679111	2018-04-14 20:56:33.679111
5968	58	11	106	2018-04-14 20:56:33.683614	2018-04-14 20:56:33.683614
5969	58	11	107	2018-04-14 20:56:33.688246	2018-04-14 20:56:33.688246
5970	58	11	110	2018-04-14 20:56:33.692597	2018-04-14 20:56:33.692597
5971	58	11	111	2018-04-14 20:56:33.697491	2018-04-14 20:56:33.697491
5972	58	13	19	2018-04-14 20:56:33.702024	2018-04-14 20:56:33.702024
5973	58	19	62	2018-04-14 20:56:33.706265	2018-04-14 20:56:33.706265
5974	58	19	63	2018-04-14 20:56:33.712597	2018-04-14 20:56:33.712597
5975	58	19	71	2018-04-14 20:56:33.720388	2018-04-14 20:56:33.720388
5976	58	19	96	2018-04-14 20:56:33.725811	2018-04-14 20:56:33.725811
5977	58	19	97	2018-04-14 20:56:33.730379	2018-04-14 20:56:33.730379
5978	58	19	105	2018-04-14 20:56:33.734992	2018-04-14 20:56:33.734992
5979	58	22	22	2018-04-14 20:56:33.739617	2018-04-14 20:56:33.739617
5980	58	22	38	2018-04-14 20:56:33.743991	2018-04-14 20:56:33.743991
5981	58	22	106	2018-04-14 20:56:33.748678	2018-04-14 20:56:33.748678
5982	58	22	110	2018-04-14 20:56:33.753189	2018-04-14 20:56:33.753189
5983	58	22	111	2018-04-14 20:56:33.75883	2018-04-14 20:56:33.75883
5984	58	23	19	2018-04-14 20:56:33.763351	2018-04-14 20:56:33.763351
5985	58	31	19	2018-04-14 20:56:33.768302	2018-04-14 20:56:33.768302
5986	58	32	19	2018-04-14 20:56:33.775196	2018-04-14 20:56:33.775196
5987	58	38	38	2018-04-14 20:56:33.782339	2018-04-14 20:56:33.782339
5988	58	50	19	2018-04-14 20:56:33.787282	2018-04-14 20:56:33.787282
5989	58	55	6	2018-04-14 20:56:33.79187	2018-04-14 20:56:33.79187
5990	58	55	22	2018-04-14 20:56:33.797596	2018-04-14 20:56:33.797596
5991	58	55	38	2018-04-14 20:56:33.802499	2018-04-14 20:56:33.802499
5992	58	55	71	2018-04-14 20:56:33.806903	2018-04-14 20:56:33.806903
5993	58	55	106	2018-04-14 20:56:33.811632	2018-04-14 20:56:33.811632
5994	58	55	110	2018-04-14 20:56:33.819126	2018-04-14 20:56:33.819126
5995	58	55	111	2018-04-14 20:56:33.82436	2018-04-14 20:56:33.82436
5996	58	56	6	2018-04-14 20:56:33.82921	2018-04-14 20:56:33.82921
5997	58	56	22	2018-04-14 20:56:33.833664	2018-04-14 20:56:33.833664
5998	58	56	38	2018-04-14 20:56:33.83977	2018-04-14 20:56:33.83977
5999	58	56	71	2018-04-14 20:56:33.845193	2018-04-14 20:56:33.845193
6000	58	56	106	2018-04-14 20:56:33.849789	2018-04-14 20:56:33.849789
6001	58	56	110	2018-04-14 20:56:33.854636	2018-04-14 20:56:33.854636
6002	58	56	111	2018-04-14 20:56:33.859097	2018-04-14 20:56:33.859097
6003	58	83	6	2018-04-14 20:56:33.863616	2018-04-14 20:56:33.863616
6004	58	83	22	2018-04-14 20:56:33.867867	2018-04-14 20:56:33.867867
6005	58	83	38	2018-04-14 20:56:33.872285	2018-04-14 20:56:33.872285
6006	58	83	65	2018-04-14 20:56:33.8768	2018-04-14 20:56:33.8768
6007	58	83	71	2018-04-14 20:56:33.881423	2018-04-14 20:56:33.881423
6008	58	83	106	2018-04-14 20:56:33.885994	2018-04-14 20:56:33.885994
6009	58	83	107	2018-04-14 20:56:33.891806	2018-04-14 20:56:33.891806
6010	58	83	110	2018-04-14 20:56:33.896606	2018-04-14 20:56:33.896606
6011	58	83	111	2018-04-14 20:56:33.901309	2018-04-14 20:56:33.901309
6012	58	100	65	2018-04-14 20:56:33.905872	2018-04-14 20:56:33.905872
6013	58	100	71	2018-04-14 20:56:33.912597	2018-04-14 20:56:33.912597
6014	58	110	110	2018-04-14 20:56:33.918068	2018-04-14 20:56:33.918068
6015	59	1	15	2018-04-14 20:56:33.922392	2018-04-14 20:56:33.922392
6016	59	1	16	2018-04-14 20:56:33.927059	2018-04-14 20:56:33.927059
6017	59	1	17	2018-04-14 20:56:33.931606	2018-04-14 20:56:33.931606
6018	59	1	24	2018-04-14 20:56:33.93617	2018-04-14 20:56:33.93617
6019	59	1	31	2018-04-14 20:56:33.940569	2018-04-14 20:56:33.940569
6020	59	1	39	2018-04-14 20:56:33.945474	2018-04-14 20:56:33.945474
6021	59	1	41	2018-04-14 20:56:33.950063	2018-04-14 20:56:33.950063
6022	59	1	46	2018-04-14 20:56:33.954186	2018-04-14 20:56:33.954186
6023	59	1	61	2018-04-14 20:56:33.959504	2018-04-14 20:56:33.959504
6024	59	1	70	2018-04-14 20:56:33.96412	2018-04-14 20:56:33.96412
6025	59	1	72	2018-04-14 20:56:33.968826	2018-04-14 20:56:33.968826
6026	59	1	74	2018-04-14 20:56:33.973242	2018-04-14 20:56:33.973242
6027	59	1	78	2018-04-14 20:56:33.977839	2018-04-14 20:56:33.977839
6028	59	1	84	2018-04-14 20:56:33.982524	2018-04-14 20:56:33.982524
6029	59	1	85	2018-04-14 20:56:33.987063	2018-04-14 20:56:33.987063
6030	59	1	86	2018-04-14 20:56:33.991623	2018-04-14 20:56:33.991623
6031	59	1	88	2018-04-14 20:56:33.995961	2018-04-14 20:56:33.995961
6032	59	1	89	2018-04-14 20:56:34.000622	2018-04-14 20:56:34.000622
6033	59	1	91	2018-04-14 20:56:34.005373	2018-04-14 20:56:34.005373
6034	59	1	95	2018-04-14 20:56:34.011257	2018-04-14 20:56:34.011257
6035	59	5	15	2018-04-14 20:56:34.015948	2018-04-14 20:56:34.015948
6036	59	5	16	2018-04-14 20:56:34.020308	2018-04-14 20:56:34.020308
6037	59	5	17	2018-04-14 20:56:34.02522	2018-04-14 20:56:34.02522
6038	59	5	24	2018-04-14 20:56:34.031472	2018-04-14 20:56:34.031472
6039	59	5	31	2018-04-14 20:56:34.03971	2018-04-14 20:56:34.03971
6040	59	5	39	2018-04-14 20:56:34.046283	2018-04-14 20:56:34.046283
6041	59	5	72	2018-04-14 20:56:34.050745	2018-04-14 20:56:34.050745
6042	59	5	74	2018-04-14 20:56:34.055487	2018-04-14 20:56:34.055487
6043	59	5	85	2018-04-14 20:56:34.059998	2018-04-14 20:56:34.059998
6044	59	5	86	2018-04-14 20:56:34.06669	2018-04-14 20:56:34.06669
6045	59	5	88	2018-04-14 20:56:34.072097	2018-04-14 20:56:34.072097
6046	59	5	89	2018-04-14 20:56:34.07662	2018-04-14 20:56:34.07662
6047	59	59	15	2018-04-14 20:56:34.08108	2018-04-14 20:56:34.08108
6048	59	59	16	2018-04-14 20:56:34.085654	2018-04-14 20:56:34.085654
6049	59	59	17	2018-04-14 20:56:34.090272	2018-04-14 20:56:34.090272
6050	59	59	24	2018-04-14 20:56:34.094792	2018-04-14 20:56:34.094792
6051	59	59	31	2018-04-14 20:56:34.099666	2018-04-14 20:56:34.099666
6052	59	59	39	2018-04-14 20:56:34.104354	2018-04-14 20:56:34.104354
6053	59	59	41	2018-04-14 20:56:34.108799	2018-04-14 20:56:34.108799
6054	59	59	46	2018-04-14 20:56:34.113339	2018-04-14 20:56:34.113339
6055	59	59	61	2018-04-14 20:56:34.11783	2018-04-14 20:56:34.11783
6056	59	59	70	2018-04-14 20:56:34.122477	2018-04-14 20:56:34.122477
6057	59	59	72	2018-04-14 20:56:34.127234	2018-04-14 20:56:34.127234
6058	59	59	74	2018-04-14 20:56:34.131781	2018-04-14 20:56:34.131781
6059	59	59	78	2018-04-14 20:56:34.136519	2018-04-14 20:56:34.136519
6060	59	59	84	2018-04-14 20:56:34.141084	2018-04-14 20:56:34.141084
6061	59	59	85	2018-04-14 20:56:34.145775	2018-04-14 20:56:34.145775
6062	59	59	86	2018-04-14 20:56:34.15017	2018-04-14 20:56:34.15017
6063	59	59	88	2018-04-14 20:56:34.154782	2018-04-14 20:56:34.154782
6064	59	59	89	2018-04-14 20:56:34.159574	2018-04-14 20:56:34.159574
6065	59	59	91	2018-04-14 20:56:34.164056	2018-04-14 20:56:34.164056
6066	59	59	95	2018-04-14 20:56:34.16862	2018-04-14 20:56:34.16862
6067	59	76	15	2018-04-14 20:56:34.173057	2018-04-14 20:56:34.173057
6068	59	76	16	2018-04-14 20:56:34.177688	2018-04-14 20:56:34.177688
6069	59	76	17	2018-04-14 20:56:34.185594	2018-04-14 20:56:34.185594
6070	59	76	24	2018-04-14 20:56:34.190805	2018-04-14 20:56:34.190805
6071	59	76	31	2018-04-14 20:56:34.197152	2018-04-14 20:56:34.197152
6072	59	76	39	2018-04-14 20:56:34.201901	2018-04-14 20:56:34.201901
6073	59	76	41	2018-04-14 20:56:34.210059	2018-04-14 20:56:34.210059
6074	59	76	46	2018-04-14 20:56:34.215937	2018-04-14 20:56:34.215937
6075	59	76	61	2018-04-14 20:56:34.221365	2018-04-14 20:56:34.221365
6076	59	76	70	2018-04-14 20:56:34.225907	2018-04-14 20:56:34.225907
6077	59	76	72	2018-04-14 20:56:34.230477	2018-04-14 20:56:34.230477
6078	59	76	74	2018-04-14 20:56:34.235576	2018-04-14 20:56:34.235576
6079	59	76	78	2018-04-14 20:56:34.240303	2018-04-14 20:56:34.240303
6080	59	76	84	2018-04-14 20:56:34.244903	2018-04-14 20:56:34.244903
6081	59	76	85	2018-04-14 20:56:34.249352	2018-04-14 20:56:34.249352
6082	59	76	86	2018-04-14 20:56:34.253832	2018-04-14 20:56:34.253832
6083	59	76	88	2018-04-14 20:56:34.258545	2018-04-14 20:56:34.258545
6084	59	76	89	2018-04-14 20:56:34.263216	2018-04-14 20:56:34.263216
6085	59	76	91	2018-04-14 20:56:34.267951	2018-04-14 20:56:34.267951
6086	59	76	95	2018-04-14 20:56:34.272454	2018-04-14 20:56:34.272454
6087	59	93	15	2018-04-14 20:56:34.277273	2018-04-14 20:56:34.277273
6088	59	93	16	2018-04-14 20:56:34.282203	2018-04-14 20:56:34.282203
6089	59	93	17	2018-04-14 20:56:34.286631	2018-04-14 20:56:34.286631
6090	59	93	24	2018-04-14 20:56:34.291092	2018-04-14 20:56:34.291092
6091	59	93	31	2018-04-14 20:56:34.296068	2018-04-14 20:56:34.296068
6092	59	93	39	2018-04-14 20:56:34.300486	2018-04-14 20:56:34.300486
6093	59	93	41	2018-04-14 20:56:34.304981	2018-04-14 20:56:34.304981
6094	59	93	46	2018-04-14 20:56:34.309661	2018-04-14 20:56:34.309661
6095	59	93	61	2018-04-14 20:56:34.315152	2018-04-14 20:56:34.315152
6096	59	93	70	2018-04-14 20:56:34.319759	2018-04-14 20:56:34.319759
6097	59	93	72	2018-04-14 20:56:34.324369	2018-04-14 20:56:34.324369
6098	59	93	74	2018-04-14 20:56:34.329185	2018-04-14 20:56:34.329185
6099	59	93	78	2018-04-14 20:56:34.334017	2018-04-14 20:56:34.334017
6100	59	93	84	2018-04-14 20:56:34.338935	2018-04-14 20:56:34.338935
6101	59	93	85	2018-04-14 20:56:34.343984	2018-04-14 20:56:34.343984
6102	59	93	86	2018-04-14 20:56:34.348681	2018-04-14 20:56:34.348681
6103	59	93	88	2018-04-14 20:56:34.353107	2018-04-14 20:56:34.353107
6104	59	93	89	2018-04-14 20:56:34.357901	2018-04-14 20:56:34.357901
6105	59	93	91	2018-04-14 20:56:34.362792	2018-04-14 20:56:34.362792
6106	59	93	95	2018-04-14 20:56:34.367501	2018-04-14 20:56:34.367501
6107	60	41	41	2018-04-14 20:56:34.372408	2018-04-14 20:56:34.372408
6108	60	41	46	2018-04-14 20:56:34.37761	2018-04-14 20:56:34.37761
6109	60	41	61	2018-04-14 20:56:34.382298	2018-04-14 20:56:34.382298
6110	60	41	70	2018-04-14 20:56:34.388126	2018-04-14 20:56:34.388126
6111	60	41	78	2018-04-14 20:56:34.392831	2018-04-14 20:56:34.392831
6112	60	41	84	2018-04-14 20:56:34.397524	2018-04-14 20:56:34.397524
6113	60	41	91	2018-04-14 20:56:34.402205	2018-04-14 20:56:34.402205
6114	60	41	95	2018-04-14 20:56:34.406653	2018-04-14 20:56:34.406653
6115	60	42	16	2018-04-14 20:56:34.411317	2018-04-14 20:56:34.411317
6116	60	42	17	2018-04-14 20:56:34.416213	2018-04-14 20:56:34.416213
6117	60	48	41	2018-04-14 20:56:34.421103	2018-04-14 20:56:34.421103
6118	60	52	41	2018-04-14 20:56:34.425815	2018-04-14 20:56:34.425815
6119	60	98	41	2018-04-14 20:56:34.430349	2018-04-14 20:56:34.430349
6120	60	102	16	2018-04-14 20:56:34.435252	2018-04-14 20:56:34.435252
6121	60	102	17	2018-04-14 20:56:34.43996	2018-04-14 20:56:34.43996
6122	61	1	5	2018-04-14 20:56:34.444538	2018-04-14 20:56:34.444538
6123	61	5	5	2018-04-14 20:56:34.449284	2018-04-14 20:56:34.449284
6124	61	43	1	2018-04-14 20:56:34.453999	2018-04-14 20:56:34.453999
6125	61	43	2	2018-04-14 20:56:34.460065	2018-04-14 20:56:34.460065
6126	61	43	4	2018-04-14 20:56:34.465792	2018-04-14 20:56:34.465792
6127	61	43	5	2018-04-14 20:56:34.471154	2018-04-14 20:56:34.471154
6128	61	43	36	2018-04-14 20:56:34.476768	2018-04-14 20:56:34.476768
6129	61	43	40	2018-04-14 20:56:34.482142	2018-04-14 20:56:34.482142
6130	61	43	59	2018-04-14 20:56:34.48718	2018-04-14 20:56:34.48718
6131	61	43	60	2018-04-14 20:56:34.492132	2018-04-14 20:56:34.492132
6132	61	43	64	2018-04-14 20:56:34.49731	2018-04-14 20:56:34.49731
6133	61	43	67	2018-04-14 20:56:34.502232	2018-04-14 20:56:34.502232
6134	61	43	76	2018-04-14 20:56:34.507475	2018-04-14 20:56:34.507475
6135	61	43	77	2018-04-14 20:56:34.51248	2018-04-14 20:56:34.51248
6136	61	43	80	2018-04-14 20:56:34.526655	2018-04-14 20:56:34.526655
6137	61	43	93	2018-04-14 20:56:34.532932	2018-04-14 20:56:34.532932
6138	61	59	5	2018-04-14 20:56:34.539612	2018-04-14 20:56:34.539612
6139	61	67	1	2018-04-14 20:56:34.544504	2018-04-14 20:56:34.544504
6140	61	67	2	2018-04-14 20:56:34.54911	2018-04-14 20:56:34.54911
6141	61	67	4	2018-04-14 20:56:34.553728	2018-04-14 20:56:34.553728
6142	61	67	5	2018-04-14 20:56:34.558345	2018-04-14 20:56:34.558345
6143	61	67	59	2018-04-14 20:56:34.563379	2018-04-14 20:56:34.563379
6144	61	67	60	2018-04-14 20:56:34.568414	2018-04-14 20:56:34.568414
6145	61	67	64	2018-04-14 20:56:34.576662	2018-04-14 20:56:34.576662
6146	61	67	76	2018-04-14 20:56:34.583944	2018-04-14 20:56:34.583944
6147	61	67	77	2018-04-14 20:56:34.588895	2018-04-14 20:56:34.588895
6148	61	67	80	2018-04-14 20:56:34.596218	2018-04-14 20:56:34.596218
6149	61	67	93	2018-04-14 20:56:34.601882	2018-04-14 20:56:34.601882
6150	61	73	64	2018-04-14 20:56:34.60647	2018-04-14 20:56:34.60647
6151	61	73	80	2018-04-14 20:56:34.611152	2018-04-14 20:56:34.611152
6152	61	75	1	2018-04-14 20:56:34.617256	2018-04-14 20:56:34.617256
6153	61	75	2	2018-04-14 20:56:34.622968	2018-04-14 20:56:34.622968
6154	61	75	4	2018-04-14 20:56:34.627719	2018-04-14 20:56:34.627719
6155	61	75	5	2018-04-14 20:56:34.632221	2018-04-14 20:56:34.632221
6156	61	75	36	2018-04-14 20:56:34.636802	2018-04-14 20:56:34.636802
6157	61	75	40	2018-04-14 20:56:34.641637	2018-04-14 20:56:34.641637
6158	61	75	43	2018-04-14 20:56:34.646394	2018-04-14 20:56:34.646394
6159	61	75	59	2018-04-14 20:56:34.651271	2018-04-14 20:56:34.651271
6160	61	75	60	2018-04-14 20:56:34.656858	2018-04-14 20:56:34.656858
6161	61	75	64	2018-04-14 20:56:34.661469	2018-04-14 20:56:34.661469
6162	61	75	67	2018-04-14 20:56:34.669254	2018-04-14 20:56:34.669254
6163	61	75	75	2018-04-14 20:56:34.676003	2018-04-14 20:56:34.676003
6164	61	75	76	2018-04-14 20:56:34.681033	2018-04-14 20:56:34.681033
6165	61	75	77	2018-04-14 20:56:34.685682	2018-04-14 20:56:34.685682
6166	61	75	80	2018-04-14 20:56:34.691107	2018-04-14 20:56:34.691107
6167	61	75	93	2018-04-14 20:56:34.700042	2018-04-14 20:56:34.700042
6168	61	75	112	2018-04-14 20:56:34.705439	2018-04-14 20:56:34.705439
6169	61	76	5	2018-04-14 20:56:34.710045	2018-04-14 20:56:34.710045
6170	61	93	5	2018-04-14 20:56:34.714858	2018-04-14 20:56:34.714858
6171	61	112	1	2018-04-14 20:56:34.720598	2018-04-14 20:56:34.720598
6172	61	112	2	2018-04-14 20:56:34.726857	2018-04-14 20:56:34.726857
6173	61	112	4	2018-04-14 20:56:34.7316	2018-04-14 20:56:34.7316
6174	61	112	5	2018-04-14 20:56:34.73629	2018-04-14 20:56:34.73629
6175	61	112	59	2018-04-14 20:56:34.743707	2018-04-14 20:56:34.743707
6176	61	112	60	2018-04-14 20:56:34.751892	2018-04-14 20:56:34.751892
6177	61	112	64	2018-04-14 20:56:34.757044	2018-04-14 20:56:34.757044
6178	61	112	76	2018-04-14 20:56:34.761516	2018-04-14 20:56:34.761516
6179	61	112	77	2018-04-14 20:56:34.766187	2018-04-14 20:56:34.766187
6180	61	112	80	2018-04-14 20:56:34.774397	2018-04-14 20:56:34.774397
6181	61	112	93	2018-04-14 20:56:34.781042	2018-04-14 20:56:34.781042
6182	62	1	23	2018-04-14 20:56:34.786399	2018-04-14 20:56:34.786399
6183	62	1	34	2018-04-14 20:56:34.79099	2018-04-14 20:56:34.79099
6184	62	5	34	2018-04-14 20:56:34.79568	2018-04-14 20:56:34.79568
6185	62	8	23	2018-04-14 20:56:34.800466	2018-04-14 20:56:34.800466
6186	62	8	34	2018-04-14 20:56:34.805106	2018-04-14 20:56:34.805106
6187	62	10	23	2018-04-14 20:56:34.809832	2018-04-14 20:56:34.809832
6188	62	10	34	2018-04-14 20:56:34.817736	2018-04-14 20:56:34.817736
6189	62	23	2	2018-04-14 20:56:34.824059	2018-04-14 20:56:34.824059
6190	62	23	4	2018-04-14 20:56:34.830409	2018-04-14 20:56:34.830409
6191	62	23	40	2018-04-14 20:56:34.835382	2018-04-14 20:56:34.835382
6192	62	23	60	2018-04-14 20:56:34.840215	2018-04-14 20:56:34.840215
6193	62	23	64	2018-04-14 20:56:34.844632	2018-04-14 20:56:34.844632
6194	62	23	77	2018-04-14 20:56:34.84937	2018-04-14 20:56:34.84937
6195	62	23	80	2018-04-14 20:56:34.85431	2018-04-14 20:56:34.85431
6196	62	25	23	2018-04-14 20:56:34.859242	2018-04-14 20:56:34.859242
6197	62	25	34	2018-04-14 20:56:34.863922	2018-04-14 20:56:34.863922
6198	62	26	23	2018-04-14 20:56:34.868773	2018-04-14 20:56:34.868773
6199	62	26	108	2018-04-14 20:56:34.876374	2018-04-14 20:56:34.876374
6200	62	30	23	2018-04-14 20:56:34.88212	2018-04-14 20:56:34.88212
6201	62	30	34	2018-04-14 20:56:34.886821	2018-04-14 20:56:34.886821
6202	62	34	2	2018-04-14 20:56:34.892244	2018-04-14 20:56:34.892244
6203	62	34	4	2018-04-14 20:56:34.899086	2018-04-14 20:56:34.899086
6204	62	34	12	2018-04-14 20:56:34.904995	2018-04-14 20:56:34.904995
6205	62	34	36	2018-04-14 20:56:34.910517	2018-04-14 20:56:34.910517
6206	62	34	40	2018-04-14 20:56:34.916576	2018-04-14 20:56:34.916576
6207	62	34	53	2018-04-14 20:56:34.92319	2018-04-14 20:56:34.92319
6208	62	34	60	2018-04-14 20:56:34.928872	2018-04-14 20:56:34.928872
6209	62	34	64	2018-04-14 20:56:34.934151	2018-04-14 20:56:34.934151
6210	62	34	77	2018-04-14 20:56:34.938929	2018-04-14 20:56:34.938929
6211	62	34	80	2018-04-14 20:56:34.943458	2018-04-14 20:56:34.943458
6212	62	34	87	2018-04-14 20:56:34.948178	2018-04-14 20:56:34.948178
6213	62	34	92	2018-04-14 20:56:34.9528	2018-04-14 20:56:34.9528
6214	62	34	103	2018-04-14 20:56:34.957165	2018-04-14 20:56:34.957165
6215	62	34	109	2018-04-14 20:56:34.961843	2018-04-14 20:56:34.961843
6216	62	44	23	2018-04-14 20:56:34.966699	2018-04-14 20:56:34.966699
6217	62	44	34	2018-04-14 20:56:34.971384	2018-04-14 20:56:34.971384
6218	62	50	108	2018-04-14 20:56:34.975971	2018-04-14 20:56:34.975971
6219	62	59	23	2018-04-14 20:56:34.980853	2018-04-14 20:56:34.980853
6220	62	59	34	2018-04-14 20:56:34.985717	2018-04-14 20:56:34.985717
6221	62	76	23	2018-04-14 20:56:34.990079	2018-04-14 20:56:34.990079
6222	62	76	34	2018-04-14 20:56:34.995022	2018-04-14 20:56:34.995022
6223	62	79	23	2018-04-14 20:56:35.000742	2018-04-14 20:56:35.000742
6224	62	79	34	2018-04-14 20:56:35.010043	2018-04-14 20:56:35.010043
6225	62	90	23	2018-04-14 20:56:35.018012	2018-04-14 20:56:35.018012
6226	62	90	34	2018-04-14 20:56:35.028828	2018-04-14 20:56:35.028828
6227	62	93	23	2018-04-14 20:56:35.040227	2018-04-14 20:56:35.040227
6228	62	93	34	2018-04-14 20:56:35.051072	2018-04-14 20:56:35.051072
6229	62	108	64	2018-04-14 20:56:35.061463	2018-04-14 20:56:35.061463
6230	62	108	80	2018-04-14 20:56:35.077378	2018-04-14 20:56:35.077378
6231	63	7	3	2018-04-14 20:56:35.082376	2018-04-14 20:56:35.082376
6232	63	7	6	2018-04-14 20:56:35.087381	2018-04-14 20:56:35.087381
6233	63	7	20	2018-04-14 20:56:35.092237	2018-04-14 20:56:35.092237
6234	63	7	22	2018-04-14 20:56:35.097352	2018-04-14 20:56:35.097352
6235	63	7	29	2018-04-14 20:56:35.102197	2018-04-14 20:56:35.102197
6236	63	7	37	2018-04-14 20:56:35.106961	2018-04-14 20:56:35.106961
6237	63	7	38	2018-04-14 20:56:35.112202	2018-04-14 20:56:35.112202
6238	63	7	45	2018-04-14 20:56:35.117219	2018-04-14 20:56:35.117219
6239	63	7	55	2018-04-14 20:56:35.122202	2018-04-14 20:56:35.122202
6240	63	7	56	2018-04-14 20:56:35.127423	2018-04-14 20:56:35.127423
6241	63	7	57	2018-04-14 20:56:35.132325	2018-04-14 20:56:35.132325
6242	63	7	58	2018-04-14 20:56:35.13759	2018-04-14 20:56:35.13759
6243	63	7	65	2018-04-14 20:56:35.142413	2018-04-14 20:56:35.142413
6244	63	7	69	2018-04-14 20:56:35.147317	2018-04-14 20:56:35.147317
6245	63	7	71	2018-04-14 20:56:35.152419	2018-04-14 20:56:35.152419
6246	63	7	81	2018-04-14 20:56:35.157212	2018-04-14 20:56:35.157212
6247	63	7	94	2018-04-14 20:56:35.161984	2018-04-14 20:56:35.161984
6248	63	7	99	2018-04-14 20:56:35.167033	2018-04-14 20:56:35.167033
6249	63	7	101	2018-04-14 20:56:35.171923	2018-04-14 20:56:35.171923
6250	63	7	106	2018-04-14 20:56:35.176817	2018-04-14 20:56:35.176817
6251	63	7	107	2018-04-14 20:56:35.181664	2018-04-14 20:56:35.181664
6252	63	7	110	2018-04-14 20:56:35.186693	2018-04-14 20:56:35.186693
6253	63	7	111	2018-04-14 20:56:35.191689	2018-04-14 20:56:35.191689
6254	63	9	3	2018-04-14 20:56:35.19652	2018-04-14 20:56:35.19652
6255	63	9	20	2018-04-14 20:56:35.201429	2018-04-14 20:56:35.201429
6256	63	9	29	2018-04-14 20:56:35.206279	2018-04-14 20:56:35.206279
6257	63	9	37	2018-04-14 20:56:35.211326	2018-04-14 20:56:35.211326
6258	63	9	45	2018-04-14 20:56:35.216265	2018-04-14 20:56:35.216265
6259	63	9	54	2018-04-14 20:56:35.221114	2018-04-14 20:56:35.221114
6260	63	9	55	2018-04-14 20:56:35.225914	2018-04-14 20:56:35.225914
6261	63	9	56	2018-04-14 20:56:35.230647	2018-04-14 20:56:35.230647
6262	63	9	57	2018-04-14 20:56:35.235634	2018-04-14 20:56:35.235634
6263	63	9	58	2018-04-14 20:56:35.24043	2018-04-14 20:56:35.24043
6264	63	9	65	2018-04-14 20:56:35.24532	2018-04-14 20:56:35.24532
6265	63	9	69	2018-04-14 20:56:35.256695	2018-04-14 20:56:35.256695
6266	63	9	81	2018-04-14 20:56:35.2715	2018-04-14 20:56:35.2715
6267	63	9	94	2018-04-14 20:56:35.278662	2018-04-14 20:56:35.278662
6268	63	9	99	2018-04-14 20:56:35.284218	2018-04-14 20:56:35.284218
6269	63	9	101	2018-04-14 20:56:35.289086	2018-04-14 20:56:35.289086
6270	63	9	107	2018-04-14 20:56:35.293387	2018-04-14 20:56:35.293387
6271	63	47	3	2018-04-14 20:56:35.299641	2018-04-14 20:56:35.299641
6272	63	47	12	2018-04-14 20:56:35.305392	2018-04-14 20:56:35.305392
6273	63	47	20	2018-04-14 20:56:35.309898	2018-04-14 20:56:35.309898
6274	63	47	29	2018-04-14 20:56:35.314408	2018-04-14 20:56:35.314408
6275	63	47	37	2018-04-14 20:56:35.318638	2018-04-14 20:56:35.318638
6276	63	47	45	2018-04-14 20:56:35.323317	2018-04-14 20:56:35.323317
6277	63	47	53	2018-04-14 20:56:35.330316	2018-04-14 20:56:35.330316
6278	63	47	54	2018-04-14 20:56:35.336229	2018-04-14 20:56:35.336229
6279	63	47	55	2018-04-14 20:56:35.340876	2018-04-14 20:56:35.340876
6280	63	47	56	2018-04-14 20:56:35.345716	2018-04-14 20:56:35.345716
6281	63	47	57	2018-04-14 20:56:35.350479	2018-04-14 20:56:35.350479
6282	63	47	58	2018-04-14 20:56:35.357399	2018-04-14 20:56:35.357399
6283	63	47	65	2018-04-14 20:56:35.362198	2018-04-14 20:56:35.362198
6284	63	47	69	2018-04-14 20:56:35.367137	2018-04-14 20:56:35.367137
6285	63	47	81	2018-04-14 20:56:35.371918	2018-04-14 20:56:35.371918
6286	63	47	87	2018-04-14 20:56:35.376505	2018-04-14 20:56:35.376505
6287	63	47	92	2018-04-14 20:56:35.381408	2018-04-14 20:56:35.381408
6288	63	47	94	2018-04-14 20:56:35.386181	2018-04-14 20:56:35.386181
6289	63	47	99	2018-04-14 20:56:35.39069	2018-04-14 20:56:35.39069
6290	63	47	101	2018-04-14 20:56:35.395126	2018-04-14 20:56:35.395126
6291	63	47	107	2018-04-14 20:56:35.399718	2018-04-14 20:56:35.399718
6292	63	47	109	2018-04-14 20:56:35.404512	2018-04-14 20:56:35.404512
\.


--
-- Name: mix_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Kieran
--

SELECT pg_catalog.setval('mix_items_id_seq', 6292, true);


--
-- Data for Name: mixes; Type: TABLE DATA; Schema: public; Owner: Kieran
--

COPY mixes (id, name, description, slug, created_at, updated_at) FROM stdin;
1	Abaddon Flame	3 powerful Fire strikes with negative status effects	abaddon-flame	2018-04-14 20:56:00.114523	2018-04-15 17:48:56.595753
2	Aqua Toxin	3 large Water strikes with negative status effects	aqua-toxin	2018-04-14 20:56:00.123405	2018-04-15 17:48:56.60175
3	Black Hole	Reduces HP of all enemies by 15/16ths	black-hole	2018-04-14 20:56:00.125747	2018-04-15 17:48:56.604841
4	Blaster Mine	Small damage with negative status effects on all enemies	blaster-mine	2018-04-14 20:56:00.127542	2018-04-15 17:48:56.608068
5	Brimstone	3 large Fire strikes with negative status effects	brimstone	2018-04-14 20:56:00.129645	2018-04-15 17:48:56.611147
6	Burning Soul	9 small Fire strikes	burning-soul	2018-04-14 20:56:00.131491	2018-04-15 17:48:56.613814
7	Calamity Bomb	Medium damage with negative status effects on all enemies	calamity-bomb	2018-04-14 20:56:00.133408	2018-04-15 17:48:56.616666
8	Chaos Grenade	Large damage with negative status effects on all enemies	chaos-grenade	2018-04-14 20:56:00.135179	2018-04-15 17:48:56.619226
9	Cluster Bomb	Large damage on all enemies	cluster-bomb	2018-04-14 20:56:00.137158	2018-04-15 17:48:56.622232
10	Dark Rain	3 powerful Water strikes with negative status effects	dark-rain	2018-04-14 20:56:00.139189	2018-04-15 17:48:56.625927
11	Eccentric	Overdrive gauge of all allies charges twice as fast	eccentric	2018-04-14 20:56:00.141134	2018-04-15 17:48:56.628825
12	Electroshock	3 large Thunder strikes with negative status effects	electroshock	2018-04-14 20:56:00.143177	2018-04-15 17:48:56.632557
13	Elixir	Restores one ally to full HP and MP	elixir	2018-04-14 20:56:00.145093	2018-04-15 17:48:56.635148
14	Final Elixir	Restores all allies to full HP and MP, cures all negative status effects and revives all dead allies	final-elixir	2018-04-14 20:56:00.147125	2018-04-15 17:48:56.637941
15	Final Phoenix	Revivies all dead allies with full HP	final-phoenix	2018-04-14 20:56:00.149109	2018-04-15 17:48:56.641625
16	Firestorm	6 small Fire strikes	firestorm	2018-04-14 20:56:00.151149	2018-04-15 17:48:56.644763
17	Flash Flood	6 small Water strikes	flash-flood	2018-04-14 20:56:00.153035	2018-04-15 17:48:56.648064
18	Frag Grenade	Small damage and Armor Break on all enemies	frag-grenade	2018-04-14 20:56:00.154809	2018-04-15 17:48:56.650761
19	Freedom	Abilities cost 0 MP for one ally	freedom	2018-04-14 20:56:00.156821	2018-04-15 17:48:56.65361
20	FreedomX	Abilities cost 0 MP for all allies	freedomx	2018-04-14 20:56:00.158759	2018-04-15 17:48:56.656248
21	Grenade	Small damage on all enemies	grenade	2018-04-14 20:56:00.160778	2018-04-15 17:48:56.658799
22	Hazardous Ice	3 large Ice strikes with negative status effects	hazardous-ice	2018-04-14 20:56:00.162632	2018-04-15 17:48:56.661366
23	Hazardous Shell	Small damage with negative status effects on all enemies	hazardous-shell	2018-04-14 20:56:00.164427	2018-04-15 17:48:56.664022
24	Heat Blaster	5 small Fire strikes	heat-blaster	2018-04-14 20:56:00.166218	2018-04-15 17:48:56.666512
25	Hero Drink	Increases critical hit rate of one ally	hero-drink	2018-04-14 20:56:00.168063	2018-04-15 17:48:56.669492
26	Hot Spurs	Overdrive gauge on one ally charges twice as fast	hot-spurs	2018-04-14 20:56:00.170154	2018-04-15 17:48:56.672195
27	Hyper Mana	Casts Focus and doubles max MP of all allies	hyper-mana	2018-04-14 20:56:00.172256	2018-04-15 17:48:56.674976
28	Hyper Mighty G	Casts Shell, Protect, Haste, Regen, and Auto-Life on all allies	hyper-mighty-g	2018-04-14 20:56:00.17437	2018-04-15 17:48:56.67748
29	Hyper NulAll	Casts Cheer, Focus, NulBlaze, NulShock, NulTide and NulFrost on all allies	hyper-nulall	2018-04-14 20:56:00.176225	2018-04-15 17:48:56.680349
30	Hyper Vitality	Casts Cheer and doubles max HP of all allies	hyper-vitality	2018-04-14 20:56:00.178229	2018-04-15 17:48:56.683075
31	Ice Blizzard	6 small Ice strikes	ice-blizzard	2018-04-14 20:56:00.180119	2018-04-15 17:48:56.685669
32	Krysta	3 powerful Ice strikes with negative status effects	krysta	2018-04-14 20:56:00.182057	2018-04-15 17:48:56.688771
33	Lightning Bolt	9 small Thunder strikes	lightning-bolt	2018-04-14 20:56:00.184102	2018-04-15 17:48:56.691617
34	Mana	Doubles max MP of one ally	mana	2018-04-14 20:56:00.18602	2018-04-15 17:48:56.69454
35	Mega Mana	Doubles max MP of all allies	mega-mana	2018-04-14 20:56:00.188132	2018-04-15 17:48:56.697264
36	Mega NulAll	Casts NulBlaze, NulShock, NulTide and NulFrost on all allies	mega-nulall	2018-04-14 20:56:00.190278	2018-04-15 17:48:56.700681
37	Mega Phoenix	Revives all dead allies	mega-phoenix	2018-04-14 20:56:00.192231	2018-04-15 17:48:56.70374
38	Mega Vitality	Doubles max HP of all allies	mega-vitality	2018-04-14 20:56:00.194124	2018-04-15 17:48:56.708136
39	Megalixir	Restores all allies to full HP and MP	megalixir	2018-04-14 20:56:00.196133	2018-04-15 17:48:56.71194
40	Mighty G	Casts Shell, Protect and Haste on all allies	mighty-g	2018-04-14 20:56:00.197976	2018-04-15 17:48:56.714696
41	Mighty Wall	Casts Shell and Protect on all allies	mighty-wall	2018-04-14 20:56:00.199951	2018-04-15 17:48:56.717326
42	Miracle Drink	Increases critical hit rate of all allies	miracle-drink	2018-04-14 20:56:00.202427	2018-04-15 17:48:56.720017
43	Nega Burst	Reduces HP of all enemies by 3/4ths	nega-burst	2018-04-14 20:56:00.205808	2018-04-15 17:48:56.72342
44	NulAll	Casts NulBlaze, NulShock, NulTide and NulFrost on one ally	nulall	2018-04-14 20:56:00.207868	2018-04-15 17:48:56.726178
45	Panacea	Cures all negative status effects of all allies	panacea	2018-04-14 20:56:00.209891	2018-04-15 17:48:56.72892
46	Pineapple	Medium damage on all enemies	pineapple	2018-04-14 20:56:00.211754	2018-04-15 17:48:56.731587
47	Potato Masher	Large damage on all enemies	potato-masher	2018-04-14 20:56:00.213586	2018-04-15 17:48:56.734096
48	Quartet of 9	One ally hits for 9999 damage every time	quartet-of-9	2018-04-14 20:56:00.21555	2018-04-15 17:48:56.736676
49	Rolling Thunder	6 small Thunder strikes	rolling-thunder	2018-04-14 20:56:00.217608	2018-04-15 17:48:56.739213
50	Snow Flurry	5 small Thunder strikes	snow-flurry	2018-04-14 20:56:00.219542	2018-04-15 17:48:56.741913
51	Super Elixir	Restores all allies to full HP and MP and cures all negative status effects	super-elixir	2018-04-14 20:56:00.221567	2018-04-15 17:48:56.744858
52	Super Mighty G	Casts Shell, Protect, Haste, and Regen on all allies	super-mighty-g	2018-04-14 20:56:00.223677	2018-04-15 17:48:56.747666
53	Supernova	Extreme damage to all enemies	supernova	2018-04-14 20:56:00.225694	2018-04-15 17:48:56.75039
54	Tallboy	Large damage to all enemies	tallboy	2018-04-14 20:56:00.227579	2018-04-15 17:48:56.753224
55	Thunderblast	3 powerful Thunder strikes with negative status effects	thunderblast	2018-04-14 20:56:00.234559	2018-04-15 17:48:56.755962
56	Thunderbolt	5 small Thunder strikes	thunderbolt	2018-04-14 20:56:00.236692	2018-04-15 17:48:56.759205
57	Tidal Wave	9 small Water strikes	tidal-wave	2018-04-14 20:56:00.238567	2018-04-15 17:48:56.762175
58	Trio of 9999	All allies hit for 9999 damage every time	trio-of-9999	2018-04-14 20:56:00.24045	2018-04-15 17:48:56.765892
59	Ultra Cure	Restores all allies to full HP and cures any negative status effects	ultra-cure	2018-04-14 20:56:00.242324	2018-04-15 17:48:56.768693
60	Ultra NulAll	Casts Cheer, Focus, Aim, Reflex, NulBlaze, NulShock, NulTide and NulFrost on all allies	ultra-nulall	2018-04-14 20:56:00.244079	2018-04-15 17:48:56.771613
61	Ultra Potion	Restores all allies to full HP	ultra-potion	2018-04-14 20:56:00.245836	2018-04-15 17:48:56.775125
62	Waterfall	5 small Water strikes	waterfall	2018-04-14 20:56:00.247733	2018-04-15 17:48:56.778203
63	Winter Storm	9 small Water strikes	winter-storm	2018-04-14 20:56:00.24945	2018-04-15 17:48:56.781108
\.


--
-- Name: mixes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Kieran
--

SELECT pg_catalog.setval('mixes_id_seq', 63, true);


--
-- Data for Name: monster_drop_abilities; Type: TABLE DATA; Schema: public; Owner: Kieran
--

COPY monster_drop_abilities (id, monster_id, ability_id, ability_type, created_at, updated_at) FROM stdin;
1	1	64	weapon	2018-04-14 20:55:53.306796	2018-04-14 20:55:53.306796
2	1	62	weapon	2018-04-14 20:55:53.321315	2018-04-14 20:55:53.321315
3	1	61	weapon	2018-04-14 20:55:53.327659	2018-04-14 20:55:53.327659
4	1	58	armour	2018-04-14 20:55:53.332566	2018-04-14 20:55:53.332566
5	1	56	armour	2018-04-14 20:55:53.336605	2018-04-14 20:55:53.336605
6	1	55	armour	2018-04-14 20:55:53.340616	2018-04-14 20:55:53.340616
7	2	30	weapon	2018-04-14 20:55:53.344531	2018-04-14 20:55:53.344531
8	2	28	weapon	2018-04-14 20:55:53.348747	2018-04-14 20:55:53.348747
9	2	27	weapon	2018-04-14 20:55:53.352517	2018-04-14 20:55:53.352517
10	2	110	armour	2018-04-14 20:55:53.356374	2018-04-14 20:55:53.356374
11	2	108	armour	2018-04-14 20:55:53.360119	2018-04-14 20:55:53.360119
12	2	107	armour	2018-04-14 20:55:53.364109	2018-04-14 20:55:53.364109
13	3	66	weapon	2018-04-14 20:55:53.368132	2018-04-14 20:55:53.368132
14	3	3	weapon	2018-04-14 20:55:53.371737	2018-04-14 20:55:53.371737
15	3	110	armour	2018-04-14 20:55:53.375763	2018-04-14 20:55:53.375763
16	3	75	armour	2018-04-14 20:55:53.379821	2018-04-14 20:55:53.379821
17	3	58	armour	2018-04-14 20:55:53.385593	2018-04-14 20:55:53.385593
18	3	55	armour	2018-04-14 20:55:53.390388	2018-04-14 20:55:53.390388
19	4	102	weapon	2018-04-14 20:55:53.394059	2018-04-14 20:55:53.394059
20	4	99	weapon	2018-04-14 20:55:53.398152	2018-04-14 20:55:53.398152
21	4	42	weapon	2018-04-14 20:55:53.40209	2018-04-14 20:55:53.40209
22	4	75	armour	2018-04-14 20:55:53.406106	2018-04-14 20:55:53.406106
23	4	40	armour	2018-04-14 20:55:53.409831	2018-04-14 20:55:53.409831
24	5	64	weapon	2018-04-14 20:55:53.416041	2018-04-14 20:55:53.416041
25	5	53	weapon	2018-04-14 20:55:53.420065	2018-04-14 20:55:53.420065
26	5	52	weapon	2018-04-14 20:55:53.423755	2018-04-14 20:55:53.423755
27	5	110	armour	2018-04-14 20:55:53.427578	2018-04-14 20:55:53.427578
28	5	75	armour	2018-04-14 20:55:53.431425	2018-04-14 20:55:53.431425
29	5	58	armour	2018-04-14 20:55:53.435189	2018-04-14 20:55:53.435189
30	5	54	armour	2018-04-14 20:55:53.439042	2018-04-14 20:55:53.439042
31	6	15	weapon	2018-04-14 20:55:53.442813	2018-04-14 20:55:53.442813
32	6	116	armour	2018-04-14 20:55:53.446764	2018-04-14 20:55:53.446764
33	6	109	armour	2018-04-14 20:55:53.450713	2018-04-14 20:55:53.450713
34	6	75	armour	2018-04-14 20:55:53.454394	2018-04-14 20:55:53.454394
35	6	57	armour	2018-04-14 20:55:53.458232	2018-04-14 20:55:53.458232
36	6	54	armour	2018-04-14 20:55:53.46201	2018-04-14 20:55:53.46201
37	6	49	armour	2018-04-14 20:55:53.465967	2018-04-14 20:55:53.465967
38	6	37	armour	2018-04-14 20:55:53.46979	2018-04-14 20:55:53.46979
39	7	64	weapon	2018-04-14 20:55:53.473482	2018-04-14 20:55:53.473482
40	7	19	weapon	2018-04-14 20:55:53.479486	2018-04-14 20:55:53.479486
41	7	109	armour	2018-04-14 20:55:53.483758	2018-04-14 20:55:53.483758
42	7	80	armour	2018-04-14 20:55:53.487619	2018-04-14 20:55:53.487619
43	7	75	armour	2018-04-14 20:55:53.4915	2018-04-14 20:55:53.4915
44	7	57	armour	2018-04-14 20:55:53.49546	2018-04-14 20:55:53.49546
45	8	103	weapon	2018-04-14 20:55:53.499592	2018-04-14 20:55:53.499592
46	8	110	armour	2018-04-14 20:55:53.503311	2018-04-14 20:55:53.503311
47	8	106	armour	2018-04-14 20:55:53.507119	2018-04-14 20:55:53.507119
48	8	75	armour	2018-04-14 20:55:53.510764	2018-04-14 20:55:53.510764
49	8	58	armour	2018-04-14 20:55:53.514705	2018-04-14 20:55:53.514705
50	9	85	weapon	2018-04-14 20:55:53.51851	2018-04-14 20:55:53.51851
51	9	81	weapon	2018-04-14 20:55:53.522363	2018-04-14 20:55:53.522363
52	9	19	weapon	2018-04-14 20:55:53.526176	2018-04-14 20:55:53.526176
53	9	88	armour	2018-04-14 20:55:53.530014	2018-04-14 20:55:53.530014
54	9	84	armour	2018-04-14 20:55:53.533914	2018-04-14 20:55:53.533914
55	9	75	armour	2018-04-14 20:55:53.538234	2018-04-14 20:55:53.538234
56	9	22	armour	2018-04-14 20:55:53.5421	2018-04-14 20:55:53.5421
57	10	114	weapon	2018-04-14 20:55:53.546018	2018-04-14 20:55:53.546018
58	10	116	armour	2018-04-14 20:55:53.550325	2018-04-14 20:55:53.550325
59	10	75	armour	2018-04-14 20:55:53.554406	2018-04-14 20:55:53.554406
60	11	85	weapon	2018-04-14 20:55:53.558393	2018-04-14 20:55:53.558393
61	11	81	weapon	2018-04-14 20:55:53.563044	2018-04-14 20:55:53.563044
62	11	116	armour	2018-04-14 20:55:53.568575	2018-04-14 20:55:53.568575
63	11	109	armour	2018-04-14 20:55:53.572709	2018-04-14 20:55:53.572709
64	11	75	armour	2018-04-14 20:55:53.576549	2018-04-14 20:55:53.576549
65	11	57	armour	2018-04-14 20:55:53.580776	2018-04-14 20:55:53.580776
66	11	54	armour	2018-04-14 20:55:53.586992	2018-04-14 20:55:53.586992
67	11	49	armour	2018-04-14 20:55:53.591247	2018-04-14 20:55:53.591247
68	11	37	armour	2018-04-14 20:55:53.599294	2018-04-14 20:55:53.599294
69	12	42	weapon	2018-04-14 20:55:53.603463	2018-04-14 20:55:53.603463
70	12	75	armour	2018-04-14 20:55:53.607429	2018-04-14 20:55:53.607429
71	13	93	weapon	2018-04-14 20:55:53.61106	2018-04-14 20:55:53.61106
72	13	41	weapon	2018-04-14 20:55:53.615286	2018-04-14 20:55:53.615286
73	13	110	armour	2018-04-14 20:55:53.619506	2018-04-14 20:55:53.619506
74	13	75	armour	2018-04-14 20:55:53.623412	2018-04-14 20:55:53.623412
75	13	58	armour	2018-04-14 20:55:53.62716	2018-04-14 20:55:53.62716
76	13	17	armour	2018-04-14 20:55:53.631154	2018-04-14 20:55:53.631154
77	14	103	weapon	2018-04-14 20:55:53.635111	2018-04-14 20:55:53.635111
78	14	69	weapon	2018-04-14 20:55:53.639105	2018-04-14 20:55:53.639105
79	14	109	armour	2018-04-14 20:55:53.64305	2018-04-14 20:55:53.64305
80	14	106	armour	2018-04-14 20:55:53.647224	2018-04-14 20:55:53.647224
81	14	75	armour	2018-04-14 20:55:53.651425	2018-04-14 20:55:53.651425
82	14	57	armour	2018-04-14 20:55:53.655348	2018-04-14 20:55:53.655348
83	15	69	weapon	2018-04-14 20:55:53.659099	2018-04-14 20:55:53.659099
84	15	15	weapon	2018-04-14 20:55:53.66364	2018-04-14 20:55:53.66364
85	15	116	armour	2018-04-14 20:55:53.668976	2018-04-14 20:55:53.668976
86	15	110	armour	2018-04-14 20:55:53.672831	2018-04-14 20:55:53.672831
87	15	109	armour	2018-04-14 20:55:53.676382	2018-04-14 20:55:53.676382
88	15	58	armour	2018-04-14 20:55:53.680185	2018-04-14 20:55:53.680185
89	15	57	armour	2018-04-14 20:55:53.684745	2018-04-14 20:55:53.684745
90	15	54	armour	2018-04-14 20:55:53.688772	2018-04-14 20:55:53.688772
91	15	49	armour	2018-04-14 20:55:53.692668	2018-04-14 20:55:53.692668
92	15	37	armour	2018-04-14 20:55:53.696377	2018-04-14 20:55:53.696377
93	16	102	weapon	2018-04-14 20:55:53.701056	2018-04-14 20:55:53.701056
94	16	100	weapon	2018-04-14 20:55:53.70483	2018-04-14 20:55:53.70483
95	16	99	weapon	2018-04-14 20:55:53.708644	2018-04-14 20:55:53.708644
96	16	120	armour	2018-04-14 20:55:53.712428	2018-04-14 20:55:53.712428
97	16	110	armour	2018-04-14 20:55:53.717017	2018-04-14 20:55:53.717017
98	16	75	armour	2018-04-14 20:55:53.720676	2018-04-14 20:55:53.720676
99	16	58	armour	2018-04-14 20:55:53.724399	2018-04-14 20:55:53.724399
100	17	102	weapon	2018-04-14 20:55:53.728166	2018-04-14 20:55:53.728166
101	17	100	weapon	2018-04-14 20:55:53.732317	2018-04-14 20:55:53.732317
102	17	99	weapon	2018-04-14 20:55:53.736075	2018-04-14 20:55:53.736075
103	17	120	armour	2018-04-14 20:55:53.739797	2018-04-14 20:55:53.739797
104	17	98	armour	2018-04-14 20:55:53.743556	2018-04-14 20:55:53.743556
105	17	75	armour	2018-04-14 20:55:53.748923	2018-04-14 20:55:53.748923
106	18	67	weapon	2018-04-14 20:55:53.753459	2018-04-14 20:55:53.753459
107	18	75	armour	2018-04-14 20:55:53.75757	2018-04-14 20:55:53.75757
108	19	76	weapon	2018-04-14 20:55:53.761461	2018-04-14 20:55:53.761461
109	19	69	weapon	2018-04-14 20:55:53.765805	2018-04-14 20:55:53.765805
110	19	116	armour	2018-04-14 20:55:53.769678	2018-04-14 20:55:53.769678
111	19	109	armour	2018-04-14 20:55:53.774365	2018-04-14 20:55:53.774365
112	19	75	armour	2018-04-14 20:55:53.784308	2018-04-14 20:55:53.784308
113	19	57	armour	2018-04-14 20:55:53.794552	2018-04-14 20:55:53.794552
114	19	54	armour	2018-04-14 20:55:53.803157	2018-04-14 20:55:53.803157
115	19	49	armour	2018-04-14 20:55:53.807343	2018-04-14 20:55:53.807343
116	19	37	armour	2018-04-14 20:55:53.811316	2018-04-14 20:55:53.811316
117	20	115	weapon	2018-04-14 20:55:53.815527	2018-04-14 20:55:53.815527
118	20	114	weapon	2018-04-14 20:55:53.819759	2018-04-14 20:55:53.819759
119	20	53	weapon	2018-04-14 20:55:53.82382	2018-04-14 20:55:53.82382
120	20	52	weapon	2018-04-14 20:55:53.827899	2018-04-14 20:55:53.827899
121	20	48	weapon	2018-04-14 20:55:53.832058	2018-04-14 20:55:53.832058
122	20	47	weapon	2018-04-14 20:55:53.836217	2018-04-14 20:55:53.836217
123	20	36	weapon	2018-04-14 20:55:53.840391	2018-04-14 20:55:53.840391
124	20	35	weapon	2018-04-14 20:55:53.844603	2018-04-14 20:55:53.844603
125	20	116	armour	2018-04-14 20:55:53.84888	2018-04-14 20:55:53.84888
126	20	54	armour	2018-04-14 20:55:53.853027	2018-04-14 20:55:53.853027
127	20	49	armour	2018-04-14 20:55:53.857047	2018-04-14 20:55:53.857047
128	20	37	armour	2018-04-14 20:55:53.86117	2018-04-14 20:55:53.86117
129	21	115	weapon	2018-04-14 20:55:53.865406	2018-04-14 20:55:53.865406
130	21	114	weapon	2018-04-14 20:55:53.86968	2018-04-14 20:55:53.86968
131	21	116	armour	2018-04-14 20:55:53.873857	2018-04-14 20:55:53.873857
132	21	75	armour	2018-04-14 20:55:53.878065	2018-04-14 20:55:53.878065
133	22	35	weapon	2018-04-14 20:55:53.882217	2018-04-14 20:55:53.882217
134	22	75	armour	2018-04-14 20:55:53.88641	2018-04-14 20:55:53.88641
135	22	37	armour	2018-04-14 20:55:53.890574	2018-04-14 20:55:53.890574
136	23	30	weapon	2018-04-14 20:55:53.894497	2018-04-14 20:55:53.894497
137	23	28	weapon	2018-04-14 20:55:53.898484	2018-04-14 20:55:53.898484
138	23	27	weapon	2018-04-14 20:55:53.902648	2018-04-14 20:55:53.902648
139	23	110	armour	2018-04-14 20:55:53.90681	2018-04-14 20:55:53.90681
140	23	108	armour	2018-04-14 20:55:53.910896	2018-04-14 20:55:53.910896
141	23	107	armour	2018-04-14 20:55:53.914956	2018-04-14 20:55:53.914956
142	25	69	weapon	2018-04-14 20:55:53.919214	2018-04-14 20:55:53.919214
143	25	15	weapon	2018-04-14 20:55:53.92855	2018-04-14 20:55:53.92855
144	25	116	armour	2018-04-14 20:55:53.93302	2018-04-14 20:55:53.93302
145	25	109	armour	2018-04-14 20:55:53.937209	2018-04-14 20:55:53.937209
146	25	75	armour	2018-04-14 20:55:53.941215	2018-04-14 20:55:53.941215
147	25	57	armour	2018-04-14 20:55:53.945154	2018-04-14 20:55:53.945154
148	25	54	armour	2018-04-14 20:55:53.949363	2018-04-14 20:55:53.949363
149	25	49	armour	2018-04-14 20:55:53.953503	2018-04-14 20:55:53.953503
150	25	37	armour	2018-04-14 20:55:53.95759	2018-04-14 20:55:53.95759
151	26	29	weapon	2018-04-14 20:55:53.96168	2018-04-14 20:55:53.96168
152	26	10	weapon	2018-04-14 20:55:53.965876	2018-04-14 20:55:53.965876
153	26	116	armour	2018-04-14 20:55:53.970116	2018-04-14 20:55:53.970116
154	26	109	armour	2018-04-14 20:55:53.974171	2018-04-14 20:55:53.974171
155	26	75	armour	2018-04-14 20:55:53.978188	2018-04-14 20:55:53.978188
156	26	57	armour	2018-04-14 20:55:53.98248	2018-04-14 20:55:53.98248
157	26	54	armour	2018-04-14 20:55:53.986425	2018-04-14 20:55:53.986425
158	26	49	armour	2018-04-14 20:55:53.990563	2018-04-14 20:55:53.990563
159	26	37	armour	2018-04-14 20:55:54.001165	2018-04-14 20:55:54.001165
160	27	70	weapon	2018-04-14 20:55:54.011295	2018-04-14 20:55:54.011295
161	27	45	weapon	2018-04-14 20:55:54.02056	2018-04-14 20:55:54.02056
162	27	41	weapon	2018-04-14 20:55:54.027542	2018-04-14 20:55:54.027542
163	27	75	armour	2018-04-14 20:55:54.033284	2018-04-14 20:55:54.033284
164	27	50	armour	2018-04-14 20:55:54.03764	2018-04-14 20:55:54.03764
165	28	30	weapon	2018-04-14 20:55:54.041726	2018-04-14 20:55:54.041726
166	28	28	weapon	2018-04-14 20:55:54.045489	2018-04-14 20:55:54.045489
167	28	27	weapon	2018-04-14 20:55:54.049557	2018-04-14 20:55:54.049557
168	28	110	armour	2018-04-14 20:55:54.05348	2018-04-14 20:55:54.05348
169	28	108	armour	2018-04-14 20:55:54.06012	2018-04-14 20:55:54.06012
170	28	107	armour	2018-04-14 20:55:54.064961	2018-04-14 20:55:54.064961
171	29	118	weapon	2018-04-14 20:55:54.071766	2018-04-14 20:55:54.071766
172	29	104	weapon	2018-04-14 20:55:54.076501	2018-04-14 20:55:54.076501
173	29	90	weapon	2018-04-14 20:55:54.080396	2018-04-14 20:55:54.080396
174	29	86	weapon	2018-04-14 20:55:54.08418	2018-04-14 20:55:54.08418
175	29	82	weapon	2018-04-14 20:55:54.088247	2018-04-14 20:55:54.088247
176	29	77	weapon	2018-04-14 20:55:54.092	2018-04-14 20:55:54.092
177	29	20	weapon	2018-04-14 20:55:54.09573	2018-04-14 20:55:54.09573
178	29	119	armour	2018-04-14 20:55:54.100587	2018-04-14 20:55:54.100587
179	29	105	armour	2018-04-14 20:55:54.104424	2018-04-14 20:55:54.104424
180	29	91	armour	2018-04-14 20:55:54.108336	2018-04-14 20:55:54.108336
181	29	87	armour	2018-04-14 20:55:54.111974	2018-04-14 20:55:54.111974
182	29	83	armour	2018-04-14 20:55:54.115856	2018-04-14 20:55:54.115856
183	29	78	armour	2018-04-14 20:55:54.120005	2018-04-14 20:55:54.120005
184	29	21	armour	2018-04-14 20:55:54.123799	2018-04-14 20:55:54.123799
185	30	118	weapon	2018-04-14 20:55:54.127754	2018-04-14 20:55:54.127754
186	30	104	weapon	2018-04-14 20:55:54.131427	2018-04-14 20:55:54.131427
187	30	90	weapon	2018-04-14 20:55:54.135538	2018-04-14 20:55:54.135538
188	30	86	weapon	2018-04-14 20:55:54.139326	2018-04-14 20:55:54.139326
189	30	82	weapon	2018-04-14 20:55:54.143118	2018-04-14 20:55:54.143118
190	30	77	weapon	2018-04-14 20:55:54.146964	2018-04-14 20:55:54.146964
191	30	24	weapon	2018-04-14 20:55:54.151291	2018-04-14 20:55:54.151291
192	30	20	weapon	2018-04-14 20:55:54.158249	2018-04-14 20:55:54.158249
193	30	119	armour	2018-04-14 20:55:54.163921	2018-04-14 20:55:54.163921
194	30	105	armour	2018-04-14 20:55:54.16831	2018-04-14 20:55:54.16831
195	30	91	armour	2018-04-14 20:55:54.172278	2018-04-14 20:55:54.172278
196	30	87	armour	2018-04-14 20:55:54.176168	2018-04-14 20:55:54.176168
197	30	83	armour	2018-04-14 20:55:54.180021	2018-04-14 20:55:54.180021
198	30	78	armour	2018-04-14 20:55:54.184071	2018-04-14 20:55:54.184071
199	30	25	armour	2018-04-14 20:55:54.187974	2018-04-14 20:55:54.187974
200	30	21	armour	2018-04-14 20:55:54.191782	2018-04-14 20:55:54.191782
201	31	89	weapon	2018-04-14 20:55:54.195589	2018-04-14 20:55:54.195589
202	31	30	weapon	2018-04-14 20:55:54.199434	2018-04-14 20:55:54.199434
203	31	116	armour	2018-04-14 20:55:54.203227	2018-04-14 20:55:54.203227
204	31	110	armour	2018-04-14 20:55:54.207158	2018-04-14 20:55:54.207158
205	31	109	armour	2018-04-14 20:55:54.21094	2018-04-14 20:55:54.21094
206	31	75	armour	2018-04-14 20:55:54.214726	2018-04-14 20:55:54.214726
207	31	58	armour	2018-04-14 20:55:54.219028	2018-04-14 20:55:54.219028
208	31	57	armour	2018-04-14 20:55:54.222943	2018-04-14 20:55:54.222943
209	31	54	armour	2018-04-14 20:55:54.226756	2018-04-14 20:55:54.226756
210	31	49	armour	2018-04-14 20:55:54.230591	2018-04-14 20:55:54.230591
211	31	37	armour	2018-04-14 20:55:54.23455	2018-04-14 20:55:54.23455
212	32	61	weapon	2018-04-14 20:55:54.243776	2018-04-14 20:55:54.243776
213	32	75	armour	2018-04-14 20:55:54.250839	2018-04-14 20:55:54.250839
214	32	58	armour	2018-04-14 20:55:54.256983	2018-04-14 20:55:54.256983
215	32	55	armour	2018-04-14 20:55:54.262699	2018-04-14 20:55:54.262699
216	33	61	weapon	2018-04-14 20:55:54.267845	2018-04-14 20:55:54.267845
217	33	75	armour	2018-04-14 20:55:54.272022	2018-04-14 20:55:54.272022
218	33	58	armour	2018-04-14 20:55:54.275981	2018-04-14 20:55:54.275981
219	33	55	armour	2018-04-14 20:55:54.280099	2018-04-14 20:55:54.280099
220	34	113	weapon	2018-04-14 20:55:54.284765	2018-04-14 20:55:54.284765
221	34	51	weapon	2018-04-14 20:55:54.288752	2018-04-14 20:55:54.288752
222	34	46	weapon	2018-04-14 20:55:54.292619	2018-04-14 20:55:54.292619
223	34	34	weapon	2018-04-14 20:55:54.296324	2018-04-14 20:55:54.296324
224	34	116	armour	2018-04-14 20:55:54.300058	2018-04-14 20:55:54.300058
225	34	54	armour	2018-04-14 20:55:54.304132	2018-04-14 20:55:54.304132
226	34	49	armour	2018-04-14 20:55:54.307909	2018-04-14 20:55:54.307909
227	34	37	armour	2018-04-14 20:55:54.311804	2018-04-14 20:55:54.311804
228	35	114	weapon	2018-04-14 20:55:54.315571	2018-04-14 20:55:54.315571
229	35	52	weapon	2018-04-14 20:55:54.31948	2018-04-14 20:55:54.31948
230	35	47	weapon	2018-04-14 20:55:54.323364	2018-04-14 20:55:54.323364
231	35	35	weapon	2018-04-14 20:55:54.32716	2018-04-14 20:55:54.32716
232	35	110	armour	2018-04-14 20:55:54.33089	2018-04-14 20:55:54.33089
233	35	107	armour	2018-04-14 20:55:54.335069	2018-04-14 20:55:54.335069
234	35	80	armour	2018-04-14 20:55:54.339009	2018-04-14 20:55:54.339009
235	35	75	armour	2018-04-14 20:55:54.342858	2018-04-14 20:55:54.342858
236	35	58	armour	2018-04-14 20:55:54.346856	2018-04-14 20:55:54.346856
237	35	55	armour	2018-04-14 20:55:54.350941	2018-04-14 20:55:54.350941
238	36	61	weapon	2018-04-14 20:55:54.355273	2018-04-14 20:55:54.355273
239	36	27	weapon	2018-04-14 20:55:54.358988	2018-04-14 20:55:54.358988
240	36	23	weapon	2018-04-14 20:55:54.363672	2018-04-14 20:55:54.363672
241	36	110	armour	2018-04-14 20:55:54.369207	2018-04-14 20:55:54.369207
242	36	75	armour	2018-04-14 20:55:54.373583	2018-04-14 20:55:54.373583
243	36	58	armour	2018-04-14 20:55:54.377529	2018-04-14 20:55:54.377529
244	36	26	armour	2018-04-14 20:55:54.38125	2018-04-14 20:55:54.38125
245	37	118	weapon	2018-04-14 20:55:54.385282	2018-04-14 20:55:54.385282
246	37	104	weapon	2018-04-14 20:55:54.389445	2018-04-14 20:55:54.389445
247	37	90	weapon	2018-04-14 20:55:54.393073	2018-04-14 20:55:54.393073
248	37	86	weapon	2018-04-14 20:55:54.396859	2018-04-14 20:55:54.396859
249	37	82	weapon	2018-04-14 20:55:54.401325	2018-04-14 20:55:54.401325
250	37	77	weapon	2018-04-14 20:55:54.406693	2018-04-14 20:55:54.406693
251	37	20	weapon	2018-04-14 20:55:54.410713	2018-04-14 20:55:54.410713
252	37	31	armour	2018-04-14 20:55:54.414615	2018-04-14 20:55:54.414615
253	37	25	armour	2018-04-14 20:55:54.418617	2018-04-14 20:55:54.418617
254	38	63	weapon	2018-04-14 20:55:54.422739	2018-04-14 20:55:54.422739
255	38	19	weapon	2018-04-14 20:55:54.426684	2018-04-14 20:55:54.426684
256	38	109	armour	2018-04-14 20:55:54.430566	2018-04-14 20:55:54.430566
257	38	80	armour	2018-04-14 20:55:54.434199	2018-04-14 20:55:54.434199
258	38	75	armour	2018-04-14 20:55:54.438128	2018-04-14 20:55:54.438128
259	38	57	armour	2018-04-14 20:55:54.442288	2018-04-14 20:55:54.442288
260	39	114	weapon	2018-04-14 20:55:54.446211	2018-04-14 20:55:54.446211
261	39	52	weapon	2018-04-14 20:55:54.450099	2018-04-14 20:55:54.450099
262	39	47	weapon	2018-04-14 20:55:54.454188	2018-04-14 20:55:54.454188
263	39	35	weapon	2018-04-14 20:55:54.458179	2018-04-14 20:55:54.458179
264	39	116	armour	2018-04-14 20:55:54.46203	2018-04-14 20:55:54.46203
265	39	75	armour	2018-04-14 20:55:54.465802	2018-04-14 20:55:54.465802
266	39	54	armour	2018-04-14 20:55:54.470035	2018-04-14 20:55:54.470035
267	39	49	armour	2018-04-14 20:55:54.473998	2018-04-14 20:55:54.473998
268	39	37	armour	2018-04-14 20:55:54.477995	2018-04-14 20:55:54.477995
269	39	1	armour	2018-04-14 20:55:54.482221	2018-04-14 20:55:54.482221
270	40	114	weapon	2018-04-14 20:55:54.486446	2018-04-14 20:55:54.486446
271	40	53	weapon	2018-04-14 20:55:54.490278	2018-04-14 20:55:54.490278
272	40	52	weapon	2018-04-14 20:55:54.494071	2018-04-14 20:55:54.494071
273	40	47	weapon	2018-04-14 20:55:54.497945	2018-04-14 20:55:54.497945
274	40	35	weapon	2018-04-14 20:55:54.501866	2018-04-14 20:55:54.501866
275	40	116	armour	2018-04-14 20:55:54.509155	2018-04-14 20:55:54.509155
276	40	75	armour	2018-04-14 20:55:54.516246	2018-04-14 20:55:54.516246
277	40	54	armour	2018-04-14 20:55:54.522924	2018-04-14 20:55:54.522924
278	40	49	armour	2018-04-14 20:55:54.528043	2018-04-14 20:55:54.528043
279	40	37	armour	2018-04-14 20:55:54.532449	2018-04-14 20:55:54.532449
280	41	67	weapon	2018-04-14 20:55:54.536821	2018-04-14 20:55:54.536821
281	41	66	weapon	2018-04-14 20:55:54.540821	2018-04-14 20:55:54.540821
282	41	75	armour	2018-04-14 20:55:54.544583	2018-04-14 20:55:54.544583
283	41	58	armour	2018-04-14 20:55:54.548393	2018-04-14 20:55:54.548393
284	41	55	armour	2018-04-14 20:55:54.55253	2018-04-14 20:55:54.55253
285	42	99	weapon	2018-04-14 20:55:54.556684	2018-04-14 20:55:54.556684
286	42	27	weapon	2018-04-14 20:55:54.560385	2018-04-14 20:55:54.560385
287	42	110	armour	2018-04-14 20:55:54.564197	2018-04-14 20:55:54.564197
288	42	109	armour	2018-04-14 20:55:54.568017	2018-04-14 20:55:54.568017
289	42	75	armour	2018-04-14 20:55:54.572033	2018-04-14 20:55:54.572033
290	43	85	weapon	2018-04-14 20:55:54.575852	2018-04-14 20:55:54.575852
291	43	82	weapon	2018-04-14 20:55:54.579799	2018-04-14 20:55:54.579799
292	43	63	weapon	2018-04-14 20:55:54.583791	2018-04-14 20:55:54.583791
293	43	116	armour	2018-04-14 20:55:54.587665	2018-04-14 20:55:54.587665
294	43	109	armour	2018-04-14 20:55:54.5918	2018-04-14 20:55:54.5918
295	43	75	armour	2018-04-14 20:55:54.595562	2018-04-14 20:55:54.595562
296	43	57	armour	2018-04-14 20:55:54.599514	2018-04-14 20:55:54.599514
297	43	54	armour	2018-04-14 20:55:54.603433	2018-04-14 20:55:54.603433
298	43	49	armour	2018-04-14 20:55:54.608358	2018-04-14 20:55:54.608358
299	43	37	armour	2018-04-14 20:55:54.612356	2018-04-14 20:55:54.612356
300	44	99	weapon	2018-04-14 20:55:54.616348	2018-04-14 20:55:54.616348
301	44	27	weapon	2018-04-14 20:55:54.620522	2018-04-14 20:55:54.620522
302	44	110	armour	2018-04-14 20:55:54.624451	2018-04-14 20:55:54.624451
303	44	109	armour	2018-04-14 20:55:54.62843	2018-04-14 20:55:54.62843
304	44	107	armour	2018-04-14 20:55:54.633107	2018-04-14 20:55:54.633107
305	44	75	armour	2018-04-14 20:55:54.638586	2018-04-14 20:55:54.638586
306	45	104	weapon	2018-04-14 20:55:54.642653	2018-04-14 20:55:54.642653
307	45	103	weapon	2018-04-14 20:55:54.649298	2018-04-14 20:55:54.649298
308	45	71	weapon	2018-04-14 20:55:54.654434	2018-04-14 20:55:54.654434
309	45	106	armour	2018-04-14 20:55:54.658538	2018-04-14 20:55:54.658538
310	45	105	armour	2018-04-14 20:55:54.663691	2018-04-14 20:55:54.663691
311	45	75	armour	2018-04-14 20:55:54.66779	2018-04-14 20:55:54.66779
312	46	85	weapon	2018-04-14 20:55:54.672332	2018-04-14 20:55:54.672332
313	46	81	weapon	2018-04-14 20:55:54.677523	2018-04-14 20:55:54.677523
314	46	63	weapon	2018-04-14 20:55:54.68253	2018-04-14 20:55:54.68253
315	46	116	armour	2018-04-14 20:55:54.686349	2018-04-14 20:55:54.686349
316	46	109	armour	2018-04-14 20:55:54.690487	2018-04-14 20:55:54.690487
317	46	75	armour	2018-04-14 20:55:54.694413	2018-04-14 20:55:54.694413
318	46	57	armour	2018-04-14 20:55:54.698236	2018-04-14 20:55:54.698236
319	46	54	armour	2018-04-14 20:55:54.702243	2018-04-14 20:55:54.702243
320	46	49	armour	2018-04-14 20:55:54.706197	2018-04-14 20:55:54.706197
321	46	37	armour	2018-04-14 20:55:54.710055	2018-04-14 20:55:54.710055
322	47	89	weapon	2018-04-14 20:55:54.713874	2018-04-14 20:55:54.713874
323	47	29	weapon	2018-04-14 20:55:54.717681	2018-04-14 20:55:54.717681
324	47	116	armour	2018-04-14 20:55:54.721573	2018-04-14 20:55:54.721573
325	47	109	armour	2018-04-14 20:55:54.728383	2018-04-14 20:55:54.728383
326	47	75	armour	2018-04-14 20:55:54.733287	2018-04-14 20:55:54.733287
327	47	57	armour	2018-04-14 20:55:54.74381	2018-04-14 20:55:54.74381
328	47	54	armour	2018-04-14 20:55:54.754508	2018-04-14 20:55:54.754508
329	47	49	armour	2018-04-14 20:55:54.764031	2018-04-14 20:55:54.764031
330	47	37	armour	2018-04-14 20:55:54.768279	2018-04-14 20:55:54.768279
331	48	118	weapon	2018-04-14 20:55:54.772334	2018-04-14 20:55:54.772334
332	48	104	weapon	2018-04-14 20:55:54.778175	2018-04-14 20:55:54.778175
333	48	90	weapon	2018-04-14 20:55:54.784309	2018-04-14 20:55:54.784309
334	48	86	weapon	2018-04-14 20:55:54.793004	2018-04-14 20:55:54.793004
335	48	82	weapon	2018-04-14 20:55:54.797273	2018-04-14 20:55:54.797273
336	48	77	weapon	2018-04-14 20:55:54.801233	2018-04-14 20:55:54.801233
337	48	24	weapon	2018-04-14 20:55:54.80649	2018-04-14 20:55:54.80649
338	48	20	weapon	2018-04-14 20:55:54.811955	2018-04-14 20:55:54.811955
339	48	119	armour	2018-04-14 20:55:54.815943	2018-04-14 20:55:54.815943
340	48	105	armour	2018-04-14 20:55:54.82045	2018-04-14 20:55:54.82045
341	48	91	armour	2018-04-14 20:55:54.824724	2018-04-14 20:55:54.824724
342	48	87	armour	2018-04-14 20:55:54.828781	2018-04-14 20:55:54.828781
343	48	83	armour	2018-04-14 20:55:54.833721	2018-04-14 20:55:54.833721
344	48	78	armour	2018-04-14 20:55:54.838977	2018-04-14 20:55:54.838977
345	48	25	armour	2018-04-14 20:55:54.843321	2018-04-14 20:55:54.843321
346	48	21	armour	2018-04-14 20:55:54.847433	2018-04-14 20:55:54.847433
347	49	44	weapon	2018-04-14 20:55:54.851465	2018-04-14 20:55:54.851465
348	49	84	armour	2018-04-14 20:55:54.856275	2018-04-14 20:55:54.856275
349	49	75	armour	2018-04-14 20:55:54.860413	2018-04-14 20:55:54.860413
350	49	37	armour	2018-04-14 20:55:54.864421	2018-04-14 20:55:54.864421
351	50	5	weapon	2018-04-14 20:55:54.868427	2018-04-14 20:55:54.868427
352	50	112	armour	2018-04-14 20:55:54.873321	2018-04-14 20:55:54.873321
353	51	30	weapon	2018-04-14 20:55:54.877568	2018-04-14 20:55:54.877568
354	51	110	armour	2018-04-14 20:55:54.881672	2018-04-14 20:55:54.881672
355	51	98	armour	2018-04-14 20:55:54.885909	2018-04-14 20:55:54.885909
356	51	75	armour	2018-04-14 20:55:54.890438	2018-04-14 20:55:54.890438
357	51	58	armour	2018-04-14 20:55:54.89464	2018-04-14 20:55:54.89464
358	52	30	weapon	2018-04-14 20:55:54.900683	2018-04-14 20:55:54.900683
359	52	28	weapon	2018-04-14 20:55:54.905037	2018-04-14 20:55:54.905037
360	52	27	weapon	2018-04-14 20:55:54.909859	2018-04-14 20:55:54.909859
361	52	110	armour	2018-04-14 20:55:54.914719	2018-04-14 20:55:54.914719
362	52	108	armour	2018-04-14 20:55:54.921856	2018-04-14 20:55:54.921856
363	52	107	armour	2018-04-14 20:55:54.931718	2018-04-14 20:55:54.931718
364	53	66	weapon	2018-04-14 20:55:54.938265	2018-04-14 20:55:54.938265
365	53	15	weapon	2018-04-14 20:55:54.947998	2018-04-14 20:55:54.947998
366	53	116	armour	2018-04-14 20:55:54.958187	2018-04-14 20:55:54.958187
367	53	109	armour	2018-04-14 20:55:54.964413	2018-04-14 20:55:54.964413
368	53	75	armour	2018-04-14 20:55:54.970527	2018-04-14 20:55:54.970527
369	53	57	armour	2018-04-14 20:55:54.977247	2018-04-14 20:55:54.977247
370	53	54	armour	2018-04-14 20:55:54.983365	2018-04-14 20:55:54.983365
371	53	49	armour	2018-04-14 20:55:54.989173	2018-04-14 20:55:54.989173
372	53	37	armour	2018-04-14 20:55:54.995043	2018-04-14 20:55:54.995043
373	54	103	weapon	2018-04-14 20:55:55.000828	2018-04-14 20:55:55.000828
374	54	106	armour	2018-04-14 20:55:55.007207	2018-04-14 20:55:55.007207
375	54	75	armour	2018-04-14 20:55:55.013073	2018-04-14 20:55:55.013073
376	55	103	weapon	2018-04-14 20:55:55.019109	2018-04-14 20:55:55.019109
377	55	106	armour	2018-04-14 20:55:55.024221	2018-04-14 20:55:55.024221
378	55	75	armour	2018-04-14 20:55:55.033826	2018-04-14 20:55:55.033826
379	56	66	weapon	2018-04-14 20:55:55.0427	2018-04-14 20:55:55.0427
380	56	30	weapon	2018-04-14 20:55:55.050469	2018-04-14 20:55:55.050469
381	56	110	armour	2018-04-14 20:55:55.060608	2018-04-14 20:55:55.060608
382	56	75	armour	2018-04-14 20:55:55.068102	2018-04-14 20:55:55.068102
383	57	97	weapon	2018-04-14 20:55:55.074789	2018-04-14 20:55:55.074789
384	57	96	weapon	2018-04-14 20:55:55.080801	2018-04-14 20:55:55.080801
385	57	95	weapon	2018-04-14 20:55:55.084703	2018-04-14 20:55:55.084703
386	57	94	weapon	2018-04-14 20:55:55.088502	2018-04-14 20:55:55.088502
387	57	116	armour	2018-04-14 20:55:55.093971	2018-04-14 20:55:55.093971
388	57	75	armour	2018-04-14 20:55:55.098628	2018-04-14 20:55:55.098628
389	57	54	armour	2018-04-14 20:55:55.102619	2018-04-14 20:55:55.102619
390	57	49	armour	2018-04-14 20:55:55.106736	2018-04-14 20:55:55.106736
391	57	37	armour	2018-04-14 20:55:55.110697	2018-04-14 20:55:55.110697
392	58	113	weapon	2018-04-14 20:55:55.114485	2018-04-14 20:55:55.114485
393	58	51	weapon	2018-04-14 20:55:55.118413	2018-04-14 20:55:55.118413
394	58	46	weapon	2018-04-14 20:55:55.122332	2018-04-14 20:55:55.122332
395	58	34	weapon	2018-04-14 20:55:55.126378	2018-04-14 20:55:55.126378
396	58	116	armour	2018-04-14 20:55:55.130929	2018-04-14 20:55:55.130929
397	58	54	armour	2018-04-14 20:55:55.137307	2018-04-14 20:55:55.137307
398	58	49	armour	2018-04-14 20:55:55.142312	2018-04-14 20:55:55.142312
399	58	37	armour	2018-04-14 20:55:55.149172	2018-04-14 20:55:55.149172
400	59	117	weapon	2018-04-14 20:55:55.153308	2018-04-14 20:55:55.153308
401	59	64	weapon	2018-04-14 20:55:55.15745	2018-04-14 20:55:55.15745
402	59	110	armour	2018-04-14 20:55:55.16116	2018-04-14 20:55:55.16116
403	59	75	armour	2018-04-14 20:55:55.165028	2018-04-14 20:55:55.165028
404	59	60	armour	2018-04-14 20:55:55.169106	2018-04-14 20:55:55.169106
405	60	117	weapon	2018-04-14 20:55:55.174794	2018-04-14 20:55:55.174794
406	60	64	weapon	2018-04-14 20:55:55.179026	2018-04-14 20:55:55.179026
407	60	110	armour	2018-04-14 20:55:55.182621	2018-04-14 20:55:55.182621
408	60	75	armour	2018-04-14 20:55:55.18733	2018-04-14 20:55:55.18733
409	60	60	armour	2018-04-14 20:55:55.191754	2018-04-14 20:55:55.191754
410	61	118	weapon	2018-04-14 20:55:55.195624	2018-04-14 20:55:55.195624
411	61	104	weapon	2018-04-14 20:55:55.199372	2018-04-14 20:55:55.199372
412	61	90	weapon	2018-04-14 20:55:55.203533	2018-04-14 20:55:55.203533
413	61	86	weapon	2018-04-14 20:55:55.210011	2018-04-14 20:55:55.210011
414	61	82	weapon	2018-04-14 20:55:55.217206	2018-04-14 20:55:55.217206
415	61	77	weapon	2018-04-14 20:55:55.222452	2018-04-14 20:55:55.222452
416	61	24	weapon	2018-04-14 20:55:55.226534	2018-04-14 20:55:55.226534
417	61	20	weapon	2018-04-14 20:55:55.230314	2018-04-14 20:55:55.230314
418	61	16	weapon	2018-04-14 20:55:55.233886	2018-04-14 20:55:55.233886
419	61	119	armour	2018-04-14 20:55:55.237554	2018-04-14 20:55:55.237554
420	61	105	armour	2018-04-14 20:55:55.241656	2018-04-14 20:55:55.241656
421	61	91	armour	2018-04-14 20:55:55.245522	2018-04-14 20:55:55.245522
422	61	87	armour	2018-04-14 20:55:55.251885	2018-04-14 20:55:55.251885
423	61	83	armour	2018-04-14 20:55:55.259289	2018-04-14 20:55:55.259289
424	61	78	armour	2018-04-14 20:55:55.264986	2018-04-14 20:55:55.264986
425	61	25	armour	2018-04-14 20:55:55.268979	2018-04-14 20:55:55.268979
426	61	21	armour	2018-04-14 20:55:55.272789	2018-04-14 20:55:55.272789
427	62	36	weapon	2018-04-14 20:55:55.276688	2018-04-14 20:55:55.276688
428	62	35	weapon	2018-04-14 20:55:55.28041	2018-04-14 20:55:55.28041
429	62	75	armour	2018-04-14 20:55:55.284451	2018-04-14 20:55:55.284451
430	62	37	armour	2018-04-14 20:55:55.288268	2018-04-14 20:55:55.288268
431	63	66	weapon	2018-04-14 20:55:55.292203	2018-04-14 20:55:55.292203
432	63	16	weapon	2018-04-14 20:55:55.296045	2018-04-14 20:55:55.296045
433	63	15	weapon	2018-04-14 20:55:55.299852	2018-04-14 20:55:55.299852
434	63	116	armour	2018-04-14 20:55:55.303557	2018-04-14 20:55:55.303557
435	63	109	armour	2018-04-14 20:55:55.307511	2018-04-14 20:55:55.307511
436	63	75	armour	2018-04-14 20:55:55.311498	2018-04-14 20:55:55.311498
437	63	57	armour	2018-04-14 20:55:55.315351	2018-04-14 20:55:55.315351
438	63	54	armour	2018-04-14 20:55:55.319138	2018-04-14 20:55:55.319138
439	63	49	armour	2018-04-14 20:55:55.322981	2018-04-14 20:55:55.322981
440	63	37	armour	2018-04-14 20:55:55.326821	2018-04-14 20:55:55.326821
441	64	69	weapon	2018-04-14 20:55:55.330595	2018-04-14 20:55:55.330595
442	64	15	weapon	2018-04-14 20:55:55.334265	2018-04-14 20:55:55.334265
443	64	116	armour	2018-04-14 20:55:55.33838	2018-04-14 20:55:55.33838
444	64	109	armour	2018-04-14 20:55:55.342161	2018-04-14 20:55:55.342161
445	64	75	armour	2018-04-14 20:55:55.346024	2018-04-14 20:55:55.346024
446	64	57	armour	2018-04-14 20:55:55.349935	2018-04-14 20:55:55.349935
447	64	54	armour	2018-04-14 20:55:55.353747	2018-04-14 20:55:55.353747
448	64	49	armour	2018-04-14 20:55:55.359354	2018-04-14 20:55:55.359354
449	64	37	armour	2018-04-14 20:55:55.364158	2018-04-14 20:55:55.364158
450	65	85	weapon	2018-04-14 20:55:55.3682	2018-04-14 20:55:55.3682
451	65	69	weapon	2018-04-14 20:55:55.372246	2018-04-14 20:55:55.372246
452	65	109	armour	2018-04-14 20:55:55.37621	2018-04-14 20:55:55.37621
453	65	88	armour	2018-04-14 20:55:55.380266	2018-04-14 20:55:55.380266
454	65	75	armour	2018-04-14 20:55:55.384063	2018-04-14 20:55:55.384063
455	65	57	armour	2018-04-14 20:55:55.387959	2018-04-14 20:55:55.387959
456	66	63	weapon	2018-04-14 20:55:55.392047	2018-04-14 20:55:55.392047
457	66	52	weapon	2018-04-14 20:55:55.3961	2018-04-14 20:55:55.3961
458	66	109	armour	2018-04-14 20:55:55.400033	2018-04-14 20:55:55.400033
459	66	75	armour	2018-04-14 20:55:55.403838	2018-04-14 20:55:55.403838
460	66	57	armour	2018-04-14 20:55:55.407748	2018-04-14 20:55:55.407748
461	66	54	armour	2018-04-14 20:55:55.414137	2018-04-14 20:55:55.414137
462	67	85	weapon	2018-04-14 20:55:55.418799	2018-04-14 20:55:55.418799
463	67	81	weapon	2018-04-14 20:55:55.422592	2018-04-14 20:55:55.422592
464	67	63	weapon	2018-04-14 20:55:55.426389	2018-04-14 20:55:55.426389
465	67	116	armour	2018-04-14 20:55:55.430231	2018-04-14 20:55:55.430231
466	67	109	armour	2018-04-14 20:55:55.436517	2018-04-14 20:55:55.436517
467	67	75	armour	2018-04-14 20:55:55.442879	2018-04-14 20:55:55.442879
468	67	57	armour	2018-04-14 20:55:55.447548	2018-04-14 20:55:55.447548
469	67	54	armour	2018-04-14 20:55:55.451375	2018-04-14 20:55:55.451375
470	67	49	armour	2018-04-14 20:55:55.454904	2018-04-14 20:55:55.454904
471	67	37	armour	2018-04-14 20:55:55.458728	2018-04-14 20:55:55.458728
472	68	19	weapon	2018-04-14 20:55:55.462605	2018-04-14 20:55:55.462605
473	68	109	armour	2018-04-14 20:55:55.466283	2018-04-14 20:55:55.466283
474	68	75	armour	2018-04-14 20:55:55.470258	2018-04-14 20:55:55.470258
475	68	57	armour	2018-04-14 20:55:55.474087	2018-04-14 20:55:55.474087
476	68	22	armour	2018-04-14 20:55:55.478078	2018-04-14 20:55:55.478078
477	69	19	weapon	2018-04-14 20:55:55.481936	2018-04-14 20:55:55.481936
478	69	116	armour	2018-04-14 20:55:55.48607	2018-04-14 20:55:55.48607
479	69	75	armour	2018-04-14 20:55:55.490098	2018-04-14 20:55:55.490098
480	69	54	armour	2018-04-14 20:55:55.494337	2018-04-14 20:55:55.494337
481	69	49	armour	2018-04-14 20:55:55.498489	2018-04-14 20:55:55.498489
482	69	37	armour	2018-04-14 20:55:55.502408	2018-04-14 20:55:55.502408
483	70	85	weapon	2018-04-14 20:55:55.506407	2018-04-14 20:55:55.506407
484	70	82	weapon	2018-04-14 20:55:55.510496	2018-04-14 20:55:55.510496
485	70	63	weapon	2018-04-14 20:55:55.517122	2018-04-14 20:55:55.517122
486	70	116	armour	2018-04-14 20:55:55.521995	2018-04-14 20:55:55.521995
487	70	109	armour	2018-04-14 20:55:55.52802	2018-04-14 20:55:55.52802
488	70	75	armour	2018-04-14 20:55:55.532674	2018-04-14 20:55:55.532674
489	70	57	armour	2018-04-14 20:55:55.537265	2018-04-14 20:55:55.537265
490	70	54	armour	2018-04-14 20:55:55.548449	2018-04-14 20:55:55.548449
491	70	49	armour	2018-04-14 20:55:55.554624	2018-04-14 20:55:55.554624
492	70	37	armour	2018-04-14 20:55:55.559917	2018-04-14 20:55:55.559917
493	71	85	weapon	2018-04-14 20:55:55.566609	2018-04-14 20:55:55.566609
494	71	82	weapon	2018-04-14 20:55:55.572906	2018-04-14 20:55:55.572906
495	71	63	weapon	2018-04-14 20:55:55.578776	2018-04-14 20:55:55.578776
496	71	116	armour	2018-04-14 20:55:55.583198	2018-04-14 20:55:55.583198
497	71	109	armour	2018-04-14 20:55:55.587072	2018-04-14 20:55:55.587072
498	71	75	armour	2018-04-14 20:55:55.591075	2018-04-14 20:55:55.591075
499	71	57	armour	2018-04-14 20:55:55.595008	2018-04-14 20:55:55.595008
500	71	54	armour	2018-04-14 20:55:55.598627	2018-04-14 20:55:55.598627
501	71	49	armour	2018-04-14 20:55:55.602557	2018-04-14 20:55:55.602557
502	71	37	armour	2018-04-14 20:55:55.60639	2018-04-14 20:55:55.60639
503	74	71	weapon	2018-04-14 20:55:55.61035	2018-04-14 20:55:55.61035
504	74	66	weapon	2018-04-14 20:55:55.614117	2018-04-14 20:55:55.614117
505	74	23	weapon	2018-04-14 20:55:55.618039	2018-04-14 20:55:55.618039
506	74	75	armour	2018-04-14 20:55:55.62188	2018-04-14 20:55:55.62188
507	74	54	armour	2018-04-14 20:55:55.625879	2018-04-14 20:55:55.625879
508	75	53	weapon	2018-04-14 20:55:55.629842	2018-04-14 20:55:55.629842
509	75	52	weapon	2018-04-14 20:55:55.633818	2018-04-14 20:55:55.633818
510	75	75	armour	2018-04-14 20:55:55.63774	2018-04-14 20:55:55.63774
511	75	54	armour	2018-04-14 20:55:55.641655	2018-04-14 20:55:55.641655
512	76	76	weapon	2018-04-14 20:55:55.645768	2018-04-14 20:55:55.645768
513	76	44	weapon	2018-04-14 20:55:55.649732	2018-04-14 20:55:55.649732
514	76	41	weapon	2018-04-14 20:55:55.653795	2018-04-14 20:55:55.653795
515	76	110	armour	2018-04-14 20:55:55.657421	2018-04-14 20:55:55.657421
516	76	79	armour	2018-04-14 20:55:55.661556	2018-04-14 20:55:55.661556
517	76	75	armour	2018-04-14 20:55:55.665353	2018-04-14 20:55:55.665353
518	76	58	armour	2018-04-14 20:55:55.668971	2018-04-14 20:55:55.668971
519	77	103	weapon	2018-04-14 20:55:55.672769	2018-04-14 20:55:55.672769
520	77	85	weapon	2018-04-14 20:55:55.676643	2018-04-14 20:55:55.676643
521	77	81	weapon	2018-04-14 20:55:55.68057	2018-04-14 20:55:55.68057
522	77	76	weapon	2018-04-14 20:55:55.684374	2018-04-14 20:55:55.684374
523	77	19	weapon	2018-04-14 20:55:55.688215	2018-04-14 20:55:55.688215
524	77	15	weapon	2018-04-14 20:55:55.692183	2018-04-14 20:55:55.692183
525	77	10	weapon	2018-04-14 20:55:55.696362	2018-04-14 20:55:55.696362
526	77	88	armour	2018-04-14 20:55:55.700075	2018-04-14 20:55:55.700075
527	77	84	armour	2018-04-14 20:55:55.703889	2018-04-14 20:55:55.703889
528	77	79	armour	2018-04-14 20:55:55.7077	2018-04-14 20:55:55.7077
529	77	75	armour	2018-04-14 20:55:55.711867	2018-04-14 20:55:55.711867
530	77	22	armour	2018-04-14 20:55:55.715932	2018-04-14 20:55:55.715932
531	78	4	weapon	2018-04-14 20:55:55.719816	2018-04-14 20:55:55.719816
532	78	72	armour	2018-04-14 20:55:55.723548	2018-04-14 20:55:55.723548
533	79	36	weapon	2018-04-14 20:55:55.727399	2018-04-14 20:55:55.727399
534	79	35	weapon	2018-04-14 20:55:55.73134	2018-04-14 20:55:55.73134
535	79	75	armour	2018-04-14 20:55:55.734986	2018-04-14 20:55:55.734986
536	79	37	armour	2018-04-14 20:55:55.738763	2018-04-14 20:55:55.738763
537	80	41	weapon	2018-04-14 20:55:55.742607	2018-04-14 20:55:55.742607
538	80	84	armour	2018-04-14 20:55:55.746326	2018-04-14 20:55:55.746326
539	80	75	armour	2018-04-14 20:55:55.750259	2018-04-14 20:55:55.750259
540	80	37	armour	2018-04-14 20:55:55.754039	2018-04-14 20:55:55.754039
541	82	69	weapon	2018-04-14 20:55:55.757872	2018-04-14 20:55:55.757872
542	82	63	weapon	2018-04-14 20:55:55.761971	2018-04-14 20:55:55.761971
543	82	44	weapon	2018-04-14 20:55:55.767328	2018-04-14 20:55:55.767328
544	82	110	armour	2018-04-14 20:55:55.771192	2018-04-14 20:55:55.771192
545	82	109	armour	2018-04-14 20:55:55.776998	2018-04-14 20:55:55.776998
546	82	80	armour	2018-04-14 20:55:55.782857	2018-04-14 20:55:55.782857
547	82	75	armour	2018-04-14 20:55:55.788037	2018-04-14 20:55:55.788037
548	82	58	armour	2018-04-14 20:55:55.791599	2018-04-14 20:55:55.791599
549	82	57	armour	2018-04-14 20:55:55.795388	2018-04-14 20:55:55.795388
550	83	69	weapon	2018-04-14 20:55:55.799357	2018-04-14 20:55:55.799357
551	83	63	weapon	2018-04-14 20:55:55.803169	2018-04-14 20:55:55.803169
552	83	44	weapon	2018-04-14 20:55:55.80695	2018-04-14 20:55:55.80695
553	83	110	armour	2018-04-14 20:55:55.810707	2018-04-14 20:55:55.810707
554	83	109	armour	2018-04-14 20:55:55.814704	2018-04-14 20:55:55.814704
555	83	80	armour	2018-04-14 20:55:55.818705	2018-04-14 20:55:55.818705
556	83	75	armour	2018-04-14 20:55:55.822696	2018-04-14 20:55:55.822696
557	83	58	armour	2018-04-14 20:55:55.826569	2018-04-14 20:55:55.826569
558	83	57	armour	2018-04-14 20:55:55.831417	2018-04-14 20:55:55.831417
559	84	69	weapon	2018-04-14 20:55:55.835179	2018-04-14 20:55:55.835179
560	84	63	weapon	2018-04-14 20:55:55.839369	2018-04-14 20:55:55.839369
561	84	44	weapon	2018-04-14 20:55:55.84339	2018-04-14 20:55:55.84339
562	84	110	armour	2018-04-14 20:55:55.847618	2018-04-14 20:55:55.847618
563	84	109	armour	2018-04-14 20:55:55.851677	2018-04-14 20:55:55.851677
564	84	80	armour	2018-04-14 20:55:55.855639	2018-04-14 20:55:55.855639
565	84	75	armour	2018-04-14 20:55:55.859309	2018-04-14 20:55:55.859309
566	84	58	armour	2018-04-14 20:55:55.863253	2018-04-14 20:55:55.863253
567	84	57	armour	2018-04-14 20:55:55.867231	2018-04-14 20:55:55.867231
568	85	11	weapon	2018-04-14 20:55:55.871229	2018-04-14 20:55:55.871229
569	85	10	weapon	2018-04-14 20:55:55.875222	2018-04-14 20:55:55.875222
570	85	116	armour	2018-04-14 20:55:55.879141	2018-04-14 20:55:55.879141
571	85	109	armour	2018-04-14 20:55:55.883344	2018-04-14 20:55:55.883344
572	85	75	armour	2018-04-14 20:55:55.887587	2018-04-14 20:55:55.887587
573	85	57	armour	2018-04-14 20:55:55.891664	2018-04-14 20:55:55.891664
574	85	54	armour	2018-04-14 20:55:55.89812	2018-04-14 20:55:55.89812
575	85	49	armour	2018-04-14 20:55:55.902987	2018-04-14 20:55:55.902987
576	85	37	armour	2018-04-14 20:55:55.907016	2018-04-14 20:55:55.907016
577	86	114	weapon	2018-04-14 20:55:55.911183	2018-04-14 20:55:55.911183
578	86	52	weapon	2018-04-14 20:55:55.915	2018-04-14 20:55:55.915
579	86	47	weapon	2018-04-14 20:55:55.918876	2018-04-14 20:55:55.918876
580	86	35	weapon	2018-04-14 20:55:55.922937	2018-04-14 20:55:55.922937
581	86	116	armour	2018-04-14 20:55:55.927716	2018-04-14 20:55:55.927716
582	86	75	armour	2018-04-14 20:55:55.933079	2018-04-14 20:55:55.933079
583	86	54	armour	2018-04-14 20:55:55.937023	2018-04-14 20:55:55.937023
584	86	49	armour	2018-04-14 20:55:55.940968	2018-04-14 20:55:55.940968
585	86	37	armour	2018-04-14 20:55:55.948723	2018-04-14 20:55:55.948723
586	87	30	weapon	2018-04-14 20:55:55.952836	2018-04-14 20:55:55.952836
587	87	28	weapon	2018-04-14 20:55:55.956811	2018-04-14 20:55:55.956811
588	87	27	weapon	2018-04-14 20:55:55.960721	2018-04-14 20:55:55.960721
589	87	110	armour	2018-04-14 20:55:55.964459	2018-04-14 20:55:55.964459
590	87	108	armour	2018-04-14 20:55:55.968432	2018-04-14 20:55:55.968432
591	87	107	armour	2018-04-14 20:55:55.975351	2018-04-14 20:55:55.975351
592	88	48	weapon	2018-04-14 20:55:55.982376	2018-04-14 20:55:55.982376
593	88	47	weapon	2018-04-14 20:55:55.989619	2018-04-14 20:55:55.989619
594	88	75	armour	2018-04-14 20:55:55.995807	2018-04-14 20:55:55.995807
595	88	49	armour	2018-04-14 20:55:56.000532	2018-04-14 20:55:56.000532
596	89	89	weapon	2018-04-14 20:55:56.004499	2018-04-14 20:55:56.004499
597	89	30	weapon	2018-04-14 20:55:56.008368	2018-04-14 20:55:56.008368
598	89	116	armour	2018-04-14 20:55:56.013152	2018-04-14 20:55:56.013152
599	89	109	armour	2018-04-14 20:55:56.017731	2018-04-14 20:55:56.017731
600	89	75	armour	2018-04-14 20:55:56.021594	2018-04-14 20:55:56.021594
601	89	57	armour	2018-04-14 20:55:56.025204	2018-04-14 20:55:56.025204
602	89	54	armour	2018-04-14 20:55:56.029182	2018-04-14 20:55:56.029182
603	89	49	armour	2018-04-14 20:55:56.033314	2018-04-14 20:55:56.033314
604	89	37	armour	2018-04-14 20:55:56.036938	2018-04-14 20:55:56.036938
605	90	53	weapon	2018-04-14 20:55:56.040933	2018-04-14 20:55:56.040933
606	90	52	weapon	2018-04-14 20:55:56.044984	2018-04-14 20:55:56.044984
607	90	110	armour	2018-04-14 20:55:56.049794	2018-04-14 20:55:56.049794
608	90	75	armour	2018-04-14 20:55:56.053657	2018-04-14 20:55:56.053657
609	90	58	armour	2018-04-14 20:55:56.057671	2018-04-14 20:55:56.057671
610	90	54	armour	2018-04-14 20:55:56.06161	2018-04-14 20:55:56.06161
611	91	89	weapon	2018-04-14 20:55:56.065682	2018-04-14 20:55:56.065682
612	91	30	weapon	2018-04-14 20:55:56.069762	2018-04-14 20:55:56.069762
613	91	116	armour	2018-04-14 20:55:56.073656	2018-04-14 20:55:56.073656
614	91	109	armour	2018-04-14 20:55:56.077365	2018-04-14 20:55:56.077365
615	91	75	armour	2018-04-14 20:55:56.083119	2018-04-14 20:55:56.083119
616	91	57	armour	2018-04-14 20:55:56.087074	2018-04-14 20:55:56.087074
617	91	54	armour	2018-04-14 20:55:56.091125	2018-04-14 20:55:56.091125
618	91	49	armour	2018-04-14 20:55:56.094939	2018-04-14 20:55:56.094939
619	91	37	armour	2018-04-14 20:55:56.098989	2018-04-14 20:55:56.098989
620	92	101	weapon	2018-04-14 20:55:56.102991	2018-04-14 20:55:56.102991
621	92	44	weapon	2018-04-14 20:55:56.106821	2018-04-14 20:55:56.106821
622	92	41	weapon	2018-04-14 20:55:56.110742	2018-04-14 20:55:56.110742
623	92	110	armour	2018-04-14 20:55:56.11761	2018-04-14 20:55:56.11761
624	92	107	armour	2018-04-14 20:55:56.122335	2018-04-14 20:55:56.122335
625	92	75	armour	2018-04-14 20:55:56.126033	2018-04-14 20:55:56.126033
626	92	58	armour	2018-04-14 20:55:56.130012	2018-04-14 20:55:56.130012
627	92	55	armour	2018-04-14 20:55:56.133783	2018-04-14 20:55:56.133783
628	93	43	weapon	2018-04-14 20:55:56.137514	2018-04-14 20:55:56.137514
629	93	42	weapon	2018-04-14 20:55:56.141432	2018-04-14 20:55:56.141432
630	93	41	weapon	2018-04-14 20:55:56.145554	2018-04-14 20:55:56.145554
631	93	110	armour	2018-04-14 20:55:56.151	2018-04-14 20:55:56.151
632	93	108	armour	2018-04-14 20:55:56.157732	2018-04-14 20:55:56.157732
633	93	107	armour	2018-04-14 20:55:56.162997	2018-04-14 20:55:56.162997
634	94	118	weapon	2018-04-14 20:55:56.167543	2018-04-14 20:55:56.167543
635	94	104	weapon	2018-04-14 20:55:56.171873	2018-04-14 20:55:56.171873
636	94	90	weapon	2018-04-14 20:55:56.175616	2018-04-14 20:55:56.175616
637	94	86	weapon	2018-04-14 20:55:56.179484	2018-04-14 20:55:56.179484
638	94	82	weapon	2018-04-14 20:55:56.183475	2018-04-14 20:55:56.183475
639	94	77	weapon	2018-04-14 20:55:56.18732	2018-04-14 20:55:56.18732
640	94	20	weapon	2018-04-14 20:55:56.191238	2018-04-14 20:55:56.191238
641	94	119	armour	2018-04-14 20:55:56.194979	2018-04-14 20:55:56.194979
642	94	105	armour	2018-04-14 20:55:56.199975	2018-04-14 20:55:56.199975
643	94	91	armour	2018-04-14 20:55:56.203631	2018-04-14 20:55:56.203631
644	94	87	armour	2018-04-14 20:55:56.20743	2018-04-14 20:55:56.20743
645	94	83	armour	2018-04-14 20:55:56.211141	2018-04-14 20:55:56.211141
646	94	78	armour	2018-04-14 20:55:56.215112	2018-04-14 20:55:56.215112
647	94	21	armour	2018-04-14 20:55:56.219315	2018-04-14 20:55:56.219315
648	95	30	weapon	2018-04-14 20:55:56.223028	2018-04-14 20:55:56.223028
649	95	28	weapon	2018-04-14 20:55:56.229141	2018-04-14 20:55:56.229141
650	95	27	weapon	2018-04-14 20:55:56.233058	2018-04-14 20:55:56.233058
651	95	110	armour	2018-04-14 20:55:56.23682	2018-04-14 20:55:56.23682
652	95	108	armour	2018-04-14 20:55:56.240645	2018-04-14 20:55:56.240645
653	95	107	armour	2018-04-14 20:55:56.24436	2018-04-14 20:55:56.24436
654	96	113	weapon	2018-04-14 20:55:56.248635	2018-04-14 20:55:56.248635
655	96	51	weapon	2018-04-14 20:55:56.252546	2018-04-14 20:55:56.252546
656	96	46	weapon	2018-04-14 20:55:56.256304	2018-04-14 20:55:56.256304
657	96	34	weapon	2018-04-14 20:55:56.260011	2018-04-14 20:55:56.260011
658	96	59	armour	2018-04-14 20:55:56.263988	2018-04-14 20:55:56.263988
659	97	76	weapon	2018-04-14 20:55:56.268063	2018-04-14 20:55:56.268063
660	97	69	weapon	2018-04-14 20:55:56.272038	2018-04-14 20:55:56.272038
661	97	116	armour	2018-04-14 20:55:56.275834	2018-04-14 20:55:56.275834
662	97	109	armour	2018-04-14 20:55:56.279841	2018-04-14 20:55:56.279841
663	97	78	armour	2018-04-14 20:55:56.283773	2018-04-14 20:55:56.283773
664	97	75	armour	2018-04-14 20:55:56.287512	2018-04-14 20:55:56.287512
665	97	57	armour	2018-04-14 20:55:56.294304	2018-04-14 20:55:56.294304
666	97	54	armour	2018-04-14 20:55:56.299116	2018-04-14 20:55:56.299116
667	97	49	armour	2018-04-14 20:55:56.302948	2018-04-14 20:55:56.302948
668	97	37	armour	2018-04-14 20:55:56.306744	2018-04-14 20:55:56.306744
669	98	30	weapon	2018-04-14 20:55:56.310609	2018-04-14 20:55:56.310609
670	98	80	armour	2018-04-14 20:55:56.314731	2018-04-14 20:55:56.314731
671	98	75	armour	2018-04-14 20:55:56.319464	2018-04-14 20:55:56.319464
672	100	43	weapon	2018-04-14 20:55:56.327241	2018-04-14 20:55:56.327241
673	100	42	weapon	2018-04-14 20:55:56.331502	2018-04-14 20:55:56.331502
674	100	41	weapon	2018-04-14 20:55:56.335786	2018-04-14 20:55:56.335786
675	100	60	armour	2018-04-14 20:55:56.339792	2018-04-14 20:55:56.339792
676	100	33	armour	2018-04-14 20:55:56.343533	2018-04-14 20:55:56.343533
677	100	17	armour	2018-04-14 20:55:56.34782	2018-04-14 20:55:56.34782
678	101	89	weapon	2018-04-14 20:55:56.351906	2018-04-14 20:55:56.351906
679	101	44	weapon	2018-04-14 20:55:56.355894	2018-04-14 20:55:56.355894
680	101	116	armour	2018-04-14 20:55:56.360858	2018-04-14 20:55:56.360858
681	101	109	armour	2018-04-14 20:55:56.36472	2018-04-14 20:55:56.36472
682	101	75	armour	2018-04-14 20:55:56.368517	2018-04-14 20:55:56.368517
683	101	57	armour	2018-04-14 20:55:56.372639	2018-04-14 20:55:56.372639
684	101	54	armour	2018-04-14 20:55:56.376595	2018-04-14 20:55:56.376595
685	101	49	armour	2018-04-14 20:55:56.380735	2018-04-14 20:55:56.380735
686	101	37	armour	2018-04-14 20:55:56.384398	2018-04-14 20:55:56.384398
687	102	89	weapon	2018-04-14 20:55:56.388157	2018-04-14 20:55:56.388157
688	102	44	weapon	2018-04-14 20:55:56.391953	2018-04-14 20:55:56.391953
689	102	116	armour	2018-04-14 20:55:56.395753	2018-04-14 20:55:56.395753
690	102	109	armour	2018-04-14 20:55:56.400634	2018-04-14 20:55:56.400634
691	102	75	armour	2018-04-14 20:55:56.40445	2018-04-14 20:55:56.40445
692	102	57	armour	2018-04-14 20:55:56.408337	2018-04-14 20:55:56.408337
693	102	54	armour	2018-04-14 20:55:56.412165	2018-04-14 20:55:56.412165
694	102	49	armour	2018-04-14 20:55:56.416103	2018-04-14 20:55:56.416103
695	102	37	armour	2018-04-14 20:55:56.420086	2018-04-14 20:55:56.420086
696	103	41	weapon	2018-04-14 20:55:56.423858	2018-04-14 20:55:56.423858
697	103	30	weapon	2018-04-14 20:55:56.427669	2018-04-14 20:55:56.427669
698	103	5	weapon	2018-04-14 20:55:56.431617	2018-04-14 20:55:56.431617
699	103	110	armour	2018-04-14 20:55:56.435518	2018-04-14 20:55:56.435518
700	103	92	armour	2018-04-14 20:55:56.439434	2018-04-14 20:55:56.439434
701	103	91	armour	2018-04-14 20:55:56.443294	2018-04-14 20:55:56.443294
702	103	75	armour	2018-04-14 20:55:56.447194	2018-04-14 20:55:56.447194
703	104	52	weapon	2018-04-14 20:55:56.451018	2018-04-14 20:55:56.451018
704	104	75	armour	2018-04-14 20:55:56.454812	2018-04-14 20:55:56.454812
705	104	54	armour	2018-04-14 20:55:56.458526	2018-04-14 20:55:56.458526
706	105	77	weapon	2018-04-14 20:55:56.462262	2018-04-14 20:55:56.462262
707	105	78	armour	2018-04-14 20:55:56.466076	2018-04-14 20:55:56.466076
708	105	75	armour	2018-04-14 20:55:56.470151	2018-04-14 20:55:56.470151
709	106	103	weapon	2018-04-14 20:55:56.474056	2018-04-14 20:55:56.474056
710	106	85	weapon	2018-04-14 20:55:56.477769	2018-04-14 20:55:56.477769
711	106	81	weapon	2018-04-14 20:55:56.481932	2018-04-14 20:55:56.481932
712	106	19	weapon	2018-04-14 20:55:56.485993	2018-04-14 20:55:56.485993
713	106	15	weapon	2018-04-14 20:55:56.489724	2018-04-14 20:55:56.489724
714	106	10	weapon	2018-04-14 20:55:56.493593	2018-04-14 20:55:56.493593
715	106	110	armour	2018-04-14 20:55:56.49746	2018-04-14 20:55:56.49746
716	106	79	armour	2018-04-14 20:55:56.501676	2018-04-14 20:55:56.501676
717	106	75	armour	2018-04-14 20:55:56.505579	2018-04-14 20:55:56.505579
718	106	58	armour	2018-04-14 20:55:56.510349	2018-04-14 20:55:56.510349
719	107	28	weapon	2018-04-14 20:55:56.514473	2018-04-14 20:55:56.514473
720	107	27	weapon	2018-04-14 20:55:56.518629	2018-04-14 20:55:56.518629
721	107	110	armour	2018-04-14 20:55:56.522469	2018-04-14 20:55:56.522469
722	107	98	armour	2018-04-14 20:55:56.526323	2018-04-14 20:55:56.526323
723	107	75	armour	2018-04-14 20:55:56.530057	2018-04-14 20:55:56.530057
724	107	58	armour	2018-04-14 20:55:56.534022	2018-04-14 20:55:56.534022
725	108	117	weapon	2018-04-14 20:55:56.537958	2018-04-14 20:55:56.537958
726	108	30	weapon	2018-04-14 20:55:56.541782	2018-04-14 20:55:56.541782
727	108	27	weapon	2018-04-14 20:55:56.545576	2018-04-14 20:55:56.545576
728	108	110	armour	2018-04-14 20:55:56.550586	2018-04-14 20:55:56.550586
729	108	109	armour	2018-04-14 20:55:56.554694	2018-04-14 20:55:56.554694
730	108	75	armour	2018-04-14 20:55:56.558695	2018-04-14 20:55:56.558695
731	109	30	weapon	2018-04-14 20:55:56.562568	2018-04-14 20:55:56.562568
732	109	10	weapon	2018-04-14 20:55:56.566808	2018-04-14 20:55:56.566808
733	109	116	armour	2018-04-14 20:55:56.571026	2018-04-14 20:55:56.571026
734	109	109	armour	2018-04-14 20:55:56.574965	2018-04-14 20:55:56.574965
735	109	75	armour	2018-04-14 20:55:56.578758	2018-04-14 20:55:56.578758
736	109	57	armour	2018-04-14 20:55:56.582652	2018-04-14 20:55:56.582652
737	109	54	armour	2018-04-14 20:55:56.58665	2018-04-14 20:55:56.58665
738	109	49	armour	2018-04-14 20:55:56.590538	2018-04-14 20:55:56.590538
739	109	37	armour	2018-04-14 20:55:56.594401	2018-04-14 20:55:56.594401
740	111	103	weapon	2018-04-14 20:55:56.598241	2018-04-14 20:55:56.598241
741	111	85	weapon	2018-04-14 20:55:56.602471	2018-04-14 20:55:56.602471
742	111	81	weapon	2018-04-14 20:55:56.606557	2018-04-14 20:55:56.606557
743	111	76	weapon	2018-04-14 20:55:56.610653	2018-04-14 20:55:56.610653
744	111	19	weapon	2018-04-14 20:55:56.614694	2018-04-14 20:55:56.614694
745	111	15	weapon	2018-04-14 20:55:56.618625	2018-04-14 20:55:56.618625
746	111	10	weapon	2018-04-14 20:55:56.622683	2018-04-14 20:55:56.622683
747	111	88	armour	2018-04-14 20:55:56.626534	2018-04-14 20:55:56.626534
748	111	84	armour	2018-04-14 20:55:56.630345	2018-04-14 20:55:56.630345
749	111	79	armour	2018-04-14 20:55:56.634254	2018-04-14 20:55:56.634254
750	111	75	armour	2018-04-14 20:55:56.638375	2018-04-14 20:55:56.638375
751	111	22	armour	2018-04-14 20:55:56.642683	2018-04-14 20:55:56.642683
752	112	118	weapon	2018-04-14 20:55:56.653747	2018-04-14 20:55:56.653747
753	112	104	weapon	2018-04-14 20:55:56.659022	2018-04-14 20:55:56.659022
754	112	90	weapon	2018-04-14 20:55:56.662955	2018-04-14 20:55:56.662955
755	112	86	weapon	2018-04-14 20:55:56.66717	2018-04-14 20:55:56.66717
756	112	82	weapon	2018-04-14 20:55:56.670939	2018-04-14 20:55:56.670939
757	112	77	weapon	2018-04-14 20:55:56.674811	2018-04-14 20:55:56.674811
758	112	20	weapon	2018-04-14 20:55:56.678689	2018-04-14 20:55:56.678689
759	112	119	armour	2018-04-14 20:55:56.682677	2018-04-14 20:55:56.682677
760	112	105	armour	2018-04-14 20:55:56.686877	2018-04-14 20:55:56.686877
761	112	91	armour	2018-04-14 20:55:56.690674	2018-04-14 20:55:56.690674
762	112	87	armour	2018-04-14 20:55:56.694591	2018-04-14 20:55:56.694591
763	112	83	armour	2018-04-14 20:55:56.698491	2018-04-14 20:55:56.698491
764	112	78	armour	2018-04-14 20:55:56.702471	2018-04-14 20:55:56.702471
765	112	21	armour	2018-04-14 20:55:56.706272	2018-04-14 20:55:56.706272
766	113	103	weapon	2018-04-14 20:55:56.710053	2018-04-14 20:55:56.710053
767	113	85	weapon	2018-04-14 20:55:56.713904	2018-04-14 20:55:56.713904
768	113	81	weapon	2018-04-14 20:55:56.717803	2018-04-14 20:55:56.717803
769	113	76	weapon	2018-04-14 20:55:56.721688	2018-04-14 20:55:56.721688
770	113	19	weapon	2018-04-14 20:55:56.72544	2018-04-14 20:55:56.72544
771	113	15	weapon	2018-04-14 20:55:56.729369	2018-04-14 20:55:56.729369
772	113	10	weapon	2018-04-14 20:55:56.733474	2018-04-14 20:55:56.733474
773	113	110	armour	2018-04-14 20:55:56.737442	2018-04-14 20:55:56.737442
774	113	79	armour	2018-04-14 20:55:56.741367	2018-04-14 20:55:56.741367
775	113	75	armour	2018-04-14 20:55:56.745244	2018-04-14 20:55:56.745244
776	113	58	armour	2018-04-14 20:55:56.749225	2018-04-14 20:55:56.749225
777	114	61	weapon	2018-04-14 20:55:56.753063	2018-04-14 20:55:56.753063
778	114	27	weapon	2018-04-14 20:55:56.757218	2018-04-14 20:55:56.757218
779	114	24	weapon	2018-04-14 20:55:56.761073	2018-04-14 20:55:56.761073
780	114	23	weapon	2018-04-14 20:55:56.764994	2018-04-14 20:55:56.764994
781	114	18	weapon	2018-04-14 20:55:56.769078	2018-04-14 20:55:56.769078
782	114	110	armour	2018-04-14 20:55:56.773054	2018-04-14 20:55:56.773054
783	114	75	armour	2018-04-14 20:55:56.776994	2018-04-14 20:55:56.776994
784	114	58	armour	2018-04-14 20:55:56.78067	2018-04-14 20:55:56.78067
785	114	55	armour	2018-04-14 20:55:56.78497	2018-04-14 20:55:56.78497
786	114	26	armour	2018-04-14 20:55:56.788861	2018-04-14 20:55:56.788861
787	115	24	weapon	2018-04-14 20:55:56.792678	2018-04-14 20:55:56.792678
788	115	23	weapon	2018-04-14 20:55:56.797538	2018-04-14 20:55:56.797538
789	115	91	armour	2018-04-14 20:55:56.802295	2018-04-14 20:55:56.802295
790	115	75	armour	2018-04-14 20:55:56.806249	2018-04-14 20:55:56.806249
791	115	25	armour	2018-04-14 20:55:56.809974	2018-04-14 20:55:56.809974
792	116	85	weapon	2018-04-14 20:55:56.813717	2018-04-14 20:55:56.813717
793	116	64	weapon	2018-04-14 20:55:56.817691	2018-04-14 20:55:56.817691
794	116	88	armour	2018-04-14 20:55:56.821642	2018-04-14 20:55:56.821642
795	116	75	armour	2018-04-14 20:55:56.825441	2018-04-14 20:55:56.825441
796	117	102	weapon	2018-04-14 20:55:56.829293	2018-04-14 20:55:56.829293
797	117	100	weapon	2018-04-14 20:55:56.83526	2018-04-14 20:55:56.83526
798	117	99	weapon	2018-04-14 20:55:56.842543	2018-04-14 20:55:56.842543
799	117	97	weapon	2018-04-14 20:55:56.849029	2018-04-14 20:55:56.849029
800	117	96	weapon	2018-04-14 20:55:56.854905	2018-04-14 20:55:56.854905
801	117	95	weapon	2018-04-14 20:55:56.859199	2018-04-14 20:55:56.859199
802	117	94	weapon	2018-04-14 20:55:56.862955	2018-04-14 20:55:56.862955
803	117	116	armour	2018-04-14 20:55:56.866971	2018-04-14 20:55:56.866971
804	117	75	armour	2018-04-14 20:55:56.870859	2018-04-14 20:55:56.870859
805	117	54	armour	2018-04-14 20:55:56.874921	2018-04-14 20:55:56.874921
806	117	49	armour	2018-04-14 20:55:56.878974	2018-04-14 20:55:56.878974
807	117	37	armour	2018-04-14 20:55:56.883036	2018-04-14 20:55:56.883036
808	118	97	weapon	2018-04-14 20:55:56.887315	2018-04-14 20:55:56.887315
809	118	96	weapon	2018-04-14 20:55:56.891136	2018-04-14 20:55:56.891136
810	118	95	weapon	2018-04-14 20:55:56.894808	2018-04-14 20:55:56.894808
811	118	94	weapon	2018-04-14 20:55:56.898661	2018-04-14 20:55:56.898661
812	118	116	armour	2018-04-14 20:55:56.902384	2018-04-14 20:55:56.902384
813	118	75	armour	2018-04-14 20:55:56.906422	2018-04-14 20:55:56.906422
814	118	54	armour	2018-04-14 20:55:56.910535	2018-04-14 20:55:56.910535
815	118	49	armour	2018-04-14 20:55:56.914478	2018-04-14 20:55:56.914478
816	118	37	armour	2018-04-14 20:55:56.918417	2018-04-14 20:55:56.918417
817	119	102	weapon	2018-04-14 20:55:56.922443	2018-04-14 20:55:56.922443
818	119	100	weapon	2018-04-14 20:55:56.926197	2018-04-14 20:55:56.926197
819	119	99	weapon	2018-04-14 20:55:56.930128	2018-04-14 20:55:56.930128
820	119	97	weapon	2018-04-14 20:55:56.934239	2018-04-14 20:55:56.934239
821	119	96	weapon	2018-04-14 20:55:56.938238	2018-04-14 20:55:56.938238
822	119	95	weapon	2018-04-14 20:55:56.942085	2018-04-14 20:55:56.942085
823	119	94	weapon	2018-04-14 20:55:56.945908	2018-04-14 20:55:56.945908
824	119	116	armour	2018-04-14 20:55:56.950333	2018-04-14 20:55:56.950333
825	119	75	armour	2018-04-14 20:55:56.954613	2018-04-14 20:55:56.954613
826	119	54	armour	2018-04-14 20:55:56.962142	2018-04-14 20:55:56.962142
827	119	49	armour	2018-04-14 20:55:56.966315	2018-04-14 20:55:56.966315
828	119	37	armour	2018-04-14 20:55:56.970473	2018-04-14 20:55:56.970473
829	120	102	weapon	2018-04-14 20:55:56.974317	2018-04-14 20:55:56.974317
830	120	100	weapon	2018-04-14 20:55:56.978058	2018-04-14 20:55:56.978058
831	120	99	weapon	2018-04-14 20:55:56.982025	2018-04-14 20:55:56.982025
832	120	97	weapon	2018-04-14 20:55:56.98615	2018-04-14 20:55:56.98615
833	120	96	weapon	2018-04-14 20:55:56.989903	2018-04-14 20:55:56.989903
834	120	95	weapon	2018-04-14 20:55:56.993812	2018-04-14 20:55:56.993812
835	120	94	weapon	2018-04-14 20:55:56.997591	2018-04-14 20:55:56.997591
836	120	116	armour	2018-04-14 20:55:57.001673	2018-04-14 20:55:57.001673
837	120	75	armour	2018-04-14 20:55:57.005534	2018-04-14 20:55:57.005534
838	120	54	armour	2018-04-14 20:55:57.00936	2018-04-14 20:55:57.00936
839	120	49	armour	2018-04-14 20:55:57.013056	2018-04-14 20:55:57.013056
840	120	37	armour	2018-04-14 20:55:57.017144	2018-04-14 20:55:57.017144
841	121	97	weapon	2018-04-14 20:55:57.021139	2018-04-14 20:55:57.021139
842	121	96	weapon	2018-04-14 20:55:57.025026	2018-04-14 20:55:57.025026
843	121	95	weapon	2018-04-14 20:55:57.028909	2018-04-14 20:55:57.028909
844	121	94	weapon	2018-04-14 20:55:57.032898	2018-04-14 20:55:57.032898
845	121	116	armour	2018-04-14 20:55:57.036911	2018-04-14 20:55:57.036911
846	121	75	armour	2018-04-14 20:55:57.04098	2018-04-14 20:55:57.04098
847	121	54	armour	2018-04-14 20:55:57.044811	2018-04-14 20:55:57.044811
848	121	49	armour	2018-04-14 20:55:57.048679	2018-04-14 20:55:57.048679
849	121	37	armour	2018-04-14 20:55:57.052519	2018-04-14 20:55:57.052519
850	122	97	weapon	2018-04-14 20:55:57.056528	2018-04-14 20:55:57.056528
851	122	96	weapon	2018-04-14 20:55:57.060373	2018-04-14 20:55:57.060373
852	122	95	weapon	2018-04-14 20:55:57.064591	2018-04-14 20:55:57.064591
853	122	94	weapon	2018-04-14 20:55:57.068736	2018-04-14 20:55:57.068736
854	122	116	armour	2018-04-14 20:55:57.072572	2018-04-14 20:55:57.072572
855	122	75	armour	2018-04-14 20:55:57.076681	2018-04-14 20:55:57.076681
856	122	54	armour	2018-04-14 20:55:57.080816	2018-04-14 20:55:57.080816
857	122	49	armour	2018-04-14 20:55:57.0846	2018-04-14 20:55:57.0846
858	122	37	armour	2018-04-14 20:55:57.088533	2018-04-14 20:55:57.088533
859	123	89	weapon	2018-04-14 20:55:57.0924	2018-04-14 20:55:57.0924
860	123	29	weapon	2018-04-14 20:55:57.096191	2018-04-14 20:55:57.096191
861	123	116	armour	2018-04-14 20:55:57.100214	2018-04-14 20:55:57.100214
862	123	109	armour	2018-04-14 20:55:57.104161	2018-04-14 20:55:57.104161
863	123	75	armour	2018-04-14 20:55:57.108083	2018-04-14 20:55:57.108083
864	123	57	armour	2018-04-14 20:55:57.111915	2018-04-14 20:55:57.111915
865	123	54	armour	2018-04-14 20:55:57.115702	2018-04-14 20:55:57.115702
866	123	49	armour	2018-04-14 20:55:57.119941	2018-04-14 20:55:57.119941
867	123	37	armour	2018-04-14 20:55:57.123649	2018-04-14 20:55:57.123649
868	124	89	weapon	2018-04-14 20:55:57.127438	2018-04-14 20:55:57.127438
869	124	85	weapon	2018-04-14 20:55:57.131081	2018-04-14 20:55:57.131081
870	124	63	weapon	2018-04-14 20:55:57.135213	2018-04-14 20:55:57.135213
871	124	116	armour	2018-04-14 20:55:57.13921	2018-04-14 20:55:57.13921
872	124	109	armour	2018-04-14 20:55:57.143182	2018-04-14 20:55:57.143182
873	124	75	armour	2018-04-14 20:55:57.147055	2018-04-14 20:55:57.147055
874	124	57	armour	2018-04-14 20:55:57.150971	2018-04-14 20:55:57.150971
875	124	54	armour	2018-04-14 20:55:57.15507	2018-04-14 20:55:57.15507
876	124	49	armour	2018-04-14 20:55:57.159077	2018-04-14 20:55:57.159077
877	124	37	armour	2018-04-14 20:55:57.162939	2018-04-14 20:55:57.162939
878	131	30	weapon	2018-04-14 20:55:57.166909	2018-04-14 20:55:57.166909
879	131	10	weapon	2018-04-14 20:55:57.170839	2018-04-14 20:55:57.170839
880	131	116	armour	2018-04-14 20:55:57.17476	2018-04-14 20:55:57.17476
881	131	109	armour	2018-04-14 20:55:57.178404	2018-04-14 20:55:57.178404
882	131	75	armour	2018-04-14 20:55:57.182123	2018-04-14 20:55:57.182123
883	131	57	armour	2018-04-14 20:55:57.186221	2018-04-14 20:55:57.186221
884	131	54	armour	2018-04-14 20:55:57.19026	2018-04-14 20:55:57.19026
885	131	49	armour	2018-04-14 20:55:57.194	2018-04-14 20:55:57.194
886	131	37	armour	2018-04-14 20:55:57.197859	2018-04-14 20:55:57.197859
887	132	89	weapon	2018-04-14 20:55:57.201761	2018-04-14 20:55:57.201761
888	132	41	weapon	2018-04-14 20:55:57.20559	2018-04-14 20:55:57.20559
889	132	116	armour	2018-04-14 20:55:57.209608	2018-04-14 20:55:57.209608
890	132	109	armour	2018-04-14 20:55:57.213352	2018-04-14 20:55:57.213352
891	132	75	armour	2018-04-14 20:55:57.217437	2018-04-14 20:55:57.217437
892	132	57	armour	2018-04-14 20:55:57.221612	2018-04-14 20:55:57.221612
893	132	54	armour	2018-04-14 20:55:57.228938	2018-04-14 20:55:57.228938
894	132	49	armour	2018-04-14 20:55:57.233108	2018-04-14 20:55:57.233108
895	132	37	armour	2018-04-14 20:55:57.237222	2018-04-14 20:55:57.237222
896	133	76	weapon	2018-04-14 20:55:57.241108	2018-04-14 20:55:57.241108
897	133	116	armour	2018-04-14 20:55:57.244852	2018-04-14 20:55:57.244852
898	133	109	armour	2018-04-14 20:55:57.248644	2018-04-14 20:55:57.248644
899	133	75	armour	2018-04-14 20:55:57.252688	2018-04-14 20:55:57.252688
900	133	57	armour	2018-04-14 20:55:57.256555	2018-04-14 20:55:57.256555
901	133	54	armour	2018-04-14 20:55:57.260416	2018-04-14 20:55:57.260416
902	133	49	armour	2018-04-14 20:55:57.264243	2018-04-14 20:55:57.264243
903	133	37	armour	2018-04-14 20:55:57.26817	2018-04-14 20:55:57.26817
904	134	113	weapon	2018-04-14 20:55:57.272933	2018-04-14 20:55:57.272933
905	134	51	weapon	2018-04-14 20:55:57.276838	2018-04-14 20:55:57.276838
906	134	46	weapon	2018-04-14 20:55:57.280596	2018-04-14 20:55:57.280596
907	134	34	weapon	2018-04-14 20:55:57.284674	2018-04-14 20:55:57.284674
908	134	116	armour	2018-04-14 20:55:57.289022	2018-04-14 20:55:57.289022
909	134	54	armour	2018-04-14 20:55:57.292843	2018-04-14 20:55:57.292843
910	134	49	armour	2018-04-14 20:55:57.296803	2018-04-14 20:55:57.296803
911	134	37	armour	2018-04-14 20:55:57.300491	2018-04-14 20:55:57.300491
912	135	114	weapon	2018-04-14 20:55:57.307097	2018-04-14 20:55:57.307097
913	135	52	weapon	2018-04-14 20:55:57.31231	2018-04-14 20:55:57.31231
914	135	47	weapon	2018-04-14 20:55:57.316199	2018-04-14 20:55:57.316199
915	135	35	weapon	2018-04-14 20:55:57.320038	2018-04-14 20:55:57.320038
916	135	75	armour	2018-04-14 20:55:57.323793	2018-04-14 20:55:57.323793
917	135	54	armour	2018-04-14 20:55:57.32758	2018-04-14 20:55:57.32758
918	136	13	weapon	2018-04-14 20:55:57.331399	2018-04-14 20:55:57.331399
919	136	12	armour	2018-04-14 20:55:57.33633	2018-04-14 20:55:57.33633
920	137	43	weapon	2018-04-14 20:55:57.341607	2018-04-14 20:55:57.341607
921	137	42	weapon	2018-04-14 20:55:57.345343	2018-04-14 20:55:57.345343
922	137	41	weapon	2018-04-14 20:55:57.349302	2018-04-14 20:55:57.349302
923	137	111	armour	2018-04-14 20:55:57.353476	2018-04-14 20:55:57.353476
924	137	75	armour	2018-04-14 20:55:57.357568	2018-04-14 20:55:57.357568
925	138	89	weapon	2018-04-14 20:55:57.361221	2018-04-14 20:55:57.361221
926	138	116	armour	2018-04-14 20:55:57.365164	2018-04-14 20:55:57.365164
927	138	109	armour	2018-04-14 20:55:57.369725	2018-04-14 20:55:57.369725
928	138	75	armour	2018-04-14 20:55:57.374143	2018-04-14 20:55:57.374143
929	138	57	armour	2018-04-14 20:55:57.377956	2018-04-14 20:55:57.377956
930	138	54	armour	2018-04-14 20:55:57.381767	2018-04-14 20:55:57.381767
931	138	49	armour	2018-04-14 20:55:57.385655	2018-04-14 20:55:57.385655
932	138	37	armour	2018-04-14 20:55:57.389574	2018-04-14 20:55:57.389574
933	139	29	weapon	2018-04-14 20:55:57.393123	2018-04-14 20:55:57.393123
934	139	75	armour	2018-04-14 20:55:57.396949	2018-04-14 20:55:57.396949
935	139	54	armour	2018-04-14 20:55:57.40076	2018-04-14 20:55:57.40076
936	140	103	weapon	2018-04-14 20:55:57.40451	2018-04-14 20:55:57.40451
937	140	85	weapon	2018-04-14 20:55:57.408334	2018-04-14 20:55:57.408334
938	140	81	weapon	2018-04-14 20:55:57.4123	2018-04-14 20:55:57.4123
939	140	76	weapon	2018-04-14 20:55:57.416126	2018-04-14 20:55:57.416126
940	140	19	weapon	2018-04-14 20:55:57.42026	2018-04-14 20:55:57.42026
941	140	15	weapon	2018-04-14 20:55:57.423964	2018-04-14 20:55:57.423964
942	140	10	weapon	2018-04-14 20:55:57.427967	2018-04-14 20:55:57.427967
943	140	110	armour	2018-04-14 20:55:57.431716	2018-04-14 20:55:57.431716
944	140	79	armour	2018-04-14 20:55:57.436468	2018-04-14 20:55:57.436468
945	140	75	armour	2018-04-14 20:55:57.440652	2018-04-14 20:55:57.440652
946	140	58	armour	2018-04-14 20:55:57.447385	2018-04-14 20:55:57.447385
947	141	114	weapon	2018-04-14 20:55:57.452094	2018-04-14 20:55:57.452094
948	141	44	weapon	2018-04-14 20:55:57.456154	2018-04-14 20:55:57.456154
949	141	116	armour	2018-04-14 20:55:57.46011	2018-04-14 20:55:57.46011
950	141	110	armour	2018-04-14 20:55:57.46389	2018-04-14 20:55:57.46389
951	141	75	armour	2018-04-14 20:55:57.469361	2018-04-14 20:55:57.469361
952	141	58	armour	2018-04-14 20:55:57.473578	2018-04-14 20:55:57.473578
953	142	93	weapon	2018-04-14 20:55:57.4775	2018-04-14 20:55:57.4775
954	142	41	weapon	2018-04-14 20:55:57.481366	2018-04-14 20:55:57.481366
955	142	110	armour	2018-04-14 20:55:57.485217	2018-04-14 20:55:57.485217
956	142	75	armour	2018-04-14 20:55:57.492505	2018-04-14 20:55:57.492505
957	142	58	armour	2018-04-14 20:55:57.498267	2018-04-14 20:55:57.498267
958	142	17	armour	2018-04-14 20:55:57.502435	2018-04-14 20:55:57.502435
959	143	13	weapon	2018-04-14 20:55:57.506745	2018-04-14 20:55:57.506745
960	143	112	armour	2018-04-14 20:55:57.512591	2018-04-14 20:55:57.512591
961	144	68	weapon	2018-04-14 20:55:57.517442	2018-04-14 20:55:57.517442
962	144	67	weapon	2018-04-14 20:55:57.52198	2018-04-14 20:55:57.52198
963	144	66	weapon	2018-04-14 20:55:57.525918	2018-04-14 20:55:57.525918
964	144	111	armour	2018-04-14 20:55:57.529718	2018-04-14 20:55:57.529718
965	145	30	weapon	2018-04-14 20:55:57.533561	2018-04-14 20:55:57.533561
966	145	28	weapon	2018-04-14 20:55:57.538257	2018-04-14 20:55:57.538257
967	145	27	weapon	2018-04-14 20:55:57.543368	2018-04-14 20:55:57.543368
968	145	32	armour	2018-04-14 20:55:57.547257	2018-04-14 20:55:57.547257
969	146	66	weapon	2018-04-14 20:55:57.550917	2018-04-14 20:55:57.550917
970	146	3	weapon	2018-04-14 20:55:57.554816	2018-04-14 20:55:57.554816
971	146	110	armour	2018-04-14 20:55:57.558495	2018-04-14 20:55:57.558495
972	146	75	armour	2018-04-14 20:55:57.56229	2018-04-14 20:55:57.56229
973	146	58	armour	2018-04-14 20:55:57.56872	2018-04-14 20:55:57.56872
974	149	30	weapon	2018-04-14 20:55:57.573463	2018-04-14 20:55:57.573463
975	149	28	weapon	2018-04-14 20:55:57.577427	2018-04-14 20:55:57.577427
976	149	27	weapon	2018-04-14 20:55:57.581117	2018-04-14 20:55:57.581117
977	149	110	armour	2018-04-14 20:55:57.584821	2018-04-14 20:55:57.584821
978	149	108	armour	2018-04-14 20:55:57.589649	2018-04-14 20:55:57.589649
979	149	107	armour	2018-04-14 20:55:57.594897	2018-04-14 20:55:57.594897
980	150	36	weapon	2018-04-14 20:55:57.598899	2018-04-14 20:55:57.598899
981	150	34	weapon	2018-04-14 20:55:57.602746	2018-04-14 20:55:57.602746
982	150	75	armour	2018-04-14 20:55:57.606919	2018-04-14 20:55:57.606919
983	150	37	armour	2018-04-14 20:55:57.610801	2018-04-14 20:55:57.610801
984	151	70	weapon	2018-04-14 20:55:57.614571	2018-04-14 20:55:57.614571
985	151	45	weapon	2018-04-14 20:55:57.618626	2018-04-14 20:55:57.618626
986	151	44	weapon	2018-04-14 20:55:57.622759	2018-04-14 20:55:57.622759
987	151	110	armour	2018-04-14 20:55:57.62938	2018-04-14 20:55:57.62938
988	151	75	armour	2018-04-14 20:55:57.634033	2018-04-14 20:55:57.634033
989	151	58	armour	2018-04-14 20:55:57.638329	2018-04-14 20:55:57.638329
990	151	50	armour	2018-04-14 20:55:57.644001	2018-04-14 20:55:57.644001
991	152	76	weapon	2018-04-14 20:55:57.649888	2018-04-14 20:55:57.649888
992	152	44	weapon	2018-04-14 20:55:57.654317	2018-04-14 20:55:57.654317
993	152	109	armour	2018-04-14 20:55:57.658151	2018-04-14 20:55:57.658151
994	152	79	armour	2018-04-14 20:55:57.661953	2018-04-14 20:55:57.661953
995	152	75	armour	2018-04-14 20:55:57.665759	2018-04-14 20:55:57.665759
996	152	57	armour	2018-04-14 20:55:57.669476	2018-04-14 20:55:57.669476
997	153	29	weapon	2018-04-14 20:55:57.673685	2018-04-14 20:55:57.673685
998	153	10	weapon	2018-04-14 20:55:57.677969	2018-04-14 20:55:57.677969
999	153	116	armour	2018-04-14 20:55:57.681916	2018-04-14 20:55:57.681916
1000	153	109	armour	2018-04-14 20:55:57.686184	2018-04-14 20:55:57.686184
1001	153	75	armour	2018-04-14 20:55:57.690467	2018-04-14 20:55:57.690467
1002	153	57	armour	2018-04-14 20:55:57.694368	2018-04-14 20:55:57.694368
1003	153	54	armour	2018-04-14 20:55:57.698513	2018-04-14 20:55:57.698513
1004	153	49	armour	2018-04-14 20:55:57.702378	2018-04-14 20:55:57.702378
1005	153	37	armour	2018-04-14 20:55:57.706616	2018-04-14 20:55:57.706616
1006	154	89	weapon	2018-04-14 20:55:57.710642	2018-04-14 20:55:57.710642
1007	154	29	weapon	2018-04-14 20:55:57.714587	2018-04-14 20:55:57.714587
1008	154	116	armour	2018-04-14 20:55:57.718553	2018-04-14 20:55:57.718553
1009	154	109	armour	2018-04-14 20:55:57.725243	2018-04-14 20:55:57.725243
1010	154	75	armour	2018-04-14 20:55:57.731567	2018-04-14 20:55:57.731567
1011	154	57	armour	2018-04-14 20:55:57.736709	2018-04-14 20:55:57.736709
1012	154	54	armour	2018-04-14 20:55:57.740812	2018-04-14 20:55:57.740812
1013	154	49	armour	2018-04-14 20:55:57.748412	2018-04-14 20:55:57.748412
1014	154	37	armour	2018-04-14 20:55:57.753046	2018-04-14 20:55:57.753046
1015	155	36	weapon	2018-04-14 20:55:57.759387	2018-04-14 20:55:57.759387
1016	155	35	weapon	2018-04-14 20:55:57.767223	2018-04-14 20:55:57.767223
1017	155	75	armour	2018-04-14 20:55:57.774052	2018-04-14 20:55:57.774052
1018	155	37	armour	2018-04-14 20:55:57.779495	2018-04-14 20:55:57.779495
1019	156	117	weapon	2018-04-14 20:55:57.78361	2018-04-14 20:55:57.78361
1020	156	30	weapon	2018-04-14 20:55:57.78762	2018-04-14 20:55:57.78762
1021	156	110	armour	2018-04-14 20:55:57.79204	2018-04-14 20:55:57.79204
1022	156	109	armour	2018-04-14 20:55:57.797594	2018-04-14 20:55:57.797594
1023	156	75	armour	2018-04-14 20:55:57.80402	2018-04-14 20:55:57.80402
1024	157	104	weapon	2018-04-14 20:55:57.810979	2018-04-14 20:55:57.810979
1025	157	105	armour	2018-04-14 20:55:57.816025	2018-04-14 20:55:57.816025
1026	157	75	armour	2018-04-14 20:55:57.820358	2018-04-14 20:55:57.820358
1027	159	114	weapon	2018-04-14 20:55:57.824664	2018-04-14 20:55:57.824664
1028	159	44	weapon	2018-04-14 20:55:57.828916	2018-04-14 20:55:57.828916
1029	159	116	armour	2018-04-14 20:55:57.833063	2018-04-14 20:55:57.833063
1030	159	109	armour	2018-04-14 20:55:57.837073	2018-04-14 20:55:57.837073
1031	159	75	armour	2018-04-14 20:55:57.841097	2018-04-14 20:55:57.841097
1032	159	57	armour	2018-04-14 20:55:57.845202	2018-04-14 20:55:57.845202
1033	160	114	weapon	2018-04-14 20:55:57.849397	2018-04-14 20:55:57.849397
1034	160	44	weapon	2018-04-14 20:55:57.85368	2018-04-14 20:55:57.85368
1035	160	116	armour	2018-04-14 20:55:57.85811	2018-04-14 20:55:57.85811
1036	160	109	armour	2018-04-14 20:55:57.862447	2018-04-14 20:55:57.862447
1037	160	75	armour	2018-04-14 20:55:57.866828	2018-04-14 20:55:57.866828
1038	160	57	armour	2018-04-14 20:55:57.870948	2018-04-14 20:55:57.870948
1039	162	66	weapon	2018-04-14 20:55:57.876187	2018-04-14 20:55:57.876187
1040	162	75	armour	2018-04-14 20:55:57.881811	2018-04-14 20:55:57.881811
1041	162	40	armour	2018-04-14 20:55:57.888519	2018-04-14 20:55:57.888519
1042	163	85	weapon	2018-04-14 20:55:57.894978	2018-04-14 20:55:57.894978
1043	163	81	weapon	2018-04-14 20:55:57.90062	2018-04-14 20:55:57.90062
1044	163	64	weapon	2018-04-14 20:55:57.908495	2018-04-14 20:55:57.908495
1045	163	116	armour	2018-04-14 20:55:57.915689	2018-04-14 20:55:57.915689
1046	163	109	armour	2018-04-14 20:55:57.921222	2018-04-14 20:55:57.921222
1047	163	75	armour	2018-04-14 20:55:57.926655	2018-04-14 20:55:57.926655
1048	163	57	armour	2018-04-14 20:55:57.934821	2018-04-14 20:55:57.934821
1049	163	54	armour	2018-04-14 20:55:57.940554	2018-04-14 20:55:57.940554
1050	163	49	armour	2018-04-14 20:55:57.946127	2018-04-14 20:55:57.946127
1051	163	37	armour	2018-04-14 20:55:57.952378	2018-04-14 20:55:57.952378
1052	164	44	weapon	2018-04-14 20:55:57.959048	2018-04-14 20:55:57.959048
1053	164	41	weapon	2018-04-14 20:55:57.964743	2018-04-14 20:55:57.964743
1054	164	109	armour	2018-04-14 20:55:57.97397	2018-04-14 20:55:57.97397
1055	164	79	armour	2018-04-14 20:55:57.981031	2018-04-14 20:55:57.981031
1056	164	75	armour	2018-04-14 20:55:57.98687	2018-04-14 20:55:57.98687
1057	164	57	armour	2018-04-14 20:55:57.993191	2018-04-14 20:55:57.993191
1058	165	41	weapon	2018-04-14 20:55:58.000494	2018-04-14 20:55:58.000494
1059	165	5	weapon	2018-04-14 20:55:58.006435	2018-04-14 20:55:58.006435
1060	165	110	armour	2018-04-14 20:55:58.014007	2018-04-14 20:55:58.014007
1061	165	92	armour	2018-04-14 20:55:58.019193	2018-04-14 20:55:58.019193
1062	165	75	armour	2018-04-14 20:55:58.027308	2018-04-14 20:55:58.027308
1063	165	58	armour	2018-04-14 20:55:58.033719	2018-04-14 20:55:58.033719
1064	166	82	weapon	2018-04-14 20:55:58.039419	2018-04-14 20:55:58.039419
1065	166	83	armour	2018-04-14 20:55:58.045433	2018-04-14 20:55:58.045433
1066	166	75	armour	2018-04-14 20:55:58.060422	2018-04-14 20:55:58.060422
1067	167	102	weapon	2018-04-14 20:55:58.064422	2018-04-14 20:55:58.064422
1068	167	75	armour	2018-04-14 20:55:58.068281	2018-04-14 20:55:58.068281
1069	167	21	armour	2018-04-14 20:55:58.07206	2018-04-14 20:55:58.07206
1070	168	102	weapon	2018-04-14 20:55:58.075899	2018-04-14 20:55:58.075899
1071	168	116	armour	2018-04-14 20:55:58.079733	2018-04-14 20:55:58.079733
1072	168	75	armour	2018-04-14 20:55:58.083457	2018-04-14 20:55:58.083457
1073	168	54	armour	2018-04-14 20:55:58.087256	2018-04-14 20:55:58.087256
1074	168	49	armour	2018-04-14 20:55:58.090898	2018-04-14 20:55:58.090898
1075	168	37	armour	2018-04-14 20:55:58.094399	2018-04-14 20:55:58.094399
1076	169	102	weapon	2018-04-14 20:55:58.098081	2018-04-14 20:55:58.098081
1077	169	99	weapon	2018-04-14 20:55:58.101835	2018-04-14 20:55:58.101835
1078	169	93	weapon	2018-04-14 20:55:58.105311	2018-04-14 20:55:58.105311
1079	169	75	armour	2018-04-14 20:55:58.109197	2018-04-14 20:55:58.109197
1080	169	56	armour	2018-04-14 20:55:58.112842	2018-04-14 20:55:58.112842
1081	170	3	weapon	2018-04-14 20:55:58.116497	2018-04-14 20:55:58.116497
1082	170	31	armour	2018-04-14 20:55:58.120101	2018-04-14 20:55:58.120101
1083	171	10	weapon	2018-04-14 20:55:58.124655	2018-04-14 20:55:58.124655
1084	171	116	armour	2018-04-14 20:55:58.129701	2018-04-14 20:55:58.129701
1085	171	109	armour	2018-04-14 20:55:58.133384	2018-04-14 20:55:58.133384
1086	171	75	armour	2018-04-14 20:55:58.137147	2018-04-14 20:55:58.137147
1087	171	57	armour	2018-04-14 20:55:58.14109	2018-04-14 20:55:58.14109
1088	171	54	armour	2018-04-14 20:55:58.144926	2018-04-14 20:55:58.144926
1089	171	49	armour	2018-04-14 20:55:58.148782	2018-04-14 20:55:58.148782
1090	171	37	armour	2018-04-14 20:55:58.152545	2018-04-14 20:55:58.152545
1091	172	63	weapon	2018-04-14 20:55:58.15618	2018-04-14 20:55:58.15618
1092	172	19	weapon	2018-04-14 20:55:58.160138	2018-04-14 20:55:58.160138
1093	172	109	armour	2018-04-14 20:55:58.163781	2018-04-14 20:55:58.163781
1094	172	80	armour	2018-04-14 20:55:58.167469	2018-04-14 20:55:58.167469
1095	172	75	armour	2018-04-14 20:55:58.171299	2018-04-14 20:55:58.171299
1096	173	82	weapon	2018-04-14 20:55:58.177615	2018-04-14 20:55:58.177615
1097	173	91	armour	2018-04-14 20:55:58.182485	2018-04-14 20:55:58.182485
1098	173	87	armour	2018-04-14 20:55:58.18624	2018-04-14 20:55:58.18624
1099	173	83	armour	2018-04-14 20:55:58.18995	2018-04-14 20:55:58.18995
1100	173	75	armour	2018-04-14 20:55:58.193796	2018-04-14 20:55:58.193796
1101	173	21	armour	2018-04-14 20:55:58.197456	2018-04-14 20:55:58.197456
1102	174	90	weapon	2018-04-14 20:55:58.201118	2018-04-14 20:55:58.201118
1103	174	91	armour	2018-04-14 20:55:58.204665	2018-04-14 20:55:58.204665
1104	174	75	armour	2018-04-14 20:55:58.208676	2018-04-14 20:55:58.208676
1105	175	114	weapon	2018-04-14 20:55:58.212305	2018-04-14 20:55:58.212305
1106	175	52	weapon	2018-04-14 20:55:58.216075	2018-04-14 20:55:58.216075
1107	175	47	weapon	2018-04-14 20:55:58.219996	2018-04-14 20:55:58.219996
1108	175	35	weapon	2018-04-14 20:55:58.223641	2018-04-14 20:55:58.223641
1109	175	75	armour	2018-04-14 20:55:58.227176	2018-04-14 20:55:58.227176
1110	175	49	armour	2018-04-14 20:55:58.230915	2018-04-14 20:55:58.230915
1111	180	114	weapon	2018-04-14 20:55:58.234552	2018-04-14 20:55:58.234552
1112	180	116	armour	2018-04-14 20:55:58.239737	2018-04-14 20:55:58.239737
1113	180	75	armour	2018-04-14 20:55:58.243725	2018-04-14 20:55:58.243725
1114	181	118	weapon	2018-04-14 20:55:58.247622	2018-04-14 20:55:58.247622
1115	181	119	armour	2018-04-14 20:55:58.251389	2018-04-14 20:55:58.251389
1116	181	75	armour	2018-04-14 20:55:58.254884	2018-04-14 20:55:58.254884
1117	182	19	weapon	2018-04-14 20:55:58.258879	2018-04-14 20:55:58.258879
1118	182	75	armour	2018-04-14 20:55:58.262677	2018-04-14 20:55:58.262677
1119	182	22	armour	2018-04-14 20:55:58.266358	2018-04-14 20:55:58.266358
1120	183	86	weapon	2018-04-14 20:55:58.270049	2018-04-14 20:55:58.270049
1121	183	87	armour	2018-04-14 20:55:58.273667	2018-04-14 20:55:58.273667
1122	183	75	armour	2018-04-14 20:55:58.278472	2018-04-14 20:55:58.278472
1123	184	114	weapon	2018-04-14 20:55:58.282373	2018-04-14 20:55:58.282373
1124	184	52	weapon	2018-04-14 20:55:58.286184	2018-04-14 20:55:58.286184
1125	184	47	weapon	2018-04-14 20:55:58.28986	2018-04-14 20:55:58.28986
1126	184	35	weapon	2018-04-14 20:55:58.293793	2018-04-14 20:55:58.293793
1127	184	116	armour	2018-04-14 20:55:58.297555	2018-04-14 20:55:58.297555
1128	184	75	armour	2018-04-14 20:55:58.30117	2018-04-14 20:55:58.30117
1129	184	54	armour	2018-04-14 20:55:58.304984	2018-04-14 20:55:58.304984
1130	184	49	armour	2018-04-14 20:55:58.308973	2018-04-14 20:55:58.308973
1131	184	37	armour	2018-04-14 20:55:58.312928	2018-04-14 20:55:58.312928
1132	185	85	weapon	2018-04-14 20:55:58.316763	2018-04-14 20:55:58.316763
1133	185	81	weapon	2018-04-14 20:55:58.320351	2018-04-14 20:55:58.320351
1134	185	116	armour	2018-04-14 20:55:58.323998	2018-04-14 20:55:58.323998
1135	185	109	armour	2018-04-14 20:55:58.327891	2018-04-14 20:55:58.327891
1136	185	75	armour	2018-04-14 20:55:58.331832	2018-04-14 20:55:58.331832
1137	185	57	armour	2018-04-14 20:55:58.335727	2018-04-14 20:55:58.335727
1138	185	54	armour	2018-04-14 20:55:58.339692	2018-04-14 20:55:58.339692
1139	185	49	armour	2018-04-14 20:55:58.343616	2018-04-14 20:55:58.343616
1140	185	37	armour	2018-04-14 20:55:58.347539	2018-04-14 20:55:58.347539
1141	186	118	weapon	2018-04-14 20:55:58.351638	2018-04-14 20:55:58.351638
1142	186	104	weapon	2018-04-14 20:55:58.355456	2018-04-14 20:55:58.355456
1143	186	90	weapon	2018-04-14 20:55:58.35926	2018-04-14 20:55:58.35926
1144	186	86	weapon	2018-04-14 20:55:58.363091	2018-04-14 20:55:58.363091
1145	186	82	weapon	2018-04-14 20:55:58.366723	2018-04-14 20:55:58.366723
1146	186	77	weapon	2018-04-14 20:55:58.370632	2018-04-14 20:55:58.370632
1147	186	20	weapon	2018-04-14 20:55:58.37438	2018-04-14 20:55:58.37438
1148	186	119	armour	2018-04-14 20:55:58.378347	2018-04-14 20:55:58.378347
1149	186	105	armour	2018-04-14 20:55:58.382187	2018-04-14 20:55:58.382187
1150	186	91	armour	2018-04-14 20:55:58.385889	2018-04-14 20:55:58.385889
1151	186	87	armour	2018-04-14 20:55:58.389795	2018-04-14 20:55:58.389795
1152	186	83	armour	2018-04-14 20:55:58.393561	2018-04-14 20:55:58.393561
1153	186	78	armour	2018-04-14 20:55:58.398854	2018-04-14 20:55:58.398854
1154	186	21	armour	2018-04-14 20:55:58.405115	2018-04-14 20:55:58.405115
1155	187	48	weapon	2018-04-14 20:55:58.41212	2018-04-14 20:55:58.41212
1156	187	47	weapon	2018-04-14 20:55:58.417429	2018-04-14 20:55:58.417429
1157	187	75	armour	2018-04-14 20:55:58.421308	2018-04-14 20:55:58.421308
1158	187	49	armour	2018-04-14 20:55:58.424826	2018-04-14 20:55:58.424826
1159	188	85	weapon	2018-04-14 20:55:58.428969	2018-04-14 20:55:58.428969
1160	188	81	weapon	2018-04-14 20:55:58.432731	2018-04-14 20:55:58.432731
1161	188	64	weapon	2018-04-14 20:55:58.436448	2018-04-14 20:55:58.436448
1162	188	116	armour	2018-04-14 20:55:58.440253	2018-04-14 20:55:58.440253
1163	188	109	armour	2018-04-14 20:55:58.444106	2018-04-14 20:55:58.444106
1164	188	75	armour	2018-04-14 20:55:58.447705	2018-04-14 20:55:58.447705
1165	188	57	armour	2018-04-14 20:55:58.451347	2018-04-14 20:55:58.451347
1166	188	54	armour	2018-04-14 20:55:58.455097	2018-04-14 20:55:58.455097
1167	188	49	armour	2018-04-14 20:55:58.459099	2018-04-14 20:55:58.459099
1168	188	37	armour	2018-04-14 20:55:58.463021	2018-04-14 20:55:58.463021
1169	190	115	weapon	2018-04-14 20:55:58.466862	2018-04-14 20:55:58.466862
1170	190	53	weapon	2018-04-14 20:55:58.470773	2018-04-14 20:55:58.470773
1171	190	48	weapon	2018-04-14 20:55:58.474276	2018-04-14 20:55:58.474276
1172	190	36	weapon	2018-04-14 20:55:58.478277	2018-04-14 20:55:58.478277
1173	190	116	armour	2018-04-14 20:55:58.482071	2018-04-14 20:55:58.482071
1174	190	75	armour	2018-04-14 20:55:58.486666	2018-04-14 20:55:58.486666
1175	190	54	armour	2018-04-14 20:55:58.492884	2018-04-14 20:55:58.492884
1176	190	49	armour	2018-04-14 20:55:58.496898	2018-04-14 20:55:58.496898
1177	190	37	armour	2018-04-14 20:55:58.50062	2018-04-14 20:55:58.50062
1178	191	114	weapon	2018-04-14 20:55:58.504379	2018-04-14 20:55:58.504379
1179	191	52	weapon	2018-04-14 20:55:58.508424	2018-04-14 20:55:58.508424
1180	191	47	weapon	2018-04-14 20:55:58.512452	2018-04-14 20:55:58.512452
1181	191	35	weapon	2018-04-14 20:55:58.516281	2018-04-14 20:55:58.516281
1182	191	116	armour	2018-04-14 20:55:58.520864	2018-04-14 20:55:58.520864
1183	191	75	armour	2018-04-14 20:55:58.524693	2018-04-14 20:55:58.524693
1184	191	54	armour	2018-04-14 20:55:58.528863	2018-04-14 20:55:58.528863
1185	191	49	armour	2018-04-14 20:55:58.532789	2018-04-14 20:55:58.532789
1186	191	37	armour	2018-04-14 20:55:58.536548	2018-04-14 20:55:58.536548
1187	192	86	weapon	2018-04-14 20:55:58.540255	2018-04-14 20:55:58.540255
1188	192	62	weapon	2018-04-14 20:55:58.546538	2018-04-14 20:55:58.546538
1189	192	88	armour	2018-04-14 20:55:58.553218	2018-04-14 20:55:58.553218
1190	192	75	armour	2018-04-14 20:55:58.558889	2018-04-14 20:55:58.558889
1191	193	44	weapon	2018-04-14 20:55:58.563759	2018-04-14 20:55:58.563759
1192	193	29	weapon	2018-04-14 20:55:58.567505	2018-04-14 20:55:58.567505
1193	193	116	armour	2018-04-14 20:55:58.571467	2018-04-14 20:55:58.571467
1194	193	110	armour	2018-04-14 20:55:58.57516	2018-04-14 20:55:58.57516
1195	193	75	armour	2018-04-14 20:55:58.578869	2018-04-14 20:55:58.578869
1196	193	58	armour	2018-04-14 20:55:58.582825	2018-04-14 20:55:58.582825
1197	194	43	weapon	2018-04-14 20:55:58.586661	2018-04-14 20:55:58.586661
1198	194	42	weapon	2018-04-14 20:55:58.590506	2018-04-14 20:55:58.590506
1199	194	41	weapon	2018-04-14 20:55:58.594493	2018-04-14 20:55:58.594493
1200	194	110	armour	2018-04-14 20:55:58.598451	2018-04-14 20:55:58.598451
1201	194	108	armour	2018-04-14 20:55:58.60248	2018-04-14 20:55:58.60248
1202	194	107	armour	2018-04-14 20:55:58.60633	2018-04-14 20:55:58.60633
1203	196	30	weapon	2018-04-14 20:55:58.610312	2018-04-14 20:55:58.610312
1204	196	28	weapon	2018-04-14 20:55:58.61446	2018-04-14 20:55:58.61446
1205	196	27	weapon	2018-04-14 20:55:58.6183	2018-04-14 20:55:58.6183
1206	196	110	armour	2018-04-14 20:55:58.622248	2018-04-14 20:55:58.622248
1207	196	108	armour	2018-04-14 20:55:58.626235	2018-04-14 20:55:58.626235
1208	196	107	armour	2018-04-14 20:55:58.630302	2018-04-14 20:55:58.630302
1209	197	43	weapon	2018-04-14 20:55:58.633994	2018-04-14 20:55:58.633994
1210	197	42	weapon	2018-04-14 20:55:58.637961	2018-04-14 20:55:58.637961
1211	197	41	weapon	2018-04-14 20:55:58.641753	2018-04-14 20:55:58.641753
1212	197	112	armour	2018-04-14 20:55:58.650457	2018-04-14 20:55:58.650457
1213	197	111	armour	2018-04-14 20:55:58.654448	2018-04-14 20:55:58.654448
1214	198	85	weapon	2018-04-14 20:55:58.658258	2018-04-14 20:55:58.658258
1215	198	69	weapon	2018-04-14 20:55:58.662214	2018-04-14 20:55:58.662214
1216	198	110	armour	2018-04-14 20:55:58.666184	2018-04-14 20:55:58.666184
1217	198	109	armour	2018-04-14 20:55:58.669987	2018-04-14 20:55:58.669987
1218	198	88	armour	2018-04-14 20:55:58.673748	2018-04-14 20:55:58.673748
1219	198	75	armour	2018-04-14 20:55:58.679374	2018-04-14 20:55:58.679374
1220	198	58	armour	2018-04-14 20:55:58.684267	2018-04-14 20:55:58.684267
1221	198	57	armour	2018-04-14 20:55:58.688143	2018-04-14 20:55:58.688143
1222	199	53	weapon	2018-04-14 20:55:58.691863	2018-04-14 20:55:58.691863
1223	199	52	weapon	2018-04-14 20:55:58.695793	2018-04-14 20:55:58.695793
1224	199	75	armour	2018-04-14 20:55:58.699711	2018-04-14 20:55:58.699711
1225	199	54	armour	2018-04-14 20:55:58.703344	2018-04-14 20:55:58.703344
1226	200	23	weapon	2018-04-14 20:55:58.707149	2018-04-14 20:55:58.707149
1227	200	92	armour	2018-04-14 20:55:58.710947	2018-04-14 20:55:58.710947
1228	200	75	armour	2018-04-14 20:55:58.714736	2018-04-14 20:55:58.714736
1229	200	26	armour	2018-04-14 20:55:58.718443	2018-04-14 20:55:58.718443
1230	202	14	weapon	2018-04-14 20:55:58.722198	2018-04-14 20:55:58.722198
1231	202	112	armour	2018-04-14 20:55:58.725875	2018-04-14 20:55:58.725875
1232	202	111	armour	2018-04-14 20:55:58.729502	2018-04-14 20:55:58.729502
1233	202	73	armour	2018-04-14 20:55:58.733257	2018-04-14 20:55:58.733257
1234	203	14	weapon	2018-04-14 20:55:58.73701	2018-04-14 20:55:58.73701
1235	203	32	armour	2018-04-14 20:55:58.740599	2018-04-14 20:55:58.740599
1236	204	41	weapon	2018-04-14 20:55:58.744317	2018-04-14 20:55:58.744317
1237	204	84	armour	2018-04-14 20:55:58.748093	2018-04-14 20:55:58.748093
1238	204	75	armour	2018-04-14 20:55:58.752095	2018-04-14 20:55:58.752095
1239	204	37	armour	2018-04-14 20:55:58.75578	2018-04-14 20:55:58.75578
1240	205	102	weapon	2018-04-14 20:55:58.759472	2018-04-14 20:55:58.759472
1241	205	61	weapon	2018-04-14 20:55:58.763249	2018-04-14 20:55:58.763249
1242	205	107	armour	2018-04-14 20:55:58.768796	2018-04-14 20:55:58.768796
1243	205	75	armour	2018-04-14 20:55:58.773776	2018-04-14 20:55:58.773776
1244	205	32	armour	2018-04-14 20:55:58.778487	2018-04-14 20:55:58.778487
1245	206	68	weapon	2018-04-14 20:55:58.782418	2018-04-14 20:55:58.782418
1246	206	67	weapon	2018-04-14 20:55:58.786164	2018-04-14 20:55:58.786164
1247	206	66	weapon	2018-04-14 20:55:58.789741	2018-04-14 20:55:58.789741
1248	206	31	armour	2018-04-14 20:55:58.793466	2018-04-14 20:55:58.793466
1249	207	30	weapon	2018-04-14 20:55:58.797391	2018-04-14 20:55:58.797391
1250	207	28	weapon	2018-04-14 20:55:58.801224	2018-04-14 20:55:58.801224
1251	207	27	weapon	2018-04-14 20:55:58.804894	2018-04-14 20:55:58.804894
1252	207	110	armour	2018-04-14 20:55:58.808621	2018-04-14 20:55:58.808621
1253	207	108	armour	2018-04-14 20:55:58.813021	2018-04-14 20:55:58.813021
1254	207	107	armour	2018-04-14 20:55:58.8175	2018-04-14 20:55:58.8175
1255	208	89	weapon	2018-04-14 20:55:58.823987	2018-04-14 20:55:58.823987
1256	208	44	weapon	2018-04-14 20:55:58.82915	2018-04-14 20:55:58.82915
1257	208	75	armour	2018-04-14 20:55:58.83414	2018-04-14 20:55:58.83414
1258	208	49	armour	2018-04-14 20:55:58.838059	2018-04-14 20:55:58.838059
1259	209	64	weapon	2018-04-14 20:55:58.84184	2018-04-14 20:55:58.84184
1260	209	23	weapon	2018-04-14 20:55:58.845736	2018-04-14 20:55:58.845736
1261	209	110	armour	2018-04-14 20:55:58.84957	2018-04-14 20:55:58.84957
1262	209	75	armour	2018-04-14 20:55:58.853202	2018-04-14 20:55:58.853202
1263	209	60	armour	2018-04-14 20:55:58.857077	2018-04-14 20:55:58.857077
1264	210	64	weapon	2018-04-14 20:55:58.86064	2018-04-14 20:55:58.86064
1265	210	23	weapon	2018-04-14 20:55:58.864538	2018-04-14 20:55:58.864538
1266	210	110	armour	2018-04-14 20:55:58.868119	2018-04-14 20:55:58.868119
1267	210	75	armour	2018-04-14 20:55:58.871716	2018-04-14 20:55:58.871716
1268	210	60	armour	2018-04-14 20:55:58.876185	2018-04-14 20:55:58.876185
1269	211	76	weapon	2018-04-14 20:55:58.880153	2018-04-14 20:55:58.880153
1270	211	66	weapon	2018-04-14 20:55:58.883742	2018-04-14 20:55:58.883742
1271	211	116	armour	2018-04-14 20:55:58.887503	2018-04-14 20:55:58.887503
1272	211	109	armour	2018-04-14 20:55:58.891289	2018-04-14 20:55:58.891289
1273	211	75	armour	2018-04-14 20:55:58.895183	2018-04-14 20:55:58.895183
1274	211	57	armour	2018-04-14 20:55:58.899242	2018-04-14 20:55:58.899242
1275	211	54	armour	2018-04-14 20:55:58.903088	2018-04-14 20:55:58.903088
1276	211	49	armour	2018-04-14 20:55:58.9072	2018-04-14 20:55:58.9072
1277	211	37	armour	2018-04-14 20:55:58.911016	2018-04-14 20:55:58.911016
1278	212	115	weapon	2018-04-14 20:55:58.914965	2018-04-14 20:55:58.914965
1279	212	114	weapon	2018-04-14 20:55:58.918936	2018-04-14 20:55:58.918936
1280	212	116	armour	2018-04-14 20:55:58.922634	2018-04-14 20:55:58.922634
1281	212	75	armour	2018-04-14 20:55:58.926249	2018-04-14 20:55:58.926249
1282	213	93	weapon	2018-04-14 20:55:58.932894	2018-04-14 20:55:58.932894
1283	213	41	weapon	2018-04-14 20:55:58.937637	2018-04-14 20:55:58.937637
1284	213	75	armour	2018-04-14 20:55:58.941345	2018-04-14 20:55:58.941345
1285	213	17	armour	2018-04-14 20:55:58.94481	2018-04-14 20:55:58.94481
1286	214	47	weapon	2018-04-14 20:55:58.949891	2018-04-14 20:55:58.949891
1287	214	75	armour	2018-04-14 20:55:58.953727	2018-04-14 20:55:58.953727
1288	214	49	armour	2018-04-14 20:55:58.957724	2018-04-14 20:55:58.957724
1289	215	97	weapon	2018-04-14 20:55:58.962125	2018-04-14 20:55:58.962125
1290	215	96	weapon	2018-04-14 20:55:58.967695	2018-04-14 20:55:58.967695
1291	215	95	weapon	2018-04-14 20:55:58.973421	2018-04-14 20:55:58.973421
1292	215	94	weapon	2018-04-14 20:55:58.978647	2018-04-14 20:55:58.978647
1293	215	116	armour	2018-04-14 20:55:58.982817	2018-04-14 20:55:58.982817
1294	215	75	armour	2018-04-14 20:55:58.986632	2018-04-14 20:55:58.986632
1295	215	54	armour	2018-04-14 20:55:58.990673	2018-04-14 20:55:58.990673
1296	215	49	armour	2018-04-14 20:55:58.994389	2018-04-14 20:55:58.994389
1297	215	37	armour	2018-04-14 20:55:58.99898	2018-04-14 20:55:58.99898
1298	216	61	weapon	2018-04-14 20:55:59.002745	2018-04-14 20:55:59.002745
1299	216	110	armour	2018-04-14 20:55:59.006417	2018-04-14 20:55:59.006417
1300	216	107	armour	2018-04-14 20:55:59.01029	2018-04-14 20:55:59.01029
1301	216	75	armour	2018-04-14 20:55:59.01451	2018-04-14 20:55:59.01451
1302	217	30	weapon	2018-04-14 20:55:59.018218	2018-04-14 20:55:59.018218
1303	217	109	armour	2018-04-14 20:55:59.022079	2018-04-14 20:55:59.022079
1304	217	75	armour	2018-04-14 20:55:59.025963	2018-04-14 20:55:59.025963
1305	217	57	armour	2018-04-14 20:55:59.031236	2018-04-14 20:55:59.031236
1306	218	114	weapon	2018-04-14 20:55:59.03546	2018-04-14 20:55:59.03546
1307	218	52	weapon	2018-04-14 20:55:59.039443	2018-04-14 20:55:59.039443
1308	218	47	weapon	2018-04-14 20:55:59.043219	2018-04-14 20:55:59.043219
1309	218	35	weapon	2018-04-14 20:55:59.048316	2018-04-14 20:55:59.048316
1310	218	110	armour	2018-04-14 20:55:59.052234	2018-04-14 20:55:59.052234
1311	218	75	armour	2018-04-14 20:55:59.057277	2018-04-14 20:55:59.057277
1312	218	58	armour	2018-04-14 20:55:59.061292	2018-04-14 20:55:59.061292
1313	219	114	weapon	2018-04-14 20:55:59.067583	2018-04-14 20:55:59.067583
1314	219	52	weapon	2018-04-14 20:55:59.07196	2018-04-14 20:55:59.07196
1315	219	47	weapon	2018-04-14 20:55:59.075802	2018-04-14 20:55:59.075802
1316	219	35	weapon	2018-04-14 20:55:59.079651	2018-04-14 20:55:59.079651
1317	219	110	armour	2018-04-14 20:55:59.083759	2018-04-14 20:55:59.083759
1318	219	109	armour	2018-04-14 20:55:59.087614	2018-04-14 20:55:59.087614
1319	219	75	armour	2018-04-14 20:55:59.09137	2018-04-14 20:55:59.09137
1320	219	58	armour	2018-04-14 20:55:59.095323	2018-04-14 20:55:59.095323
1321	219	57	armour	2018-04-14 20:55:59.09958	2018-04-14 20:55:59.09958
1322	219	37	armour	2018-04-14 20:55:59.103394	2018-04-14 20:55:59.103394
1323	220	114	weapon	2018-04-14 20:55:59.107305	2018-04-14 20:55:59.107305
1324	220	52	weapon	2018-04-14 20:55:59.111232	2018-04-14 20:55:59.111232
1325	220	47	weapon	2018-04-14 20:55:59.11531	2018-04-14 20:55:59.11531
1326	220	35	weapon	2018-04-14 20:55:59.120709	2018-04-14 20:55:59.120709
1327	220	110	armour	2018-04-14 20:55:59.124485	2018-04-14 20:55:59.124485
1328	220	75	armour	2018-04-14 20:55:59.128521	2018-04-14 20:55:59.128521
1329	220	58	armour	2018-04-14 20:55:59.13269	2018-04-14 20:55:59.13269
1330	221	114	weapon	2018-04-14 20:55:59.136542	2018-04-14 20:55:59.136542
1331	221	52	weapon	2018-04-14 20:55:59.14315	2018-04-14 20:55:59.14315
1332	221	47	weapon	2018-04-14 20:55:59.149625	2018-04-14 20:55:59.149625
1333	221	110	armour	2018-04-14 20:55:59.156016	2018-04-14 20:55:59.156016
1334	221	109	armour	2018-04-14 20:55:59.160533	2018-04-14 20:55:59.160533
1335	221	75	armour	2018-04-14 20:55:59.1649	2018-04-14 20:55:59.1649
1336	221	58	armour	2018-04-14 20:55:59.169259	2018-04-14 20:55:59.169259
1337	221	57	armour	2018-04-14 20:55:59.173428	2018-04-14 20:55:59.173428
1338	222	52	weapon	2018-04-14 20:55:59.177268	2018-04-14 20:55:59.177268
1339	222	75	armour	2018-04-14 20:55:59.181413	2018-04-14 20:55:59.181413
1340	222	54	armour	2018-04-14 20:55:59.185587	2018-04-14 20:55:59.185587
1341	223	67	weapon	2018-04-14 20:55:59.189441	2018-04-14 20:55:59.189441
1342	223	75	armour	2018-04-14 20:55:59.19333	2018-04-14 20:55:59.19333
1343	225	89	weapon	2018-04-14 20:55:59.199039	2018-04-14 20:55:59.199039
1344	225	116	armour	2018-04-14 20:55:59.203885	2018-04-14 20:55:59.203885
1345	225	109	armour	2018-04-14 20:55:59.209475	2018-04-14 20:55:59.209475
1346	225	75	armour	2018-04-14 20:55:59.21622	2018-04-14 20:55:59.21622
1347	225	57	armour	2018-04-14 20:55:59.225315	2018-04-14 20:55:59.225315
1348	225	54	armour	2018-04-14 20:55:59.229239	2018-04-14 20:55:59.229239
1349	225	49	armour	2018-04-14 20:55:59.234562	2018-04-14 20:55:59.234562
1350	225	37	armour	2018-04-14 20:55:59.238328	2018-04-14 20:55:59.238328
1351	229	118	weapon	2018-04-14 20:55:59.242468	2018-04-14 20:55:59.242468
1352	229	119	armour	2018-04-14 20:55:59.246517	2018-04-14 20:55:59.246517
1353	229	75	armour	2018-04-14 20:55:59.250393	2018-04-14 20:55:59.250393
1354	230	90	weapon	2018-04-14 20:55:59.254399	2018-04-14 20:55:59.254399
1355	230	89	weapon	2018-04-14 20:55:59.258116	2018-04-14 20:55:59.258116
1356	230	116	armour	2018-04-14 20:55:59.261984	2018-04-14 20:55:59.261984
1357	230	109	armour	2018-04-14 20:55:59.268187	2018-04-14 20:55:59.268187
1358	230	75	armour	2018-04-14 20:55:59.273035	2018-04-14 20:55:59.273035
1359	230	57	armour	2018-04-14 20:55:59.27677	2018-04-14 20:55:59.27677
1360	230	54	armour	2018-04-14 20:55:59.281485	2018-04-14 20:55:59.281485
1361	230	49	armour	2018-04-14 20:55:59.286124	2018-04-14 20:55:59.286124
1362	230	37	armour	2018-04-14 20:55:59.289922	2018-04-14 20:55:59.289922
1363	231	19	weapon	2018-04-14 20:55:59.293623	2018-04-14 20:55:59.293623
1364	231	109	armour	2018-04-14 20:55:59.297398	2018-04-14 20:55:59.297398
1365	231	75	armour	2018-04-14 20:55:59.301037	2018-04-14 20:55:59.301037
1366	231	57	armour	2018-04-14 20:55:59.304681	2018-04-14 20:55:59.304681
1367	231	22	armour	2018-04-14 20:55:59.30872	2018-04-14 20:55:59.30872
\.


--
-- Name: monster_drop_abilities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Kieran
--

SELECT pg_catalog.setval('monster_drop_abilities_id_seq', 1367, true);


--
-- Data for Name: monsters; Type: TABLE DATA; Schema: public; Owner: Kieran
--

COPY monsters (id, name, slug, health, overkill, strength, defense, magic, magic_defense, mp, agility, luck, ap, evasion, accuracy, gil, boss, notes, skills, created_at, updated_at) FROM stdin;
1	Abadon	abadon	380000	10000	40	180	95	160	999	120	0	10000	0	0	0	t	Unlocked at the Monster Arena by capturing 1 of each fiend from Inside Sin	\N	2018-04-14 20:55:48.127943	2018-04-14 20:55:48.127943
2	Abyss Worm	abyss-worm	480000	12000	60	24	93	63	999	22	0	8000	0	0	0	t	Unlocked at the Monster Arena by capturing 1 of each fiend from Sanubia Desert	\N	2018-04-14 20:55:48.134104	2018-04-14 20:55:48.134104
3	Achelous	achelous	5100	7500	33	10	52	20	85	20	15	730	0	0	420	f	\N	\N	2018-04-14 20:55:48.138894	2018-04-14 20:55:48.138894
4	Adamantoise	adamantoise	54400	11036	38	90	31	90	40	15	15	12500	0	0	2200	f	\N	\N	2018-04-14 20:55:48.14294	2018-04-14 20:55:48.14294
5	Aerouge	aerouge	200	300	1	1	16	120	220	11	15	92	13	0	144	f	\N	\N	2018-04-14 20:55:48.146173	2018-04-14 20:55:48.146173
6	Ahriman	ahriman	2800	4200	1	1	38	180	400	24	15	2200	18	0	650	f	\N	\N	2018-04-14 20:55:48.149207	2018-04-14 20:55:48.149207
7	Alcyone	alcyone	430	645	16	1	1	1	42	26	15	310	15	0	240	f	\N	\N	2018-04-14 20:55:48.153375	2018-04-14 20:55:48.153375
8	Anacondaur	anacondaur	5800	4060	27	1	48	1	70	16	15	1380	0	0	750	f	\N	Stone Breath	2018-04-14 20:55:48.1564	2018-04-14 20:55:48.1564
9	Anima	anima	18000	1400	25	1	20	1	50	25	20	2500	0	30	3000	t	Seymour summons him during your battle against him	\N	2018-04-14 20:55:48.159285	2018-04-14 20:55:48.159285
10	Aqua Flan	aqua-flan	2025	3038	1	100	22	1	1	9	15	240	0	0	340	f	\N	\N	2018-04-14 20:55:48.161999	2018-04-14 20:55:48.161999
11	Bandersnatch	bandersnatch	1800	2700	32	1	1	180	75	32	15	820	11	0	880	f	\N	\N	2018-04-14 20:55:48.165371	2018-04-14 20:55:48.165371
12	Barbatos	barbatos	95000	13560	42	90	38	15	480	28	15	17500	0	0	1550	f	\N	\N	2018-04-14 20:55:48.170063	2018-04-14 20:55:48.170063
13	Bashura	bashura	17000	6972	34	45	1	1	5	16	15	1860	0	0	730	f	\N	\N	2018-04-14 20:55:48.174814	2018-04-14 20:55:48.174814
14	Basilisk	basilisk	2025	924	14	1	35	1	20	9	15	140	0	0	125	f	\N	Stone Breath	2018-04-14 20:55:48.17973	2018-04-14 20:55:48.17973
15	Bat Eye	bat-eye	380	570	1	1	29	120	280	16	15	240	13	0	320	f	\N	\N	2018-04-14 20:55:48.182936	2018-04-14 20:55:48.182936
16	Behemoth	behemoth	23000	6972	43	1	37	1	480	23	15	6540	0	0	1350	f	\N	Mighty Guard	2018-04-14 20:55:48.185576	2018-04-14 20:55:48.185576
17	Behemoth King	behemoth-king	67500	13560	46	25	44	25	700	27	15	16800	0	0	1850	f	\N	Mighty Guard	2018-04-14 20:55:48.188351	2018-04-14 20:55:48.188351
18	Biran Ronso	biran-ronso	0	2500	25	30	2	50	200	0	15	4500	0	100	1500	t	Fights with Yenke Ronso against a lone Kimahri	Doom, Mighty Guard, Self-Destruct, Thrust Kick	2018-04-14 20:55:48.191287	2018-04-14 20:55:48.191287
19	Bite Bug	bite-bug	200	300	13	1	1	1	10	15	15	40	12	0	62	f	\N	\N	2018-04-14 20:55:48.194265	2018-04-14 20:55:48.194265
20	Black Element	black-element	7600	11400	1	240	28	20	500	15	0	3150	0	0	1040	f	\N	\N	2018-04-14 20:55:48.197909	2018-04-14 20:55:48.197909
21	Blue Element	blue-element	1500	2250	1	120	27	1	220	9	15	240	0	0	180	f	\N	\N	2018-04-14 20:55:48.20119	2018-04-14 20:55:48.20119
22	Bomb	bomb	850	560	19	1	20	1	30	11	15	22	0	0	70	f	\N	Self-Destruct	2018-04-14 20:55:48.205326	2018-04-14 20:55:48.205326
23	Bomb King	bomb-king	480000	10000	73	200	71	200	999	46	0	10000	0	0	0	t	Unlocked at the Monster Arena by capturing 5 of each bomb species	\N	2018-04-14 20:55:48.208465	2018-04-14 20:55:48.208465
24	Braska's Final Aeon	braska-s-final-aeon	180000	20000	50	100	50	100	0	40	15	0	0	10	0	t	Use Tidus' trigger command if its overdrive gauge gets close to full to reduce the gauge	\N	2018-04-14 20:55:48.212334	2018-04-14 20:55:48.212334
25	Buer	buer	230	345	1	1	22	120	250	12	15	92	12	0	132	f	\N	\N	2018-04-14 20:55:48.215968	2018-04-14 20:55:48.215968
26	Bunyip	bunyip	400	600	22	1	1	120	15	6	15	48	0	0	97	f	\N	\N	2018-04-14 20:55:48.218694	2018-04-14 20:55:48.218694
27	Cactuar	cactuar	800	1200	23	1	1	255	1	24	15	8000	20	0	1500	f	\N	\N	2018-04-14 20:55:48.221407	2018-04-14 20:55:48.221407
28	Cactuar King	cactuar-king	100000	10000	255	100	255	255	999	80	0	8000	0	240	0	t	Unlocked at the Monster Arena by capturing 1 of each fiend from Thunder Plains	\N	2018-04-14 20:55:48.224209	2018-04-14 20:55:48.224209
29	Catastrophe	catastrophe	2200000	99999	120	80	77	80	999	34	0	50000	0	0	0	t	Unlocked at the Monster Arena by conquering 6 areas	\N	2018-04-14 20:55:48.22688	2018-04-14 20:55:48.22688
30	Catoblepas	catoblepas	550000	10000	76	33	58	27	999	47	0	8000	0	0	0	t	Unlocked at the Monster Arena by capturing 1 of each fiend from Mt. Gagazet and Zanarkand Ruins	\N	2018-04-14 20:55:48.230712	2018-04-14 20:55:48.230712
31	Cave Iguion	cave-iguion	550	825	24	1	1	120	1	21	15	240	9	0	300	f	\N	\N	2018-04-14 20:55:48.235122	2018-04-14 20:55:48.235122
32	Chimera	chimera	5250	1432	25	1	22	1	130	9	15	1220	0	0	970	f	\N	Aqua Breath	2018-04-14 20:55:48.237804	2018-04-14 20:55:48.237804
33	Chimera Brain	chimera-brain	9800	4060	34	10	32	10	250	14	15	1200	0	0	1000	f	\N	Aqua Breath	2018-04-14 20:55:48.240574	2018-04-14 20:55:48.240574
34	Chimerageist	chimerageist	120000	10000	66	10	68	10	999	29	0	8000	0	0	0	t	Unlocked at the Monster Arena by capturing 1 of each fiend from Calm Lands	\N	2018-04-14 20:55:48.243219	2018-04-14 20:55:48.243219
35	Chocobo Eater	chocobo-eater	10000	800	25	25	20	35	5	12	15	90	0	25	970	t	You only receive the 2 Lv. 1 Key Spheres if you manage to push it over the cliff	\N	2018-04-14 20:55:48.245838	2018-04-14 20:55:48.245838
36	Coeurl	coeurl	6000	4060	38	1	26	40	480	18	15	1300	0	0	1100	f	\N	\N	2018-04-14 20:55:48.250134	2018-04-14 20:55:48.250134
37	Coeurlregina	coeurlregina	380000	10000	1	40	70	40	999	75	0	8000	0	0	0	t	Unlocked at the Monster Arena by capturing 1 of each fiend from Mushroom Rock	\N	2018-04-14 20:55:48.254654	2018-04-14 20:55:48.254654
38	Condor	condor	95	143	9	1	1	1	15	11	15	2	10	0	12	f	\N	\N	2018-04-14 20:55:48.257425	2018-04-14 20:55:48.257425
39	Crawler	crawler	16000	4000	25	100	30	50	1	20	15	4400	0	30	7000	t	Launches a Negator, if the Negator is destroyed it will countdown to Mana Beam	\N	2018-04-14 20:55:48.260142	2018-04-14 20:55:48.260142
40	Dark Element	dark-element	1800	2700	1	180	30	1	280	15	0	810	0	0	520	f	\N	\N	2018-04-14 20:55:48.262874	2018-04-14 20:55:48.262874
41	Dark Flan	dark-flan	12800	19200	1	220	30	200	250	11	15	3750	0	0	1080	f	\N	White Wind	2018-04-14 20:55:48.26564	2018-04-14 20:55:48.26564
42	Defender	defender	1200	4060	40	1	5	1	1	11	15	2700	0	0	1300	f	\N	\N	2018-04-14 20:55:48.268545	2018-04-14 20:55:48.268545
43	Defender X	defender-x	64000	4060	42	30	5	1	1	20	15	6600	0	0	3500	t	Has very strong physical attacks	\N	2018-04-14 20:55:48.271187	2018-04-14 20:55:48.271187
44	Defender Z	defender-z	42300	8848	45	70	5	70	1	16	15	6000	0	0	2400	f	\N	\N	2018-04-14 20:55:48.273569	2018-04-14 20:55:48.273569
45	Demonolith	demonolith	45000	13560	33	1	99	1	1	18	15	6000	0	0	2400	f	Counterattacks with Curse/Darkness/Poison/Silence, occasionally will try to Stone entire party	\N	2018-04-14 20:55:48.276699	2018-04-14 20:55:48.276699
46	Dingo	dingo	125	188	13	1	1	120	10	10	15	2	5	0	15	f	\N	\N	2018-04-14 20:55:48.281838	2018-04-14 20:55:48.281838
47	Dinonix	dinonix	140	210	14	1	1	120	25	13	15	9	5	0	27	f	\N	\N	2018-04-14 20:55:48.286511	2018-04-14 20:55:48.286511
48	Don Tonberry	don-tonberry	480000	10000	95	100	75	100	999	37	0	8000	0	0	0	t	Unlocked at the Monster Arena by capturing 1 of each fiend from the Cavern of the Stolen Fayth	\N	2018-04-14 20:55:48.289875	2018-04-14 20:55:48.289875
49	Dual Horn	dual-horn	1875	560	22	1	3	1	18	12	15	42	0	0	105	f	\N	Fire Breath	2018-04-14 20:55:48.292965	2018-04-14 20:55:48.292965
50	Earth Eater	earth-eater	1300000	99999	117	200	186	210	999	47	0	50000	0	0	0	t	Unlocked at the Monster Arena by conquering 2 areas	\N	2018-04-14 20:55:48.300676	2018-04-14 20:55:48.300676
51	Epaaj	epaaj	8700	4060	28	20	1	20	25	28	15	970	0	0	950	f	Its attacks have Poisontouch	\N	2018-04-14 20:55:48.303919	2018-04-14 20:55:48.303919
52	Espada	espada	280000	15000	44	100	31	160	999	51	0	8000	0	12	0	t	Unlocked at the Monster Arena by capturing 1 of each fiend from Macalania	\N	2018-04-14 20:55:48.306468	2018-04-14 20:55:48.306468
53	Evil Eye	evil-eye	310	465	1	1	26	120	300	15	15	300	13	0	205	f	\N	\N	2018-04-14 20:55:48.309071	2018-04-14 20:55:48.309071
54	Evrae	evrae	32000	2000	36	1	30	1	500	20	15	5400	0	100	2600	t	Use Tidus and Rikku's trigger commands to avoid its Poison Breath attack after it inhales	\N	2018-04-14 20:55:48.311725	2018-04-14 20:55:48.311725
55	Evrae Altana	evrae-altana	16384	2000	32	1	27	1	200	25	15	5800	0	100	3000	t	Use the trigger commands to break the locks for an easier fight (however you will miss a couple of items)	\N	2018-04-14 20:55:48.314427	2018-04-14 20:55:48.314427
56	Exoray	exoray	7400	11100	1	1	24	1	300	14	15	2400	0	0	840	f	\N	\N	2018-04-14 20:55:48.317535	2018-04-14 20:55:48.317535
57	Extractor	extractor	4000	600	23	1	15	1	10	15	15	660	0	30	2400	t	\N	\N	2018-04-14 20:55:48.320112	2018-04-14 20:55:48.320112
58	Fafnir	fafnir	1100000	13000	76	30	109	130	999	38	0	10000	0	0	0	t	Unlocked at the Monster Arena by capturing 4 of each dragon species	\N	2018-04-14 20:55:48.322676	2018-04-14 20:55:48.322676
59	Fallen Monk	fallen-monk	3300	4950	28	40	33	40	1	27	15	1200	0	0	540	f	Zombie version of Warrior Monk, its attacks can cause Zombie	\N	2018-04-14 20:55:48.325344	2018-04-14 20:55:48.325344
60	Fallen Monk (Flamethrower)	fallen-monk-flamethrower	3300	4950	28	40	33	40	1	27	15	1200	0	0	540	f	Zombie version of Warrior Monk (Flamethrower)	\N	2018-04-14 20:55:48.328149	2018-04-14 20:55:48.328149
61	Fenrir	fenrir	850000	10000	73	40	12	165	999	200	0	10000	0	60	0	t	Unlocked at the Monster Arena by capturing 3 of each wolf species	\N	2018-04-14 20:55:48.330829	2018-04-14 20:55:48.330829
62	Flame Flan	flame-flan	1500	2250	1	180	20	1	220	6	15	480	0	0	448	f	\N	\N	2018-04-14 20:55:48.333562	2018-04-14 20:55:48.333562
63	Floating Death	floating-death	6700	10050	1	1	45	120	520	33	15	7100	18	0	1265	f	\N	\N	2018-04-14 20:55:48.336207	2018-04-14 20:55:48.336207
64	Floating Eye	floating-eye	140	210	1	1	18	120	200	10	15	21	11	0	44	f	\N	\N	2018-04-14 20:55:48.339064	2018-04-14 20:55:48.339064
65	Funguar	funguar	540	810	1	1	26	1	60	4	15	44	0	0	42	f	\N	\N	2018-04-14 20:55:48.341992	2018-04-14 20:55:48.341992
66	Gandarewa	gandarewa	148	220	1	1	23	120	160	9	15	32	12	0	62	f	\N	\N	2018-04-14 20:55:48.345177	2018-04-14 20:55:48.345177
67	Garm	garm	240	360	17	1	1	120	35	16	15	48	7	0	88	f	\N	\N	2018-04-14 20:55:48.348263	2018-04-14 20:55:48.348263
68	Garuda	garuda	1400	2100	13	1	10	1	50	7	15	8	0	0	30	f	\N	\N	2018-04-14 20:55:48.351285	2018-04-14 20:55:48.351285
69	Garuda (Luca)	garuda-luca	1800	500	18	1	12	1	10	10	15	28	0	20	0	f	\N	\N	2018-04-14 20:55:48.354005	2018-04-14 20:55:48.354005
70	Gemini (A)	gemini-a	36000	13560	33	50	1	30	1	21	0	7800	1	9	1111	f	\N	\N	2018-04-14 20:55:48.356425	2018-04-14 20:55:48.356425
71	Gemini (B)	gemini-b	36000	13560	33	50	1	30	1	21	0	7800	1	9	1111	f	\N	\N	2018-04-14 20:55:48.35904	2018-04-14 20:55:48.35904
72	Geneaux's Tentacle	geneaux-s-tentacle	450	500	14	1	1	1	10	10	10	5	0	20	30	t	Part of Sinspawn Geneaux	\N	2018-04-14 20:55:48.3617	2018-04-14 20:55:48.3617
73	Geosgaeno	geosgaeno	32767	32767	36	50	40	50	128	48	15	0	0	50	0	t	Can suck a character into its chest. It gains any statuses that that character had on when they were swallowed.	\N	2018-04-14 20:55:48.364396	2018-04-14 20:55:48.364396
74	Ghost	ghost	9999	4060	1	120	33	1	350	14	15	1450	0	0	810	f	Casts Doom as first attack, has a random weakness to one element	Doom	2018-04-14 20:55:48.367332	2018-04-14 20:55:48.367332
75	Gold Element	gold-element	1200	1800	1	120	32	1	180	7	15	92	0	0	107	f	\N	\N	2018-04-14 20:55:48.370069	2018-04-14 20:55:48.370069
76	Grat	grat	4000	6000	28	50	1	50	25	12	15	980	0	0	520	f	\N	Seed Cannon	2018-04-14 20:55:48.373041	2018-04-14 20:55:48.373041
77	Great Malboro	great-malboro	64000	13560	36	1	42	1	1	18	15	21000	0	0	1900	f	\N	Bad Breath	2018-04-14 20:55:48.37607	2018-04-14 20:55:48.37607
78	Greater Sphere	greater-sphere	1500000	99999	87	130	102	120	999	55	0	50000	0	0	0	t	Unlocked at the Monster Arena by conquering 2 species	\N	2018-04-14 20:55:48.379235	2018-04-14 20:55:48.379235
79	Grenade	grenade	7500	5384	26	1	24	150	63	17	15	1350	0	0	540	f	Self-destructs after being hit 3 times	Self-Destruct	2018-04-14 20:55:48.383506	2018-04-14 20:55:48.383506
80	Grendel	grendel	9500	6972	41	50	23	1	62	31	15	2600	0	0	730	f	\N	Fire Breath	2018-04-14 20:55:48.386194	2018-04-14 20:55:48.386194
81	Grothia	grothia	8000	2550	23	10	21	1	600	18	15	0	0	0	0	t	Issaru's version of Ifrit	\N	2018-04-14 20:55:48.389007	2018-04-14 20:55:48.389007
82	Guado Guardian	guado-guardian	1200	1432	1	1	14	1	330	20	15	290	0	0	480	f	\N	\N	2018-04-14 20:55:48.391659	2018-04-14 20:55:48.391659
83	Guado Guardian (II)	guado-guardian-ii	2000	2000	10	1	15	1	10	12	15	290	0	100	300	f	\N	\N	2018-04-14 20:55:48.394109	2018-04-14 20:55:48.394109
84	Guado Guardian (III)	guado-guardian-iii	2600	1432	1	1	24	1	600	20	15	540	0	0	380	f	\N	\N	2018-04-14 20:55:48.396697	2018-04-14 20:55:48.396697
85	Halma	halma	13000	13560	46	1	1	120	1	23	15	5300	0	0	1030	f	\N	\N	2018-04-14 20:55:48.400383	2018-04-14 20:55:48.400383
86	Head	head	4000	1000	1	1	1	1	200	15	0	48	0	0	200	t	\N	\N	2018-04-14 20:55:48.406064	2018-04-14 20:55:48.406064
87	Hornet	hornet	620000	50000	63	70	88	95	999	102	0	10000	0	17	0	t	Unlocked at the Monster Arena by capturing 4 of each insect species	\N	2018-04-14 20:55:48.411192	2018-04-14 20:55:48.411192
88	Ice Flan	ice-flan	1350	2025	1	120	21	1	160	9	15	300	0	0	188	f	\N	\N	2018-04-14 20:55:48.415653	2018-04-14 20:55:48.415653
89	Iguion	iguion	370	555	23	1	1	120	70	19	15	240	8	0	138	f	\N	\N	2018-04-14 20:55:48.419301	2018-04-14 20:55:48.419301
90	Imp	imp	880	1320	1	1	25	180	300	24	15	770	16	0	610	f	Flying	\N	2018-04-14 20:55:48.422203	2018-04-14 20:55:48.422203
91	Ipiria	ipiria	180	270	15	1	1	120	35	13	15	24	7	0	46	f	\N	\N	2018-04-14 20:55:48.424836	2018-04-14 20:55:48.424836
92	Iron Giant	iron-giant	3600	924	30	1	1	1	1	7	15	800	0	0	600	f	\N	\N	2018-04-14 20:55:48.427445	2018-04-14 20:55:48.427445
93	Ironclad	ironclad	2000000	99999	100	220	0	180	999	65	0	10000	0	0	0	t	Unlocked at the Monster Arena by capturing 10 of each iron giant species	\N	2018-04-14 20:55:48.430221	2018-04-14 20:55:48.430221
94	Jormungand	jormungand	520000	10000	77	33	80	186	999	53	0	8000	0	6	0	t	Unlocked at the Monster Arena by capturing 1 of each fiend from Djose Highroad	\N	2018-04-14 20:55:48.432818	2018-04-14 20:55:48.432818
95	Juggernault	juggernault	1200000	15000	98	140	70	62	999	42	0	8000	0	0	0	t	Unlocked at the Monster Arena by capturing 5 of each horned species	\N	2018-04-14 20:55:48.435476	2018-04-14 20:55:48.435476
96	Jumbo Flan	jumbo-flan	1300000	99999	3	255	98	80	999	60	0	10000	0	0	0	t	Unlocked at the Monster Arena by capturing 3 of each flan species	\N	2018-04-14 20:55:48.438517	2018-04-14 20:55:48.438517
97	Killer Bee	killer-bee	110	165	8	1	1	1	5	8	15	9	10	0	23	f	\N	\N	2018-04-14 20:55:48.441493	2018-04-14 20:55:48.441493
148	Pterya	pterya	12000	2500	20	10	18	10	1000	21	15	0	0	0	0	t	Issaru's version of Valefor	\N	2018-04-14 20:55:48.596833	2018-04-14 20:55:48.596833
98	Kimahri	kimahri	750	300	10	15	8	5	10	15	15	3	0	10	100	t	Shortly after you fight him, he joins your party	\N	2018-04-14 20:55:48.444169	2018-04-14 20:55:48.444169
99	Klikk	klikk	1500	400	14	1	1	1	5	4	15	5	0	50	50	t	\N	\N	2018-04-14 20:55:48.446778	2018-04-14 20:55:48.446778
100	Kottos	kottos	440000	15000	88	60	12	1	999	36	0	8000	0	0	0	t	Unlocked at the Monster Arena by capturing 1 of each fiend from Mi'ihen Highroad	\N	2018-04-14 20:55:48.449476	2018-04-14 20:55:48.449476
101	Kusariqqu	kusariqqu	445	668	32	1	35	120	31	7	15	92	0	0	112	f	\N	\N	2018-04-14 20:55:48.452056	2018-04-14 20:55:48.452056
102	Lamashtu	lamashtu	275	413	23	1	20	120	21	6	15	32	0	0	72	f	\N	\N	2018-04-14 20:55:48.454789	2018-04-14 20:55:48.454789
103	Land Worm	land-worm	80000	13560	55	10	50	10	160	21	15	22000	0	0	2200	f	\N	\N	2018-04-14 20:55:48.457695	2018-04-14 20:55:48.457695
104	Larva	larva	1498	924	10	40	19	45	1000	10	15	262	0	0	330	f	\N	\N	2018-04-14 20:55:48.460216	2018-04-14 20:55:48.460216
105	Left Fin	left-fin	65000	10000	30	100	30	50	999	20	15	16000	0	0	10000	t	Sin's left fin	\N	2018-04-14 20:55:48.462865	2018-04-14 20:55:48.462865
106	Lord Ochu	lord-ochu	4649	800	15	1	23	1	39	8	20	40	0	10	420	t	\N	\N	2018-04-14 20:55:48.465679	2018-04-14 20:55:48.465679
107	Machea	machea	18000	13560	35	55	1	10	59	39	15	8300	0	0	1450	f	\N	\N	2018-04-14 20:55:48.468975	2018-04-14 20:55:48.468975
108	Maelspike	maelspike	10000	7500	40	50	33	1	35	14	15	600	0	0	330	f	\N	\N	2018-04-14 20:55:48.472518	2018-04-14 20:55:48.472518
109	Mafdet	mafdet	710	1065	29	1	1	120	25	9	15	300	0	0	172	f	\N	\N	2018-04-14 20:55:48.474909	2018-04-14 20:55:48.474909
110	Magic Urn	magic-urn	999999	9999999	1	255	40	255	9999	0	0	0	0	0	0	f	\N	\N	2018-04-14 20:55:48.477625	2018-04-14 20:55:48.477625
111	Malboro	malboro	27000	4060	32	1	32	1	1	10	15	2200	0	0	1500	f	\N	Bad Breath	2018-04-14 20:55:48.480381	2018-04-14 20:55:48.480381
112	Malboro Menace	malboro-menace	640000	12000	60	24	53	63	999	34	0	8000	0	0	0	t	Unlocked at the Monster Arena by capturing 1 of each fiend from Kilika	\N	2018-04-14 20:55:48.483094	2018-04-14 20:55:48.483094
113	Mandragora	mandragora	31000	5384	34	12	40	15	120	13	15	6230	0	0	1200	f	\N	\N	2018-04-14 20:55:48.485904	2018-04-14 20:55:48.485904
114	Master Coeurl	master-coeurl	13000	13560	42	1	38	40	540	23	15	6500	0	0	2030	f	\N	\N	2018-04-14 20:55:48.488678	2018-04-14 20:55:48.488678
115	Master Tonberry	master-tonberry	48000	13560	47	10	52	1	1	18	15	20000	0	0	2400	f	\N	\N	2018-04-14 20:55:48.491344	2018-04-14 20:55:48.491344
116	Maze Larva	maze-larva	2222	2108	11	40	24	45	1111	15	0	1850	0	0	620	f	\N	\N	2018-04-14 20:55:48.494117	2018-04-14 20:55:48.494117
117	Mech Defender	mech-defender	8700	5384	40	15	1	10	1	7	15	950	0	0	880	f	Steal or Mug destroys it	\N	2018-04-14 20:55:48.497243	2018-04-14 20:55:48.497243
118	Mech Guard	mech-guard	1280	1432	25	1	1	1	1	10	15	310	0	0	600	f	Steal or Mug destroys it	\N	2018-04-14 20:55:48.500332	2018-04-14 20:55:48.500332
119	Mech Gunner	mech-gunner	2800	1432	31	10	1	10	1	6	15	540	0	0	800	f	Steal or Mug destroys it	\N	2018-04-14 20:55:48.503132	2018-04-14 20:55:48.503132
120	Mech Hunter	mech-hunter	5500	4060	36	10	1	10	1	8	15	820	0	0	673	f	Steal or Mug destroys it	\N	2018-04-14 20:55:48.505858	2018-04-14 20:55:48.505858
121	Mech Leader	mech-leader	3700	5550	31	5	28	19	1	15	0	830	0	0	530	f	Steal or Mug destroys it	\N	2018-04-14 20:55:48.508675	2018-04-14 20:55:48.508675
122	Mech Scouter	mech-scouter	2750	4125	30	1	1	1	1	14	15	480	0	0	384	f	Steal or Mug destroys it	\N	2018-04-14 20:55:48.511533	2018-04-14 20:55:48.511533
123	Melusine	melusine	265	405	20	1	1	120	65	17	15	92	8	0	108	f	\N	\N	2018-04-14 20:55:48.514185	2018-04-14 20:55:48.514185
124	Mi'ihen Fang	mi-ihen-fang	160	240	16	1	1	120	20	13	15	20	5	0	33	f	\N	\N	2018-04-14 20:55:48.51831	2018-04-14 20:55:48.51831
125	Mimic (A)	mimic-a	40000	13560	33	150	38	150	10	29	15	0	0	0	50000	f	Small version; gives you tons of gil	\N	2018-04-14 20:55:48.522521	2018-04-14 20:55:48.522521
126	Mimic (B)	mimic-b	40000	13560	42	1	58	255	10	22	15	0	0	0	50000	f	Medium version; gives you tons of gil	\N	2018-04-14 20:55:48.525232	2018-04-14 20:55:48.525232
127	Mimic (C)	mimic-c	60000	13560	54	1	26	1	10	25	15	0	0	0	50000	f	Large version; gives you tons of gil	\N	2018-04-14 20:55:48.528229	2018-04-14 20:55:48.528229
128	Mimic (D)	mimic-d	40000	13560	42	255	12	1	10	22	15	0	0	0	50000	f	Flying version; gives you tons of gil	\N	2018-04-14 20:55:48.531105	2018-04-14 20:55:48.531105
129	Mortibody	mortibody	4000	0	22	50	20	1	50	28	20	0	0	100	0	t	Part of Seymour Natus, cannot be killed since it draws HP from Seymour Natus whenever its HP is depleted	\N	2018-04-14 20:55:48.533828	2018-04-14 20:55:48.533828
130	Mortiorchis	mortiorchis	4000	0	40	100	40	1	512	38	15	0	0	100	0	t	Part of Seymour Flux, cannot be killed since it draws HP from Seymour Flux whenever its HP is depleted	\N	2018-04-14 20:55:48.53659	2018-04-14 20:55:48.53659
131	Murussu	murussu	580	870	25	1	1	120	20	7	15	240	0	0	165	f	\N	\N	2018-04-14 20:55:48.539332	2018-04-14 20:55:48.539332
132	Mushussu	mushussu	680	1020	36	1	42	120	38	9	15	310	0	0	270	f	\N	\N	2018-04-14 20:55:48.542066	2018-04-14 20:55:48.542066
133	Nebiros	nebiros	700	1050	22	1	1	1	65	22	15	480	16	0	320	f	\N	\N	2018-04-14 20:55:48.544825	2018-04-14 20:55:48.544825
134	Nega Elemental	nega-elemental	1300000	15000	0	140	80	62	999	44	0	10000	0	0	0	t	Unlocked at the Monster Arena by capturing 3 of each elemental species	\N	2018-04-14 20:55:48.547699	2018-04-14 20:55:48.547699
135	Negator	negator	1000	1000	1	1	1	1	1	0	0	220	0	0	300	t	Launched by Crawler, blocks all magic	\N	2018-04-14 20:55:48.558996	2018-04-14 20:55:48.558996
136	Nemesis	nemesis	10000000	99999	255	150	255	150	999	200	0	50000	0	0	0	t	Unlocked at the Monster Arena by capturing 10 of each fiend and defeating all other bosses at the Monster Arena	\N	2018-04-14 20:55:48.561966	2018-04-14 20:55:48.561966
137	Neslug	neslug	4000000	12000	130	80	130	80	999	43	0	50000	0	0	0	t	Unlocked at the Monster Arena by conquering all areas	\N	2018-04-14 20:55:48.564665	2018-04-14 20:55:48.564665
138	Nidhogg	nidhogg	2000	3000	43	1	50	180	46	10	15	810	0	0	602	f	\N	\N	2018-04-14 20:55:48.568116	2018-04-14 20:55:48.568116
139	Oblitzerator	oblitzerator	6000	600	16	1	1	1	10	1	0	36	0	10	580	t	Use the trigger command to shorten the battle considerably	\N	2018-04-14 20:55:48.570829	2018-04-14 20:55:48.570829
140	Ochu	ochu	7200	924	22	1	14	1	35	6	15	180	0	0	520	f	\N	\N	2018-04-14 20:55:48.573626	2018-04-14 20:55:48.573626
141	Octopus	octopus	4500	2108	27	1	5	1	1	10	15	700	0	0	220	f	\N	\N	2018-04-14 20:55:48.57611	2018-04-14 20:55:48.57611
142	Ogre	ogre	9400	4060	28	1	1	1	3	17	15	1080	0	0	980	f	\N	\N	2018-04-14 20:55:48.57873	2018-04-14 20:55:48.57873
143	Omega Weapon	omega-weapon	99999	13560	54	80	50	20	1	32	15	50000	0	0	30000	t	\N	Nova	2018-04-14 20:55:48.581585	2018-04-14 20:55:48.581585
144	One-Eye	one-eye	150000	15000	55	58	77	183	999	38	0	10000	0	10	0	t	Unlocked at the Monster Arena by capturing 4 of each flying eyeball species	\N	2018-04-14 20:55:48.584381	2018-04-14 20:55:48.584381
145	Ornitholestes	ornitholestes	800000	99999	83	55	30	170	999	130	0	10000	0	80	0	t	Unlocked at the Monster Arena by capturing 3 of each lizard species	\N	2018-04-14 20:55:48.587577	2018-04-14 20:55:48.587577
146	Phlegyas	phlegyas	1680	2108	26	1	33	20	50	12	15	650	0	0	410	f	\N	\N	2018-04-14 20:55:48.590741	2018-04-14 20:55:48.590741
147	Piranha	piranha	50	225	6	1	1	1	1	12	15	1	0	0	2	f	\N	\N	2018-04-14 20:55:48.593755	2018-04-14 20:55:48.593755
149	Pteryx	pteryx	100000	99999	90	100	5	100	999	60	0	10000	0	60	0	t	Unlocked at the Monster Arena by capturing 4 of each bird species	\N	2018-04-14 20:55:48.601064	2018-04-14 20:55:48.601064
150	Puraboros	puraboros	20000	13560	36	40	25	1	180	28	15	3200	0	0	970	f	\N	Self-Destruct	2018-04-14 20:55:48.603945	2018-04-14 20:55:48.603945
151	Qactuar	qactuar	500	750	19	1	1	255	1	15	15	350	17	0	1500	f	\N	\N	2018-04-14 20:55:48.606491	2018-04-14 20:55:48.606491
152	Ragora	ragora	780	1170	18	1	1	1	15	8	15	20	0	0	48	f	\N	Seed Cannon	2018-04-14 20:55:48.609027	2018-04-14 20:55:48.609027
153	Raldo	raldo	240	360	19	1	1	120	10	5	15	20	0	0	42	f	\N	\N	2018-04-14 20:55:48.611528	2018-04-14 20:55:48.611528
154	Raptor	raptor	200	300	18	1	1	120	45	14	15	32	7	0	48	f	\N	\N	2018-04-14 20:55:48.614232	2018-04-14 20:55:48.614232
155	Red Element	red-element	450	675	1	120	23	1	130	6	15	32	0	0	55	f	\N	\N	2018-04-14 20:55:48.617094	2018-04-14 20:55:48.617094
156	Remora	remora	3000	2108	30	30	25	1	22	11	15	830	0	0	535	f	\N	\N	2018-04-14 20:55:48.620148	2018-04-14 20:55:48.620148
157	Right Fin	right-fin	65000	10000	30	100	30	50	999	20	15	17000	0	0	10000	t	Sin's right fin	\N	2018-04-14 20:55:48.624417	2018-04-14 20:55:48.624417
158	Sahagin	sahagin	100	200	3	1	1	1	5	5	0	0	0	25	0	f	\N	\N	2018-04-14 20:55:48.627117	2018-04-14 20:55:48.627117
159	Sahagin (II)	sahagin-ii	1380	2070	28	15	24	1	20	15	15	200	0	0	180	f	\N	\N	2018-04-14 20:55:48.629625	2018-04-14 20:55:48.629625
160	Sahagin (III)	sahagin-iii	380	570	13	1	1	1	20	18	15	560	0	0	200	f	\N	\N	2018-04-14 20:55:48.632174	2018-04-14 20:55:48.632174
161	Sahagin Chief	sahagin-chief	170	340	12	1	1	1	5	8	0	2	0	25	20	f	\N	\N	2018-04-14 20:55:48.635072	2018-04-14 20:55:48.635072
162	Sanctuary Keeper	sanctuary-keeper	40000	6400	37	100	40	100	256	32	15	11000	0	50	6500	t	\N	\N	2018-04-14 20:55:48.637865	2018-04-14 20:55:48.637865
163	Sand Wolf	sand-wolf	450	675	23	1	1	120	55	23	15	310	9	0	225	f	\N	\N	2018-04-14 20:55:48.640649	2018-04-14 20:55:48.640649
164	Sandragora	sandragora	12750	1432	25	1	1	1	3	12	15	540	0	0	336	f	\N	Seed Cannon	2018-04-14 20:55:48.643492	2018-04-14 20:55:48.643492
165	Sandworm	sandworm	45000	1432	30	5	28	5	100	8	15	2000	0	0	1000	f	\N	\N	2018-04-14 20:55:48.646122	2018-04-14 20:55:48.646122
166	Seymour	seymour	6000	1400	20	1	25	25	100	20	15	2000	0	100	5000	t	This is your first battle against him	\N	2018-04-14 20:55:48.648762	2018-04-14 20:55:48.648762
167	Seymour Flux	seymour-flux	70000	3500	30	40	15	40	512	38	15	10000	0	100	6000	t	This is your 3rd battle against him	\N	2018-04-14 20:55:48.652186	2018-04-14 20:55:48.652186
168	Seymour Natus	seymour-natus	36000	3500	30	1	25	1	200	2	15	6300	0	100	3500	t	This is your 2nd battle against him, he also has Mortibody in this battle	\N	2018-04-14 20:55:48.655062	2018-04-14 20:55:48.655062
169	Seymour Omnis	seymour-omnis	80000	15000	20	180	35	100	999	40	20	24000	0	0	12000	t	This is your 4th battle against him	\N	2018-04-14 20:55:48.657578	2018-04-14 20:55:48.657578
170	Shinryu	shinryu	2000000	99999	92	60	86	98	999	70	0	50000	0	0	0	t	Unlocked at the Monster Arena by capturing 2 of each underwater fiend in Gagazet	\N	2018-04-14 20:55:48.66026	2018-04-14 20:55:48.66026
171	Shred	shred	1950	2925	35	1	1	180	30	10	15	480	0	0	368	f	\N	\N	2018-04-14 20:55:48.662955	2018-04-14 20:55:48.662955
172	Simurgh	simurgh	200	300	13	1	1	1	27	17	15	48	0	0	73	f	\N	\N	2018-04-14 20:55:48.665405	2018-04-14 20:55:48.665405
173	Sin	sin	140000	10000	30	40	30	40	999	30	15	20000	0	0	12000	t	\N	\N	2018-04-14 20:55:48.669587	2018-04-14 20:55:48.669587
174	Sin (Core)	sin-core	36000	3000	1	100	30	100	999	20	15	18000	0	0	10000	t	Must destroy Sinspawn Genais to reach it	\N	2018-04-14 20:55:48.675105	2018-04-14 20:55:48.675105
175	Sin (Fin)	sin-fin	2000	1000	1	1	1	1	100	6	15	10	0	0	100	t	Does not attack, just keeps sending over Sinscales	\N	2018-04-14 20:55:48.68034	2018-04-14 20:55:48.68034
176	Sinscale	sinscale	100	500	5	1	1	1	0	8	0	0	0	0	0	f	\N	\N	2018-04-14 20:55:48.685119	2018-04-14 20:55:48.685119
177	Sinscale (II)	sinscale-ii	100	300	11	1	1	1	0	1	0	2	0	100	24	f	\N	\N	2018-04-14 20:55:48.688923	2018-04-14 20:55:48.688923
178	Sinscale (III)	sinscale-iii	200	400	13	1	1	1	0	12	0	2	0	0	22	f	\N	\N	2018-04-14 20:55:48.692235	2018-04-14 20:55:48.692235
179	Sinspawn Ammes	sinspawn-ammes	2400	1000	1	1	5	1	400	9	10	0	0	0	0	t	Its only attack is Demi so it can't kill you directly	\N	2018-04-14 20:55:48.69483	2018-04-14 20:55:48.69483
180	Sinspawn Echuilles	sinspawn-echuilles	2000	400	10	1	15	1	20	5	15	12	0	15	115	t	Accompanies by unlimited Sinscales	\N	2018-04-14 20:55:48.697588	2018-04-14 20:55:48.697588
181	Sinspawn Genais	sinspawn-genais	20000	2000	30	80	35	50	200	25	15	1800	0	0	10000	t	Must defeat it to reach Sin (Core)	\N	2018-04-14 20:55:48.700155	2018-04-14 20:55:48.700155
182	Sinspawn Geneaux	sinspawn-geneaux	3000	900	15	1	10	1	30	7	0	48	0	100	300	t	Watch out for Geneaux's Tentacles behind you	\N	2018-04-14 20:55:48.70293	2018-04-14 20:55:48.70293
183	Sinspawn Gui	sinspawn-gui	12000	800	29	1	20	30	30	10	15	400	0	100	1000	t	This is the torso which is protected by the Sinspawn Gui (Arm)s	\N	2018-04-14 20:55:48.705699	2018-04-14 20:55:48.705699
184	Sinspawn Gui (Arm)	sinspawn-gui-arm	800	500	1	1	1	1	1	0	0	37	0	0	300	t	The arms of Sinspawn Gui protect the torso from attack	\N	2018-04-14 20:55:48.708997	2018-04-14 20:55:48.708997
185	Skoll	skoll	1000	1500	1	120	19	1	60	7	15	480	0	0	420	f	\N	\N	2018-04-14 20:55:48.711816	2018-04-14 20:55:48.711816
186	Sleep Sprout	sleep-sprout	98000	10000	3	167	112	203	999	26	0	10000	0	0	0	t	Unlocked at the Monster Arena by capturing 4 of each fungus species	\N	2018-04-14 20:55:48.714375	2018-04-14 20:55:48.714375
187	Snow Flan	snow-flan	600	900	1	1	1	8	120	0	0	48	0	100	93	f	\N	\N	2018-04-14 20:55:48.716923	2018-04-14 20:55:48.716923
188	Snow Wolf	snow-wolf	400	600	20	1	1	120	50	20	15	300	8	0	192	f	\N	\N	2018-04-14 20:55:48.719858	2018-04-14 20:55:48.719858
189	Spathi	spathi	20000	2550	31	1	38	1	1500	20	15	0	0	0	0	t	Issaru's version of Bahamut	\N	2018-04-14 20:55:48.722637	2018-04-14 20:55:48.722637
190	Spectral Keeper	spectral-keeper	52000	8000	36	100	1	100	500	36	15	12000	0	8	7000	t	Use the trigger command to move your characters before the mines go off and to avoid its attacks, attack from behind it whenever possible	\N	2018-04-14 20:55:48.72527	2018-04-14 20:55:48.72527
191	Spherimorph	spherimorph	12000	2000	20	100	20	1	100	15	15	3420	0	30	4000	t	Elemental weakness changes whenever it is hit with magic, hit it with the opposite of whatever it uses	\N	2018-04-14 20:55:48.728095	2018-04-14 20:55:48.728095
192	Spirit	spirit	10000	13560	1	60	42	1	700	24	15	4300	0	0	1300	f	\N	White Wind	2018-04-14 20:55:48.730698	2018-04-14 20:55:48.730698
193	Splasher	splasher	200	900	14	1	1	1	2	20	15	140	10	0	100	f	\N	\N	2018-04-14 20:55:48.733907	2018-04-14 20:55:48.733907
194	Stratavis	stratavis	320000	10000	73	41	32	82	999	32	0	8000	0	5	0	t	Unlocked at the Monster Arena by capturing 1 of each fiend from Besaid	\N	2018-04-14 20:55:48.736844	2018-04-14 20:55:48.736844
195	Swamp Mafdet	swamp-mafdet	850	1275	30	1	1	120	1	6	15	0	0	0	0	f	\N	\N	2018-04-14 20:55:48.739747	2018-04-14 20:55:48.739747
196	Tanket	tanket	900000	10000	103	100	3	250	999	41	0	10000	0	0	0	t	Unlocked at the Monster Arena by capturing 3 of each mole species	\N	2018-04-14 20:55:48.742287	2018-04-14 20:55:48.742287
197	Th'uban	th-uban	3000000	99999	102	80	212	80	999	53	0	50000	0	0	0	t	Unlocked at the Monster Arena by conquering 6 species	\N	2018-04-14 20:55:48.745081	2018-04-14 20:55:48.745081
198	Thorn	thorn	4080	4060	1	1	25	1	120	8	15	830	0	0	530	f	Counters with Pollen (Sleep and Darkness)	\N	2018-04-14 20:55:48.748072	2018-04-14 20:55:48.748072
199	Thunder Flan	thunder-flan	450	675	1	120	17	1	50	6	15	24	0	0	50	f	\N	\N	2018-04-14 20:55:48.751069	2018-04-14 20:55:48.751069
200	Tonberry	tonberry	13500	4060	40	10	43	10	1	14	15	6500	0	0	2000	f	\N	\N	2018-04-14 20:55:48.754004	2018-04-14 20:55:48.754004
201	Tros	tros	2200	600	10	1	1	1	10	12	15	8	0	20	100	t	\N	\N	2018-04-14 20:55:48.756459	2018-04-14 20:55:48.756459
202	Ultima Buster	ultima-buster	5000000	99999	168	60	178	71	999	72	0	50000	0	0	0	t	Unlocked at the Monster Arena by capturing 5 of each fiend	\N	2018-04-14 20:55:48.759272	2018-04-14 20:55:48.759272
203	Ultima Weapon	ultima-weapon	70000	13560	50	60	45	60	1	28	15	40000	0	0	20000	t	\N	\N	2018-04-14 20:55:48.762091	2018-04-14 20:55:48.762091
204	Valaha	valaha	8700	4060	36	1	21	1	29	23	15	1320	0	0	720	f	\N	Fire Breath	2018-04-14 20:55:48.764979	2018-04-14 20:55:48.764979
205	Varuna	varuna	56000	11036	13	50	38	10	1	26	15	19500	0	0	1780	f	\N	\N	2018-04-14 20:55:48.768205	2018-04-14 20:55:48.768205
206	Vidatu	vidatu	95000	10000	12	230	77	230	999	33	0	10000	0	80	0	t	Unlocked at the Monster Arena by capturing 4 of each wizard species	\N	2018-04-14 20:55:48.77125	2018-04-14 20:55:48.77125
207	Vorban	vorban	630000	10000	95	100	75	100	999	33	0	8000	0	0	0	t	Unlocked at the Monster Arena by capturing 1 of each fiend from Omega Ruins	\N	2018-04-14 20:55:48.774027	2018-04-14 20:55:48.774027
208	Vouivre	vouivre	255	500	20	1	1	1	1	3	0	14	0	25	50	f	\N	\N	2018-04-14 20:55:48.776547	2018-04-14 20:55:48.776547
209	Warrior Monk	warrior-monk	1400	2100	20	1	20	1	20	16	15	420	0	0	460	f	\N	\N	2018-04-14 20:55:48.779257	2018-04-14 20:55:48.779257
210	Warrior Monk (Flamethrower)	warrior-monk-flamethrower	1400	2100	20	1	20	1	20	17	15	420	0	0	460	f	Uses a flamethrower attack that does Fire damage to entire party	\N	2018-04-14 20:55:48.782043	2018-04-14 20:55:48.782043
211	Wasp	wasp	360	540	17	1	1	1	30	18	15	240	13	0	142	f	\N	\N	2018-04-14 20:55:48.785002	2018-04-14 20:55:48.785002
212	Water Flan	water-flan	315	473	3	120	15	1	30	5	15	2	0	0	18	f	\N	\N	2018-04-14 20:55:48.787962	2018-04-14 20:55:48.787962
213	Wendigo	wendigo	18000	1432	40	1	1	1	32	18	15	2000	0	0	3000	t	\N	\N	2018-04-14 20:55:48.795605	2018-04-14 20:55:48.795605
214	White Element	white-element	390	585	1	120	22	1	120	5	15	20	0	0	48	f	\N	\N	2018-04-14 20:55:48.798799	2018-04-14 20:55:48.798799
215	Worker	worker	300	600	12	100	1	1	1	10	15	7	0	15	85	f	\N	\N	2018-04-14 20:55:48.80161	2018-04-14 20:55:48.80161
216	Wraith	wraith	22222	13560	1	150	24	30	3500	25	15	3100	0	0	1070	f	\N	Doom	2018-04-14 20:55:48.804251	2018-04-14 20:55:48.804251
217	Xiphos	xiphos	2700	1432	20	1	1	1	5	17	15	520	0	0	220	f	\N	\N	2018-04-14 20:55:48.806944	2018-04-14 20:55:48.806944
218	YAT-97	yat-97	3700	5550	43	1	38	120	1	12	15	3200	0	0	1080	f	\N	\N	2018-04-14 20:55:48.809671	2018-04-14 20:55:48.809671
219	YAT-99	yat-99	2700	2108	40	1	32	120	1	9	15	1870	0	0	1300	f	\N	\N	2018-04-14 20:55:48.812249	2018-04-14 20:55:48.812249
220	YKT-11	ykt-11	6200	8848	34	1	1	60	1	25	15	3200	0	0	1080	f	\N	Thrust Kick	2018-04-14 20:55:48.814942	2018-04-14 20:55:48.814942
221	YKT-63	ykt-63	4200	2108	30	1	1	60	1	22	15	1870	0	0	1300	f	\N	Thrust Kick	2018-04-14 20:55:48.817642	2018-04-14 20:55:48.817642
222	Yellow Element	yellow-element	300	450	1	120	18	1	100	5	15	9	0	0	33	f	\N	\N	2018-04-14 20:55:48.820672	2018-04-14 20:55:48.820672
223	Yenke Ronso	yenke-ronso	0	2500	2	30	3	50	200	4	15	4500	0	100	1500	t	Fights with Biran Ronso, attack him when they are not together on the same side	Aqua Breath, Fire Breath, Stone Breath, White Wind	2018-04-14 20:55:48.823737	2018-04-14 20:55:48.823737
224	Yojimbo	yojimbo	33000	4060	34	80	35	1	2000	32	15	0	0	0	0	t	\N	\N	2018-04-14 20:55:48.827028	2018-04-14 20:55:48.827028
225	Yowie	yowie	900	1350	26	1	1	180	95	29	15	810	10	0	480	f	Attacks have various negative status effects	\N	2018-04-14 20:55:48.829804	2018-04-14 20:55:48.829804
226	Yu Yevon	yu-yevon	99999	0	0	0	0	0	0	0	0	0	0	0	0	t	All characters have permanent Auto-Life for this battle so there's no way to lose	\N	2018-04-14 20:55:48.832378	2018-04-14 20:55:48.832378
227	Yunalesca	yunalesca	24000	0	20	50	30	50	500	40	20	0	0	0	0	t	Her 1st form, see also Yunalesca (2nd) and Yunalesca (3rd)	\N	2018-04-14 20:55:48.835739	2018-04-14 20:55:48.835739
228	Yunalesca (2nd)	yunalesca-2nd	48000	0	20	50	30	50	500	40	20	0	0	0	0	t	Her 2nd form, see also Yunalesca and Yunalesca (3rd)	\N	2018-04-14 20:55:48.838493	2018-04-14 20:55:48.838493
229	Yunalesca (3rd)	yunalesca-3rd	60000	0	20	50	30	50	500	40	20	14000	0	0	9000	t	Her 3rd form, see also Yunalesca and Yunalesca (2nd)	\N	2018-04-14 20:55:48.84131	2018-04-14 20:55:48.84131
230	Zaurus	zaurus	7850	11775	38	30	1	120	1	46	15	5000	14	0	950	f	\N	\N	2018-04-14 20:55:48.84398	2018-04-14 20:55:48.84398
231	Zu	zu	12000	12000	32	20	30	20	50	8	15	1200	0	0	1200	f	\N	\N	2018-04-14 20:55:48.846544	2018-04-14 20:55:48.846544
\.


--
-- Name: monsters_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Kieran
--

SELECT pg_catalog.setval('monsters_id_seq', 231, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: Kieran
--

COPY schema_migrations (version) FROM stdin;
20180414205255
20180414175644
20180414180253
20180414195032
20180414195110
20180414195205
20180414195250
20180414195504
20180414195633
20180414195901
20180414200008
20180414201526
20180414201652
20180414201736
\.


--
-- Data for Name: steal_drops; Type: TABLE DATA; Schema: public; Owner: Kieran
--

COPY steal_drops (id, monster_id, item_id, amount, rare, created_at, updated_at) FROM stdin;
1	1	78	3	f	2018-04-14 20:55:51.602996	2018-04-14 20:55:51.602996
2	1	83	1	t	2018-04-14 20:55:51.61404	2018-04-14 20:55:51.61404
3	2	82	4	f	2018-04-14 20:55:51.618521	2018-04-14 20:55:51.618521
4	2	96	1	t	2018-04-14 20:55:51.622827	2018-04-14 20:55:51.622827
5	3	108	2	f	2018-04-14 20:55:51.62706	2018-04-14 20:55:51.62706
6	3	41	1	t	2018-04-14 20:55:51.630932	2018-04-14 20:55:51.630932
7	4	42	1	f	2018-04-14 20:55:51.634927	2018-04-14 20:55:51.634927
8	4	96	1	t	2018-04-14 20:55:51.638698	2018-04-14 20:55:51.638698
9	5	26	1	f	2018-04-14 20:55:51.642636	2018-04-14 20:55:51.642636
10	5	50	1	t	2018-04-14 20:55:51.646485	2018-04-14 20:55:51.646485
11	6	70	2	f	2018-04-14 20:55:51.650401	2018-04-14 20:55:51.650401
12	6	70	3	t	2018-04-14 20:55:51.654282	2018-04-14 20:55:51.654282
13	7	89	1	f	2018-04-14 20:55:51.658942	2018-04-14 20:55:51.658942
14	7	89	2	t	2018-04-14 20:55:51.664127	2018-04-14 20:55:51.664127
15	8	72	1	f	2018-04-14 20:55:51.669291	2018-04-14 20:55:51.669291
16	8	72	2	t	2018-04-14 20:55:51.67669	2018-04-14 20:55:51.67669
17	9	85	3	f	2018-04-14 20:55:51.681652	2018-04-14 20:55:51.681652
18	9	31	1	t	2018-04-14 20:55:51.686797	2018-04-14 20:55:51.686797
19	10	34	2	f	2018-04-14 20:55:51.69141	2018-04-14 20:55:51.69141
20	10	23	2	t	2018-04-14 20:55:51.696242	2018-04-14 20:55:51.696242
21	11	24	2	f	2018-04-14 20:55:51.703466	2018-04-14 20:55:51.703466
22	12	98	1	f	2018-04-14 20:55:51.709398	2018-04-14 20:55:51.709398
23	12	11	1	t	2018-04-14 20:55:51.716638	2018-04-14 20:55:51.716638
24	13	91	1	f	2018-04-14 20:55:51.722178	2018-04-14 20:55:51.722178
25	13	91	2	t	2018-04-14 20:55:51.72724	2018-04-14 20:55:51.72724
26	14	72	1	f	2018-04-14 20:55:51.731146	2018-04-14 20:55:51.731146
27	15	43	1	f	2018-04-14 20:55:51.738747	2018-04-14 20:55:51.738747
28	15	85	1	t	2018-04-14 20:55:51.742793	2018-04-14 20:55:51.742793
29	16	28	1	f	2018-04-14 20:55:51.746557	2018-04-14 20:55:51.746557
30	16	62	1	t	2018-04-14 20:55:51.750371	2018-04-14 20:55:51.750371
31	17	41	1	f	2018-04-14 20:55:51.754476	2018-04-14 20:55:51.754476
32	17	105	2	t	2018-04-14 20:55:51.758519	2018-04-14 20:55:51.758519
33	18	55	1	f	2018-04-14 20:55:51.762341	2018-04-14 20:55:51.762341
34	18	55	2	t	2018-04-14 20:55:51.766171	2018-04-14 20:55:51.766171
35	19	8	1	f	2018-04-14 20:55:51.770242	2018-04-14 20:55:51.770242
36	19	74	1	t	2018-04-14 20:55:51.77402	2018-04-14 20:55:51.77402
37	20	43	1	f	2018-04-14 20:55:51.777753	2018-04-14 20:55:51.777753
38	20	83	4	t	2018-04-14 20:55:51.781522	2018-04-14 20:55:51.781522
39	21	34	2	f	2018-04-14 20:55:51.785272	2018-04-14 20:55:51.785272
40	21	34	3	t	2018-04-14 20:55:51.78957	2018-04-14 20:55:51.78957
41	22	14	2	f	2018-04-14 20:55:51.793481	2018-04-14 20:55:51.793481
42	22	14	3	t	2018-04-14 20:55:51.797447	2018-04-14 20:55:51.797447
43	23	33	4	f	2018-04-14 20:55:51.801148	2018-04-14 20:55:51.801148
44	23	83	1	t	2018-04-14 20:55:51.805106	2018-04-14 20:55:51.805106
45	24	104	1	f	2018-04-14 20:55:51.809233	2018-04-14 20:55:51.809233
46	25	43	1	f	2018-04-14 20:55:51.813035	2018-04-14 20:55:51.813035
47	25	70	1	t	2018-04-14 20:55:51.816783	2018-04-14 20:55:51.816783
48	26	43	1	f	2018-04-14 20:55:51.823104	2018-04-14 20:55:51.823104
49	26	46	1	t	2018-04-14 20:55:51.830869	2018-04-14 20:55:51.830869
50	27	16	1	f	2018-04-14 20:55:51.837258	2018-04-14 20:55:51.837258
51	27	17	1	t	2018-04-14 20:55:51.84275	2018-04-14 20:55:51.84275
52	28	17	2	f	2018-04-14 20:55:51.847205	2018-04-14 20:55:51.847205
53	28	21	1	t	2018-04-14 20:55:51.853975	2018-04-14 20:55:51.853975
54	29	38	1	f	2018-04-14 20:55:51.862044	2018-04-14 20:55:51.862044
55	29	54	1	t	2018-04-14 20:55:51.868269	2018-04-14 20:55:51.868269
56	30	41	3	f	2018-04-14 20:55:51.873819	2018-04-14 20:55:51.873819
57	30	97	1	t	2018-04-14 20:55:51.878139	2018-04-14 20:55:51.878139
58	31	90	1	f	2018-04-14 20:55:51.882818	2018-04-14 20:55:51.882818
59	31	72	1	t	2018-04-14 20:55:51.887548	2018-04-14 20:55:51.887548
60	32	83	1	f	2018-04-14 20:55:51.892163	2018-04-14 20:55:51.892163
61	32	2	2	t	2018-04-14 20:55:51.896361	2018-04-14 20:55:51.896361
62	33	47	1	f	2018-04-14 20:55:51.901688	2018-04-14 20:55:51.901688
63	33	49	2	t	2018-04-14 20:55:51.905951	2018-04-14 20:55:51.905951
64	34	61	2	f	2018-04-14 20:55:51.910095	2018-04-14 20:55:51.910095
65	34	95	1	t	2018-04-14 20:55:51.913903	2018-04-14 20:55:51.913903
66	35	75	1	f	2018-04-14 20:55:51.917841	2018-04-14 20:55:51.917841
67	36	61	1	f	2018-04-14 20:55:51.925034	2018-04-14 20:55:51.925034
68	37	32	2	f	2018-04-14 20:55:51.931462	2018-04-14 20:55:51.931462
69	37	11	1	t	2018-04-14 20:55:51.935966	2018-04-14 20:55:51.935966
70	38	73	1	f	2018-04-14 20:55:51.940204	2018-04-14 20:55:51.940204
71	38	89	1	t	2018-04-14 20:55:51.943884	2018-04-14 20:55:51.943884
72	39	52	1	f	2018-04-14 20:55:51.947783	2018-04-14 20:55:51.947783
73	39	52	2	t	2018-04-14 20:55:51.95178	2018-04-14 20:55:51.95178
74	40	84	1	f	2018-04-14 20:55:51.955745	2018-04-14 20:55:51.955745
75	40	84	2	t	2018-04-14 20:55:51.959558	2018-04-14 20:55:51.959558
76	41	98	1	f	2018-04-14 20:55:51.96327	2018-04-14 20:55:51.96327
77	41	98	2	t	2018-04-14 20:55:51.966936	2018-04-14 20:55:51.966936
78	42	52	1	f	2018-04-14 20:55:51.970793	2018-04-14 20:55:51.970793
79	42	52	2	t	2018-04-14 20:55:51.974675	2018-04-14 20:55:51.974675
80	43	52	4	f	2018-04-14 20:55:51.978284	2018-04-14 20:55:51.978284
81	44	52	2	f	2018-04-14 20:55:51.982206	2018-04-14 20:55:51.982206
82	45	72	2	f	2018-04-14 20:55:51.98597	2018-04-14 20:55:51.98597
83	46	75	1	f	2018-04-14 20:55:51.990039	2018-04-14 20:55:51.990039
84	46	88	1	t	2018-04-14 20:55:51.993871	2018-04-14 20:55:51.993871
85	47	90	1	f	2018-04-14 20:55:51.997446	2018-04-14 20:55:51.997446
86	47	72	1	t	2018-04-14 20:55:52.001268	2018-04-14 20:55:52.001268
87	48	15	2	f	2018-04-14 20:55:52.004996	2018-04-14 20:55:52.004996
88	48	21	1	t	2018-04-14 20:55:52.008878	2018-04-14 20:55:52.008878
89	49	75	1	f	2018-04-14 20:55:52.012473	2018-04-14 20:55:52.012473
90	49	43	1	t	2018-04-14 20:55:52.016105	2018-04-14 20:55:52.016105
91	50	38	1	f	2018-04-14 20:55:52.020115	2018-04-14 20:55:52.020115
92	50	53	1	t	2018-04-14 20:55:52.024075	2018-04-14 20:55:52.024075
93	51	43	1	f	2018-04-14 20:55:52.028562	2018-04-14 20:55:52.028562
94	51	43	2	t	2018-04-14 20:55:52.032476	2018-04-14 20:55:52.032476
95	52	31	4	f	2018-04-14 20:55:52.036447	2018-04-14 20:55:52.036447
96	52	32	1	t	2018-04-14 20:55:52.040444	2018-04-14 20:55:52.040444
97	53	70	1	f	2018-04-14 20:55:52.043924	2018-04-14 20:55:52.043924
98	53	70	2	t	2018-04-14 20:55:52.047654	2018-04-14 20:55:52.047654
99	54	108	1	f	2018-04-14 20:55:52.051382	2018-04-14 20:55:52.051382
100	54	108	2	t	2018-04-14 20:55:52.057426	2018-04-14 20:55:52.057426
101	55	108	2	f	2018-04-14 20:55:52.061174	2018-04-14 20:55:52.061174
102	55	95	1	t	2018-04-14 20:55:52.064921	2018-04-14 20:55:52.064921
103	56	85	1	f	2018-04-14 20:55:52.068667	2018-04-14 20:55:52.068667
104	56	28	1	t	2018-04-14 20:55:52.072509	2018-04-14 20:55:52.072509
105	57	75	1	f	2018-04-14 20:55:52.076372	2018-04-14 20:55:52.076372
106	58	39	2	f	2018-04-14 20:55:52.080042	2018-04-14 20:55:52.080042
107	58	95	2	t	2018-04-14 20:55:52.083634	2018-04-14 20:55:52.083634
108	59	15	2	f	2018-04-14 20:55:52.087789	2018-04-14 20:55:52.087789
109	59	78	1	t	2018-04-14 20:55:52.094538	2018-04-14 20:55:52.094538
110	60	15	2	f	2018-04-14 20:55:52.09908	2018-04-14 20:55:52.09908
111	60	78	1	t	2018-04-14 20:55:52.103051	2018-04-14 20:55:52.103051
112	61	16	2	f	2018-04-14 20:55:52.107925	2018-04-14 20:55:52.107925
113	61	17	1	t	2018-04-14 20:55:52.111504	2018-04-14 20:55:52.111504
114	62	33	1	f	2018-04-14 20:55:52.115203	2018-04-14 20:55:52.115203
115	62	33	2	t	2018-04-14 20:55:52.118947	2018-04-14 20:55:52.118947
116	63	70	4	f	2018-04-14 20:55:52.122738	2018-04-14 20:55:52.122738
117	63	70	5	t	2018-04-14 20:55:52.126481	2018-04-14 20:55:52.126481
118	64	25	1	f	2018-04-14 20:55:52.130152	2018-04-14 20:55:52.130152
119	64	70	1	t	2018-04-14 20:55:52.133758	2018-04-14 20:55:52.133758
120	65	85	1	f	2018-04-14 20:55:52.138236	2018-04-14 20:55:52.138236
121	65	28	1	t	2018-04-14 20:55:52.143327	2018-04-14 20:55:52.143327
122	66	26	1	f	2018-04-14 20:55:52.147047	2018-04-14 20:55:52.147047
123	66	26	2	t	2018-04-14 20:55:52.151045	2018-04-14 20:55:52.151045
124	67	43	1	f	2018-04-14 20:55:52.156017	2018-04-14 20:55:52.156017
125	67	88	1	t	2018-04-14 20:55:52.161027	2018-04-14 20:55:52.161027
126	70	48	1	f	2018-04-14 20:55:52.164771	2018-04-14 20:55:52.164771
127	70	48	2	t	2018-04-14 20:55:52.16836	2018-04-14 20:55:52.16836
128	71	48	1	f	2018-04-14 20:55:52.173142	2018-04-14 20:55:52.173142
129	71	48	2	t	2018-04-14 20:55:52.179978	2018-04-14 20:55:52.179978
130	74	73	1	f	2018-04-14 20:55:52.185697	2018-04-14 20:55:52.185697
131	74	31	1	t	2018-04-14 20:55:52.190438	2018-04-14 20:55:52.190438
132	75	26	1	f	2018-04-14 20:55:52.197111	2018-04-14 20:55:52.197111
133	75	26	2	t	2018-04-14 20:55:52.203191	2018-04-14 20:55:52.203191
134	76	8	4	f	2018-04-14 20:55:52.208399	2018-04-14 20:55:52.208399
135	76	79	3	t	2018-04-14 20:55:52.212049	2018-04-14 20:55:52.212049
136	77	79	1	f	2018-04-14 20:55:52.215791	2018-04-14 20:55:52.215791
137	77	63	1	t	2018-04-14 20:55:52.219877	2018-04-14 20:55:52.219877
138	78	38	1	f	2018-04-14 20:55:52.223814	2018-04-14 20:55:52.223814
139	78	81	1	t	2018-04-14 20:55:52.22766	2018-04-14 20:55:52.22766
140	79	33	2	f	2018-04-14 20:55:52.231238	2018-04-14 20:55:52.231238
141	79	33	3	t	2018-04-14 20:55:52.234862	2018-04-14 20:55:52.234862
142	80	43	1	f	2018-04-14 20:55:52.239146	2018-04-14 20:55:52.239146
143	80	67	1	t	2018-04-14 20:55:52.244191	2018-04-14 20:55:52.244191
144	82	43	1	f	2018-04-14 20:55:52.247791	2018-04-14 20:55:52.247791
145	82	112	1	t	2018-04-14 20:55:52.251268	2018-04-14 20:55:52.251268
146	83	43	1	f	2018-04-14 20:55:52.254989	2018-04-14 20:55:52.254989
147	83	28	1	t	2018-04-14 20:55:52.259342	2018-04-14 20:55:52.259342
148	84	43	1	f	2018-04-14 20:55:52.263014	2018-04-14 20:55:52.263014
149	84	112	2	t	2018-04-14 20:55:52.266771	2018-04-14 20:55:52.266771
150	85	46	3	f	2018-04-14 20:55:52.27038	2018-04-14 20:55:52.27038
151	85	82	2	t	2018-04-14 20:55:52.274173	2018-04-14 20:55:52.274173
152	86	75	1	f	2018-04-14 20:55:52.28058	2018-04-14 20:55:52.28058
153	87	74	4	f	2018-04-14 20:55:52.284917	2018-04-14 20:55:52.284917
154	87	78	2	t	2018-04-14 20:55:52.288696	2018-04-14 20:55:52.288696
155	88	9	1	f	2018-04-14 20:55:52.292533	2018-04-14 20:55:52.292533
156	88	9	2	t	2018-04-14 20:55:52.296045	2018-04-14 20:55:52.296045
157	89	90	1	f	2018-04-14 20:55:52.299784	2018-04-14 20:55:52.299784
158	89	72	1	t	2018-04-14 20:55:52.303376	2018-04-14 20:55:52.303376
159	90	49	1	f	2018-04-14 20:55:52.308438	2018-04-14 20:55:52.308438
160	90	49	2	t	2018-04-14 20:55:52.312306	2018-04-14 20:55:52.312306
161	91	90	1	f	2018-04-14 20:55:52.315942	2018-04-14 20:55:52.315942
162	91	72	1	t	2018-04-14 20:55:52.319503	2018-04-14 20:55:52.319503
163	92	48	1	f	2018-04-14 20:55:52.32325	2018-04-14 20:55:52.32325
164	93	48	4	f	2018-04-14 20:55:52.326986	2018-04-14 20:55:52.326986
165	93	96	1	t	2018-04-14 20:55:52.330553	2018-04-14 20:55:52.330553
166	94	72	4	f	2018-04-14 20:55:52.334327	2018-04-14 20:55:52.334327
167	94	103	1	t	2018-04-14 20:55:52.338577	2018-04-14 20:55:52.338577
168	95	52	4	f	2018-04-14 20:55:52.343608	2018-04-14 20:55:52.343608
169	95	83	1	t	2018-04-14 20:55:52.347763	2018-04-14 20:55:52.347763
170	96	52	4	f	2018-04-14 20:55:52.351734	2018-04-14 20:55:52.351734
171	96	62	1	t	2018-04-14 20:55:52.355646	2018-04-14 20:55:52.355646
172	97	8	1	f	2018-04-14 20:55:52.359908	2018-04-14 20:55:52.359908
173	97	74	1	t	2018-04-14 20:55:52.363805	2018-04-14 20:55:52.363805
174	99	40	1	f	2018-04-14 20:55:52.372619	2018-04-14 20:55:52.372619
175	99	40	2	t	2018-04-14 20:55:52.376776	2018-04-14 20:55:52.376776
176	100	95	4	f	2018-04-14 20:55:52.380558	2018-04-14 20:55:52.380558
177	100	91	2	t	2018-04-14 20:55:52.386837	2018-04-14 20:55:52.386837
178	101	43	1	f	2018-04-14 20:55:52.393885	2018-04-14 20:55:52.393885
179	101	86	1	t	2018-04-14 20:55:52.401215	2018-04-14 20:55:52.401215
180	102	43	1	f	2018-04-14 20:55:52.408367	2018-04-14 20:55:52.408367
181	102	86	1	t	2018-04-14 20:55:52.414735	2018-04-14 20:55:52.414735
182	103	95	1	f	2018-04-14 20:55:52.419445	2018-04-14 20:55:52.419445
183	104	52	1	f	2018-04-14 20:55:52.425291	2018-04-14 20:55:52.425291
184	104	52	2	t	2018-04-14 20:55:52.431869	2018-04-14 20:55:52.431869
185	105	67	1	f	2018-04-14 20:55:52.437555	2018-04-14 20:55:52.437555
186	105	100	1	t	2018-04-14 20:55:52.442539	2018-04-14 20:55:52.442539
187	106	75	1	f	2018-04-14 20:55:52.449461	2018-04-14 20:55:52.449461
188	107	43	2	f	2018-04-14 20:55:52.456312	2018-04-14 20:55:52.456312
189	107	97	1	t	2018-04-14 20:55:52.462102	2018-04-14 20:55:52.462102
190	108	108	2	f	2018-04-14 20:55:52.468975	2018-04-14 20:55:52.468975
191	108	108	3	t	2018-04-14 20:55:52.475158	2018-04-14 20:55:52.475158
192	109	43	1	f	2018-04-14 20:55:52.479824	2018-04-14 20:55:52.479824
193	109	46	1	t	2018-04-14 20:55:52.483866	2018-04-14 20:55:52.483866
194	111	79	1	f	2018-04-14 20:55:52.487754	2018-04-14 20:55:52.487754
195	112	79	4	f	2018-04-14 20:55:52.4915	2018-04-14 20:55:52.4915
196	112	61	2	t	2018-04-14 20:55:52.495218	2018-04-14 20:55:52.495218
197	113	79	2	f	2018-04-14 20:55:52.499052	2018-04-14 20:55:52.499052
198	114	31	2	f	2018-04-14 20:55:52.502622	2018-04-14 20:55:52.502622
199	114	31	4	t	2018-04-14 20:55:52.506318	2018-04-14 20:55:52.506318
200	115	61	1	f	2018-04-14 20:55:52.510381	2018-04-14 20:55:52.510381
201	115	102	1	t	2018-04-14 20:55:52.514027	2018-04-14 20:55:52.514027
202	116	34	2	f	2018-04-14 20:55:52.517692	2018-04-14 20:55:52.517692
203	116	23	2	t	2018-04-14 20:55:52.521362	2018-04-14 20:55:52.521362
204	117	5	2	f	2018-04-14 20:55:52.525133	2018-04-14 20:55:52.525133
205	118	40	2	f	2018-04-14 20:55:52.529155	2018-04-14 20:55:52.529155
206	118	40	3	t	2018-04-14 20:55:52.532795	2018-04-14 20:55:52.532795
207	119	5	1	f	2018-04-14 20:55:52.536705	2018-04-14 20:55:52.536705
208	119	5	2	t	2018-04-14 20:55:52.540758	2018-04-14 20:55:52.540758
209	120	5	1	f	2018-04-14 20:55:52.54457	2018-04-14 20:55:52.54457
210	120	5	2	t	2018-04-14 20:55:52.548322	2018-04-14 20:55:52.548322
211	121	40	1	f	2018-04-14 20:55:52.552099	2018-04-14 20:55:52.552099
212	121	36	2	t	2018-04-14 20:55:52.556286	2018-04-14 20:55:52.556286
213	122	40	3	f	2018-04-14 20:55:52.561164	2018-04-14 20:55:52.561164
214	122	40	4	t	2018-04-14 20:55:52.565083	2018-04-14 20:55:52.565083
215	123	90	1	f	2018-04-14 20:55:52.568966	2018-04-14 20:55:52.568966
216	123	72	1	t	2018-04-14 20:55:52.572794	2018-04-14 20:55:52.572794
217	124	75	1	f	2018-04-14 20:55:52.576661	2018-04-14 20:55:52.576661
218	124	88	1	t	2018-04-14 20:55:52.580467	2018-04-14 20:55:52.580467
219	131	43	1	f	2018-04-14 20:55:52.584035	2018-04-14 20:55:52.584035
220	131	46	1	t	2018-04-14 20:55:52.587878	2018-04-14 20:55:52.587878
221	132	43	1	f	2018-04-14 20:55:52.593636	2018-04-14 20:55:52.593636
222	132	39	1	t	2018-04-14 20:55:52.600672	2018-04-14 20:55:52.600672
223	133	74	1	f	2018-04-14 20:55:52.606479	2018-04-14 20:55:52.606479
224	133	74	2	t	2018-04-14 20:55:52.6111	2018-04-14 20:55:52.6111
225	134	98	4	f	2018-04-14 20:55:52.61487	2018-04-14 20:55:52.61487
226	134	105	1	t	2018-04-14 20:55:52.618795	2018-04-14 20:55:52.618795
227	135	43	1	f	2018-04-14 20:55:52.622669	2018-04-14 20:55:52.622669
228	136	56	1	f	2018-04-14 20:55:52.626292	2018-04-14 20:55:52.626292
229	136	107	1	t	2018-04-14 20:55:52.629947	2018-04-14 20:55:52.629947
230	137	38	1	f	2018-04-14 20:55:52.633642	2018-04-14 20:55:52.633642
231	137	37	1	t	2018-04-14 20:55:52.637292	2018-04-14 20:55:52.637292
232	138	43	1	f	2018-04-14 20:55:52.641299	2018-04-14 20:55:52.641299
233	138	39	1	t	2018-04-14 20:55:52.645241	2018-04-14 20:55:52.645241
234	140	79	1	f	2018-04-14 20:55:52.652007	2018-04-14 20:55:52.652007
235	140	79	2	t	2018-04-14 20:55:52.659412	2018-04-14 20:55:52.659412
236	141	23	2	f	2018-04-14 20:55:52.665885	2018-04-14 20:55:52.665885
237	141	108	2	t	2018-04-14 20:55:52.670796	2018-04-14 20:55:52.670796
238	142	95	1	f	2018-04-14 20:55:52.67466	2018-04-14 20:55:52.67466
239	142	95	2	t	2018-04-14 20:55:52.678449	2018-04-14 20:55:52.678449
240	143	38	50	f	2018-04-14 20:55:52.68221	2018-04-14 20:55:52.68221
241	144	52	3	f	2018-04-14 20:55:52.6861	2018-04-14 20:55:52.6861
242	144	11	1	t	2018-04-14 20:55:52.689974	2018-04-14 20:55:52.689974
243	145	80	1	f	2018-04-14 20:55:52.69603	2018-04-14 20:55:52.69603
244	145	17	1	t	2018-04-14 20:55:52.702501	2018-04-14 20:55:52.702501
245	146	23	2	f	2018-04-14 20:55:52.707306	2018-04-14 20:55:52.707306
246	146	108	1	t	2018-04-14 20:55:52.711363	2018-04-14 20:55:52.711363
247	147	40	1	f	2018-04-14 20:55:52.714945	2018-04-14 20:55:52.714945
248	147	40	2	t	2018-04-14 20:55:52.718948	2018-04-14 20:55:52.718948
249	149	89	4	f	2018-04-14 20:55:52.72344	2018-04-14 20:55:52.72344
250	149	15	1	t	2018-04-14 20:55:52.72807	2018-04-14 20:55:52.72807
251	150	33	3	f	2018-04-14 20:55:52.732522	2018-04-14 20:55:52.732522
252	150	33	4	t	2018-04-14 20:55:52.737412	2018-04-14 20:55:52.737412
253	151	16	1	f	2018-04-14 20:55:52.750935	2018-04-14 20:55:52.750935
254	152	8	1	f	2018-04-14 20:55:52.763608	2018-04-14 20:55:52.763608
255	152	79	1	t	2018-04-14 20:55:52.769213	2018-04-14 20:55:52.769213
256	153	75	1	f	2018-04-14 20:55:52.773239	2018-04-14 20:55:52.773239
257	153	46	1	t	2018-04-14 20:55:52.777495	2018-04-14 20:55:52.777495
258	154	90	1	f	2018-04-14 20:55:52.781397	2018-04-14 20:55:52.781397
259	154	72	1	t	2018-04-14 20:55:52.784904	2018-04-14 20:55:52.784904
260	155	14	1	f	2018-04-14 20:55:52.788769	2018-04-14 20:55:52.788769
261	155	14	2	t	2018-04-14 20:55:52.794744	2018-04-14 20:55:52.794744
262	156	23	1	f	2018-04-14 20:55:52.801764	2018-04-14 20:55:52.801764
263	156	23	2	t	2018-04-14 20:55:52.807476	2018-04-14 20:55:52.807476
264	157	112	1	f	2018-04-14 20:55:52.814487	2018-04-14 20:55:52.814487
265	157	83	1	t	2018-04-14 20:55:52.819046	2018-04-14 20:55:52.819046
266	159	34	2	f	2018-04-14 20:55:52.822985	2018-04-14 20:55:52.822985
267	159	23	2	t	2018-04-14 20:55:52.82736	2018-04-14 20:55:52.82736
268	160	34	2	f	2018-04-14 20:55:52.834066	2018-04-14 20:55:52.834066
269	160	23	1	t	2018-04-14 20:55:52.84037	2018-04-14 20:55:52.84037
270	162	104	1	f	2018-04-14 20:55:52.84565	2018-04-14 20:55:52.84565
271	162	104	2	t	2018-04-14 20:55:52.850357	2018-04-14 20:55:52.850357
272	163	88	2	f	2018-04-14 20:55:52.857641	2018-04-14 20:55:52.857641
273	164	79	2	f	2018-04-14 20:55:52.866043	2018-04-14 20:55:52.866043
274	164	70	10	t	2018-04-14 20:55:52.869752	2018-04-14 20:55:52.869752
275	165	82	2	f	2018-04-14 20:55:52.873668	2018-04-14 20:55:52.873668
276	165	95	2	t	2018-04-14 20:55:52.880532	2018-04-14 20:55:52.880532
277	166	104	1	f	2018-04-14 20:55:52.885011	2018-04-14 20:55:52.885011
278	168	102	2	f	2018-04-14 20:55:52.888837	2018-04-14 20:55:52.888837
279	168	102	3	t	2018-04-14 20:55:52.892661	2018-04-14 20:55:52.892661
280	169	83	1	f	2018-04-14 20:55:52.896402	2018-04-14 20:55:52.896402
281	169	100	1	t	2018-04-14 20:55:52.900097	2018-04-14 20:55:52.900097
282	170	38	1	f	2018-04-14 20:55:52.90386	2018-04-14 20:55:52.90386
283	170	103	1	t	2018-04-14 20:55:52.909498	2018-04-14 20:55:52.909498
284	171	46	1	f	2018-04-14 20:55:52.914481	2018-04-14 20:55:52.914481
285	171	46	2	t	2018-04-14 20:55:52.918262	2018-04-14 20:55:52.918262
286	172	89	1	f	2018-04-14 20:55:52.921867	2018-04-14 20:55:52.921867
287	172	89	2	t	2018-04-14 20:55:52.925596	2018-04-14 20:55:52.925596
288	173	28	1	f	2018-04-14 20:55:52.92969	2018-04-14 20:55:52.92969
289	173	100	1	t	2018-04-14 20:55:52.933384	2018-04-14 20:55:52.933384
290	174	95	3	f	2018-04-14 20:55:52.937208	2018-04-14 20:55:52.937208
291	174	95	4	t	2018-04-14 20:55:52.940958	2018-04-14 20:55:52.940958
292	181	98	1	f	2018-04-14 20:55:52.944946	2018-04-14 20:55:52.944946
293	181	83	1	t	2018-04-14 20:55:52.948753	2018-04-14 20:55:52.948753
294	183	75	1	f	2018-04-14 20:55:52.952841	2018-04-14 20:55:52.952841
295	184	75	1	f	2018-04-14 20:55:52.956729	2018-04-14 20:55:52.956729
296	185	24	1	f	2018-04-14 20:55:52.960478	2018-04-14 20:55:52.960478
297	185	24	2	t	2018-04-14 20:55:52.964503	2018-04-14 20:55:52.964503
298	186	74	4	f	2018-04-14 20:55:52.968565	2018-04-14 20:55:52.968565
299	186	32	1	t	2018-04-14 20:55:52.972364	2018-04-14 20:55:52.972364
300	187	7	2	f	2018-04-14 20:55:52.976621	2018-04-14 20:55:52.976621
301	188	88	1	f	2018-04-14 20:55:52.983921	2018-04-14 20:55:52.983921
302	188	88	2	t	2018-04-14 20:55:52.988422	2018-04-14 20:55:52.988422
303	190	28	1	f	2018-04-14 20:55:52.992367	2018-04-14 20:55:52.992367
304	190	104	1	t	2018-04-14 20:55:52.996491	2018-04-14 20:55:52.996491
305	191	28	1	f	2018-04-14 20:55:53.00028	2018-04-14 20:55:53.00028
306	191	104	1	t	2018-04-14 20:55:53.003944	2018-04-14 20:55:53.003944
307	192	41	1	f	2018-04-14 20:55:53.007735	2018-04-14 20:55:53.007735
308	192	41	2	t	2018-04-14 20:55:53.011404	2018-04-14 20:55:53.011404
309	193	40	1	f	2018-04-14 20:55:53.015253	2018-04-14 20:55:53.015253
310	193	36	1	t	2018-04-14 20:55:53.019552	2018-04-14 20:55:53.019552
311	194	89	3	f	2018-04-14 20:55:53.02306	2018-04-14 20:55:53.02306
312	194	95	2	t	2018-04-14 20:55:53.026947	2018-04-14 20:55:53.026947
313	196	48	4	f	2018-04-14 20:55:53.030816	2018-04-14 20:55:53.030816
314	196	52	4	t	2018-04-14 20:55:53.034642	2018-04-14 20:55:53.034642
315	197	38	1	f	2018-04-14 20:55:53.038564	2018-04-14 20:55:53.038564
316	197	101	1	t	2018-04-14 20:55:53.042403	2018-04-14 20:55:53.042403
317	198	85	2	f	2018-04-14 20:55:53.046806	2018-04-14 20:55:53.046806
318	198	28	1	t	2018-04-14 20:55:53.050497	2018-04-14 20:55:53.050497
319	199	26	1	f	2018-04-14 20:55:53.054329	2018-04-14 20:55:53.054329
320	199	26	2	t	2018-04-14 20:55:53.058045	2018-04-14 20:55:53.058045
321	200	43	1	f	2018-04-14 20:55:53.061943	2018-04-14 20:55:53.061943
322	200	31	1	t	2018-04-14 20:55:53.065742	2018-04-14 20:55:53.065742
323	201	40	1	f	2018-04-14 20:55:53.070085	2018-04-14 20:55:53.070085
324	201	40	3	t	2018-04-14 20:55:53.074121	2018-04-14 20:55:53.074121
325	202	38	1	f	2018-04-14 20:55:53.077892	2018-04-14 20:55:53.077892
326	202	55	1	t	2018-04-14 20:55:53.081662	2018-04-14 20:55:53.081662
327	203	22	10	f	2018-04-14 20:55:53.085613	2018-04-14 20:55:53.085613
328	203	22	20	t	2018-04-14 20:55:53.08942	2018-04-14 20:55:53.08942
329	204	43	1	f	2018-04-14 20:55:53.093278	2018-04-14 20:55:53.093278
330	204	43	2	t	2018-04-14 20:55:53.097211	2018-04-14 20:55:53.097211
331	205	32	1	f	2018-04-14 20:55:53.100882	2018-04-14 20:55:53.100882
332	205	83	1	t	2018-04-14 20:55:53.104757	2018-04-14 20:55:53.104757
333	206	49	4	f	2018-04-14 20:55:53.10891	2018-04-14 20:55:53.10891
334	206	63	1	t	2018-04-14 20:55:53.112911	2018-04-14 20:55:53.112911
335	207	41	2	f	2018-04-14 20:55:53.117771	2018-04-14 20:55:53.117771
336	207	96	1	t	2018-04-14 20:55:53.122454	2018-04-14 20:55:53.122454
337	209	43	2	f	2018-04-14 20:55:53.126261	2018-04-14 20:55:53.126261
338	209	78	1	t	2018-04-14 20:55:53.131228	2018-04-14 20:55:53.131228
339	210	43	1	f	2018-04-14 20:55:53.134807	2018-04-14 20:55:53.134807
340	210	78	1	t	2018-04-14 20:55:53.138449	2018-04-14 20:55:53.138449
341	211	43	1	f	2018-04-14 20:55:53.142383	2018-04-14 20:55:53.142383
342	211	74	1	t	2018-04-14 20:55:53.146332	2018-04-14 20:55:53.146332
343	212	34	1	f	2018-04-14 20:55:53.150504	2018-04-14 20:55:53.150504
344	212	23	1	t	2018-04-14 20:55:53.154447	2018-04-14 20:55:53.154447
345	213	43	1	f	2018-04-14 20:55:53.158298	2018-04-14 20:55:53.158298
346	213	112	1	t	2018-04-14 20:55:53.162457	2018-04-14 20:55:53.162457
347	214	7	1	f	2018-04-14 20:55:53.166531	2018-04-14 20:55:53.166531
348	214	7	2	t	2018-04-14 20:55:53.170571	2018-04-14 20:55:53.170571
349	216	31	1	f	2018-04-14 20:55:53.174542	2018-04-14 20:55:53.174542
350	216	32	1	t	2018-04-14 20:55:53.178326	2018-04-14 20:55:53.178326
351	217	43	1	f	2018-04-14 20:55:53.182325	2018-04-14 20:55:53.182325
352	217	67	1	t	2018-04-14 20:55:53.186209	2018-04-14 20:55:53.186209
353	218	44	3	f	2018-04-14 20:55:53.190079	2018-04-14 20:55:53.190079
354	218	28	2	t	2018-04-14 20:55:53.194802	2018-04-14 20:55:53.194802
355	219	79	1	f	2018-04-14 20:55:53.201726	2018-04-14 20:55:53.201726
356	219	28	1	t	2018-04-14 20:55:53.209421	2018-04-14 20:55:53.209421
357	220	26	1	f	2018-04-14 20:55:53.21503	2018-04-14 20:55:53.21503
358	220	50	1	t	2018-04-14 20:55:53.219252	2018-04-14 20:55:53.219252
359	221	55	1	f	2018-04-14 20:55:53.223176	2018-04-14 20:55:53.223176
360	221	55	2	t	2018-04-14 20:55:53.227	2018-04-14 20:55:53.227
361	222	44	3	f	2018-04-14 20:55:53.230781	2018-04-14 20:55:53.230781
362	222	28	2	t	2018-04-14 20:55:53.234599	2018-04-14 20:55:53.234599
363	223	79	1	f	2018-04-14 20:55:53.242399	2018-04-14 20:55:53.242399
364	223	28	1	t	2018-04-14 20:55:53.24664	2018-04-14 20:55:53.24664
365	225	90	1	f	2018-04-14 20:55:53.253293	2018-04-14 20:55:53.253293
366	225	72	1	t	2018-04-14 20:55:53.259342	2018-04-14 20:55:53.259342
367	229	67	1	f	2018-04-14 20:55:53.264916	2018-04-14 20:55:53.264916
368	229	32	1	t	2018-04-14 20:55:53.269676	2018-04-14 20:55:53.269676
369	230	72	1	f	2018-04-14 20:55:53.27451	2018-04-14 20:55:53.27451
370	230	72	3	t	2018-04-14 20:55:53.278288	2018-04-14 20:55:53.278288
371	231	89	3	f	2018-04-14 20:55:53.282187	2018-04-14 20:55:53.282187
372	231	89	4	t	2018-04-14 20:55:53.286114	2018-04-14 20:55:53.286114
\.


--
-- Name: steal_drops_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Kieran
--

SELECT pg_catalog.setval('steal_drops_id_seq', 372, true);


--
-- Name: abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: Kieran; Tablespace: 
--

ALTER TABLE ONLY abilities
    ADD CONSTRAINT abilities_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: Kieran; Tablespace: 
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: bribe_drops_pkey; Type: CONSTRAINT; Schema: public; Owner: Kieran; Tablespace: 
--

ALTER TABLE ONLY bribe_drops
    ADD CONSTRAINT bribe_drops_pkey PRIMARY KEY (id);


--
-- Name: elements_pkey; Type: CONSTRAINT; Schema: public; Owner: Kieran; Tablespace: 
--

ALTER TABLE ONLY elements
    ADD CONSTRAINT elements_pkey PRIMARY KEY (id);


--
-- Name: items_pkey; Type: CONSTRAINT; Schema: public; Owner: Kieran; Tablespace: 
--

ALTER TABLE ONLY items
    ADD CONSTRAINT items_pkey PRIMARY KEY (id);


--
-- Name: key_items_pkey; Type: CONSTRAINT; Schema: public; Owner: Kieran; Tablespace: 
--

ALTER TABLE ONLY key_items
    ADD CONSTRAINT key_items_pkey PRIMARY KEY (id);


--
-- Name: kill_drops_pkey; Type: CONSTRAINT; Schema: public; Owner: Kieran; Tablespace: 
--

ALTER TABLE ONLY kill_drops
    ADD CONSTRAINT kill_drops_pkey PRIMARY KEY (id);


--
-- Name: locations_monsters_pkey; Type: CONSTRAINT; Schema: public; Owner: Kieran; Tablespace: 
--

ALTER TABLE ONLY locations_monsters
    ADD CONSTRAINT locations_monsters_pkey PRIMARY KEY (id);


--
-- Name: locations_pkey; Type: CONSTRAINT; Schema: public; Owner: Kieran; Tablespace: 
--

ALTER TABLE ONLY locations
    ADD CONSTRAINT locations_pkey PRIMARY KEY (id);


--
-- Name: mix_items_pkey; Type: CONSTRAINT; Schema: public; Owner: Kieran; Tablespace: 
--

ALTER TABLE ONLY mix_items
    ADD CONSTRAINT mix_items_pkey PRIMARY KEY (id);


--
-- Name: mixes_pkey; Type: CONSTRAINT; Schema: public; Owner: Kieran; Tablespace: 
--

ALTER TABLE ONLY mixes
    ADD CONSTRAINT mixes_pkey PRIMARY KEY (id);


--
-- Name: monster_drop_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: Kieran; Tablespace: 
--

ALTER TABLE ONLY monster_drop_abilities
    ADD CONSTRAINT monster_drop_abilities_pkey PRIMARY KEY (id);


--
-- Name: monsters_pkey; Type: CONSTRAINT; Schema: public; Owner: Kieran; Tablespace: 
--

ALTER TABLE ONLY monsters
    ADD CONSTRAINT monsters_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: Kieran; Tablespace: 
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: steal_drops_pkey; Type: CONSTRAINT; Schema: public; Owner: Kieran; Tablespace: 
--

ALTER TABLE ONLY steal_drops
    ADD CONSTRAINT steal_drops_pkey PRIMARY KEY (id);


--
-- Name: index_abilities_on_item_id; Type: INDEX; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE INDEX index_abilities_on_item_id ON abilities USING btree (item_id);


--
-- Name: index_bribe_drops_on_item_id; Type: INDEX; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE INDEX index_bribe_drops_on_item_id ON bribe_drops USING btree (item_id);


--
-- Name: index_bribe_drops_on_monster_id; Type: INDEX; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE INDEX index_bribe_drops_on_monster_id ON bribe_drops USING btree (monster_id);


--
-- Name: index_elements_on_monster_id; Type: INDEX; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE INDEX index_elements_on_monster_id ON elements USING btree (monster_id);


--
-- Name: index_key_items_on_location_id; Type: INDEX; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE INDEX index_key_items_on_location_id ON key_items USING btree (location_id);


--
-- Name: index_kill_drops_on_item_id; Type: INDEX; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE INDEX index_kill_drops_on_item_id ON kill_drops USING btree (item_id);


--
-- Name: index_kill_drops_on_monster_id; Type: INDEX; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE INDEX index_kill_drops_on_monster_id ON kill_drops USING btree (monster_id);


--
-- Name: index_locations_monsters_on_location_id; Type: INDEX; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE INDEX index_locations_monsters_on_location_id ON locations_monsters USING btree (location_id);


--
-- Name: index_locations_monsters_on_monster_id; Type: INDEX; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE INDEX index_locations_monsters_on_monster_id ON locations_monsters USING btree (monster_id);


--
-- Name: index_mix_items_on_mix_id; Type: INDEX; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE INDEX index_mix_items_on_mix_id ON mix_items USING btree (mix_id);


--
-- Name: index_monster_drop_abilities_on_ability_id; Type: INDEX; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE INDEX index_monster_drop_abilities_on_ability_id ON monster_drop_abilities USING btree (ability_id);


--
-- Name: index_monster_drop_abilities_on_monster_id; Type: INDEX; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE INDEX index_monster_drop_abilities_on_monster_id ON monster_drop_abilities USING btree (monster_id);


--
-- Name: index_steal_drops_on_item_id; Type: INDEX; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE INDEX index_steal_drops_on_item_id ON steal_drops USING btree (item_id);


--
-- Name: index_steal_drops_on_monster_id; Type: INDEX; Schema: public; Owner: Kieran; Tablespace: 
--

CREATE INDEX index_steal_drops_on_monster_id ON steal_drops USING btree (monster_id);


--
-- Name: fk_rails_0ea6c5750c; Type: FK CONSTRAINT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY mix_items
    ADD CONSTRAINT fk_rails_0ea6c5750c FOREIGN KEY (mix_id) REFERENCES mixes(id);


--
-- Name: fk_rails_13452c2cc5; Type: FK CONSTRAINT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY monster_drop_abilities
    ADD CONSTRAINT fk_rails_13452c2cc5 FOREIGN KEY (monster_id) REFERENCES monsters(id);


--
-- Name: fk_rails_1cb8105058; Type: FK CONSTRAINT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY steal_drops
    ADD CONSTRAINT fk_rails_1cb8105058 FOREIGN KEY (item_id) REFERENCES items(id);


--
-- Name: fk_rails_481ec0ecbd; Type: FK CONSTRAINT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY kill_drops
    ADD CONSTRAINT fk_rails_481ec0ecbd FOREIGN KEY (item_id) REFERENCES items(id);


--
-- Name: fk_rails_663d51ca14; Type: FK CONSTRAINT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY key_items
    ADD CONSTRAINT fk_rails_663d51ca14 FOREIGN KEY (location_id) REFERENCES locations(id);


--
-- Name: fk_rails_9193238058; Type: FK CONSTRAINT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY locations_monsters
    ADD CONSTRAINT fk_rails_9193238058 FOREIGN KEY (location_id) REFERENCES locations(id);


--
-- Name: fk_rails_a10b5238db; Type: FK CONSTRAINT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY steal_drops
    ADD CONSTRAINT fk_rails_a10b5238db FOREIGN KEY (monster_id) REFERENCES monsters(id);


--
-- Name: fk_rails_b7a1c72c0d; Type: FK CONSTRAINT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY elements
    ADD CONSTRAINT fk_rails_b7a1c72c0d FOREIGN KEY (monster_id) REFERENCES monsters(id);


--
-- Name: fk_rails_bcf2da4bf0; Type: FK CONSTRAINT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY abilities
    ADD CONSTRAINT fk_rails_bcf2da4bf0 FOREIGN KEY (item_id) REFERENCES items(id);


--
-- Name: fk_rails_c465755bc6; Type: FK CONSTRAINT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY bribe_drops
    ADD CONSTRAINT fk_rails_c465755bc6 FOREIGN KEY (monster_id) REFERENCES monsters(id);


--
-- Name: fk_rails_c87d89b58a; Type: FK CONSTRAINT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY locations_monsters
    ADD CONSTRAINT fk_rails_c87d89b58a FOREIGN KEY (monster_id) REFERENCES monsters(id);


--
-- Name: fk_rails_d30352a8e3; Type: FK CONSTRAINT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY monster_drop_abilities
    ADD CONSTRAINT fk_rails_d30352a8e3 FOREIGN KEY (ability_id) REFERENCES abilities(id);


--
-- Name: fk_rails_ddb9c5f808; Type: FK CONSTRAINT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY kill_drops
    ADD CONSTRAINT fk_rails_ddb9c5f808 FOREIGN KEY (monster_id) REFERENCES monsters(id);


--
-- Name: fk_rails_e3a46a8ee2; Type: FK CONSTRAINT; Schema: public; Owner: Kieran
--

ALTER TABLE ONLY bribe_drops
    ADD CONSTRAINT fk_rails_e3a46a8ee2 FOREIGN KEY (item_id) REFERENCES items(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: Kieran
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM "Kieran";
GRANT ALL ON SCHEMA public TO "Kieran";
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

